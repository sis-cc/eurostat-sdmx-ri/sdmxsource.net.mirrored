// -----------------------------------------------------------------------
// <copyright file="HeaderWriter.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxParseBase.
//     SdmxParseBase is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxParseBase is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxParseBase.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.SdmxParseBase.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Xml;

    using Estat.Sri.SdmxParseBase.Model;
    using Estat.Sri.SdmxXmlConstants;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.Util.Date;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util.Attributes;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     This class is used by all the other message writers classes to write the Sdmx Header
    /// </summary>
    /// <remarks>
    ///     This class was copied from DataGenerator
    /// </remarks>
    public class HeaderWriter : Writer
    {
        /// <summary>
        ///     The default sender id.
        /// </summary>
        private const string DefaultSenderId = "ESTAT";

        /// <summary>
        ///     The _retrieval manager
        /// </summary>
        private readonly IHeaderRetrievalManager _retrievalManager;

        /// <summary>
        ///     Initializes a new instance of the <see cref="HeaderWriter" /> class.
        /// </summary>
        /// <param name="writer">
        ///     The writer.
        /// </param>
        /// <param name="namespaces">
        ///     The namespaces.
        /// </param>
        /// <param name="schema">
        ///     The schema.
        /// </param>
        public HeaderWriter([ValidatedNotNull]XmlWriter writer, SdmxNamespaces namespaces, [ValidatedNotNull] SdmxSchema schema)
            : base(writer, namespaces, schema)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="HeaderWriter" /> class.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="namespaces">The namespaces.</param>
        /// <param name="schema">The schema.</param>
        /// <param name="retrievalManager">The header retrieval manager.</param>
        public HeaderWriter(
            [ValidatedNotNull]XmlWriter writer, 
            SdmxNamespaces namespaces, 
            [ValidatedNotNull]SdmxSchema schema, 
            IHeaderRetrievalManager retrievalManager)
            : base(writer, namespaces, schema)
        {
            this._retrievalManager = retrievalManager;
        }

        /// <summary>
        ///     Gets the default namespace
        /// </summary>
        protected override NamespacePrefixPair DefaultNS
        {
            get
            {
                return this.Namespaces.Message;
            }
        }

        /// <summary>
        ///     This is the main method of the class that writes the <see cref="IHeader" />
        ///     The methods write the xml Header tag and add it's attributes.
        /// </summary>
        /// <param name="header">
        ///     The <see cref="IHeader" /> object containing the header data to be written
        /// </param>
        public void WriteHeader(IHeader header)
        {
           this.WriteHeader(header, null,null,new IDataStructureObject[0]);
        }

        /// <summary>
        ///     This is the main method of the class that writes the <see cref="IHeader" />
        ///     The methods write the xml Header tag and add it's attributes.
        /// </summary>
        /// <param name="header">
        ///     The <see cref="IHeader" /> object containing the header data to be written
        /// </param>
        /// <param name="dimensionAtObservation">
        ///     The dimension At Observation.
        /// </param>
        /// <param name="datasetStructureReference"></param>
        /// <param name="dataStructureObjects">
        ///     The data Structure Objects.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="dataStructureObjects"/> is <see langword="null" />.</exception>
        public void WriteHeader(IHeader header,
            string dimensionAtObservation,
            IDatasetStructureReference datasetStructureReference,
            params IDataStructureObject[] dataStructureObjects)
        {

            if (dataStructureObjects is null)
            {
                throw new ArgumentNullException(nameof(dataStructureObjects));
            }

            if (this.TargetSchema == SdmxSchemaEnumType.VersionTwo)
            {
                WriteHeader20(header, dataStructureObjects);
            }
            else
            {
                WriteHeader21(header, dimensionAtObservation, datasetStructureReference, dataStructureObjects);
            }

        }

        public void WriteHeader(IHeader header,
            string dimensionAtObservation,
            IDatasetStructureReference datasetStructureReference,
            params IDataflowObject[] dataflows)
        {
            if (dataflows is null)
            {
                throw new ArgumentNullException(nameof(dataflows));
            }

            if (this.TargetSchema == SdmxSchemaEnumType.VersionTwo)
            {
                WriteHeader20(header, dataflows);
            }
            else
            {
                WriteHeader21(header, dimensionAtObservation, datasetStructureReference, dataflows);
            }
        }

        /// <summary>
        ///     This is the main method of the class that writes the <see cref="IHeader" /> for SDMX 2.0
        ///     The methods write the xml Header tag and add it's attributes.
        /// </summary>
        /// <param name="header">
        ///     The <see cref="IHeader" /> object containing the header data to be written
        /// </param>
        /// <param name="datastructures">
        ///     The data Structure Objects.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="dataStructureObjects"/> is <see langword="null" />.</exception>
        private void WriteHeader20<T>(IHeader header,
            IList<T> datastructures) where T: IMaintainableObject
        {
            IHeader providedHeader = header;
            SdmxSchemaEnumType sdmxSchema = this.TargetSchema.EnumType;

            // TODO split to v2 and v2.1 to reduce complexity and more readable
            if (header == null)
            {
                header = this._retrievalManager != null
                             ? this._retrievalManager.Header
                             : new HeaderImpl(
                                   string.Format(System.Globalization.CultureInfo.InvariantCulture, "IDREF{0}", DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture)), 
                                   DefaultSenderId);
            }

            if (datastructures == null)
            {
                throw new ArgumentNullException("datastructures");
            }
            IStructureReference structureReference;
            if (datastructures.Count == 1)
            {
                structureReference = datastructures[0].AsReference;
            }
            else
            {
                structureReference = new StructureReferenceImpl();
            }

            // start header
            this.WriteStartElement(this.Namespaces.Message, ElementNameTable.Header);

            // id
            this.TryToWriteElement(this.Namespaces.Message, ElementNameTable.ID, header.Id);

            // test
            this.WriteElement(this.Namespaces.Message, ElementNameTable.Test, header.Test);

            // truncated (2.0)
            string elementName = NameTableCache.GetElementName(ElementNameTable.Truncated);
            bool isTruncated = header.HasAdditionalAttribute(elementName)
                               && bool.TrueString.Equals(header.GetAdditionalAttribtue(elementName));
            this.TryToWriteElement(this.Namespaces.Message, ElementNameTable.Truncated, isTruncated);

            // names (2.0)
            this.WriteTextType(this.Namespaces.Message, header.Name, ElementNameTable.Name);

            // prepared
            DateTime prepared = header.Prepared.HasValue ? header.Prepared.Value : DateTime.Now;
            this.TryToWriteElement(this.Namespaces.Message, ElementNameTable.Prepared, DateUtil.FormatDate(prepared));

            // sender TODO ensure/check that Sender cannot be null
            this.WritePartyType(header.Sender, ElementNameTable.Sender);

            // receiver TODO ensure/check that Receiver cannot be null
            foreach (IParty text in header.Receiver)
            {
                this.WritePartyType(text, ElementNameTable.Receiver);
            }

            if (structureReference != null)
            {
                // HACK This header can be used also for non data file where there is no structureReference
                // TODO refactor to have different writer for 2.0/2.1/3.0.0 and (metadata) data vs no-data messages
                if (structureReference.MaintainableStructureEnumType?.EnumType == SdmxStructureEnumType.Dsd)
                {
                    // keyfamily ref (2.0)
                    this.TryToWriteElement(this.Namespaces.Message, ElementNameTable.KeyFamilyRef, structureReference.MaintainableId);

                    // keyfamily agency (2.0)
                    this.TryToWriteElement(this.Namespaces.Message, ElementNameTable.KeyFamilyAgency, structureReference.AgencyId);
                }
                else if (structureReference.HasAgencyId())
                {
                    this.TryToWriteElement(this.Namespaces.Message, ElementNameTable.DataSetAgency, structureReference.AgencyId);
                }

                if (!string.IsNullOrEmpty(header.DatasetId))
                {
                    // datasetId (2.0) - NOTE in java 0.9.4 because they use buffering, if null they retrieve this info from dataset header. We can't
                    this.TryToWriteElement(this.Namespaces.Message, ElementNameTable.DataSetID, header.DatasetId);
                }
                else if (structureReference.HasMaintainableId())
                {
                    this.TryToWriteElement(this.Namespaces.Message, ElementNameTable.DataSetID, structureReference.MaintainableId);
                }
            }

            if (header.Action != null)
            {
                // datasetaction (2.0) - NOTE in java 0.9.4 because they use buffering, if null they retrieve this info from dataset header. We can't
                this.TryToWriteElement(
                    this.Namespaces.Message,
                    ElementNameTable.DataSetAction,
                    header.Action.Action);
            }

            // extracted
            this.TryToWriteElement(this.Namespaces.Message, ElementNameTable.Extracted, header.Extracted);

            // report begin
            this.TryToWriteElement(this.Namespaces.Message, ElementNameTable.ReportingBegin, header.ReportingBegin);

            // report end
            this.TryToWriteElement(this.Namespaces.Message, ElementNameTable.ReportingEnd, header.ReportingEnd);

            // source
            this.WriteTextType(this.Namespaces.Message, header.Source, ElementNameTable.Source);

            this.WriteEndElement();
        }

        /// <summary>
        ///     This is the main method of the class that writes the <see cref="IHeader" />
        ///     The methods write the xml Header tag and add it's attributes.
        /// </summary>
        /// <param name="header">
        ///     The <see cref="IHeader" /> object containing the header data to be written
        /// </param>
        /// <param name="dimensionAtObservation">
        ///     The dimension At Observation.
        /// </param>
        /// <param name="datasetStructureReference"></param>
        /// <param name="dataStructureObjects">
        ///     The data Structure Objects.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="dataStructureObjects"/> or <pis <see langword="null" />.</exception>
        private void WriteHeader21<T>(IHeader header,
            string dimensionAtObservation,
            IDatasetStructureReference datasetStructureReference,
            IList<T> dataStructureObjects
            ) where T: IMaintainableObject
        {
            IHeader providedHeader = header;
            SdmxSchemaEnumType sdmxSchema = this.TargetSchema.EnumType;

            if (header == null)
            {
                header = this._retrievalManager != null
                             ? this._retrievalManager.Header
                             : new HeaderImpl(
                                   string.Format(System.Globalization.CultureInfo.InvariantCulture, "IDREF{0}", DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture)),
                                   DefaultSenderId);
            }

            if (dataStructureObjects == null)
            {
                throw new ArgumentNullException("dataStructureObjects");
            }

            // start header
            this.WriteStartElement(this.Namespaces.Message, ElementNameTable.Header);

            // id
            this.TryToWriteElement(this.Namespaces.Message, ElementNameTable.ID, header.Id);

            // test
            this.WriteElement(this.Namespaces.Message, ElementNameTable.Test, header.Test);

            // prepared
            DateTime prepared = header.Prepared.HasValue ? header.Prepared.Value : DateTime.Now;
            this.TryToWriteElement(this.Namespaces.Message, ElementNameTable.Prepared, DateUtil.FormatDate(prepared));

            // sender TODO ensure/check that Sender cannot be null
            this.WritePartyType(header.Sender, ElementNameTable.Sender);

            // receiver TODO ensure/check that Receiver cannot be null
            foreach (IParty text in header.Receiver)
            {
                this.WritePartyType(text, ElementNameTable.Receiver);
            }

            this.WriteTextType(this.Namespaces.Common, header.Name, ElementNameTable.Name);
            Action<NamespacePrefixPair, ElementNameTable, IStructureReference> writeRef;
            if (sdmxSchema == SdmxSchemaEnumType.VersionThree)
            {
                writeRef = WriteRef30;
            }
            else
            {
                writeRef = WriteRef21;
            }

            if (dataStructureObjects.Count > 0)
            {
                ElementNameTable structureElement = GetStructureElement(dataStructureObjects[0].StructureType);


                // structures (2.1)
                foreach (T datasetStructure in dataStructureObjects)
                {
                    // start structure  (message ns)
                    this.WriteStartElement(this.Namespaces.Message, ElementNameTable.Structure);

                    // write structureId attribute
                    this.WriteAttributeString(AttributeNameTable.structureID, GetRef(datasetStructure));

                    // structure specific namespace attribute
                    if (this.Namespaces.DataSetStructureSpecific != null)
                    {
                        this.WriteAttributeString(
                            AttributeNameTable.@namespace,
                            this.Namespaces.DataSetStructureSpecific.NS);

                        if (datasetStructureReference != null && datasetStructureReference.IsExplicitMeasures)
                        {
                            this.WriteAttributeString(AttributeNameTable.explicitMeasures, "true");
                        }
                    }

                    // dimension at observation attribute
                    string dimensionAtObs = dimensionAtObservation ?? DimensionObject.TimeDimensionFixedId;
                    this.WriteAttributeString(AttributeNameTable.dimensionAtObservation, dimensionAtObs);

                    writeRef(this.Namespaces.Common, structureElement, datasetStructure.AsReference);

                    // end structure (message ns)
                    this.WriteEndElement();
                }
            }

            if (providedHeader != null)
            {
                if (header.DataProviderReference != null)
                {
                    writeRef(this.Namespaces.Message, ElementNameTable.DataProvider, header.DataProviderReference);
                }

                if (header.Action != null)
                {
                    // datasetaction (2.0) - NOTE in java 0.9.4 because they use buffering, if null they retrieve this info from dataset header. We can't
                    // TODO check also DataSetHeader 
                    this.TryToWriteElement(
                        this.Namespaces.Message,
                        ElementNameTable.DataSetAction,
                        header.Action.Action);
                }

                // datasetId (2.1) - NOTE in java 0.9.4 because they use buffering, if null they retrieve this info from dataset header. We can't
                // TODO check also DataSetHeader 
                this.TryToWriteElement(this.Namespaces.Message, ElementNameTable.DataSetID, header.DatasetId);


                // extracted
                this.TryToWriteElement(this.Namespaces.Message, ElementNameTable.Extracted, header.Extracted);

                // report begin
                this.TryToWriteElement(this.Namespaces.Message, ElementNameTable.ReportingBegin, header.ReportingBegin);

                // report end
                this.TryToWriteElement(this.Namespaces.Message, ElementNameTable.ReportingEnd, header.ReportingEnd);

                // embargo date (2.1)
                this.TryToWriteElement(this.Namespaces.Message, ElementNameTable.EmbargoDate, header.EmbargoDate);

                // source
                this.WriteTextType(this.Namespaces.Message, header.Source, ElementNameTable.Source);
            }

            this.WriteEndElement();
        }

        private void WriteRef21(NamespacePrefixPair ns, ElementNameTable structureElement, IStructureReference structureReference)
        {
            // start structure (common ns)
            this.WriteStartElement(ns, structureElement);

            // start Ref
            this.WriteStartElement(ElementNameTable.Ref);
            this.WriteAttributeString(AttributeNameTable.agencyID, structureReference.AgencyId);
            if (structureReference.HasChildReference())
            {
                this.WriteAttributeString(AttributeNameTable.maintainableParentID, structureReference.MaintainableId);
                this.WriteAttributeString(AttributeNameTable.maintainableParentVersion, structureReference.Version);
                this.WriteAttributeString(AttributeNameTable.version, structureReference.Version);
            }
            else
            {
                this.WriteAttributeString(AttributeNameTable.id, structureReference.MaintainableId);
                this.WriteAttributeString(AttributeNameTable.version, structureReference.Version);

            }

            // end Ref
            this.WriteEndElement();
            // end common structure
            this.WriteEndElement();
        }
        private void WriteRef30(NamespacePrefixPair ns, ElementNameTable structureElement, IStructureReference structureReference) 
        {
            // start structure (common ns)
            this.TryToWriteElement(ns, structureElement, structureReference.TargetUrn);
        }


        private ElementNameTable GetStructureElement(SdmxStructureEnumType type)
        {
            switch(type)
            {
                case SdmxStructureEnumType.Dsd:
                    return ElementNameTable.Structure;
                case SdmxStructureEnumType.Dataflow: 
                    return ElementNameTable.StructureUsage;
                case SdmxStructureEnumType.ProvisionAgreement:
                    return ElementNameTable.ProvisionAgreement;
                default:
                    throw new SdmxNotImplementedException("Unsupported structure type for Structure Specific Data reference");
            }
        }

        /// <summary>
        ///     This is an internal method that is used to write a <see cref="IContact" />
        ///     The method creates a xml Contact element
        /// </summary>
        /// <param name="contactObj">
        ///     The <see cref="IContact" /> object containing the data to be written
        /// </param>
        private void WriteContactType(IContact contactObj)
        {
            this.WriteStartElement(this.Namespaces.Message, ElementNameTable.Contact);
            this.WriteTextType(this.NameNamespace, contactObj.Name, ElementNameTable.Name);
            this.WriteTextType(this.Namespaces.Message, contactObj.Departments, ElementNameTable.Department);
            this.WriteTextType(this.Namespaces.Message, contactObj.Role, ElementNameTable.Role);

            this.WriteListContacts(this.Namespaces.Message, ElementNameTable.Telephone, contactObj.Telephone);
            this.WriteListContacts(this.Namespaces.Message, ElementNameTable.Fax, contactObj.Fax);
            this.WriteListContacts(this.Namespaces.Message, ElementNameTable.X400, contactObj.X400);
            this.WriteListContacts(this.Namespaces.Message, ElementNameTable.URI, contactObj.Uri);
            this.WriteListContacts(this.Namespaces.Message, ElementNameTable.Email, contactObj.Email);
            this.WriteEndElement();
        }

        /// <summary>
        ///     Write the list contacts.
        /// </summary>
        /// <param name="namespacePrefixPair">
        ///     The namespace prefix pair.
        /// </param>
        /// <param name="element">
        ///     The element.
        /// </param>
        /// <param name="list">
        ///     The list.
        /// </param>
        private void WriteListContacts(
            NamespacePrefixPair namespacePrefixPair, 
            ElementNameTable element, 
            IEnumerable<string> list)
        {
            foreach (string value in list)
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.WriteElement(namespacePrefixPair, element, value);
                }
            }
        }

        /// <summary>
        ///     This is an internal method that is used to write the specified <paramref name="partyObj" />
        /// </summary>
        /// <param name="partyObj">
        ///     The <see cref="IParty" /> object containing the data to be written
        /// </param>
        /// <param name="name">
        ///     The name of the xml element
        /// </param>
        private void WritePartyType(IParty partyObj, ElementNameTable name)
        {
            this.WriteStartElement(this.Namespaces.Message, name);

            this.TryWriteAttribute(AttributeNameTable.id, partyObj.Id);
            this.WriteTextType(this.NameNamespace, partyObj.Name, ElementNameTable.Name);

            foreach (IContact contact in partyObj.Contacts)
            {
                this.WriteContactType(contact);
            }

            this.WriteEndElement();
        }
    }
}