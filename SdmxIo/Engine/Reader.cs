// -----------------------------------------------------------------------
// <copyright file="Reader.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxParseBase.
//     SdmxParseBase is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxParseBase is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxParseBase.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.SdmxParseBase.Engine
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml;

    using Estat.Sri.SdmxParseBase.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Util.Attributes;

    /// <summary>
    ///     The reader.
    /// </summary>
    public abstract class Reader : IoBase
    {
        /// <summary>
        ///     The current XML attributes in a name-value map
        /// </summary>
        private readonly IDictionary<string, string> _attributes = new Dictionary<string, string>(
            StringComparer.Ordinal);

        /// <summary>
        ///     The stack of elements to parsed, used by xml parser.
        /// </summary>
        private readonly Stack<object> _elements = new Stack<object>();

        /// <summary>
        ///     The <c>xsd</c> schemas used by sdmx
        /// </summary>
        private readonly XmlReaderSettings _settings;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Reader" /> class.
        /// </summary>
        /// <param name="namespaces">
        ///     The namespaces.
        /// </param>
        /// <param name="schema">
        ///     The schema.
        /// </param>
        protected Reader(SdmxNamespaces namespaces, [ValidatedNotNull]SdmxSchema schema)
            : base(namespaces, schema)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Reader" /> class.
        /// </summary>
        /// <param name="schema">
        ///     The schema.
        /// </param>
        protected Reader(SdmxSchema schema)
            : this(schema, null)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Reader" /> class.
        /// </summary>
        /// <param name="schema">
        ///     The schema.
        /// </param>
        /// <param name="settings">
        ///     The settings.
        /// </param>
        protected Reader(SdmxSchema schema, XmlReaderSettings settings)
            : this(null, schema, settings)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Reader" /> class.
        /// </summary>
        /// <param name="namespaces">
        ///     The namespaces.
        /// </param>
        /// <param name="schema">
        ///     The schema.
        /// </param>
        /// <param name="settings">
        ///     The settings.
        /// </param>
        protected Reader(SdmxNamespaces namespaces, [ValidatedNotNull] SdmxSchema schema, XmlReaderSettings settings)
            : base(namespaces, schema)
        {
            this._settings = settings;
        }

        /// <summary>
        ///     Delegate for handling elements
        /// </summary>
        /// <typeparam name="T">
        ///     The type of the <paramref name="parent" />
        /// </typeparam>
        /// <param name="parent">
        ///     The parent object
        /// </param>
        /// <param name="localName">
        ///     the local name string as object
        /// </param>
        /// <returns>
        ///     The current object or null
        /// </returns>
        protected delegate object HandleComplexElement<in T>(T parent, object localName);

        /// <summary>
        ///     Delegate for handling simple element types with only text
        /// </summary>
        /// <typeparam name="T">
        ///     The type of the <paramref name="parent" />
        /// </typeparam>
        /// <param name="parent">
        ///     The parent object
        /// </param>
        /// <param name="localName">
        ///     the local name string as object
        /// </param>
        protected delegate void HandleText<in T>(T parent, object localName);

        /// <summary>
        ///     Gets the current XML attributes in a name-value map
        /// </summary>
        protected IDictionary<string, string> Attributes
        {
            get
            {
                return this._attributes;
            }
        }

        /// <summary>
        ///     Gets the stack of elements to parsed, used by xml parser.
        /// </summary>
        protected Stack<object> Elements
        {
            get
            {
                return this._elements;
            }
        }

        /// <summary>
        ///     Gets the <c>xsd</c> schemas used by sdmx
        /// </summary>
        protected XmlReaderSettings Settings
        {
            get
            {
                return this._settings;
            }
        }

        /// <summary>
        ///     Create an <see cref="XmlReader" /> object with <see cref="_settings" /> and the specified
        ///     <paramref name="textReader" />
        /// </summary>
        /// <param name="textReader">
        ///     The input <see cref="TextReader" />
        /// </param>
        /// <returns>
        ///     an <see cref="XmlReader" />
        /// </returns>
        protected XmlReader CreateXmlReader(TextReader textReader)
        {
            return XmlReader.Create(textReader, this._settings);
        }

        /// <summary>
        ///     Read the header and return the <see cref="IHeader" />
        /// </summary>
        /// <param name="reader">
        ///     The <see cref="XmlReader" /> to read the header from
        /// </param>
        /// <returns>
        ///     the <see cref="IHeader" />
        /// </returns>
        protected IHeader ReadHeader(XmlReader reader)
        {
            var headerReader = new HeaderReader(this.Namespaces, this.TargetSchema);
            return headerReader.Read(reader);
        }
    }
}