// -----------------------------------------------------------------------
// <copyright file="StructureXmlSamples.cs" company="EUROSTAT">
//   Date Created : 2022-02-27
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Sdmx.TestUtils.XmlRoundtripTests
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// Has methods to return all the xml sample files for structures for different SDMX schema versions.
    /// </summary>
    public class StructureXmlSamples
    {
        private static IEnumerable<object> ReturnFiles(string[] dirs, SdmxSchemaEnumType sdmxSchema, params string[] excludeFiles)
        {
            foreach (var dir in dirs)
            {
                foreach (var file in Directory.GetFiles(dir))
                {
                    if (!excludeFiles.Any(f => file.EndsWith(f)) && Path.GetExtension(file).Equals(".xml"))
                    {
                        yield return new object[] { file, sdmxSchema };
                    }
                }
            }
        }

        public static IEnumerable<object> GetV30samples(params string[] excludeFiles)
        {
            string[] dirs = new string[]
            {
                "tests/v30/",
                "tests/v30/samples/Dataflow",
                "tests/v30/samples/Concept Scheme",
                "tests/v30/samples/Codelist",
                "tests/v30/samples/Annotations"
            };
            return ReturnFiles(dirs, SdmxSchemaEnumType.VersionThree, excludeFiles);
        }

        public static IEnumerable<object> GetV21samples(params string[] excludeFiles)
        {
            string[] dirs = new string[]
            {
                "tests/v21/Structure/"
            };
            return ReturnFiles(dirs, SdmxSchemaEnumType.VersionTwoPointOne, excludeFiles);
        }
    }
}

