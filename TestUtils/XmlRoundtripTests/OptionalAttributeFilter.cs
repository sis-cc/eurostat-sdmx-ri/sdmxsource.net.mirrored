// -----------------------------------------------------------------------
// <copyright file="OptionalAttributeFilter.cs" company="EUROSTAT">
//   Date Created : 2022-02-27
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Sdmx.TestUtils.XmlRoundtripTests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Xml;

    /// <summary>
    /// This class is used to determine the comparison outcom between two xml files, 
    /// for the xml attributes that are declared optional in the xsd schema.
    /// An optional attribute may not exist in an xml and exist with the default value in another. 
    /// In this case the two xml files should be considered identical.
    /// </summary>
    public class OptionalAttributeFilter
    {
        /// <summary>
        /// A delegate to request the <see cref="OptionalXmlAttribute"/>s for an element.
        /// </summary>
        private readonly Func<string, IEnumerable<OptionalXmlAttribute>> _getOptionalAttributesForType;

        /// <summary>
        /// Initializes a new instance of the class <see cref="OptionalAttributeFilter"/>
        /// </summary>
        /// <param name="getOptionalAttributesForType">A delegate to request the <see cref="OptionalXmlAttribute"/>s for an element.</param>
        public OptionalAttributeFilter(Func<string, IEnumerable<OptionalXmlAttribute>> getOptionalAttributesForType)
        {
            _getOptionalAttributesForType = getOptionalAttributesForType;
        }

        /// <summary>
        /// Desides if an <see cref="XmlAttribute"/> will be evaluated during comparison or not.
        /// It is invoked on both control and test files.
        /// </summary>
        /// <param name="attribute">The attribute to filter.</param>
        /// <returns><c>true</c> if attribute will be evaluated, <c>false</c> if attribute will not.</returns>
        public bool ExcludeOptionalAttributes(XmlAttribute attribute)
        {
            if (attribute is null)
            {
                throw new ArgumentNullException(nameof(attribute));
            }
            if (_getOptionalAttributesForType is null)
            {
                throw new ArgumentNullException($"No {nameof(XmlOptionalAttributesParser)} is provided.");
            }

            // get optional attributes for this type
            var optionalAttributes = _getOptionalAttributesForType.Invoke(attribute.OwnerElement.LocalName);
            if (!optionalAttributes.Any())
            {
                return true;
            }

            if (optionalAttributes.Any(p => IsMatch(attribute, p)))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Determines if an <see cref="XmlAttribute"/> matches an <see cref="OptionalXmlAttribute"/>.
        /// </summary>
        /// <param name="xmlAttribute">The <see cref="XmlAttribute"/> to test match.</param>
        /// <param name="optionalAttribute">The <see cref="OptionalXmlAttribute"/> to match against.</param>
        /// <returns><c>true</c> if is a match, <c>false</c> otherwise.</returns>
        private bool IsMatch(XmlAttribute xmlAttribute, OptionalXmlAttribute optionalAttribute)
        {
            if (xmlAttribute.LocalName != optionalAttribute.Name)
            {
                return false;
            }

            // In general, matching an optional attribute means having its default value.
            // If the default value is null, the attribute should not be excluded.
            // The values of control and test elements should be compared.
            // Exceptions are those optional attributes that are declared as such.
            // A problem with how the exceptional attributes are treated would be in the case where
            // both control and test attribute have a non-null value, but different to each other. 
            // They would both be excluded from comparison whatsoever.
            // The solution for all would be to move the whole process in the SDMXDifferenceEvaluator class.
            bool isMatched = optionalAttribute.IsExceptional ||
                (optionalAttribute.DefaultValue?.Equals(xmlAttribute.Value) ?? false);

            if (isMatched)
            {
                Debug.WriteLine("*SKIPPING EVALUATOIN OF ATTR WITH NAME " 
                    + xmlAttribute.Name + " AND VALUE " + xmlAttribute.Value 
                    + " . IT IS OPTIONAL WITH DEFAULT VALUE " + optionalAttribute.DefaultValue);
            }

            return isMatched;
        }
    }
}
