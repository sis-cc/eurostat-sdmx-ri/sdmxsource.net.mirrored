// -----------------------------------------------------------------------
// <copyright file="SDMXDifferenceEvaluator.cs" company="EUROSTAT">
//   Date Created : 2022-02-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Sdmx.TestUtils.XmlRoundtripTests
{
    using System;
    using System.Xml;
    using Org.Sdmxsource.Sdmx.Util.Date;
    using Org.XmlUnit.Diff;

    public static class SDMXDifferenceEvaluator
    {
        /// <summary>
        /// Implements the <see cref="DifferenceEvaluator"/> delegate.
        /// Evaluates the equiality of two timestamps.
        /// </summary>
        /// <param name="comparison">The <see cref="Comparison"/> object.</param>
        /// <param name="outcome">The <see cref="ComparisonResult"/> taken from previous steps.</param>
        /// <returns>A <see cref="ComparisonResult"/>.</returns>
        public static ComparisonResult EvaluateTimestamps(Comparison comparison, ComparisonResult outcome)
        {
            if (outcome == ComparisonResult.EQUAL ||
                outcome == ComparisonResult.SIMILAR)
            {
                return outcome;
            }
            if (comparison.Type != ComparisonType.TEXT_VALUE)
            {
                return outcome;
            }
            if (comparison.ControlDetails.Target.NodeType != XmlNodeType.Text ||
                comparison.TestDetails.Target.NodeType != XmlNodeType.Text)
            {
                return outcome;
            }
            DateTime controlDate, testDate;
            try
            {
                controlDate = DateUtil.FormatDate(comparison.ControlDetails.Value);
                testDate = DateUtil.FormatDate(comparison.TestDetails.Value);
            }
            catch
            {
                // not a timestamp
                return outcome;
            }

            if (controlDate.Equals(testDate))
            {
                return ComparisonResult.SIMILAR; // will evaluate this difference as similar
            }

            return outcome;
        }
    }
}
