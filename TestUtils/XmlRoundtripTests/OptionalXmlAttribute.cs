// -----------------------------------------------------------------------
// <copyright file="OptionalXmlAttribute.cs" company="EUROSTAT">
//   Date Created : 2022-02-27
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Sdmx.TestUtils.XmlRoundtripTests
{
    using System;

    /// <summary>
    /// The class to represent an xml optional attribute
    /// </summary>
    public class OptionalXmlAttribute
    {
        /// <summary>
        /// The xml attribute name
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// The attribute default value (if any)
        /// </summary>
        public string DefaultValue { get; }

        /// <summary>
        /// xml attributes are optional by default.
        /// Set this to <c>true</c> for the default cases
        /// and <c>false</c> when it is explicitly declared as optional.
        /// </summary>
        public bool IsImplicit { get; }

        /// <summary>
        /// Marks the attribute to be excepted from default comparison logic.
        /// </summary>
        public bool IsExceptional { get; }

        public OptionalXmlAttribute(string name, string defaultValue, bool isImplicit, bool isExceptional)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException($"'{nameof(name)}' cannot be null or empty.", nameof(name));
            }

            Name = name;
            DefaultValue = defaultValue;
            IsImplicit = isImplicit;
            IsExceptional = isExceptional;
        }
    }
}
