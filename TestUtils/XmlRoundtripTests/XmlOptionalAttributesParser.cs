// -----------------------------------------------------------------------
// <copyright file="XmlOptionalAttributesParser.cs" company="EUROSTAT">
//   Date Created : 2022-03-03
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Sdmx.TestUtils.XmlRoundtripTests
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;

    /// <summary>
    /// Parses the xml schemas provided and creates a collection of optional attributes that are then publicly available. 
    /// </summary>
    public class XmlOptionalAttributesParser
    {
        /// <summary>
        /// The optional attributes retrieved from the provided xsd schema(s) (if any).
        /// </summary>
        private readonly IList<OptionalXmlAttribute> _optionalAttributes
            = new List<OptionalXmlAttribute>();

        public XmlOptionalAttributesParser(IDictionary<string, Stream> schemas)
        {
            if (schemas is null || !schemas.Any())
            {
                throw new ArgumentNullException(nameof(schemas));
            }

            var xmlReaderSettings = new XmlReaderSettings
            {
                IgnoreComments = true,
                IgnoreProcessingInstructions = true,
                IgnoreWhitespace = true
            };

            foreach (var schema in schemas)
            {
                string xsdFile = schema.Key;
                Stream xsdStream = schema.Value;
                xsdStream.Position = 0;
                ParseSingleOptionalXmlAttributes(xsdStream, xmlReaderSettings, xsdFile);
                xsdStream.Position = 0;
            }
        }

        /// <summary>
        /// Parses a single xsd file for optional attributes.
        /// </summary>
        /// <param name="xsdFile">the xsd file to parse.</param>
        /// <param name="xmlReaderSettings">the xml reader settings.</param>
        /// <param name="schemaName">The name of the xsd file.</param>
        private void ParseSingleOptionalXmlAttributes(Stream xsdFile, XmlReaderSettings xmlReaderSettings, string schemaName)
        {
            var reader = XmlReader.Create(xsdFile, xmlReaderSettings);
            string attributeName = string.Empty, defaultValue = string.Empty;
            bool isOptional = false, isImplicit = false;

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if (reader.LocalName.Equals("attribute", StringComparison.OrdinalIgnoreCase))
                        {
                            isOptional = false;
                            isImplicit = false;
                            attributeName = reader.GetAttribute("name");
                            if (attributeName == null)
                            {
                                continue;
                            }
                            defaultValue = reader.GetAttribute("default");
                            string useValue = reader.GetAttribute("use");
                            if (useValue == null)
                            {
                                isImplicit = true;
                            }
                            else
                            {
                                isOptional = useValue.Equals("optional", StringComparison.OrdinalIgnoreCase);
                            }
                        }
                        if (isOptional || isImplicit)
                        {
                            bool isExceptional = attributeName.Equals("id") || attributeName.Equals("urn");
                            _optionalAttributes.Add(new OptionalXmlAttribute(
                                attributeName,
                                defaultValue,
                                isImplicit,
                                isExceptional));
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// Retrieves all optional attributes found.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<OptionalXmlAttribute> GetAllOptionalAttributes()
        {
            return new ReadOnlyCollection<OptionalXmlAttribute>(_optionalAttributes);
        }
    }
}
