// -----------------------------------------------------------------------
// <copyright file="XmlCompareEngine.cs" company="EUROSTAT">
//   Date Created : 2022-02-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Sdmx.TestUtils.XmlRoundtripTests
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using Org.XmlUnit.Builder;
    using Org.XmlUnit.Diff;
    using Org.XmlUnit.Input;
    using Org.XmlUnit.Validation;

    /// <summary>
    /// Can compare two xml files for equility.
    /// - Timestamps are compared using the <see cref="DateUtil.FormatDate(object)"/> method.
    /// - Optional attributes from the schemas that appear in only one of the files are ignored.
    /// - The order of structure elements is ignored.
    /// - other minor differences are ignored.
    /// Can validate an xml file against xsd schema(s).
    /// </summary>
    public class XmlCompareEngine
    {
        private readonly Validator _schemaValidator;

        private readonly INodeMatcher _nodeMatcher;

        private readonly IDictionary<string, Stream> _schemas;

        /// <summary>
        /// Parses the schemas and provides the optional attributes.
        /// </summary>
        private readonly XmlOptionalAttributesParser _optionalAttributesParser;

        /// <summary>
        /// The method to use for filtering out the attributes for comparison.
        /// Should return <c>false</c> for those attributes that should be excluded from comparison.
        /// </summary>
        private readonly Predicate<XmlAttribute> _attributeFilter;

        /// <summary>
        /// The method to use for filtering out the nodes for comparison.
        /// Should return <c>false</c> for those nodes that should be excluded from comparison.
        /// </summary>
        private Predicate<XmlNode> _nodeFilter;

        /// <summary>
        /// Each evaluator is a rule on how to evaluate certain differemces.
        /// </summary>
        private readonly IList<DifferenceEvaluator> _evaluators
            = new List<DifferenceEvaluator>();

        /// <summary>
        /// Builder class to build and configure an <see cref="XmlCompareEngine"/>.
        /// </summary>
        public class Builder
        {
            IDictionary<string, Stream> _schemas;

            /// <summary>
            /// Start the built of a new engine.
            /// </summary>
            /// <returns></returns>
            public static Builder NewEngine()
            {
                return new Builder();
            }

            /// <summary>
            /// Add xsd schemas to use for the comparison and validation.
            /// </summary>
            /// <param name="getFileStream">A delegate to get the file stream for a filename.</param>
            /// <param name="fileNames">The xsd file names.</param>
            /// <returns></returns>
            public Builder WithSchemas(IDictionary<string, Stream> schemas)
            {
                _schemas = schemas;
                return this;
            }

            /// <summary>
            /// Creates the <see cref="XmlCompareEngine"/> instance.
            /// </summary>
            /// <returns></returns>
            public XmlCompareEngine Build()
            {
                return new XmlCompareEngine(_schemas);
            }
        }

        private XmlCompareEngine(IDictionary<string, Stream> schemas)
        {
            if (schemas != null && schemas.Any())
            {
                _schemas = schemas;
                _schemaValidator = Validator.ForLanguage(Languages.W3C_XML_SCHEMA_NS_URI);
                _optionalAttributesParser = new XmlOptionalAttributesParser(_schemas);
                _attributeFilter = new OptionalAttributeFilter(n => _optionalAttributesParser.GetAllOptionalAttributes()).ExcludeOptionalAttributes;
            }
            else
            {
                _attributeFilter = (a) => true;
            }

            _nodeFilter = (n) => true; // intially, no nodes are ignored

            // add standard evaluators
            _evaluators.Add(DifferenceEvaluators.Default);
            _evaluators.Add(SDMXDifferenceEvaluator.EvaluateTimestamps);

            // This will match nodes by Name and attributes instead of by order.
            // If this is not successful, will match by order.
            _nodeMatcher = new DefaultNodeMatcher(
                ElementSelectors.ConditionalBuilder()
                .When(e => e.Name.StartsWith("str:"))
                .ThenUse(ElementSelectors.ByNameAndAllAttributes)
                .Build(),
                ElementSelectors.Default);
        }

        /// <summary>
        /// Use the engine with ignoring the givn nodes.
        /// </summary>
        /// <param name="nodeNames"></param>
        /// <returns></returns>
        public XmlCompareEngine IgnoreNodes(params string[] nodeNames)
        {
            _nodeFilter = (n) =>
                !nodeNames.Any(s => n.Name.EndsWith(s));
                // node names are in the form "prefix:NodeName"
            return this;
        }

        /// <summary>
        /// Tests two xml files for equality.
        /// </summary>
        /// <param name="controlXMLfilepath">The file to test against (as path).</param>
        /// <param name="testXMLfilepath">The file to test (as path).</param>
        /// <returns>A <see cref="RoundtripComparisonResult"/></returns>
        public RoundtripComparisonResult AreEqual(string controlXMLfilepath, string testXMLfilepath)
        {
            return AreEqual(
                DiffBuilder.Compare(Input.FromFile(controlXMLfilepath)).WithTest(Input.FromFile(testXMLfilepath)));
        }

        /// <summary>
        /// Tests two xml files for equality.
        /// </summary>
        /// <param name="controlXmlStream">The file to test against (as stream).</param>
        /// <param name="testXmlStream">The file to test (as stream).</param>
        /// <returns></returns>
        public RoundtripComparisonResult AreEqual(Stream controlXmlStream, Stream testXmlStream)
        {
            return AreEqual(
                DiffBuilder.Compare(Input.FromStream(controlXmlStream)).WithTest(Input.FromStream(testXmlStream)));
        }

        /// <summary>
        /// Compares an xml file for validity against the provided xsd schema(s).
        /// </summary>
        /// <param name="xmlFile">The xml file to test for validity.</param>
        /// <returns><c>true</c> if valid, <c>false</c> otherwise.</returns>
        public bool IsValid(string xmlFile)
        {
            PrepareSchemaValidator(_schemas.Values);
            ValidationResult result = _schemaValidator.ValidateInstance(new StreamSource(xmlFile));
            return result.Valid;
        }

        private RoundtripComparisonResult AreEqual(DiffBuilder builder)
        {
            var xmlDiff = builder
                .WithDifferenceEvaluator(DifferenceEvaluators.Chain(_evaluators.ToArray()))
                .WithAttributeFilter(_attributeFilter)
                .WithComparisonController(ComparisonControllers.StopWhenDifferent)
                .WithNodeMatcher(_nodeMatcher)
                .WithNodeFilter(_nodeFilter)
                .CheckForSimilar()
                .Build();

            return new RoundtripComparisonResult(!xmlDiff.HasDifferences(), xmlDiff.FullDescription());
        }

        /// <summary>
        /// Re-creates xml readers for the schema files.
        /// </summary>
        /// <param name="xsdSchemas">The xsd schema(s) the xml files are built on.</param>
        private void PrepareSchemaValidator(ICollection<Stream> xsdSchemas)
        {
            if (xsdSchemas == null || !xsdSchemas.Any())
            {
                throw new ArgumentException($"{nameof(xsdSchemas)} doesn't contain any schemas.");
            }

            _schemaValidator.SchemaSources = new StreamSource[xsdSchemas.Count];
            for (int i = 0; i < xsdSchemas.Count; i++)
            {
                var xsdStream = xsdSchemas.ElementAt(i);
                xsdStream.Position = 0;
                var xsdSource = new StreamSource(xsdStream);
                _schemaValidator.SchemaSources.SetValue(xsdSource, i);
            }
        }
    }

    /// <summary>
    /// Contains the results information of a roundtrip comparison.
    /// </summary>
    public class RoundtripComparisonResult
    {
        private readonly bool _isEqual;
        private readonly string _resultstring;

        public RoundtripComparisonResult(bool isEqual, string resultstring)
        {
            this._isEqual = isEqual;
            this._resultstring = resultstring;
        }

        public bool IsEqual => _isEqual;

        public string GetResultString()
        {
            return _resultstring;
        }
    }
}
