// -----------------------------------------------------------------------
// <copyright file="SdmxXmlCompareFactory.cs" company="EUROSTAT">
//   Date Created : 2022-03-03
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Sdmx.TestUtils.XmlRoundtripTests
{
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.XmlHelper;

    public class SdmxXmlCompareFactory
    {
        private readonly IDictionary<SdmxSchemaEnumType, XmlCompareEngine> _engines
            = new Dictionary<SdmxSchemaEnumType, XmlCompareEngine>();

        /// <summary>
        /// Retrieve the xml comparison engine for the given sdmx schema version.
        /// </summary>
        /// <param name="sdmxSchema">The sdmx schema version</param>
        /// <returns>The engine</returns>
        public XmlCompareEngine GetEngine(SdmxSchemaEnumType sdmxSchema)
        {
            XmlCompareEngine engine;
            if (_engines.ContainsKey(sdmxSchema))
            {
                engine = _engines[sdmxSchema];
                if (engine == null)
                {
                    engine = BuildEngine(sdmxSchema);
                    _engines.Add(sdmxSchema, engine);
                }
            }
            else
            {
                engine = BuildEngine(sdmxSchema);
                _engines.Add(sdmxSchema, engine);
            }
            return engine;
        }

        private XmlCompareEngine BuildEngine(SdmxSchemaEnumType sdmxSchema)
        {
            var schemas = EmbeddedResourceHelper.GetXSDResourcesDictionary(sdmxSchema);
            var engine = XmlCompareEngine.Builder.NewEngine()
                .WithSchemas(schemas)
                .Build();

            return engine;
        }
    }
}
