// -----------------------------------------------------------------------
// <copyright file="SdmxObjectRetrievalManagerWrapper.cs" company="EUROSTAT">
//   Date Created : 2014-04-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureRetrieval.
//     SdmxStructureRetrieval is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureRetrieval is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureRetrieval.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.Api.Model.Query;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex;
using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;
using Org.Sdmxsource.Sdmx.Util.Sdmx;
using Org.Sdmxsource.Util;

namespace Org.Sdmxsource.Sdmx.StructureRetrieval.Manager
{
    /// <summary>
    /// A wrapper implementation of <see cref="ISdmxObjectRetrievalManager"/> that uses internally a <see cref="ICommonSdmxObjectRetrievalManager"/>
    /// needed for backwards compatibility for users of <see cref="ISdmxObjectRetrievalManager"/>
    /// </summary>
    public class SdmxObjectRetrievalManagerWrapper : BaseSdmxObjectRetrievalManager, ISdmxObjectRetrievalManager 
    {
        private readonly ICommonSdmxObjectRetrievalManager _decorated;

        /// <summary>
        /// Initializes a new instance of the class
        /// </summary>
        /// <param name="decorated">The actual retrieval manager for retrieving the artefacts.</param>
        /// <exception cref="ArgumentNullException"></exception>
        public SdmxObjectRetrievalManagerWrapper(ICommonSdmxObjectRetrievalManager decorated)
        {
            if (decorated is null)
            {
                throw new ArgumentNullException(nameof(decorated));
            }

            _decorated = decorated;
        }

        /// <summary>
        /// Initializes a new instance of the class
        /// </summary>
        /// <param name="factory">The factory for the actual retrieval manager for retrieving the artefacts.</param>
        /// <exception cref="ArgumentNullException"></exception>
        public SdmxObjectRetrievalManagerWrapper(ICommonSdmxObjectRetrievalFactory factory, ConnectionStringSettings connectionStringSettings)
        {
            if (factory is null)
            {
                throw new ArgumentNullException(nameof(factory));
            }

            _decorated = factory.GetManager(connectionStringSettings);
        }
        #region interface implementation

        /// <inheritdoc/>
        public override ISet<T> GetMaintainableObjects<T>(
            IMaintainableRefObject maintainableReference, 
            bool returnStub, 
            bool returnLatest)
        {
            if (maintainableReference is null)
            {
                throw new ArgumentNullException(nameof(maintainableReference));
            }

            SdmxStructureType type = SdmxStructureType.ParseClass(typeof(T));
            CommonStructureQueryCore.Builder builder = CommonBuilder(maintainableReference, returnLatest, type);
            builder.SetRequestedDetail(returnStub ? ComplexStructureQueryDetailEnumType.Stub : ComplexStructureQueryDetailEnumType.Full);
            builder.SetReferencedDetail(returnStub ? ComplexMaintainableQueryDetailEnumType.Stub : ComplexMaintainableQueryDetailEnumType.Full);
            ICommonStructureQuery structureQuery = builder.Build();
            var maintainable = _decorated.GetMaintainables(structureQuery);
            return new HashSet<T>(maintainable.GetMaintainables(type).Cast<T>());
        }

        /// <inheritdoc/>
        public override ISet<T> GetMaintainableObjects<T>(IMaintainableRefObject maintainableReference, IObjectQueryMetadata metadata) 
        {
             if (maintainableReference is null)
            {
                throw new ArgumentNullException(nameof(maintainableReference));
            }

            if (metadata is null)
            {
                throw new ArgumentNullException(nameof(metadata));
            }

            SdmxStructureType type = SdmxStructureType.ParseClass(typeof(T));
            CommonStructureQueryCore.Builder builder = CommonBuilder(maintainableReference, metadata.IsReturnLatest, type, metadata.IsReturnStable);
            builder.SetRequestedDetail(metadata.IsReturnStub ? ComplexStructureQueryDetailEnumType.Stub : ComplexStructureQueryDetailEnumType.Full);
            builder.SetReferencedDetail(metadata.IsReturnStub ? ComplexMaintainableQueryDetailEnumType.Stub : ComplexMaintainableQueryDetailEnumType.Full);
            ICommonStructureQuery structureQuery = builder.Build();
            var maintainable = _decorated.GetMaintainables(structureQuery);
            return new HashSet<T>(maintainable.GetMaintainables(type).Cast<T>());
        }

        /// <inheritdoc/>
        public override ISdmxObjects GetSdmxObjects(
            IStructureReference structureReference, 
            ResolveCrossReferences resolveCrossReferences)
        {
            if (structureReference is null)
            {
                throw new ArgumentNullException(nameof(structureReference));
            }

            SdmxStructureType structureType = structureReference.MaintainableStructureEnumType;
            CommonStructureQueryCore.Builder builder = CommonBuilder(structureReference, false, structureType, false);

            switch (resolveCrossReferences)
            {
                case ResolveCrossReferences.DoNotResolve:
                case ResolveCrossReferences.ResolveExcludeAgencies:
                    builder.SetReferences(StructureReferenceDetailEnumType.Children);
                    break;
                default:
                    builder.SetReferences(StructureReferenceDetailEnumType.None);
                    break;
            }

            ICommonStructureQuery structureQuery = builder.Build();
            var maintainable = _decorated.GetMaintainables(structureQuery);
            if (resolveCrossReferences == ResolveCrossReferences.ResolveExcludeAgencies) 
            {
                return new SdmxObjectsImpl(maintainable.GetAllMaintainables(SdmxStructureEnumType.AgencyScheme).ToArray());
            }

            return maintainable;
        }

        /// <inheritdoc/>
        public override ISdmxObjects GetMaintainables(IRestStructureQuery restquery)
        {
            if (restquery is null)
            {
                throw new ArgumentNullException(nameof(restquery));
            }

            var query = BuildCommonStructureQuery(restquery);
            return _decorated.GetMaintainables(query);
        }

        #endregion

        #region query builders

        // The ISdmxObjectRetrievalManager interface is still used externally, so need to keep converters to ICommonStructureQuery

        private CommonStructureQueryCore.Builder CommonBuilder(IMaintainableRefObject maintainableReference, bool returnLatest, SdmxStructureType type, bool returnStable = false)
        {
            var builder = CommonStructureQueryCore.Builder.NewQuery(CommonStructureQueryType.Other, StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument));
            builder.SetMaintainableTarget(type);
            if (maintainableReference.HasAgencyId())
            {
                builder.SetAgencyIds(maintainableReference.AgencyId.Split(','));
            }
            if (maintainableReference.HasMaintainableId())
            {
                builder.SetMaintainableIds(maintainableReference.MaintainableId.Split(','));
            }

            builder.SetVersionRequests(VersionRequestCore.FromOldApi(maintainableReference.Version, returnLatest, returnStable).ToArray());
            return builder;
        }

        private ICommonStructureQuery BuildCommonStructureQuery(IRestStructureQuery restStructureQuery)
        {
            IStructureReference structureReference = restStructureQuery.StructureReference;

            var builder = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.REST, null)
                .SetRequestedDetail(restStructureQuery.StructureQueryMetadata.StructureQueryDetail.GetDetailForRequestedArtefacts())
                .SetReferencedDetail(restStructureQuery.StructureQueryMetadata.StructureQueryDetail.GetDetailForReferencedArtefacts())
                .SetReferences(restStructureQuery.StructureQueryMetadata.StructureReferenceDetail)
                .SetMaintainableTarget(structureReference.MaintainableStructureEnumType)
                .SetItemCascade(CascadeSelection.False);
            
            if (ObjectUtil.ValidCollection(restStructureQuery.SpecificItems))
            {
                SdmxStructureType itemType = SdmxStructureTypeUtils.GetItemStructureType(restStructureQuery.StructureType);
                builder.SetChildReferences(restStructureQuery.SpecificItems.Select(x => ComplexIdentifiableReferenceCore.CreateForRest(x, itemType)).ToArray());
            }
            if (restStructureQuery.StructureQueryMetadata.SpecificStructureReference != null)
            {
                builder.SetSpecificReferences(restStructureQuery.StructureQueryMetadata.SpecificStructureReference);
            }
            if (structureReference.HasVersion())
            {
                builder.SetVersionRequests(structureReference.Version.Split(',').Select(s => new VersionRequestCore(s)).ToArray<IVersionRequest>());
            }
            else
            {
                builder.SetVersionRequests(new VersionRequestCore(structureReference.Version));
            }
            if (structureReference.HasMaintainableId())
            {
                builder.SetMaintainableIds(structureReference.MaintainableId.Split(','));
            }
            if (structureReference.HasAgencyId())
            {
                builder.SetAgencyIds(structureReference.AgencyId.Split(','));
            }

            return builder.Build();
        }

        #endregion
    }
}