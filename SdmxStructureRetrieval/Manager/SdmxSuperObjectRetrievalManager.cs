﻿// -----------------------------------------------------------------------
// <copyright file="SdmxSuperObjectRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureRetrieval.
//     SdmxStructureRetrieval is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureRetrieval is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureRetrieval.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.StructureRetrieval.Manager
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Registry;
    using Org.Sdmxsource.Sdmx.Util.Objects;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// Sdmx Super Object Retrieval Manager
    /// </summary>
    /// <seealso cref="Org.Sdmxsource.Sdmx.StructureRetrieval.Manager.BaseSdmxObjectRetrievalManager" />
    public class SdmxSuperObjectRetrievalManager : ISdmxSuperObjectRetrievalManager
    {
        /// <summary>
        /// The sdmx object retrieval manager
        /// </summary>
        private readonly ISdmxObjectRetrievalManager _sdmxObjectRetrievalManager;

        /// <summary>
        /// The super object builder
        /// </summary>
        private readonly ISuperObjectsBuilder _superObjectsBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="SdmxSuperObjectRetrievalManager" /> class.
        /// </summary>
        /// <param name="sdmxObjectRetrievalManager">The sdmx object retrieval manager.</param>
        /// <param name="superObjectsBuilder">The super object builder.</param>
        public SdmxSuperObjectRetrievalManager(ISdmxObjectRetrievalManager sdmxObjectRetrievalManager, ISuperObjectsBuilder superObjectsBuilder)
        {
            this._sdmxObjectRetrievalManager = sdmxObjectRetrievalManager;
            this._superObjectsBuilder = superObjectsBuilder;
        }

        /// <summary>
        /// Gets the category scheme super object.
        /// </summary>
        /// <param name="maintainableRef">The maintainableRef.</param>
        /// <returns>The <see cref="ICategorisationSuperObject" />.</returns>
        public ICategorySchemeSuperObject GetCategorySchemeSuperObject(IMaintainableRefObject maintainableRef)
        {
            var superObjects = this.GetCategorySchemeSuperObjects(maintainableRef);

            if (!ObjectUtil.ValidCollection(superObjects))
            {
                return null;
            }

            return (ICategorySchemeSuperObject)SuperObjectRefUtil<ICategorySchemeSuperObject>.ResolveReference(superObjects, maintainableRef);
        }

        /// <summary>
        /// Gets the category scheme super objects.
        /// </summary>
        /// <param name="x">The maintainable Reference.</param>
        /// <returns>The <see cref="ISet{ICategorySchemeSuperObject}" />.</returns>
        public ISet<ICategorySchemeSuperObject> GetCategorySchemeSuperObjects(IMaintainableRefObject x)
        {
            return this.GetSuperObjects(new StructureReferenceImpl(x, SdmxStructureEnumType.CategoryScheme)).CategorySchemes;
        }

        /// <summary>
        /// Gets the codelist super object.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <returns>The <see cref="ICodelistSuperObject" />.</returns>
        public ICodelistSuperObject GetCodelistSuperObject(IMaintainableRefObject maintainableRef)
        {
            var superObjects = this.GetCodelistSuperObjects(maintainableRef);

            if (!ObjectUtil.ValidCollection(superObjects))
            {
                return null;
            }

            return (ICodelistSuperObject)SuperObjectRefUtil<ICodelistSuperObject>.ResolveReference(superObjects, maintainableRef);
        }

        /// <summary>
        /// Gets the codelist super objects.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <returns>The <see cref="ISet{ICodelistSuperObject}" />.</returns>
        public ISet<ICodelistSuperObject> GetCodelistSuperObjects(IMaintainableRefObject maintainableRef)
        {
            return this.GetSuperObjects(new StructureReferenceImpl(maintainableRef, SdmxStructureEnumType.CodeList)).Codelists;
        }

        /// <summary>
        /// Gets the concept scheme super object.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <returns>The <see cref="IConceptSchemeSuperObject" />.</returns>
        public IConceptSchemeSuperObject GetConceptSchemeSuperObject(IMaintainableRefObject maintainableRef)
        {
            var superObjects = this.GetConceptSchemeSuperObjects(maintainableRef);

            if (!ObjectUtil.ValidCollection(superObjects))
            {
                return null;
            }

            return (IConceptSchemeSuperObject)SuperObjectRefUtil<IConceptSchemeSuperObject>.ResolveReference(superObjects, maintainableRef);
        }

        /// <summary>
        /// Gets the concept scheme super objects.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <returns>The <see cref="ISet{IConceptSchemeSuperObject}" />.</returns>
        public ISet<IConceptSchemeSuperObject> GetConceptSchemeSuperObjects(IMaintainableRefObject maintainableRef)
        {
            return this.GetSuperObjects(new StructureReferenceImpl(maintainableRef, SdmxStructureEnumType.ConceptScheme)).ConceptSchemes;
        }

        /// <summary>
        /// Gets the dataflow super object.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <returns>The <see cref="IDataflowSuperObject" />.</returns>
        public IDataflowSuperObject GetDataflowSuperObject(IMaintainableRefObject maintainableRef)
        {
            var superObjects = this.GetDataflowSuperObjects(maintainableRef);

            if (!ObjectUtil.ValidCollection(superObjects))
            {
                return null;
            }

            return (IDataflowSuperObject)SuperObjectRefUtil<IDataflowSuperObject>.ResolveReference(superObjects, maintainableRef);
        }

        /// <summary>
        /// Gets the dataflow super objects.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <returns>The <see cref="ISet{IDataflowSuperObject}" />.</returns>
        public ISet<IDataflowSuperObject> GetDataflowSuperObjects(IMaintainableRefObject maintainableRef)
        {
            return this.GetSuperObjects(new StructureReferenceImpl(maintainableRef, SdmxStructureEnumType.Dataflow)).Dataflows;
        }

        /// <summary>
        /// Gets the data structure super object.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <returns>The <see cref="IDataStructureSuperObject" />.</returns>
        public IDataStructureSuperObject GetDataStructureSuperObject(IMaintainableRefObject maintainableRef)
        {
            var superObjects = this.GetDataStructureSuperObjects(maintainableRef);

            if (!ObjectUtil.ValidCollection(superObjects))
            {
                return null;
            }

            return (IDataStructureSuperObject)SuperObjectRefUtil<IDataStructureSuperObject>.ResolveReference(superObjects, maintainableRef);
        }

        /// <summary>
        /// Gets the data structure super objects.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <returns>The <see cref="ISet{IDataStructureSuperObject}" />.</returns>
        public ISet<IDataStructureSuperObject> GetDataStructureSuperObjects(IMaintainableRefObject maintainableRef)
        {
            return this.GetSuperObjects(new StructureReferenceImpl(maintainableRef, SdmxStructureEnumType.Dsd)).DataStructures;
        }

        /// <summary>
        /// Gets the hierarchic code list super object.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <returns>The <see cref="IHierarchicalCodelistSuperObject" />.</returns>
        public IHierarchicalCodelistSuperObject GetHierarchicCodeListSuperObject(IMaintainableRefObject maintainableRef)
        {
            var superObjects = this.GetHierarchicCodeListSuperObjects(maintainableRef);

            if (!ObjectUtil.ValidCollection(superObjects))
            {
                return null;
            }

            return (IHierarchicalCodelistSuperObject)SuperObjectRefUtil<IHierarchicalCodelistSuperObject>.ResolveReference(superObjects, maintainableRef);
        }

        /// <summary>
        /// Gets the hierarchic code list super objects.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <returns>The <see cref="ISet{IHierarchicalCodelistSuperObject}" />.</returns>
        public ISet<IHierarchicalCodelistSuperObject> GetHierarchicCodeListSuperObjects(IMaintainableRefObject maintainableRef)
        {
            return this.GetSuperObjects(new StructureReferenceImpl(maintainableRef, SdmxStructureEnumType.HierarchicalCode)).HierarchicalCodelists;
        }

        /// <summary>
        /// Gets the provision agreement super object.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <returns>The <see cref="IProvisionAgreementSuperObject" />.</returns>
        public IProvisionAgreementSuperObject GetProvisionAgreementSuperObject(IMaintainableRefObject maintainableRef)
        {
            var superObjects = this.GetProvisionAgreementSuperObjects(maintainableRef);

            if (!ObjectUtil.ValidCollection(superObjects))
            {
                return null;
            }

            return (IProvisionAgreementSuperObject)SuperObjectRefUtil<IProvisionAgreementSuperObject>.ResolveReference(superObjects, maintainableRef);
        }

        /// <summary>
        /// Gets the provision agreement super objects.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <returns>The <see cref="ISet{IProvisionAgreementSuperObject}" />.</returns>
        public ISet<IProvisionAgreementSuperObject> GetProvisionAgreementSuperObjects(IMaintainableRefObject maintainableRef)
        {
            return this.GetSuperObjects(new StructureReferenceImpl(maintainableRef, SdmxStructureEnumType.ProvisionAgreement)).Provisions;
        }

        /// <summary>
        /// Gets the super objects.
        /// </summary>
        /// <param name="structureReference">The s reference.</param>
        /// <returns>The <see cref="ISuperObjects" />.</returns>
        /// <exception cref="System.InvalidOperationException">SdmxObjectBuilder not set at the constructor</exception>
        private ISuperObjects GetSuperObjects(IStructureReference structureReference)
        {
            if (this._superObjectsBuilder == null)
            {
                throw new InvalidOperationException("SdmxObjectBuilder not set at the constructor");
            }

            var sdmxObjects = this._sdmxObjectRetrievalManager.GetSdmxObjects(structureReference, ResolveCrossReferences.ResolveExcludeAgencies);

            return this._superObjectsBuilder.Build(sdmxObjects);
        }
    }
}