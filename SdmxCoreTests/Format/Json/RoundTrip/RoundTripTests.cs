using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Io.Sdmx.Format.Json.Engine.Structure.Writer.Sdmx.V2;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Util;
using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;
using Org.Sdmxsource.Util.Io;
using SdmxCore.Io.Sdmx.Parsers;
using SdmxCoreTests.Format.Json.Engine.Structure.Reader.Generic;

namespace SdmxCoreTests.Format.Json.RoundTrip
{
    
    internal class RoundTripTests 
    {

        [Test]
        public void UseJSONFileToReadThenWriteAndCompare()
        {
            var supportedTypes = new List<SdmxStructureEnumType>() { 
                SdmxStructureEnumType.Dataflow,
                SdmxStructureEnumType.Dsd,
                SdmxStructureEnumType.CodeList,
                SdmxStructureEnumType.Categorisation,
                SdmxStructureEnumType.ConceptScheme,
                SdmxStructureEnumType.CategoryScheme
            };

            var ignoreFiles = new List<string>()
            {
                "TestAttributesOnly",
            };
            foreach (var item in supportedTypes)
            {
                var directory = "Resources/Engine/Structure/" + "SdmxJsonV2" + "/" + SdmxStructureType.GetFromEnum(item).UrnClass + "/";
                foreach(var file in Directory.GetFiles(directory))
                {
                    if (ignoreFiles.Exists(x=>file.Contains(x)))
                    {
                        continue;
                    }
                    try
                    {
                        IReadableDataLocation rdl = new ReadableDataLocationTmp(new FileInfo(file));
                        ISdmxObjects sdmxBeans = SdmxJsonStructureReaderManagerV2.Instance.ReadJson(rdl, new InMemoryRetrievalManager());
                        var actualObjects = sdmxBeans.GetMaintainables(item);

                        using var bos = new MemoryStream();
                        if (actualObjects.Count == 1)
                        {
                            SdmxJsonStructureWriterEngineV2.Instance.WriteStructure(actualObjects.First(), null, bos);
                        }
                        else
                        {
                            SdmxJsonStructureWriterEngineV2.Instance.WriteStructures(sdmxBeans, null, bos);
                        }
                        string actual = System.Text.Encoding.UTF8.GetString(bos.ToArray());
                        string expected = File.ReadAllText(file);
                        AssertJsons(actual, expected);
                        Console.WriteLine("Checked file : " + file);
                    }
                    catch(Exception ex)
                    {
                        Console.WriteLine("Exception for file : " + file);
                        Console.WriteLine(ex.ToString());
                        continue;
                    }
                   
                }
            }
            
        }

        protected void AssertJsons(string actual, string expected,
            params string[] ignoreTokens)
        {
            ignoreTokens = ignoreTokens.Union(new List<string>()
            {
                "meta.prepared",
                "meta.id",
                "meta.contentLanguages"
            }).ToArray();

            JToken expectedToken = JToken.Parse(expected);
            JToken actualToken = JToken.Parse(actual);

            // Remove the properties to ignore from the actualToken and expectedToken
            foreach (string removeToken in ignoreTokens)
            {
                actualToken.SelectToken(removeToken)?.Parent?.Remove();
                expectedToken.SelectToken(removeToken)?.Parent?.Remove();
            }

            // Assert that the modified actualToken is deep equal to the modified expectedToken
            Assert.True(JToken.DeepEquals(actualToken, expectedToken));
        }

    }
}
