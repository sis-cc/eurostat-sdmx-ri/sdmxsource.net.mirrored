// -----------------------------------------------------------------------
// <copyright file="AbstractWriterTestsExtension.cs" company="EUROSTAT">
//   Date Created : 2023-05-24
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.Format.Json.Engine.Structure.Writer.Sdmx.V2
{
    using SdmxCoreTests.Format.Json.Engine.Structure.Writer.Generic;
    using Io.Sdmx.Core.Sdmx.Api.Engine.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using NUnit.Framework;
    using Newtonsoft.Json.Linq;

    public abstract class AbstractWriterTestsExtension : AbstractWriterTests
    {
        protected bool isDebug = false;
        private readonly IStructureWriterEngine _writer;

        public AbstractWriterTestsExtension(SdmxStructureEnumType structureType, string format, IStructureWriterEngine writer)
            : base(structureType, format, writer)
        {
            _writer = writer;
        }

        /// <summary> 
        /// Writes the Maintainable(s) and asserts the contents is the same as the test file, whose name is the same 
        /// as the test name 
        /// </summary>
        /// <param name=" maint"> 
        /// </param> 
        protected void AssertWriter(params IMaintainableObject[] maint)
        {
            FileInfo file = GetTestFile();
            using var bos = new MemoryStream();

            ISdmxObjects objects = new SdmxObjectsImpl(maint);
            SetExpectedLocales(objects);
            _writer.WriteStructures(objects, null, bos);

            string actual = System.Text.Encoding.UTF8.GetString(bos.ToArray());
            if (isDebug)
            {
                Console.WriteLine(actual);
                Assert.Fail("set to fail in debug mode");
            }
            else
            {
                string expected = File.ReadAllText(file.FullName);
                AssertJsons(actual, expected);
            }
        }

        private void SetExpectedLocales(ISdmxObjects IsdmxObjects)
        {
            IsdmxObjects.SetSelectedLocales(GetExpectedLocales());
        }

        //if setting selected locale is needed for tests we can override it with expected locales
        protected virtual ISet<string> GetExpectedLocales()
        {
            return null;
        }
    }
}