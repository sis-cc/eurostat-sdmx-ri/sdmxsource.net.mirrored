// -----------------------------------------------------------------------
// <copyright file="TestSemanticVersion.cs" company="EUROSTAT">
//   Date Created : 2023-05-24
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.Format.Json.Engine.Structure.Writer.V2
{
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using SdmxCoreTests.TestUtils;
    using SdmxCoreTests.Format.Json.Engine.Structure.Writer.Sdmx.V2;
    using Io.Sdmx.Format.Json.Engine.Structure.Writer.Sdmx.V2;
    using NUnit.Framework;

    public class TestSemanticVersion : AbstractWriterTestsExtension
    {
        public TestSemanticVersion()
            : base(SdmxStructureEnumType.CategoryScheme, "SdmxJsonV2", SdmxJsonStructureWriterEngineV2.Instance)
        {
        }

        /// <summary> 
        /// This test is based on AbstractTestWritingCategoryScheme.testGenerateFlatScheme and just changes version file in json file 
        /// </summary> 
        [Test]
        public void ExtensionTestNotFinal()
        {
            ICategorySchemeObject ItestObject = TestCategorySchemeUtil.GenerateCategoryScheme(
                "ECB", "FLAT_CS", "1.0.1-testnonfinal", "INTEREST_RATES", "EXCHANGE_RATES");
            Assert.That(ItestObject.IsFinal.IsTrue, Is.False);
            AssertWriter(ItestObject);
        }
    }
}