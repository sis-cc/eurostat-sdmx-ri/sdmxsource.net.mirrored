// -----------------------------------------------------------------------
// <copyright file="AbstractWriterTests.cs" company="EUROSTAT">
//   Date Created : 2023-05-23
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.Format.Json.Engine.Structure.Writer.Generic
{
    using Io.Sdmx.Core.Sdmx.Api.Engine.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Newtonsoft.Json.Linq;
    using SdmxCoreTests.Format.Json.Engine.Structure.Reader.Generic;
    using NUnit.Framework;

    public class AbstractWriterTests : AbstractReadWriteTests
    {
        protected bool isDebug = false;
        private readonly IStructureWriterEngine _writer;

        public AbstractWriterTests(SdmxStructureEnumType structureType, string format, IStructureWriterEngine writer)
            : base(structureType, format)
        {
            _writer = writer;
        }

        /// <summary> 
        /// Writes the Maintainable(s) and asserts the contents is the same as the test file, whose name is the same 
        /// as the test name 
        /// </summary>
        /// <param name=" maint"> 
        /// </param> 
        protected void AssertWriter(params IMaintainableObject[] maint)
        {
            FileInfo file = GetTestFile();
            using var bos = new MemoryStream();
            if (maint.Length == 1)
            {
                _writer.WriteStructure(maint[0], null, bos);
            }
            else
            {
                ISdmxObjects objects = new SdmxObjectsImpl(maint);
                _writer.WriteStructures(objects, null, bos);
            }
            string actual = System.Text.Encoding.UTF8.GetString(bos.ToArray());
            if (isDebug)
            {
                Console.WriteLine(actual);
                Assert.Fail("set to fail in debug mode");
            }
            else
            {
                string expected = File.ReadAllText(file.FullName);
                AssertJsons(actual, expected);
            }
        }

        protected void AssertJsons(string actual, string expected,
            params string[] ignoreTokens)
        {
            ignoreTokens = ignoreTokens.Union(new List<string>()
            {
                "meta.prepared",
                "meta.id"
            }).ToArray();

            JToken expectedToken = JToken.Parse(expected);
            JToken actualToken = JToken.Parse(actual);

            // Remove the properties to ignore from the actualToken and expectedToken
            foreach (string removeToken in ignoreTokens)
            {
                actualToken.SelectToken(removeToken)?.Parent?.Remove();
                expectedToken.SelectToken(removeToken)?.Parent?.Remove();
            }

            // Assert that the modified actualToken is deep equal to the modified expectedToken
            Assert.True(JToken.DeepEquals(actualToken, expectedToken));
        }
    }
}