// -----------------------------------------------------------------------
// <copyright file="AbstractTestWritingCategorisation.cs" company="EUROSTAT">
//   Date Created : 2023-05-23
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.Format.Json.Engine.Structure.Writer.Generic
{
    using Io.Sdmx.Core.Sdmx.Api.Engine.Structure;
    using SdmxCoreTests.TestInstances;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using NUnit.Framework;

    public abstract class AbstractTestWritingCategorisation : AbstractWriterTests
    {
        public AbstractTestWritingCategorisation(string format, IStructureWriterEngine writer) : base(SdmxStructureEnumType.Categorisation, format, writer)
        {
        }

        [Test]
        public void TestMaintainableCategorisation()
        {
            AssertWriter(TestCategorisationInstances.MaintainableCategorisation);
        }

        [Test]
        public void TestCodeCategorisation()
        {
            AssertWriter(TestCategorisationInstances.CodeCategorisation);
        }

        [Test]
        public void TestDimensionCategorisation()
        {
            AssertWriter(TestCategorisationInstances.DimensionCategorisation);
        }

        [Test]
        public void TestMultipleCategorisation()
        {
            AssertWriter(TestCategorisationInstances.DimensionCategorisation, TestCategorisationInstances.CodeCategorisation);
        }
    }
}