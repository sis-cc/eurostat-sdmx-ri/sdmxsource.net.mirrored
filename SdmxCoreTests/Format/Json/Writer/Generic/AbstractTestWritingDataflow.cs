// -----------------------------------------------------------------------
// <copyright file="AbstractTestWritingDataflow.cs" company="EUROSTAT">
//   Date Created : 2023-05-23
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.Format.Json.Engine.Structure.Writer.Generic
{
    using SdmxCoreTests.TestUtils;
    using Io.Sdmx.Core.Sdmx.Api.Engine.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using NUnit.Framework;

    public abstract class AbstractTestWritingDataflow : AbstractWriterTests
    {
        public AbstractTestWritingDataflow(string format, IStructureWriterEngine writer) : base(SdmxStructureEnumType.Dataflow, format, writer)
        {
        }

        [Test]
        public void TestSimpleFlow()
        {
            AssertWriter(TestDataflowUtil.GenerateDataflow(new StructureReferenceImpl("BIS", "DF_EXR", "1.0.1", SdmxStructureEnumType.Dsd)));
        }

        [Test]
        public void TestSubAgencyReference()
        {
            AssertWriter(TestDataflowUtil.GenerateDataflow(new StructureReferenceImpl("BIS.SUB", "DF_EXR", "1.0.1", SdmxStructureEnumType.Dsd)));
        }
    }
}