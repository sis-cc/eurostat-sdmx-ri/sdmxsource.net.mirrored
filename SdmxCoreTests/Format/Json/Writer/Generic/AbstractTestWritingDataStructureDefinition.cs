// -----------------------------------------------------------------------
// <copyright file="AbstractTestWritingDataStructureDefinition.cs" company="EUROSTAT">
//   Date Created : 2023-05-23
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.Format.Json.Engine.Structure.Writer.Generic
{
    using Org.Sdmxsource.Util.Extensions;
    using Io.Sdmx.Core.Sdmx.Api.Engine.Structure;
    using SdmxCoreTests.TestInstances;
    using SdmxCoreTests.TestUtils;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using NUnit.Framework;

    /// <summary> 
    ///  //NOTE SDMXSOURCE_SDMXCORE CHANGE : The tests add all measures as default measure relationships in dsd's attributes. 
    ///  We do this because SdmxSource AttributeObjectImpl.validateAttributeAttributes does not add them like sdmx-core does. 
    /// </summary> 
    public abstract class AbstractTestWritingDataStructureDefinition : AbstractWriterTests
    {
        public AbstractTestWritingDataStructureDefinition(string format, IStructureWriterEngine writer)
            : base(SdmxStructureEnumType.Dsd, format, writer)
        {
        }

        [Test]
        public void TestDSDWithComplexContent()
        {
            AssertWriter(GetDsdWithDefaultMeasuresWhenNone(TestDSDInstances.GetDSDWithComplexContent()));
        }

        [Test]
        public void TestDSDWithMSD()
        {
            AssertWriter(TestDSDInstances.GetDSDWithMSD());
        }

        /* TODO SDMXCORE GEO
        [Test]
        public void TestGeoGridRef() {
            AssertWriter(TestDSDInstances.DSDWithGeoGrid);
        }*/

        [Test]
        public void TestBasic()
        {
            AssertWriter(TestDSDInstances.GenerateMixedBag(false, false));
        }

        [Test]
        public void TestSeriesAttributes()
        {
            AssertWriter(TestDSDInstances.GenerateMixedBag(true, false));
        }

        [Test]
        public void TestMultipleMeasures()
        {
            AssertWriter(GetDsdWithDefaultMeasuresWhenNone(TestDSDInstances.GenerateMixedBag(false, true)));
        }

        /// <summary> 
        /// This is similar to testMultipleMeasures.json but the json file has no measure relationships for attributes with observation attachment level instead of explicitly including all of them. 
        /// Business logic-wise they should be identical because default measure relationships in that case are all measures. But we want IAttributeObject to match the json file instead of automatically add all measure relationships like sdmx core does) 
        /// </summary> 
        [Test]
        public void TestMultipleMeasuresNoDefaultMeasureRelationship()
        {
            AssertWriter(TestDSDInstances.GenerateMixedBag(false, true));
        }

        [Test]
        public void TestMultipleMandatoryMeasures()
        {
            IDataStructureObject dsd = TestDSDInstances.GenerateMixedBag(false, true);
            dsd = TestDSDInstances.SetMandatoryMeasures(dsd, "OBS_VALUE", "BIRTHS");
            dsd = GetDsdWithDefaultMeasuresWhenNone(dsd);
            AssertWriter(dsd);
        }

        [Test]
        public void TestNoMeasuresNoTime()
        {
            AssertWriter(TestDSDUtil.GenerateDSD(false, false, "FREQ", "REF_AREA", "INDICATOR"));
        }

        [Test]
        public void TestNoMeasures()
        {
            AssertWriter(TestDSDUtil.GenerateDSD(true, false, "FREQ", "REF_AREA", "INDICATOR"));
        }

        [Test]
        [Ignore("not implemented")]
        /// <summary> 
        /// DSD with only attributes - no dimensions or measures 
        /// </summary> 
        public void TestAttributesOnly()
        {
            //TODO
        }

        /// <summary> 
        /// A DSD with a Attribute attached at the DataSet level may have measure relationships 
        /// </summary> 
        [Test]
        public void TestMeasureRelationshipOnDataSetAttribute()
        {
            CreateDsdWithMultipleMeasuresAndAttributeOnSpecificLevel(AttributeAttachmentLevel.DataSet);
        }

        /// <summary> 
        /// A DSD with a Attribute attached at the Series level may have measure relationships 
        /// </summary> 
        [Test]
        public void TestMeasureRelationshipOnSeriesAttribute()
        {
            CreateDsdWithMultipleMeasuresAndAttributeOnSpecificLevel(AttributeAttachmentLevel.DimensionGroup);
        }

        /// <summary> 
        /// A DSD with a Attribute attached at the Observation level may have measure relationships 
        /// </summary> 
        [Test]
        public void TestMeasureRelationshipOnObservationAttribute()
        {
            CreateDsdWithMultipleMeasuresAndAttributeOnSpecificLevel(AttributeAttachmentLevel.Observation);
        }

        private void CreateDsdWithMultipleMeasuresAndAttributeOnSpecificLevel(AttributeAttachmentLevel attachmentLevel)
        {
            IDataStructureObject dsd = TestDSDUtil.GenerateDSD(true, true, "FREQ", "REF_AREA");
            IDataStructureMutableObject dsdMutable = dsd.MutableInstance;
            dsdMutable.MeasureList = new MeasureListMutableCore();
            TestDSDUtil.CreateMeasure(dsdMutable, "MEASURE1");
            TestDSDUtil.CreateMeasure(dsdMutable, "MEASURE2");
            TestDSDUtil.CreateMeasure(dsdMutable, "MEASURE3");
            IAttributeMutableObject attributeMutable = TestDSDUtil.CreateAttribute(dsdMutable, "ATTR1", attachmentLevel);
            //NOTE SDMXSOURCE_SDMXCORE CHANGE
            attributeMutable.AddMeasureRelationship("MEASURE1");
            attributeMutable.AddMeasureRelationship("MEASURE3");

            SetDefaultMeasuresWhenNone(attributeMutable, dsdMutable);

            AssertWriter(dsdMutable.ImmutableInstance);
        }

        //NOTE SDMXSOURCE_SDMXCORE CHANGE : following methods added for tests to work with SdmxSource:

        private static void SetDefaultMeasuresWhenNone(IAttributeMutableObject attr, IDataStructureMutableObject dsd)
        {

            if (attr.AttachmentLevel == AttributeAttachmentLevel.Observation)
            {
                if (!ObjectUtil.ValidCollection(attr.DimensionReferences))
                {
                    if (attr.MeasureRelationships.Count == 0)
                    {
                        IList<string> measures = dsd.Measures.Select(m => m.Id).ToList();
                        attr.MeasureRelationships.AddAll(measures);
                    }
                }
            }
        }

        private static IDataStructureObject GetDsdWithDefaultMeasuresWhenNone(IDataStructureObject dsd)
        {

            IDataStructureMutableObject mutableDsd = dsd.MutableInstance;

            mutableDsd.Attributes.ToList().ForEach(attr => SetDefaultMeasuresWhenNone(attr, mutableDsd));
            return mutableDsd.ImmutableInstance;
        }
    }
}