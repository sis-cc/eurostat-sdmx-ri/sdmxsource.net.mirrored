// -----------------------------------------------------------------------
// <copyright file="AbstractTestWritingCodelist.cs" company="EUROSTAT">
//   Date Created : 2023-05-23
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.Format.Json.Engine.Structure.Writer.Generic
{
    using Io.Sdmx.Core.Sdmx.Api.Engine.Structure;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using SdmxCoreTests.TestInstances;

    public abstract class AbstractTestWritingCodelist : AbstractWriterTests
    {
        public AbstractTestWritingCodelist(string format, IStructureWriterEngine writer) 
            : base(SdmxStructureEnumType.CodeList, format, writer)
        {
        }

        [Test]
        public void TestInheritanceRestrictionByInclusion()
        {
            AssertWriter(TestCodelistInstances.GetCodelistWithInheritenceRAW(false));
        }

        [Test]
        public void TestInheritanceRestrictionByExclusion()
        {
            AssertWriter(TestCodelistInstances.GetCodelistWithInheritenceRAW(true));
        }

        [Test]
        public void TestFlatWithDescriptions()
        {
            AssertWriter(TestCodelistInstances.GetActivityWithDescriptions());
        }

        [Test]
        public void TestHierarchy()
        {
            AssertWriter(TestCodelistInstances.GetSexWithHierarchy());
        }

        //TODO SDMXCORE GEO
        /*
        [Test]
        public void TestGeographicSingleFeature() {
            AssertWriter(TestGeographicCodelistInstances.CreateSingleGeoFeature());
        }

        [Test]
        public void TestGeoGridSingleCode() {
            AssertWriter(TestGeoGridCodelistInstances.SingleGeoCode);
        }

        [Test]
        public void TestMultipleMixed() {
            AssertWriter(TestCodelistInstances.ActivityWithDescriptions,
                    TestCodelistInstances.SexWithHierarchy,
                    TestGeographicCodelistInstances.CreateSingleGeoFeature(),
                    TestGeoGridCodelistInstances.SingleGeoCode);
        }
        */
    }
}