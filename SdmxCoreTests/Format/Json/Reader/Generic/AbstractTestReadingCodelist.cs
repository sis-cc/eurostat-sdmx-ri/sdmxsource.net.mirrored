using System;
using SdmxCoreTests.TestInstances;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using SdmxCore.Io.Sdmx.Parsers;

namespace SdmxCoreTests.Format.Json.Reader.Generic
{
    public abstract class AbstractTestReadingCodelist : AbstractReaderTests
    {
        public AbstractTestReadingCodelist(string format, IJsonStructureReaderManager writer) : base(SdmxStructureEnumType.CodeList, format, writer)
        {
            // Constructor logic
        }

        [Test]
        public void TestInheritanceRestrictionByInclusion()
        {
            AssertReader(TestCodelistInstances.GetCodelistWithInheritenceRAW(false));
        }

        [Test]
        public void TestInheritanceRestrictionByExclusion()
        {
            AssertReader(TestCodelistInstances.GetCodelistWithInheritenceRAW(true));
        }

        [Test]
        public void TestFlatWithDescriptions()
        {
            AssertReader(TestCodelistInstances.GetActivityWithDescriptions());
        }

        [Test]
        public void TestHierarchy()
        {
            AssertReader(TestCodelistInstances.GetSexWithHierarchy());
        }

        // TODO SDMXCORE GEO
        /*
        [TestMethod]
        public void TestGeographicSingleFeature()
        {
            AssertReader(TestGeographicCodelistInstances.CreateSingleGeoFeature());
        }

        [TestMethod]
        public void TestGeoGridSingleCode()
        {
            AssertReader(TestGeoGridCodelistInstances.GetSingleGeoCode());
        }

        [TestMethod]
        public void TestMultipleMixed()
        {
            AssertReader(
                TestCodelistInstances.GetActivityWithDescriptions(),
                TestCodelistInstances.GetSexWithHierarchy(),
                TestGeographicCodelistInstances.CreateSingleGeoFeature(),
                TestGeoGridCodelistInstances.GetSingleGeoCode());
        }
        */
    }
}
