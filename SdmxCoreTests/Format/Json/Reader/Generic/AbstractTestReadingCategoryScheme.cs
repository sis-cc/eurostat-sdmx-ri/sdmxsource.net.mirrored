using System;
using NUnit.Framework;
using NUnit.Framework.Internal;
using Org.Sdmxsource.Sdmx.Api.Constants;
using SdmxCore.Io.Sdmx.Parsers;
using SdmxCoreTests.TestInstances;

namespace SdmxCoreTests.Format.Json.Reader.Generic
{
    [TestFixture]
    public abstract class AbstractTestReadingCategoryScheme : AbstractReaderTests
    {
        public AbstractTestReadingCategoryScheme(string format, IJsonStructureReaderManager writer)
            : base(SdmxStructureEnumType.CategoryScheme, format, writer)
        {
        }

        [Test]
        public void TestGenerateFlatScheme()
        {
            AssertReader(TestCategorySchemeInstances.GenerateFlatScheme());
        }

        [Test]
        public void TestGenerateHierarchicalScheme()
        {
            AssertReader(TestCategorySchemeInstances.GenerateHierarchicalScheme());
        }
    }
}
