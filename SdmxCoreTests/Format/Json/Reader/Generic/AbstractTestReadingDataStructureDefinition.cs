/*******************************************************************************
 *  This software and the accompanying materials of the FMR tools are licensed under the
 *  Apache Licence, version 2.0 (the “Licence”) which accompanies the distribution and is
 *  available at: http://www.apache.org/licenses/LICENSE-2.0
 *  You may not use this file except in compliance with the Licence.
 *  This file is part of the FMR tools and source code libraries.
 *  The software is distributed on an “AS IS” BASIS, WITHOUT WARRANTIES OR
 *  CONDITIONS OF ANY KIND, either express or implied. See the Apache 2.0 Licence for
 *  more details regarding permissions and limitations under the Licence.
 * Copyright © 2022, Bank for International Settlements
 ******************************************************************************/
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.DataStructure;
using SdmxCore.Io.Sdmx.Parsers;
using SdmxCoreTests.TestInstances;
using SdmxCoreTests.TestUtils;
namespace SdmxCoreTests.Format.Json.Reader.Generic
{
    public abstract class AbstractTestReadingDataStructureDefinition : AbstractReaderTests
    {

        public AbstractTestReadingDataStructureDefinition(String format, IJsonStructureReaderManager reader) : base(SdmxStructureEnumType.Dsd, format, reader)
        { }

        [Test]
        public void TestDSDWithComplexContent()
        {
            AssertReader(TestDSDInstances.GetDSDWithComplexContent());
        }

        [Test]
        public void TestDSDWithMSD()
        {
            AssertReader(TestDSDInstances.GetDSDWithMSD());
        }

        /* TODO SDMXCORE GEO
        @Test
        public void testGeoGridRef() {
            assertReader(TestDSDInstances.getDSDWithGeoGrid());
        }
        */

        [Test]
        public void TestBasic()
        {
            AssertReader(TestDSDInstances.GenerateMixedBag(false, false));
        }

        [Test]
        public void TestSeriesAttributes()
        {
            AssertReader(TestDSDInstances.GenerateMixedBag(true, false));
        }

        [Test]
        public void TestMultipleMeasures()
        {
            AssertReader(TestDSDInstances.GenerateMixedBag(false, true));
        }

        [Test]
        public void TestNoMeasuresNoTime()
        {
            AssertReader(TestDSDUtil.GenerateDSD(false, false, "FREQ", "REF_AREA", "INDICATOR"));
        }

        [Test]
        public void TestNoMeasures()
        {
            AssertReader(TestDSDUtil.GenerateDSD(true, false, "FREQ", "REF_AREA", "INDICATOR"));
        }

        [Test]
        public void TestMultipleMandatoryMeasures()
        {
            var dsd = TestDSDInstances.GenerateMixedBag(false, true);
            dsd = TestDSDInstances.SetMandatoryMeasures(dsd, "OBS_VALUE", "BIRTHS");
            AssertReader(dsd);
        }

        [Test]
        /**
         * DSD with only attributes - no dimensions or measures
         */
        public void TestAttributesOnly()
        {
            //TODO
        }

        /**
         * A DSD with a Attribute attached at the DataSet level may have measure relationships
         */
        [Test]
        public void TestMeasureRelationshipOnDataSetAttribute()
        {
            CreateDsdWithMultipleMeasuresAndAttributeOnSpecificLevel(AttributeAttachmentLevel.DataSet);
        }

        /**
         * A DSD with a Attribute attached at the Series level may have measure relationships
         */
        [Test]
        public void TestMeasureRelationshipOnSeriesAttribute()
        {
            CreateDsdWithMultipleMeasuresAndAttributeOnSpecificLevel(AttributeAttachmentLevel.DimensionGroup);
        }

        /**
         * A DSD with a Attribute attached at the Observation level may have measure relationships
         */
        [Test]
        public void TestMeasureRelationshipOnObservationAttribute()
        {
            CreateDsdWithMultipleMeasuresAndAttributeOnSpecificLevel(AttributeAttachmentLevel.Observation);
        }

        private void CreateDsdWithMultipleMeasuresAndAttributeOnSpecificLevel(AttributeAttachmentLevel attachmentLevel)
        {
            IDataStructureObject dsd = TestDSDUtil.GenerateDSD(true, true, "FREQ", "REF_AREA");
            var dsdMutable = dsd.MutableInstance;
            dsdMutable.MeasureList = new MeasureListMutableCore();
            TestDSDUtil.CreateMeasure(dsdMutable, "MEASURE1");
            TestDSDUtil.CreateMeasure(dsdMutable, "MEASURE2");
            TestDSDUtil.CreateMeasure(dsdMutable, "MEASURE3");
            var attributeMutable = TestDSDUtil.CreateAttribute(dsdMutable, "ATTR1", attachmentLevel);
            attributeMutable.AddMeasureRelationship("MEASURE1");
            attributeMutable.AddMeasureRelationship("MEASURE3");
            AssertReader(dsdMutable.ImmutableInstance);
        }

    }
}