using System;
using NUnit.Framework;

using SdmxCoreTests.TestUtils;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using SdmxCore.Io.Sdmx.Parsers;

namespace SdmxCoreTests.Format.Json.Reader.Generic
{
    public abstract class AbstractTestReadingDataflow : AbstractReaderTests
    {

        public AbstractTestReadingDataflow(string format, IJsonStructureReaderManager reader)
            : base(SdmxStructureEnumType.Dataflow, format, reader)
        {
        }

        [Test]
        public void TestSimpleFlow()
        {
            AssertReader(TestDataflowUtil.GenerateDataflow(new StructureReferenceImpl("BIS", "DF_EXR", "1.0.1", SdmxStructureEnumType.Dsd)));
        }

        [Test]
        public void TestSubAgencyReference()
        {
            AssertReader(TestDataflowUtil.GenerateDataflow(new StructureReferenceImpl("BIS.SUB", "DF_EXR", "1.0.1", SdmxStructureEnumType.Dsd)));
        }
    }
}
