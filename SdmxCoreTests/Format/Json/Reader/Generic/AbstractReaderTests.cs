/*******************************************************************************
 *  This software and the accompanying materials of the FMR tools are licensed under the
 *  Apache Licence, version 2.0 (the “Licence”) which accompanies the distribution and is
 *  available at: http://www.apache.org/licenses/LICENSE-2.0
 *  You may not use this file except in compliance with the Licence.
 *  This file is part of the FMR tools and source code libraries.
 *  The software is distributed on an “AS IS” BASIS, WITHOUT WARRANTIES OR
 *  CONDITIONS OF ANY KIND, either express or implied. See the Apache 2.0 Licence for
 *  more details regarding permissions and limitations under the Licence.
 * Copyright © 2022, Bank for International Settlements
 ******************************************************************************/
using Io.Sdmx.Utils.Core.Application;
using Io.Sdmx.Utils.Sdmx.Xs;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Util;
using Org.Sdmxsource.Sdmx.Util.Date;
using Org.Sdmxsource.Util.Io;
using Org.Sdmxsource.Util;
using System.Drawing;
using SdmxCoreTests.Format.Json.Engine.Structure.Reader.Generic;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
using System.Linq;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
using SdmxCore.Io.Sdmx.Parsers;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Diff;

public class AbstractReaderTests : AbstractReadWriteTests
{
    private IJsonStructureReaderManager readerEngine;
    private SdmxStructureEnumType structureType;

    public AbstractReaderTests(SdmxStructureEnumType structureType, String format, IJsonStructureReaderManager readerEngine): base(structureType, format)
    {
        this.readerEngine = readerEngine;
        this.structureType = structureType;
    }

    [OneTimeSetUp]
    public static void clearState()
    {
        SingletonStore.ClearState();
        IFusionObjectStore.ClearState();
    }

    protected void AssertReader(params IMaintainableObject[] expected)
    {
        var f = GetTestFile();
        IReadableDataLocation rdl = new ReadableDataLocationTmp(f);
        ISdmxObjects sdmxBeans = readerEngine.ReadJson(rdl, new InMemoryRetrievalManager());
        var actualObjects = sdmxBeans.GetMaintainables(structureType);
        foreach (IMaintainableObject maint in expected)
        {
            var objectByUrn = actualObjects.FirstOrDefault(actual => actual.Urn == maint.Urn);

            Assert.NotNull("Missing in Expected: " + maint.Urn);

            var diffReport = new DiffReport(maint, objectByUrn);
            var equal = maint.DeepEquals(objectByUrn, true, diffReport);
            if (!equal)
            {
                Console.WriteLine("Object with name : " + maint.Name + " has differences");
                foreach (var diff in diffReport.GetDifferences())
                {
                    Console.WriteLine("FOR THE PROPERTY " + diff.Property + ":  \nEXPECTED VALUE " + diff.SourceValue + " ACTUAL VALUE " + diff.TargetValue);
                }
            }

            Assert.True(equal);

            //else
            //{
            //    Assert.True(maint.DeepEquals(objectByUrn, true), "Diff: " + maint.Urn);
            //    Assert.True(objectByUrn.DeepEquals(maint, true), "Diff: " + objectByUrn.Urn);
            //}
        }

        foreach (var item in actualObjects)
        {
            var objectByUrn = actualObjects.FirstOrDefault(x => x.Urn == item.Urn);
            Assert.NotNull("Missing in Actual: " + item.Urn);
        }
    }
}