// -----------------------------------------------------------------------
// <copyright file="AbstractReadWriteTests.cs" company="EUROSTAT">
//   Date Created : 2023-05-23
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.Format.Json.Engine.Structure.Reader.Generic
{
    using Io.Sdmx.Utils.Core.Application;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    public class AbstractReadWriteTests
    {
        private readonly string _baseLocation;

        public AbstractReadWriteTests(SdmxStructureEnumType structureType, string format)
        {
            _baseLocation = "Resources/Engine/Structure/" + GetURNPostfix(structureType, format);
        }

        protected static string GetURNPostfix(SdmxStructureEnumType structureType, string format)
        {
            return format + "/" + SdmxStructureType.GetFromEnum(structureType).UrnClass + "/";
        }

        /// <summary> 
        /// Based on test name 
        /// </summary>
        /// <returns>
        /// 
        /// </returns> 
        protected FileInfo GetTestFile()
        {
            var expectedXMLFile = TestContext.CurrentContext.Test.MethodName + ".json";
            return new FileInfo(Path.Combine(_baseLocation, expectedXMLFile));
        }

        [OneTimeSetUp]
        public static void ClearState()
        {
            SingletonStore.ClearState();
            IFusionObjectStore.ClearState();
        }
    }
}