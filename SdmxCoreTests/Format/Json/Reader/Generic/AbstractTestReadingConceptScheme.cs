using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using SdmxCore.Io.Sdmx.Parsers;
using SdmxCoreTests.TestInstances;

namespace SdmxCoreTests.Format.Json.Reader.Generic
{
    public abstract class AbstractTestReadingConceptScheme : AbstractReaderTests
    {
        public AbstractTestReadingConceptScheme(string format, IJsonStructureReaderManager reader)
            : base(SdmxStructureEnumType.ConceptScheme, format, reader)
        {
        }

        [Test]
        public void TestMixedRepresentations()
        {
            AssertReader(TestConceptSchemeInstances.GenerateMixedRepresentations());
        }

        [Test]
        public void TestNonRepresentedScheme()
        {
            AssertReader(TestConceptSchemeInstances.GenerateNonRepresentedScheme());
        }
    }
}
