using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using SdmxCore.Io.Sdmx.Parsers;
using SdmxCoreTests.TestInstances;

namespace SdmxCoreTests.Format.Json.Reader.Generic // Replace with your actual namespace
{
    public abstract class AbstractTestReadCategorization : AbstractReaderTests
    {
        public AbstractTestReadCategorization(string format, IJsonStructureReaderManager writer)
            : base(SdmxStructureEnumType.Categorisation, format, writer)
        {
        }

        [Test]
        public void TestMaintainableCategorisation()
        {
            AssertReader(TestCategorisationInstances.MaintainableCategorisation);
        }

        [Test]
        public void TestCodeCategorisation()
        {
            AssertReader(TestCategorisationInstances.CodeCategorisation);
        }

        [Test]
        public void TestDimensionCategorisation()
        {
            AssertReader(TestCategorisationInstances.DimensionCategorisation);
        }

        [Test]
        public void TestMultipleCategorisation()
        {
            AssertReader(TestCategorisationInstances.DimensionCategorisation, TestCategorisationInstances.CodeCategorisation);
        }
    }
}