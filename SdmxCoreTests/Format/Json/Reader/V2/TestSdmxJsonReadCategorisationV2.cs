using System;
using NUnit.Framework;
using SdmxCoreTests.Format.Json.Reader.Generic;
using SdmxJsonStructureReaderManagerV2 = SdmxCore.Io.Sdmx.Parsers.SdmxJsonStructureReaderManagerV2;

namespace SdmxCoreTests.Format.Json.Reader.V2
{
    public class TestSdmxJsonReadCategorisationV2 : AbstractTestReadCategorization
    {
        public TestSdmxJsonReadCategorisationV2()
            : base("SdmxJsonV2", SdmxJsonStructureReaderManagerV2.Instance)
        {
        }
    }
}
