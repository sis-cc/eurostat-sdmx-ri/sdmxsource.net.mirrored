using NUnit.Framework;
using SdmxCoreTests.TestUtils;
using SdmxCoreTests.Format.Json.Reader.Generic;
using SdmxCore.Io.Sdmx.Parsers;

namespace SdmxCoreTests.Format.Json.Reader.V2
{
    public class TestSdmxJsonReadDataflowV2 : AbstractTestReadingDataflow
    {
        public TestSdmxJsonReadDataflowV2()
            : base("SdmxJsonV2", SdmxJsonStructureReaderManagerV2.Instance)
        {
        }
    }
}
