using SdmxCore.Io.Sdmx.Parsers;
using SdmxCoreTests.Format.Json.Reader.Generic;

/**
 * Reads in SDMX-JSON version 2.0 format - the files are in src/test/resources/engine/structure/SdmxJsonV2/DataStructureDefinition
 * and the names follow the test names, which are in the abstract supertype
 */
public class TestSdmxJsonReadDataStructureDefinitionV2 : AbstractTestReadingDataStructureDefinition
{

    public TestSdmxJsonReadDataStructureDefinitionV2() : base("SdmxJsonV2", SdmxJsonStructureReaderManagerV2.Instance)
    {

    }
}