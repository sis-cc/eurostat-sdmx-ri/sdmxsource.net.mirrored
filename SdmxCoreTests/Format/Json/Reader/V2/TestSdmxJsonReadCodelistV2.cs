using System;
using SdmxCore.Io.Sdmx.Parsers;
using SdmxCoreTests.Format.Json.Reader.Generic;
using SdmxCoreTests.TestUtils;

namespace SdmxCoreTests.Format.Json.Reader.V2
{
    public class TestSdmxJsonReadCodelistV2 : AbstractTestReadingCodelist
    {
        public TestSdmxJsonReadCodelistV2() : base("SdmxJsonV2", SdmxJsonStructureReaderManagerV2.Instance)
        {
        }
    }
}