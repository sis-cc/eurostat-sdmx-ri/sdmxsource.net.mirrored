using SdmxCore.Io.Sdmx.Parsers;
using SdmxCoreTests.Format.Json.Reader.Generic;

public class TestSdmxJsonReadConceptSchemeV2 : AbstractTestReadingConceptScheme
{
    public TestSdmxJsonReadConceptSchemeV2() : base("SdmxJsonV2", SdmxJsonStructureReaderManagerV2.Instance)
    {
    }
}
