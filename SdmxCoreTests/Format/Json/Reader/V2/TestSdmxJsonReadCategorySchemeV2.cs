using SdmxCore.Io.Sdmx.Parsers;
using SdmxCoreTests.Format.Json.Reader.Generic;

namespace SdmxCoreTests.Format.Json.Reader.V2
{
    public class TestSdmxJsonReadCategorySchemeV2 : AbstractTestReadingCategoryScheme
    {
        public TestSdmxJsonReadCategorySchemeV2()
            : base("SdmxJsonV2", SdmxJsonStructureReaderManagerV2.Instance)
        {
        }
    }
}
