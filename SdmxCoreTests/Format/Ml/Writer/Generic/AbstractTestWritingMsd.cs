// -----------------------------------------------------------------------
// <copyright file="AbstractTestWritingMsd.cs" company="EUROSTAT">
//   Date Created : 2024-03-19
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Io.Sdmx.Core.Sdmx.Api.Engine.Structure;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using SdmxCoreTests.TestInstances;
using SdmxCoreTests.TestInstances.io.sdmx.im.util.model;

namespace SdmxCoreTests.Format.Ml.Writer.Generic
{
    public abstract class AbstractTestWritingMsd : AbstractStaxWriterTest
    {
        public AbstractTestWritingMsd(SdmxSchema version, IStructureWriterEngine writer)
            : base(SdmxStructureEnumType.Msd, version, writer) { }


        [Test]
        public void TestCodedAttributes()
        {
            AssertWriter(TestMSDInstances.GetFlatNoHierarchy(true));
        }


        [Test]
        public void TestNonCodedAttributes()
        {
            AssertWriter(TestMSDInstances.GetFlatNoHierarchy(false));
        }

        [Test]
        public void TestHierarchy()
        {
            AssertWriter(TestMSDInstances.GetMixedHierarchy());
        }

        [Test]
        public void TestMultiple()
        {
            AssertWriter(TestMSDInstances.GetMixedHierarchy(), TestMSDInstances.GetFlatNoHierarchy(false));
        }

        [Test]
        public void TestDataQualityReport()
        {
            AssertWriter(TestMSDInstances.GetDataQualityReport("Test","Id","1.0"));
        }
    }
}
