using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Io.Sdmx.Core.Sdmx.Api.Engine.Structure;
using Org.Sdmxsource.Sdmx.Api.Constants;
using SdmxCoreTests.TestInstances;
using NUnit.Framework;

namespace SdmxCoreTests.Format.Ml.Writer.Generic
{
    public abstract class AbstractTestWritingContentConstraint : AbstractStaxWriterTest {
        public AbstractTestWritingContentConstraint(SdmxSchema version, IStructureWriterEngine writer)
           : base(SdmxStructureEnumType.ContentConstraint, version, writer)
        {
        }

        [Test]
        public void TestActualConstraint()
        {
            AssertWriter(TestContentConstraintInstances.GetActualConstraint());
        }

        [Test]
        public void TestCascade()
        {
            AssertWriter(TestContentConstraintInstances.GetCascade(true));
        }

        [Test]
        public void TestCubeConstraintWithValidity()
        {
            AssertWriter(TestContentConstraintInstances.GetCubeConstraintWithValidity(true));
        }

        [Test]
        public void TestCubesRemovePrefix()
        {
            AssertWriter(TestContentConstraintInstances.GetCubesRemovePrefix());
        }

        [Test]
        public void TestSeriesRemovePrefix()
        {
            AssertWriter(TestContentConstraintInstances.GetSeriesRemovePrefix());
        }

        [Test]
        public void TestMultipleCubes()
        {
            AssertWriter(TestContentConstraintInstances.GetMultipleCubes());
        }

        [Test]
        public void TestSeriesConstraint()
        {
            AssertWriter(TestContentConstraintInstances.GetSeriesConstraint());
        }

        [Test]
        public void TestSeriesConstraintWithAttrValues()
        {
            AssertWriter(TestContentConstraintInstances.GetSeriesConstraintWithAttrValues());
        }

        [Test]
        public void TestSeriesConstraintWithMultiValues()
        {
            AssertWriter(TestContentConstraintInstances.GetSeriesConstraintWithMultiValues());
        }
    } 
}
