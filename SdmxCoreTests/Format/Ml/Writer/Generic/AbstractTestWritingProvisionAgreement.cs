using Io.Sdmx.Core.Sdmx.Api.Engine.Structure;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using SdmxCoreTests.TestInstances;

namespace SdmxCoreTests.Format.Ml.Writer.Generic
{
    public abstract class AbstractTestWritingProvisionAgreement : AbstractStaxWriterTest
    {
        public AbstractTestWritingProvisionAgreement(SdmxSchema version, IStructureWriterEngine writer) 
            : base(SdmxStructureEnumType.ProvisionAgreement, version, writer)
        {
        }

        [Test]
        public void TestSimpleProvision()
        {
            AssertWriter(TestDataProvisionInstances.GetEXRProvision("DP1"));
        }
    }
}
