using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3;
using Org.Sdmxsource.Sdmx.Api.Constants;
using SdmxCoreTests.Format.Ml.Writer.Generic;

namespace SdmxCoreTests.Format.Ml.Writer.V3
{
    public class TestWritingContentConstraintV3 : AbstractTestWritingContentConstraint
    {

        public TestWritingContentConstraintV3()
        : base(SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionThree),
                      StaxStructureWriterEngineV3.GetInstance(true, null, null, null, null, null, null))
        { 
        }

    }
}
