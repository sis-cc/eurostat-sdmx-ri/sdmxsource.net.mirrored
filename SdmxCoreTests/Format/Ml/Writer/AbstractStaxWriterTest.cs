// -----------------------------------------------------------------------
// <copyright file="AbstractStaxWriterTest.cs" company="EUROSTAT">
//   DateTime Created : 2023-06-13
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.Format.Ml.Writer
{
    using System.Diagnostics;
    using System.IO;
    using System.Text;
    using Io.Sdmx.Core.Sdmx.Api.Engine.Structure;
    using Io.Sdmx.Utils.Core.Xml;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Util.Io;
    using Org.Sdmxsource.XmlHelper;
    using Sdmx.TestUtils.XmlRoundtripTests;
    using SdmxCoreTests.Format.Ml;

    public abstract class AbstractStaxWriterTest : AbstractReadWriteTests
    {
        private readonly SdmxStructureEnumType? _structureType;
        private readonly SdmxSchema _version;
        private readonly IStructureWriterEngine _writer;
        private readonly SdmxXmlCompareFactory _xmlCompareFactory = new SdmxXmlCompareFactory();

        public AbstractStaxWriterTest(SdmxStructureEnumType? structureType, SdmxSchema version, IStructureWriterEngine writer)
            : base(structureType, version)
        {
            this._structureType = structureType;
            this._writer = writer;
            this._version = version;
        }

        protected void AssertWriter(params IMaintainableObject[] maints)
        {
            ISdmxObjects objects = new SdmxObjectsImpl();
            foreach (IMaintainableObject maint in maints)
            {
                objects.AddIdentifiable(maint);
            }
            AssertWriter(objects);
        }

        protected void AssertWriter(ISdmxObjects objects)
        {
            // Ensure there is at least 1 sdmxObject of the expected type for this test
            ISet<IMaintainableObject> IexpectedObjects = _structureType == null ? objects.GetAllMaintainables() : objects.GetMaintainables(_structureType.Value);
            Assert.That(IexpectedObjects, Has.Count.GreaterThan(0), "There are no objects in this input of the expected type: " + _structureType);

            // Write out the objects in SDMX 2.0
            MemoryStream bos = new MemoryStream();
            WriteStructures(objects, bos);

            // Assert the output is as expected
            FileInfo expectedFile = GetTestFile();
            var x = Encoding.ASCII.GetString(bos.ToArray());
            try
            {
                XMLParser.ValidateXml(new MemoryStream(bos.ToArray()), _version.EnumType);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Debug.WriteLine(XmlUtils.FormatXml(bos.ToString()));
                Assert.Fail("XML invalid against Schema");
            }

            var testEngine = _xmlCompareFactory.GetEngine(_version.EnumType);
            bos.Position = 0;
            RoundtripComparisonResult result;
            using (Stream controlStream = FileUtil.ReadFileAsStream(expectedFile.FullName))
            {
                result = testEngine.IgnoreNodes("Header").AreEqual(controlStream, bos);
            }
            Assert.That(result.IsEqual, Is.True, result.GetResultString());
        }

        protected void WriteStructures(ISdmxObjects objects, Stream outputStream)
        {
            foreach (IMaintainableObject maint in objects.GetAllMaintainables())
            {
                Assert.That(_writer.IsSupportedType(maint), Is.True);
            }
            _writer.WriteStructures(objects, null, outputStream);
        }
    }
}