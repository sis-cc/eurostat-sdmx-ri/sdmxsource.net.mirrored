// -----------------------------------------------------------------------
// <copyright file="AbstractReadWriteTests.cs" company="EUROSTAT">
//   DateTime Created : 2023-06-13
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.Format.Ml
{
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using NUnit.Framework;
    using Io.Sdmx.Utils.Core.Application;

    public abstract class AbstractReadWriteTests
    {
        private readonly string _baseLocation;

        protected AbstractReadWriteTests(SdmxStructureEnumType? structureType, SdmxSchema version)
        {
            _baseLocation = "Resources/Engine/Structure/" + version.ToString() + GetURNPostfix(structureType);
        }

        protected static string GetURNPostfix(SdmxStructureEnumType? structureType)
        {
            if (structureType != null)
            {
                if (structureType == SdmxStructureEnumType.Any)
                {
                    return "/MultipleStructures/";
                }
                else
                {
                    return "/" + SdmxStructureType.GetFromEnum(structureType.Value).UrnClass + "/";
                }
            }
            return string.Empty;
        }

        /// <summary> 
        /// Based on test name 
        /// </summary>
        /// <returns>
        /// </returns> 
        protected FileInfo GetTestFile()
        {
            string expectedXMLFile = TestContext.CurrentContext.Test.MethodName + ".Xml";
            FileInfo expectedFile = new FileInfo(Path.Combine(_baseLocation, expectedXMLFile));
            return expectedFile;
        }

        [OneTimeSetUp]
        public void ClearState()
        {
            SingletonStore.ClearState();
            IFusionObjectStore.ClearState();
        }
    }
}