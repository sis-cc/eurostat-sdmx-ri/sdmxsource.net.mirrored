// -----------------------------------------------------------------------
// <copyright file="AbstractReaderTests.cs" company="EUROSTAT">
//   Date Created : 2023-06-23
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.Format.Ml.Reader
{
    using Io.Sdmx.Format.Ml.Api.Engine;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Diff;
    using Org.Sdmxsource.Util.Io;
    using SdmxCoreTests.Format.Ml;

    public abstract class AbstractReaderTests : AbstractReadWriteTests
    {
        private readonly IStaxStructureReaderEngine _readerEngine;
        private readonly SdmxStructureEnumType _structureType;

        protected AbstractReaderTests(SdmxStructureEnumType structureType, SdmxSchema version, IStaxStructureReaderEngine readerEngine)
                : base(structureType, version)
        {
            _readerEngine = readerEngine;
            _structureType = structureType;
        }

        //[OneTimeSetUp]
        //public override void ClearState()
        //{
        //    SingletonStore.ClearState();
        //    IFusionObjectStore.ClearState();
        //}

        protected void AssertReader(params IMaintainableObject[] maint)
        {
            FileInfo f = GetTestFile();
            IReadableDataLocation rdl = new ReadableDataLocationTmp(f);
            ISdmxObjects IsdmxObjects = _readerEngine.GetSdmxObjects(rdl);

            IDictionary<string, IMaintainableObject> expected = maint.ToDictionary(m => m.Urn.AbsoluteUri, maint => maint);
            IDictionary<string, IMaintainableObject> actual = IsdmxObjects.GetMaintainables(_structureType).ToDictionary(m => m.Urn.AbsoluteUri, maint => maint);

            CollectionAssert.AreEquivalent(expected, actual);
            foreach (string urn in actual.Keys)
            {
                IMaintainableObject mAc = actual[urn];
                IMaintainableObject mEx = expected[urn];
                //NOTE SDMXSOURCE_SDMXCORE CHANGE diff was not used
                //DiffReport diff = mAc.diff(mEx);
                var report = new DiffReport(mAc, mEx);
                var result = mAc.DeepEquals(mEx, true, report);
                foreach (var diff in report.GetDifferences())
                { 
                    Console.WriteLine(diff.ToString()); 
                }
                Assert.IsTrue(result);

            }
        }
    }
}