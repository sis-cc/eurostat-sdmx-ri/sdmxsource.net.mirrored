using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Io.Sdmx.Format.Ml.Api.Engine;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using SdmxCoreTests.TestInstances;

namespace SdmxCoreTests.Format.Ml.Reader.Generic
{
    public abstract class AbstractTestReadingDataConsumerScheme: AbstractReaderTests
    {
        public AbstractTestReadingDataConsumerScheme(SdmxSchema version, IStaxStructureReaderEngine writer) :
            base(SdmxStructureEnumType.DataConsumerScheme, version, writer)
        {
        }

        [Test]
        public void TestSimpleScheme()
        {
            AssertReader(TestDataConsumerSchemeInstances.GetDCScheme());
        }

        [Test]
        public void TestContactDetails()
        {
            AssertReader(TestDataConsumerSchemeInstances.GetSubScheme("IMF"));
        }

        [Test]
        public void TestMultipleSchemes()
        {
            AssertReader(TestDataConsumerSchemeInstances.GetSubScheme("IMF"), TestDataConsumerSchemeInstances.GetSubScheme("IMF.SUB"));
        }
    }
}
