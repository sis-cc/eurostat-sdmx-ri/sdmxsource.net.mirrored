using Io.Sdmx.Format.Ml.Api.Engine;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using SdmxCoreTests.TestInstances;

namespace SdmxCoreTests.Format.Ml.Reader.Generic
{
    public abstract class AbstractTestReadingDataProviderScheme : AbstractReaderTests
    {
        public AbstractTestReadingDataProviderScheme(SdmxSchema version, IStaxStructureReaderEngine writer) :
            base(SdmxStructureEnumType.DataProviderScheme, version, writer)
        {
        }

        [Test]
        public void TestSimpleScheme()
        {
            AssertReader(TestDataProviderSchemeInstances.GetDPScheme());
        }

        [Test]
        public void TestContactDetails()
        {
            AssertReader(TestDataProviderSchemeInstances.GetSubScheme("IMF"));
        }

        [Test]
        public void TestMultipleSchemes()
        {
            AssertReader(TestDataProviderSchemeInstances.GetSubScheme("IMF"), TestDataProviderSchemeInstances.GetSubScheme("IMF.SUB"));
        }
    }
}
