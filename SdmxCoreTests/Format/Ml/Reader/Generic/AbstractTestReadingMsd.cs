// -----------------------------------------------------------------------
// <copyright file="AbstractTestReadingMsd.cs" company="EUROSTAT">
//   Date Created : 2024-04-02
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using Io.Sdmx.Format.Ml.Api.Engine;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using SdmxCoreTests.TestInstances;

namespace SdmxCoreTests.Format.Ml.Writer.Generic
{
    public abstract class AbstractTestReadingMsd : Reader.AbstractReaderTests
    {
        public AbstractTestReadingMsd(SdmxSchema version, IStaxStructureReaderEngine readerEngine)
            : base(SdmxStructureEnumType.Msd, version, readerEngine) { }


        [Test]
        public void TestCodedAttributes()
        {
            AssertReader(TestMSDInstances.GetFlatNoHierarchy(true));
        }


        [Test]
        public void TestNonCodedAttributes()
        {
            AssertReader(TestMSDInstances.GetFlatNoHierarchy(false));
        }

        [Test]
        public void TestHierarchy()
        {
            AssertReader(TestMSDInstances.GetMixedHierarchy());
        }

        [Test]
        public void TestMultiple()
        {
            AssertReader(TestMSDInstances.GetMixedHierarchy(), TestMSDInstances.GetFlatNoHierarchy(false));
        }

        [Test]
        public void TestDataQualityReport()
        {
            AssertReader(TestMSDInstances.GetDataQualityReport("Test", "Id", "1.0"));
        }
    }
}
