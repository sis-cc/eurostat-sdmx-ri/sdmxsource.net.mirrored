// -----------------------------------------------------------------------
// <copyright file="AbstractTestReadingAgencyScheme.cs" company="EUROSTAT">
//   Date Created : 2023-11-23
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Reader.Generic
{
    using Io.Sdmx.Format.Ml.Api.Engine;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using SdmxCoreTests.TestInstances;
    using SdmxCoreTests.Format.Ml.Reader;

    public abstract class AbstractTestReadingAgencyScheme : AbstractReaderTests
    {

        public AbstractTestReadingAgencyScheme(SdmxSchema version, IStaxStructureReaderEngine writer) 
            : base(SdmxStructureEnumType.AgencyScheme, version, writer)
        {
        }

        [Test]
        public void TestSdmxScheme()
        {
            AssertReader(TestAgencySchemeInstances.GetSDMXScheme());
        }

        [Test]
        public void TestContactDetails()
        {
            AssertReader(TestAgencySchemeInstances.GetSubScheme("IMF"));
        }

        [Test]
        public void TestMultipleSchemes()
        {
            AssertReader(TestAgencySchemeInstances.GetSubScheme("IMF"), TestAgencySchemeInstances.GetSubScheme("IMF.SUB"));
        }

    }
}