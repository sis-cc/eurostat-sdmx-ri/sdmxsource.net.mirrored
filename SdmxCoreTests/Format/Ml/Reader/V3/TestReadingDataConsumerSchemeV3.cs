using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3;
using Org.Sdmxsource.Sdmx.Api.Constants;
using SdmxCoreTests.Format.Ml.Reader.Generic;

namespace SdmxCoreTests.Format.Ml.Reader.V3
{
    public class TestReadingDataConsumerSchemeV3 : AbstractTestReadingDataConsumerScheme
    {
        public TestReadingDataConsumerSchemeV3()
            : base(SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionThree), StaxStructureReaderEngineV3.Instance)
        {
        }
    }
}
