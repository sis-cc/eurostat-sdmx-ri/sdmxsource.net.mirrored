// -----------------------------------------------------------------------
// <copyright file="TestReadingAgencySchemeV3.cs" company="EUROSTAT">
//   Date Created : 2023-11-23
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxCoreTests.Format.Ml.Reader.V3
{
    using Io.Sdmx.Format.Ml.Engine.Structure.Reader.Generic;
    using Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary> 
    /// Reads in version 3.0 format - the response files are in src/test/resources/engine/structure/3.0/Hierarchy 
    /// and the names follow the test names, which are in the abstract supertype 
    /// </summary> 
    public class TestReadingAgencySchemeV3 : AbstractTestReadingAgencyScheme
    {

        public TestReadingAgencySchemeV3()
                : base(SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionThree), StaxStructureReaderEngineV3.Instance)
        {
        }

    }

}