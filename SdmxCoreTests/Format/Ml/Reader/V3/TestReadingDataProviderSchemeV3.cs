using Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3;
using Org.Sdmxsource.Sdmx.Api.Constants;
using SdmxCoreTests.Format.Ml.Reader.Generic;

namespace SdmxCoreTests.Format.Ml.Reader.V3
{
    public class TestReadingDataProviderSchemeV3 : AbstractTestReadingDataProviderScheme
    {
        public TestReadingDataProviderSchemeV3() 
            : base(SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionThree), StaxStructureReaderEngineV3.Instance)
        {
        }
    }
}
