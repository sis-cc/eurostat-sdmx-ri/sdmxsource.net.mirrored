// -----------------------------------------------------------------------
// <copyright file="TestGeoGridCodelistInstances.cs" company="EUROSTAT">
//   Date Created : 2024-03-13
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using SdmxCoreTests.TestUtils;

namespace SdmxCoreTests.TestInstances
{
    public class TestGeoGridCodelistInstances
    {
        public static IGeoGridCodelistObject GetSingleGeoCode()
        {
            string gridDefinition = "4326:UL;9.580078,55.103516;1000,1000:W3W";
            string geoCodeId = "UK";
            string geoCell = "0,1:chats.loyal.equal";
            return TestGeoGridCodelistUtil.GenerateCodelist("ACY", "CL_REF_AREA", "1.0", gridDefinition, ToCode(geoCodeId, geoCell));
        }

        private static string ToCode(string codeId, string geoCell)
        {
            return codeId + ":" + geoCell;
        }
    }
}
