// -----------------------------------------------------------------------
// <copyright file="TestDSDInstances.cs" company="EUROSTAT">
//   Date Created : 2023-05-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.TestInstances
{
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using SdmxCoreTests.TestUtils;
    using SdmxCoreTests.Utils;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    public class TestDSDInstances
    {
        public static ISdmxObjects CreateCodedDsd()
        {
            return CreateCodedInstance(false, false, false);
        }

        public static ISdmxObjects CreateCodedDsdWithSeriesAttributes()
        {
            return CreateCodedInstance(false, true, false);
        }

        public static ISdmxObjects CreateCodedDsdWithSeriesAndObsAttributes()
        {
            return CreateCodedInstance(false, true, true);
        }

        public static ISdmxObjects CreateCodedDsdWithDatasetSeriesAndObsAttributes()
        {
            return CreateCodedInstance(true, true, true);
        }

        /**
         * Creates DSD with
         * Dimensions:
         * 	FREQ
         *  REF_AREA  		=> FR, UK
         Indicator		=> Bw, Dw
         Series Attributes (if requested)
         ICollection		=> B, E
         TimeFormat		=> 602, 208

         Obs Attributes (if requested)
         ObsStatus		=> A, B, E, Z
         ObsConf		=> C, F
         RepYearStart 	=> 1, 2, 3
         *
         * @param includeDsAttributes
         * @param includeSeriesAttributes
         * @param includeObsAttributes
         * @return
         */
        public static IDataStructureSuperObject GetSuperObject(bool includeDsAttributes, bool includeCodedSeriesAttributes, bool includeCodedObsAttributes)
        {
            string testAcy = "TEST";

            ICodelistObject refAreaCodelist = TestCodelistUtil.GenerateCodelist(testAcy, "CL_REF_AREA", "1.0", "FR France", "UK United Kingdom");
            ICodelistObject indicatorCodelist = TestCodelistUtil.GenerateCodelist(testAcy, "CL_INDICATOR", "1.0", "BW Boiled Water", "DW Drinking Water");
            ICodelistObject collectionCodelist = TestCodelistUtil.GenerateCodelist(testAcy, "CL_COLLECTION", "1.0", "B Beginning of period", "E End of period");
            ICodelistObject timeFormatCodelist = TestCodelistUtil.GenerateCodelist(testAcy, "CL_TIME_FORMAT", "1.0", "602 Ccyy", "608 Ccyyq");
            ICodelistObject obsStatusCodelist = TestCodelistUtil.GenerateCodelist(testAcy, "CL_OBS_STATUS", "1.0", "A Normal Value", "B Break", "E Estimated Value", "Z Unknown, or unavailable status");
            ICodelistObject obsConfCodelist = TestCodelistUtil.GenerateCodelist(testAcy, "CL_OBS_CONF", "1.0", "C Confidential", "F Free");
            ICodelistObject repYearCodelist = TestCodelistUtil.GenerateCodelist(testAcy, "CL_REP_YEAR_START", "1.0", "1 1900-1910", "2 1911-1920", "3 1921-1930");

            // Create a DSD with coded dimensions
            IDictionary<string, ICodelistObject> representations = new Dictionary<string, ICodelistObject>();
            representations.Add("REF_AREA", refAreaCodelist);
            representations.Add("INDICATOR", indicatorCodelist);
            representations.Add("COLLECTION", collectionCodelist);
            representations.Add("TIME_FORMAT", timeFormatCodelist);
            representations.Add("OBS_STATUS", obsStatusCodelist);
            representations.Add("OBS_CONF", obsConfCodelist);
            representations.Add("REP_YEAR_START", repYearCodelist);

            IList<string> dimensions = new List<string>() { "FREQ", "REF_AREA", "INDICATOR" };
            IList<string> serAttr;

            if (includeCodedSeriesAttributes)
            {
                serAttr = new List<string>() { "COLLECTION", "TIME_FORMAT" };
            }
            else
            {
                serAttr = new List<string>();
            }

            IList<string> obsAttr;
            if (includeCodedObsAttributes)
            {
                obsAttr = new List<string>() { "OBS_STATUS", "OBS_PRE_BREAK", "OBS_CONF", "OBS_INFO" };
            }
            else
            {
                obsAttr = new List<string>();
            }

            return TestDSDUtil.GenerateDSDSb(dimensions, serAttr, true, true, obsAttr, null, representations, testAcy, "TEST_DSD", "1.0");
        }

        private static ISdmxObjects CreateCodedInstance(bool includeDsAttributes, bool includeCodedSeriesAttributes, bool includeCodedObsAttributes)
        {
            string testAcy = "TEST";

            ICodelistObject refAreaCodelist = TestCodelistUtil.GenerateCodelist(testAcy, "CL_REF_AREA", "1.0", "FR France", "UK United Kingdom");
            ICodelistObject indicatorCodelist = TestCodelistUtil.GenerateCodelist(testAcy, "CL_INDICATOR", "1.0", "BW Boiled Water", "DW Drinking Water");
            ICodelistObject collectionCodelist = TestCodelistUtil.GenerateCodelist(testAcy, "CL_COLLECTION", "1.0", "B Beginning of period", "E End of period");
            ICodelistObject timeFormatCodelist = TestCodelistUtil.GenerateCodelist(testAcy, "CL_TIME_FORMAT", "1.0", "602 Ccyy", "608 Ccyyq");
            ICodelistObject obsStatusCodelist = TestCodelistUtil.GenerateCodelist(testAcy, "CL_OBS_STATUS", "1.0", "A Normal Value", "B Break", "E Estimated Value", "Z Unknown, or unavailable status");
            ICodelistObject obsConfCodelist = TestCodelistUtil.GenerateCodelist(testAcy, "CL_OBS_CONF", "1.0", "C Confidential", "F Free");
            ICodelistObject repYearCodelist = TestCodelistUtil.GenerateCodelist(testAcy, "CL_REP_YEAR_START", "1.0", "1 1900-1910", "2 1911-1920", "3 1921-1930");

            // Create a DSD with coded dimensions
            IDictionary<string, ICodelistObject> representations = new Dictionary<string, ICodelistObject>();
            representations.Add("REF_AREA", refAreaCodelist);
            representations.Add("INDICATOR", indicatorCodelist);
            representations.Add("COLLECTION", collectionCodelist);
            representations.Add("TIME_FORMAT", timeFormatCodelist);
            representations.Add("OBS_STATUS", obsStatusCodelist);
            representations.Add("OBS_CONF", obsConfCodelist);
            representations.Add("REP_YEAR_START", repYearCodelist);

            IDataStructureSuperObject dsdSb = GetSuperObject(includeDsAttributes, includeCodedSeriesAttributes, includeCodedObsAttributes);
            IDataStructureObject dsd = dsdSb.BuiltFrom;

            // Make REF_AREA and INDICATOR coded dimensions
            IDataStructureMutableObject dsdMut = new DataStructureMutableCore(dsd);
            CreateCodedComponent(dsdMut.GetDimension("REF_AREA"), refAreaCodelist);
            CreateCodedComponent(dsdMut.GetDimension("INDICATOR"), indicatorCodelist);

            if (includeCodedSeriesAttributes)
            {
                CreateCodedComponent(dsdMut.GetAttribute("COLLECTION"), collectionCodelist);
                CreateCodedComponent(dsdMut.GetAttribute("TIME_FORMAT"), timeFormatCodelist);
            }

            if (includeCodedObsAttributes)
            {
                CreateCodedComponent(dsdMut.GetAttribute("OBS_STATUS"), obsStatusCodelist);
                CreateCodedComponent(dsdMut.GetAttribute("OBS_CONF"), obsConfCodelist);
            }

            if (includeDsAttributes)
            {
                // Add coded attribute
                IAttributeMutableObject repYearAttr = new AttributeMutableCore();
                repYearAttr.Id = "REP_YEAR_START";
                repYearAttr.ConceptRef = TestDSDUtil.CreateConceptReference("REP_YEAR_START");
                repYearAttr.SetMandatory(false);
                repYearAttr.AttachmentLevel = AttributeAttachmentLevel.DataSet;
                dsdMut.AddAttribute(repYearAttr);
                CreateCodedComponent(dsdMut.GetAttribute("REP_YEAR_START"), repYearCodelist);

                // Add uncoded attribute
                IAttributeMutableObject commentAttr = new AttributeMutableCore();
                commentAttr.Id = "COMMENT_DSET";
                commentAttr.ConceptRef = TestDSDUtil.CreateConceptReference("COMMENT_DSET");
                commentAttr.SetMandatory(false);
                commentAttr.AttachmentLevel = AttributeAttachmentLevel.DataSet;
                dsdMut.AddAttribute(commentAttr);
            }

            dsd = dsdMut.ImmutableInstance;

            ISdmxObjects objects = new SdmxObjectsImpl();
            objects.AddIdentifiable(refAreaCodelist);
            objects.AddIdentifiable(indicatorCodelist);
            objects.AddIdentifiable(collectionCodelist);
            objects.AddIdentifiable(timeFormatCodelist);
            objects.AddIdentifiable(obsStatusCodelist);
            objects.AddIdentifiable(obsConfCodelist);
            objects.AddIdentifiable(repYearCodelist);
            objects.AddIdentifiable(dsd);

            IConceptSchemeObject testConceptScheme = TestConceptUtil.GenerateConceptSchemeWithProps(
                testAcy, "CS", "1.0", "_NAME", new string[] { "T", "FREQ", "INDICATOR", "REF_AREA", "COLLECTION", "TIME_FORMAT", "OBS_PRE_BREAK", "OBS_CONF", "OBS_STATUS", "OBS_INFO", "COMMENT_DSET" });
            IConceptSchemeMutableObject csMut = new ConceptSchemeMutableCore(testConceptScheme);

            // Make a couple of the concepts have a clearer name
            ConceptMutableCore obsValueConcept = new ConceptMutableCore();
            obsValueConcept.Id = "OBS_VALUE";
            obsValueConcept.AddName("en", "Observation Value");
            csMut.AddItem(obsValueConcept);
            if (includeDsAttributes)
            {
                ConceptMutableCore repYearConcept = new ConceptMutableCore();
                repYearConcept.Id = "REP_YEAR_START";
                repYearConcept.AddName("en", "Reporting Year Start");
                csMut.AddItem(repYearConcept);
            }

            objects.AddIdentifiable(csMut.ImmutableInstance);

            return objects;
        }

        /// <summary> 
        /// Creates an ISDMXObjects object containing the DSD for the World Developmentg Indicator (WDI) in agency "TEST" 
        /// </summary> 
        public static ISdmxObjects CreateWorldDevelopmentIndicatorsDsd()
        {
            string testAcy = "TEST";

            ICodelistObject refAreaCodelist = TestCodelistUtil.GenerateCodelist(testAcy, "CL_REF_AREA", "1.0", "FR France", "UK United Kingdom");
            ICodelistObject seriesCodelist = TestCodelistUtil.GenerateCodelist(testAcy, "CL_SERIES_WDI", "1.0", "AG_AGR_TRAC_NO Agricultural machinery,tractors", "AG_CON_FERT_PT_ZS Fertilizer consumption");
            ICodelistObject unitMultiplierCodelist = TestCodelistUtil.GenerateCodelist(testAcy, "CL_UNIT_MULT_WDI", "1.0", "0 Units", "1 Tens", "2 Hundreds");

            // Create a DSD with coded dimensions
            IDictionary<string, ICodelistObject> representations = new Dictionary<string, ICodelistObject>();
            representations.Add("REF_AREA", refAreaCodelist);
            representations.Add("SERIES", seriesCodelist);
            representations.Add("UNIT_MULT", unitMultiplierCodelist);

            IList<string> dimensions = new List<string>() { "FREQ", "REF_AREA", "SERIES" };
            IList<string> serAttr = new List<string>();
            IList<string> obsAttr = new List<string>() { "UNIT_MULT" };

            IDataStructureSuperObject dsdSb = TestDSDUtil.GenerateDSDSb(dimensions, serAttr, true, true, obsAttr, null, representations, testAcy, "TEST_WDI_DSD", "1.0");
            IDataStructureObject dsd = dsdSb.BuiltFrom;

            // Make REF_AREA, SERIES and UNIT_MULT coded dimensions
            IDataStructureMutableObject dsdMut = new DataStructureMutableCore(dsd);
            CreateCodedComponent(dsdMut.GetDimension("REF_AREA"), refAreaCodelist);
            CreateCodedComponent(dsdMut.GetDimension("SERIES"), seriesCodelist);
            CreateCodedComponent(dsdMut.GetAttribute("UNIT_MULT"), unitMultiplierCodelist);

            dsd = dsdMut.ImmutableInstance;

            ISdmxObjects objects = new SdmxObjectsImpl();
            objects.AddIdentifiable(refAreaCodelist);
            objects.AddIdentifiable(seriesCodelist);
            objects.AddIdentifiable(unitMultiplierCodelist);
            objects.AddIdentifiable(dsd);

            IConceptSchemeObject testConceptScheme = TestConceptUtil.GenerateConceptSchemeWithProps(testAcy, "CS", "1.0", "_NAME", new string[] { "T", "FREQ", "SERIES", "REF_AREA", "UNIT_MULT" });
            IConceptSchemeMutableObject csMut = new ConceptSchemeMutableCore(testConceptScheme);

            // Make OBS VALUE have a clearer name
            ConceptMutableCore obsValueConcept = new ConceptMutableCore();
            obsValueConcept.Id = "OBS_VALUE";
            obsValueConcept.AddName("en", "Observation Value");
            csMut.AddItem(obsValueConcept);
            objects.AddIdentifiable(csMut.ImmutableInstance);

            return objects;
        }

        /// <summary> 
        /// Creates an ISDMXObjects object containing the DSD for the World Developmentg Indicator (WDI) in agency "TEST" 
        /// </summary> 
        public static ISdmxObjects CreateDiscriminatedUnionStructures()
        {
            IDataStructureObject dsd = TestDSDUtil.GenerateDSD(true, true, "FREQ:CL", "REF_AREA:CL", "INDICATOR:CL");
            string testAcy = dsd.AgencyId;

            ICodelistObject refAreaCodelist = TestCodelistUtil.GenerateCodelist("ACY", "CL_REF_AREA", "1.0", "FR France", "UK United Kingdom");
            //TODO SDMXCORE
            //ICodelistObject clIndicator 			= TestCodelistInstances.GetCodelistForDiscriminatedUnion("ACY");
            ICodelistObject clFreq = TestCodelistInstances.GetFREQ("ACY", "1.0");


            ISdmxObjects objects = new SdmxObjectsImpl();
            objects.AddIdentifiable(refAreaCodelist);
            //        objects.AddIdentifiable(clIndicator);
            objects.AddIdentifiable(clFreq);
            objects.AddIdentifiable(dsd);

            IConceptSchemeObject testConceptScheme = TestConceptUtil.GenerateConceptSchemeWithProps(testAcy, "CS", "1.0", "_NAME", new string[] { "T", "OBS_VALUE", "FREQ", "INDICATOR", "REF_AREA" });
            objects.AddIdentifiable(testConceptScheme);
            return objects;
        }

        private static void CreateCodedComponent(IComponentMutableObject comp, ICodelistObject aCodelist)
        {
            IRepresentationMutableObject representation = new RepresentationMutableCore();
            representation.Representation = aCodelist.AsReference;
            comp.Representation = representation;
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public static IDataStructureObject GetDSDWithMultiValueSeriesAttribute()
        {
            IDataStructureObject dsd = TestDSDUtil.GenerateDSD(true, true, "FREQ", "REF_AREA", "INDICATOR");
            IDataStructureMutableObject mutable = dsd.MutableInstance;
            IAttributeMutableObject attribute = TestDSDUtil.CreateAttribute(mutable, "AT1_STRING_MULTI", AttributeAttachmentLevel.DimensionGroup);
            TestDSDUtil.CreateAttribute(mutable, "AT1_STRING_MULTI", AttributeAttachmentLevel.DimensionGroup).SetRepresentationMinOccurs(2).SetRepresentationMaxOccurs(4);
            return mutable.ImmutableInstance;
        }

        /// <summary> 
        /// </summary> 
        public static IDataStructureObject GetDSDWithMultiValueObsAttribute()
        {
            IDataStructureObject dsd = TestDSDUtil.GenerateDSD(true, true, "FREQ", "REF_AREA", "INDICATOR");
            IDataStructureMutableObject mutable = dsd.MutableInstance;
            TestDSDUtil.CreateAttribute(mutable, "AT1_STRING_MULTI", AttributeAttachmentLevel.Observation).SetRepresentationMinOccurs(2).SetRepresentationMaxOccurs(4);
            return mutable.ImmutableInstance;
        }

        /// <summary> 
        /// </summary>
        /// <returns>
        /// DSD with series and obs attributes containing min and max occurs, html content, and multilingual content 
        /// </returns> 
        public static IDataStructureObject GetDSDWithComplexContent()
        {
            IDataStructureObject dsd = TestDSDUtil.GenerateDSD(true, true, "FREQ", "REF_AREA", "INDICATOR");
            IDataStructureMutableObject mutable = dsd.MutableInstance;

            ITextFormatMutableObject multilingualString = new TextFormatMutableCore();
            multilingualString.Multilingual = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.True);


            ITextFormatMutableObject multilingualXHTML = new TextFormatMutableCore();
            multilingualXHTML.Multilingual = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.True);
            multilingualXHTML.TextType = TextType.GetFromEnum(TextEnumType.Xhtml);
            TestDSDUtil.CreateAttribute(mutable, "AT1_MULTILINGUAL_STRING_MULTI", AttributeAttachmentLevel.DimensionGroup)
                .SetMandatory(true).SetRepresentationMinOccurs(2).SetRepresentationMaxOccurs(4).SetTextFormat(multilingualString);
            TestDSDUtil.CreateAttribute(mutable, "AT1_MULTILINGUAL_XHTML_MULTI", AttributeAttachmentLevel.DimensionGroup)
                .SetRepresentationMinOccurs(2).SetRepresentationMaxOccurs(4).SetTextFormat(multilingualXHTML);
            TestDSDUtil.CreateAttribute(mutable, "AT1_STRING_MULTI", AttributeAttachmentLevel.DimensionGroup)
                .SetRepresentationMinOccurs(2).SetRepresentationMaxOccurs(4);
            TestDSDUtil.CreateAttribute(mutable, "AT2_DOUBLE_MULTI", AttributeAttachmentLevel.DimensionGroup)
                .SetRepresentationMinOccurs(1).SetRepresentationMaxOccurs(2).SetTextType(TextType.GetFromEnum(TextEnumType.Double));
            TestDSDUtil.CreateMeasure(mutable, "M1_INTEGER_MULTI").SetRepresentationMinOccurs(2).SetRepresentationMaxOccurs(3)
                .SetTextType(TextType.GetFromEnum(TextEnumType.Integer));
            TestDSDUtil.CreateMeasure(mutable, "M1_HTML_1_MULTI").SetRepresentationMinOccurs(2).SetRepresentationMaxOccurs(3)
                .SetTextType(TextType.GetFromEnum(TextEnumType.Xhtml));
            TestDSDUtil.CreateMeasure(mutable, "M1_HTML_2_SINGLE").SetTextType(TextType.GetFromEnum(TextEnumType.Xhtml));
            //TODO SDMXCORE VALUELIST (without it, default TextFormat string is created which doesnt exist in json)
            TestDSDUtil.CreateAttribute(mutable, "ATT_OBS_CODED_MULTI", AttributeAttachmentLevel.Observation)
                .SetRepresentationMinOccurs(0).SetRepresentationMaxOccurs(2);
            //.SetEnumeration(new StructureReferenceImpl("ACY", "VL_UNIT", "1.0", SDMX_STRUCTURE_TYPE.VALUE_LIST));

            return mutable.ImmutableInstance;
        }


        /// <summary> 
        /// </summary>
        /// <returns>
        /// DSD with a reference to an MSD 
        /// </returns> 
        public static IDataStructureObject GetDSDWithMSD()
        {
            IDataStructureObject dsd = TestDSDUtil.GenerateDSD(true, true, "FREQ", "REF_AREA", "INDICATOR");

            IDataStructureMutableObject mutable = dsd.MutableInstance;

            mutable.MetadataStructure = new StructureReferenceImpl(dsd.AgencyId, "METADATA", "1.0", SdmxStructureEnumType.Msd);
            return mutable.ImmutableInstance;
        }

        /// <summary> 
        /// </summary>
        /// <returns>
        /// DSD with Geo Grid Dimension and required reference 
        /// </returns> 
        public static IDataStructureObject GetDSDWithGeoGrid()
        {
            IDataStructureObject dsd = TestDSDUtil.GenerateDSD(true, true, "FREQ", "REF_AREA", "INDICATOR");

            IDataStructureMutableObject mutable = dsd.MutableInstance;

            foreach (IDimensionMutableObject dim in mutable.Dimensions)
            {
                if (dim.Id.Equals("TIME_PERIOD"))
                {
                    continue;
                }
                IRepresentationMutableObject rep = new RepresentationMutableCore();
                rep.Representation = new StructureReferenceImpl(dsd.AgencyId, "CL_" + dim.Id, "1.0", SdmxStructureEnumType.CodeList);
                //			if(dim.Id.Equals("REF_AREA")) {
                //				ITextFormatMutable tf = new TextFormatMutableCore();
                //				tf.TextType = TEXT_TYPE.GEO;
                //				rep.TextFormat = tf;
                //			}
                dim.Representation = rep;
            }

            return mutable.ImmutableInstance;
        }


        /// <summary> 
        /// DSD with Dimensions FREQ, REF_AREA, INDICATOR, TIME_PERIOD, OBS_VALUE - and optional series and measure attributees 
        /// </summary>
        /// <param name=" includeSeriesAttributes"> if true creates TITLE and COLLECTION 
        /// </param>
        /// <param name=" includeMeasureAttributes"> if true additional measures is created for BIRTHS with attributes UNIT, OBS_CONF, OBS_STATUS, SOURCE 
        /// </param>
        /// <returns>
        /// DSD with coded and uncoded 
        /// </returns> 
        public static IDataStructureObject GenerateMixedBag(bool includeSeriesAttributes, bool includeMeasureAttributes)
        {
            IDataStructureObject dsd = TestDSDUtil.GenerateDSD(true, true, "FREQ", "REF_AREA", "INDICATOR");

            IDataStructureMutableObject mutable = dsd.MutableInstance;
            if (includeSeriesAttributes)
            {
                TestDSDUtil.CreateAttribute(mutable, "TITLE", AttributeAttachmentLevel.DimensionGroup)
                    .SetTextType(TextType.GetFromEnum(TextEnumType.String)).MaxLength = 50;
                TestDSDUtil.CreateAttribute(mutable, "COLLECTION", AttributeAttachmentLevel.DimensionGroup)
                    .SetEnumeration(new StructureReferenceImpl("ACY", "CL_COLLECTION", "1.0", SdmxStructureEnumType.CodeList));
            }
            if (includeMeasureAttributes)
            {
                //Mandatory
                IAttributeMutableObject source = TestDSDUtil.CreateAttribute(mutable, "SOURCE", AttributeAttachmentLevel.Observation);
                source.AddMeasureRelationship("OBS_VALUE");
                source.SetMandatory(true).SetRepresentationMinOccurs(1).SetRepresentationMaxOccurs(2);
                source.SetTextType(TextType.GetFromEnum(TextEnumType.Alphanumeric)).MaxLength = 5;

                TestDSDUtil.CreateMeasure(mutable, "BIRTHS");

                //TODO SDMXCORE VALUELIST
                TestDSDUtil.CreateAttribute(mutable, "UNIT", AttributeAttachmentLevel.Observation);//.SetEnumeration(new StructureReferenceImpl("ACY", "VL_UNIT", "1.0", SDMX_STRUCTURE_TYPE.VALUE_LIST));
                TestDSDUtil.CreateAttribute(mutable, "OBS_CONF", AttributeAttachmentLevel.Observation)
                    .SetEnumeration(new StructureReferenceImpl("ACY", "CL_OBS_CONF", "1.0", SdmxStructureEnumType.CodeList));
                TestDSDUtil.CreateAttribute(mutable, "OBS_STATUS", AttributeAttachmentLevel.Observation)
                    .SetEnumeration(new StructureReferenceImpl("ACY", "CL_OBS_STATUS", "1.0", SdmxStructureEnumType.CodeList));
            }
            dsd = mutable.ImmutableInstance;

            foreach (IDimensionMutableObject dim in mutable.Dimensions)
            {
                if (dim.Id.Equals("TIME_PERIOD"))
                {
                    continue;
                }
                IRepresentationMutableObject rep = new RepresentationMutableCore();
                rep.Representation = new StructureReferenceImpl(dsd.AgencyId, "CL_" + dim.Id, "1.0", SdmxStructureEnumType.CodeList);
                //			if(dim.Id.Equals("REF_AREA")) {
                //				ITextFormatMutable tf = new TextFormatMutableCore();
                //				tf.TextType = TEXT_TYPE.GEO;
                //				rep.TextFormat = tf;
                //			}
                dim.Representation = rep;
            }
            return mutable.ImmutableInstance;
        }

        /// <summary> 
        /// This is essentially BIS_BOJ:FOF_RAW(1.0) 
        /// </summary>
        /// <returns>
        /// 
        /// </returns> 
        public static IDataStructureObject GenerateDSDWithNoTimeSeries()
        {
            return TestDSDUtil.GenerateDSD(false, true, "INTERNAL_CODE", "FREQ", "FOF_PERIOD");
        }

        public static IDataStructureObject SetMandatoryMeasures(IDataStructureObject dsd, params string[] measureIds)
        {
            if (measureIds == null || measureIds.Length < 1)
            {
                return dsd;
            }

            IDataStructureMutableObject mutable = dsd.MutableInstance;
            IList<IMeasureMutableObject> measures = mutable.Measures;
            foreach (IMeasureMutableObject ImeasureMutable in measures)
            {
                foreach (string anId in measureIds)
                {
                    if (ImeasureMutable.Id.Equals(anId))
                    {
                        ImeasureMutable.SetMandatory(true);
                        break;
                    }
                }
            }
            return mutable.ImmutableInstance;
        }

        /// <summary> 
        /// DSD with a reference to an MSD for additional attributes 
        /// </summary> 
        public static IDataStructureObject GenerateWithMSDReference(string msdId)
        {
            IDataStructureObject dsd = TestDSDUtil.GenerateDSD(true, true, "FREQ", "REF_AREA", "INDICATOR");
            IDataStructureMutableObject mutable = dsd.MutableInstance;
            mutable.MetadataStructure = new StructureReferenceImpl("ACY", msdId, "1.0.0", SdmxStructureEnumType.Msd);

            return mutable.ImmutableInstance;
        }


        /// <summary> 
        /// Generates the references structures for the DSD, rules are: 
        /// 1. If a Component or Concept has a coded representation, a codelist will be created 
        /// 2. If a Component has a representation of GEO and no codelist reference, a IGeographicCodelistObject will be created and linked to the Concept 
        /// 3. All Agencies will be created based on the DSD agency and any agencies in references, agency Ids can include sub-agencies using the SDMX.ESAT syntax 
        /// 
        /// </summary>
        /// <returns>
        /// 
        /// </returns> 
        public static ISdmxObjects GenerateReferences(IDataStructureObject dsd, bool includeDSD)
        {
            ISdmxObjects objects = new SdmxObjectsImpl();
            GenerateReferences(dsd, objects, includeDSD);
            return objects;
        }

        /// <summary> 
        /// Component Values; 
        /// FREQ:      A, S, T, Q, M, W, D, H, I 
        /// INDICATOR: P1_IND_0, P2_IND_1, P3_IND_2, P1_IND_4,params  P2_IND_5.[]and so on 
        /// COMPANY:   GOO=Google, QUO=Quora, Inc., IBM=IBM, MANN=Mann and Son's, JAJ=Johnson & Johnson 
        /// 
        /// 
        /// All Other Coded Dimensions 
        /// AA, AB, AC, AD, AE, AF, AG, AH, AI 
        /// 
        /// 
        /// 
        /// </summary>
        /// <param name=" dsd"> 
        /// </param>
        /// <param name=" objects"> 
        /// </param>
        /// <param name=" includeDSD"> 
        /// </param> 
        public static ISdmxObjects GenerateReferences(IDataStructureObject dsd, ISdmxObjects objects, bool includeDSD)
        {
            if (objects == null)
            {
                objects = new SdmxObjectsImpl();
            }
            IDictionary<string, ISet<string>> csMap = new Dictionary<string, ISet<string>>();  //Conecpt Scheme to concept ids
            IDictionary<string, ISet<string>> geoCodedConceptMap = new Dictionary<string, ISet<string>>();  //GeoCoded Concepts

            IDictionary<string, ISet<string>> agenyMap = new Dictionary<string, ISet<string>>();  //parnet id to agency id, parent default SDMX

            //AGENCEIS
            AddAgencyIdToMap(dsd.AgencyId, agenyMap);
            foreach (ICrossReference xsRef in dsd.CrossReferences)
            {
                AddAgencyIdToMap(xsRef.AgencyId, agenyMap);
            }

            //CONCEPTS
            foreach (IComponent comp in dsd.Components)
            {
                string csUrn = comp.ConceptRef.MaintainableUrn.AbsoluteUri;
                CollectionUtil.AddToMappedSet(csMap, csUrn, comp.ConceptRef.FullId);
                if (comp.TextType == TextEnumType.GeospatialInformation && comp.EnumeratedRepresentation == null)
                {
                    CollectionUtil.AddToMappedSet(geoCodedConceptMap, csUrn, comp.ConceptRef.FullId);
                }
            }

            if (includeDSD)
            {
                objects.AddIdentifiable(dsd);
            }
            foreach (string csUrn in csMap.Keys)
            {
                IStructureReference sRef = new StructureReferenceImpl(csUrn);
                ISet<string> conceptIds = csMap[csUrn];
                IConceptSchemeObject cs = TestConceptUtil.GenerateConceptSchemeWithProps(sRef.AgencyId, sRef.MaintainableId, sRef.Version, "-name", conceptIds.ToArray());

                ISet<string> geoCodedConcepts = geoCodedConceptMap[csUrn];
                if (geoCodedConcepts != null)
                {
                    IConceptSchemeMutableObject csMut = cs.MutableInstance;
                    foreach (string geoConcept in geoCodedConcepts)
                    {
                        IStructureReference clRef = new StructureReferenceImpl(cs.AgencyId, "CL_" + geoConcept, "1.0.0", SdmxStructureEnumType.CodeList);
                        IRepresentationMutableObject rep = new RepresentationMutableCore();
                        rep.Representation = clRef;
                        ITextFormatMutableObject tf = new TextFormatMutableCore();
                        tf.TextType = TextType.GetFromEnum(TextEnumType.GeospatialInformation);
                        rep.TextFormat = tf;
                        csMut.GetItemById(geoConcept).CoreRepresentation = rep;

                        //TODO SDMXCORE GEO
                        //                    ICodelistObject cl = TestGeographicCodelistUtil.GenerateCodelist(clRef.AgencyId, clRef.MaintainableId, clRef.Version, GetCodes(clRef.MaintainableId));
                        //                    objects.AddIdentifiable(cl);
                    }
                    cs = csMut.ImmutableInstance;
                }

                objects.AddIdentifiable(cs);
            }

            foreach (IComponent comp in dsd.Components)
            {
                if (comp.EnumeratedRepresentation != null)
                {
                    ICrossReference enumRef = comp.EnumeratedRepresentation;

                    ICodelistObject cl = null;
                    if (comp.Id.Equals("REF_AREA"))
                    {
                        //TODO SDMXCORE GEO
                        //cl = TestGeographicCodelistUtil.GenerateCodelist(enumRef.AgencyId, enumRef.MaintainableId, enumRef.Version, GeoCodes());
                    }
                    else if (enumRef.MaintainableStructureEnumType == SdmxStructureEnumType.ValueList)
                    {
                        //TODO SDMXCORE VALUELIST
                        //cl = TestValueListUtil.GenerateValuelist(enumRef.AgencyId, enumRef.MaintainableId, enumRef.Version, "$ Dollar", "£ Pound Sterling", "% Percent");
                    }
                    else
                    {
                        cl = TestCodelistUtil.GenerateCodelist(enumRef.AgencyId, enumRef.MaintainableId, enumRef.Version, GetCodes(enumRef.MaintainableId));
                    }
                    objects.AddIdentifiable(cl);
                }
            }
            foreach (string acySchemeId in agenyMap.Keys)
            {
                objects.AddIdentifiable(TestAgencySchemeUtil.GenerateAgencyScheme(acySchemeId, agenyMap[acySchemeId].ToArray()));
            }

            return objects;
        }

        private static string[] GeoCodes()
        {
            return new string[] { "EUR Europe:geofeatures", "EUR.Uk United Kingdom:geofeatures", "EUR.Fr France:geofeatures", "EUR.De Germany:geofeatures", "EUR.Es Spain:geofeatures", "ASIA Asia:geofeatures", "ASIA.Ch China:geofeatures", "ASIA.Th:geofeatures" };
        }

        private static string[] GetCodes(string codelistId)
        {
            switch (codelistId)
            {
                case "CL_FREQ": return new string[] { "A Annual", "S Semi-Annual", "T Tri-Annual", "Q Quarterly", "M Monthly", "W Weekly", "D Daily", "H Hourly", "I Date Time" };
                case "CL_DATA_PROVIDER": return new string[] { "ONS Office for National Statistics", "INSEE National Institute of Statistics and Economic Studies", "BOE  Bank of England" };
                case "CL_INDICATOR":
                    int max = 100;
                    string[] s = new string[max];
                    string[] prefix = new string[] { "P1", "P2", "P3" };

                    for (int i = 0, j = 0; i < max; i++, j++)
                    {
                        if (j >= prefix.Length)
                        {
                            j = 0;
                        }
                        s[i] = prefix[j] + "_IND_" + i;
                    }
                    return s;
                case "CL_COMPANY":
                    return new string[] { "GOO Google", "QUO Quora, Inc", "IBM Ibm", "MANN Mann and Son's", "JAJ Johnson & Johnson" };
            }
            return new string[] { "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI" };
        }

        private static void AddAgencyIdToMap(string agencyId, IDictionary<string, ISet<string>> agenyMap)
        {
            string[] agencySplit = agencyId.Split('.');
            agencyId = agencySplit[agencySplit.Length - 1];
            string parent = agencySplit.Length > 1 ? agencyId.Substring(0, agencyId.LastIndexOf('.')) : "SDMX";
            CollectionUtil.AddToMappedSet(agenyMap, parent, agencyId);
        }
    }
}