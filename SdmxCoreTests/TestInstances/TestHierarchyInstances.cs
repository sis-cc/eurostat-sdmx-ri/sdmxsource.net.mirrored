// -----------------------------------------------------------------------
// <copyright file="TestHierarchyInstances.cs" company="EUROSTAT">
//   Date Created : 2024-03-05
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.TestInstances
{
    using System;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using SdmxCoreTests.TestUtils;

    public class TestHierarchyInstances
    {
        /// <summary>
        /// Hierarchy with multiple levels
        /// </summary>
        /// <returns></returns>
        public static IHierarchyV30 GetHierarchy()
        {
            var hierarchy = new HierarchyMutableCoreV30()
            {
                AgencyId = "EXAMPLE",
                Id = "HCL_AGE",
                
            };

            hierarchy.SetFormalLevels(false);
            hierarchy.AddName("en", "Example hierarchy using codes from CL_AGE");
            var level3 = new LevelMutableCore()
            {
                Id = "LEV3"
            };
            level3.AddName("en", "LEV3");
            var level2 = new LevelMutableCore()
            {
                ChildLevel = level3,
                Id = "LEV2",
            };
            level2.AddName("en", "Level 2");
            var level1 = new LevelMutableCore()
            {
                ChildLevel = level2,
                Id = "LEV1"
            };
            level1.AddName("en", "Root level");
            hierarchy.SetChildLevel(level1);

            var hierarchicalCodeY = new CodeRefMutableCore()
            {
                Id = "Y"
            };
            hierarchicalCodeY.CodeReference = new StructureReferenceImpl("SDMX", "CL_AGE", "1.0", Org.Sdmxsource.Sdmx.Api.Constants.SdmxStructureEnumType.Code,"Y");
            
            var hierarchicalCodeM = new CodeRefMutableCore()
            {
                Id = "M"
            };
            hierarchicalCodeM.CodeReference = new StructureReferenceImpl("SDMX", "CL_AGE", "1.0", Org.Sdmxsource.Sdmx.Api.Constants.SdmxStructureEnumType.Code,"M");
            hierarchicalCodeM.LevelReference = "LEV2";
            var hierarchicalCodeS = new CodeRefMutableCore()
            {
                Id = "S"
            };
            hierarchicalCodeS.CodeReference = new StructureReferenceImpl("SDMX", "CL_AGE", "1.0", Org.Sdmxsource.Sdmx.Api.Constants.SdmxStructureEnumType.Code,"M");
            hierarchicalCodeS.LevelReference = "LEV2";
            hierarchicalCodeY.CodeRefs.Add(hierarchicalCodeM);
            hierarchicalCodeY.CodeRefs.Add(hierarchicalCodeS);

            hierarchicalCodeY.LevelReference = "LEV1";
            hierarchy.AddHierarchicalCodeBean(hierarchicalCodeY);

            return hierarchy.ImmutableInstance;
        }

        internal static IHierarchyAssociation GetHierarchyAssociation()
        {
            var hierarchyAssociation = new HierarchyAssociationMutableCore()
            {
                AgencyId = "EXAMPLE", 
                Id = "HA_DIMENSION_EXAMPLE",
            };
            hierarchyAssociation.AddName("en", "Example hiearchical association that links to a dimension in a dataflow");
            hierarchyAssociation.SetHierarchyRef(new StructureReferenceImpl("EXAMPLE", "HCL_AGE","1.0",Org.Sdmxsource.Sdmx.Api.Constants.SdmxStructureEnumType.HierarchyV30));
            hierarchyAssociation.SetLinkedStructure(new StructureReferenceImpl("EXAMPLE", "DSD_EX", "1.0",Org.Sdmxsource.Sdmx.Api.Constants.SdmxStructureEnumType.Dimension, "AGE"));
            hierarchyAssociation.SetContextStructure(new StructureReferenceImpl("EXAMPLE", "DF_EX", "1.0",Org.Sdmxsource.Sdmx.Api.Constants.SdmxStructureEnumType.Dataflow));
            return hierarchyAssociation.ImmutableInstance;
        }
    }
}
