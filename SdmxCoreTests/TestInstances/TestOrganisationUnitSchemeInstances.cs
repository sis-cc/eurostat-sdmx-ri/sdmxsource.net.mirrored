// -----------------------------------------------------------------------
// <copyright file="TestOrganisationUnitSchemeInstances.cs" company="EUROSTAT">
//   Date Created : 2023-11-13
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.TestInstances
{
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using SdmxCoreTests.TestUtils;

    public class TestOrganisationUnitSchemeInstances
    {       
        /// <summary>
        /// return simple scheme with UNIT1, 2 and 3
        /// </summary>
        /// <returns></returns>
        public static IOrganisationUnitSchemeObject GetOrgUnitScheme()
        {
            return TestOrganisationUnitSchemeUtil.GenerateOrganisationUnitScheme("ACY", "OUS", "1.0", "UNIT1 Unit One",
                    "UNIT2 Unit Two",
                    "UNIT3 Unit Three");
        }
      
        /// <summary>
        /// return simple scheme with UNIT1, 2 and 3
        /// </summary>
        /// <returns></returns>
        public static IOrganisationUnitSchemeObject GetOrgUnitSchemeWithHierarchy()
        {
            return TestOrganisationUnitSchemeUtil.GenerateOrganisationUnitScheme("ACY", "OUS", "1.0", "UNIT1 Unit One",
                "UNIT1.SUB1 Sub Unit One",
                "SUB1.SUB2 Sub Unit One.One",
                "UNIT2 Unit Two",
                "UNIT2.SUB Sub Unit Two",
                "UNIT3 Unit Three");
        }
    }
}
