// -----------------------------------------------------------------------
// <copyright file="TestMetadataProviderSchemeInstances.cs" company="EUROSTAT">
//   Date Created : 2024-04-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

namespace SdmxCoreTests.TestInstances
{
    public class TestMetadataProviderSchemeInstances
    {
        public static IMetadataProviderScheme GetSimple()
        {
            IMetadataProviderSchemeMutable metadataProviderScheme = new MetadataProviderSchemeMutableCore();
            metadataProviderScheme.AgencyId = "EXAMPLE";
            metadataProviderScheme.Id = "METADATA_PROVIDERS";
            metadataProviderScheme.AddName("en", "Test metadata providers");
            metadataProviderScheme.CreateItem("TEST1", "Test provider");
            return metadataProviderScheme.ImmutableInstance;
        }
    }
}
