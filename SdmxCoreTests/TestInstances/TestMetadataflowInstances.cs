

namespace SdmxCoreTests.TestInstances
{
    using Io.Sdmx.Im.Util.Model;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    public class TestMetadataflowInstances
    {
        /// <summary>
        ///   Generates a metadataflow with a single target of type 'type' all other target properties are wildcarded
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static IMetadataFlow SingleWildcardTargetOfType(SdmxStructureType type)
        {
            return TestMetadataflowUtil.GenerateMetaDataflow("ACY", "MDF", "1.0.0", new StructureReferenceImpl(type));
        }

        public static IMetadataFlow SingleAbsoluteTargetOfType(SdmxStructureType type)
        {
            return TestMetadataflowUtil.GenerateMetaDataflow("ACY", "MDF", "1.0.0", new StructureReferenceImpl("ACY", "TGT", "1.0.0", type));
        }

        public static IMetadataFlow MultipleTargets()
        {
            IStructureReference ref1 = new StructureReferenceImpl("ACY", "DF1", "1.0.0", SdmxStructureEnumType.Dataflow);
            IStructureReference ref2 = new StructureReferenceImpl("ACY", "CL_REF_AREA", "1.0.0", SdmxStructureEnumType.Code, "UK");
            IStructureReference ref3 = new StructureReferenceImpl("ACY", "CS", "1.0.0", SdmxStructureEnumType.Concept, "INDICATOR");
            return TestMetadataflowUtil.GenerateMetaDataflow("ACY", "MDF", "1.0.0", ref1, ref2, ref3);
        }
    }
}
