// -----------------------------------------------------------------------
// <copyright file="TestCodelistInstances.cs" company="EUROSTAT">
//   Date Created : 2023-05-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.TestInstances
{
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Codelist;
    using System.Linq;
    using Org.Sdmxsource.Util.Extensions;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using SdmxCoreTests.TestUtils;

    public class TestCodelistInstances
    {
        /// <summary> 
        /// </summary>
        /// <returns>
        /// a raw codelist (not resolved) which inherits from another codelist 
        /// </returns> 
        public static ICodelistObject GetCodelistWithInheritenceRAW(bool exclusiveRules)
        {
            ICodelistObject cl = TestCodelistUtil.GenerateCodelist("SDMX", "CL_REF_ARAEA", "1.0.2", "UK", "FR", "DE", "ASIA", "TH", "CN", "IND");
            ICodelistObject clEx = TestCodelistUtil.GenerateCodelist("SDMX", "CL_GENDER", "1.0.2", "ES", "FR");

            ICodelistMutableObject mutable = clEx.MutableInstance;
            ICodelistInheritanceRuleMutableObject rule = new CodelistInheritanceRuleMutableCore();
            rule.CodelistRef = cl.AsReference;
            rule.Prefix = "EX_";

            ICodeSelectionMutableObject selection = new CodeSelectionMutableCore();
            selection.IsInclusive = !exclusiveRules;
            selection.MemberValues.AddAll(new string[] { "%US", "%EU", "UK", "FR" }.Select(s =>
            {
                IMemberValueMutableObject m = new MemberValueMutableCore();
                m.Value = s;
                m.Cascade = CascadeSelection.False;
                return m;
            }));
            selection.MemberValues.AddAll(new string[] { "ASIA" }.Select(s =>
            {
                IMemberValueMutableObject m = new MemberValueMutableCore();
                m.Value = s;
                m.Cascade = CascadeSelection.True;
                return m;
            }));

            rule.CodeSelection = selection;
            mutable.CodelistExtensions.Add(rule);
            return mutable.ImmutableInstance;
        }

        /// <summary> 
        /// </summary>
        /// <returns>
        /// an extended Codelist (NOT Raw) of Indicators, each codelist which is extended includes a Prefix to ensure the uniqueness of Code 
        /// </returns> 
        //TODO SDMXCORE
        /*
        public static ICodelistObject GetCodelistForDiscriminatedUnion(string acy) {
            ICodelistObject clIndicator1 = TestCodelistUtil.GenerateCodelist(acy, "CL_INDICATOR_1",     "1.0", "IA Indicator A ex1", "IB Indicator B ex1");
            ICodelistObject clIndicator2 = TestCodelistUtil.GenerateCodelist(acy, "CL_INDICATOR_2",     "1.0", "IA Indicator A ex2", "IB Indicator B ex2");
            ICodelistObject clIndicator3 = TestCodelistUtil.GenerateCodelist(acy, "CL_INDICATOR_3",     "1.0", "IA Indicator A ex3", "IB Indicator B ex3");

            IList<ICodelistObject> extendedCodelists  = new ArrayList<>();
            extendedCodelists.Add(clIndicator1);
            extendedCodelists.Add(clIndicator2);
            extendedCodelists.Add(clIndicator3);

            ICodelistMutable mutable = new CodelistMutableCore();
            TestMaintainableUtil.MaintainableProperties = mutable, acy, "CL_INDICATOR", "1.0";

            ICodelistInheritanceRuleMutableObject rule = new CodelistInheritanceRuleMutableObject();
            rule.CodelistRef = clIndicator1.AsReference();
            rule.Prefix = "EX1_";
            mutable.AddInheritenceRules(rule);

            rule = new CodelistInheritanceRuleMutableObject();
            rule.CodelistRef = clIndicator2.AsReference();
            rule.Prefix = "EX2_";
            mutable.AddInheritenceRules(rule);

            rule = new CodelistInheritanceRuleMutableObject();
            rule.CodelistRef = clIndicator3.AsReference();
            rule.Prefix = "EX3_";
            mutable.AddInheritenceRules(rule);
            return new CombinedCodelistObjectImpl(mutable.ImmutableInstance, extendedCodelists);
        }

         */

        /// <summary> 
        /// </summary>
        /// <returns>
        /// activity codelist - some codes have descriptions 
        /// </returns> 
        public static ICodelistObject GetActivityWithDescriptions()
        {
            ICodelistObject cl = TestCodelistUtil.GenerateCodelist("ESTAT", "CL_ACTIVITY", "1.0.2",
                "_T 	Total - All activities",
                "_X Unspecified",
                "_Z Not appliable",
                "A Agriculture, forestry and fishing",
                "A_B Total Primary sector");

            ICodelistMutableObject mutable = cl.MutableInstance;
            mutable.GetItemById("_T").AddDescription("en", "This aggregate includes all activities");
            mutable.GetItemById("A_B").AddDescription("en", "A+B");

            return mutable.ImmutableInstance;
        } 

        /// <summary> 
        /// </summary>
        /// <returns>
        /// freq codelist 
        /// </returns> 
        public static ICodelistObject GetFREQ(string agecyId, string version)
        {
            return TestCodelistUtil.GenerateCodelist(agecyId, "CL_FREQ", version, "A Annual", "S Semi-Annual", "T Tri-Annual", "Q Quarterly", "M Monthly", "W Weekly", "D Daily", "H Hourly", "I Date Time");
        }

        /// <summary> 
        /// </summary>
        /// <returns>
        /// sex codelist - total parent, child codes 
        /// </returns> 
        public static ICodelistObject GetSexWithHierarchy()
        {
            ICodelistObject cl = TestCodelistUtil.GenerateCodelist("ESTAT", "CL_ACTIVITY", "1.0.2",
                "T Total",
                "M Male",
                "F Female",
                "_O Other");

            ICodelistMutableObject mutable = cl.MutableInstance;
            mutable.GetItemById("M").ParentCode = "T";
            mutable.GetItemById("F").ParentCode = "T";
            mutable.GetItemById("_O").ParentCode = "T";

            return mutable.ImmutableInstance;
        }

        /// <summary> 
        /// </summary>
        /// <returns>
        /// a codelist "CL_AREA" which inherits from 2 other codelists: CL_REF_AREA and CL_COUNTRY 
        /// </returns> 
        public static ICodelistObject GetMultipleExtendingCodelist()
        {
            ICodelistObject cl1 = TestCodelistUtil.GenerateCodelist("SDMX", "CL_REF_AREA", "1.0", "UK", "FR", "DE");
            ICodelistObject cl2 = TestCodelistUtil.GenerateCodelist("SDMX", "CL_COUNTRY", "1.0", "001", "002");
            ICodelistObject clEx = TestCodelistUtil.GenerateCodelist("SDMX", "CL_AREA", "1.0", "X", "Y");

            return TestCodelistUtil.ExtendCodelists(clEx, cl1, cl2);
        }
    }
}