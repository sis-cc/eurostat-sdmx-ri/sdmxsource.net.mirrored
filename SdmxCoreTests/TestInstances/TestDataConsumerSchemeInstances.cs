// -----------------------------------------------------------------------
// <copyright file="TestDataConsumerSchemeInstances.cs" company="EUROSTAT">
//   Date Created : 2023-11-28
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxCoreTests.TestInstances
{
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using SdmxCoreTests.TestUtils;

    public class TestDataConsumerSchemeInstances
    {
        /// <summary> 
        /// Builds a SDMX scheme with the sponsor organisations present, no contact details or descriptions) 
        /// </summary>
        /// <returns>
        /// 
        /// </returns> 
        public static IDataConsumerScheme GetDCScheme()
        {
            IDataConsumerScheme acy = TestDataConsumerSchemeUtil.GenerateDataProviderScheme("SDMX", "BIS", "ECB");
            IDataConsumerSchemeMutableObject mutable = acy.MutableInstance;
            mutable.GetItemById("BIS").AddName("en", "Bank for International Settlements");
            mutable.GetItemById("ECB").AddName("en", "European Central Bank");
            return mutable.ImmutableInstance;
        }


        /// <summary> 
        /// Builds a sub-agency scheme with department info: HR, IT, STATISTICS, MANAGEMENT 
        /// 
        /// Contact details are added 
        /// 
        /// </summary>
        /// <returns>
        /// 
        /// </returns> 
        public static IDataConsumerScheme GetSubScheme(string parentAgency)
        {
            IDataConsumerScheme acySch = TestDataConsumerSchemeUtil.GenerateDataProviderScheme(parentAgency, "HR", "IT", "STATISTICS", "MANAGEMENT");
            IDataConsumerSchemeMutableObject mutable = acySch.MutableInstance;

            IContactMutableObject contact = new ContactMutableObjectCore();
            contact.AddEmail("hr@gmail.com");
            contact.AddName(TextTypeWrapperMutableCore.GetInstance("en", "Alexie Plexie"));
            contact.AddDepartment(TextTypeWrapperMutableCore.GetInstance("en", "HR"));
            contact.AddDepartment(TextTypeWrapperMutableCore.GetInstance("fr", "Le HR"));

            contact.AddRole(TextTypeWrapperMutableCore.GetInstance("en", "Human Reources"));
            contact.AddRole(TextTypeWrapperMutableCore.GetInstance("fr", "Reources Le Human"));
            contact.AddFax("+44 123 333444");
            contact.AddTelephone("+44 1234 888777");
            contact.AddTelephone("01234 412345");

            TestAgencySchemeUtil.AddContactDetails(mutable, contact, "HR");

            mutable.GetItemById("IT").AddDescription("en", "In the basement");
            return mutable.ImmutableInstance;
        }

    }
}