// -----------------------------------------------------------------------
// <copyright file="TestDataProvisionInstances.cs" company="EUROSTAT">
//   Date Created : 2023-05-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------


using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using SdmxCore.Io.Sdmx.Im.Util.Model;
using SdmxCoreTests.TestUtils;

namespace SdmxCoreTests.TestInstances
{
    public class TestDataProvisionInstances
    {
        public static IProvisionAgreementObject GetEXRProvision(string providerId)
        {
            return TestDataProvisionUtil.GenerateProvision("ACY", providerId, new StructureReferenceImpl("ECB", "DF_EXR", "1.0.0", SdmxStructureEnumType.Dataflow));
        }

        /**
         * Generates a provision with all references built and added to the objects container
         *
         * @param providerId Id of the data provider
         * @param flowRef agency, id, version of dataflow
         * @param time has time dimenison
         * @param pm has primary measure
         * @param dimensionIds dimension ids, postfix with :CL or :VL for codelist/valuelist reference or any valid TEXT_TYPE, e.g. DimId:INTEGER, CL:REF_AREA, CL:FREQ, CL:DATA_PROVIDER, CL:INDICATOR will create codelists with specific codes
         * all other codelists/valuelists will be generated combination of characters @see TestDSDInstances.geoCodes and TestDSDInstances.getCodes
         * @return
         */
        public static ISdmxObjects GetProvisionWithDescendants(string providerId, IMaintainableRefObject flowRef, bool time, bool pm, params string[] dimensionIds)
        {

            IDataStructureObject dsd = TestDSDUtil.GenerateDSD(flowRef, time, pm, dimensionIds);
            ISdmxObjects objects = TestDSDInstances.GenerateReferences(dsd, true);

            IProvisionAgreementObject prov = TestDataProvisionUtil.GenerateProvision(flowRef.AgencyId, providerId,
                    new StructureReferenceImpl(flowRef.AgencyId, flowRef.MaintainableId, flowRef.Version, SdmxStructureEnumType.Dataflow));

            IDataProviderScheme dps = TestDataProviderSchemeUtil.GenerateDataProviderScheme(prov.DataproviderRef.AgencyId,new List<string> { prov.DataproviderRef.FullId });
            objects.AddIdentifiable(dps);

            objects.AddIdentifiable(TestDataflowUtil.GenerateDataflow((IStructureReference)dsd));
            objects.AddIdentifiable(prov);
            return objects;
        }
    }
}
