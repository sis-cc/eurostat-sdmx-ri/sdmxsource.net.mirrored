// -----------------------------------------------------------------------
// <copyright file="TestCategorySchemeInstances.cs" company="EUROSTAT">
//   Date Created : 2023-05-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.TestInstances
{
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using SdmxCoreTests.TestUtils;

    public class TestCategorySchemeInstances
    {
        public static ICategorySchemeObject GenerateFlatScheme()
        {
            return TestCategorySchemeUtil.GenerateCategoryScheme("ECB", "FLAT_CS", "1.0.1", "INTEREST_RATES", "EXCHANGE_RATES");
        }

        public static ICategorySchemeObject GenerateHierarchicalScheme()
        {
            return TestCategorySchemeUtil.GenerateCategoryScheme("SDMX", "STAT_SUBJECT_MATTER", "1.0.1",
                    "DEMO_SOCIAL_STAT Demographic and social statistics",
                    "DEMO_SOCIAL_STAT.LABOUR Labour",
                    "DEMO_SOCIAL_STAT.HEALTH Health",
                    "DEMO_SOCIAL_STAT.INCOME_CONSUMP Income and consumption",
                    "ECO_STAT Economic statistics",
                    "MACROECO_STAT Macroeconomic statistics",
                    "ECO_STAT.ECO_ACCOUNTS Economic accounts",
                    "ECO_STAT.SECTORAL_STAT Sectoral statistics",
                    "ECO_STAT.SECTORAL_STAT.AGRI_FOREST_FISH Agriculture, forestry, fisheries",
                    "ECO_STAT.SECTORAL_STAT.ENERGY Energy");
        }
    }
}