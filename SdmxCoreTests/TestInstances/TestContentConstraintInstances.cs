// -----------------------------------------------------------------------
// <copyright file="TestContentConstraintInstances.cs" company="EUROSTAT">
//   Date Created : 2023-11-11
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.TestInstances
{
    using Io.Sdmx.Im.Util.Model;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using SdmxCoreTests.Utils;

    public class TestContentConstraintInstances
    {
        /// <summary>
        ///  Generates a cube region with cascade on EUR and ASIA (with exclude root)
        /// </summary>
        /// <param name="isIncluded"></param>
        /// <returns></returns>
        public static IContentConstraintObject GetCascade(bool isIncluded)
        {
            IContentConstraintMutableObject tCons = TestContentConstraintUtil.Create("ACY", "CONS1", "1.0", new StructureReferenceImpl("ACY", "EXR", "1.0", SdmxStructureEnumType.Dsd));
            Dictionary<string, List<string>> restrictedValues = new Dictionary<string, List<string>>();
            CollectionUtil.AddToMappedList(restrictedValues, "REF_AREA", "EUR");
            IKeyValuesMutable kvs = TestContentConstraintUtil.AddCubeRegion(tCons, isIncluded, restrictedValues).KeyValues[0];
            kvs.AddCascade("ASIA");  //Asia is in cascade but not in the restricted list
            kvs.AddCascade("EUR");
            return tCons.ImmutableInstance;
        }

        /// <summary>
        /// Generates a cube region with cascade on EUR and ASIA (with exclude root)
        /// </summary>
        /// <param name="isIncluded"></param>
        /// <returns></returns>
        public static IContentConstraintObject GetCubeWithWildcard(bool isIncluded)
        {
            IContentConstraintMutableObject tCons = TestContentConstraintUtil.Create("ACY", "CONS1", "1.0", new StructureReferenceImpl("ACY", "EXR", "1.0", SdmxStructureEnumType.Dsd));
            Dictionary<string, List<string>> restrictedValues = new Dictionary<string, List<string>>();
            CollectionUtil.AddToMappedList(restrictedValues, "REF_AREA", "%UK%");
            _ = TestContentConstraintUtil.AddCubeRegion(tCons, isIncluded, restrictedValues).KeyValues[0];
            return tCons.ImmutableInstance;
        }

        /// <summary>
        ///  Generates multiple cube region, type actual
        /// </summary>
        /// <returns></returns>
        public static IContentConstraintObject GetActualConstraint()
        {
            IContentConstraintMutableObject tCons = GetMultipleCubes().MutableInstance;
            tCons.IsDefiningActualDataPresent = true;
            return tCons.ImmutableInstance;
        }

        /// <summary>
        /// Generates multiple cube region, one with wildcards
        /// </summary>
        /// <returns></returns>
        public static IContentConstraintObject GetMultipleCubes()
        {
            IContentConstraintMutableObject tCons = TestContentConstraintUtil.Create("ACY", "CONS2", "1.0", new StructureReferenceImpl("ACY", "EXR", "1.0", SdmxStructureEnumType.Dsd));
            Dictionary<string, List<string>> includeValues = new Dictionary<string, List<string>>();
            CollectionUtil.AddToMappedList(includeValues, "REF_AREA", "EUR");
            CollectionUtil.AddToMappedList(includeValues, "REF_AREA", "ASIA");

            Dictionary<string, List<string>> excludeValues = new Dictionary<string, List<string>>();
            CollectionUtil.AddToMappedList(excludeValues, "INDICATOR", "BIR%");
            TestContentConstraintUtil.AddCubeRegion(tCons, true, includeValues);
            TestContentConstraintUtil.AddCubeRegion(tCons, false, excludeValues);

            return tCons.ImmutableInstance;
        }

        /// <summary>
        ///  Generates a cube region with key values set to remove a prefix
        /// </summary>
        /// <returns></returns>
        public static IContentConstraintObject GetCubesRemovePrefix()
        {
            IContentConstraintMutableObject tCons = TestContentConstraintUtil.Create("ACY", "CONS2", "1.0", new StructureReferenceImpl("ACY", "EXR", "1.0", SdmxStructureEnumType.Dsd));
            Dictionary<string, List<string>> includeValues = new Dictionary<string, List<string>>();
            CollectionUtil.AddToMappedList(includeValues, "REF_AREA", "EUR");
            CollectionUtil.AddToMappedList(includeValues, "REF_AREA", "ASIA");
            TestContentConstraintUtil.AddCubeRegion(tCons, true, includeValues).KeyValues[0].IsRemovePrefix = true;
            return tCons.ImmutableInstance;
        }

        /// <summary>
        /// Generates a cube region with key values set to remove a prefix
        /// </summary>
        /// <returns></returns>
        public static IContentConstraintObject GetSeriesRemovePrefix()
        {
            IContentConstraintMutableObject tCons = TestContentConstraintUtil.Create("ACY", "CONS2", "1.0", new StructureReferenceImpl("ACY", "EXR", "1.0", SdmxStructureEnumType.Dsd));
            TestContentConstraintUtil.AddKeyset(tCons, true, "FREQ:A,REF_AREA:UK,%INDICATOR:ABC");  //indicators starts with a % to indicate remove prefix
            return tCons.ImmutableInstance;
        }

        /// <summary>
        ///  Generates a series constraint with one included and one excluded row, and one with remove prefix set to true
        /// </summary>
        /// <returns></returns>
        public static IContentConstraintObject GetSeriesConstraint()
        {
            IContentConstraintMutableObject tCons = TestContentConstraintUtil.Create("ACY", "CONS3", "1.0", new StructureReferenceImpl("ACY", "EXR", "1.0", SdmxStructureEnumType.Dsd));
            TestContentConstraintUtil.AddKeyset(tCons, true, "FREQ:A,REF_AREA:UK,INDICATOR:ABC");
            TestContentConstraintUtil.AddKeyset(tCons, false, "FREQ:M,INDICATOR:ABC,%INDUSTRY:REV1_%");
            return tCons.ImmutableInstance;
        }

        /// <summary>
        /// Generates a series constraint with attribute values
        /// </summary>
        /// <returns></returns>
        public static IContentConstraintObject GetSeriesConstraintWithAttrValues()
        {
            IContentConstraintMutableObject tCons = TestContentConstraintUtil.Create("ACY", "CONS3", "1.0", new StructureReferenceImpl("ACY", "EXR", "1.0", SdmxStructureEnumType.Dsd));
            TestContentConstraintUtil.AddKeyset(tCons, true, "FREQ:A,REF_AREA:UK,INDICATOR:ABC;COLLECTION:A,UNIT:0");
            TestContentConstraintUtil.AddKeyset(tCons, false, "FREQ:M,INDICATOR:ABC,INDUSTRY:REV1_%;COLLECTION:B,UNIT:1");
            return tCons.ImmutableInstance;
        }

        /// <summary>
        ///  FREQ, REF_AREA, INDICATOR
        /// A UK+FR+DE ABC Allowed
        //  M ABC Restricted
        /// </summary>
        /// <returns></returns>
        public static IContentConstraintObject GetSeriesConstraintWithMultiValues()
        {
            IContentConstraintMutableObject tCons = TestContentConstraintUtil.Create("ACY", "CONS4", "1.0", new StructureReferenceImpl("ACY", "EXR", "1.0", SdmxStructureEnumType.Dataflow));
            TestContentConstraintUtil.AddKeyset(tCons, true, "FREQ:A,REF_AREA:UK+FR+DE,INDICATOR:ABC");
            TestContentConstraintUtil.AddKeyset(tCons, false, "FREQ:M,INDICATOR:ABC");
            return tCons.ImmutableInstance;
        }

        /// <summary>
        ///  Generates a series constraint with REF AREA as UK+FR+DE
        /// </summary>
        /// <returns></returns>
        public static IContentConstraintObject GetSeriesConstraintWithValidity()
        {
            IContentConstraintMutableObject tCons = TestContentConstraintUtil.Create("ACY", "CONS4", "1.0", new StructureReferenceImpl("ACY", "EXR", "1.0", SdmxStructureEnumType.Dataflow));

            IConstraintDataKeySetMutableObject keys = TestContentConstraintUtil.AddKeyset(tCons, true, "FREQ:A,REF_AREA:UK,INDICATOR:ABC",
                    "FREQ:A,REF_AREA:FR,INDICATOR:ABC",
                    "FREQ:A,REF_AREA:DE,INDICATOR:ABC",
                    "FREQ:M,REF_AREA:UK,INDICATOR:ABC",
                    "FREQ:Q,REF_AREA:UK,INDICATOR:ABC",
                    "FREQ:Q,REF_AREA:FR,INDICATOR:ABC",
                    "FREQ:Q,REF_AREA:DE,INDICATOR:ABC");

            keys.ConstrainedDataKeys[2].ValidFrom = "2008";
            keys.ConstrainedDataKeys[4].ValidTo = "2010";
            keys.ConstrainedDataKeys[6].ValidFrom = "2008";
            keys.ConstrainedDataKeys[6].ValidTo = "2009";
            return tCons.ImmutableInstance;
        }

        /// <summary>
        ///  UK 1990/2000
        ///  FR 1995/
        ///  DE /1997
        /// </summary>
        /// <param name="isInclude"></param>
        /// <returns></returns>
        public static IContentConstraintObject GetCubeConstraintWithValidity(bool isInclude)
        {
            IContentConstraintMutableObject tCons = TestContentConstraintUtil.Create("ACY", "CONS5", "1.0", new StructureReferenceImpl("ACY", "EXR", "1.0", SdmxStructureEnumType.ProvisionAgreement));
            IKeyValuesMutable keyValues1 = new KeyValuesMutableImpl();

            ICubeRegionMutableObject cubeRegion = new CubeRegionMutableCore();
            keyValues1.Id = "REF_AREA";
            keyValues1.AddValue("UK");
            keyValues1.AddValue("FR");
            keyValues1.AddValue("DE");
            keyValues1.SetValidity("UK", "1990", "2000");
            keyValues1.SetValidity("FR", "1995", null);
            keyValues1.SetValidity("DE", null, "1997");

            cubeRegion.AddKeyValue(keyValues1);
            if (isInclude)
            {
                tCons.IncludedCubeRegion = (cubeRegion);
            }
            else
            {
                tCons.ExcludedCubeRegion = (cubeRegion);
            }
            return tCons.ImmutableInstance;
        }

        /// <summary>
        ///  Include: DECIMALS 0 and 1
        ///  Exclude: UNIT_MULT 8,9,12 and 15
        /// </summary>
        /// <returns>constraint with cube region, where the constraints are against the attribute / component</returns>
        public static IContentConstraintObject GetCubeConstraintOnComponent()
        {
            IContentConstraintMutableObject tCons = TestContentConstraintUtil.Create("ACY", "CONS9", "1.0", new StructureReferenceImpl("ACY", "EXR", "1.0", SdmxStructureEnumType.Dsd));

            ICubeRegionMutableObject cubeRegionInc = new CubeRegionMutableCore();
            IKeyValuesMutable keyValues1 = new KeyValuesMutableImpl();
            keyValues1.Id = "DECIMALS";
            keyValues1.AddValue("0");
            keyValues1.AddValue("1");
            cubeRegionInc.AddAttributeValue(keyValues1);
            tCons.IncludedCubeRegion = cubeRegionInc;

            ICubeRegionMutableObject cubeRegionExc = new CubeRegionMutableCore();
            IKeyValuesMutable keyValues2 = new KeyValuesMutableImpl();

            keyValues2.Id = "UNIT_MULT";
            keyValues2.AddValue("15");
            keyValues2.AddValue("12");
            keyValues2.AddValue("9");
            keyValues2.AddValue("8");
            cubeRegionExc.AddAttributeValue(keyValues2);
            tCons.ExcludedCubeRegion = cubeRegionExc;

            return tCons.ImmutableInstance;
        }
    }
}
