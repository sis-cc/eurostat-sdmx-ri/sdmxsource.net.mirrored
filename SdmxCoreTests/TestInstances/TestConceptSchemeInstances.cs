// -----------------------------------------------------------------------
// <copyright file="TestConceptSchemeInstances.cs" company="EUROSTAT">
//   Date Created : 2023-05-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.TestInstances
{
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using SdmxCoreTests.TestUtils;
    using SdmxCoreTests.Utils;

    public class TestConceptSchemeInstances
    {
        /// <summary> 
        /// </summary>
        /// <returns>
        /// a concept scheme, no representation ref, standard concepts, no hierarchy 
        /// </returns> 
        public static IConceptSchemeObject GenerateConceptsWithParents()
        {
            return TestConceptUtil.GenerateConceptScheme("CONTACT Contact",
                    "CONTACT.FirstName First Name",
                    "CONTACT.LastName Last Name",
                    "CONTACT.Address Address",
                    "ADDRESS.Line1 Address Line 1",
                    "ADDRESS.Line2 Address Line 2",
                    "ADDRESS.PostCode Post Code",
                    "CONTACT.Phone Phone Number");
        }

        /// <summary> 
        /// </summary>
        /// <returns>
        /// a concept scheme, no repreesentation ref, standard concepts, no hierarchy 
        /// </returns> 
        public static IConceptSchemeObject GenerateNonRepresentedScheme()
        {
            return TestConceptUtil.GenerateConceptScheme("FREQ", "REF_AREA", "INDICATOR", "TIME_PERIOD", "OBS_VALUE", "OBS_STATUS", "OBS_CONF", "OBS_PRE_BREAK", "SERIES_TITLE", "COLLECTION");
        }

        /// <summary> 
        /// </summary>
        /// <returns>
        /// a concept scheme with concepts with no representation, some coded, some geo, some other text formats 
        /// </returns> 
        public static IConceptSchemeObject GenerateMixedRepresentations()
        {
            IList<string> concepts = new List<string>();
            //NOTE SDMXSOURCE_SDMXCORE CHANGE
            for (int i = 0; i < TextType.Values.Count() + 1; i++)
            {
                concepts.Add("A" + i);
            }
            IConceptSchemeObject cs = TestConceptUtil.GenerateConceptScheme(concepts.ToArray());
            IConceptSchemeMutableObject mutable = cs.MutableInstance;

            int j = 0;
            foreach (IConceptMutableObject concept in mutable.Items)
            {
                j++;
                IRepresentationMutableObject rep = new RepresentationMutableCore();
                ITextFormatMutableObject tf = new TextFormatMutableCore();
                switch (j)
                {
                    case 1:  //Codelist
                        rep.Representation = new StructureReferenceImpl("ACY", "CL_REF", "1.0", SdmxStructureEnumType.CodeList);
                        break;
                    case 2:  //Codelist with text format
                        rep.Representation = new StructureReferenceImpl("ACY", "CL_REF", "1.0", SdmxStructureEnumType.CodeList);
                        tf.MinLength = 1;
                        tf.MaxLength = 3;
                        rep.TextFormat = tf;
                        break;
                    //TODO SDMXCORE VALUELIST
                    case 3:  //Value IList
                             //                    rep.Representation = new StructureReferenceImpl("ACY", "VL_REF", "1.0", SDMX_STRUCTURE_TYPE.VALUE_LIST);
                        break;
                    case 4:  //Value IList with text format
                             //                    rep.Representation = new StructureReferenceImpl("ACY", "VL_REF", "1.0", SDMX_STRUCTURE_TYPE.VALUE_LIST);
                             //                    tf.MaxLength = BigInteger.ValueOf(3);
                             //                    rep.TextFormat = tf;
                        break;
                    //TODO SDMXCORE GEO
                    case 5:  //Geo Codelist
                        rep.Representation = new StructureReferenceImpl("ACY", "VL_REF", "1.0", SdmxStructureEnumType.CodeList);
                        break;
                    default:
                        if (TextType.Values.Count() - 9 > j - 6)
                        {
                            TextType tt = TextType.Values.ToList()[j - 6];
                            //this is done because we use TextType.ParseString in SdmxJsonRepresentationUtil when parsing and there are elements with the same
                            //Sdmx3 string but different enum type and values.
                            //ex:
                            //{TextEnumType.DateTime,new TextType(TextEnumType.DateTime, true, sdmx21: "DateTime")},
                            //{TextEnumType.Date,new TextType(TextEnumType.Date, false, sdmx21: "DateTime")},
                            // in class TextType
                            //This leads to objects not to be equal and at least
                            //one test fails AbstractTestReadingConceptScheme.TestMixedRepresentations
                            var ttw = TextType.ParseString(tt.Sdmx3);
                            tf.TextType = ttw;
                            rep.TextFormat = tf;
                        }
                        else
                        {
                            rep = null;
                        }
                        break;
                }
                if (rep != null)
                {
                    concept.CoreRepresentation = rep;
                }
            }
            return mutable.ImmutableInstance;
        }


        /// <summary> 
        /// Generates the references structures for the DSD 
        /// </summary>
        /// <returns>
        /// 
        /// </returns> 
        public static ISdmxObjects GenerateReferences(IConceptSchemeObject cs, bool includeCS)
        {
            IDictionary<string, ISet<string>> agenyMap = new Dictionary<string, ISet<string>>();  //parnet id to agency id, parent default SDMX

            //AGENCEIS
            AddAgencyIdToMap(cs.AgencyId, agenyMap);
            foreach (ICrossReference xsRef in cs.CrossReferences)
            {
                AddAgencyIdToMap(xsRef.AgencyId, agenyMap);
            }

            //CONCEPTS
            ISdmxObjects objects = new SdmxObjectsImpl();
            if (includeCS)
            {
                objects.AddIdentifiable(cs);
            }

            foreach (IConceptObject concept in cs.Items)
            {
                if (concept.EnumeratedRepresentation != null)
                {
                    ICrossReference enumRef = concept.EnumeratedRepresentation;
                    //TODO SDMXCORE VALUELIST
                    if (enumRef.TargetReference == SdmxStructureEnumType.ValueList)
                    {
                        //objects.AddIdentifiable(TestValueListUtil.GenerateValuelist(enumRef.AgencyId, enumRef.MaintainableId, enumRef.Version, "$"));
                    }
                    //TODO SDMXCORE GEO
                    else
                    {
                        ICodelistObject cl;
                        /*if(concept.TextType == TEXT_TYPE.GEO) {
                            cl = TestGeographicCodelistUtil.GenerateCodelist(enumRef.AgencyId, enumRef.MaintainableId, enumRef.Version, "UK:geofeatures");
                        } else {*/
                        cl = TestCodelistUtil.GenerateCodelist(enumRef.AgencyId, enumRef.MaintainableId, enumRef.Version, "A", "B", "C");
                        //}
                        objects.AddIdentifiable(cl);
                    }


                }
            }

            foreach (string acySchemeId in agenyMap.Keys)
            {
                objects.AddIdentifiable(TestAgencySchemeUtil.GenerateAgencyScheme(acySchemeId, agenyMap[acySchemeId].ToArray()));
            }

            return objects;
        }

        private static void AddAgencyIdToMap(string agencyId, IDictionary<string, ISet<string>> agenyMap)
        {
            string[] agencySplit = agencyId.Split('.');
            agencyId = agencySplit[agencySplit.Length - 1];
            string parent = agencySplit.Length > 1 ? agencyId.Substring(0, agencyId.LastIndexOf('.')) : "SDMX";
            CollectionUtil.AddToMappedSet(agenyMap, parent, agencyId);
        }
    }
}