// -----------------------------------------------------------------------
// <copyright file="TestMSDInstances.cs" company="EUROSTAT">
//   Date Created : 2024-03-19
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SdmxCoreTests.TestInstances
{
    using System;
    using System.Numerics;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using SdmxCoreTests.TestInstances.io.sdmx.im.util.model;

    public static class TestMSDInstances
    {

        /// <summary>
        /// MSD with Attributes which are not in a hierarchy, and have non coded attributes
        /// </summary>
        /// <param name="coded"></param>
        /// <returns></returns>
        public static IMetadataStructureDefinitionObject GetFlatNoHierarchy(bool coded)
        {
            IMetadataStructureDefinitionObject msd = TestMSDUtil.GenerateMetadataStructure(coded, "A1", "A2", "A3");
            return msd;
        }


        /// <summary>
        /// MSD with hierarhcy in attributes, coded, non coded, text type of integer - mixed cardinality
        /// </summary>
        /// <returns></returns>
        public static IMetadataStructureDefinitionObject GetMixedHierarchy()
        {
            IMetadataStructureDefinitionObject msd = TestMSDUtil.GenerateMetadataStructure(0, "ACY", "MSD1", "1.1");
            msd = TestMSDUtil.AddAttributes(msd.MutableInstance, 1, 1, true, new TextFormatMutableCore(), "COUNTRY", "COUNTRY.STATE");
            msd = TestMSDUtil.AddAttributes(msd.MutableInstance, -1, -1, false, new TextFormatMutableCore(), "CONTACT"); // presentational -1
            msd = TestMSDUtil.AddAttributes(msd.MutableInstance, 1, 1, false, new TextFormatMutableCore(), "CONTACT.FIRST_NAME", "CONTACT.LAST_NAME");

            var tf = new TextFormatMutableCore();
            tf.TextType = TextType.GetFromEnum(TextEnumType.Integer);
            tf.MaxLength = 10;
            tf.MinLength = 5;
            msd = TestMSDUtil.AddAttributes(msd.MutableInstance, 0, 1, false, tf, "CONTACT.PHONE");
            return msd;
        }

        /// <summary>
        /// MSD which partially replicates the IMF DQAF. Compatible with TestMetadataSetInstances.getDataQualityReport
        /// </summary>
        /// <param name="agencyId"></param>
        /// <param name="id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static IMetadataStructureDefinitionObject GetDataQualityReport(string agencyId, string id, string version)
        {
            var htmlType = new TextFormatMutableCore();
            htmlType.TextType = TextType.GetFromEnum(TextEnumType.Xhtml);
            htmlType.Multilingual = TertiaryBool.ParseBoolean(true);

            var stringType = new TextFormatMutableCore();
            stringType.TextType = TextType.GetFromEnum(TextEnumType.String);
            stringType.Multilingual = TertiaryBool.ParseBoolean(false);
            stringType.MinLength =  3;
            stringType.MaxLength = 10;

            IMetadataStructureDefinitionObject msd = TestMSDUtil.GenerateMetadataStructure(0, agencyId, id, version);
            msd = TestMSDUtil.AddAttributes(msd.MutableInstance, -1, -1, false, new TextFormatMutableCore(), "QUALITY");
            msd = TestMSDUtil.AddAttributes(msd.MutableInstance, 1, 1, false, htmlType, "QUALITY.LEGAL");
            msd = TestMSDUtil.AddAttributes(msd.MutableInstance, 1, 1, false, htmlType, "QUALITY.RESOURCE");
            msd = TestMSDUtil.AddAttributes(msd.MutableInstance, 1, 1, false, htmlType, "QUALITY.RELEVANCE");
            msd = TestMSDUtil.AddAttributes(msd.MutableInstance, -1, -1, false, new TextFormatMutableCore(), "INTEGRITY");
            msd = TestMSDUtil.AddAttributes(msd.MutableInstance, 1, 1, false, htmlType, "INTEGRITY.INST");
            msd = TestMSDUtil.AddAttributes(msd.MutableInstance, 1, 1, false, htmlType, "INTEGRITY.TRANSPARENCY");
            msd = TestMSDUtil.AddAttributes(msd.MutableInstance, 1, 1, false, htmlType, "INTEGRITY.ETHICAL");
            msd = TestMSDUtil.AddAttributes(msd.MutableInstance, -1, -1, false, new TextFormatMutableCore(), "METHODOLOGY");
            msd = TestMSDUtil.AddAttributes(msd.MutableInstance, 1, 1, false, htmlType, "METHODOLOGY.SCOPE");
            msd = TestMSDUtil.AddAttributes(msd.MutableInstance, 1, 1, false, htmlType, "METHODOLOGY.CLASSIFICATION");
            msd = TestMSDUtil.AddAttributes(msd.MutableInstance, 1, 1, false, htmlType, "METHODOLOGY.BASIS");
            msd = TestMSDUtil.AddAttributes(msd.MutableInstance, 1, 1, false, stringType, "METHODOLOGY.SOURCE");

            return msd;
        }
    }

}
