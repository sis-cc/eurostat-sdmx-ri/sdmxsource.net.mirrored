// -----------------------------------------------------------------------
// <copyright file="CollectionUtil.cs" company="EUROSTAT">
//   Date Created : 2023-05-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.Utils
{
    using System.Collections.Generic;

    /// <summary> 
    /// Utility class providing helper methods to expand the functionality of C# Collections. 
    /// </summary> 
    public class CollectionUtil
    {
        public static void AddToMappedSet<V, T>(IDictionary<V, ISet<T>> map, V key, T value)
        {
            ISet<T> set = map[key];
            if (set == null)
            {
                set = new HashSet<T>();
                map.Add(key, set);
            }
            set.Add(value);
        }

        public static List<T> GetImmutableList<T>(params T[] contents)
        {
            List<T> list = new List<T>(contents == null ? 0 : contents.Length);

            if (contents != null)
            {
                foreach (T item in contents)
                {
                    if (item != null)
                    {
                        list.Add(item);
                    }
                }
            }

            return list;
        }

        public static void AddToMappedList<V, T>(IDictionary<V, List<T>> map, V key, T value)
        {
            if (!map.TryGetValue(key, out List<T> list))
            {
                list = new List<T>();
                map[key] = list;
            }

            list.Add(value);
        }
    }
}