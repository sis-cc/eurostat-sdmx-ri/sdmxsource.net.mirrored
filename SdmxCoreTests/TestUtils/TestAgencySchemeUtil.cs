// -----------------------------------------------------------------------
// <copyright file="TestAgencySchemeUtil.cs" company="EUROSTAT">
//   Date Created : 2023-05-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.TestUtils
{
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;

    public class TestAgencySchemeUtil
    {
        public static IAgencyScheme GenerateAgencyScheme(string agencyId, params string[] agenciesIds)
        {
            IAgencySchemeMutableObject mutable = new AgencySchemeMutableCore();
            TestMaintainableUtil.SetMaintainableProperties(mutable, agencyId, AgencyScheme.FixedId, AgencyScheme.FixedVersion);
            foreach (string providerId in agenciesIds)
            {
                mutable.CreateItem(providerId, providerId);
            }
            return mutable.ImmutableInstance;
        }

        public static void AddContactDetails<ΤImmutable, TMutable>(IOrganisationSchemeMutableObject<ΤImmutable, TMutable> mutable, IContactMutableObject contact, string agencyId)
            where ΤImmutable : IMaintainableObject where TMutable : IOrganisationMutableObject
        {
            IOrganisationMutableObject org = mutable.GetItemById(agencyId);
            org.AddContact(contact);
        }
    }
}