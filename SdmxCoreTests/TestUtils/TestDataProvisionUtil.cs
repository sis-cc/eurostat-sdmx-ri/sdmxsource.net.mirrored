using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using SdmxCoreTests.TestUtils;

namespace SdmxCore.Io.Sdmx.Im.Util.Model
{
    public class TestDataProvisionUtil
    {
        public static IProvisionAgreementObject GenerateProvision(string agencyId, string providerId, IStructureReference flowRef)
        {
            IProvisionAgreementMutableObject pa = new ProvisionAgreementMutableCore();
            
            TestMaintainableUtil.SetMaintainableProperties(pa, agencyId, flowRef.MaintainableId + "_" + providerId, "1.0.0");
            pa.StructureUsage = flowRef; 
            pa.DataproviderRef = new StructureReferenceImpl(agencyId, "DataProviders", "1.0", SdmxStructureEnumType.DataProvider, providerId); 
            return pa.ImmutableInstance;
        }

        public static IProvisionAgreementObject GenerateProvision(string agencyId, IDataflowObject flow, IDataProvider provider)
        {
            IProvisionAgreementMutableObject pa = new ProvisionAgreementMutableCore();
            TestMaintainableUtil.SetMaintainableProperties(pa, agencyId, flow.Id + "_" + provider.Id, "1.0.0");
            pa.StructureUsage = (IStructureReference)flow;
            pa.DataproviderRef = (IStructureReference)provider;
            return pa.ImmutableInstance;
        }
    }
}
