// -----------------------------------------------------------------------
// <copyright file="TestMaintainableUtil.cs" company="EUROSTAT">
//   Date Created : 2023-05-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.TestUtils
{
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Process;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Process;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;

    /// <summary> 
    /// Used to generate objects 
    /// </summary> 
    public class TestMaintainableUtil
    {
        public static IContentConstraintObject GenerateContentConstraint()
        {
            return GenerateContentConstraint(false, null, null, null);
        }
        public static IContentConstraintObject GenerateContentConstraint(bool isDefiningDataPresent, string? agency, string? id, string? version)
        {
            IContentConstraintMutableObject mutable = GetContentConstraintMutableObject(agency, id, version);
            mutable.IsDefiningActualDataPresent = isDefiningDataPresent;
            mutable.IncludedCubeRegion = GetCubeRegionMutableObject();
            return mutable.ImmutableInstance;
        }

        /// <summary> 
        /// </summary>
        /// <param name=" toConstrain"> 
        /// </param>
        /// <param name=" dimCodes"> e.g "FREQ:A,B,C" 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static IContentConstraintObject GenerateContentConstraint(IConstrainableObject toConstrain, bool isIncludeCube, params string[] dimCodes)
        {
            IContentConstraintMutableObject mutable = GetContentConstraintMutableObject(null, null, null);
            ICubeRegionMutableObject cubeReg = new CubeRegionMutableCore();
            foreach (string currentDimCodes in dimCodes)
            {
                string[] dimToCodes = currentDimCodes.Split(":");
                string[] codes = dimToCodes[1].Split(",");
                cubeReg.AddKeyValue(GetKvs(dimToCodes[0], codes.ToList()));
            }
            if (isIncludeCube)
            {
                mutable.IncludedCubeRegion = cubeReg;
            }
            else
            {
                mutable.ExcludedCubeRegion = cubeReg;
            }
            return mutable.ImmutableInstance;
        }

        public static ICategorisationObject GenerateCategorisation()
        {
            return GenerateCategorisation(null, null, null);
        }
        public static ICategorisationObject GenerateCategorisation(string? agency, string? id, string? version)
        {
            ICategorisationMutableObject mutable = new CategorisationMutableCore();
            SetMaintainableProperties(mutable, agency, id, version);
            mutable.StructureReference = GetRef(SdmxStructureEnumType.Dataflow, "DF1");
            mutable.CategoryReference = GetRef(SdmxStructureEnumType.Category, "CS", "C1");
            return mutable.ImmutableInstance;
        }

        /// <summary> 
        /// </summary>
        /// <param name=" items"> 
        /// </param>
        /// <returns>
        /// agencyshcme with agencies id ITEM[x] 
        /// </returns> 
        public static IAgencyScheme GenerateAgencyScheme(int items)
        {
            return GenerateAgencyScheme(items, null);
        }
        public static IAgencyScheme GenerateAgencyScheme(int items, string? agency)
        {
            IAgencySchemeMutableObject mutable = new AgencySchemeMutableCore();
            SetMaintainableProperties(mutable, agency, AgencyScheme.FixedId, AgencyScheme.FixedVersion);
            for (int i = 0; i < items; i++)
            {
                IAgencyMutableObject acy = new AgencyMutableCore();
                SetNameableProperties(acy, "ITEM", i);
                mutable.AddItem(acy);
            }
            return mutable.ImmutableInstance;
        }

        public static IDataProviderScheme GenerateDataProviderScheme(int items)
        {
            return GenerateDataProviderScheme(items, null);
        }
        public static IDataProviderScheme GenerateDataProviderScheme(int items, string? agency)
        {
            IDataProviderSchemeMutableObject mutable = new DataProviderSchemeMutableCore();
            SetMaintainableProperties(mutable, agency, DataProviderScheme.FixedId, DataProviderScheme.FixedVersion);
            for (int i = 0; i < items; i++)
            {
                IDataProviderMutableObject item = new DataProviderMutableCore();
                SetNameableProperties(item, "ITEM", i);
                mutable.AddItem(item);
            }
            return mutable.ImmutableInstance;
        }

        public static IDataConsumerScheme GenerateDataConsumerScheme(int items)
        {
            return GenerateDataConsumerScheme(items, null);
        }
        public static IDataConsumerScheme GenerateDataConsumerScheme(int items, string? agency)
        {
            IDataConsumerSchemeMutableObject mutable = new DataConsumerSchemeMutableCore();
            SetMaintainableProperties(mutable, agency, DataConsumerScheme.FixedId, DataConsumerScheme.FixedVersion);
            for (int i = 0; i < items; i++)
            {
                IDataConsumerMutableObject item = new DataConsumerMutableCore();
                SetNameableProperties(item, "ITEM", i);
                mutable.AddItem(item);
            }
            return mutable.ImmutableInstance;
        }

        public static IOrganisationUnitSchemeObject GenerateOrganisationUnitScheme(int items)
        {
            return GenerateOrganisationUnitScheme(items, null, null, null);
        }
        public static IOrganisationUnitSchemeObject GenerateOrganisationUnitScheme(int items, string? agency, string? id, string? version)
        {
            IOrganisationUnitSchemeMutableObject mutable = new OrganisationUnitSchemeMutableCore();
            SetMaintainableProperties(mutable, agency, id, version);
            for (int i = 0; i < items; i++)
            {
                IOrganisationUnitMutableObject item = new OrganisationUnitMutableCore();
                SetNameableProperties(item, "ITEM", i);
                mutable.AddItem(item);
            }
            return mutable.ImmutableInstance;
        }

        public static ICategorySchemeObject GenerateCategoryScheme(int items)
        {
            return GenerateCategoryScheme(items, null, null, null);
        }
        public static ICategorySchemeObject GenerateCategoryScheme(int items, string? agency, string? id, string? version)
        {
            ICategorySchemeMutableObject mutable = new CategorySchemeMutableCore();
            SetMaintainableProperties(mutable, agency, id, version);
            for (int i = 0; i < items; i++)
            {
                ICategoryMutableObject item = new CategoryMutableCore();
                SetNameableProperties(item, "ITEM", i);
                mutable.AddItem(item);
            }
            return mutable.ImmutableInstance;
        }

        /// <summary> 
        /// Generates a codelist, code Ids are: 
        /// ITEM[x] where x starts from 1 
        /// </summary>
        /// <param name=" items"> 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static ICodelistObject GenerateCodelist(int items)
        {
            return GenerateCodelist(items, null, null, null);
        }
        public static ICodelistObject GenerateCodelist(int items, string agency, string id, string version)
        {
            ICodelistMutableObject mutable = new CodelistMutableCore();
            SetMaintainableProperties(mutable, agency, id, version);
            if (id != null)
            {
                mutable.Id = id;
            }
            for (int i = 0; i < items; i++)
            {
                ICodeMutableObject item = new CodeMutableCore();
                SetNameableProperties(item, "ITEM", i);
                mutable.AddItem(item);
            }
            return mutable.ImmutableInstance;
        }

        public static IConceptSchemeObject GenerateConceptScheme(int items)
        {
            return GenerateConceptScheme(items, null, null, null);
        }
        public static IConceptSchemeObject GenerateConceptScheme(int items, string? agency, string? id, string? version)
        {
            IConceptSchemeMutableObject mutable = new ConceptSchemeMutableCore();
            SetMaintainableProperties(mutable, agency, id, version);
            for (int i = 0; i < items; i++)
            {
                IConceptMutableObject item = new ConceptMutableCore();
                SetNameableProperties(item, "ITEM", i);
                mutable.AddItem(item);
            }
            return mutable.ImmutableInstance;
        }

        /// <summary> 
        /// Generates a DSD with NON Coded Dimensions and Measures 
        /// </summary>
        /// <param name=" dimensions"> - number of dimensions to create with IDs of D[x] with x starting at 1: e.g D1, D2 
        /// </param>
        /// <param name=" seriesAttributes"> - number of seriesAttributes to create with IDs of SA[x] with x starting at 1: e.g SA1, SA2 
        /// </param>
        /// <param name=" time"> - if true a Time Dimension is created 
        /// </param>
        /// <param name=" pm"> - if true a Primary Measure is created 
        /// </param>
        /// <param name=" obsAttributes"> - number of obsAttributes to create with IDs of OA[x] with x starting at 1: e.g OA1, OA2 
        /// </param>
        /// <param name=" additionalMeasures"> - Number of additional Measures with IDs of M[x] with x starting at 1: e.g M1, M2 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        /* // TODO SDMXRI-1914 DSD
             public static IDataStructureObject GenerateDSD(int dimensions, int seriesAttributes, bool time, bool pm, int obsAttributes, int additionalMeasures) {
                return GenerateDSD(dimensions, seriesAttributes, time, pm, obsAttributes, additionalMeasures, null, null, null);
            }*/

        /*
        // TODO SDMXRI-1914 DSD
        public static IDataStructureObject GenerateDSD(int dimensions, int seriesAttributes, bool time, bool pm, int obsAttributes, int additionalMeasures, string agency, string id, string version) {
            IDataStructureMutableObject dsdMutable = new DataStructureMutableCore();
            dsdMutable.AgencyId = agency == null ? "TEST" : agency;
            dsdMutable.Id = id == null ? "TEST" : id;
            dsdMutable.Version = version == null ? "1.0" : version;
            dsdMutable.AddName("en", "TEST");
            for(int i =0; i < dimensions; i++) {
                dsdMutable.AddDimension(CreateDimension((i+1)));
            }
            for(int i=0; i < seriesAttributes; i++) {
                dsdMutable.AddAttribute(CreateAttribute(dsdMutable, (i+1), AttributeAttachmentLevel.DimensionGroup));

            }
            for(int i=0; i < obsAttributes; i++) {
                dsdMutable.AddAttribute(CreateAttribute(dsdMutable, (i+1), AttributeAttachmentLevel.Observation));
            }
            for(int i =0; i < additionalMeasures; i++) {
                dsdMutable.AddMeasure(CreateMeasure((i+1)));
            }
            if(pm) {
                dsdMutable.PrimaryMeasure = new PrimaryMeasureMutableCore();
                dsdMutable.PrimaryMeasure.ConceptRef = CreateConceptReference("OBS_VALUE");
            }
            if(time) {
                IDimensionMutableObject dim = new DimensionMutableCore();
                dim.Id = "TIME_PERIOD";
                dim.TimeDimension = true;
                dim.ConceptRef = CreateConceptReference("T");
                dsdMutable.AddDimension(dim);
            }
            IDataStructureObject dsd = dsdMutable.ImmutableInstance;

            return dsd;
        }*/

        public static IDataflowObject GenerateDataflow()
        {
            return GenerateDataflow(null, null, null);
        }
        public static IDataflowObject GenerateDataflow(string? agency, string? id, string? version)
        {
            IDataflowMutableObject mutable = new DataflowMutableCore();
            SetMaintainableProperties(mutable, agency, id, version);
            mutable.DataStructureRef = GetRef(SdmxStructureEnumType.Dsd, "DSD");
            return mutable.ImmutableInstance;
        }

        private static IInputOutputMutableObject GetInputOutput(int idx)
        {
            IInputOutputMutableObject io = new InputOutputMutableCore();
            io.LocalId = "IO" + idx;
            io.StructureReference = GetRef(SdmxStructureEnumType.Code, "CL", "CODE" + idx);
            return io;
        }

        public static IProvisionAgreementObject GenerateProvisionAgreement()
        {
            return GenerateProvisionAgreement(null, null, null);
        }
        public static IProvisionAgreementObject GenerateProvisionAgreement(string? agency, string? id, string? version)
        {
            IProvisionAgreementMutableObject mutable = new ProvisionAgreementMutableCore();
            SetMaintainableProperties(mutable, agency, id, version);
            mutable.DataproviderRef = GetRef(SdmxStructureEnumType.DataProvider, "DATA_PROVIDERS", "DP1");
            mutable.StructureUsage = GetRef(SdmxStructureEnumType.Dataflow, "FLOW");
            return mutable.ImmutableInstance;
        }

        public static IRegistrationObject GenerateRegistration()
        {
            return GenerateRegistration(null, null, null);
        }
        public static IRegistrationObject GenerateRegistration(string? agency, string? id, string? version)
        {
            IRegistrationMutableObject mutable = new RegistrationMutableCore();
            SetMaintainableProperties(mutable, agency, id, version);
            mutable.ProvisionAgreementRef = GetRef(SdmxStructureEnumType.ProvisionAgreement, "PA");
            IDataSourceMutableObject dataSource = new DataSourceMutableCore();
            dataSource.DataUrl = new Uri("http://www.google.com");
            dataSource.RESTDatasource = true;
            mutable.DataSource = dataSource;

            return mutable.ImmutableInstance;
        }

        public static IReportingTaxonomyObject GenerateReportingTaxonomy(int items)
        {
            return GenerateReportingTaxonomy(items, null, null, null);
        }
        public static IReportingTaxonomyObject GenerateReportingTaxonomy(int items, string? agency, string? id, string? version)
        {
            IReportingTaxonomyMutableObject mutable = new ReportingTaxonomyMutableCore();
            SetMaintainableProperties(mutable, agency, id, version);
            for (int i = 0; i < items; i++)
            {
                IReportingCategoryMutableObject item = new ReportingCategoryMutableCore();
                SetNameableProperties(item, "ITEM", i);
                mutable.AddItem(item);
            }
            return mutable.ImmutableInstance;
        }

        /*
        //TODO SDMXCORE_LOWPRIO StructureSet
        public static IStructureSetObject GenerateStructureSet(int structureMaps, int codelistMaps) {
            return GenerateStructureSet(structureMaps, codelistMaps, null, null, null);
        }

        public static IStructureSetObject GenerateStructureSet(int structureMaps, int codelistMaps, string agency, string id, string version) {
            IStructureSetMutableObject mutable = new StructureSetMutableCore();
            SetMaintainableProperties(mutable, agency, id, version);

            for(int i=0; i < structureMaps; i++) {
                IStructureMapMutableObject structureMap = new StructureMapMutableCore();
                structureMap.SourceRef = GetRef(SdmxStructureEnumType.Dataflow, "DF_SOURCE"+i);
                structureMap.TargetRef = GetRef(SdmxStructureEnumType.Dataflow, "DF_TARGET"+i);
                SetNameableProperties(structureMap, "SM", i);
                IComponentMapMutableObject compMap = new ComponentMapMutableCore();
                compMap.AddMapConceptRef("D_SOURCE"+i);
                compMap.AddMapTargetConceptRef("D_TARGET"+i);
                structureMap.AddComponent(compMap);
                mutable.AddStructureMap(structureMap);
            }

            for(int i=0; i < codelistMaps; i++) {
                ICodelistMapMutableObject clMap = new CodelistMapMutableCore();
                SetNameableProperties(clMap, "CL_MAP", i);
                clMap.SourceRef = GetRef(SdmxStructureEnumType.CodeList, "CL_SOURCE"+i);
                clMap.TargetRef = GetRef(SdmxStructureEnumType.CodeList, "CL_TARGET"+i);

                for(int j=0; j < codelistMaps; j++) {
                    ICodeMapMutableObject codeMap = new CodeMapMutableCore();
                    codeMap.SourceId = "C_SOURCE"+j;
                    codeMap.TargetId = "C_TARGET"+j;
                    clMap.AddItem(codeMap);
                }

                mutable.AddCodelistMap(clMap);
            }

            return mutable.ImmutableInstance;
        }*/

        /*    //TODO SDMXCORE_LOWPRIO RepresentationMap
            public static IRepresentationMapObject GenerateRepresentationMap(string agency, string id, string version) {
                IRepresentationMapMutableObject mutable = new RepresentationMapMutableObject();
                SetMaintainableProperties(mutable, agency, id, version);
                mutable.AddSourceRef(GetRef(SdmxStructureEnumType.CodeList, "CL_SOURCE"));
                mutable.AddTargetRef(GetRef(SdmxStructureEnumType.CodeList, "CL_TARGET"));
                mutable.AddMappedRelationship().AddTargetValue("OUT_A").AddSourceValue().Value = "IN_A";
                mutable.AddMappedRelationship().AddTargetValue("OUT_B").AddSourceValue().Value = "IN_B";
                mutable.AddMappedRelationship().AddTargetValue("OUT_C").AddSourceValue().Value = "IN_C";
                return mutable.ImmutableInstance;
            }*/

        /*    public static IReportingTemplateObject GenerateReportingTemplate() {
                return GenerateReportingTemplate(null, null, null);
            }
            public static IReportingTemplateObject GenerateReportingTemplate(string agency, string id, string version) {
                IReportingTemplateMutableObject mutable = new ReportingTemplateMutableCore();
                SetMaintainableProperties(mutable, agency, id, version);
                //TODO
                return mutable.ImmutableInstance;
            }*/

        private static ICubeRegionMutableObject GetCubeRegionMutableObject()
        {
            ICubeRegionMutableObject cubeReg = new CubeRegionMutableCore();
            cubeReg.AddKeyValue(GetKvs("EXR_TYPE", new List<string> { "NRP0", "ERU1", "ERC0", "SP00" }));
            cubeReg.AddKeyValue(GetKvs("EXR_SUFFIX", new List<string> { "P", "A", "R", "S", "T", "E" }));
            cubeReg.AddKeyValue(GetKvs("FREQ", new List<string> { "A", "Q", "M" }));
            cubeReg.AddKeyValue(GetKvs("CURRENCY", new List<string> { "_T", "EUR", "USD" }));
            cubeReg.AddKeyValue(GetKvs("CURRENCY_DENOM", new List<string> { "_T", "EUR", "USD" }));
            return cubeReg;
        }

        private static IContentConstraintMutableObject GetContentConstraintMutableObject(string? agency, string? id, string? version)
        {
            IContentConstraintMutableObject mutable = new ContentConstraintMutableCore();
            mutable.IsDefiningActualDataPresent = false;
            SetMaintainableProperties(mutable, agency, id, version);
            SetConstraintAttachment(mutable);
            return mutable;
        }

        private static void SetConstraintAttachment(IConstraintMutableObject mutable)
        {
            IConstraintAttachmentMutableObject attachment = new ContentConstraintAttachmentMutableCore();
            IStructureReference strucRef = new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ECB:EXR(1.0)");
            attachment.AddStructureReference(strucRef);
            mutable.ConstraintAttachment = attachment;
        }

        private static IKeyValuesMutable GetKvs(string id, IList<string> values)
        {
            IKeyValuesMutable kv = new KeyValuesMutableImpl();
            kv.Id = id;
            foreach(var value in values)
            {
                kv.AddValue(value);
            }
            return kv;
        }


        public static void SetMaintainableProperties(IMaintainableMutableObject mutable, IMaintainableRefObject refObj)
        {
            mutable.AgencyId = refObj.AgencyId;
            mutable.Id = refObj.MaintainableId;
            mutable.Version = refObj.Version;
            if (refObj.MaintainableId != null)
            {
                mutable.AddName("en", refObj.MaintainableId);
            }
        }

        public static void SetMaintainableProperties(IMaintainableMutableObject mutable, string? agency, string? id, string? version)
        {
            mutable.AgencyId = "TEST";
            mutable.Id = "TEST";
            mutable.AddName("en", "TEST");
            if (agency != null)
            {
                mutable.AgencyId = agency;
            }
            if (id != null)
            {
                mutable.Id = id;
            }
            if (version != null)
            {
                mutable.Version = version;
            }
        }

        public static void SetNameableProperties(INameableMutableObject nameable, string? id, string? name)
        {
            nameable.Id = id;
            nameable.AddName("en", name);
        }
        private static void SetNameableProperties(INameableMutableObject nameable, string? idPrefix, int idx)
        {
            idx++;
            nameable.Id = idPrefix + idx;
            nameable.AddName("en", idPrefix + idx);
        }

        public static IStructureReference GetRef(SdmxStructureEnumType type, string? maintId, params string[] identifableId)
        {
            return new StructureReferenceImpl("TEST", maintId, "1.0", type, identifableId);
        }

        private static IDimensionMutableObject CreateDimension(int num)
        {
            IDimensionMutableObject dim = new DimensionMutableCore();
            dim.Id = "D" + num;
            dim.ConceptRef = CreateConceptReference("D" + num);
            return dim;
        }

        /*
        // TODO SDMXRI-1914 DSD
        public static IAttributeMutableObject CreateAttribute(IDataStructureMutableObject dsdMutable, int num, AttributeAttachmentLevel attach) {
            IAttributeMutableObject attr = new AttributeMutableCore();
            attr.AttachmentLevel = attach;
            string prefix = "A";
            switch(attach) {
                case DataSet:
                    prefix = "DA";
                    break;
                case DimensionGroup:
                    prefix = "SA";
                    foreach (IDimensionMutableObject dim in dsdMutable.Dimensions) {
                        attr.AddDimensionReferences(dim.Id);
                    }
                    break;
                case Group:
                    prefix = "GA";
                    break;
                case Observation:
                    prefix = "OA";
                    break;
                default:
                    break;

            }
            attr.Id = prefix+num;
            attr.ConceptRef = CreateConceptReference("A"+num);
            attr.Mandatory = false;
            return attr;
        }*/

        private static IMeasureMutableObject CreateMeasure(int num)
        {
            IMeasureMutableObject measure = new MeasureMutableCore();
            measure.Id = "M" + num;
            measure.ConceptRef = CreateConceptReference("M" + num);
            return measure;
        }

        public static IStructureReference CreateConceptReference(string id)
        {
            return new StructureReferenceImpl("TEST", "CS", "1.0", SdmxStructureEnumType.Concept, id);
        }

        /*    // TODO SDMXRI-1914 DSD
            public static IDataStructureObject CreateValueListReference(IDataStructureObject dsd, string component, string valueListId) {
                //Modify to use a value list, and the default format changes
                IDataStructureMutableObject dsdMutable = dsd.MutableInstance;
                IRepresentationMutableObject rep = new RepresentationMutableCore();
                rep.Representation = new StructureReferenceImpl("TEST", valueListId, "1.0", SdmxStructureEnumType.ValueList);
                dsdMutable.GetComponentById(component).Representation = rep;
                return dsdMutable.ImmutableInstance;
            }

            public static IDataStructureObject CreateCodelistListReference(IDataStructureObject dsd, string component, string valueListId) {
                //Modify to use a value list, and the default format changes
                IDataStructureMutableObject dsdMutable = dsd.MutableInstance;
                IRepresentationMutableObject rep = new RepresentationMutableCore();
                rep.Representation = new StructureReferenceImpl("TEST", valueListId, "1.0", SdmxStructureEnumType.CodeList);
                dsdMutable.GetComponentById(component).Representation = rep;
                return dsdMutable.ImmutableInstance;
            }*/

        public static IMaintainableObject AddName(IMaintainableObject maint, string locale, string name)
        {
            IMaintainableMutableObject mutable = maint.MutableInstance;
            mutable.AddName(locale, name);
            return mutable.ImmutableInstance;
        }

        public static IMaintainableObject AddDescription(IMaintainableObject maint, string locale, string name)
        {
            IMaintainableMutableObject mutable = maint.MutableInstance;
            mutable.AddDescription(locale, name);
            return mutable.ImmutableInstance;
        }

        public static IMaintainableObject AddAnnotation(IMaintainableObject maint, string title, string type, string url)
        {
            IMaintainableMutableObject mutable = maint.MutableInstance;
            mutable.AddAnnotation(title, type, url);
            return mutable.ImmutableInstance;
        }
    }
}