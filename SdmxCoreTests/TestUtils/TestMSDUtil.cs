// -----------------------------------------------------------------------
// <copyright file="TestMSDUtil.cs" company="EUROSTAT">
//   Date Created : 2024-03-19
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;

namespace SdmxCoreTests.TestInstances
{
    using System;
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;
    using SdmxCoreTests.TestUtils;

    namespace io.sdmx.im.util.model
    {
        public static class TestMSDUtil
        {
            public static IMetadataStructureDefinitionObject GenerateMetadataStructure(int attributes)
            {
                return GenerateMetadataStructure(attributes, string.Empty, string.Empty, string.Empty);
            }

            public static IMetadataStructureDefinitionObject GenerateMetadataStructure(bool coded, params string[] attributes)
            {
                var mutable = new MetadataStructureDefinitionMutableCore();
                TestMaintainableUtil.SetMaintainableProperties(mutable, "ACY", "MSD", "1.0.0");
                AddAttributes(mutable, 0, 1, coded, new TextFormatMutableCore(), attributes);
                return mutable.ImmutableInstance;
            }

            public static IMetadataStructureDefinitionObject GenerateMetadataStructure(int attributes, string agency, string id, string version)
            {
                var mutable = new MetadataStructureDefinitionMutableCore();
                TestMaintainableUtil.SetMaintainableProperties(mutable, agency, id, version);

                string[] attributeIds = new string[attributes];
                for (int i = 0; i < attributes; i++)
                {
                    attributeIds[i] = "MA" + i;
                }
                return AddAttributes(mutable, 0, 1, true, new TextFormatMutableCore(), attributeIds);
            }

            public static IMetadataStructureDefinitionObject AddAttributes(IMetadataStructureDefinitionObject msd, bool coded, params string[] attributeId)
            {
                return AddAttributes(msd.MutableInstance, 0, 1, coded, new TextFormatMutableCore(), attributeId);
            }

            public static IMetadataStructureDefinitionObject AddAttributes(IMetadataStructureDefinitionMutableObject mutable,
                                                                        int minOccurs,
                                                                        int maxOccurs,
                                                                        bool coded,
                                                                        ITextFormatMutableObject textFormat,
                                                                        params string[] attributeIds)
            {
                if (attributeIds != null)
                {
                    Dictionary<string, IList<IMetadataAttributeMutableObject>> attMap = new Dictionary<string, IList<IMetadataAttributeMutableObject>>();

                    // ID ot he actual attribute, i.e ADDRESS.FIRST_LINE = attribute
                    Dictionary<string, IMetadataAttributeMutableObject> parentIdToAttributeMap = new Dictionary<string, IMetadataAttributeMutableObject>();
                    PopulateAttributeMap(attMap, parentIdToAttributeMap, string.Empty, mutable.MetadataAttributes.ToList());

                    foreach (string fullQalifiedId in attributeIds)
                    {
                        string parentId = fullQalifiedId.Contains(".") ? fullQalifiedId.Substring(0, fullQalifiedId.LastIndexOf(".")) : string.Empty;
                        string id = fullQalifiedId.Contains(".") ? fullQalifiedId.Substring(fullQalifiedId.LastIndexOf(".") + 1) : fullQalifiedId;

                        if (!attMap.TryGetValue(parentId, out var l))
                        {
                            l = new List<IMetadataAttributeMutableObject>();
                            attMap[parentId] = l;
                        }
                        IMetadataAttributeMutableObject attMutable = new MetadataAttributeMutableCore();
                        parentIdToAttributeMap[fullQalifiedId] = attMutable;
                        attMutable.Id = id;
                        SdmxStructureEnumType st = SdmxStructureEnumType.Concept;
                        IStructureReference conceptRef = new StructureReferenceImpl("SDMX", "MSD_CONCEPTS", "1.0", st, id);
                        attMutable.ConceptRef = conceptRef;
                        if (minOccurs < 0 && maxOccurs < 0)
                        {
                            attMutable.Presentational = TertiaryBool.ParseBoolean(true);
                        }
                        else
                        {
                            attMutable.MinOccurs = minOccurs == 0 ? null : minOccurs;
                            attMutable.MaxOccurs = maxOccurs;
                        }
                        l.Add(attMutable);

                        var representation = attMutable.Representation;
                        if (representation == null)
                        {
                            representation = new RepresentationMutableCore();
                            attMutable.Representation = representation;
                        }
                        representation.TextFormat = textFormat;
                        if (coded)
                        {
                            representation.Representation = new StructureReferenceImpl("SDMX", "CL_" + id, "1.0", SdmxStructureEnumType.CodeList);
                        }
                    }
                    // Tie all parents to children
                    foreach (string attParent in attMap.Keys)
                    {
                        if (attParent != null && parentIdToAttributeMap.ContainsKey(attParent))
                        {
                            var att = parentIdToAttributeMap[attParent];
                            att.SetMetadataAttributes(attMap[attParent].ToList());
                        }
                    }
                    mutable.MetadataAttributes = attMap[string.Empty];
                }

                return mutable.ImmutableInstance;
            }

            private static void PopulateAttributeMap(Dictionary<string, IList<IMetadataAttributeMutableObject>> attMap,
                                                      Dictionary<string, IMetadataAttributeMutableObject> parentIdToAttributeMap,
                                                      string parentId, IList<IMetadataAttributeMutableObject> metadataAttributes)
            {
                if (metadataAttributes != null)
                {
                    attMap[parentId] = metadataAttributes;
                    foreach (var att in metadataAttributes)
                    {
                        string nextParentId = parentId == string.Empty ? att.Id : parentId + "." + att.Id;
                        parentIdToAttributeMap[nextParentId] = att;
                        if (ObjectUtil.ValidCollection(att.MetadataAttributes))
                        {
                            PopulateAttributeMap(attMap, parentIdToAttributeMap, nextParentId, att.MetadataAttributes);
                        }
                    }
                }
            }
        }
    }

}
