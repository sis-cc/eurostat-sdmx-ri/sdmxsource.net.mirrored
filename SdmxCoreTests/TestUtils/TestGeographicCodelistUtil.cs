// -----------------------------------------------------------------------
// <copyright file="TestGeographicCodelistUtil.cs" company="EUROSTAT">
//   Date Created : 2023-05-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.TestUtils
{
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Codelist;

    public class TestGeographicCodelistUtil
    {
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="agency"></param>
        /// <param name="id"></param>
        /// <param name="version"></param>
        /// <param name="geoCodes">array of code  ids, if an id contains a whitespace, then everything after the whitespace is the name, if there is no name 
        /// then the name is the combination of id-name </param>
        /// <returns></returns>
        public static IGeographicCodelistObject GenerateCodelist(string agency, string id, string version, params string[] geoCodes)
        {
            IGeographicCodelistMutableObject mutable = new GeographicCodelistMutableObjectCore();
            TestMaintainableUtil.SetMaintainableProperties(mutable, agency, id, version);
            foreach (string codeId in geoCodes)
            {
                AddCode(mutable, codeId);
            }
            return mutable.ImmutableInstance;
        }

        /// <summary> 
        /// Mutates the codelist so that it : the other codelists passed in 
        /// </summary>
        /// <param name=" codelist"> to mutate and return 
        /// </param>
        /// <param name=" toExtend"> the codelist definition will be updated to extend these codelists 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static ICodelistObject ExtendCodelists(ICodelistObject codelist, params ICodelistObject[] toExtend)
        {
            if (toExtend == null)
            {
                return codelist;
            }
            ICodelistMutableObject mutable = codelist.MutableInstance;
            foreach (ICodelistObject cl in toExtend)
            {
                ICodelistInheritanceRuleMutableObject rule1 = new CodelistInheritanceRuleMutableCore();
                rule1.CodelistRef = cl.AsReference;
                mutable.CodelistExtensions.Add(rule1);
            }
            return mutable.ImmutableInstance;
        }
     
        private static IGeoFeatureSetCodeMutableObject AddCode(IGeographicCodelistMutableObject mutable, string codeId)
        {           
            IGeoFeatureSetCodeMutableObject item = new GeoFeatureSetCodeMutableCore();
            
            string [] idSplit = codeId.Trim().Split(':');
            codeId = idSplit[0];

            string[] splitOnDot = codeId.Split('.', 2);
            codeId = splitOnDot[splitOnDot.Length - 1];
           
            if (splitOnDot.Length > 1)
            {
                item.ParentCode = splitOnDot[0];
            }

            item.GeoFeatureSet = idSplit[1];

            string[] split = codeId.Split(" ", 2);
            string codeName = split.Length > 1 ? split[1] : codeId + "-name";           
            TestMaintainableUtil.SetNameableProperties(item, split[0], codeName);
            mutable.AddItem(item);
            return item;
        }
        
    }
}