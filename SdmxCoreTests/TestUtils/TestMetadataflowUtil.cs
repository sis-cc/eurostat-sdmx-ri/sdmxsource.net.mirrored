namespace Io.Sdmx.Im.Util.Model
{
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using SdmxCoreTests.TestUtils;

    public class TestMetadataflowUtil
    {
        /// <summary>   
        ///  Generates a Metadataflow with the same agency, id, version as the DSD that it references
        /// </summary>
        /// <param name="targets"> - 1 or more targets </param>
        /// <returns>
        /// </returns> 
        public static IMetadataFlow GenerateMetaDataflow(string agency, string id, string version, params IStructureReference[] targets)
        {
            IMetadataFlowMutableObject mutable = new MetadataflowMutableCore();
            IStructureReference msdRef = new StructureReferenceImpl(agency, id, version, SdmxStructureEnumType.Msd);
            mutable.MetadataStructureRef = msdRef;
            if (targets != null)
            {
                foreach (IStructureReference target in targets)
                {
                    mutable.AddTarget(target);
                }
            }
            TestMaintainableUtil.SetMaintainableProperties(mutable, new MaintainableRefObjectImpl(agency, id, version));
            return mutable.ImmutableInstance;
        }

    }

}