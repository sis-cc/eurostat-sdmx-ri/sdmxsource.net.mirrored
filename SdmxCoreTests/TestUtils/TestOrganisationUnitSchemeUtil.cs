// -----------------------------------------------------------------------
// <copyright file="TestOrganisationUnitSchemeUtil.cs" company="EUROSTAT">
//   Date Created : 2023-11-13
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.TestUtils
{
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    public class TestOrganisationUnitSchemeUtil
    {
        public static IOrganisationUnitSchemeObject GenerateOrganisationUnitScheme(string agencyId, string id, string version, params string[] unitIds)
        {
            IOrganisationUnitSchemeMutableObject mutable = new OrganisationUnitSchemeMutableCore();
            TestMaintainableUtil.SetMaintainableProperties(mutable, agencyId, id, version);
            foreach (string unitId in unitIds)
            {
                string name = unitId.Contains(" ") ? unitId.Split(' ', 2)[1] : unitId;
                var uId = unitId.Contains(" ") ? unitId.Split(' ', 2)[0] : unitId;

                string parentId = null;
                if (uId.Contains('.'))
                {
                    string[] split = uId.Split('.');
                    parentId = split[0];
                    uId = split[1];
                }
                IOrganisationUnitMutableObject unitBean = mutable.CreateItem(uId, name);
                unitBean.ParentUnit = parentId;
            }
            return mutable.ImmutableInstance;
        }
    }
}
