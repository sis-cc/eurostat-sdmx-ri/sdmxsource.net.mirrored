// -----------------------------------------------------------------------
// <copyright file="TestCategorySchemeUtil.cs" company="EUROSTAT">
//   Date Created : 2023-05-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.TestUtils
{
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.CategoryScheme;

    public class TestCategorySchemeUtil
    {
        /// <summary> 
        /// </summary>
        /// <param name=" agency"> category scheme agency 
        /// </param>
        /// <param name=" id"> category scheme id 
        /// </param>
        /// <param name=" version"> category scheme version 
        /// </param>
        /// <param name=" categories"> array of code  ids, if an id contains a whitespace, then everything after the whitespace is the name, if there is no name 
        /// then the name is the combination of id-name 
        /// </param>
        /// <returns>
        /// </returns> 
        public static ICategorySchemeObject GenerateCategoryScheme(string agency, string id, string version, params string[] categories)
        {
            ICategorySchemeMutableObject mutable = new CategorySchemeMutableCore();
            TestMaintainableUtil.SetMaintainableProperties(mutable, agency, id, version);
            foreach (string catId in categories)
            {
                AddCategory(mutable, catId);
            }
            return mutable.ImmutableInstance;
        }

        private static ICategoryMutableObject AddCategory(ICategorySchemeMutableObject mutable, string catId)
        {
            ICategoryMutableObject item = new CategoryMutableCore();
            string[] split = catId.Split(" ", 2);

            string hCodeId = split[0];
            hCodeId = hCodeId.Contains('.') ? hCodeId.Substring(hCodeId.LastIndexOf('.') + 1) : hCodeId;

            string catName = split.Length > 1 ? split[1] : hCodeId + "-name";

            TestMaintainableUtil.SetNameableProperties(item, hCodeId, catName);

            if (!catId.Contains('.'))
            {
                mutable.AddItem(item);
            }
            else
            {
                string parentId = catId.Substring(0, catId.LastIndexOf('.'));

                IList<ICategoryMutableObject> cats = mutable.Items;
                ICategoryMutableObject? parent = null;
                foreach (string currentParentId in parentId.Split('.'))
                {
                    foreach (ICategoryMutableObject possibleParent in cats)
                    {
                        if (possibleParent.Id.Equals(currentParentId))
                        {
                            parent = possibleParent;
                            cats = parent.Items;
                            break;
                        }
                    }
                }
                parent?.AddItem(item);
            }
            return item;
        }
    }
}