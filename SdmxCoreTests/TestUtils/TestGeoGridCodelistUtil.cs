// -----------------------------------------------------------------------
// <copyright file="TestGeoGridCodelistUtil.cs" company="EUROSTAT">
//   Date Created : 2024-03-13
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.TestUtils
{
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;

    public class TestGeoGridCodelistUtil
    {
        /// <summary>
        /// </summary>
        /// <param name="agency"></param>
        /// <param name="id"></param>
        /// <param name="version"></param>
        /// <param name="gridDefinition"></param>
        /// <param name="geoCodes"></param>
        /// <returns></returns>
        public static IGeoGridCodelistObject GenerateCodelist(string agency, string id, string version,
            string gridDefinition, params string[] geoCodes)
        {
            IGeoGridCodelistMutableObject mutable = new GeoGridCodelistMutableCore();
            TestMaintainableUtil.SetMaintainableProperties(mutable, agency, id, version);
            foreach (string codeId in geoCodes)
            {
                AddCode(mutable, codeId);
            }
            mutable.GridDefinition = gridDefinition;
            return mutable.ImmutableInstance;
        }

        /// <summary>
        /// </summary>
        /// <param name="mutable"></param>
        /// <param name="codeId"></param>
        /// <returns></returns>
        private static IGeoGridCodeMutableObject AddCode(IGeoGridCodelistMutableObject mutable, string codeId)
        {
            IGeoGridCodeMutableObject item = new GeoGridCodeMutableCore();
            string[] idSplit = codeId.Split(":");
            codeId = idSplit[0];
            item.GeoCell = idSplit[1];
            string[] split = codeId.Split(" ", 2);
            string codeName = split.Length > 1 ? split[1] : codeId + "-name";
            TestMaintainableUtil.SetNameableProperties(item, split[0], codeName);
            mutable.AddItem(item);
            return item;
        }
    }
}
