// -----------------------------------------------------------------------
// <copyright file="TestCategorisationUtil.cs" company="EUROSTAT">
//   Date Created : 2023-05-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.TestUtils
{
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.CategoryScheme;

    public class TestCategorisationUtil
    {
        public static ICategorisationObject GenerateCategorisation(string catId, SdmxStructureEnumType toRef, string toId)
        {
            ICategorisationMutableObject mutable = new CategorisationMutableCore();
            TestMaintainableUtil.SetMaintainableProperties(mutable, "ACY", catId.Replace(".", "_") + "2" + toId.Replace(".", "_"), "1.0.0");

            string maintId = toId;
            string[] identId = new string[2];
            if (toId.Contains('.'))
            {
                string[] split = toId.Split(".");
                maintId = split[0];
                identId = toId.Substring(toId.IndexOf(".") + 1).Split(".");
            }
            mutable.StructureReference = TestMaintainableUtil.GetRef(toRef, maintId, identId);
            mutable.CategoryReference = TestMaintainableUtil.GetRef(SdmxStructureEnumType.Category, "CS", catId.Split("."));
            return mutable.ImmutableInstance;
        }
    }
}