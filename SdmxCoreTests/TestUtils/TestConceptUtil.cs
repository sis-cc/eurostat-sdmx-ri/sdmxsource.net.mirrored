// -----------------------------------------------------------------------
// <copyright file="TestConceptUtil.cs" company="EUROSTAT">
//   Date Created : 2023-05-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.TestUtils
{
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary> 
    /// Generates test concept schemes 
    /// </summary> 
    public class TestConceptUtil
    {
        public static IConceptSchemeObject GenerateConceptScheme(params string[] concepts)
        {
            return GenerateConceptSchemeWithProps(null, null, null, "-Name", concepts);
        }

        /// <summary> 
        /// </summary>
        /// <param name=" agency"> concept scheme agency 
        /// </param>
        /// <param name=" id"> concept scheme id 
        /// </param>
        /// <param name=" version"> concept scheme version 
        /// </param>
        /// <param name=" namePostfix"> added onto the end of each id (e.g Concept FREQ with postfix -NAME would have a name of FREQ-NAME) 
        /// </param>
        /// <param name=" concepts"> array of concept ids to create in the concept scheme 
        /// </param>
        /// <returns>
        /// </returns> 
        public static IConceptSchemeObject GenerateConceptSchemeWithProps(string? agency, string? id, string? version, string? namePostfix, params string[] concepts)
        {
            IConceptSchemeMutableObject mutable = new ConceptSchemeMutableCore();
            TestMaintainableUtil.SetMaintainableProperties(mutable, agency, id, version);
            foreach (string conceptId in concepts)
            {
                IConceptMutableObject item = new ConceptMutableCore();

                var idValue = conceptId.Trim();

                string[] splitOnDot = idValue.Split('.', 2);
                idValue = splitOnDot[splitOnDot.Length - 1];
                if (splitOnDot.Length > 1)
                {
                    item.ParentConcept = splitOnDot[0];
                }
                string[] split = conceptId.Split(" ", 2);
                idValue = split[0];
                string conceptName = split.Length > 1 ? split[1] : conceptId + namePostfix;

                TestMaintainableUtil.SetNameableProperties(item, conceptId, conceptName);
                mutable.AddItem(item);
            }
            return mutable.ImmutableInstance;
        }

        public static IConceptSchemeObject LinkConceptToCodelist(IConceptSchemeObject cs, string? conceptId, IMaintainableRefObject refObj)
        {
            IConceptSchemeMutableObject csMut = cs.MutableInstance;
            IConceptMutableObject concept = csMut.GetItemById(conceptId);
            if (concept == null)
            {
                throw new SystemException("Concept not found:" + concept);
            }
            IRepresentationMutableObject rep = new RepresentationMutableCore();
            rep.Representation = new StructureReferenceImpl(refObj, SdmxStructureEnumType.CodeList);
            concept.CoreRepresentation = rep;
            return csMut.ImmutableInstance;
        }

        public static IConceptSchemeObject GenerateConceptSchemeWithTextFormat(string[] concepts, params string[] conceptToApplyTextFormat)
        {
            IConceptSchemeObject conceptScheme = TestConceptUtil.GenerateConceptScheme(concepts);

            IConceptSchemeMutableObject mutable = conceptScheme.MutableInstance;

            foreach (string conceptId in conceptToApplyTextFormat)
            {
                IConceptMutableObject anItem = mutable.GetItemById(conceptId);
                ITextFormatMutableObject tf = new TextFormatMutableCore();
                tf.Pattern = "[A-Z0-9]{1,255}";
                tf.TextType = TextType.GetFromEnum(TextEnumType.String);
                tf.Multilingual = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.True);
                IRepresentationMutableObject rep = new RepresentationMutableCore();
                rep.TextFormat = tf;
                anItem.CoreRepresentation = rep;
            }

            return mutable.ImmutableInstance;
        }
    }
}