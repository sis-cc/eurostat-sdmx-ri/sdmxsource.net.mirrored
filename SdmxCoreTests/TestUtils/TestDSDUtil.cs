// -----------------------------------------------------------------------
// <copyright file="TestDSDUtil.cs" company="EUROSTAT">
//   Date Created : 2023-05-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.TestUtils
{
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.ConceptScheme;
    using Org.Sdmxsource.Util.Extensions;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;

    /// <summary> 
    /// Used to create test Data Structures 
    /// </summary> 
    public class TestDSDUtil
    {
        public static IDataStructureObject GenerateDSD(bool time, bool pm, params string[] dimensionIds)
        {
            return GenerateDSD(dimensionIds.ToList(), null, time, pm, null, null, null, null, null);
        }

        public static IDataStructureObject GenerateDSD(IMaintainableRefObject mRef, bool time, bool pm, params string[] dimensionIds)
        {
            IDataStructureObject dsd = GenerateDSD(dimensionIds.ToList(), null, time, pm, null, null, null, null, null);
            IDataStructureMutableObject dsdMutable = dsd.MutableInstance;
            TestMaintainableUtil.SetMaintainableProperties(dsdMutable, mRef);
            return dsdMutable.ImmutableInstance;
        }

        public static IDataStructureObject AddDimension(IDataStructureObject dsd, string conceptId, string clAcy, string clId, string clVersion, bool isCodelist)
        {
            SdmxStructureEnumType st = isCodelist ? SdmxStructureEnumType.CodeList : SdmxStructureEnumType.ValueList;
            IDataStructureMutableObject dsdMutable = dsd.MutableInstance;
            IStructureReference clRef = new StructureReferenceImpl(clAcy, clId, clVersion, st);
            dsdMutable.AddDimension(CreateConceptReference(conceptId), clRef);
            return dsdMutable.ImmutableInstance;
        }

        public static IDataStructureSuperObject GenerateDSDSb(
            IList<string> dimensions, IList<string> seriesAttributes,
            bool time, bool pm, IList<string> obsAttributes, IList<string> additionalMeasures,
            IDictionary<string, ICodelistObject> representations, string agency, string id, string version)
        {
            IConceptSchemeSuperObject csSb = GenerateConceptScheme(dimensions, seriesAttributes, time, pm, obsAttributes, additionalMeasures, agency, id, version);
            IDataStructureObject dsd = GenerateDSD(dimensions, seriesAttributes, time, pm, obsAttributes, additionalMeasures, agency, id, version);
            IList<IDimensionSuperObject> IdimensionSuperObjects = new List<IDimensionSuperObject>();
            IList<IAttributeSuperObject> attributes = new List<IAttributeSuperObject>();
            IList<IMeasureSuperObject> measures = new List<IMeasureSuperObject>();
            foreach (IDimension dim in dsd.DimensionList.Dimensions)
            {
                IDimensionSuperObject dSb = new DimensionSuperObject(dim, new CodelistSuperObject(representations[dim.Id]), csSb.GetItemById(dim.Id));
                IdimensionSuperObjects.Add(dSb);
            }
            foreach (IAttributeObject attr in dsd.Attributes)
            {
                attributes.Add(new AttributeSuperObject(attr, new CodelistSuperObject(representations[attr.Id]), csSb.GetItemById(attr.Id)));
            }
            foreach (IMeasure meas in dsd.Measures)
            {
                measures.Add(new MeasureSuperObject(meas, new CodelistSuperObject(representations[meas.Id]), csSb.GetItemById(meas.Id)));
            }
            return new DataStructureSuperObject(dsd, IdimensionSuperObjects, attributes, measures);
        }

        public static IConceptSchemeSuperObject GenerateConceptScheme(IList<string> dimensions, IList<string> seriesAttributes, bool time, bool pm, IList<string> obsAttributes, IList<string> additionalMeasures, string agency, string id, string version)
        {
            IList<string> allComponents = new List<string>();
            if (dimensions != null)
            {
                allComponents.AddAll(dimensions);
            }
            if (seriesAttributes != null)
            {
                allComponents.AddAll(seriesAttributes);
            }
            if (obsAttributes != null)
            {
                allComponents.AddAll(obsAttributes);
            }
            if (additionalMeasures != null)
            {
                allComponents.AddAll(additionalMeasures);
            }
            if (time)
            {
                allComponents.Add(DimensionObject.TimeDimensionFixedId);
            }
            if (pm)
            {
                allComponents.Add(PrimaryMeasure.FixedId);
            }
            string[] conceptIds = allComponents.ToArray();
            IConceptSchemeObject cs = TestConceptUtil.GenerateConceptSchemeWithProps(agency, id, version, "-name", conceptIds);
            return new ConceptSchemeSuperObject(cs, new Dictionary<IConceptObject, ICodelistSuperObject>());
        }

        public static IDataStructureObject GenerateDSD(
            IList<string> dimensions, IList<string> seriesAttributes, bool time, bool pm,
            IList<string> obsAttributes, IList<string> additionalMeasures, IMaintainableRefObject dsdRef)
        {
            return GenerateDSD(dimensions, seriesAttributes, time, pm, obsAttributes, additionalMeasures,
                dsdRef.AgencyId, dsdRef.MaintainableId, dsdRef.Version);

        }
        public static IDataStructureObject GenerateDSD(
            IList<string> dimensions, IList<string> seriesAttributes, bool time, bool pm, IList<string> obsAttributes,
            IList<string> additionalMeasures, string agency, string id, string version)
        {
            IDataStructureMutableObject dsdMutable = new DataStructureMutableCore();
            dsdMutable.AgencyId = agency == null ? "TEST" : agency;
            dsdMutable.Id = id == null ? "TEST" : id;
            dsdMutable.Version = version == null ? "1.0" : version;
            dsdMutable.AddName("en", "TEST");
            if (dimensions != null)
            {
                foreach (string dimId in dimensions)
                {
                    dsdMutable.AddDimension(CreateDimension((dimId)));
                }
            }
            if (seriesAttributes != null)
            {
                foreach (string dimId in seriesAttributes)
                {
                    CreateAttribute(dsdMutable, dimId, AttributeAttachmentLevel.DimensionGroup);
                }
            }
            if (obsAttributes != null)
            {
                foreach (string dimId in obsAttributes)
                {
                    CreateAttribute(dsdMutable, dimId, AttributeAttachmentLevel.Observation);
                }
            }
            if (additionalMeasures != null)
            {
                foreach (string dimId in additionalMeasures)
                {
                    dsdMutable.AddMeasure(CreateMeasure(dimId));
                }
            }
            // Note that normally there are no Primary measures in SDMX 3.0.0
            // Assuming this is used to test if we can use a SDMX 2.x DSD in 3.0.0 formats ?
            if (pm)
            {
                //dsdMutable.PrimaryMeasure = new PrimaryMeasureMutableCore();
                dsdMutable.AddMeasure(new MeasureMutableCore() { ConceptRef = CreateConceptReference("OBS_VALUE") });
                //dsdMutable.MeasureList[0].ConceptRef = CreateConceptReference("OBS_VALUE");
            }
            if (time)
            {
                IDimensionMutableObject dim = new DimensionMutableCore();
                dim.Id = "TIME_PERIOD";
                dim.TimeDimension = true;
                dim.ConceptRef = CreateConceptReference("T");
                dsdMutable.AddDimension(dim);
            }
            IDataStructureObject dsd = dsdMutable.ImmutableInstance;

            return dsd;
        }

        private static IDimensionMutableObject CreateDimension(string id)
        {
            IDimensionMutableObject dim = new DimensionMutableCore();
            SetComponentRepresentation(id, dim);
            dim.ConceptRef = CreateConceptReference(id);
            return dim;
        }

        private static void SetComponentRepresentation(string id, IComponentMutableObject comp)
        {
            string[] idSplit = id.Split(":");
            comp.Id = idSplit[0];
            if (idSplit.Length > 1)
            {
                if (idSplit[1].Equals("CL"))
                {  //Codelist
                    comp.SetEnumeration(new StructureReferenceImpl("ACY", "CL_" + idSplit[0], "1.0", SdmxStructureEnumType.CodeList));
                }
                else if (idSplit[1].Equals("VL"))
                {  //Valuelist
                    comp.SetEnumeration(new StructureReferenceImpl("ACY", "VL_" + idSplit[0], "1.0", SdmxStructureEnumType.ValueList));
                }
                else
                {
                    comp.SetTextType(TextType.Values.FirstOrDefault(t => t.Sdmx3.Equals(idSplit[1])));
                }
            }
        }

        public static IDataStructureObject CreateAttribute(IDataStructureObject dsd, string id, AttributeAttachmentLevel attach)
        {
            IDataStructureMutableObject dsdMutable = dsd.MutableInstance;
            CreateAttribute(dsdMutable, id, attach);
            return dsdMutable.ImmutableInstance;
        }

        /// <summary> 
        /// </summary>
        /// <param name=" dsd"> 
        /// </param>
        /// <param name=" id"> 
        /// </param>
        /// <param name=" attachments"> Dimension Ids and/or Measure ids 
        /// </param>
        /// <returns>
        /// </returns> 
        public static IDataStructureObject CreateAttribute(IDataStructureObject dsd, string id, params string[] attachments)
        {
            IDataStructureMutableObject dsdMutable = dsd.MutableInstance;
            IAttributeMutableObject attr = CreateAttribute(dsdMutable, id, AttributeAttachmentLevel.Null);
            foreach (string attach in attachments)
            {
                if (dsd.GetDimension(attach) != null)
                {
                    attr.AddDimensionReferences(attach);
                    if (attr.AttachmentLevel == AttributeAttachmentLevel.Null)
                    {
                        attr.AttachmentLevel = AttributeAttachmentLevel.DimensionGroup;
                    }
                }
                else if (dsd.GetMeasure(attach) != null)
                {
                    attr.AddMeasureRelationship(attach);
                    attr.AttachmentLevel = AttributeAttachmentLevel.Observation;
                }
            }
            return dsdMutable.ImmutableInstance;
        }

        public static IAttributeMutableObject CreateAttribute(IDataStructureMutableObject dsdMutable, string id, AttributeAttachmentLevel attach)
        {
            IAttributeMutableObject attr = new AttributeMutableCore();
            attr.AttachmentLevel = attach;
            if (attach == AttributeAttachmentLevel.DimensionGroup)
            {
                foreach (IDimensionMutableObject dim in dsdMutable.Dimensions)
                {
                    if (!dim.Id.Equals(DimensionObject.TimeDimensionFixedId))
                    {
                        attr.AddDimensionReferences(dim.Id);
                    }
                }
            }
            SetComponentRepresentation(id, attr);
            attr.ConceptRef = CreateConceptReference(id);
            attr.SetMandatory(false);
            dsdMutable.AddAttribute(attr);
            return attr;
        }

        public static IGroupMutableObject CreateGroup(IDataStructureMutableObject dsdMutable, string id, params string[] dimensions)
        {
            IGroupMutableObject group = new GroupMutableCore();
            group.Id = id;
            foreach (string dimId in dimensions)
            {
                group.AddDimensionRef(dimId);
            }
            dsdMutable.AddGroup(group);
            return group;
        }

        public static IDataStructureObject CreateMeasure(IDataStructureObject dsd, string id)
        {
            IDataStructureMutableObject dsdMutable = dsd.MutableInstance;
            dsdMutable.AddMeasure(CreateMeasure(id));
            return dsdMutable.ImmutableInstance;
        }

        public static IMeasureMutableObject CreateMeasure(string id)
        {
            IMeasureMutableObject measure = new MeasureMutableCore();
            measure.Id = id;
            measure.ConceptRef = CreateConceptReference(id);
            return measure;
        }

        public static IMeasureMutableObject CreateMeasure(IDataStructureMutableObject dsdMutable, string id)
        {
            IMeasureMutableObject measure = CreateMeasure(id);
            dsdMutable.AddMeasure(measure);
            return measure;
        }

        public static IStructureReference CreateConceptReference(string id)
        {
            string[] idSplit = id.Split(":");
            id = idSplit[0];
            return new StructureReferenceImpl("TEST", "CS", "1.0", SdmxStructureEnumType.Concept, id);
        }
    }
}