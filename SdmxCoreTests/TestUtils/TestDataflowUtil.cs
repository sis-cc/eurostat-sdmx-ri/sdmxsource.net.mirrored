// -----------------------------------------------------------------------
// <copyright file="TestDataflowUtil.cs" company="EUROSTAT">
//   Date Created : 2023-05-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCoreTests.TestUtils
{
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.DataStructure;
    using SdmxCoreTests.TestInstances;

    public class TestDataflowUtil
    {
        /// <summary> 
        /// Generates a Dataflow with the same agency, id, version as the DSD that it references 
        /// </summary>
        /// <param name=" dsdRef"> 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static IDataflowSuperObject GenerateDataflowSuperObject(bool includeDsAttributes, bool includeCodedSeriesAttributes, bool includeCodedObsAttributes)
        {
            IDataStructureSuperObject dsdSb = TestDSDInstances.GetSuperObject(includeDsAttributes, includeCodedSeriesAttributes, includeCodedObsAttributes);
            return new DataflowSuperObject(GenerateDataflow(dsdSb.BuiltFrom.AsReference), dsdSb);
        }

        /// <summary> 
        /// Generates a Dataflow with the same agency, id, version as the DSD that it references 
        /// </summary>
        /// <param name=" dsdRef"> 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static IDataflowObject GenerateDataflow(IStructureReference refObj)
        {
            IDataflowMutableObject mutable = new DataflowMutableCore();
            IStructureReference dsdRef = new StructureReferenceImpl(refObj, SdmxStructureEnumType.Dsd);
            mutable.DataStructureRef = dsdRef;
            TestMaintainableUtil.SetMaintainableProperties(mutable, refObj);
            return mutable.ImmutableInstance;
        }
    }
}