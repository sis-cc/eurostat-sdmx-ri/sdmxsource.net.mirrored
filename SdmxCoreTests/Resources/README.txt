 //NOTE SDMXSOURCE_SDMXCORE CHANGE

CHANGES WE HAD TO DO DUE IN JSON FILES THAT NEED TO STICK:

 - Because isFinal is not part of v2 schema we needed the following changes:
     - "isFinal" is removed from test json files
     - "isFinal" not written any more from SdmxJsonMaintainableStructureWriterEngineV2

 - Added testMultipleMeasuresNoDefaultMeasureRelationship.json which is similar to testMultipleMeasures.json
  but has no measure relationships for attributes with observation attachment level instead of explicitly including all of them.
   (business logic-wise they should be identical because default measure relationships in that case are all measures. But we want AttributeBean to match the json file instead of automatically add all measure relationships like sdmx core does)

CHANGES DUE TO UNIMPLEMENTED FEATURES THAT WILL NEED TO FIX IN THE FUTURE:

 - Because ValueList is not yet supported we have the removed the related representation from testMultipleMandatoryMeasures.json, testMulipleMeasures.json and testDSDWithComplexContent

        "localRepresentation": {
            "enumeration": "urn:sdmx:org.sdmx.infomodel.codelist.ValueList=ACY:VL_UNIT(1.0)"
        }

