﻿// -----------------------------------------------------------------------
// <copyright file="TestBaseSdmxObjectRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2016-08-02
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureRetrievalTests.
// 
//     SdmxStructureRetrievalTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureRetrievalTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureRetrievalTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxStructureRetrievalTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Moq;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    /// The test base sdmx object retrieval manager.
    /// </summary>
    public class TestBaseSdmxObjectRetrievalManager
    {
        /// <summary>
        /// Tests the gets maintainables.
        /// </summary>
        [Test]
        public void TestGestMaintainables()
        {
            var dsd = BuildDsd();
            var dataflow = new DataflowMutableCore(dsd).ImmutableInstance;
            var conceptScheme = BuildConceptScheme(dsd);
            var mockBaseRetrieval = new Mock<BaseSdmxObjectRetrievalManager> { CallBase = true };
            mockBaseRetrieval.Setup(manager => manager.GetCodelistObjects(It.IsNotNull<IMaintainableRefObject>(), It.IsAny<bool>(), It.IsAny<bool>())).Returns<IMaintainableRefObject, bool, bool>(
                (mref, latest, stub) => new HashSet<ICodelistObject>() { BuildCodelist(mref, stub) });
            mockBaseRetrieval.Setup(manager => manager.GetConceptSchemeObjects(It.IsNotNull<IMaintainableRefObject>(), It.IsAny<bool>(), It.IsAny<bool>())).Returns(conceptScheme);
            mockBaseRetrieval.Setup(manager => manager.GetDataStructureObjects(It.IsNotNull<IMaintainableRefObject>(), It.IsAny<bool>(), It.IsAny<bool>())).Returns(new HashSet<IDataStructureObject>() { dsd });
            mockBaseRetrieval.Setup(manager => manager.GetDataflowObjects(It.IsNotNull<IMaintainableRefObject>(), It.IsAny<bool>(), It.IsAny<bool>())).Returns(new HashSet<IDataflowObject>() { dataflow });

            var restStructureQueryCore = new RESTStructureQueryCore(StructureQueryDetail.GetFromEnum(StructureQueryDetailEnumType.Full), StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.Descendants), null, dataflow.AsReference, false);
            mockBaseRetrieval.Object.GetMaintainables(restStructureQueryCore);
            mockBaseRetrieval.Verify(manager => manager.GetConceptSchemeObjects(It.IsAny<IMaintainableRefObject>(), It.IsAny<bool>(), It.IsAny<bool>()), Times.Exactly(1));
            mockBaseRetrieval.Verify(manager => manager.GetDataStructureObjects(It.IsAny<IMaintainableRefObject>(), It.IsAny<bool>(), It.IsAny<bool>()), Times.Exactly(1));
            mockBaseRetrieval.Verify(manager => manager.GetDataflowObjects(It.IsAny<IMaintainableRefObject>(), It.IsAny<bool>(), It.IsAny<bool>()), Times.Exactly(1));
            mockBaseRetrieval.Verify(manager => manager.GetCodelistObjects(It.IsAny<IMaintainableRefObject>(), It.IsAny<bool>(), It.IsAny<bool>()), Times.Exactly(4));
        }

        /// <summary>
        /// Builds the codelist.
        /// </summary>
        /// <param name="refObject">The reference object.</param>
        /// <param name="stub">if set to <c>true</c> [stub].</param>
        /// <returns>
        /// The <see cref="ISet{ICodelistObject}"/>.
        /// </returns>
        private static ICodelistObject BuildCodelist(IMaintainableRefObject refObject, bool stub)
        {
            var codelist = new CodelistMutableCore() { Id = refObject.MaintainableId, Version = refObject.Version, AgencyId = refObject.AgencyId };
            codelist.AddName("en", refObject.ToString());
            if (!stub)
            {
                for (int i = 0; i < 10; i++)
                {
                    var item = new CodeMutableCore() { Id = "C" + i };
                    item.AddName("en", "Name for C" + i);
                    codelist.AddItem(item);
                }
            }
            else
            {
                codelist.Stub = true;
                codelist.StructureURL = new Uri("http://nowhere.to/be/found");
            }

            return codelist.ImmutableInstance;
        }

        /// <summary>
        /// Builds the concept scheme.
        /// </summary>
        /// <param name="dsd">
        /// The DSD.
        /// </param>
        /// <returns>
        /// The <see cref="ISet{IConceptSchemeObject}"/>.
        /// </returns>
        private static ISet<IConceptSchemeObject> BuildConceptScheme(IDataStructureObject dsd)
        {
            IDictionary<string, IConceptSchemeMutableObject> conceptSchemes = new Dictionary<string, IConceptSchemeMutableObject>(StringComparer.Ordinal);
            foreach (var component in dsd.Components)
            {
                var maintainableRefObject = component.ConceptRef.MaintainableReference;
                IConceptSchemeMutableObject conceptScheme;
                if (!conceptSchemes.TryGetValue(maintainableRefObject.ToString(), out conceptScheme))
                {
                    conceptScheme = new ConceptSchemeMutableCore() { Id = component.ConceptRef.MaintainableId, Version = component.ConceptRef.Version, AgencyId = component.ConceptRef.AgencyId };
                    conceptScheme.AddName("en", maintainableRefObject.ToString());
                    conceptSchemes.Add(maintainableRefObject.ToString(), conceptScheme);
                }

                var concept = new ConceptMutableCore() { Id = component.ConceptRef.IdentifiableIds.Last() };
                concept.AddName("en", component.ConceptRef.FullId);
                conceptScheme.AddItem(concept);
            }

            return new HashSet<IConceptSchemeObject>(conceptSchemes.Values.Select(o => o.ImmutableInstance));
        }

        /// <summary>
        /// Builds the DSD.
        /// </summary>
        /// <returns>
        /// The <see cref="IDataStructureObject" />.
        /// </returns>
        private static IDataStructureObject BuildDsd()
        {
            IDataStructureMutableObject dsdMutableObject = new DataStructureMutableCore { AgencyId = "TEST", Id = "TEST_DSD", Version = "1.0" };
            dsdMutableObject.AddName("en", "Test data");

            // FREQ="Q" ADJUSTMENT="N" STS_ACTIVITY="A" 
            dsdMutableObject.AddDimension(
                new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "FREQ"),
                new StructureReferenceImpl("SDMX", "CL_FREQ", "1.0", SdmxStructureEnumType.CodeList));
            dsdMutableObject.AddDimension(
                new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "ADJUSTMENT"),
                new StructureReferenceImpl("SDMX", "CL_ADJUSTMENT", "1.0", SdmxStructureEnumType.CodeList));
            dsdMutableObject.AddDimension(
                new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "STS_ACTIVITY"),
                new StructureReferenceImpl("STS", "CL_STS_ACTIVITY", "1.0", SdmxStructureEnumType.CodeList));
            dsdMutableObject.AddDimension(
                new DimensionMutableCore { ConceptRef = new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "TIME_PERIOD"), TimeDimension = true });

            dsdMutableObject.AddPrimaryMeasure(new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "OBS_VALUE"));

            var attributeMutableObject = dsdMutableObject.AddAttribute(
                new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "DECIMALS"),
                new StructureReferenceImpl("STS", "CL_DECIMALS", "1.0", SdmxStructureEnumType.CodeList));
            attributeMutableObject.AttachmentLevel = AttributeAttachmentLevel.DimensionGroup;
            attributeMutableObject.DimensionReferences.AddAll(new[] { "FREQ", "ADJUSTMENT", "STS_ACTIVITY" });
            attributeMutableObject.AssignmentStatus = "Mandatory";
            return dsdMutableObject.ImmutableInstance;
        }
    }
}