// -----------------------------------------------------------------------
// <copyright file="AbstractStructureWriterEngine.cs" company="EUROSTAT">
//   Date Created : 2023-05-10
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Core.Sdmx.Engine.Structure
{
    using System.Collections.Generic;
    using System.Linq;
    using Io.Sdmx.Core.Sdmx.Api.Engine.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Util.Extensions;

    public abstract class AbstractStructureWriterEngine
    {
        private ISet<SdmxStructureType> _supportedTypes;

        protected AbstractStructureWriterEngine(bool include, params SdmxStructureType[] types)
        {
            if (types != null)
            {
                ISet<SdmxStructureType> supportedTypes = new HashSet<SdmxStructureType>();
                foreach (SdmxStructureType st in types)
                {
                    supportedTypes.Add(st);
                }
                CreateSupportedTypes(include, supportedTypes);
            }
        }

        protected AbstractStructureWriterEngine(bool include, ICollection<SdmxStructureType> types)
        {
            CreateSupportedTypes(include, types);
        }

        protected void CreateSupportedTypes(bool include, ICollection<SdmxStructureType> types)
        {
            var supportedTypes = new HashSet<SdmxStructureType>();
            if (include)
            {
                supportedTypes.AddAll(types);
            }
            else
            {
                supportedTypes = new HashSet<SdmxStructureType>(SdmxStructureType.MaintainableStructureTypes.Where(maintType => !types.Contains(maintType)));
            }
            _supportedTypes = supportedTypes;
        }

        public ISet<SdmxStructureType> SupportedTypes => _supportedTypes;

        public virtual bool IsSupportedType(IMaintainableObject structureType)
        {
            if (structureType == null)
            {
                return false;
            }
            return SupportedTypes.Contains(structureType.StructureType);
        }
    }
}