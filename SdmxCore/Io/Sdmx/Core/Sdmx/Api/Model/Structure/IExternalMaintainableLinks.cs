// -----------------------------------------------------------------------
// <copyright file="IExternalMaintainableLinks.cs" company="EUROSTAT">
//   Date Created : 2023-05-10
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Core.Sdmx.Api.Model.Structure
{
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary> 
    /// Links that are not maintained on the maintainable sdmxObject, but attached after be another process, for example reference metadata 
    /// </summary> 
    public interface IExternalMaintainableLinks
    {
        /// <summary> 
        /// </summary>
        /// <returns>
        /// the maintainable that these links are for 
        /// </returns> 
        string MaintainableUrn { get; }

        /// <summary> 
        /// </summary>
        /// <returns>
        /// set of linked identifiables 
        /// </returns> 
        ISet<string> LinkedIdentifiable { get; }

        /// <summary> 
        /// </summary>
        /// <param name=" identifableUrn"> 
        /// </param>
        /// <returns>
        /// all links for an identifiable, or an empty set if none exist 
        /// </returns> 
        ISet<ILinkObject> GetLinks(string identifableUrn);
    }
}