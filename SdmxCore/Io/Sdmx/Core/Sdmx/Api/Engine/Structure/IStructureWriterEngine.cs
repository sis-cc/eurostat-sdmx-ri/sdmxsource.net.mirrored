// -----------------------------------------------------------------------
// <copyright file="IStructureWriterEngine.cs" company="EUROSTAT">
//   Date Created : 2023-05-10
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Core.Sdmx.Api.Engine.Structure
{
    using System.Collections.Generic;
    using System.IO;
    using Io.Sdmx.Core.Sdmx.Api.Model.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary> 
    /// Classes that support this interface can write one or many structures. 
    /// <p/> 
    /// The location and format that the structures are written to and in, are implementation dependent. 
    /// </summary> 
    public interface IStructureWriterEngine
    {
        /// <summary> 
        /// Writes the objects to the output location in the format specified by the implementation 
        /// </summary>
        /// <param name=" objects"> 
        /// </param>
        /// <param name=" additionalLinks"> optional additional links, the map key is an identifiable 
        /// </param>
        /// <param name=" schemaVersion"> 
        /// </param> 
        void WriteStructures(ISdmxObjects objects, IDictionary<string, IExternalMaintainableLinks> additionalLinks, Stream outputStream);

        /// <summary> 
        /// Writes the sdmxObject out to the output location in the format specified by the implementation 
        /// </summary>
        /// <param name=" objects"> 
        /// </param>
        /// <param name=" schemaVersion"> 
        /// </param> 
        void WriteStructure(IMaintainableObject sdmxObject, IExternalMaintainableLinks additionalLinks, Stream outputStream);

        /// <summary> 
        /// Returns true if the given structure type is a supported output format for this factory 
        /// </summary>
        /// <param name=" structureType"> 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        bool IsSupportedType(IMaintainableObject structure);

        /// <summary> 
        /// Returns an immutable Set of supported structure type for this factory 
        /// </summary>
        /// <returns>
        /// 
        /// </returns> 
        ISet<SdmxStructureType> SupportedTypes { get; }
    }
}