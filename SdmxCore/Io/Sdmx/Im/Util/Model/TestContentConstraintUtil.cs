namespace Io.Sdmx.Im.Util.Model
{
    using System;
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
    public class TestContentConstraintUtil
    {
        public static IContentConstraintMutableObject Create(string agencyId, string id, string version, params IStructureReference [] attach)
        {
            return Create(agencyId, id, version, false, attach);
        }

        public static IContentConstraintMutableObject Create(string agencyId, string id, string version, bool isDefiningActualData, params IStructureReference[] attach)
        {
            IContentConstraintMutableObject mutable = new ContentConstraintMutableCore();
            mutable.AgencyId = agencyId;
            mutable.Id = id;
            mutable.Version =  version;
            mutable.AddName("en", id + " Name");
            mutable.IsDefiningActualDataPresent = isDefiningActualData;
            if (attach != null)
            {
                IConstraintAttachmentMutableObject cAttach = new ContentConstraintAttachmentMutableCore();
                foreach (IStructureReference currentAttach in attach)
                {
                    cAttach.AddStructureReference(currentAttach);
                }
                mutable.ConstraintAttachment = cAttach; 
            }
            return mutable;
        }

        public static IContentConstraintObject AddCubeRegion(IContentConstraintObject bean, bool includeMode, Dictionary<string, List<string>> data)
        {
            IContentConstraintMutableObject mutable = bean.MutableInstance;
            AddCubeRegion(mutable, includeMode, data);
            return mutable.ImmutableInstance;
        }

        public static ICubeRegionMutableObject AddCubeRegion(IContentConstraintMutableObject mutable, bool includeMode, Dictionary<string, List<string>> data)
        {
            ICubeRegionMutableObject cubeRegion;
            if (includeMode)
            {
                cubeRegion = mutable.IncludedCubeRegion;
                if (cubeRegion == null)
                {
                    cubeRegion = new CubeRegionMutableCore();
                    mutable.IncludedCubeRegion = cubeRegion;
                }
            }
            else
            {
                cubeRegion = mutable.ExcludedCubeRegion;
                if (cubeRegion == null)
                {
                    cubeRegion = new CubeRegionMutableCore();
                    mutable.ExcludedCubeRegion = cubeRegion;
                }
            }

            foreach (string key in data.Keys)
            {
                KeyValuesMutableImpl keyValues = new KeyValuesMutableImpl();
                keyValues.Id = key;
                keyValues.KeyValues = data[key];
                cubeRegion.AddKeyValue(keyValues);
            }
            return cubeRegion;
        }

        public static IContentConstraintObject AddKeyset(IContentConstraintObject bean, bool includeMode, params string[] constrainedKeys)
        {
            IContentConstraintMutableObject mutable = bean.MutableInstance;
            AddKeyset(mutable, includeMode, constrainedKeys);
            return mutable.ImmutableInstance;
        }

        /// <summary>
        /// If the dimension ID is prefixed with % then the remove prefix boolean will be set to true
        /// </summary>
        /// <param name="mutable"></param>
        /// <param name="includeMode"></param>
        /// <param name="constrainedKeys"></param>
        /// <returns></returns>
        public static IConstraintDataKeySetMutableObject AddKeyset(IContentConstraintMutableObject mutable, bool includeMode, params string[]constrainedKeys)
        {
            IConstraintDataKeySetMutableObject keyset = new ConstraintDataKeySetMutableCore();

            foreach (string currentKey in constrainedKeys)
            {
                IConstrainedDataKeyMutableObject cds = new ConstrainedDataKeyMutableCore();
                keyset.AddConstrainedDataKey(cds);
                string[] dimAndAtt = currentKey.Split(';');

                foreach (string currentKv in dimAndAtt[0].Split(','))
                {
                    cds.AddKeyValue(GetConstrainedKeyValue(currentKv));
                }
                if (dimAndAtt.Length > 1)
                {
                    foreach (string currentKv in dimAndAtt[1].Split(',')) 
                    {
                        cds.AddComponentValues(GetConstrainedKeyValue(currentKv));
                    }
                }
            }
            if (includeMode)
            {
                mutable.IncludedSeriesKeys = keyset;
            }
            else
            {
                mutable.ExcludedSeriesKeys = keyset;
            }
            return keyset;
        }

        /// <summary>
        /// Syntax [%][DIMID]:[CODEID] example
        /// %FREQ:A 	(remove prefix=true)
        /// FREQ:A 	(remove prefix=false)
        /// </summary>
        /// <param name="currentKv"></param>
        /// <returns></returns>
        private static ConstrainedKeyValue GetConstrainedKeyValue(String currentKv)
        {
            string[] kv = currentKv.Split(':');
            string dimId = kv[0].Trim();
            bool removePrefix = false;
            if (dimId.StartsWith("%"))
            {
                dimId = dimId.Substring(1);
                removePrefix = true;
            }
            return ConstrainedKeyValue.GetInstance(dimId, removePrefix, kv[1]);
        }
    }
}
