
using System;
using System.Collections.Generic;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Util.Date;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using Org.Sdmxsource.Util;
using Org.Sdmxsource.Util.Extensions;
using SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2;

namespace SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2.Util
{
    public class SdmxJsonRepresentationUtil
    {
        public static IRepresentationMutableObject ReadRepresentation(JsonReader jReader)
        {
            var representation = new RepresentationMutableCore();
            ReadRepresentation(representation, jReader);
            return representation;
        }

        public static void ReadRepresentation(IRepresentationMutableObject representation, JsonReader jReader)
        {
            string fieldName;
            string value;
            while (jReader.MoveNext())
            {
                if (jReader.IsEndObject())
                {
                    break;
                }
                fieldName = jReader.GetCurrentFieldName();
                switch (fieldName)
                {
                    case "format":
                    case "textFormat":
                    case "enumerationFormat":
                        representation.TextFormat = ReadTextFormat(jReader);
                        break;
                    case "enumeration":
                        value = jReader.GetValueAsString();
                        if (ObjectUtil.ValidString(value))
                        {
                            var representationRef = new StructureReferenceImpl(value);
                            representation.Representation = representationRef;
                        }
                        break;
                    case "minOccurs":
                        representation.MinOccurs = jReader.GetValueAsInt();
                        break;
                    case "maxOccurs":
                        // NOTE SDMXSOURCE_SDMXCORE CHANGE
                        representation.MaxOccurs = new FiniteOccurenceObjectCore(jReader.GetValueAsInt());
                        break;
                }
            }
        }


        public static List<ITextTypeWrapperMutableObject> ReadLocalizedStrings(JsonReader jReader)
        {
            List<ITextTypeWrapperMutableObject> names = new List<ITextTypeWrapperMutableObject>();
            String fieldName;
            String value;
            while (jReader.MoveNext())
            {
                if (jReader.IsEndObject())
                {
                    break;
                }
                fieldName = jReader.GetCurrentFieldName();
                value = jReader.GetValueAsString();
                names.Add(new TextTypeWrapperMutableCore(fieldName, value));
            }
            return names;
        }

        public static ISentinelValueMutableObject ReadSentinelValue(JsonReader jReader)
        {
            SentinelValueMutableCore sentinelMutableBean = new SentinelValueMutableCore();
            String fieldName;
            String value;
            while (jReader.MoveNext())
            {
                if (jReader.IsEndObject())
                {
                    break;
                }
                fieldName = jReader.GetCurrentFieldName();
                value = jReader.GetValueAsString();
                switch (fieldName)
                {
                    case "value":
                        sentinelMutableBean.Value = value;
                        break;
                    case "names":
                        sentinelMutableBean.Names = ReadLocalizedStrings(jReader);
                        break;
                    case "descriptions":
                        sentinelMutableBean.Descriptions = ReadLocalizedStrings(jReader);
                        break;
                    case "name":
                    case "description":
                        break;
                }
            }
            return sentinelMutableBean;
        }

        public static List<ISentinelValueMutableObject> ReadSentinelValues(JsonReader jReader)
        {
            List<ISentinelValueMutableObject> sentinelMutableList = new List<ISentinelValueMutableObject>();
            while (jReader.MoveNext())
            {
                if (jReader.IsEndArray())
                {
                    break;
                }
                sentinelMutableList.Add(ReadSentinelValue(jReader));
            }
            return sentinelMutableList;
        }
        public static ITextFormatMutableObject ReadTextFormat(JsonReader jReader)
        {
            var tf = new TextFormatMutableCore();
            string fieldName;
            string value;
            while (jReader.MoveNext())
            {
                if (jReader.IsEndObject())
                {
                    break;
                }
                fieldName = jReader.GetCurrentFieldName();
                value = jReader.GetValueAsString();
                switch (fieldName)
                {
                    case "textType":
                        tf.TextType = TextType.ParseString(value);
                        break;
                    case "startTime":
                        // NOTE SDMXSOURCE_SDMXCORE CHANGE
                        tf.StartTime = new SdmxDateCore(DateUtil.FormatDate(value, true));
                        break;
                    case "endTime":
                        tf.EndTime = new SdmxDateCore(DateUtil.FormatDate(value, false));
                        break;
                    case "isSequence":
                        tf.Sequence = TertiaryBool.ParseBoolean(Convert.ToBoolean(value));
                        break;
                    case "minLength":
                        tf.MinLength = int.Parse(value);
                        break;
                    case "maxLength":
                        tf.MaxLength = int.Parse(value);
                        break;
                    case "startValue":
                        tf.StartValue = decimal.Parse(value);
                        break;
                    case "endValue":
                        tf.EndValue = decimal.Parse(value);
                        break;
                    case "maxValue":
                        tf.MaxValue = decimal.Parse(value);
                        break;
                    case "minValue":
                        tf.MinValue = decimal.Parse(value);
                        break;
                    case "interval":
                        tf.Interval = decimal.Parse(value);
                        break;
                    case "timeInterval":
                        tf.TimeInterval = value;
                        break;
                    case "decimals":
                        tf.Decimals = int.Parse(value);
                        break;
                    case "pattern":
                        tf.Pattern = value;
                        break;
                    case "isMultiLingual":
                        tf.Multilingual = TertiaryBool.ParseBoolean(Convert.ToBoolean(value));
                        break;
                    case "sentinelValues":
                        List<ISentinelValueMutableObject> sentinelMutableList = ReadSentinelValues(jReader);
                        tf.SentinelValues.AddAll(sentinelMutableList);
                        break;
                }
            }
            return tf;
        }
    }
}