using System;
using System.Collections.Generic;
using System.Linq;
using Io.Sdmx.Api.Singleton;
using Io.Sdmx.Utils.Core.Application;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using Org.Sdmxsource.Util.Extensions;
using SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2.Util;

namespace SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2
{
    public class SdmxJsonCodelistReaderEngineV2 : SdmxJsonItemSchemeReaderEngineV2<ICodelistObject, ICodeMutableObject, ICodelistMutableObject>, IFusionSingleton
    {
        private static SdmxJsonCodelistReaderEngineV2 _instance;

        public static SdmxJsonCodelistReaderEngineV2 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SdmxJsonCodelistReaderEngineV2();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }

        public void DestroyInstance()
        {
            _instance = null;
        }

        private SdmxJsonCodelistReaderEngineV2()
            : base(SdmxStructureEnumType.CodeList)
        {
        }

        protected override bool ProcessItemSchemeField(ICodelistMutableObject itmSch, JsonReader jReader, string fieldName, string value)
        {
            if (fieldName.Equals("codelistExtensions"))
            {
                while (jReader.MoveNext())
                {
                    if (jReader.IsStartObject())
                    {
                        ProcessCodelistExtension(itmSch, jReader);
                    }
                    if (jReader.IsEndArray())
                    {
                        break;
                    }
                }
                return true;
            }
            return base.ProcessItemSchemeField(itmSch, jReader, fieldName, value);
        }

        private void ProcessCodelistExtension(ICodelistMutableObject itmSch, JsonReader jReader)
        {
            var rule = new CodelistInheritanceRuleMutableCore();
            while (jReader.MoveNext())
            {
                if (jReader.IsEndArray())
                {
                    break;
                }

                string field = jReader.GetCurrentFieldName();
                switch (field)
                {
                    case "prefix":
                        rule.Prefix = jReader.GetValueAsString();
                        break;
                    case "codelist":
                        rule.CodelistRef = new StructureReferenceImpl(jReader.GetValueAsString());
                        break;
                    case "exclusiveCodeSelection":
                        HandleCodeSelection(jReader, rule, true);
                        break;
                    case "inclusiveCodeSelection":
                        HandleCodeSelection(jReader, rule, false);

                        break;
                }
            }
            itmSch.CodelistExtensions.Add(rule);
        }

        private void HandleCodeSelection(JsonReader jReader, CodelistInheritanceRuleMutableCore rule, bool exclusive)
        {
            //NOTE SDMXSOURCE_SDMXCORE CHANGE: USING EXISTING ICodelistInheritanceRuleBean instead of sdmxcore's ICodelistInheritanceRuleBean
            List<IMemberValueMutableObject> memberValues = new List<IMemberValueMutableObject>();
            ProcessCodelistExtensionValues(memberValues, jReader);
            var codeSelection = new CodeSelectionMutableCore()
            {
                IsInclusive = !exclusive,

            };
            codeSelection.MemberValues.AddAll(memberValues);
            rule.CodeSelection = codeSelection;
        }

        private void ProcessCodelistExtensionValues(List<IMemberValueMutableObject> memberValues, JsonReader jReader)
        {
            while (jReader.MoveNext())
            {
                if (jReader.IsEndObject())
                {
                    return;
                }
                string field = jReader.GetCurrentFieldName();
                switch (field)
                {
                    case "wildcardedMemberValues":
                        memberValues.AddRange(jReader.ReadStringArray().Select(s =>
                        {
                            var m = new MemberValueMutableCore();
                            m.Value = s;
                            m.Cascade = CascadeSelection.False;
                            return m;
                        }).ToList());
                        break;
                    case "memberValues":
                        ProcessCodelistMemberValues(memberValues, jReader);
                        break;
                }
            }
        }

        private void ProcessCodelistMemberValues(List<IMemberValueMutableObject> includedValues, JsonReader jReader)
        {
            while (jReader.MoveNext())
            {
                if (jReader.IsEndArray())
                {
                    return;
                }
                if (jReader.IsStartObject())
                {
                    ProcessCodelistMemberValue(includedValues, jReader);
                }
            }
        }

        private void ProcessCodelistMemberValue(List<IMemberValueMutableObject> includedValues, JsonReader jReader)
        {
            bool cascade = false;
            string value = null;
            while (jReader.MoveNext())
            {
                if (jReader.IsEndObject())
                {
                    break;
                }
                string field = jReader.GetCurrentFieldName();
                switch (field)
                {
                    case "value":
                        value = jReader.GetValueAsString();
                        break;
                    case "cascadeValues":
                        cascade = jReader.GetValueAsBoolean().Value;
                        break;
                }
            }
            if (value != null)
            {
                var memberValue = new MemberValueMutableCore();
                memberValue.Value = value;
                memberValue.Cascade = cascade ? CascadeSelection.True : CascadeSelection.False;
                includedValues.Add(memberValue);
            }
        }

        private ICodeMutableObject ReadCodeBean(JsonReader jReader)
        {
            var aCode = new CodeMutableCore();
            while (jReader.MoveNext())
            {
                if (jReader.IsEndObject())
                {
                    break;
                }
                if (SdmxJsonNameableUtil.ProcessBean(aCode, jReader))
                {
                    continue;
                }
                string fieldName = jReader.GetCurrentFieldName();
                string value = jReader.GetValueAsString();
                if (fieldName.Equals("parent"))
                {
                    aCode.ParentCode = value;
                }
            }
            return aCode;
        }

        protected override List<ICodeMutableObject> ReadItems(JsonReader jReader)
        {
            List<ICodeMutableObject> returnList = new List<ICodeMutableObject>();
            while (jReader.MoveNext())
            {
                if (jReader.IsEndArray())
                {
                    break;
                }
                if (jReader.IsStartObject())
                {
                    var aCode = ReadCodeBean(jReader);
                    returnList.Add(aCode);
                }
            }

            return returnList;
        }

        protected override ICodelistMutableObject GetSchemeMut()
        {
            return new CodelistMutableCore();
        }

        protected override string GetItemLabel()
        {
            return "codes";
        }
    }
}
