/*******************************************************************************
 *  This software and the accompanying materials of the FMR tools are licensed under the
 *  Apache Licence, version 2.0 (the “Licence”) which accompanies the distribution and is
 *  available at: http://www.apache.org/licenses/LICENSE-2.0
 *  You may not use this file except in compliance with the Licence.
 *  This file is part of the FMR tools and source code libraries.
 *  The software is distributed on an “AS IS” BASIS, WITHOUT WARRANTIES OR
 *  CONDITIONS OF ANY KIND, either express or implied. See the Apache 2.0 Licence for
 *  more details regarding permissions and limitations under the Licence.
 * Copyright © 2022, Bank for International Settlements
 ******************************************************************************/

using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader;
/**
* Any Reader implemented this interface will be assumed to be part of the official JSON format. This will automatically be picked up by the JsonStructureReaderFormatFactory
*
* @param <T>
*/

namespace SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2
{
    public interface SdmxJsonMaintainableBeanReaderEngine<T> : IJsonMaintainableBeanReaderEngine<T> where T : IMaintainableObject
    {

    }
}