using System;
using System.Collections.Generic;
using System.IO;
using Io.Sdmx.Api.Singleton;
using Io.Sdmx.Utils.Core.Application;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2.Util;

namespace SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2
{
    public class SdmxJsonDataflowReaderEngineV2 : SdmxJsonMaintainableBeanReaderEngineV2<IMaintainableObject>, IFusionSingleton
    {
        private static SdmxJsonDataflowReaderEngineV2 _instance;
        public static SdmxJsonDataflowReaderEngineV2 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SdmxJsonDataflowReaderEngineV2();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }

        public void DestroyInstance()
        {
            _instance = null;
        }

        private SdmxJsonDataflowReaderEngineV2() : base(SdmxStructureEnumType.Dataflow)
        {
        }

        protected override IMaintainableObject Build(JsonReader jReader, ISdmxObjectRetrievalManager beanRetrievalManager)
        {
            var df = new DataflowMutableCore();
            while (jReader.MoveNext())
            {
                if (jReader.IsEndObject())
                {
                    break;
                }
                if (SdmxJsonMaintainableUtil.ProcessBean(df, jReader))
                {
                    continue;
                }
                string fieldName = jReader.GetCurrentFieldName();
                if (fieldName.Equals("structure"))
                {
                    string value = jReader.GetValueAsString();
                    df.DataStructureRef = new StructureReferenceImpl(value);
                }
            }
            return df.ImmutableInstance;
        }
    }
}
