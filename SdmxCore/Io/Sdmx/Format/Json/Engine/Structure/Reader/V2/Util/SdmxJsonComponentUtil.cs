
using System;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2;

namespace SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2.Util
{
    public class SdmxJsonComponentUtil
    {
        public static bool ProcessBean(IComponentMutableObject bean, JsonReader jReader)
        {
            string fieldName = jReader.GetCurrentFieldName();
            if (SdmxJsonIdentifiableUtil.ProcessBean(bean, jReader))
            {
                return true;
            }

            if (fieldName.Equals("usage"))
            {
                string value = jReader.GetValueAsString().ToLower();
                if (!value.Equals("optional") && !value.Equals("mandatory"))
                {
                    throw new Exception("'usage' has illegal value of:" + value);
                }
                bean.SetMandatory(!value.Equals("optional"));
            }
            else if (fieldName.Equals("localRepresentation"))
            {
                var representation = bean.Representation;
                if (representation == null)
                {
                    representation = new RepresentationMutableCore();
                    bean.Representation = representation;
                }
                SdmxJsonRepresentationUtil.ReadRepresentation(representation, jReader);
                return true;
            }
            else if (fieldName.Equals("conceptIdentity"))
            {
                var conceptRef = new StructureReferenceImpl(jReader.GetValueAsString());
                bean.ConceptRef = conceptRef;
                return true;
            }
            return false;
        }
    }
}