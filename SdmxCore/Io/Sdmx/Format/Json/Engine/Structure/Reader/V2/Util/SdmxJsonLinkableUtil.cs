using System;
using System.Collections.Generic;
using Io.Sdmx.Api.Sdmx.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Util;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2;

namespace SdmxCore.Io.Format.Json.Engine.Structure.Reader.V2.Util.Parsers
{
    public class SdmxJsonLinkableUtil
    {
        public static bool ProcessBean(IMutableObject bean, JsonReader jReader)
        {
            string fieldName = jReader.GetCurrentFieldName();
            if (fieldName.Equals("links"))
            {
                while (jReader.MoveNext())
                {
                    if (jReader.IsEndArray())
                    {
                        break;
                    }
                    if (jReader.IsStartObject())
                    {
                        Dictionary<string, string> kvMap = jReader.ReadStringMap();
                        kvMap.TryGetValue("rel", out string relStr);

                        if (relStr == null)
                        {
                            continue;
                        }
                        if (relStr.Equals(LinkRelEnumType.Self.ToString()))
                        {
                            continue;
                        }
                        if (kvMap.TryGetValue("uri", out string uri) && ObjectUtil.ValidString(uri))
                        {
                            if (bean is IIdentifiableMutableObject identifiableBean)
                            {
                                //TO DO the test data doesn't have proper URI
                                //identifiableBean.Uri = new Uri(uri);
                            }
                        }


                        if (kvMap.TryGetValue("href", out string href) && ObjectUtil.ValidString(href))
                        {
                            if (bean is IMaintainableMutableObject maintainableBean)
                            {
                                // NOTE SDMXSOURCE_SDMXCORE CHANGE
                                maintainableBean.ExternalReference = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.True);
                                maintainableBean.StructureURL = new Uri(href);
                            }
                        }
                    }
                }
                return true;
            }
            return false;
        }
    }
}