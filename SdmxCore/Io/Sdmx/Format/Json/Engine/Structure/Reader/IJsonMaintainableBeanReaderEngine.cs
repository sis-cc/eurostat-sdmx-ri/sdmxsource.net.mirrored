/*******************************************************************************
 *  This software and the accompanying materials of the FMR tools are licensed under the
 *  Apache Licence, version 2.0 (the “Licence”) which accompanies the distribution and is
 *  available at: http://www.apache.org/licenses/LICENSE-2.0
 *  You may not use this file except in compliance with the Licence.
 *  This file is part of the FMR tools and source code libraries.
 *  The software is distributed on an “AS IS” BASIS, WITHOUT WARRANTIES OR
 *  CONDITIONS OF ANY KIND, either express or implied. See the Apache 2.0 Licence for
 *  more details regarding permissions and limitations under the Licence.
 * Copyright © 2022, Bank for International Settlements
 ******************************************************************************/

using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2;
using System.Collections.Generic;
using Xml.Schema.Linq;

namespace SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader
{
    /**
     * Base interface for both new and custom JSON formats
     *
     * @param <T>
     */
    public interface IJsonMaintainableBeanReaderEngine<T> where T : IMaintainableObject
    {

        /**
         * Builds a maintainable bean from a JsonReader
         * @param reader
         * @return the maintainable bean
         */
        List<T> BuildMaintainable(JsonReader reader, ISdmxObjectRetrievalManager beanRetrievalManager);

        /**
         * Returns the supported structure type this reader can read
         * @return
         */
        SdmxStructureEnumType SupportedType { get; }

        /**
         * If the construction of this maintainable type relies on other maintainable types being constructed first, then this returns a set of
         * structure types this relies on.  Will return any empty Set if there is no reliance on other types.
         * @return
         */
        HashSet<SdmxStructureEnumType> GetReliesOn();
    }
}