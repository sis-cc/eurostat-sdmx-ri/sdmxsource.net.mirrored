using Org.Sdmxsource.Sdmx.Util.Date;
using SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2;
using System;

namespace SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2.Util
{
    public class SdmxJsonDateUtil
    {
        public static DateTime ReadDate(JsonReader jReader)
        {
            string dateString = jReader.GetValueAsString();
            return DateUtil.FormatDate(dateString, true);
        }
    }
}