using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;

namespace SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2.Util
{
    public class SdmxJsonIdentifiableUtil
    {
        public static bool ProcessBean(IIdentifiableMutableObject ident, JsonReader jReader)
        {
            return ProcessAttributes(ident, jReader) || SdmxJsonAnnotableUtil.ProcessBean(ident, jReader);
        }

        /**
         * Populates the Identifiable with 'id' and 'uri' attribute values read from the current JSON node.
         * @param identifiableBean
         * @param jReader
         */
        private static bool ProcessAttributes(IIdentifiableMutableObject identifiableBean, JsonReader jReader)
        {
            string fieldName = jReader.GetCurrentFieldName();
            string value = jReader.GetValueAsString();
            if (fieldName.Equals("id"))
            {
                identifiableBean.Id = value;
                return true;
            }
            return false;
        }
    }
}