

using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2;

namespace SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2.Util
{
    public class SdmxJsonMaintainableUtil
    {
        public static bool ProcessBean(IMaintainableMutableObject maint, JsonReader jReader)
        {
            return GenericJsonReaderUtil.ProcessLinks(maint, jReader) || SdmxJsonNameableUtil.ProcessBean(maint, jReader) || ProcessAttributes(maint, jReader);
        }

        private static bool ProcessAttributes(IMaintainableMutableObject maint, JsonReader jReader)
        {
            string fieldName = jReader.GetCurrentFieldName();
            string value = jReader.GetValueAsString();

            if (fieldName.Equals("isExternalReference"))
            {
                maint.ExternalReference = TertiaryBool.ParseBoolean(bool.Parse(value));
                return true;
            }
            if (fieldName.Equals("agencyID"))
            {
                maint.AgencyId = value;
                return true;
            }
            if (fieldName.Equals("isFinal"))
            {
                maint.FinalStructure = TertiaryBool.ParseBoolean(bool.Parse(value));
                return true;
            }
            if (fieldName.Equals("version"))
            {
                maint.Version = value;
                return true;
            }
            if (fieldName.Equals("validFrom"))
            {
                maint.StartDate = SdmxJsonDateUtil.ReadDate(jReader);
                return true;
            }
            if (fieldName.Equals("validTo"))
            {
                maint.EndDate = SdmxJsonDateUtil.ReadDate(jReader);
                return true;
            }
            return false;
        }
    }
}