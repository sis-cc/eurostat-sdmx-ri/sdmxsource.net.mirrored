///*******************************************************************************
// *  This software and the accompanying materials of the FMR tools are licensed under the
// *  Apache Licence, version 2.0 (the “Licence”) which accompanies the distribution and is
// *  available at: http://www.apache.org/licenses/LICENSE-2.0
// *  You may not use this file except in compliance with the Licence.
// *  This file is part of the FMR tools and source code libraries.
// *  The software is distributed on an “AS IS” BASIS, WITHOUT WARRANTIES OR
// *  CONDITIONS OF ANY KIND, either express or implied. See the Apache 2.0 Licence for
// *  more details regarding permissions and limitations under the Licence.
// * Copyright © 2022, Bank for International Settlements
// ******************************************************************************/
//using Io.Sdmx.Utils.Core.Thread;
//using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System;
//using Xml.Schema.Linq;

//package io.sdmx.utils.sdmx.structure;

//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Locale;
//import java.util.Set;

//import org.sdmxsource.sdmx.api.exception.SdmxSemmanticException;
////import io.sdmx.api.http.ClientRequest;
//import org.sdmxsource.sdmx.api.model.beans.base.IdentifiableBean;
//import org.sdmxsource.sdmx.api.model.beans.base.TextTypeWrapper;
////import org.sdmxsource.sdmx.api.model.mutable.base.TextTypeWrapperMutableBean;
////import io.sdmx.utils.core.object.ObjectUtil;
//import io.sdmx.utils.core.thread.ThreadLocalUtil;
//import org.apache.logging.log4j.Logger;
//import org.apache.logging.log4j.LogManager;

//public class TextTypeUtil
//{
//    private static final Logger LOG = LogManager.getLogger(TextTypeUtil.class);

//    public static final String LOCALE_FILTER = "restrict-locale";


//    /**
//     * Gets the locale on the ThreadLocal for the user, this locale will be used as a way to filter out other locales when
//     * writing structures
//     */
//    public static Locale getLocaleFilter()
//    {
//        return (Locale)ThreadLocalUtil.retrieveFromThread(LOCALE_FILTER);
//    }

//    /**
//     * Returns true if there is a locale filter set
//     */
//    public static boolean hasLocaleFilter()
//    {
//        return ThreadLocalUtil.contains(LOCALE_FILTER);
//    }

//    /**
//     * Sets the locale on the ThreadLocal for the user, this locale will be used as a way to filter out other locales when
//     * writing structures
//     * @param locale
//     */
//    public static void setLocaleFilter(Locale locale)
//    {
//        if (locale == null)
//        {
//            clearLocaleFilter();
//        }
//        else
//        {
//            LOG.debug("Set filter on locale : " + locale);
//            ThreadLocalUtil.storeOnThread(LOCALE_FILTER, locale);
//        }
//    }

//    /**
//     * Removes the locale filter from ThreadLocal
//     */
//    public static void clearLocaleFilter()
//    {
//        LOG.debug("Clear filter on locale");
//        ThreadLocalUtil.clearFromThread(LOCALE_FILTER);
//    }

//    /**
//     * Returns the same input list unless the ThreadLocal contains a restriction on the locale, in which case the list is filtered
//     * to return only the text with the given locale, or if no text with the specified locale exists, then the list will use whatever
//     * the next best default is, based on the method getDefaultLocale.
//     * @param tt
//     */
//    //TODO SDMXCORE IF NEEDED
//    /*
//    public static List<TextTypeWrapper> optionalFilterOnTextType(List<TextTypeWrapper> tt) {
//        if(hasLocaleFilter()) {
//            Locale locale = getLocaleFilter();
//            String langFilter = locale == null ? null : locale.getLanguage();
//            List<TextTypeWrapper> returnList = new ArrayList<>();
//            for(TextTypeWrapper currentTT : tt) {
//                if(langFilter != null && langFilter.equals(currentTT.getISO639Language())) {
//                    returnList.add(currentTT);
//                    return returnList;
//                }
//            }
//            TextTypeWrapper detaulTT = getDefaultLocale(tt);
//            if(detaulTT != null) {
//                returnList.add(detaulTT);
//            }
//            return returnList;
//        }
//        return tt;
//    }
//    */

//    /**
//     * Ensures there are no duplicate locales in the collection
//     * @param tt
//     */
//    public static void ensureNoDuplicates(Collection<TextTypeWrapper> tt)
//    {
//        if (tt != null)
//        {
//            Set<String> languages = new HashSet<>();
//            for (TextTypeWrapper currentTextType : tt)
//            {
//                if (languages.contains(currentTextType.getLocale()))
//                {
//                    IdentifiableBean parent = currentTextType.getParent(IdentifiableBean.class, true);
//                    String parentUrn = parent == null ? "" : parent.getUrn();
//                    throw new SdmxSemmanticException("Duplicate language '"+currentTextType.getLocale()+"' for Structure: "+ parentUrn);
//}
//languages.add(currentTextType.getLocale());
//            }
//        }
//    }

//    /**
//     * Returns the lable for the given locale, or null if there is not one
//     * @param tt
//     * @param locae
//     * @return
//     */
//    public static String getTextForLocale(List<TextTypeWrapper> tt, String locae)
//{
//    for (TextTypeWrapper currentTT : tt) {
//    if (locae != null && locae.equals(currentTT.getISO639Language()))
//    {
//        return currentTT.getValue();
//    }
//}
//return null;
//    }

//    //TODO SDMXCORE IF NEEDED
//    /*
//    public static List<TextTypeWrapperMutableBean> optionalFilterOnMutableTextType(List<TextTypeWrapperMutableBean> tt) {
//        if(hasLocaleFilter()) {
//            Locale locale = getLocaleFilter();
//            String langFilter = locale == null ? null : locale.getLanguage();
//            List<TextTypeWrapperMutableBean> returnList = new ArrayList<>();
//            for(TextTypeWrapperMutableBean currentTT : tt) {
//                if(new Locale(currentTT.getLocale()).getLanguage().equals(langFilter)) {
//                    returnList.add(currentTT);
//                    return returnList;
//                }
//            }
//            TextTypeWrapperMutableBean detaulTT = getDefaultLocaleMutable(tt);
//            if(detaulTT != null) {
//                returnList.add(detaulTT);
//            }
//            return returnList;
//        }
//        return tt;
//    }

//    public static TextTypeWrapper getDefaultLocale(List<TextTypeWrapper> textTypes) {
//        ClientRequest clientRequest = ThreadLocalUtil.getClientRequest();

//        TextTypeWrapper defaultTT = null;
//        if(ObjectUtil.validCollection(textTypes)) {
//            for(TextTypeWrapper tt : textTypes) {
//                if(clientRequest != null) {
//                    if(clientRequest.getLocale().getLanguage().equals(tt.getISO639Language())) {
//                        return tt;
//                    } else if(clientRequest.getLocale().toString().equals(tt.getISO639Language())) {
//                        return tt;
//                    }
//                }
//                if(tt.getLocale().equals("en")) {
//                    defaultTT = tt;
//                } else if(defaultTT == null) {
//                    defaultTT = tt;
//                }
//            }
//        }
//        return defaultTT;
//    }

//    private static TextTypeWrapperMutableBean getDefaultLocaleMutable(List<TextTypeWrapperMutableBean> textTypes) {
//        ClientRequest clientRequest = ThreadLocalUtil.getClientRequest();

//        TextTypeWrapperMutableBean defaultTT = null;
//        if(ObjectUtil.validCollection(textTypes)) {
//            for(TextTypeWrapperMutableBean tt : textTypes) {
//                if(clientRequest != null) {
//                    if(clientRequest.getLocale().getLanguage().equals(new Locale(tt.getLocale()).getLanguage())) {
//                        return tt;
//                    } else if(clientRequest.getLocale().toString().equals(new Locale(tt.getLocale()).getLanguage())) {
//                        return tt;
//                    }
//                }
//                if(tt.getLocale().equals("en")) {
//                    defaultTT = tt;
//                } else if(defaultTT == null) {
//                    defaultTT = tt;
//                }
//            }
//        }
//        return defaultTT;
//    }
//     */
//}