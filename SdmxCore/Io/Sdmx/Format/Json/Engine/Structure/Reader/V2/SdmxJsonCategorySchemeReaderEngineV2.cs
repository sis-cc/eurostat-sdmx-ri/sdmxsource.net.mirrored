using Io.Sdmx.Api.Singleton;
using Io.Sdmx.Utils.Core.Application;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.CategoryScheme;
using SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2.Util;
using System;
using System.Collections.Generic;

namespace SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2
{
    public class SdmxJsonCategorySchemeReaderEngineV2 : SdmxJsonItemSchemeReaderEngineV2<ICategorySchemeObject, ICategoryMutableObject, ICategorySchemeMutableObject>, IFusionSingleton
    {
        private static SdmxJsonCategorySchemeReaderEngineV2 _instance;

        public static SdmxJsonCategorySchemeReaderEngineV2 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SdmxJsonCategorySchemeReaderEngineV2();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }


        public void DestroyInstance()
        {
            _instance = null;
        }

        private SdmxJsonCategorySchemeReaderEngineV2()
            : base(SdmxStructureEnumType.CategoryScheme)
        {
        }

        protected override List<ICategoryMutableObject> ReadItems(JsonReader jReader)
        {
            List<ICategoryMutableObject> returnList = new List<ICategoryMutableObject>();
            while (jReader.MoveNext())
            {
                if (jReader.IsEndArray())
                {
                    break;
                }
                if (jReader.IsStartObject())
                {
                    var aCategoryMutable = ReadCategoryBean(jReader);
                    returnList.Add(aCategoryMutable);
                }
            }

            return returnList;
        }

        private ICategoryMutableObject ReadCategoryBean(JsonReader jReader)
        {
            var aCategory = new CategoryMutableCore();
            while (jReader.MoveNext())
            {
                if (jReader.IsEndObject())
                {
                    break;
                }

                if (SdmxJsonNameableUtil.ProcessBean(aCategory, jReader))
                {
                    continue;
                }

                string fieldName = jReader.GetCurrentFieldName();
                if (fieldName.Equals(GetItemLabel()))
                {
                    foreach (var item in ReadItems(jReader))
                    {
                        aCategory.AddItem(item);
                    }
                }
            }

            return aCategory;
        }

        protected override ICategorySchemeMutableObject GetSchemeMut()
        {
            return new CategorySchemeMutableCore();
        }

        protected override string GetItemLabel()
        {
            return "categories";
        }
    }
}
