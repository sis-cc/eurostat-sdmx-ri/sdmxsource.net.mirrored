using System;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Io.Sdmx.Api.Singleton;
using Io.Sdmx.Utils.Core.Application;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.CategoryScheme;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2.Util;

namespace SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2
{
    public class SdmxJsonCategorisationReaderEngineV2 : SdmxJsonMaintainableBeanReaderEngineV2<IMaintainableObject>, IFusionSingleton
    {
        private static SdmxJsonCategorisationReaderEngineV2 _instance;

        public static SdmxJsonCategorisationReaderEngineV2 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SdmxJsonCategorisationReaderEngineV2();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }


        public void DestroyInstance()
        {
            _instance = null;
        }

        private SdmxJsonCategorisationReaderEngineV2()
            : base(SdmxStructureEnumType.Categorisation)
        {
        }

        protected override IMaintainableObject Build(JsonReader jReader, ISdmxObjectRetrievalManager beanRetrievalManager)
        {
            ICategorisationMutableObject aCategorisation = new CategorisationMutableCore();

            while (jReader.MoveNext())
            {
                if (jReader.IsEndObject())
                {
                    break;
                }
                if (SdmxJsonMaintainableUtil.ProcessBean(aCategorisation, jReader))
                {
                    continue;
                }

                string fieldName = jReader.GetCurrentFieldName();
                string value = jReader.GetValueAsString();

                if (fieldName.Equals("target"))
                {
                    var structureReference = new StructureReferenceImpl(value);
                    aCategorisation.StructureReference = structureReference;
                }
                if (fieldName.Equals("source"))
                {
                    var categoryRef = new StructureReferenceImpl(value);
                    aCategorisation.CategoryReference = categoryRef;
                }
            }

            return aCategorisation.ImmutableInstance;
        }
    }
}
