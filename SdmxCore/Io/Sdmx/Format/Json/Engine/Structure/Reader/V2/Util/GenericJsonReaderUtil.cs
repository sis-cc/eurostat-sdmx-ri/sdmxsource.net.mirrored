using System;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Io.Sdmx.Utils.Sdmx.Xs;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using static Io.Sdmx.Format.Json.Util.SdmxJsonStaticHelper;
using SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2;
//using static Io.Sdmx.Format.Json.Util.SdmxJsonStaticHelper;

namespace SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2.Util
{
    public class GenericJsonReaderUtil
    {
        public static bool ProcessLinks(IIdentifiableMutableObject identifiableBean, JsonReader jReader)
        {
            string fieldName = jReader.GetCurrentFieldName();
            if (jReader.IsStartArray() && fieldName.Equals("links"))
            {
                while (jReader.MoveNext())
                {
                    if (jReader.IsStartObject())
                    {
                        ProcessLink(identifiableBean, jReader);
                    }
                    if (jReader.IsEndArray())
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private static bool ProcessLink(IIdentifiableMutableObject identifiableBean, JsonReader jReader)
        {
            string rel = null;
            Uri href = null;
            Uri uri = null;
            IURN urn = null;
            string type = null;
            while (jReader.MoveNext())
            {
                if (jReader.IsEndObject())
                {
                    //identifiableBean.AddLink(new LinkObject(rel, href, uri, urn));
                    return true;
                }
                string fieldName = jReader.GetCurrentFieldName();
                switch (fieldName)
                {
                    case "rel":
                        rel = jReader.GetValueAsString();
                        break;
                    case "href":
                        href = ToURI(jReader.GetValueAsString());
                        break;
                    case "type":
                        type = jReader.GetValueAsString();
                        break;
                    case "urn":
                        //  urn = URN.GetInstance(jReader.GetValueAsString());
                        break;
                }
            }
            return false;
        }

        private static Uri ToURI(string value)
        {
            if (value == null)
            {
                return null;
            }
            try
            {
                return new Uri(value);
            }
            catch (UriFormatException e)
            {
                throw new SdmxSemmanticException("Link contains illegal URI : " + value, e);
            }
        }
    }
}