/*******************************************************************************
 *  This software and the accompanying materials of the FMR tools are licensed under the
 *  Apache Licence, version 2.0 (the “Licence”) which accompanies the distribution and is
 *  available at: http://www.apache.org/licenses/LICENSE-2.0
 *  You may not use this file except in compliance with the Licence.
 *  This file is part of the FMR tools and source code libraries.
 *  The software is distributed on an “AS IS” BASIS, WITHOUT WARRANTIES OR
 *  CONDITIONS OF ANY KIND, either express or implied. See the Apache 2.0 Licence for
 *  more details regarding permissions and limitations under the Licence.
 * Copyright © 2022, Bank for International Settlements
 ******************************************************************************/
using log4net;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Util;
using Org.Sdmxsource.Sdmx.Util.Date;
using System.Collections.Generic;
using System.IO;
using System;
using Xml.Schema.Linq;
using System.Linq;
using System.Text;

namespace SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2
{
    public class JsonReader : IDisposable
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(JsonReader));
        //not used?
        //public static readonly SkipObjectIterator SkipObjectIterator = new JsonReader().new SkipObjectIterator();
        private IReadableDataLocation rdl;
        private JsonTextReader jReader;
        private JsonToken token;

        private Stack<JsonStackItem> stack;
        private Stack<Iterator> iteratorStack;
        private bool iterating;
        private JsonStackItem currentStackItem;
        private string fieldName;
        private TextReader streamReader;

        public interface Iterator
        {
            void Next(string fieldName);

            void End(string fieldName, bool isObject);

            Iterator Start(string fieldName, bool isObject);
        }

        private JsonReader() { }

        public JsonReader(IReadableDataLocation rdl)
        {
            this.rdl = rdl;
            Reset();
        }
        public IReadableDataLocation Rdl => rdl;

        public static bool IsValidJson(IReadableDataLocation rdl)
        {
            try
            {
                using (var reader = new JsonReader(rdl))
                {
                    // Attempt to moveNext 10 times. If this succeeds we are very likely in a valid JSON file.
                    for (var i = 0; i < 10; i++)
                    {
                        var retVal = reader.MoveNext();
                        if (!retVal)
                        {
                            return i > 0; // If we have moved only once, and it returned false, then the file is empty, return false
                        }
                    }
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public void Dispose()
        {
            try
            {
                jReader.Close();
                streamReader.Close();
            }
            catch (IOException e)
            {
                _logger.Warn("Error attempting to close JsonTextReader", e);
            }
        }

        public void Reset()
        {
            _logger.Debug("Reset");
            token = default;
            stack = new Stack<JsonStackItem>();
            currentStackItem = null;
            iteratorStack = new Stack<Iterator>();
            //var jsonString = Encoding.UTF8.GetString(rdl.InputStream.get);
            streamReader = new StreamReader(rdl.InputStream);
            jReader = new JsonTextReader(streamReader);
        }

        public void Iterate(Iterator it)
        {
            _logger.Debug("Iterate");
            iterating = true;
            iteratorStack = new Stack<Iterator>();
            while (iterating && it != null && MoveNext())
            {
                if (IsEndObject() || IsEndArray())
                {
                    var fieldName = currentStackItem?.FieldName;
                    it.End(fieldName, IsEndObject());
                    if (iteratorStack.Count == 0)
                    {
                        break;
                    }
                    it = iteratorStack.Pop();
                }
                else if (IsStartObject() || IsStartArray())
                {
                    var fieldName = currentStackItem?.FieldName;
                    var subObjectIt = it.Start(fieldName, IsStartObject());
                    iteratorStack.Push(it); // Store the old iterator
                    if (subObjectIt != null)
                    {
                        it = subObjectIt;
                    }
                }
                else
                {
                    it.Next(fieldName);
                }
            }
        }

        public void StopIterating()
        {
            iterating = false;
        }

        public bool MoveNextStartObject()
        {
            while (MoveNext())
            {
                if (IsStartObject())
                {
                    return true;
                }
            }
            return false;
        }

        public bool MoveNextStartArray()
        {
            while (MoveNext())
            {
                if (IsStartArray())
                {
                    return true;
                }
            }
            return false;
        }

        public bool MoveToEndArray()
        {
            var i = 1;
            while (MoveNext())
            {
                if (IsEndArray())
                {
                    i--;
                    if (i == 0)
                    {
                        return true;
                    }
                }
                if (IsStartArray())
                {
                    i++;
                }
            }
            return false;
        }

        public bool MoveToEndObject()
        {
            var i = 0;
            while (MoveNext())
            {
                if (IsEndObject())
                {
                    i--;
                    if (i < 1)
                    {
                        return true;
                    }
                }
                if (IsStartObject())
                {
                    i++;
                }
            }
            return false;
        }

        public bool IsField() => token == JsonToken.PropertyName;

        public bool IsStartObject() => token == JsonToken.StartObject;

        public bool IsEndObject() => token == JsonToken.EndObject;

        public bool IsStartArray() => token == JsonToken.StartArray;

        public bool IsEndArray() => token == JsonToken.EndArray;

        public string GetCurrentFieldName() => fieldName;

        public int GetValueAsInt() => token == JsonToken.Integer ? Convert.ToInt32(jReader.Value) : 0;

        public string GetValueAsString() => jReader.Value?.ToString();

        public DateTime? GetValueAsDate()
        {
            var dateStr = GetValueAsString();
            return DateUtil.FormatDate(dateStr, true);
        }

        public bool? GetValueAsBoolean() => jReader.Value as bool?;

        public JsonToken GetToken() => token;

        public Dictionary<string, string> ReadStringMap()
        {
            var returnMap = new Dictionary<string, string>();
            if (!IsStartObject())
            {
                throw new SdmxSemmanticException($"JSON '{GetCurrentFieldName()}' expected to be of type Object");
            }
            while (MoveNext())
            {
                if (IsEndObject())
                {
                    break;
                }
                returnMap[GetCurrentFieldName()] = GetValueAsString();
            }
            return returnMap;
        }

        public List<string> ReadStringArray()
        {
            var list = new List<string>();
            while (MoveNext() && token != JsonToken.EndArray)
            {
                list.Add(jReader.Value as string);
            }
            return list;
        }

        public HashSet<string> ReadStringSet()
        {
            var set = new HashSet<string>();
            while (MoveNext() && token != JsonToken.EndArray)
            {
                set.Add(jReader.Value as string);
            }
            return set;
        }

        public List<int?> ReadIntegerArray(bool valuesMustBeInt = false)
        {
            var list = new List<int?>();
            while (MoveNext() && token != JsonToken.EndArray)
            {
                var asStr = jReader.Value as string;
                if (asStr == null || asStr.Equals("null"))
                {
                    list.Add(null);
                }
                else
                {
                    if (valuesMustBeInt && jReader.TokenType == JsonToken.String)
                    {
                        throw new FormatException($"The input was expected to be a number but was a string: {asStr}");
                    }
                    list.Add(int.Parse(asStr));
                }
            }
            return list;
        }

        public bool IsParentField(string name)
        {
            if (currentStackItem != null && currentStackItem.ParentItem != null)
            {
                if (name == null)
                {
                    if (currentStackItem.ParentItem.FieldName == null)
                    {
                        return true;
                    }
                }
                else if (name.Equals(currentStackItem.ParentItem.FieldName))
                {
                    return true;
                }
            }
            return false;
        }

        public JsonStackItem GetCurrentStackItem() => currentStackItem;

        //public bool MoveNext()
        //{
        //    JsonStackItem newStackItem = null;
        //    token = jReader.TokenType;
        //    if (token != JsonToken.None)
        //    {
        //        switch (token)
        //        {
        //            case JsonToken.StartArray:
        //                newStackItem = new JsonStackItem(currentStackItem, null, STACK_TYPE.ARRAY);
        //                stack.Push(newStackItem);
        //                break;
        //            case JsonToken.StartObject:
        //                newStackItem = new JsonStackItem(currentStackItem, null, STACK_TYPE.OBJECT);
        //                stack.Push(newStackItem);
        //                break;
        //            case JsonToken.PropertyName:
        //                fieldName = jReader.Value as string;
        //                jReader.Read();
        //                switch (jReader.TokenType)
        //                {
        //                    case JsonToken.StartArray:
        //                        newStackItem = new JsonStackItem(currentStackItem, fieldName, STACK_TYPE.ARRAY);
        //                        stack.Push(newStackItem);
        //                        break;
        //                    case JsonToken.StartObject:
        //                        newStackItem = new JsonStackItem(currentStackItem, fieldName, STACK_TYPE.OBJECT);
        //                        stack.Push(newStackItem);
        //                        break;
        //                }
        //                break;
        //            case JsonToken.EndArray:
        //            case JsonToken.EndObject:
        //                stack.Pop();
        //                if (stack.Count > 0)
        //                {
        //                    currentStackItem = stack.Last();
        //                }
        //                else
        //                {
        //                    currentStackItem = null;
        //                }
        //                break;
        //        }
        //    }
        //    if (newStackItem != null)
        //    {
        //        currentStackItem = stack.Last();
        //    }
        //    return token != JsonToken.None;
        //}

        public bool MoveNext()
        {
            JsonStackItem newStackItem = null;
            try
            {
                jReader.Read();
                token = jReader.TokenType;
                if (token != JsonToken.None)
                {
                    switch (token)
                    {
                        case JsonToken.StartArray:
                            // Root Item
                            // Console.WriteLine("Start Array: " + jParser.Path);
                            newStackItem = new JsonStackItem(currentStackItem, null, STACK_TYPE.ARRAY);
                            stack.Push(newStackItem);
                            break;
                        case JsonToken.StartObject:
                            // Root Item
                            // Console.WriteLine("Start Object: " + jParser.Path);
                            newStackItem = new JsonStackItem(currentStackItem, null, STACK_TYPE.OBJECT);
                            stack.Push(newStackItem);
                            break;
                        case JsonToken.PropertyName:
                            fieldName = (string)jReader.Value;
                            jReader.Read();
                            token = jReader.TokenType;
                            switch (jReader.TokenType)
                            {
                                case JsonToken.StartArray:
                                    // Console.WriteLine("Start Array: " + jParser.Path);
                                    _logger.Debug("Start Array: " + jReader.Path);
                                    newStackItem = new JsonStackItem(currentStackItem, fieldName, STACK_TYPE.ARRAY);
                                    stack.Push(newStackItem);
                                    break;
                                case JsonToken.StartObject:
                                    // Console.WriteLine("Start Object: " + jParser.Path);
                                    _logger.Debug("Start Object: " + jReader.Path);
                                    newStackItem = new JsonStackItem(currentStackItem, fieldName, STACK_TYPE.OBJECT);
                                    stack.Push(newStackItem);
                                    break;
                            }
                            break;
                        case JsonToken.EndArray:
                        case JsonToken.EndObject:
                            stack.Pop();
                            if (stack.Count > 0)
                            {
                                currentStackItem = stack.Peek();
                            }
                            else
                            {
                                currentStackItem = null;
                            }
                            if (currentStackItem != null)
                            {
                                // Console.WriteLine("End Object|Array: " + currentStackItem.FieldName);
                                _logger.Debug("End Object|Array: " + currentStackItem.FieldName);
                            }
                            break;
                    }
                }
                if (newStackItem != null)
                {
                    currentStackItem = stack.Peek();
                }
                return token != JsonToken.None;
            }
            catch (JsonReaderException e)
            {
                throw new SdmxException("Error Reading JSON", e);
            }
            catch (IOException e)
            {
                throw new SdmxException("Error Reading JSON", e);
            }
        }

        public enum STACK_TYPE
        {
            OBJECT,
            ARRAY
        }

        public class JsonStackItem
        {
            public JsonStackItem(JsonStackItem parentItem, string fieldName, STACK_TYPE type)
            {
                ParentItem = parentItem;
                FieldName = fieldName;
                Type = type;
            }

            public JsonStackItem ParentItem { get; }

            public string FieldName { get; }

            public STACK_TYPE Type { get; }

            public bool IsRoot => ParentItem == null;
        }

        public class SkipObjectIterator : Iterator
        {
            public void Next(string fieldName)
            {
                // Do nothing
            }

            public void End(string fieldName, bool isObject)
            {
                // Do nothing
            }

            public Iterator Start(string fieldName, bool isObject)
            {
                return null;
            }
        }

        public enum JsonTokenId
        {
            ID_STRING,
            ID_NUMBER
        }
    }
}