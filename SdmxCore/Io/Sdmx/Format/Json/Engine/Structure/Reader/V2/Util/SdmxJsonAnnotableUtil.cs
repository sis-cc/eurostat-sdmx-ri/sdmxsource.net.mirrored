using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
using Org.Sdmxsource.Util;
using SdmxCore.Io.Format.Json.Engine.Structure.Reader.V2.Util.Parsers;
using SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2;
using System.Collections.Generic;

namespace SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2.Util
{
    public class SdmxJsonAnnotableUtil
    {
        public static bool ProcessBean(IAnnotableMutableObject bean, JsonReader jReader)
        {
            return DoIt(bean, jReader) || SdmxJsonLinkableUtil.ProcessBean(bean, jReader);
        }

        public static bool DoIt(IAnnotableMutableObject bean, JsonReader jReader)
        {
            string fieldName = jReader.GetCurrentFieldName();
            if (fieldName.Equals("annotations"))
            {
                var readAnnotations = ReadAnnotations(jReader);
                foreach (var anAnnotation in readAnnotations)
                {
                    /* TODO SDMXCORE VALUELIST
                    if (anAnnotation.Type != null &&  anAnnotation.Type.Equals(ValidityPeriodBean.VALIDITY_TYPE)){
                        continue;
                    }
                    */
                    bean.AddAnnotation(anAnnotation);
                }
                return true;
            }
            return false;
        }

        private static List<IAnnotationMutableObject> ReadAnnotations(JsonReader jReader)
        {
            List<IAnnotationMutableObject> returnList = new List<IAnnotationMutableObject>();

            while (jReader.MoveNext())
            {
                if (jReader.IsStartObject())
                {
                    // Build an AnnotableMutableBean
                    var annotationBean = BuildAnnotation(jReader);
                    returnList.Add(annotationBean);
                }
                if (jReader.IsEndArray())
                {
                    break;
                }
            }

            return returnList;
        }

        private static IAnnotationMutableObject BuildAnnotation(JsonReader jReader)
        {
            var anAnnotation = new AnnotationMutableCore();
            string annotationText = null;
            while (jReader.MoveNext())
            {
                if (jReader.IsEndObject())
                {
                    break;
                }
                string fieldName = jReader.GetCurrentFieldName();
                string value = jReader.GetValueAsString();

                if (fieldName.Equals("title"))
                {
                    anAnnotation.Title = value;
                }
                else if (fieldName.Equals("id"))
                {
                    anAnnotation.Id = value;
                }
                else if (fieldName.Equals("type"))
                {
                    anAnnotation.Type = value;
                }
                else if (fieldName.Equals("links"))
                {
                    SdmxJsonLinkableUtil.ProcessBean(anAnnotation, jReader);
                    // anAnnotation.Uri = value;
                }
                else if (fieldName.Equals("texts"))
                {
                    var readTextTypeWrappers = SdmxJsonNameableUtil.ReadTextTypeWrappers(jReader);
                    foreach (var textTypeWrapperMutableBean in readTextTypeWrappers)
                    {
                        anAnnotation.AddText(textTypeWrapperMutableBean);
                    }
                }
                else if (fieldName.Equals("text"))
                {
                    annotationText = value;
                }
            }
            // If there was not a texts field, and there was a text field - then use the text field, otherwise do not
            if (annotationText != null && !ObjectUtil.ValidCollection(anAnnotation.Text))
            {
                anAnnotation.AddText("en", annotationText);
            }
            return anAnnotation;
        }
    }
}