using System.Collections.Generic;
using Io.Sdmx.Utils.Core.Application;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.ConceptScheme;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2.Util;

namespace SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2
{
    public class SdmxJsonConceptSchemeReaderEngineV2 : SdmxJsonItemSchemeReaderEngineV2<IConceptSchemeObject, IConceptMutableObject, IConceptSchemeMutableObject>
    {
        private static SdmxJsonConceptSchemeReaderEngineV2 _instance;

        public static SdmxJsonConceptSchemeReaderEngineV2 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SdmxJsonConceptSchemeReaderEngineV2();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }

        public void DestroyInstance()
        {
            _instance = null;
        }

        private SdmxJsonConceptSchemeReaderEngineV2()
            : base(SdmxStructureEnumType.ConceptScheme)
        {
        }

        protected override List<IConceptMutableObject> ReadItems(JsonReader jReader)
        {
            List<IConceptMutableObject> returnList = new List<IConceptMutableObject>();
            while (jReader.MoveNext())
            {
                if (jReader.IsEndArray())
                {
                    break;
                }
                if (jReader.IsStartObject())
                {
                    IConceptMutableObject aConcept = ReadConceptBean(jReader);
                    returnList.Add(aConcept);
                }
            }

            return returnList;
        }

        protected override IConceptSchemeMutableObject GetSchemeMut()
        {
            return new ConceptSchemeMutableCore();
        }

        protected override string GetItemLabel()
        {
            return "concepts";
        }

        private IConceptMutableObject ReadConceptBean(JsonReader jReader)
        {
            var aConcept = new ConceptMutableCore();
            while (jReader.MoveNext())
            {
                if (jReader.IsEndObject())
                {
                    break;
                }

                if (SdmxJsonNameableUtil.ProcessBean(aConcept, jReader))
                {
                    continue;
                }

                string fieldName = jReader.GetCurrentFieldName();
                string value = jReader.GetValueAsString();
                if (fieldName.Equals("parent"))
                {
                    aConcept.ParentConcept = value;
                }
                else if (fieldName.Equals("coreRepresentation"))
                {
                    aConcept.CoreRepresentation = SdmxJsonRepresentationUtil.ReadRepresentation(jReader);
                }
                else if (fieldName.Equals("isoConceptReference"))
                {
                    aConcept.IsoConceptReference = new StructureReferenceImpl(value);
                }
                //TODO SDMXCORE
                //StaxValidityPeriodUtil.ProcessValidityAnnotations(aConcept);
            }
            return aConcept;
        }
    }
}
