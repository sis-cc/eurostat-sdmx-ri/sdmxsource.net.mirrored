using System.Collections.Generic;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2
{
    public abstract class SdmxJsonMaintainableBeanReaderEngineV2<T> : SdmxJsonMaintainableBeanReaderEngine<T> where T : IMaintainableObject
    {
        private SdmxStructureEnumType supportedType;

        public SdmxJsonMaintainableBeanReaderEngineV2(SdmxStructureEnumType supportedType)
        {
            this.supportedType = supportedType;
        }

        public SdmxStructureEnumType SupportedType => supportedType;

        public List<T> BuildMaintainable(JsonReader jReader, ISdmxObjectRetrievalManager beanRetrievalManager)
        {
            List<T> returnList = new List<T>();

            while (jReader.MoveNext())
            {
                if (jReader.IsStartObject())
                {
                    returnList.Add(Build(jReader, beanRetrievalManager));
                }

                if (jReader.IsEndArray())
                {
                    break;
                }
            }
            return returnList;
        }

        public HashSet<SdmxStructureEnumType> GetReliesOn()
        {
            return new HashSet<SdmxStructureEnumType>();
        }

        protected abstract T Build(JsonReader jReader, ISdmxObjectRetrievalManager beanRetrievalManager);
    }
}