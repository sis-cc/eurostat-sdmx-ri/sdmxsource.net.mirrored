using System;
using System.Collections.Generic;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2.Util;

namespace SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2
{
    public abstract class SdmxJsonItemSchemeReaderEngineV2<T, Y, F> : SdmxJsonMaintainableBeanReaderEngineV2<IMaintainableObject> where T : IMaintainableObject where Y : IItemMutableObject where F : IItemSchemeMutableObject<Y>
    {
        protected SdmxJsonItemSchemeReaderEngineV2(SdmxStructureEnumType supportedType) : base(supportedType)
        {
        }

        protected override IMaintainableObject Build(JsonReader jReader, ISdmxObjectRetrievalManager beanRetrievalManager)
        {
            F itmSch = GetSchemeMut();
            while (jReader.MoveNext())
            {
                if (jReader.IsEndObject())
                {
                    break;
                }

                if (SdmxJsonMaintainableUtil.ProcessBean(itmSch, jReader))
                {
                    continue;
                }
                string fieldName = jReader.GetCurrentFieldName();
                string value = jReader.GetValueAsString();
                ProcessItemSchemeField(itmSch, jReader, fieldName, value);

            }
            return (T)itmSch.ImmutableInstance;
        }

        protected virtual bool ProcessItemSchemeField(F itmSch, JsonReader jReader, string fieldName, string value)
        {
            if (fieldName.Equals("isPartial"))
            {
                if (value.Equals("true", StringComparison.OrdinalIgnoreCase))
                {
                    itmSch.IsPartial = true;
                }
                else if (value.Equals("false", StringComparison.OrdinalIgnoreCase))
                {
                    itmSch.IsPartial = false;
                }
                else
                {
                    throw new SdmxSemmanticException("Illegal value for 'isPartial'. Valid values are 'true' or 'false' but the following value was supplied: " + value);
                }
                return true;
            }
            if (fieldName.Equals(GetItemLabel()))
            {
                List<Y> items = ReadItems(jReader);
                foreach (Y itm in items)
                {
                    itmSch.AddItem(itm);
                }
                return true;
            }
            return false;
        }

        protected abstract List<Y> ReadItems(JsonReader jReader);

        protected abstract F GetSchemeMut();

        protected abstract string GetItemLabel();
    }
}
