using System;
using System.Collections.Generic;
using System.Linq;
using Io.Sdmx.Api.Singleton;
using Io.Sdmx.Utils.Core.Application;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2.Util;

namespace SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2
{
    public class SdmxJsonDataStructureReaderEngineV2 : SdmxJsonMaintainableBeanReaderEngineV2<IMaintainableObject>, IFusionSingleton
    {
        private static SdmxJsonDataStructureReaderEngineV2 _instance;

        public static SdmxJsonDataStructureReaderEngineV2 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SdmxJsonDataStructureReaderEngineV2();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }


        public void DestroyInstance()
        {
            _instance = null;
        }

        private SdmxJsonDataStructureReaderEngineV2() : base(SdmxStructureEnumType.Dsd)
        {
        }

        protected override IMaintainableObject Build(JsonReader jReader, ISdmxObjectRetrievalManager beanRetrievalManager)
        {
            var dsdBean = new DataStructureMutableCore();
            while (jReader.MoveNext())
            {
                if (jReader.IsEndObject())
                {
                    break;
                }
                string fieldName = jReader.GetCurrentFieldName();
                if (SdmxJsonMaintainableUtil.ProcessBean(dsdBean, jReader))
                {
                    continue;
                }
                switch (fieldName)
                {
                    case "metadata":
                        dsdBean.MetadataStructure = StructureReferenceImpl.BuildAndVerify(jReader.GetValueAsString(), SdmxStructureEnumType.Msd);
                        break;
                    case "dataStructureComponents":
                        BuildDataStructure(jReader, dsdBean);
                        break;
                }
            }
            return dsdBean.ImmutableInstance;
        }

        private void BuildDataStructure(JsonReader jReader, IDataStructureMutableObject dsdBean)
        {
            while (jReader.MoveNext())
            {
                if (jReader.IsEndObject())
                {
                    break;
                }
                string fieldName = jReader.GetCurrentFieldName();
                switch (fieldName)
                {
                    case "attributeList":
                        var attras = GetAttributes(jReader);
                        dsdBean.AttributeList = attras;
                        break;
                    case "dimensionList":
                        var dims = ReadDimensions(jReader);
                        dsdBean.DimensionList = dims;
                        break;
                    case "groups":
                        var groups = ReadGroups(jReader);
                        foreach (var item in groups)
                        {
                            dsdBean.AddGroup(item);
                        }
                        break;
                    case "measureList":
                        var measureList = ReadMeasureList(jReader);
                        dsdBean.MeasureList = measureList;
                        break;
                }
            }
        }

        private IMeasureListMutableObject ReadMeasureList(JsonReader jReader)
        {
            var ret = new MeasureListMutableCore();
            while (jReader.MoveNext())
            {
                if (jReader.IsEndObject())
                {
                    break;
                }
                if (SdmxJsonIdentifiableUtil.ProcessBean(ret, jReader))
                {
                    continue;
                }
                if (jReader.GetCurrentFieldName() == "measures")
                {
                    ret.Measures = ReadMeasures(jReader);
                }
            }
            return ret;
        }

        private List<IMeasureMutableObject> ReadMeasures(JsonReader jReader)
        {
            var retList = new List<IMeasureMutableObject>();
            while (jReader.MoveNext())
            {
                if (jReader.IsEndArray())
                {
                    break;
                }
                if (jReader.IsStartObject())
                {
                    IMeasureMutableObject measure = new MeasureMutableCore();
                    while (jReader.MoveNext())
                    {
                        if (jReader.IsEndObject())
                        {
                            break;
                        }
                        if (SdmxJsonComponentUtil.ProcessBean(measure, jReader))
                        {
                            continue;
                        }
                    }
                    retList.Add(measure);
                }
            }
            return retList;
        }

        private List<IGroupMutableObject> ReadGroups(JsonReader jReader)
        {
            List<IGroupMutableObject> retList = new List<IGroupMutableObject>();
            while (jReader.MoveNext())
            {
                if (jReader.IsEndArray())
                {
                    break;
                }
                var group = new GroupMutableCore();
                while (jReader.MoveNext())
                {
                    if (jReader.IsEndObject())
                    {
                        break;
                    }
                    string fieldName = jReader.GetCurrentFieldName();
                    if (SdmxJsonIdentifiableUtil.ProcessBean(group, jReader))
                    {
                        continue;
                    }
                    switch (fieldName)
                    {
                        case "attachmentConstraint":
                            string value = jReader.GetValueAsString();
                            group.AttachmentConstraintRef = new StructureReferenceImpl(value);
                            break;
                        case "groupDimensions":
                            List<string> groupDimIds = jReader.ReadStringArray();
                            foreach (var item in groupDimIds)
                            {
                                group.AddDimensionRef(item);
                            }
                            break;
                    }
                }
                retList.Add(group);
            }
            return retList;
        }

        private IDimensionListMutableObject ReadDimensions(JsonReader jReader)
        {
            IDimensionListMutableObject dimList = new DimensionListMutableCore();
            while (jReader.MoveNext())
            {
                if (SdmxJsonIdentifiableUtil.ProcessBean(dimList, jReader))
                {
                    continue;
                }
                if (jReader.IsEndObject())
                {
                    break;
                }
                string fieldName = jReader.GetCurrentFieldName();
                switch (fieldName)
                {
                    case "dimensions":
                        List<IDimensionMutableObject> dims = ReadDims(jReader, SdmxStructureEnumType.Dimension);
                        AddDimensions(dims, dimList);
                        break;
                    case "timeDimensions":
                        List<IDimensionMutableObject> timeDims = ReadDims(jReader, SdmxStructureEnumType.TimeDimension);
                        AddDimensions(timeDims, dimList);
                        break;
                }
            }
            return dimList;
        }

        private void AddDimensions(List<IDimensionMutableObject> dims, IDimensionListMutableObject dimList)
        {
            dims.ForEach(dimList.AddDimension);
        }

        private List<IDimensionMutableObject> ReadDims(JsonReader jReader, SdmxStructureEnumType dimensionType)
        {
            List<IDimensionMutableObject> rtestList = new List<IDimensionMutableObject>();
            while (jReader.MoveNext())
            {
                if (jReader.IsEndArray())
                {
                    break;
                }
                var dim = new DimensionMutableCore();
                if (dimensionType == SdmxStructureEnumType.TimeDimension)
                {
                    dim.TimeDimension = true;
                }
                else if (dimensionType == SdmxStructureEnumType.MeasureDimension)
                {
                    dim.MeasureDimension = true;
                }
                while (jReader.MoveNext())
                {
                    if (jReader.IsEndObject())
                    {
                        break;
                    }
                    string fieldName = jReader.GetCurrentFieldName();
                    if (SdmxJsonComponentUtil.ProcessBean(dim, jReader))
                    {
                        continue;
                    }
                    switch (fieldName)
                    {
                        case "conceptRoles":
                            var concepts = ReadConceptRoles(jReader);
                            foreach (var item in concepts)
                            {
                                dim.AddConceptRole(item);
                            }

                            break;
                    }
                }
                rtestList.Add(dim);
            }
            return rtestList;
        }

        private IAttributeListMutableObject GetAttributes(JsonReader jReader)
        {
            var attrList = new AttributeListMutableCore();
            while (jReader.MoveNext())
            {
                if (jReader.IsEndObject())
                {
                    break;
                }
                string currentFieldName = jReader.GetCurrentFieldName();
                if (SdmxJsonIdentifiableUtil.ProcessBean(attrList, jReader))
                {
                    continue;
                }
                if (currentFieldName == "attributes")
                {
                    while (jReader.MoveNext())
                    {
                        if (jReader.IsEndArray())
                        {
                            break;
                        }
                        var atBean = new AttributeMutableCore();
                        while (jReader.MoveNext())
                        {
                            if (jReader.IsEndObject())
                            {
                                break;
                            }
                            string fieldName = jReader.GetCurrentFieldName();
                            if (SdmxJsonComponentUtil.ProcessBean(atBean, jReader))
                            {
                                continue;
                            }
                            switch (fieldName)
                            {
                                case "usage":
                                    string value = jReader.GetValueAsString().ToLower();
                                    if (!value.Equals("optional") && !value.Equals("mandatory"))
                                    {
                                        throw new SdmxSemmanticException("'usage' has illegal value of: ");
                                    }
                                    atBean.SetMandatory(!value.Equals("optional"));
                                    break;
                                case "measureRelationship":
                                    foreach (var item in jReader.ReadStringArray())
                                    {
                                        atBean.AddMeasureRelationship(item);
                                    }

                                    break;
                                case "attributeRelationship":
                                    ReadAttributeRelationship(jReader, atBean);
                                    break;
                                case "conceptRoles":
                                    var concepts = ReadConceptRoles(jReader);
                                    foreach (var concept in concepts)
                                    {
                                        atBean.AddConceptRole(concept);
                                    }

                                    break;
                            }
                        }
                        attrList.AddAttribute(atBean);
                    }
                }
            }
            return attrList;
        }

        private List<IStructureReference> ReadConceptRoles(JsonReader jReader)
        {
            return jReader.ReadStringArray().Select(item => new StructureReferenceImpl(item)).ToList<IStructureReference>();
        }

        private void ReadAttributeRelationship(JsonReader jReader, IAttributeMutableObject atBean)
        {
            while (jReader.MoveNext())
            {
                if (jReader.IsEndObject())
                {
                    break;
                }
                string fieldName = jReader.GetCurrentFieldName();
                string value = jReader.GetValueAsString();
                switch (fieldName)
                {
                    case "dimensions":
                        atBean.AttachmentLevel = AttributeAttachmentLevel.DimensionGroup;
                        List<string> dims = jReader.ReadStringArray();
                        atBean.DimensionReferences = dims;
                        break;
                    case "group":
                        atBean.AttachmentLevel = AttributeAttachmentLevel.Group;
                        atBean.AttachmentGroup = value;
                        break;
                    case "dataflow":
                        atBean.AttachmentLevel = AttributeAttachmentLevel.DataSet;
                        jReader.MoveToEndObject(); // empty node
                        break;
                    case "observation":
                        atBean.AttachmentLevel = AttributeAttachmentLevel.Observation;
                        jReader.MoveToEndObject();  // empty node
                        break;
                }
            }
        }
    }
}