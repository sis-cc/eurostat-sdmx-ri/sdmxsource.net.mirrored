/*******************************************************************************
 *  This software and the accompanying materials of the FMR tools are licensed under the
 *  Apache Licence, version 2.0 (the “Licence”) which accompanies the distribution and is
 *  available at: http://www.apache.org/licenses/LICENSE-2.0
 *  You may not use this file except in compliance with the Licence.
 *  This file is part of the FMR tools and source code libraries.
 *  The software is distributed on an “AS IS” BASIS, WITHOUT WARRANTIES OR
 *  CONDITIONS OF ANY KIND, either express or implied. See the Apache 2.0 Licence for
 *  more details regarding permissions and limitations under the Licence.
 * Copyright © 2022, Bank for International Settlements
 ******************************************************************************/
using System.Collections.Generic;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
using Org.Sdmxsource.Util;
using SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2;

namespace SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2.Util
{
    public class SdmxJsonNameableUtil
    {
        public static bool ProcessBean(INameableMutableObject nameable, JsonReader jReader)
        {
            return SdmxJsonIdentifiableUtil.ProcessBean(nameable, jReader) || ProcessNode(nameable, jReader);
        }

        private static bool ProcessNode(INameableMutableObject nameableMutableBean, JsonReader jReader)
        {
            string fieldName = jReader.GetCurrentFieldName();

            if (fieldName.Equals("names"))
            {
                List<ITextTypeWrapperMutableObject> readTextTypeWrappers = ReadTextTypeWrappers(jReader);
                foreach (var ttw in readTextTypeWrappers)
                {
                    nameableMutableBean.AddName(ttw.Locale, ttw.Value);
                }
                return true;
            }
            if (fieldName.Equals("descriptions"))
            {
                var readTextTypeWrappers = ReadTextTypeWrappers(jReader);
                foreach (var ttw in readTextTypeWrappers)
                {
                    nameableMutableBean.AddDescription(ttw.Locale, ttw.Value);
                }
                return true;
            }

            if (fieldName.Equals("name"))
            {
                if (!ObjectUtil.ValidCollection(nameableMutableBean.Names))
                {
                    nameableMutableBean.AddName("en", jReader.GetValueAsString());
                }
                return true;
            }
            else if (fieldName.Equals("description"))
            {
                if (!ObjectUtil.ValidCollection(nameableMutableBean.Descriptions))
                {
                    nameableMutableBean.AddDescription("en", jReader.GetValueAsString());
                }
                return true;
            }
            return false;
        }

        public static List<ITextTypeWrapperMutableObject> ReadTextTypeWrappers(JsonReader jReader)
        {
            // JReader is at the start of an object
            List<ITextTypeWrapperMutableObject> returnList = new List<ITextTypeWrapperMutableObject>();
            while (jReader.MoveNext())
            {
                if (jReader.IsEndObject())
                {
                    break;
                }

                ITextTypeWrapperMutableObject ttw = AddTextTypeWrapper(jReader);
                if (ttw != null)
                {
                    returnList.Add(ttw);
                }
            }
            return returnList;
        }

        private static ITextTypeWrapperMutableObject AddTextTypeWrapper(JsonReader jReader)
        {
            var ttw = new TextTypeWrapperMutableCore();
            string locale = jReader.GetCurrentFieldName();
            string value = jReader.GetValueAsString();
            ttw.Locale = locale;
            ttw.Value = value;
            if (ObjectUtil.ValidString(ttw.Value) && ObjectUtil.ValidString(ttw.Locale))
            {
                return ttw;
            }
            return null;
        }
    }
}