using System.Collections.Generic;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
using SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2;

namespace SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2.Util
{
    public class SdmxJsonContactUtil
    {
        public static List<IContactMutableObject> ReadContacts(JsonReader jReader)
        {
            var retList = new List<IContactMutableObject>();
            string fieldName = jReader.GetCurrentFieldName();
            if (fieldName.Equals("contacts"))
            {
                List<IContactMutableObject> contacts = ReadContactsList(jReader);
                retList.AddRange(contacts);
            }
            return retList;
        }

        private static List<IContactMutableObject> ReadContactsList(JsonReader jReader)
        {
            var returnList = new List<IContactMutableObject>();
            while (jReader.MoveNext())
            {
                if (jReader.IsEndArray())
                {
                    break;
                }
                if (jReader.IsStartObject())
                {
                    var aContact = ReadContact(jReader);
                    returnList.Add(aContact);
                }
            }
            return returnList;
        }

        private static IContactMutableObject ReadContact(JsonReader jReader)
        {
            var aContact = new ContactMutableObjectCore();
            while (jReader.MoveNext())
            {
                if (jReader.IsEndObject())
                {
                    break;
                }
                string fieldName = jReader.GetCurrentFieldName();
                string value = jReader.GetValueAsString();
                if (fieldName.Equals("departments"))
                {
                    var departments = SdmxJsonNameableUtil.ReadTextTypeWrappers(jReader);
                    foreach (var item in departments)
                    {
                        aContact.AddDepartment(item);
                    }
                }
                else if (fieldName.Equals("roles"))
                {
                    var roles = SdmxJsonNameableUtil.ReadTextTypeWrappers(jReader);
                    foreach (var item in roles)
                    {
                        aContact.AddRole(item);
                    }
                }
                else if (fieldName.Equals("names"))
                {
                    var names = SdmxJsonNameableUtil.ReadTextTypeWrappers(jReader);
                    foreach (var item in names)
                    {
                        aContact.AddName(item);
                    }
                }
                else if (fieldName.Equals("telephones"))
                {
                    if (value != null)
                    {
                        aContact.AddTelephone(value);
                    }
                }
                else if (fieldName.Equals("faxes"))
                {
                    if (value != null)
                    {
                        aContact.AddFax(value);
                    }
                }
                else if (fieldName.Equals("emails"))
                {
                    if (value != null)
                    {
                        aContact.AddEmail(value);
                    }
                }
                else if (fieldName.Equals("x400s"))
                {
                    if (value != null)
                    {
                        aContact.AddX400(value);
                    }
                }
                else if (fieldName.Equals("uris"))
                {
                    if (value != null)
                    {
                        aContact.AddUri(value);
                    }
                }
                else if (fieldName.Equals("id"))
                {
                    if (value != null)
                    {
                        aContact.Id = value;
                    }
                }
            }

            return aContact;
        }
    }
}