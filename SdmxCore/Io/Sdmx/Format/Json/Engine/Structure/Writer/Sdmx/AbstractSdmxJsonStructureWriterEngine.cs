// -----------------------------------------------------------------------
// <copyright file="AbstractSdmxJsonStructureWriterEngine.cs" company="EUROSTAT">
//   Date Created : 2023-05-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Json.Engine.Structure.Writer.Sdmx
{
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using System.Collections.Generic;
    using Io.Sdmx.Core.Sdmx.Api.Model.Structure;
    using System.IO;
    using System;
    using Org.Sdmxsource.Json;
    using Io.Sdmx.Format.Json.Util;
    using Io.Sdmx.Api.Singleton;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    public abstract class AbstractSdmxJsonStructureWriterEngine : AbstractJsonStructureWriterEngine
    {
        protected AbstractSdmxJsonStructureWriterEngine(bool include, params SdmxStructureType[] types)
        : base(include, types)
        {
        }

        public override void WriteStructures(ISdmxObjects objects, IDictionary<string, IExternalMaintainableLinks> additionalLinks, Stream outStream)
        {
            try
            {
                base.WriteStructures(objects, additionalLinks, outStream);
            }
            catch (Exception th)
            {
                // TODO write error object
            }
        }

        public void LoadInterceptors(Interceptor type, JsonGenerator gen, ISdmxObjects objects)
        {
            switch (type)
            {
                case Interceptor.BeforeWrite:
                    //TODO SDMXCORE
                    SdmxJsonWriterUtil.WriteMeta(gen, objects, SchemaLocation);
                    gen.WriteObjectFieldStart("data");
                    break;
                case Interceptor.AfterWrite:
                    gen.WriteEndObject();
                    break;
            }
        }

        protected abstract string SchemaLocation { get; }

        /// <summary> 
        /// Returns the type as per the schema, or defaults to the lower case class name plus a 's' to make it plaural 
        /// </summary> 
        protected override string GetMaintainableJsonKey(SdmxStructureType maintainableType)
        {
            switch (maintainableType.EnumType)
            {
                case SdmxStructureEnumType.Dsd: return "dataStructures";
                case SdmxStructureEnumType.CategoryScheme: return "categorySchemes";
                case SdmxStructureEnumType.ConceptScheme: return "conceptSchemes";
                case SdmxStructureEnumType.Hierarchy: return "hierarchicalCodelists";
                case SdmxStructureEnumType.AgencyScheme: return "agencySchemes";
                case SdmxStructureEnumType.DataProviderScheme: return "dataProviderSchemes";
                case SdmxStructureEnumType.DataConsumerScheme: return "dataConsumerSchemes";
                case SdmxStructureEnumType.OrganisationScheme: return "organisationUnitSchemes";
                case SdmxStructureEnumType.ReportingTaxonomy: return "reportingTaxonomies";
                case SdmxStructureEnumType.ProvisionAgreement: return "provisionAgreements";
                case SdmxStructureEnumType.StructureSet: return "structureSets";
                case SdmxStructureEnumType.ContentConstraint: return "contentConstraints";
                case SdmxStructureEnumType.Process: return "processes";
                case SdmxStructureEnumType.CodeList: return "codelists";
                case SdmxStructureEnumType.Dataflow: return "dataflows";
                case SdmxStructureEnumType.MetadataFlow: return "metadataflows";
                case SdmxStructureEnumType.Msd: return "metadataStructures";
                case SdmxStructureEnumType.MetadataProviderScheme: return "metadataProviderSchemes";
                //            case METADATA_PROVISION : return "metadataProvisionAgreements";
                case SdmxStructureEnumType.Categorisation: return "categorisations";
                default:
                    return maintainableType.UrnClass.ToLower() + "s";
            }
        }

        //TODO SDMXCORE GEO
        /*
            //@Override
            protected string GeographicCodelistJsonKey => null;

            //@Override
            protected string GeoGridCodelistJsonKey => null;

            //@Override
            protected JsonMaintainableStructureWriterEngine<IGeographicCodelistObject> GeographicalCodelistWriter => null;

            //@Override
            protected JsonMaintainableStructureWriterEngine<IGeoGridCodelistObject> GeoGridCodelistWriter => null;
        */
    }
}