// -----------------------------------------------------------------------
// <copyright file="SdmxJsonDataStructureWriterEngineV2.cs" company="EUROSTAT">
//   Date Created : 2023-05-11
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Json.Engine.Structure.Writer.Sdmx.V2
{
    using Org.Sdmxsource.Json;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Io.Sdmx.Api.Singleton;
    using Io.Sdmx.Core.Sdmx.Api.Model.Structure;
    using Io.Sdmx.Utils.Core.Application;
    using System.Collections.Generic;
    using Io.Sdmx.Format.Json.Util;
    using System.Linq;

    public class SdmxJsonDataStructureWriterEngineV2 : SdmxJsonMaintainableStructureWriterEngineV2<IDataStructureObject>, IFusionSingleton
    {
        private static SdmxJsonDataStructureWriterEngineV2 _instance;

        public static SdmxJsonDataStructureWriterEngineV2 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SdmxJsonDataStructureWriterEngineV2();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }

        public void DestroyInstance()
        {
            _instance = null;
        }

        private SdmxJsonDataStructureWriterEngineV2()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd))
        {
        }

        protected override void WriteStructureInternal(JsonGenerator jsonGenerator, IExternalMaintainableLinks externalLinks, IDataStructureObject dsd)
        {
            //NOTE SDMXSOURCE_SDMXCORE CHANGE
            if (dsd.MetadataStructure != null)
            {
                jsonGenerator.WriteStringField("metadata", dsd.MetadataStructure.TargetUrn.AbsoluteUri);
            }
            WriteDataStructureComponents(jsonGenerator, externalLinks, dsd);
        }

        private void WriteDataStructureComponents(JsonGenerator jsonGenerator, IExternalMaintainableLinks externalLinks, IDataStructureObject dsd)
        {
            jsonGenerator.WriteObjectFieldStart("dataStructureComponents");

            WriteAttributeList(dsd, externalLinks, jsonGenerator);
            WriteDimensionList(dsd, externalLinks, jsonGenerator);
            WriteGroups(dsd, externalLinks, jsonGenerator);
            WriteMeasureList(dsd, externalLinks, jsonGenerator);

            jsonGenerator.WriteEndObject();
        }

        private void WriteAttributeList(IDataStructureObject dsd, IExternalMaintainableLinks externalLinks, JsonGenerator jsonGenerator)
        {
            if (dsd.AttributeList != null)
            {
                jsonGenerator.WriteObjectFieldStart("attributeList");
                IAttributeList attributeList = dsd.AttributeList;
                base.WriteIdentifiable(jsonGenerator, attributeList, externalLinks);
                //			WriteReportingYearStartDays(attributeList, jsonGenerator);
                IList<IAttributeObject> attributeListArr = attributeList.Attributes;
                if (ObjectUtil.ValidCollection(attributeListArr))
                {
                    jsonGenerator.WriteArrayFieldStart("attributes");
                    WriteListOfAttributes(attributeListArr, externalLinks, jsonGenerator);
                    jsonGenerator.WriteEndArray();
                }
                jsonGenerator.WriteEndObject();
            }
        }

        private void WriteListOfAttributes(IList<IAttributeObject> attributes, IExternalMaintainableLinks externalLinks, JsonGenerator jsonGenerator)
        {
            foreach (IAttributeObject attr in attributes)
            {
                jsonGenerator.WriteStartObject();

                string usage = attr.Mandatory ? "mandatory" : "optional";
                SdmxJsonStaticHelper.WriteNonNullString(jsonGenerator, "usage", usage);
                WriteAttributeRelationship(attr, jsonGenerator);

                if (attr.ConceptRoles.Any())
                {
                    WriteConceptRoles(jsonGenerator, attr.ConceptRoles);
                }
                WriteComponent(attr, externalLinks, jsonGenerator);
                jsonGenerator.WriteEndObject();
            }
        }

        private void WriteConceptRoles(JsonGenerator jsonGenerator, IList<ICrossReference> roles)
        {
            jsonGenerator.WriteArrayFieldStart("conceptRoles");
            foreach (ICrossReference reference in roles)
            {
                jsonGenerator.WriteString(reference.TargetUrn.AbsoluteUri);
            }
            jsonGenerator.WriteEndArray();
        }

        /// <summary> 
        /// <p>This method is used to create the attributeRelationship as defined in the JSON Schemea </p> 
        /// <p>The only issue is that the Schema, the documentation and the samples are all contradicting eachover. </p> 
        /// <p>The Schema wants things like AttachmentGroup and dimensionReferences to be URNS's where our JAVA returns these in IDs </p> 
        /// <p>the official example. however uses ID~s like we do, and not URNS <a href="https://github.com/sdmx-twg/sdmx-json/blob/develop/structure-message/docs/1-sdmx-json-field-guide.md#attributerelationship">https://github.com/sdmx-twg/sdmx-json/blob/develop/structure-message/docs/1-sdmx-json-field-guide.md#attributerelationship</a> </p> 
        /// <p>The description here, however backs up what the Schema says, So I am not sure what needs to be actually put here, if it is, indeed URNS. then we need a method on IAttributeObject to get the actual group object. and not the ID, and we need a way of getting the Dimension URN's instead of ID </p> 
        /// <p>Also, attachmentLevel is not in the Schema, and the documentation says <q>The attachment level of the attribute will be determined by the data format and which dimensions are referenced.</q></p> 
        /// </summary>
        /// <param name=" attr"> 
        /// </param>
        /// <param name=" jsonGenerator"> 
        /// @ 
        /// </param> 
        private void WriteAttributeRelationship(IAttributeObject attr, JsonGenerator jsonGenerator)
        {
            jsonGenerator.WriteObjectFieldStart("attributeRelationship");
            switch (attr.AttachmentLevel)
            {
                case AttributeAttachmentLevel.DataSet:
                    jsonGenerator.WriteObjectFieldStart("dataflow");
                    jsonGenerator.WriteEndObject();
                    // none
                    break;
                case AttributeAttachmentLevel.DimensionGroup:
                    IList<string> dimensionReferences = attr.DimensionReferences;
                    jsonGenerator.WriteArrayFieldStart("dimensions");
                    foreach (string dimId in dimensionReferences)
                    {
                        jsonGenerator.WriteString(dimId);
                    }
                    jsonGenerator.WriteEndArray();
                    //dimensions
                    break;
                case AttributeAttachmentLevel.Group:
                    SdmxJsonStaticHelper.WriteNonNullString(jsonGenerator, "group", attr.AttachmentGroup);
                    //group
                    break;
                case AttributeAttachmentLevel.Observation:
                    jsonGenerator.WriteObjectFieldStart("observation");
                    jsonGenerator.WriteEndObject();

                    break;
                default:
                    // attachmentGroups in the schema is not supported here
                    break;

            }
            jsonGenerator.WriteEndObject();

            //measure relationship
            //NOTE SDMXSOURCE_SDMXCORE CHANGE
            if (attr.MeasureRelationships.Any())
            {
                jsonGenerator.WriteArrayFieldStart("measureRelationship");
                foreach (string measure in attr.MeasureRelationships)
                {
                    jsonGenerator.WriteString(measure);
                }
                jsonGenerator.WriteEndArray();
            }

        }

        private void WriteDimensionList(IDataStructureObject dsd, IExternalMaintainableLinks externalLinks, JsonGenerator jsonGenerator)
        {
            jsonGenerator.WriteObjectFieldStart("dimensionList");
            IDimensionList dimensionList = dsd.DimensionList;
            if (dimensionList != null)
            {
                WriteIdentifiable(jsonGenerator, dimensionList, externalLinks);
            }

            IList<IDimension> dimensions = dsd.GetDimensions(SdmxStructureEnumType.Dimension);
            if (dimensions.Any())
            {
                jsonGenerator.WriteArrayFieldStart("dimensions");
                foreach (IDimension dim in dimensions)
                {
                    WriteDimension(dim, externalLinks, jsonGenerator);
                }
                jsonGenerator.WriteEndArray();
            }

            IList<IDimension> measureDims = dsd.GetDimensions(SdmxStructureEnumType.MeasureDimension);
            if (measureDims.Any())
            {
                jsonGenerator.WriteArrayFieldStart("measureDimensions");
                foreach (IDimension dim in measureDims)
                {
                    WriteDimension(dim, externalLinks, jsonGenerator);
                }
                jsonGenerator.WriteEndArray();
            }

            IList<IDimension> timeDims = dsd.GetDimensions(SdmxStructureEnumType.TimeDimension);
            if (timeDims.Any())
            {
                jsonGenerator.WriteArrayFieldStart("timeDimensions");
                foreach (IDimension dim in timeDims)
                {
                    WriteDimension(dim, externalLinks, jsonGenerator);
                }
                jsonGenerator.WriteEndArray();
            }


            jsonGenerator.WriteEndObject();
        }

        private void WriteDimension(IDimension IdimObject, IExternalMaintainableLinks externalLinks, JsonGenerator jsonGenerator)
        {
            jsonGenerator.WriteStartObject();  //Start Dimension
            jsonGenerator.WriteNumberField("position", IdimObject.Position);
            if (IdimObject.MeasureDimension)
            {
                jsonGenerator.WriteStringField("type", "MeasureDimension");
            }
            else if (IdimObject.TimeDimension)
            {
                jsonGenerator.WriteStringField("type", "TimeDimension");
            }
            else
            {
                jsonGenerator.WriteStringField("type", "Dimension");
            }
            if (IdimObject.ConceptRole.Any())
            {
                WriteConceptRoles(jsonGenerator, IdimObject.ConceptRole);
            }
            WriteComponent(IdimObject, externalLinks, jsonGenerator);
            jsonGenerator.WriteEndObject();

        }

        private void WriteGroups(IDataStructureObject dsd, IExternalMaintainableLinks externalLinks, JsonGenerator jsonGenerator)
        {
            if (ObjectUtil.ValidCollection(dsd.Groups))
            {
                jsonGenerator.WriteArrayFieldStart("groups");
                foreach (IGroup group in dsd.Groups)
                {
                    WriteGroup(group, externalLinks, jsonGenerator);
                }
                jsonGenerator.WriteEndArray();
            }
        }

        private void WriteGroup(IGroup group, IExternalMaintainableLinks externalLinks, JsonGenerator jsonGenerator)
        {
            jsonGenerator.WriteStartObject();
            WriteIdentifiable(jsonGenerator, group, externalLinks);
            jsonGenerator.WriteStringField("id", group.Id);

            if (group.AttachmentConstraintRef != null)
            {
                SdmxJsonStaticHelper.WriteNonNullString(jsonGenerator, "attachmentConstraint", group.AttachmentConstraintRef.TargetUrn.AbsoluteUri);
            }

            jsonGenerator.WriteArrayFieldStart("groupDimensions");
            foreach (string dimensionRef in group.DimensionRefs)
            {
                jsonGenerator.WriteString(dimensionRef);
            }
            jsonGenerator.WriteEndArray();

            jsonGenerator.WriteEndObject();
        }

        private void WriteMeasureList(IDataStructureObject dsd, IExternalMaintainableLinks externalLinks, JsonGenerator jsonGenerator)
        {
            IMeasureList measureList = dsd.MeasureList;
            if (measureList == null)
            {
                return;
            }
            jsonGenerator.WriteObjectFieldStart("measureList");
            WriteIdentifiable(jsonGenerator, measureList, externalLinks);
            WriteMeasures(measureList, externalLinks, jsonGenerator);
            jsonGenerator.WriteEndObject();
        }

        private void WriteMeasures(IMeasureList measureList, IExternalMaintainableLinks externalLinks, JsonGenerator jsonGenerator)
        {
            jsonGenerator.WriteArrayFieldStart("measures");
            foreach (IMeasure currentMeasure in measureList.Measures)
            {
                jsonGenerator.WriteStartObject();
                string usage = currentMeasure.Usage == UsageType.Mandatory ? "mandatory" : "optional";
                SdmxJsonStaticHelper.WriteNonNullString(jsonGenerator, "usage", usage);
                WriteComponent(currentMeasure, externalLinks, jsonGenerator);
                jsonGenerator.WriteEndObject();
            }
            jsonGenerator.WriteEndArray();
        }
    }
}