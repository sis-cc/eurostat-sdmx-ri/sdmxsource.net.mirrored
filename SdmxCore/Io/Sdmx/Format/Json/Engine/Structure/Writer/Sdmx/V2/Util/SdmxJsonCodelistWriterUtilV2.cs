// -----------------------------------------------------------------------
// <copyright file="SdmxJsonCodelistWriterUtilV2.cs" company="EUROSTAT">
//   Date Created : 2023-05-11
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Json.Engine.Structure.Writer.Sdmx.V2.Util
{
    using System.Collections.Generic;
    using System.Linq;
    using Io.Sdmx.Utils.Json;
    using Org.Sdmxsource.Json;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Util;

    public class SdmxJsonCodelistWriterUtilV2
    {
        //NOTE SDMXSOURCE_SDMXCORE CHANGE: USING EXISTING ICodelistInheritanceRuleObject instead of sdmxcore's ICodelistInheritanceRuleObject
        public static void WriteInheritence(JsonGenerator jsonGenerator, ICodelistObject cl)
        {
            if (ObjectUtil.ValidCollection(cl.CodelistExtensions))
            {
                jsonGenerator.WriteArrayFieldStart("codelistExtensions");
                foreach (ICodelistInheritanceRule clInheritance in cl.CodelistExtensions)
                {
                    WriteInheritanceRuleObject(clInheritance, jsonGenerator);
                }
                jsonGenerator.WriteEndArray();
            }
        }

        private static void WriteInheritanceRuleObject(ICodelistInheritanceRule rule, JsonGenerator jsonGenerator)
        {
            jsonGenerator.WriteStartObject();
            if (rule.Prefix != null)
            {
                jsonGenerator.WriteStringField("prefix", rule.Prefix);
            }
            jsonGenerator.WriteStringField("codelist", rule.CodelistRef.TargetUrn.AbsoluteUri);
            WriteInheritanceMembers(rule.CodeSelection.MemberValues, rule.CodeSelection.IsInclusive, jsonGenerator);
            jsonGenerator.WriteEndObject();
        }

        private static void WriteInheritanceMembers(IList<IMemberValue> members, bool included, JsonGenerator writer)
        {
            if (ObjectUtil.ValidCollection(members))
            {
                string elementName = included ? "inclusiveCodeSelection" : "exclusiveCodeSelection";
                writer.WriteObjectFieldStart(elementName);

                List<string> wildcaredMembers = new List<string>(members.Count);
                List<IMemberValue> memberValues = new List<IMemberValue>(members.Count);
                foreach (IMemberValue member in members)
                {
                    if (member.Value.Contains("%"))
                    {
                        wildcaredMembers.Add(member.Value);
                    }
                    else
                    {
                        memberValues.Add(member);
                    }
                }
                if (wildcaredMembers.Any())
                {
                    JsonWriterUtil.WriteStringArray(writer, "wildcardedMemberValues", wildcaredMembers);
                }
                if (memberValues.Any())
                {
                    WriteInheritanceMemberValues(memberValues, writer);
                }
                writer.WriteEndObject();
            }
        }

        private static void WriteInheritanceMemberValues(IList<IMemberValue> members, JsonGenerator writer)
        {
            writer.WriteArrayFieldStart("memberValues");
            foreach (IMemberValue member in members)
            {
                WriteInheritanceMemberValue(member.Value, member.Cascade == CascadeSelection.True, writer);
            }
            writer.WriteEndArray();
        }

        private static void WriteInheritanceMemberValue(string value, bool cascade, JsonGenerator writer)
        {
            writer.WriteStartObject();
            writer.WriteStringField("value", value);
            if (cascade)
            {
                writer.WriteBooleanField("cascadeValues", cascade);
            }
            writer.WriteEndObject();
        }
    }
}