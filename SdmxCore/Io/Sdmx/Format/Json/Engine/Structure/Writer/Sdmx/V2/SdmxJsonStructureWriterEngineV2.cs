// -----------------------------------------------------------------------
// <copyright file="SdmxJsonStructureWriterEngineV2.cs" company="EUROSTAT">
//   Date Created : 2023-05-11
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Json.Engine.Structure.Writer.Sdmx.V2
{
    using Io.Sdmx.Api.Singleton;
    using Io.Sdmx.Utils.Core.Application;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using SdmxCore.IO.Sdmx.Format.Json.Constant;

    public class SdmxJsonStructureWriterEngineV2 : AbstractSdmxJsonStructureWriterEngine, IFusionSingleton
    {
        private static SdmxJsonStructureWriterEngineV2 _instance;

        public static SdmxJsonStructureWriterEngineV2 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SdmxJsonStructureWriterEngineV2();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }

        public void DestroyInstance()
        {
            _instance = null;
        }

        private SdmxJsonStructureWriterEngineV2()
            : base(false)
        {
        }

        /// <summary> 
        /// Returns the type as per the schema, or defaults to the lower case class name plus a 's' to make it plaural 
        /// </summary> 
        protected override string GetMaintainableJsonKey(SdmxStructureType maintainableType)
        {
            switch (maintainableType.EnumType)
            {
                case SdmxStructureEnumType.ContentConstraint: return "dataConstraints";
                case SdmxStructureEnumType.RepresentationMap: return "representationMaps";
                case SdmxStructureEnumType.StructureMap: return "structureMaps";
                default:
                    return base.GetMaintainableJsonKey(maintainableType);
            }
        }

        //TODO SDMXCORE GEO
        /*
        //@Override
        protected string GeographicCodelistJsonKey => "geographicCodelists";

        //@Override
        protected string GeoGridCodelistJsonKey => "geoGridCodelists";

        //@Override
        protected JsonMaintainableStructureWriterEngine<IGeographicCodelistObject> GeographicalCodelistWriter => SdmxJsonGeographicCodelistWriterEngineV2.Instance;

        //@Override
        protected JsonMaintainableStructureWriterEngine<IGeoGridCodelistObject> GeoGridCodelistWriter => SdmxJsonGeoGridCodelistWriterEngineV2.Instance;*/

        protected override string SchemaLocation => SdmxJsonSchema.StructureV2;
    }
}