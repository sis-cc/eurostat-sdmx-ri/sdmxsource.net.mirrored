// -----------------------------------------------------------------------
// <copyright file="SdmxJsonIdentifiableStructureWriterEngineV2.cs" company="EUROSTAT">
//   Date Created : 2023-05-11
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Json.Engine.Structure.Writer.Sdmx.V2
{
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Io.Sdmx.Core.Sdmx.Api.Model.Structure;
    using Org.Sdmxsource.Json;
    using SdmxCore.IO.Sdmx.Format.Json.Constant;
    using Io.Sdmx.Format.Json.Util;

    public abstract class SdmxJsonIdentifiableStructureWriterEngineV2<T>
        where T : IIdentifiableObject
    {
        protected abstract void WriteStructureInternal(JsonGenerator jsonGenerator, IExternalMaintainableLinks externalLinks, T identifiable);

        protected void WriteIdentifiable(JsonGenerator jsonGenerator, IIdentifiableObject identifiable, IExternalMaintainableLinks externalLinks)
        {
            SdmxJsonStaticHelper.WriteIdentifiable(jsonGenerator, externalLinks, identifiable, SdmxJsonSchema.GetFromEnum(JsonSchemaEnumType.StructureV2));
        }

        protected void WriteComponent(IComponent IcomponentObject, IExternalMaintainableLinks externalLinks, JsonGenerator jsonGenerator)
        {
            WriteIdentifiable(jsonGenerator, IcomponentObject, externalLinks);
            if (IcomponentObject.Representation != null)
            {
                SdmxJsonStaticHelper.WriteRepresentation(jsonGenerator, IcomponentObject.Representation, IncludeMinMaxInLocalRepresentation(), "localRepresentation");
            }
            if (IcomponentObject.ConceptRef != null)
            {
                jsonGenerator.WriteStringField("conceptIdentity", IcomponentObject.ConceptRef.TargetUrn.AbsoluteUri);
            }
        }

        protected bool IncludeMinMaxInLocalRepresentation()
        {
            return true;
        }

        //	protected void WriteLinks(JsonGenerator jsonGenerator, IList<ILink> links)  {
        //		SdmxJsonStaticHelper.WriteStoredLinks(jsonGenerator, links);
        //	}

        //	protected ISelfLink GetSelfLink(IdentifiableObject ident) {
        //
        //		ISelfLink selfLink =  new SelfLink(type, ident.Urn);
        //		selfLink.SetHreflang("en");
        //		if(ident.Uri != null) {
        //			selfLink.SetURI(ident.Uri.ToString());
        //		} else {
        //			selfLink.SetURI(SdmxJsonStructureWriterEngineV2.Instance.SchemaLocation);
        //		}
        //		return selfLink;
        //	}
    }
}