// -----------------------------------------------------------------------
// <copyright file="SdmxJsonCategorySchemeWriterEngineV2.cs" company="EUROSTAT">
//   Date Created : 2023-05-11
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Json.Engine.Structure.Writer.Sdmx.V2
{
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Json;
    using Io.Sdmx.Core.Sdmx.Api.Model.Structure;
    using Org.Sdmxsource.Util;
    using Io.Sdmx.Utils.Core.Application;
    using Io.Sdmx.Api.Singleton;
    using System.Collections.Generic;

    public class SdmxJsonCategorySchemeWriterEngineV2 : SdmxJsonItemSchemeWriterEngineV2<ICategoryObject, ICategorySchemeObject>, IFusionSingleton
    {
        private static SdmxJsonCategorySchemeWriterEngineV2 _instance;

        public static SdmxJsonCategorySchemeWriterEngineV2 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SdmxJsonCategorySchemeWriterEngineV2();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }

        public void DestroyInstance()
        {
            _instance = null;
        }

        private SdmxJsonCategorySchemeWriterEngineV2()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CategoryScheme))
        {

        }

        protected override void WriteItemDetails(JsonGenerator jsonGenerator, IExternalMaintainableLinks externalLinks, ICategoryObject item)
        {
            WriteItems(jsonGenerator, externalLinks, item.Items);
        }

        protected override void WriteItems(JsonGenerator jsonGenerator, IExternalMaintainableLinks externalLinks, IList<ICategoryObject> items)
        {
            if (!ObjectUtil.ValidCollection(items))
            {
                return;
            }

            jsonGenerator.WriteArrayFieldStart(ItemName);
            foreach (ICategoryObject currentItem in items)
            {
                WriteItem(jsonGenerator, externalLinks, currentItem);
            }
            jsonGenerator.WriteEndArray();
        }

        protected override string ItemName => "categories";
    }
}