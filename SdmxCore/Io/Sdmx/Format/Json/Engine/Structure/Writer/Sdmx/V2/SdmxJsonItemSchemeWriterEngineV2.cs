// -----------------------------------------------------------------------
// <copyright file="SdmxJsonItemSchemeWriterEngineV2.cs" company="EUROSTAT">
//   Date Created : 2023-05-11
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Json.Engine.Structure.Writer.Sdmx.V2
{
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Io.Sdmx.Core.Sdmx.Api.Model.Structure;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Json;
    using System.Collections.Generic;

    public abstract class SdmxJsonItemSchemeWriterEngineV2<R, T> : SdmxJsonMaintainableStructureWriterEngineV2<T>
        where R : IItemObject
        where T : IItemSchemeObject<R>
    {
        protected SdmxJsonItemSchemeWriterEngineV2(SdmxStructureType sypportedType)
            : base(sypportedType)
        {
        }

        protected override void WriteStructureInternal(JsonGenerator jsonGenerator, IExternalMaintainableLinks externalLinks, T identifiable)
        {
            IList<R> items = identifiable.Items;
            jsonGenerator.WriteBooleanField("isPartial", identifiable.Partial);
            WriteItems(jsonGenerator, externalLinks, items);
        }

        protected virtual void WriteItems(JsonGenerator jsonGenerator, IExternalMaintainableLinks externalLinks, IList<R> items)
        {
            if (!ObjectUtil.ValidCollection(items))
            {
                return;
            }
            jsonGenerator.WriteArrayFieldStart(ItemName);
            foreach (R currentItem in items)
            {
                WriteItem(jsonGenerator, externalLinks, currentItem);
            }
            jsonGenerator.WriteEndArray();
        }

        protected virtual void WriteItem(JsonGenerator jsonGenerator, IExternalMaintainableLinks externalLinks, R currentItem)
        {
            jsonGenerator.WriteStartObject();
            WriteNameable(jsonGenerator, currentItem, externalLinks);
            WriteItemDetails(jsonGenerator, externalLinks, currentItem);
            jsonGenerator.WriteEndObject();
        }

        protected abstract void WriteItemDetails(JsonGenerator jsonGenerator, IExternalMaintainableLinks externalLinks, R item);

        protected abstract string ItemName { get; }
    }
}