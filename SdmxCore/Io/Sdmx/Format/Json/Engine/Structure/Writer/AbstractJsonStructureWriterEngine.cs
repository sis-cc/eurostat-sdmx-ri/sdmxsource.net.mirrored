// -----------------------------------------------------------------------
// <copyright file="AbstractJsonStructureWriterEngine.cs" company="EUROSTAT">
//   Date Created : 2023-05-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Json.Engine.Structure.Writer
{
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Io.Sdmx.Core.Sdmx.Api.Model.Structure;
    using System.IO;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Io.Sdmx.Core.Sdmx.Util;
    using System;
    using Org.Sdmxsource.Json;
    using Io.Sdmx.Format.Json.Engine.Structure.Writer.Sdmx;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Util;
    using Io.Sdmx.Core.Sdmx.Engine.Structure;
    using Io.Sdmx.Core.Sdmx.Api.Engine.Structure;
    using Org.Sdmxsource.Sdmx.Util.Sort;
    using Org.Sdmxsource.Util.Extensions;
    using Io.Sdmx.Utils.Json;
    using Io.Sdmx.Utils.Core.Thread;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using System.Linq;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Io.Sdmx.Format.Json.Engine.Structure.Writer.Sdmx.V2;
    using log4net;

    public abstract class AbstractJsonStructureWriterEngine : AbstractStructureWriterEngine, IStructureWriterEngine
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(AbstractJsonStructureWriterEngine));
        private readonly Dictionary<SdmxStructureType, IJsonMaintainableStructureWriterEngine<IMaintainableObject>> _writers
                = new Dictionary<SdmxStructureType, IJsonMaintainableStructureWriterEngine<IMaintainableObject>>();

        public AbstractJsonStructureWriterEngine(bool include, params SdmxStructureType[] types)
            : base(include, types)
        {
        }

        protected AbstractJsonStructureWriterEngine(bool include, ICollection<SdmxStructureType> types)
            : base(include, types)
        {
        }

        public virtual void WriteStructure(IMaintainableObject sdmxObject, IExternalMaintainableLinks additionalLinks, Stream outStream)
        {
            StructureWriterUtil.WriteStructure(this, sdmxObject, additionalLinks, outStream);
        }

        public virtual void WriteStructures(ISdmxObjects objects, IDictionary<string, IExternalMaintainableLinks> additionalLinks, Stream outStream)
        {
            //ThreadLocalUtil.StoreOnThread(Annotable.IncludeInternalKey, true);
            using (JsonGenerator jsonGenerator = JsonWriterUtil.CreateJsonWriter(outStream))
            {
                try
                {
                    jsonGenerator.WriteStartObject();
                    if (this is AbstractSdmxJsonStructureWriterEngine engine)
                    {
                        engine.LoadInterceptors(Interceptor.BeforeWrite, jsonGenerator, objects);
                    }
                    foreach (SdmxStructureType maintainableType in SdmxStructureType.MaintainableStructureTypes)
                    {
                        string arrayName = GetMaintainableJsonKey(maintainableType);
                        switch (maintainableType.EnumType)
                        {
                            case SdmxStructureEnumType.CodeList:
                                {
                                    ISet<ICodelistObject> standardCodelists = 
                                        new SortedSet<ICodelistObject>(MaintainableSortByIdentifiers<ICodelistObject>.Instance);
                                    //TODO SDMXCORE GEO
                                    //ISet<IGeographicCodelistObject> geographicalCodelists = new TreeSet<>(MaintainableSortByIdentifiers.INSTANCE);
                                    //ISet<IGeoGridCodelistObject> geoCodelists = new TreeSet<>(MaintainableSortByIdentifiers.INSTANCE);

                                    foreach (ICodelistObject cl in objects.Codelists)
                                    {
                                        /*if(cl.IsGeoCodelist()) {
                                            if(cl is IGeographicCodelistObject) {
                                                geographicalCodelists.Add((IGeographicCodelistObject)cl);
                                            } else if(cl is IGeoGridCodelistObject) {
                                                geoCodelists.Add((IGeoGridCodelistObject)cl);
                                            }
                                        } else {*/
                                        standardCodelists.Add(cl);
                                        //}
                                    }
                                    //WriteStructuresOfType(jsonGenerator, geographicalCodelists, additionalLinks, GeographicCodelistJSONKey, GeographicalCodelistWriter);
                                    //WriteStructuresOfType(jsonGenerator, geoCodelists, additionalLinks, GeoGridCodelistJSONKey, GeoGridCodelistWriter);
                                    WriteStructuresOfType(jsonGenerator, standardCodelists, additionalLinks, arrayName, 
                                        SdmxJsonCodelistWriterEngineV2.Instance);
                                }
                                break;
                            case SdmxStructureEnumType.Categorisation:
                                ISet<ICategorisationObject> categorisations = 
                                    new SortedSet<ICategorisationObject>(MaintainableSortByIdentifiers<ICategorisationObject>.Instance);
                                categorisations.AddAll(objects.GetMaintainables(maintainableType).Cast<ICategorisationObject>());
                                WriteStructuresOfType(jsonGenerator, categorisations, additionalLinks, arrayName,
                                    SdmxJsonCategorisationWriterEngineV2.Instance);
                                break;
                            case SdmxStructureEnumType.CategoryScheme:
                                ISet<ICategorySchemeObject> categorySchemes =
                                    new SortedSet<ICategorySchemeObject>(MaintainableSortByIdentifiers<ICategorySchemeObject>.Instance);
                                categorySchemes.AddAll(objects.GetMaintainables(maintainableType).Cast<ICategorySchemeObject>());
                                WriteStructuresOfType(jsonGenerator, categorySchemes, additionalLinks, arrayName,
                                    SdmxJsonCategorySchemeWriterEngineV2.Instance);
                                break;
                            case SdmxStructureEnumType.ConceptScheme:
                                ISet<IConceptSchemeObject> conceptSchemes =
                                    new SortedSet<IConceptSchemeObject>(MaintainableSortByIdentifiers<IConceptSchemeObject>.Instance);
                                conceptSchemes.AddAll(objects.GetMaintainables(maintainableType).Cast<IConceptSchemeObject>());
                                WriteStructuresOfType(jsonGenerator, conceptSchemes, additionalLinks, arrayName,
                                    SdmxJsonConceptSchemeWriterEngineV2.Instance);
                                break;
                            case SdmxStructureEnumType.Dataflow:
                                ISet<IDataflowObject> dataflows =
                                    new SortedSet<IDataflowObject>(MaintainableSortByIdentifiers<IDataflowObject>.Instance);
                                dataflows.AddAll(objects.GetMaintainables(maintainableType).Cast<IDataflowObject>());
                                WriteStructuresOfType(jsonGenerator, dataflows, additionalLinks, arrayName,
                                    SdmxJsonDataflowWriterEngineV2.Instance);
                                break;
                            case SdmxStructureEnumType.Dsd:
                                ISet<IDataStructureObject> dsds =
                                    new SortedSet<IDataStructureObject>(MaintainableSortByIdentifiers<IDataStructureObject>.Instance);
                                dsds.AddAll(objects.GetMaintainables(maintainableType).Cast<IDataStructureObject>());
                                WriteStructuresOfType(jsonGenerator, dsds, additionalLinks, arrayName,
                                    SdmxJsonDataStructureWriterEngineV2.Instance);
                                break;
                                //TODO SDMXCORE other types too
                            default: continue;
                        }
                    }
                    jsonGenerator.WriteEndObject();

                    //NOTE SDMXCORE (only for v1 so probably not needed)
                    /*
                    if(this is SdmxJsonStructureWriterEngine) {
                        ((SdmxJsonStructureWriterEngine)this).LoadInterceptors(Interceptor.AfterWrite, jsonGenerator, objects);
                    }
                    */
                }
                catch (Exception e)
                {
                    _log.Error("Error writing structures in Json", e);
                    throw new SdmxException("Error writing structures in Json", e);
                }
                finally
                {
                    //ThreadLocalUtil.ClearFromThread(Annotable.IncludeInternalKey);
                }
            }
        }

        private void WriteStructuresOfType<T>(JsonGenerator jsonGenerator, ISet<T> maintainables, IDictionary<string, 
            IExternalMaintainableLinks> additionalLinks, string arrayName, IJsonMaintainableStructureWriterEngine<T> writer)
            where T : IMaintainableObject
        {
            if (ObjectUtil.ValidCollection(maintainables) && writer != null)
            {
                if (additionalLinks == null)
                {
                    additionalLinks = new Dictionary<string, IExternalMaintainableLinks>(); 
                }
                jsonGenerator.WriteArrayFieldStart(arrayName);
                foreach (T maint in maintainables)
                {
                    //NOTE SDMXCORE IValidatableObject is sdmx-core's custom functionality, don't need it at SdmxSource
                    /*
                    if(maint is IValidatableObject){
                        maint = ItemValidityPeriodHelper.PotentiallyMutateIt((IValidatableObject<?>) maint);
                    }else if(maint is IStructureSetObject){
                        maint =  ItemValidityPeriodHelper.PotentiallyMutateIt((IStructureSetObject)maint);
                    }
                    */
                    additionalLinks.TryGetValue(maint.Urn.AbsoluteUri, out IExternalMaintainableLinks maintLinks);
                    writer.WriteStructure(jsonGenerator, maintLinks, maint);
                }
                jsonGenerator.WriteEndArray();
            }
        }

        /// <summary> 
        /// returns the key to use for the maintainable array 
        /// </summary> 
        protected abstract string GetMaintainableJsonKey(SdmxStructureType maintainableType);

        //TODO SDMXCORE GEO
        /*
        protected abstract string GeographicCodelistJsonKey { get; }
        protected abstract string GeoGridCodelistJsonKey { get; }

        protected abstract JsonMaintainableStructureWriterEngine<IGeographicCodelistObject> GeographicalCodelistWriter { get; }
        protected abstract JsonMaintainableStructureWriterEngine<IGeoGridCodelistObject> GeoGridCodelistWriter { get; }
        */
    }

    public enum Interceptor
    {
        BeforeWrite,
        AfterWrite
    }
}