// -----------------------------------------------------------------------
// <copyright file="SdmxJsonMaintainableStructureWriterEngineV2.cs" company="EUROSTAT">
//   Date Created : 2023-05-11
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Json.Engine.Structure.Writer.Sdmx.V2
{
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Io.Sdmx.Core.Sdmx.Api.Model.Structure;
    using Org.Sdmxsource.Json;

    public abstract class SdmxJsonMaintainableStructureWriterEngineV2<T> : SdmxJsonNameableStructureWriterEngineV2<T>, ISdmxJsonMaintainableStructureWriterEngine<T>
        where T : IMaintainableObject
    {
        private readonly SdmxStructureType _supportedType;

        protected SdmxJsonMaintainableStructureWriterEngineV2(SdmxStructureType sypportedType)
        {
            this._supportedType = sypportedType;
        }

        public void WriteStructure(JsonGenerator jsonGenerator, IExternalMaintainableLinks externalLinks, T maintainable)
        {
            jsonGenerator.WriteStartObject();
            base.WriteNameable(jsonGenerator, maintainable, externalLinks);
            jsonGenerator.WriteStringField("version", maintainable.Version);
            jsonGenerator.WriteStringField("agencyID", maintainable.AgencyId);

            //NOTE SDMXSOURCE_SDMXCORE CHANGE
            jsonGenerator.WriteBooleanField("isExternalReference", maintainable.IsExternalReference.IsTrue);
            if (maintainable.StartDate != null)
            {
                WriteDate(jsonGenerator, "validFrom", maintainable.StartDate);
            }
            if (maintainable.EndDate != null)
            {
                WriteDate(jsonGenerator, "validTo", maintainable.EndDate);
            }

            WriteStructureInternal(jsonGenerator, externalLinks, maintainable);
            jsonGenerator.WriteEndObject();
        }

        private void WriteDate(JsonGenerator jsonGenerator, string fieldName, ISdmxDate aDate)
        {
            if (aDate == null)
            {
                return;
            }
            string date = aDate.DateInSdmxFormat;
            jsonGenerator.WriteStringField(fieldName, date);
        }

        public SdmxStructureType SupportedType => this._supportedType;
    }
}