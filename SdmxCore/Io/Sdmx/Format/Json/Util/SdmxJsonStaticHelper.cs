// -----------------------------------------------------------------------
// <copyright file="SdmxJsonStaticHelper.cs" company="EUROSTAT">
//   Date Created : 2023-05-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Json.Util
{
    using Io.Sdmx.Api.Sdmx.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Io.Sdmx.Core.Sdmx.Api.Model.Structure;
    using Org.Sdmxsource.Json;
    using SdmxCore.IO.Sdmx.Format.Json.Constant;
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Io.Sdmx.Utils.Sdmx.Xs;
    using Io.Sdmx.Utils.Core.Object;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

    public class SdmxJsonStaticHelper
    {
        public static void WriteIdentifiable(JsonGenerator jsonGenerator,
            IExternalMaintainableLinks externalLinks, IIdentifiableObject identifiable, SdmxJsonSchema schema)
        {
            WriteLinks(identifiable, schema, externalLinks, jsonGenerator);
            jsonGenerator.WriteStringField("id", identifiable.Id);
            WriteAnnotable(jsonGenerator, identifiable);
        }

        public static void WriteLinks(IIdentifiableObject ident, SdmxJsonSchema schema,
            IExternalMaintainableLinks externalLinks, JsonGenerator writer)
        {
            ISet<ILinkObject> links = externalLinks != null ? externalLinks.GetLinks(ident.Urn.AbsoluteUri) : null;
            writer.WriteArrayFieldStart("links");
            SdmxStructureType type = ident.StructureType;
            //NOTE SDMXSOURCE_SDMXCORE CHANGE: in SDMX 3.0 primary measure is replaced
            // SDMX JSON 2.0.0 Does not support PrimaryMeasure only Measure
            if (schema.EnumType == JsonSchemaEnumType.StructureV2 && type == SdmxStructureEnumType.PrimaryMeasure)
            {
                ident = ((IMeasure)ident).ToSDMX3Measure();
                // The type must be Measure in SDMX JSON 2.0.0
                // https://github.com/sdmx-twg/sdmx-json/blob/master/structure-message/tools/schemas/2.0.0/sdmx-json-structure-schema.json#L542
                type = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Measure);
            }
            else if (schema.EnumType == JsonSchemaEnumType.StructureV1 && type == SdmxStructureEnumType.Measure)
            {
                type = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.PrimaryMeasure);
            }
            string selfLinkType = type.UrnClass.ToLower();

            IMaintainableObject maint = ident.StructureType.IsMaintainable ? ident.MaintainableParent : null;
            Uri strUrl = null;
            if (maint != null)
            {
                try
                {
                    strUrl = maint.StructureUrl != null ? maint.StructureUrl : null;
                    if (strUrl == null)
                    {
                        strUrl = maint.ServiceUrl != null ? maint.ServiceUrl : null;
                    }
                }
                catch (UriFormatException e)
                {
                    // TODO Auto-generated catch block
                    Console.Error.WriteLine(e.StackTrace);
                }
            }
            Uri hRef = strUrl;

            WriteLink(new LinkObject(ident, hRef, schema, selfLinkType), writer);

            WriteLinks(links, writer, false);
            WriteLinks(ident.Links, writer, false);

            writer.WriteEndArray();
        }

        public static void WriteLinks(ICollection<ILinkObject> links, JsonGenerator writer, bool includeFieldName)
        {
            if (ObjectUtil.ValidCollection(links))
            {
                if (includeFieldName)
                {
                    writer.WriteArrayFieldStart("links");
                }
                foreach (ILinkObject link in links)
                {
                    WriteLink(link, writer);
                }
                if (includeFieldName)
                {
                    writer.WriteEndArray();
                }
            }
        }

        private static void WriteLink(ILinkObject link, JsonGenerator writer)
        {
            writer.WriteStartObject();
            if (link.Rel != null)
            {
                writer.WriteStringField("rel", link.Rel);
            }
            if (link.HRef != null)
            {
                writer.WriteStringField("href", link.HRef.ToString());
            }
            if (link.Type != null)
            {
                writer.WriteStringField("type", link.Type);
            }
            if (link.Uri != null)
            {
                writer.WriteStringField("uri", link.Uri.ToString());
            }
            if (link.Urn != null)
            {
                writer.WriteStringField("urn", link.Urn.ToString());
            }
            if (ObjectUtil.ValidCollection(link.GetTitles(false)))
            {
                WriteTextTypeWrapperArray(writer, link.GetTitles(false), "titles");
            }
            writer.WriteEndObject();
        }

        public static void WriteAnnotable(JsonGenerator jsonGenerator, IAnnotableObject annotable)
        {
            if (!annotable.Annotations.Any())
            {
                return;
            }
            IList<IAnnotation> annotations = annotable.Annotations;
            jsonGenerator.WriteArrayFieldStart("annotations");
            foreach (IAnnotation IannotationObject in annotations)
            {
                /* //NOTE SDMXSOURCE_SDMXCORE CHANGE
                if (IannotationObject.Type != null) {
                    if(IannotationObject.Type.Equals(IValidityPeriodObject.ValidityType)){
                        continue;
                    }
                }
                */
                jsonGenerator.WriteStartObject();
                WriteNonNullString(jsonGenerator, "id", IannotationObject.Id);
                WriteNonNullString(jsonGenerator, "title", IannotationObject.Title);
                WriteNonNullString(jsonGenerator, "type", IannotationObject.Type);
                if (IannotationObject.Uri != null)
                {
                    jsonGenerator.WriteArrayFieldStart("links");
                    jsonGenerator.WriteStartObject();
                    //TODO What is the link rel for an annotation?
                    jsonGenerator.WriteStringField("href", IannotationObject.Uri.ToString());
                    jsonGenerator.WriteEndObject();
                    jsonGenerator.WriteEndArray();
                }
                if (IannotationObject.Text != null && !IannotationObject.Text.Any())
                {
                    WriteTextTypeWrapperArray(jsonGenerator, IannotationObject.Text, "texts");
                }
                jsonGenerator.WriteEndObject();
            }
            //TODO: Link object contains props from Maintainable (urn). but can be attached to anything, and also is an array, Annotables do not have any fields that a Link object has yet, so we shall ignore it for now
            //WriteLinkableObject(jsonGenerator, annotable);
            jsonGenerator.WriteEndArray();
        }

        public static void WriteNameable(JsonGenerator jsonGenerator, INameableObject nameable)
        {
            //	WriteIdentifiable(jsonGenerator, nameable);
            WriteTextTypeWrapperArray(jsonGenerator, nameable.Names, "names");
            WriteTextTypeWrapperArray(jsonGenerator, nameable.Descriptions, "descriptions");
        }

        /// <summary> 
        /// Writes both the localisedBestMatchText and the text type array to the object 
        /// </summary>
        /// <param name=" jsonGenerator"> 
        /// </param>
        /// <param name=" items"> 
        /// </param>
        /// <param name=" objectFieldName"> 
        /// @ 
        /// @ 
        /// </param> 
        public static void WriteTextTypeWrapperArray(JsonGenerator jsonGenerator, IList<ITextTypeWrapper> items, string objectFieldName)
        {
            if (!items.Any())
            {
                return;
            }
            string singular = StringUtil.Chop(objectFieldName);
            WriteLocalisedBestMatchText(jsonGenerator, items, singular);
            jsonGenerator.WriteObjectFieldStart(objectFieldName);
            foreach (ITextTypeWrapper textTypeWrapper in items)
            {
                WriteNonNullString(jsonGenerator, textTypeWrapper.Locale, textTypeWrapper.Value);
            }
            jsonGenerator.WriteEndObject();

        }

        /// <summary> 
        /// <p>
        ///     A human-readable (best-language-match) description of the target link. 
        ///     This will default to the language from the accept-language header from the request, this should check "titles" for the title 
        /// </p> 
        /// <p>In case that there is no language match for a particular localisable element, it is optional to:</p> 
        /// <ul> 
        ///     <li>return the element in a system-default language or alternatively to not return the element</li> 
        ///     <li>indicate available alternative languages for the element's maintainable artefact through links to underlying localised resources</li> 
        /// </ul> 
        /// <p><strong>
        ///     It is recommended to indicate all languages used anywhere in the message for localised elements through 
        ///     http Content-Language response header (languages of the intended audience) and/or through a “contentLanguages” property in the meta tag. 
        ///     The main language used can be indicated through the “lang” property in the meta tag.
        /// </strong></p> 
        /// </summary>
        /// <param name=" jsonGenerator"></param>
        /// <param name=" list"></param>
        /// <param name=" fieldName"></param> 
        private static void WriteLocalisedBestMatchText(JsonGenerator jsonGenerator, IList<ITextTypeWrapper> list, string fieldName)
        {
            ITextTypeWrapper localisedBestMatch = TextTypeUtil.GetDefaultLocale(list);
            if (localisedBestMatch != null)
            {
                SdmxJsonStaticHelper.WriteNonNullString(jsonGenerator, fieldName, localisedBestMatch.Value);
            }
        }

        /// <summary> 
        /// Only writes the value to the field if the value is not null 
        /// </summary>
        /// <param name=" jsonGenerator"></param>
        /// <param name=" fieldName"></param>
        /// <param name=" value"></param> 
        public static void WriteNonNullString(JsonGenerator jsonGenerator, string fieldName, string value)
        {
            if (!ObjectUtil.ValidString(value))
            {
                // Do nothing;
                return;
            }
            jsonGenerator.WriteStringField(fieldName, value);
        }

        public static void WriteRepresentation(JsonGenerator jsonGenerator, IRepresentation representation, bool isV2, string fieldName)
        {
            if (representation == null)
            {
                return;
            }
            jsonGenerator.WriteObjectFieldStart(fieldName);
            string textFormatNode = isV2 ? representation.Representation != null ? "enumerationFormat" : "format" : "textFormat";
            SdmxJsonStaticHelper.WriteTexFormat(jsonGenerator, representation.TextFormat, textFormatNode);
            if (representation.Representation != null)
            {
                jsonGenerator.WriteStringField("enumeration", representation.Representation.MaintainableUrn.AbsoluteUri);
            }
            if (isV2)
            {
                //NOTE SDMXSOURCE_SDMXCORE CHANGE
                if (representation.MinOccurs.HasValue)
                {
                    jsonGenerator.WriteNumberField("minOccurs", representation.MinOccurs.Value);
                }
                if (representation.MaxOccurs != null && !representation.MaxOccurs.Equals(FiniteOccurenceObjectCore.DefaultValue))
                {
                    if (representation.MaxOccurs.IsUnbounded)
                    {
                        jsonGenerator.WriteStringField("maxOccurs", representation.MaxOccurs.ToString());
                    }
                    else
                    {
                        jsonGenerator.WriteNumberField("maxOccurs", representation.MaxOccurs.Occurrences);
                    }
                }
            }
            jsonGenerator.WriteEndObject();
        }

        public static void WriteTexFormat(JsonGenerator jsonGenerator, ITextFormat tf, string field)
        {
            if (tf != null && (tf.TextType != null || tf.HasRestrictions()))
            {
                if (!ObjectUtil.ValidString(field))
                {
                    field = "textFormat";
                }
                jsonGenerator.WriteObjectFieldStart(field);
                if (tf.Decimals.HasValue)
                {
                    jsonGenerator.WriteNumberField("decimals", tf.Decimals.Value);
                }
                if (tf.EndTime != null)
                {
                    jsonGenerator.WriteStringField("endTime", tf.EndTime.DateInSdmxFormat);
                }
                if (tf.EndValue.HasValue)
                {
                    jsonGenerator.WriteNumberField("endValue", tf.EndValue.Value);
                }
                if (tf.Interval.HasValue)
                {
                    jsonGenerator.WriteNumberField("interval", tf.Interval.Value);
                }
                if (tf.Multilingual != null && tf.Multilingual.IsSet())
                {
                    jsonGenerator.WriteBooleanField("isMultiLingual", tf.Multilingual.IsTrue);
                }
                if (tf.Sequence != null && tf.Sequence.IsSet())
                {
                    jsonGenerator.WriteBooleanField("isSequence", tf.Sequence.IsTrue);
                }
                if (tf.MaxLength.HasValue)
                {
                    jsonGenerator.WriteNumberField("maxLength", tf.MaxLength.Value);
                }
                if (tf.MaxValue.HasValue)
                {
                    jsonGenerator.WriteNumberField("maxValue", tf.MaxValue.Value);
                }
                if (tf.MinLength.HasValue)
                {
                    jsonGenerator.WriteNumberField("minLength", tf.MinLength.Value);
                }
                if (tf.MinValue.HasValue)
                {
                    jsonGenerator.WriteNumberField("minValue", tf.MinValue.Value);
                }
                if (tf.Pattern != null)
                {
                    jsonGenerator.WriteStringField("pattern", tf.Pattern);
                }
                if (tf.StartTime != null)
                {
                    jsonGenerator.WriteStringField("startTime", tf.StartTime.DateInSdmxFormat);
                }
                if (tf.StartValue.HasValue)
                {
                    jsonGenerator.WriteNumberField("startValue", tf.StartValue.Value);
                }

                if (tf.TextType != null)
                {
                    //NOTE SDMXSOURCE_SDMXCORE CHANGE
                    jsonGenerator.WriteStringField("textType", tf.TextType.Sdmx3);
                }
                if (tf.TimeInterval != null)
                {
                    jsonGenerator.WriteStringField("timeInterval", tf.TimeInterval);
                }

                if (tf.SentinelValues != null && tf.SentinelValues.Any())
                {
                    WriteSentinelValues(jsonGenerator, tf.SentinelValues);
                }


                jsonGenerator.WriteEndObject();  // End Text Format
            }
        }

        private static void WriteSentinelValues(JsonGenerator jsonGenerator, IList<ISentinelValue> sentinelBeans)  {
            jsonGenerator.WriteArrayFieldStart("sentinelValues");
            foreach(ISentinelValue sentinelBean in sentinelBeans){
                jsonGenerator.WriteStartObject();
                jsonGenerator.WriteStringField("value", sentinelBean.Value);
                WriteTextTypeWrapperArray(jsonGenerator, sentinelBean.Names, "names");
                WriteTextTypeWrapperArray(jsonGenerator, sentinelBean.Descriptions, "descriptions");
                jsonGenerator.WriteEndObject();
            }
            jsonGenerator.WriteEndArray();
        }

public static class RegistryJsonOrganisationWriter
        {
            public static void WriteStructure(JsonGenerator jsonGenerator, IOrganisation identifiable)
            {
                if (!ObjectUtil.ValidCollection(identifiable.Contacts))
                {
                    return;
                }
                jsonGenerator.WriteArrayFieldStart("contacts");
                foreach (IContact contact in identifiable.Contacts)
                {
                    WriteContact(jsonGenerator, contact);
                }
                jsonGenerator.WriteEndArray();
            }

            public static void WriteContact(JsonGenerator jsonGenerator, IContact contact)
            {
                jsonGenerator.WriteStartObject();

                if (contact.Id != null)
                {
                    jsonGenerator.WriteStringField("id", contact.Id);
                }
                WriteTextTypeWrapperArray(jsonGenerator, contact.Name, "names");
                WriteTextTypeWrapperArray(jsonGenerator, contact.Departments, "departments");
                WriteTextTypeWrapperArray(jsonGenerator, contact.Role, "roles");

                WriteArray(jsonGenerator, contact.Telephone, "telephones");
                WriteArray(jsonGenerator, contact.Fax, "faxes");
                WriteArray(jsonGenerator, contact.X400, "x400s");
                WriteArray(jsonGenerator, contact.Uri, "uris");
                WriteArray(jsonGenerator, contact.Email, "emails");

                jsonGenerator.WriteEndObject();
            }

            private static void WriteArray(JsonGenerator jsonGenerator, IList<string> array, string arrayName)
            {
                if (!ObjectUtil.ValidCollection(array))
                {
                    return;
                }
                jsonGenerator.WriteArrayFieldStart(arrayName);
                foreach (string currentItem in array)
                {
                    jsonGenerator.WriteString(currentItem);
                }
                jsonGenerator.WriteEndArray();
            }
        }

        public class LinkObject : ILinkObject
        {
            public LinkObject(IIdentifiableObject ident, Uri hRef, SdmxJsonSchema schema, string selfLinkType)
            {
                Urn = URN.GetInstance(ident.Urn);
                HRef = hRef;
                Uri = schema.Uri;
                Type = selfLinkType;
            }

            //@Override
            public IURN Urn { get; }

            //@Override
            public Uri HRef { get; }

            //@Override
            public Uri Uri { get; }

            //@Override
            public string Type { get; }

            //@Override
            public IList<ITextTypeWrapper> GetTitles(bool disableLocaleFilter)
            {
                return null;
            }

            //@Override
            public string GetTitle(string locale)
            {
                return null;
            }

            //@Override
            public string Title => null;

            //@Override
            public string Rel => LinkRel.GetFromEnum(LinkRelEnumType.Self)?.ToString();
        }
    }

}