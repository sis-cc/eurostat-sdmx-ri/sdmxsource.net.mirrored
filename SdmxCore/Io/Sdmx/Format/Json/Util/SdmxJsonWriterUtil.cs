// -----------------------------------------------------------------------
// <copyright file="SdmxJsonWriterUtil.cs" company="EUROSTAT">
//   Date Created : 2023-05-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Json.Util
{
    using Org.Sdmxsource.Json;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.Util.Date;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using System;
    using Io.Sdmx.Utils.Json;
    using System.Collections.Generic;
    using System.Linq;

    public class SdmxJsonWriterUtil
    {
        public static void WriteMeta(JsonGenerator jWriter, IMetadataContainer container, string schema)
        {
            jWriter.WriteObjectFieldStart("meta");
            IHeader header = container.Header;
            if (header == null)
            {
                header = new HeaderImpl("FusionRegistry");
            }
            SdmxJsonStaticHelper.WriteNonNullString(jWriter, "id", header.Id);
            jWriter.WriteBooleanField("test", header.Test);
            SdmxJsonStaticHelper.WriteNonNullString(jWriter, "schema", schema);
            DateTime prepared = header.Prepared ?? new DateTime();
            SdmxJsonStaticHelper.WriteNonNullString(jWriter, "prepared", DateUtil.FormatDate(prepared) + "Z");

            JsonWriterUtil.WriteStringArray(jWriter, "contentLanguages", container.AllLocales);


            IList<ITextTypeWrapper> names = header.Name;
            SdmxJsonStaticHelper.WriteTextTypeWrapperArray(jWriter, names, "names");
            IParty sender = header.Sender;
            if (sender != null)
            {
                WritePartyObject(jWriter, sender, true);
            }

            IList<IParty> receiver = header.Receiver;
            if (receiver.Any())
            {
                jWriter.WriteArrayFieldStart("receivers");
                foreach (IParty IpartyObject in receiver)
                {
                    WritePartyObject(jWriter, IpartyObject, false);
                }
                jWriter.WriteEndArray();
            }
            jWriter.WriteEndObject();
        }


        private static void WritePartyObject(JsonGenerator jWriter, IParty sender, bool writeField)
        {
            if (writeField)
            {
                jWriter.WriteObjectFieldStart("sender");
            }
            else
            {
                jWriter.WriteStartObject();
            }
            SdmxJsonStaticHelper.WriteNonNullString(jWriter, "id", sender.Id);
            SdmxJsonStaticHelper.WriteTextTypeWrapperArray(jWriter, sender.Name, "names");
            IList<IContact> contacts = sender.Contacts;
            if (contacts.Any())
            {
                jWriter.WriteArrayFieldStart("contacts");
                foreach (IContact IcontactObject in contacts)
                {
                    SdmxJsonStaticHelper.RegistryJsonOrganisationWriter.WriteContact(jWriter, IcontactObject);
                }
                jWriter.WriteEndArray();
            }
            jWriter.WriteEndObject();
        }

    }

}