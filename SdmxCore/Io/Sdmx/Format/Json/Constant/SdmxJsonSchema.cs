// -----------------------------------------------------------------------
// <copyright file="SdmxJsonSchema.cs" company="EUROSTAT">
//   Date Created : 2023-05-10
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCore.IO.Sdmx.Format.Json.Constant
{
    using System;
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    public enum JsonSchemaEnumType
    {
        StructureV1,
        StructureV2,
        RefMetadataV2
    }

    public class SdmxJsonSchema : BaseConstantType<JsonSchemaEnumType>
    {
        public static readonly string StructureV1 =
            "https://raw.githubusercontent.com/sdmx-twg/sdmx-json/develop/structure-message/tools/schemas/1.0/sdmx-json-structure-schema.json";
        public static readonly string StructureV2 =
            "https://raw.githubusercontent.com/sdmx-twg/sdmx-json/develop/structure-message/tools/schemas/2.0.0/sdmx-json-structure-schema.json";
        public static readonly string RefMetadataV2 =
            "https://raw.githubusercontent.com/sdmx-twg/sdmx-json/master/metadata-message/tools/schemas/2.0.0/sdmx-json-metadata-schema.json";

        private static readonly Dictionary<JsonSchemaEnumType, SdmxJsonSchema> _instances =
            new Dictionary<JsonSchemaEnumType, SdmxJsonSchema>()
            {
                {
                    JsonSchemaEnumType.StructureV1,
                    new SdmxJsonSchema(JsonSchemaEnumType.StructureV1, StructureV1)
                },
                {
                    JsonSchemaEnumType.StructureV2,
                    new SdmxJsonSchema(JsonSchemaEnumType.StructureV2, StructureV2)
                },
                {
                    JsonSchemaEnumType.RefMetadataV2,
                    new SdmxJsonSchema(JsonSchemaEnumType.RefMetadataV2, RefMetadataV2)
                }
            };

        private Uri _uri;

        private SdmxJsonSchema(JsonSchemaEnumType enumType, string location)
            : base(enumType)
        {
            try
            {
                _uri = new Uri(location);
            }
            catch (UriFormatException e)
            {
                Console.Error.WriteLine(e.StackTrace);
            }
        }

        /// <summary>
        ///     Gets the instance of <see cref="SdmxJsonSchema" /> mapped to <paramref name="enumType" />
        /// </summary>
        /// <param name="enumType">
        ///     The <c>enum</c> type
        /// </param>
        /// <returns>
        ///     the instance of <see cref="SdmxJsonSchema" /> mapped to <paramref name="enumType" />
        /// </returns>
        public static SdmxJsonSchema GetFromEnum(JsonSchemaEnumType enumType)
        {
            if (_instances.TryGetValue(enumType, out SdmxJsonSchema output))
            {
                return output;
            }

            return null;
        }

        public Uri Uri => _uri;
    }
}