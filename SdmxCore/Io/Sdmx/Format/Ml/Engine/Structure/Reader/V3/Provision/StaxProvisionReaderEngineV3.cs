// -----------------------------------------------------------------------
// <copyright file="StaxProvisionReaderEngineV3.cs" company="EUROSTAT">
//   DateTime Created : 2023-06-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Provision
{
    using System;
    using System.Collections.Generic;
    using Io.Sdmx.Api.Singleton;
    using Io.Sdmx.Api.Xml;
    using Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Util;
    using Io.Sdmx.Format.Ml.Util;
    using Io.Sdmx.Utils.Core.Application;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
    using SdmxCore.IO.Sdmx.Format.Ml.Api.Engine;

    public class StaxProvisionReaderEngineV3 : AbstractStaxMaintainableReaderEngine<IProvisionAgreementObject>, IFusionSingleton
    {
        private static StaxProvisionReaderEngineV3 _instance;

        public StaxProvisionReaderEngineV3() { }

        public static StaxProvisionReaderEngineV3 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StaxProvisionReaderEngineV3();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }

        public void DestroyInstance()
        {
            _instance = null;
        }

        public override ISet<IProvisionAgreementObject> ProcessSDMX(IStaxReader reader)
        {
            ISet<IProvisionAgreementObject> retrunSet = new HashSet<IProvisionAgreementObject>();

            string containerElement = reader.ElementName;
            while (reader.MoveNextNode())
            {
                string nodeName = reader.ElementName;
                if (reader.IsStartElement)
                {
                    if (nodeName.Equals("ProvisionAgreement"))
                    {
                        IProvisionAgreementObject IaObject = ProcessProvisionAgreement(reader);
                        if (!retrunSet.Add(IaObject))
                        {
                            throw new SdmxSemmanticException("Duplicate IProvisionAgreementObject found in message. AgencyId=" + IaObject.AgencyId + " Id=" + IaObject.Id + " Version=" + IaObject.Version);
                        }
                    }
                    else
                    {
                        StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                    }
                }
                else if (nodeName.Equals(containerElement))
                {
                    break;
                }
            }
            return retrunSet;
        }

        private IProvisionAgreementObject ProcessProvisionAgreement(IStaxReader reader)
        {
            IProvisionAgreementMutableObject provisonAgreement = new ProvisionAgreementMutableCore();
            StaxMaintainableUtilV3.ProcessObject(provisonAgreement, reader);
            while (reader.IsStartElement)
            {
                String elementName = reader.ElementName;
                if (elementName.Equals("Dataflow"))
                {
                    provisonAgreement.StructureUsage = StaxStructureRefReaderV3.GetStructureReference(reader, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow));
                }
                else if (elementName.Equals("DataProvider"))
                {
                    provisonAgreement.DataproviderRef = StaxStructureRefReaderV3.GetStructureReference(reader, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataProvider));
                }
                else
                {
                    StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                }
                reader.MoveNextNode();
            }
            return provisonAgreement.ImmutableInstance;
        }
    }

}
