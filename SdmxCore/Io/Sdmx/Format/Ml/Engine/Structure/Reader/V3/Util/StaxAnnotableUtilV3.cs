// -----------------------------------------------------------------------
// <copyright file="StaxAnnotableUtilV3.cs" company="EUROSTAT">
//   DateTime Created : 2023-06-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Util
{
    using System;
    using Io.Sdmx.Api.Xml;
    using Io.Sdmx.Format.Ml.Util;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    public class StaxAnnotableUtilV3
    {
        public static void ProcessObject(IAnnotableMutableObject sdmxObject, IStaxReader reader)
        {
            if (reader.MoveNextStartNode())
            {
                if (reader.ElementName.Equals("Annotations"))
                {
                    ProcessAnnotations(sdmxObject, reader);
                    reader.MoveNextNode(); //Move to the start/end of the next node
                }
            }
        }

        /// <summary> 
        /// if the IStaxReader points at an Annotations start node, it will then call 
        /// this method for processing. 
        /// 
        /// </summary>
        /// <param name=" IannotableObject"> 
        /// </param>
        /// <param name=" staxReader"> 
        /// </param> 
        public static void ProcessAnnotations(IAnnotableMutableObject IannotableObject, IStaxReader staxReader)
        {
            while (staxReader.MoveNextNode())
            {
                string nodeName = staxReader.ElementName;
                if (staxReader.IsStartElement)
                {
                    if (!nodeName.Equals("Annotation"))
                    {
                        StaxStructureUtil.ThrowUnexpectedNodeException(staxReader);
                    }
                    IAnnotationMutableObject currentAnnotation = new AnnotationMutableCore();
                    ProcessAnnotation(currentAnnotation, staxReader);
                    IannotableObject.AddAnnotation(currentAnnotation);
                }
                else if (nodeName.Equals("Annotations"))
                {
                    return;
                }
            }
        }

        /// <summary> 
        /// Process an annotation, the stax reader is expecting to be on the node Annoation 
        /// </summary>
        /// <param name=" annotation"> 
        /// </param>
        /// <param name=" staxReader"> 
        /// </param> 
        private static void ProcessAnnotation(IAnnotationMutableObject annotation, IStaxReader staxReader)
        {
            annotation.Id = staxReader.GetAttributeValue(null, "id");
            while (staxReader.MoveNextNode())
            {
                string nodeName = staxReader.ElementName;
                if (staxReader.IsStartElement)
                {
                    if (nodeName.Equals("AnnotationTitle"))
                    {
                        annotation.Title = staxReader.GetElementText();
                    }
                    else if (nodeName.Equals("AnnotationType"))
                    {
                        annotation.Type = staxReader.GetElementText();
                    }
                    else if (nodeName.Equals("AnnotationURL"))
                    {
                        annotation.Uri = new Uri(staxReader.GetElementText());
                    }
                    else if (nodeName.Equals("AnnotationText"))
                    {
                        annotation.AddText(StaxStructureUtil.GetText(staxReader));
                    }
                    else
                    {
                        StaxStructureUtil.ThrowUnexpectedNodeException(staxReader);
                    }
                }
                else if (nodeName.Equals("Annotation"))
                {
                    return;
                }
            }
        }
    }
}