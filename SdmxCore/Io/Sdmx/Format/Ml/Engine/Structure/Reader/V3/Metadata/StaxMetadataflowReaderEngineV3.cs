// -----------------------------------------------------------------------
// <copyright file="StaxMetadataflowReaderEngineV3.cs" company="EUROSTAT">
//   DateTime Created : 2024-04-04
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Metadata
{
    using System.Collections.Generic;
    using Io.Sdmx.Api.Singleton;
    using Io.Sdmx.Api.Xml;
    using Io.Sdmx.Format.Ml.Api.Engine;
    using Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Util;
    using Io.Sdmx.Format.Ml.Util;
    using Io.Sdmx.Utils.Core.Application;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    public class StaxMetadataflowReaderEngineV3 : IStaxMaintainableReaderEngine<IMetadataFlow>, IFusionSingleton
    {
        private static StaxMetadataflowReaderEngineV3 _instance;

        //Private constructor - lazy load instance
        private StaxMetadataflowReaderEngineV3() { }

        public static StaxMetadataflowReaderEngineV3 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StaxMetadataflowReaderEngineV3();
                    IFusionObjectStore.RegisterInstance(Instance);
                }
                return _instance;
            }
        }

        public void DestroyInstance()
        {
            _instance = null;
        }
    
        public ISdmxObjects ObtainObjectsFromSDMX(IStaxReader reader)
        {
            ISdmxObjects objects = new SdmxObjectsImpl();
            objects.AddIdentifiables(ProcessSDMX(reader));
            return objects;
        }

        public ISet<IMetadataFlow> ProcessSDMX(IStaxReader reader)
        {
            ISet<IMetadataFlow> returnSet = new HashSet<IMetadataFlow>();

            string containerElement = reader.ElementName;
            while (reader.MoveNextNode())
            {
                string nodeName = reader.ElementName;
                if (reader.IsStartElement)
                {
                    if (nodeName.Equals("Metadataflow"))
                    {
                        IMetadataFlow IaObject = ProcessMetadataflow(reader);
                        if (!returnSet.Add(IaObject))
                        {
                            throw new SdmxSemmanticException("Duplicate IMetadataFlow found in message. AgencyId=" + IaObject.AgencyId + " Id=" + IaObject.Id + " Version=" + IaObject.Version);
                        }
                    }
                    else
                    {
                        StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                    }
                }
                else if (nodeName.Equals(containerElement))
                {
                    break;
                }
            }
            return returnSet;
        }

        private IMetadataFlow ProcessMetadataflow(IStaxReader reader)
        {
            IMetadataFlowMutableObject metadataflow = new MetadataflowMutableCore();

            StaxMaintainableUtilV3.ProcessObject(metadataflow, reader);

            while (reader.IsStartElement)
            {
                string nodeName = reader.ElementName;
                if (nodeName.Equals("Structure"))
                {
                    metadataflow.MetadataStructureRef = StaxStructureRefReaderV3.GetStructureReference(reader, SdmxStructureEnumType.Msd);
                }
                else if (nodeName.Equals("Target"))
                {
                    metadataflow.AddTarget(StaxStructureRefReaderV3.GetStructureReference(reader));
                }
                else
                {
                    StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                }
                reader.MoveNextNode();
            }
            return metadataflow.ImmutableInstance;
        }
    }

}