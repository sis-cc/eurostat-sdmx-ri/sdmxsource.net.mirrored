// -----------------------------------------------------------------------
// <copyright file="StaxNameableUtilV3.cs" company="EUROSTAT">
//   DateTime Created : 2023-06-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Util
{
    using Io.Sdmx.Api.Xml;
    using Io.Sdmx.Format.Ml.Util;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;

    public class StaxNameableUtilV3
    {
        /// <summary> 
        /// Process sdmxObject either leave at the start of an unknown node, or the end of the object 
        /// </summary>
        /// <param name=" sdmxObject"> 
        /// </param>
        /// <param name=" reader"> 
        /// </param> 
        public static void ProcessObject(INameableMutableObject sdmxObject, IStaxReader reader)
        {
            StaxIdentifiableUtilV3.ProcessObject(sdmxObject, reader);
            while (ProcessNode(sdmxObject, reader))
            {
                //Processing Nodes
                reader.MoveNextNode();
            }
        }

        public static bool ProcessNode(INameableMutableObject InameableMutableObject, IStaxReader staxReader)
        {
            //		if(!staxReader.isStartElement()) {
            //			//If it is an end element, we are expecting that to be end annotations, otherwise it should be
            //			//the start of a name or description
            //			staxReader.moveNextNode();
            //		}
            if (staxReader.IsStartElement)
            {
                string nodeNameEnum = staxReader.ElementName;
                if (nodeNameEnum.Equals("Name"))
                {
                    var nameWrapper = StaxStructureUtil.GetText(staxReader);
                    InameableMutableObject.AddName(nameWrapper.Locale, nameWrapper.Value);
                    return true;
                }
                else if (nodeNameEnum.Equals("Description"))
                {
                    var descriptionWrapper = StaxStructureUtil.GetText(staxReader);
                    InameableMutableObject.AddDescription(descriptionWrapper.Locale, descriptionWrapper.Value);
                    return true;
                }
                //If it is not a name or description then it is an unknown node, so we leave the method for
                //something else to process it
            }
            return false;
        }
    }
}