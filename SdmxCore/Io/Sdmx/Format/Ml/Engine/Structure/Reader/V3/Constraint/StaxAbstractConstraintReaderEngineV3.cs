// -----------------------------------------------------------------------
// <copyright file="StaxAbstractConstraintReaderEngineV3.cs" company="EUROSTAT">
//   Date Created : 2023-06-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Constraint
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Io.Sdmx.Api.Xml;
    using Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Util;
    using Io.Sdmx.Format.Ml.Util;
    using Io.Sdmx.Utils.Core.Object;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
    using Org.Sdmxsource.Util;

    public abstract class StaxAbstractConstraintReaderEngineV3
    {
        protected void ProcessDataKeySet(IStaxReader reader, IConstraintMutableObject constraint)
        {
            IConstraintDataKeySetMutableObject includedKeys = new ConstraintDataKeySetMutableCore();
            IConstraintDataKeySetMutableObject excludedKeys = new ConstraintDataKeySetMutableCore();
            ProcessConstraintKey(reader, includedKeys, excludedKeys);
            if (ObjectUtil.ValidCollection(includedKeys.ConstrainedDataKeys))
            {
                constraint.IncludedSeriesKeys = includedKeys;
            }
            if (ObjectUtil.ValidCollection(excludedKeys.ConstrainedDataKeys))
            {
                constraint.ExcludedSeriesKeys = excludedKeys;
            }
        }

        protected void ProcessMetadataKeySet(IStaxReader reader, IConstraintMutableObject constraint)
        {
            IConstraintDataKeySetMutableObject includedKeys = new ConstraintDataKeySetMutableCore();
            IConstraintDataKeySetMutableObject excludedKeys = new ConstraintDataKeySetMutableCore();
            ProcessConstraintKey(reader, includedKeys, excludedKeys);
            if (ObjectUtil.ValidCollection(includedKeys.ConstrainedDataKeys))
            {
                constraint.IncludedMetadataKeys = includedKeys;
            }
            if (ObjectUtil.ValidCollection(excludedKeys.ConstrainedDataKeys))
            {
                constraint.ExcludedMetadataKeys = excludedKeys;
            }
        }

        private void ProcessConstraintKey(IStaxReader reader,
                                    IConstraintDataKeySetMutableObject includedKeys,
                                    IConstraintDataKeySetMutableObject excludedKeys)
        {

            bool isIncluded = StaxStructureUtil.GetAttributeAsBoolean(reader.GetAttributes(), "isIncluded", true, true);
            string exitElement = reader.ElementName;
            while (reader.MoveNextNode())
            {
                string nodeName = reader.ElementName;
                if (reader.IsStartElement)
                {
                    StaxStructureUtil.CheckStartNode(reader, "Key");
                    IDictionary<string, string> attributes = reader.GetAttributes();                   
                    attributes.TryGetValue("validFrom", out string validFrom);
                    attributes.TryGetValue("validTo", out string validTo);

                    bool? isStillIncluded;
                    string asString = StaxStructureUtil.GetAttribute(attributes, "include", false);
                    if (asString == null)
                    {
                        isStillIncluded = null;
                    }
                    else
                    {
                        isStillIncluded = bool.TryParse(asString, out bool result);
                    }

                    IConstrainedDataKeyMutableObject key = ProcessConstraintKeyValues(reader);
                    key.ValidFrom = validFrom;
                    key.ValidTo = validTo; 
                    if (isIncluded)
                    {
                        if (isStillIncluded.HasValue && isStillIncluded.Value == false)
                        {
                            excludedKeys.AddConstrainedDataKey(key);
                        }
                        else
                        {
                            includedKeys.AddConstrainedDataKey(key);
                        }
                    }
                    else
                    {
                        if (isStillIncluded.HasValue && isStillIncluded.Value == false)
                        {
                            includedKeys.AddConstrainedDataKey(key);
                        }
                        else
                        {
                            excludedKeys.AddConstrainedDataKey(key);
                        }
                    }
                }
                else if (nodeName.Equals(exitElement))
                {
                    break;
                }
            }
        }

        private IConstrainedDataKeyMutableObject ProcessConstraintKeyValues(IStaxReader reader)
        {
            IConstrainedDataKeyMutableObject key = new ConstrainedDataKeyMutableCore();

            List<IConstrainedKeyValue> keyValues = new List<IConstrainedKeyValue>();
            List<IConstrainedKeyValue> componentValues = new List<IConstrainedKeyValue>();
            string exitElement = reader.ElementName;
            while (reader.MoveNextNode())
            {
                string nodeName = reader.ElementName;
                if (reader.IsStartElement)
                {
                    List<IConstrainedKeyValue> list;
                    if (nodeName.Equals("KeyValue"))
                    {
                        list = keyValues;
                    }
                    else if (nodeName.Equals("Component"))
                    {
                        list = componentValues;
                    }
                    else
                    {
                        throw new SdmxSemmanticException("Unexpected XML element : " + reader.GetXMLPath() + ". Expected XML element: KeyValue or Component");
                    }
                    IDictionary<string, string> attributes = reader.GetAttributes();
                    bool removePrefix = StaxStructureUtil.GetAttributeAsBoolean(attributes, "removePrefix", false, false);
                    string id = StaxStructureUtil.GetAttribute(attributes, "id", true);
                    id = StringUtil.CleanString(id);
                    reader.MoveNextNode();
                    StaxStructureUtil.CheckStartNode(reader, "Value");
                    string value = reader.GetElementText();
                    value = StringUtil.CleanString(value);
                    list.Add(ConstrainedKeyValue.GetInstance(id, removePrefix, value));
                    reader.SkipToEndNode(nodeName);
                }
                else if (nodeName.Equals(exitElement))
                {
                    break;
                }
            }
            keyValues.ForEach(k => key.AddKeyValue(k));
            key.ComponentValues = componentValues;
          
            return key;
        }

        private IDataSourceMutableObject ProcessQueryableDataSourceType(IStaxReader reader)
        {
            IDataSourceMutableObject dataSourceMutableBean = new DataSourceMutableCore();

            IDictionary<string, string> attributes = reader.GetAttributes(); ;
            dataSourceMutableBean.RESTDatasource = StaxStructureUtil.GetAttributeAsBoolean(attributes, "isRESTDatasource", false, false); 
            dataSourceMutableBean.WebServiceDatasource = StaxStructureUtil.GetAttributeAsBoolean(attributes, "isWebServiceDatasource", false, true);

            string exitElement = reader.ElementName;
            while (reader.MoveNextNode())
            {
                string nodeName = reader.ElementName;
                if (reader.IsStartElement)
                {
                    if (nodeName.Equals("DataURL"))
                    {
                        dataSourceMutableBean.DataUrl = new Uri(reader.GetElementText());
                    }
                    else if (nodeName.Equals("WSDLUrl"))
                    {
                        dataSourceMutableBean.WADLUrl = new  Uri(reader.GetElementText());
                    }
                    else
                    {
                        throw new SdmxSemmanticException("Unexpected XML element : " + reader.GetXMLPath() + ". Expected XML element: DataURL or WSDLUrl");
                    }
                   }
                else if (nodeName.Equals(exitElement))
                {
                    break;
                }
            }
            return dataSourceMutableBean;
        }

        protected IConstraintAttachmentMutableObject ProcessConstraintAttachment(IStaxReader reader)
        {
            IConstraintAttachmentMutableObject constraintAttach = new ContentConstraintAttachmentMutableCore();
            while (reader.MoveNextNode())
            {
                string elementName = reader.ElementName;
                if (reader.IsStartElement)
                {
                    if (elementName.Equals("DataProvider"))
                    {
                        constraintAttach.AddStructureReference(StaxStructureRefReaderV3.GetStructureReference(reader, SdmxStructureEnumType.DataProvider));
                    }
                    else if (elementName.Equals("MetadataSet"))
                    {
                        //TODO What about the 3.0 model - do we just drop support for this in version 2.1?
                        //constraintAttach.setDataOrMetadataSetReference(processDataAndMetadataSetReference(reader, false));
                    }
                    else if (elementName.Equals("SimpleDataSource"))
                    {
                        constraintAttach.AddDataSources(reader.GetElementText());
                    }
                    else if (elementName.Equals("QueryableDataSource"))
                    {
                        constraintAttach.AddDataSources(ProcessQueryableDataSourceType(reader));
                    }
                    else if (elementName.Equals("DataStructure"))
                    {
                        constraintAttach.AddStructureReference(StaxStructureRefReaderV3.GetStructureReference(reader, SdmxStructureEnumType.Dsd));
                    }
                    else if (elementName.Equals("MetadataStructure"))
                    {
                        constraintAttach.AddStructureReference(StaxStructureRefReaderV3.GetStructureReference(reader, SdmxStructureEnumType.Msd));
                    }
                    else if (elementName.Equals("Dataflow"))
                    {
                        constraintAttach.AddStructureReference(StaxStructureRefReaderV3.GetStructureReference(reader, SdmxStructureEnumType.Dataflow));
                    }
                    else if (elementName.Equals("Metadataflow"))
                    {
                        constraintAttach.AddStructureReference(StaxStructureRefReaderV3.GetStructureReference(reader, SdmxStructureEnumType.MetadataFlow));
                    }
                    else if (elementName.Equals("ProvisionAgreement"))
                    {
                        constraintAttach.AddStructureReference(StaxStructureRefReaderV3.GetStructureReference(reader, SdmxStructureEnumType.ProvisionAgreement));
                    }
                    else
                    {
                        StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                    }
                }
                else if (elementName.Equals("ConstraintAttachment"))
                {
                    break;
                }
            }
            return constraintAttach;
        }
    }
}
