// -----------------------------------------------------------------------
// <copyright file="StaxHierarchyAssociationReaderEngineV3.cs" company="EUROSTAT">
//   DateTime Created : 2024-03-06
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using Io.Sdmx.Api.Singleton;
using Io.Sdmx.Api.Xml;
using Io.Sdmx.Format.Ml.Api.Engine;
using Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Util;
using Io.Sdmx.Format.Ml.Util;
using Io.Sdmx.Utils.Core.Application;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.Util.Objects;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using SdmxCore.IO.Sdmx.Format.Ml.Api.Engine;

namespace SdmxCore.Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Hcl
{
    public class StaxHierarchyAssociationReaderEngineV3 : AbstractStaxMaintainableReaderEngine<IHierarchyAssociation>, IFusionSingleton
    {
        private static StaxHierarchyAssociationReaderEngineV3 _instance;

        // Private constructor - lazy load instance
        private StaxHierarchyAssociationReaderEngineV3() { }

        public static StaxHierarchyAssociationReaderEngineV3 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StaxHierarchyAssociationReaderEngineV3();
                    IFusionObjectStore.RegisterInstance(_instance);
                }

                return _instance;
            }
        }

        public void DestroyInstance()
        {
            _instance = null;
        }

        public override ISet<IHierarchyAssociation> ProcessSDMX(IStaxReader reader)
        {
            HashSet<IHierarchyAssociation> returnSet = new HashSet<IHierarchyAssociation>();

            string containerElement = reader.ElementName;
            while (reader.MoveNextNode())
            {
                string nodeName = reader.ElementName;
                if (reader.IsStartElement)
                {
                    if (nodeName.Equals("HierarchyAssociation"))
                    {
                        var hierarchy = ProcessHierarchyAssociation(reader);
                        if (!returnSet.Add(hierarchy))
                        {
                            throw new SdmxSemmanticException("Duplicate Hierarchy found in message. AgencyId=" + hierarchy.AgencyId + " Id=" + hierarchy.Id + " Version=" + hierarchy.Version);
                        }
                    }
                    else
                    {
                        StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                    }
                }
                else if (nodeName.Equals(containerElement))
                {
                    break;
                }
            }
            return returnSet;
        }

        /**
         * Start node is IncludedCodelist
         * @param reader
         * @return
         */
        private IHierarchyAssociation ProcessHierarchyAssociation(IStaxReader reader)
        {
            var returnBean = new HierarchyAssociationMutableCore();
            StaxMaintainableUtilV3.ProcessObject(returnBean, reader);

            while (reader.IsStartElement)
            {
                string nodeName = reader.ElementName;
                if (nodeName.Equals("LinkedHierarchy"))
                {
                    var urn = StaxStructureRefReaderV3.GetLocalReference(reader);
                    var components = UrnUtil.GetUrnComponents(urn);
                    var version = UrnUtil.GetVersionFromUrn(urn);
                    var structureRef = new StructureReferenceImpl(components[0], components[1], version, SdmxStructureEnumType.HierarchyV30);

                    returnBean.SetHierarchyRef(structureRef);
                }
                else if (nodeName.Equals("LinkedObject"))
                {
                    returnBean.SetLinkedStructure(StaxStructureRefReaderV3.GetStructureReference(reader, null));
                }
                else if (nodeName.Equals("ContextObject"))
                {
                    returnBean.SetContextStructure(StaxStructureRefReaderV3.GetStructureReference(reader, null));
                }
                else
                {
                    StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                }
                reader.MoveNextNode();
            }
            return returnBean.ImmutableInstance;
        }
    }

}
