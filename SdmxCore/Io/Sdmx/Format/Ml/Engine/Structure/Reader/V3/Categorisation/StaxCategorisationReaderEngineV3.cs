// -----------------------------------------------------------------------
// <copyright file="StaxCategorisationReaderEngineV3.cs" company="EUROSTAT">
//   DateTime Created : 2023-06-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Categorisation
{
    using Io.Sdmx.Api.Singleton;
    using Io.Sdmx.Api.Xml;
    using Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Util;
    using Io.Sdmx.Format.Ml.Util;
    using Io.Sdmx.Utils.Core.Application;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.CategoryScheme;
    using System.Collections.Generic;
    using SdmxCore.IO.Sdmx.Format.Ml.Api.Engine;

    public class StaxCategorisationReaderEngineV3 : AbstractStaxMaintainableReaderEngine<ICategorisationObject>, IFusionSingleton
    {
        private static StaxCategorisationReaderEngineV3 _instance;

        //Private constructor - lazy load instance
        private StaxCategorisationReaderEngineV3() { }

        public static StaxCategorisationReaderEngineV3 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StaxCategorisationReaderEngineV3();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }

        public void DestroyInstance()
        {
            _instance = null;
        }

        public override ISet<ICategorisationObject> ProcessSDMX(IStaxReader reader)
        {
            ISet<ICategorisationObject> returnSet = new HashSet<ICategorisationObject>();
            string containerElement = reader.ElementName;
            while (reader.MoveNextNode())
            {
                string nodeName = reader.ElementName;
                if (reader.IsStartElement)
                {
                    if (nodeName.Equals("Categorisation"))
                    {
                        ICategorisationObject IaObject = ProcessCategorisationObject(reader);
                        if (!returnSet.Add(IaObject))
                        {
                            throw new SdmxSemmanticException("Duplicate ICategorisationObject found in message. AgencyId=" + IaObject.AgencyId + " Id=" + IaObject.Id + " Version=" + IaObject.Version);
                        }
                    }
                    else
                    {
                        StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                    }
                }
                else if (nodeName.Equals(containerElement))
                {
                    break;
                }
            }
            return returnSet;
        }

        private ICategorisationObject ProcessCategorisationObject(IStaxReader reader)
        {
            ICategorisationMutableObject sdmxObject = new CategorisationMutableCore();
            StaxMaintainableUtilV3.ProcessObject(sdmxObject, reader);

            while (reader.IsStartElement)
            {
                if (reader.ElementName.Equals("Source"))
                {
                    sdmxObject.StructureReference = StaxStructureRefReaderV3.GetStructureReference(reader, null);
                }
                else if (reader.ElementName.Equals("Target"))
                {
                    sdmxObject.CategoryReference = StaxStructureRefReaderV3.GetStructureReference(reader, SdmxStructureEnumType.Category);
                }
                else
                {
                    StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                }
                reader.MoveNextNode();
            }
            return sdmxObject.ImmutableInstance;
        }
    }
}