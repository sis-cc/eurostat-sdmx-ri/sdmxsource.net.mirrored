// -----------------------------------------------------------------------
// <copyright file="StaxAbstractOrgansiationReaderV3.cs" company="EUROSTAT">
//   DateTime Created : 2024-01-31
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using Io.Sdmx.Api.Singleton;
using Io.Sdmx.Api.Xml;
using Io.Sdmx.Format.Ml.Api.Engine;
using Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.OrgScheme;
using Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Util;
using Io.Sdmx.Format.Ml.Util;
using Io.Sdmx.Utils.Core.Application;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;

namespace SdmxCore.Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.OrgScheme
{
    public class StaxDataConsumerSchemeReaderEngineV3 : StaxAbstractOrgansiationReaderV3, IStaxMaintainableReaderEngine<IDataConsumerScheme>, IFusionSingleton
    {
        private static StaxDataConsumerSchemeReaderEngineV3 _instance;

        // Private constructor - lazy load instance
        private StaxDataConsumerSchemeReaderEngineV3() { }

        public static StaxDataConsumerSchemeReaderEngineV3 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StaxDataConsumerSchemeReaderEngineV3();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }

        public void DestroyInstance()
        {
            _instance = null;
        }

        public ISdmxObjects ObtainObjectsFromSDMX(IStaxReader reader)
        {
            ISdmxObjects objects = new SdmxObjectsImpl();
            objects.AddIdentifiables(ProcessSDMX(reader));
            return objects;
        }

        public ISet<IDataConsumerScheme> ProcessSDMX(IStaxReader reader)
        {
            var returnSet = new HashSet<IDataConsumerScheme>();
            var exitNode = reader.ElementName;
            while (reader.MoveNextNode())
            {
                var nodeName = reader.ElementName;
                if (reader.IsStartElement)
                {
                    if (nodeName.Equals("DataConsumerScheme"))
                    {
                        var dpScheme = new DataConsumerSchemeMutableCore();
                        StaxItemSchemeUtilV3.ProcessObject(dpScheme, reader);

                        // Expected position is now Agency
                        var elementName = reader.ElementName;
                        while (reader.IsStartElement)
                        {
                            if (!elementName.Equals("DataConsumer"))
                            {
                                StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                            }
                            ProcessDataConsumer(dpScheme, reader);
                            reader.MoveNextNode();
                        }
                        var dpSCh = dpScheme.ImmutableInstance;
                        returnSet.Add(dpSCh);
                    }
                    else
                    {
                        StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                    }
                }
                else if (nodeName.Equals(exitNode))
                {
                    break;
                }
            }
            return returnSet;
        }

        private void ProcessDataConsumer(DataConsumerSchemeMutableCore beanToPopulate, IStaxReader reader)
        {
            var consumer = new DataConsumerMutableCore();
            beanToPopulate.AddItem(consumer);
            base.ProcessOrganisation(consumer, reader);
        }
    }
}
