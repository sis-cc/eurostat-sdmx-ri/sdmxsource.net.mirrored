// -----------------------------------------------------------------------
// <copyright file="StaxAbstractOrgansiationReaderV3.cs" company="EUROSTAT">
//   DateTime Created : 2023-10-31
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.OrgScheme
{
    using Io.Sdmx.Api.Xml;
    using Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Util;
    using Io.Sdmx.Format.Ml.Util;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    public abstract class StaxAbstractOrgansiationReaderV3
    {
        protected virtual void ProcessOrganisation(IOrganisationMutableObject org, IStaxReader reader)
        {
            StaxNameableUtilV3.ProcessObject(org, reader);
            if (reader.IsStartElement)
            {
                ProcessStartElement(org, reader);
            }
        }

        protected virtual void ProcessStartElement(IOrganisationMutableObject org, IStaxReader reader)
        {
            while (reader.IsStartElement)
            {
                if (reader.ElementName.Equals("Contact"))
                {
                    IContactMutableObject contact = new ContactMutableObjectCore();
                    org.AddContact(contact);
                    StaxContactUtilV3.ProcessObject(contact, reader);
                }
                else
                {
                    StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                }
                reader.MoveNextNode();
            }

        }
    }
}
