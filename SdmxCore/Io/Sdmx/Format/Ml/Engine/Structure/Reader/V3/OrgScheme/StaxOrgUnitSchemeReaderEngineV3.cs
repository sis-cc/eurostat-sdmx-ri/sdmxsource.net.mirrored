// -----------------------------------------------------------------------
// <copyright file="StaxOrgUnitSchemeReaderEngineV3.cs" company="EUROSTAT">
//   DateTime Created : 2023-10-31
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.OrgScheme
{
    using System.Collections.Generic;
    using Io.Sdmx.Api.Singleton;
    using Io.Sdmx.Api.Xml;
    using Io.Sdmx.Format.Ml.Api.Engine;
    using Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Util;
    using Io.Sdmx.Format.Ml.Util;
    using Io.Sdmx.Utils.Core.Application;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    public class StaxOrgUnitSchemeReaderEngineV3 : StaxAbstractOrgansiationReaderV3, IStaxMaintainableReaderEngine<IOrganisationUnitSchemeObject>, IFusionSingleton
    {

        private static StaxOrgUnitSchemeReaderEngineV3 _instance;

        //Private constructor - lazy load instance
        private StaxOrgUnitSchemeReaderEngineV3() { }
        public static StaxOrgUnitSchemeReaderEngineV3 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StaxOrgUnitSchemeReaderEngineV3();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }

        public void DestroyInstance()
        {
            _instance = null;
        }

        public ISdmxObjects ObtainObjectsFromSDMX(IStaxReader reader)
        {
            ISdmxObjects objects = new SdmxObjectsImpl();
            objects.AddIdentifiables(ProcessSDMX(reader));
            return objects;
        }

        public ISet<IOrganisationUnitSchemeObject> ProcessSDMX(IStaxReader reader)
        {
            ISet<IOrganisationUnitSchemeObject> returnSet = new HashSet<IOrganisationUnitSchemeObject>();
            string exitNode = reader.ElementName;
            while (reader.MoveNextNode())
            {
                string nodeName = reader.ElementName;
                if (reader.IsStartElement)
                {
                    if (nodeName.Equals("OrganisationUnitScheme"))
                    {
                        IOrganisationUnitSchemeMutableObject maint = new OrganisationUnitSchemeMutableCore();
                        StaxItemSchemeUtilV3.ProcessObject(maint, reader);

                        //Expected position is now Agency
                        string elementName = reader.ElementName;
                        while (reader.IsStartElement)
                        {
                            if (!elementName.Equals("OrganisationUnit"))
                            {
                                StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                            }
                            ProcessOrgUnit(maint, reader);
                            reader.MoveNextNode();
                        }

                        IOrganisationUnitSchemeObject orgunitSch = maint.ImmutableInstance;
                        returnSet.Add(orgunitSch);
                    }
                    else
                    {
                        StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                    }
                }
                else if (nodeName.Equals(exitNode))
                {
                    break;
                }
            }
            return returnSet;
        }

        private void ProcessOrgUnit(IOrganisationUnitSchemeMutableObject beanToPopulate, IStaxReader reader)
        {
            IOrganisationUnitMutableObject org = new OrganisationUnitMutableCore();
            beanToPopulate.AddItem(org);
            ProcessOrganisation(org, reader);
        }

        protected override void ProcessOrganisation(IOrganisationMutableObject org, IStaxReader reader)
        {
            StaxNameableUtilV3.ProcessObject(org, reader);
            if (reader.IsStartElement)
            {
                ProcessStartElement(org, reader);
            }
        }

        protected override void ProcessStartElement(IOrganisationMutableObject org, IStaxReader reader)
        {
            if (reader.ElementName.Equals("Parent"))
            {
                ((IOrganisationUnitMutableObject)org).ParentUnit = StaxStructureRefReaderV3.GetLocalReference(reader);

                reader.MoveNextNode();
                if (reader.IsStartElement)
                {
                    base.ProcessStartElement(org, reader);
                }
            }
            else
            {
                base.ProcessStartElement(org, reader);
            }
        }
    }
}
