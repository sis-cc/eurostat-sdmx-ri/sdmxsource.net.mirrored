// -----------------------------------------------------------------------
// <copyright file="StaxHeaderReaderEngineV3.cs" company="EUROSTAT">
//   Date Created : 2023-06-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3
{
    using Io.Sdmx.Api.Xml;
    using Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Util;
    using Io.Sdmx.Format.Ml.Util;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Date;
    using System.Collections.Generic;
    using System;

    public class StaxHeaderReaderEngineV3
    {
        public static IHeader ReaderHeader(IStaxReader reader)
        {
            //All the attributes required to build a header
            IDictionary<string, string> additionalAttributes = new Dictionary<string, string>();
            IStructureReference dataProviderReference = null;
            IList<IDatasetStructureReference> structure = new List<IDatasetStructureReference>();
            DatasetAction datasetAction = null;
            string id = null;
            IList<ITextTypeWrapper> name = new List<ITextTypeWrapper>();
            IList<ITextTypeWrapper> source = new List<ITextTypeWrapper>();
            IList<IParty> receiver = new List<IParty>();
            IParty sender = null;
            bool test = false;
            DateTime? prepared = null;

            while (reader.MoveNextNode())
            {
                string nodeName = reader.ElementName;
                if (reader.IsStartElement)
                {
                    if (nodeName.Equals("ID"))
                    {
                        id = reader.GetElementText();
                    }
                    else if (nodeName.Equals("Test"))
                    {
                        test = bool.Parse(reader.GetElementText());
                    }
                    else if (nodeName.Equals("Prepared"))
                    {
                        prepared = DateUtil.FormatDate(reader.GetElementText(), true);
                    }
                    else if (nodeName.Equals("Sender"))
                    {
                        sender = ProcessParty(reader);
                    }
                    else if (nodeName.Equals("Receiver"))
                    {
                        receiver.Add(ProcessParty(reader));
                    }
                    else if (nodeName.Equals("Name"))
                    {
                        //NOTE SDMXSOURCE_SDMXCORE CHANGE no "property" attribute in sdmxsource
                        name.Add(new TextTypeWrapperImpl(StaxStructureUtil.GetText(reader), null));
                    }
                    else if (nodeName.Equals("Source"))
                    {
                        source.Add(new TextTypeWrapperImpl(StaxStructureUtil.GetText(reader), null));
                    }
                    else if (nodeName.Equals("Structure"))
                    {
                        string structureId = reader.GetAttributeValue("structureID");
                        IStructureReference sRef = ReadStructureURN(reader);
                        if (sRef != null)
                        {
                            structure.Add(new DatasetStructureReferenceCore(structureId, sRef, null, null, null));
                        }
                    }
                    else
                    {
                        //	StaxStructureUtil.throwUnexpectedNodeException(reader); //not useful, prevents reading of GenericMetadata
                    }
                }
                else if (nodeName.Equals("Header"))
                {
                    break;
                }
            }

            return new HeaderImpl(additionalAttributes,
                    structure,
                    dataProviderReference,
                    datasetAction,
                    id,
                    null,
                    null,
                    null,
                    prepared,
                    null,
                    null,
                    name,
                    source,
                    receiver,
                    sender,
                    test);
        }

        /// <summary> 
        /// Reads the Structure URN from the header 
        /// </summary>
        /// <param name=" reader"> 
        /// </param>
        /// <returns>
        /// </returns> 
        private static IStructureReference ReadStructureURN(IStaxReader reader)
        {
            while (reader.MoveNextNode())
            {
                if (reader.IsStartElement)
                {
                    if (reader.ElementName.Equals("URN"))
                    {
                        return new StructureReferenceImpl(reader.GetElementText());
                    }
                }
                else if (reader.ElementName.Equals("Structure"))
                {
                    break;
                }
            }
            return null;
        }

        private static IParty ProcessParty(IStaxReader reader)
        {
            string exitNode = reader.ElementName;
            bool isEmptyElement = reader.IsEmptyElement;
            string id = StaxStructureUtil.GetAttribute(reader.GetAttributes(), "id", true);
            if (isEmptyElement)
            {
                return new PartyCore(null, id, null, null);
            }

            IList<ITextTypeWrapper> name = null;
            IList<IContact> contacts = null;
            string timeZone = null;
            while (reader.MoveNextNode())
            {
                string nodeName = reader.ElementName;
                if (reader.IsStartElement)
                {
                    if (nodeName.Equals("Name"))
                    {
                        if (name == null)
                        {
                            name = new List<ITextTypeWrapper>();
                        }
                        //NOTE SDMXSOURCE_SDMXCORE CHANGE
                        name.Add(new TextTypeWrapperImpl(StaxStructureUtil.GetText(reader), null));
                    }
                    else if (nodeName.Equals("Contact"))
                    {
                        if (contacts == null)
                        {
                            contacts = new List<IContact>();
                        }
                        contacts.Add(ProcessContact(reader));
                    }
                    else if (nodeName.Equals("Timezone"))
                    {
                        timeZone = reader.GetElementText();
                    }
                    else
                    {
                        StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                    }
                }
                else if (nodeName.Equals(exitNode))
                {
                    break;
                }
            }
            return new PartyCore(name, id, contacts, timeZone);
        }

        private static IContact ProcessContact(IStaxReader reader)
        {
            IContactMutableObject contact = new ContactMutableObjectCore();
            StaxContactUtilV3.ProcessObject(contact, reader);
            return new ContactCore(contact);
        }
    }
}