// -----------------------------------------------------------------------
// <copyright file="StaxRepresentationUtilV3.cs" company="EUROSTAT">
//   DateTime Created : 2024-04-02
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using Io.Sdmx.Api.Xml;
using Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Util;
using Io.Sdmx.Format.Ml.Util;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

namespace SdmxCore.Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Util
{
    public class StaxRepresentationUtilV3
    {
        public static IRepresentationMutableObject ProcessObject(IStaxReader reader)
        {
            var bean = new RepresentationMutableCore();
            ProcessObject(reader, bean);
            return bean;
        }

        public static void ProcessObject(IStaxReader reader, IRepresentationMutableObject bean)
        {
            string exitNode = reader.ElementName;
            IDictionary<string, string> attributes = reader.GetAttributes();
            int? minOccurs = StaxStructureUtil.GetAttributeAsInteger(attributes, "minOccurs", false);
            int? maxOccurs = StaxStructureUtil.GetAttributeAsInteger(attributes, "maxOccurs", false);
            if (minOccurs != null)
            {
                bean.MinOccurs = minOccurs.Value;
            }
            if (maxOccurs != null)
            {
                // NOTE SDMXSOURCE_SDMXCORE CHANGE
                bean.MaxOccurs = new FiniteOccurenceObjectCore(maxOccurs.Value);
            }
            while (reader.MoveNextNode())
            {
                string nodeName = reader.ElementName;
                if (reader.IsStartElement)
                {
                    if (nodeName.Equals("TextFormat"))
                    {
                        bean.TextFormat = StaxTextFormatUtilV3.ProcessBean(reader);
                    }
                    else if (nodeName.Equals("Enumeration"))
                    {
                        bean.Representation = StaxStructureRefReaderV3.GetStructureReference(reader, SdmxStructureEnumType.ValueList, SdmxStructureEnumType.CodeList);
                    }
                    else if (nodeName.Equals("EnumerationFormat"))
                    {
                        bean.TextFormat = StaxTextFormatUtilV3.ProcessBean(reader);
                    }
                    else
                    {
                        StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                    }
                }
                else if (nodeName.Equals(exitNode))
                {
                    break;
                }
            }
        }

    }
}
