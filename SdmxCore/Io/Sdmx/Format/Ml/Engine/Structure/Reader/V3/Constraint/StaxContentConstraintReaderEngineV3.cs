// -----------------------------------------------------------------------
// <copyright file="StaxContentConstraintReaderEngineV3.cs" company="EUROSTAT">
//   DateTime Created : 2023-03-11
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Constraint
{
    using System;
    using System.Collections.Generic;
    using Io.Sdmx.Api.Singleton;
    using Io.Sdmx.Api.Xml;
    using Io.Sdmx.Format.Ml.Api.Engine;
    using Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Util;
    using Io.Sdmx.Format.Ml.Util;
    using Io.Sdmx.Utils.Core.Application;
    using Io.Sdmx.Utils.Core.Object;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util.Extensions;

    public class StaxContentConstraintReaderEngineV3 : StaxAbstractConstraintReaderEngineV3, IStaxMaintainableReaderEngine<IContentConstraintObject>, IFusionSingleton
    {
        private static StaxContentConstraintReaderEngineV3 _instance;

        public StaxContentConstraintReaderEngineV3() {}

        public static StaxContentConstraintReaderEngineV3 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StaxContentConstraintReaderEngineV3();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }

        public void DestroyInstance()
        {
            _instance = null;
        }

        public ISdmxObjects ObtainObjectsFromSDMX(IStaxReader reader)
        {
            throw new NotImplementedException();
        }

        public ISet<IContentConstraintObject> ProcessSDMX(IStaxReader reader)
        {
            ISet<IContentConstraintObject> returnSet = new HashSet<IContentConstraintObject>();
            string containerElement = reader.ElementName;
            while (reader.MoveNextNode())
            {
                string nodeName = reader.ElementName;
                if (reader.IsStartElement)
                {
                    if (nodeName.Equals("DataConstraint"))
                    {
                        IContentConstraintObject aBean = ProcessConstraint(reader);
                        if (!returnSet.Add(aBean))
                        {
                            throw new SdmxSemmanticException("Duplicate DataConstraintBean found in message. AgencyId=" + aBean.AgencyId + " Id=" + aBean.Id + " Version=" + aBean.Version);
                        }
                    }
                    else
                    {
                        StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                    }
                }
                else if (nodeName.Equals(containerElement))
                {
                    break;
                }

            }
            return returnSet;
        }

        private IContentConstraintObject ProcessConstraint(IStaxReader reader)
        {
            IContentConstraintMutableObject constraint = new ContentConstraintMutableCore();

            string constraintType = reader.GetAttributeValue(null, "role");
            if (constraintType != null)
            {
                constraint.IsDefiningActualDataPresent = constraintType.Equals("Actual"); 
            }

            StaxMaintainableUtilV3.ProcessObject(constraint, reader);

            ICubeRegionMutableObject includedCubeRegion = new CubeRegionMutableCore();
            ICubeRegionMutableObject excludedCubeRegion = new CubeRegionMutableCore();
            while (reader.IsStartElement)
            {
                string elementName = reader.ElementName;
                if (elementName.Equals("ConstraintAttachment"))
                {
                    constraint.ConstraintAttachment = ProcessConstraintAttachment(reader);
                }
                else if (elementName.Equals("DataKeySet"))
                {
                    ProcessDataKeySet(reader, constraint);
                }
                else if (elementName.Equals("CubeRegion"))
                {
                    ProcessCubeRegion(reader, includedCubeRegion, excludedCubeRegion);
                    constraint.IncludedCubeRegion = includedCubeRegion;
                    constraint.ExcludedCubeRegion = excludedCubeRegion;
                }
                else if (elementName.Equals("ReleaseCalendar"))
                {
                    constraint.ReleaseCalendar = ProcessReleaseCalendar(reader);
                }
                else
                {
                    StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                }
                reader.MoveNextNode();
            }
            return constraint.ImmutableInstance; 
        }

        private IReleaseCalendarMutableObject ProcessReleaseCalendar(IStaxReader reader)
        {
            IReleaseCalendarMutableObject returnBean = new ReleaseCalendarMutableCore();

            while (reader.MoveNextNode())
            {
                String elementName = reader.ElementName;
                if (reader.IsStartElement)
                {
                    if (elementName.Equals("Periodicity"))
                    {
                        returnBean.Periodicity = reader.GetElementText(); 
                    }
                    else if (elementName.Equals("Offset"))
                    {
                        returnBean.Offset = reader.GetElementText(); 
                    }
                    else if (elementName.Equals("Tolerance"))
                    {
                        returnBean.Tolerance = reader.GetElementText(); 
                    }
                    else
                    {
                        StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                    }
                }
                else if (elementName.Equals("ReleaseCalendar"))
                {
                    break;
                }
            }
            return returnBean;
        }

        private void ProcessCubeRegion(IStaxReader reader,
                                  ICubeRegionMutableObject includedCubeRegion,
                                  ICubeRegionMutableObject excludedCubeRegion)
        {

            bool isIncluded = StaxStructureUtil.GetAttributeAsBoolean(reader.GetAttributes(), "include", false, true);

            string exitElement = reader.ElementName;
            while (reader.MoveNextNode())
            {
                string nodeName = reader.ElementName;
                if (reader.IsStartElement)
                {
                    TertiaryBool isStillIncluded = StaxStructureUtil.GetAttributeAsTertiaryBoolean(reader.GetAttributes(), "include", false);
                    if (nodeName.Equals("KeyValue"))
                    {
                        IKeyValuesMutable keyValues = ProcessCubeRegionKeyValues(reader);
                        if (isIncluded)
                        {
                            if (isStillIncluded.IsSet() && !isStillIncluded.IsTrue)
                            {
                                excludedCubeRegion.AddKeyValue(keyValues);
                            }
                            else
                            {
                                includedCubeRegion.AddKeyValue(keyValues);
                            }
                        }
                        else
                        {
                            if (isStillIncluded.IsSet() && !isStillIncluded.IsTrue)
                            {
                                includedCubeRegion.AddKeyValue(keyValues);
                            }
                            else
                            {
                                excludedCubeRegion.AddKeyValue(keyValues); 
                            }
                        }
                    }
                    else if (nodeName.Equals("Component"))
                    {
                        IKeyValuesMutable attrValues = ProcessCubeRegionKeyValues(reader);
                        if (isIncluded)
                        {
                            if (isStillIncluded.IsSet() && !isStillIncluded.IsTrue)
                            {
                                excludedCubeRegion.AddAttributeValue(attrValues);
                            }
                            else
                            {
                                includedCubeRegion.AddAttributeValue(attrValues);
                            }
                        }
                        else
                        {
                            if (isStillIncluded.IsSet() && !isStillIncluded.IsTrue)
                            {
                                includedCubeRegion.AddAttributeValue(attrValues);
                            }
                            else
                            {
                                excludedCubeRegion.AddAttributeValue(attrValues);
                            }
                        }
                    }
                    else
                    {
                        StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                    }
                }
                else if (nodeName.Equals(exitElement))
                {
                    break;
                }
            }
        }

        private IKeyValuesMutable ProcessCubeRegionKeyValues(IStaxReader reader)
        {
            IKeyValuesMutable returnBean = new KeyValuesMutableImpl();
            returnBean.Id = StaxStructureUtil.GetAttribute(reader.GetAttributes(), "id", true);
            returnBean.IsRemovePrefix = StaxStructureUtil.GetAttributeAsBoolean(reader.GetAttributes(), "removePrefix", false, false);
            string exitElement = reader.ElementName;
            while (reader.MoveNextNode())
            {
                string nodeName = reader.ElementName;
                if (reader.IsStartElement)
                {
                    if (nodeName.Equals("Value"))
                    {
                        IDictionary<string, string> attributes = reader.GetAttributes();
                        string elText = reader.GetElementText();
                        elText = StringUtil.CleanString(elText);
                        string cascade = attributes.GetOrDefault("cascadeValues"); 
                        string validFrom = attributes.GetOrDefault("validFrom"); 
                        string validTo = attributes.GetOrDefault("validTo");
                        returnBean.SetValidity(elText, validFrom, validTo);
                        if (cascade == null || cascade.Equals("false"))
                        {
                            returnBean.AddValue(elText);
                        }
                        else if (cascade.Equals("excluderoot"))
                        {
                            returnBean.AddCascade(elText);
                        }
                        else if (cascade.Equals("true"))
                        {
                            returnBean.AddCascade(elText);
                            returnBean.AddValue(elText);
                        }
                        else
                        {
                            throw new SdmxSemmanticException("Unexpected cascadeValues value: " + cascade + ", expecitng 'true', 'false' or 'excluderoot'");
                        }
                    }
                    else if (nodeName.Equals("TimeRange"))
                    {
                        returnBean.TimeRange = ProcessTimeRange(reader);
                    }
                    else
                    {
                        StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                    }
                }
                else if (nodeName.Equals(exitElement))
                {
                    break;
                }
            }
            return returnBean;
        }
       
        private ITimeRangeMutableObject ProcessTimeRange(IStaxReader reader)
        {
            ITimeRangeMutableObject returnBean = new TimeRangeMutableCore();

            string exitElement = reader.ElementName;
            while (reader.MoveNextNode())
            {
                string nodeName = reader.ElementName;
                if (reader.IsStartElement)
                {
                    if (nodeName.Equals("BeforePeriod"))
                    {
                        returnBean.StartDate = new SdmxDateCore(reader.GetElementText());
                        returnBean.IsRange = false; 
                    }
                    else if (nodeName.Equals("AfterPeriod"))
                    {
                        returnBean.EndDate = new SdmxDateCore(reader.GetElementText());
                        returnBean.IsRange = false;
                    }
                    else if (nodeName.Equals("StartPeriod"))
                    {
                        bool isInclusive = StaxStructureUtil.GetAttributeAsBoolean(reader.GetAttributes(), "isInclusive", false, false);
                        returnBean.IsStartInclusive = isInclusive;
                        returnBean.StartDate = new SdmxDateCore(reader.GetElementText());
                        returnBean.IsRange = true; 
                    }
                    else if (nodeName.Equals("EndPeriod"))
                    {
                        bool isInclusive = StaxStructureUtil.GetAttributeAsBoolean(reader.GetAttributes(), "isInclusive", false, false);
                        returnBean.IsEndInclusive = isInclusive;
                        returnBean.EndDate = new SdmxDateCore(reader.GetElementText());
                        returnBean.IsRange = true; 
                    }
                    else
                    {
                        StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                    }
                }
                else if (nodeName.Equals(exitElement))
                {
                    break;
                }
            }
            return returnBean;
        }

    }
}
