// -----------------------------------------------------------------------
// <copyright file="StaxAbstractOrgansiationReaderV3.cs" company="EUROSTAT">
//   DateTime Created : 2023-10-31
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.OrgScheme
{
    using System.Collections.Generic;
    using Io.Sdmx.Api.Singleton;
    using Io.Sdmx.Api.Xml;
    using Io.Sdmx.Format.Ml.Api.Engine;
    using Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Util;
    using Io.Sdmx.Format.Ml.Util;
    using Io.Sdmx.Utils.Core.Application;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    public class StaxDataProviderSchemeReaderEngineV3 : StaxAbstractOrgansiationReaderV3, IStaxMaintainableReaderEngine<IDataProviderScheme>, IFusionSingleton
    {
        private static StaxDataProviderSchemeReaderEngineV3 _instance;

        //Private constructor - lazy load instance
        private StaxDataProviderSchemeReaderEngineV3() { }
        public static StaxDataProviderSchemeReaderEngineV3 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StaxDataProviderSchemeReaderEngineV3();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }

        public void DestroyInstance()
        {
            _instance = null;
        }

        public ISdmxObjects ObtainObjectsFromSDMX(IStaxReader reader)
        {
            ISdmxObjects objects = new SdmxObjectsImpl();
            objects.AddIdentifiables(ProcessSDMX(reader));
            return objects;
        }

        public ISet<IDataProviderScheme> ProcessSDMX(IStaxReader reader)
        {
            ISet<IDataProviderScheme> returnSet = new HashSet<IDataProviderScheme>();
            string exitNode = reader.ElementName;
            while (reader.MoveNextNode())
            {
                string nodeName = reader.ElementName;
                if (reader.IsStartElement)
                {
                    if (nodeName.Equals("DataProviderScheme"))
                    {
                        IDataProviderSchemeMutableObject dpScheme = new DataProviderSchemeMutableCore();
                        //NOTE SDMXSOURCE_SDMXCORE CHANGE: used V3 util class instead of the old V21 one
                        StaxItemSchemeUtilV3.ProcessObject(dpScheme, reader);

                        //Expected position is now Agency
                        string elementName = reader.ElementName;
                        while (reader.IsStartElement)
                        {
                            if (!elementName.Equals("DataProvider"))
                            {
                                StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                            }
                            ProcessDataProvider(dpScheme, reader);
                            reader.MoveNextNode();
                        }
                        IDataProviderScheme dpSCh = dpScheme.ImmutableInstance;
                        returnSet.Add(dpSCh);
                    }
                    else
                    {
                        StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                    }
                }
                else if (nodeName.Equals(exitNode))
                {
                    break;
                }
            }
            return returnSet;
        }

        private void ProcessDataProvider(IDataProviderSchemeMutableObject objToPopulate, IStaxReader reader)
        {
            IDataProviderMutableObject provider = new DataProviderMutableCore();
            objToPopulate.AddItem(provider);
            base.ProcessOrganisation(provider, reader);
        }
    }

}
