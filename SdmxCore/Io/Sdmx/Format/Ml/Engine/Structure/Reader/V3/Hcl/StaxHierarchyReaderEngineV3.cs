// -----------------------------------------------------------------------
// <copyright file="StaxHierarchyReaderEngineV3.cs" company="EUROSTAT">
//   DateTime Created : 2024-03-04
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Hcl
{
    using System;
    using System.Collections.Generic;
    using Io.Sdmx.Api.Singleton;
    using Io.Sdmx.Api.Xml;
    using Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Util;
    using Io.Sdmx.Format.Ml.Util;
    using Io.Sdmx.Utils.Core.Application;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
    using SdmxCore.Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Util;
    using SdmxCore.IO.Sdmx.Format.Ml.Api.Engine;

    public class StaxHierarchyReaderEngineV3 : AbstractStaxMaintainableReaderEngine<IHierarchyV30>, IFusionSingleton
    {
        private static StaxHierarchyReaderEngineV3 _instance;

        public StaxHierarchyReaderEngineV3() { }

        public static StaxHierarchyReaderEngineV3 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StaxHierarchyReaderEngineV3();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }

        public void DestroyInstance()
        {
            _instance = null;
        }

        public override ISet<IHierarchyV30> ProcessSDMX(IStaxReader reader)
        {
            var returnSet = new HashSet<IHierarchyV30>();

            var containerElement = reader.ElementName;
            while (reader.MoveNextNode())
            {
                var nodeName = reader.ElementName;
                if (reader.IsStartElement)
                {
                    if (nodeName.Equals("Hierarchy"))
                    {
                        var hierarchy = ProcessHierarchy(reader);
                        if (!returnSet.Add(hierarchy))
                        {
                            throw new SdmxSemmanticException("Duplicate Hierarchy found in message. AgencyId=" + hierarchy.AgencyId + " Id=" + hierarchy.Id + " Version=" + hierarchy.Version);
                        }
                    }
                    else
                    {
                        StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                    }
                }
                else if (nodeName.Equals(containerElement))
                {
                    break;
                }
            }
            return returnSet;
        }

        private IHierarchyV30 ProcessHierarchy(IStaxReader reader)
        {
            var returnBean = new HierarchyMutableCoreV30();
            returnBean.SetFormalLevels(StaxStructureUtil.GetAttributeAsTertiaryBoolean(reader.GetAttributes(), "hasFormalLevels", false).IsTrue);

            StaxMaintainableUtilV3.ProcessObject(returnBean, reader);

            while (reader.IsStartElement)
            {
                var nodeName = reader.ElementName;
                if (nodeName.Equals("HierarchicalCode"))
                {
                    returnBean.AddHierarchicalCodeBean(ProcessHierarchicalCode(reader));
                }
                else if (nodeName.Equals("Level"))
                {
                    returnBean.SetChildLevel(ProcessChildLevel(reader));
                }
                else
                {
                    StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                }
                reader.MoveNextNode();
            }
            return returnBean.ImmutableInstance;
        }

        private CodeRefMutableCore ProcessHierarchicalCode(IStaxReader reader)
        {
            var returnBean = new CodeRefMutableCore();
            var attributes = reader.GetAttributes();
            returnBean.ValidFrom = StaxStructureUtil.GetAttributeAsDate(attributes, "validFrom", false);
            returnBean.ValidTo = StaxStructureUtil.GetAttributeAsDate(attributes, "validTo", false);

            StaxIdentifiableUtilV3.ProcessObject(returnBean, reader);

            while (reader.IsStartElement)
            {
                var nodeName = reader.ElementName;
                if (nodeName.Equals("Code"))
                {
                    returnBean.CodeReference = StaxStructureRefReaderV3.GetStructureReference(reader, SdmxStructureEnumType.Code);
                }
                else if (nodeName.Equals("Level"))
                {
                    returnBean.LevelReference = StaxStructureRefReaderV3.GetLocalReference(reader);
                }
                else if (nodeName.Equals("HierarchicalCode"))
                {
                    returnBean.AddCodeRef(ProcessHierarchicalCode(reader));
                }
                else
                {
                    StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                }
                reader.MoveNextNode();
            }
            return returnBean;
        }

        private LevelMutableCore ProcessChildLevel(IStaxReader reader)
        {
            var returnBean = new LevelMutableCore();
            StaxNameableUtilV3.ProcessObject(returnBean, reader);

            while (reader.IsStartElement)
            {
                var nodeName = reader.ElementName;
                if (nodeName.Equals("CodingFormat"))
                {
                    returnBean.CodingFormat = StaxTextFormatUtilV3.ProcessBean(reader);
                    reader.MoveNextNode();
                }
                else if (nodeName.Equals("Level"))
                {
                    returnBean.ChildLevel = ProcessChildLevel(reader);
                }
                else
                {
                    StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                }
                reader.MoveNextNode();
            }
            return returnBean;
        }

    }
}
