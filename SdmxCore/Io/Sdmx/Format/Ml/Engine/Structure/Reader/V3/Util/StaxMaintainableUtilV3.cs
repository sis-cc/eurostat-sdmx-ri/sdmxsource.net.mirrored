// -----------------------------------------------------------------------
// <copyright file="StaxMaintainableUtilV3.cs" company="EUROSTAT">
//   DateTime Created : 2023-06-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>

// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Util
{
    using Io.Sdmx.Api.Xml;
    using Io.Sdmx.Format.Ml.Util;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using System;
    using System.Collections.Generic;

    public class StaxMaintainableUtilV3
    {

        public static void ProcessObject(IMaintainableMutableObject sdmxObject, IStaxReader reader)
        {
            ProcessAttributes(sdmxObject, reader);
            StaxNameableUtilV3.ProcessObject(sdmxObject, reader);
        }

        public static void ProcessAttributes(IMaintainableMutableObject maint, IStaxReader staxReader)
        {
            IDictionary<string, string> attributes = staxReader.GetAttributes();
            maint.StartDate = StaxStructureUtil.GetAttributeAsDate(attributes, "validFrom", false);
            maint.EndDate = StaxStructureUtil.GetAttributeAsDate(attributes, "validTo", false);
            maint.AgencyId = StaxStructureUtil.GetAttribute(attributes, "agencyID", true);
            //NOTE SDMXSOURCE_SDMXCORE CHANGE got tertiary bool
            maint.ExternalReference = StaxStructureUtil.GetAttributeAsTertiaryBoolean(attributes, "isExternalReference", false);
            string serviceUrl = StaxStructureUtil.GetAttribute(attributes, "serviceURL", false);
            maint.ServiceURL = serviceUrl != null ? new Uri(serviceUrl) : null;
            string structureUrl = StaxStructureUtil.GetAttribute(attributes, "structureURL", false);
            maint.StructureURL = structureUrl != null ? new Uri(structureUrl) : null;

            maint.FinalStructure = StaxStructureUtil.GetAttributeAsTertiaryBoolean(attributes, "isFinal", false);
            maint.Version = StaxStructureUtil.GetAttribute(attributes, "version", false);
        }
    }
}