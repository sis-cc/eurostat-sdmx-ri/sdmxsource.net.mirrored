// -----------------------------------------------------------------------
// <copyright file="StaxTextFormatUtilV3.cs" company="EUROSTAT">
//   DateTime Created : 2024-03-05
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using Io.Sdmx.Api.Xml;
using Io.Sdmx.Format.Ml.Util;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

namespace SdmxCore.Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Util
{
    public class StaxTextFormatUtilV3
    {
        public static TextFormatMutableCore ProcessBean(IStaxReader reader)
        {
            var bean = new TextFormatMutableCore();

            var attributes = reader.GetAttributes();

            var startTime = StaxStructureUtil.GetAttributeAsDate(attributes, "startTime", false);
            var endTime = StaxStructureUtil.GetAttributeAsDate(attributes, "endTime", false);

            bean.TextType = StaxStructureUtil.GetAttributeAsTextType(attributes, "textType", false);
            bean.Sequence = StaxStructureUtil.GetAttributeAsTertiaryBoolean(attributes, "isSequence", false);
            bean.Interval = StaxStructureUtil.GetAttributeAsBigDecimal(attributes, "interval", false);
            bean.StartValue = StaxStructureUtil.GetAttributeAsBigDecimal(attributes, "startValue", false);
            bean.EndValue = StaxStructureUtil.GetAttributeAsBigDecimal(attributes, "endValue", false);
            bean.TimeInterval = StaxStructureUtil.GetAttribute(attributes, "timeInterval", false);
            bean.StartTime = startTime != null ? new SdmxDateCore(startTime) : null;
            bean.EndTime = endTime != null ? new SdmxDateCore(endTime) : null;
            bean.MinLength = StaxStructureUtil.GetAttributeAsBigInteger(attributes, "minLength", false);
            bean.MaxLength = StaxStructureUtil.GetAttributeAsBigInteger(attributes, "maxLength", false);
            bean.MinValue = StaxStructureUtil.GetAttributeAsBigDecimal(attributes, "minValue", false);
            bean.MaxValue = StaxStructureUtil.GetAttributeAsBigDecimal(attributes, "maxValue", false);
            bean.Decimals = StaxStructureUtil.GetAttributeAsBigInteger(attributes, "decimals", false);
            bean.Pattern = StaxStructureUtil.GetAttribute(attributes, "pattern", false);
            bean.Multilingual = StaxStructureUtil.GetAttributeAsTertiaryBoolean(attributes, "isMultiLingual", false);

            return bean;
        }


    }
}
