// -----------------------------------------------------------------------
// <copyright file="StaxCategorySchemeReaderEngineV3.cs" company="EUROSTAT">
//   DateTime Created : 2023-10-19
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------


namespace Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.CategoryScheme
{
    using System.Collections.Generic;
    using Io.Sdmx.Api.Singleton;
    using Io.Sdmx.Api.Xml;
    using Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Util;
    using Io.Sdmx.Format.Ml.Util;
    using Io.Sdmx.Utils.Core.Application;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.CategoryScheme;
    using SdmxCore.IO.Sdmx.Format.Ml.Api.Engine;

    public class StaxCategorySchemeReaderEngineV3 : AbstractStaxMaintainableReaderEngine<ICategorySchemeObject>, IFusionSingleton
    {
        private static StaxCategorySchemeReaderEngineV3 _instance;

        //Private constructor - lazy load instance
        private StaxCategorySchemeReaderEngineV3() { }

        public static StaxCategorySchemeReaderEngineV3 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StaxCategorySchemeReaderEngineV3();
                    IFusionObjectStore.RegisterInstance(_instance);
                }

                return _instance;
            }
        }

        public void DestroyInstance()
        {
            _instance = null;
        }

        public override ISet<ICategorySchemeObject> ProcessSDMX(IStaxReader reader)
        {
            ISet<ICategorySchemeObject> returnSet = new HashSet<ICategorySchemeObject>();
            string containerElement = reader.ElementName;
            while (reader.MoveNextNode())
            {
                string nodeName = reader.ElementName;
                if (reader.IsStartElement)
                {
                    if (nodeName.Equals("CategoryScheme"))
                    {
                        ICategorySchemeObject IaObject = ProcessCategoryScheme(reader);
                        if (!returnSet.Add(IaObject))
                        {
                            throw new SdmxSemmanticException("Duplicate ICategorySchemeObject found in message. AgencyId=" + IaObject.AgencyId + " Id=" + IaObject.Id + " Version=" + IaObject.Version);
                        }
                    }
                    else
                    {
                        StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                    }
                }
                else if (nodeName.Equals(containerElement))
                {
                    break;
                }
            }
            return returnSet;
        }

        private ICategorySchemeObject ProcessCategoryScheme(IStaxReader reader)
        {
            ICategorySchemeMutableObject categoryScheme = new CategorySchemeMutableCore();
            StaxItemSchemeUtilV3.ProcessObject(categoryScheme, reader);

            while (reader.IsStartElement)
            {
                if (!reader.ElementName.Equals("Category"))
                {
                    StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                }

                categoryScheme.AddItem(ProcessCategoryObject(reader));
                //After processing the category move to the next element which is either ending this
                //category, or starting a new one
                reader.MoveNextNode();
            }
            return categoryScheme.ImmutableInstance;
        }

        private ICategoryMutableObject ProcessCategoryObject(IStaxReader reader)
        {
            ICategoryMutableObject sdmxObject = new CategoryMutableCore();
            StaxNameableUtilV3.ProcessObject(sdmxObject, reader);

            while (reader.IsStartElement)
            {
                if (!reader.ElementName.Equals("Category"))
                {
                    StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                }

                sdmxObject.AddItem(ProcessCategoryObject(reader));
                //After processing the category move to the next element which is either ending this
                //category, or starting a new one
                reader.MoveNextNode();
            }
            return sdmxObject;
        }
    }
}
