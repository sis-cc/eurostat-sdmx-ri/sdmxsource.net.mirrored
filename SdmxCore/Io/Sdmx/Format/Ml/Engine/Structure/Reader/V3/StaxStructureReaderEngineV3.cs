// -----------------------------------------------------------------------
// <copyright file="StaxStructureReaderEngineV3.cs" company="EUROSTAT">
//   DateTime Created : 2023-06-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3
{
    using Io.Sdmx.Api.Singleton;
    using Io.Sdmx.Api.Xml;
    using Io.Sdmx.Utils.Core.Application;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using System.Collections.Generic;
    using Org.Sdmxsource.Util.Extensions;

    public class StaxStructureReaderEngineV3 : AbstractStaxStructureReaderEngineV3, IFusionSingleton
    {
        private static readonly IDictionary<string, SdmxStructureEnumType> _structureTypesToNodeName
            = new Dictionary<string, SdmxStructureEnumType>();

        static StaxStructureReaderEngineV3()
        {
            _structureTypesToNodeName.Put("AgencySchemes", SdmxStructureEnumType.AgencyScheme);
            _structureTypesToNodeName.Put("Categorisations", SdmxStructureEnumType.Categorisation);
            //CategorySchemeMaps
            _structureTypesToNodeName.Put("CategorySchemes", SdmxStructureEnumType.CategoryScheme);
            _structureTypesToNodeName.Put("Codelists", SdmxStructureEnumType.CodeList);
            //ConceptSchemeMaps
            _structureTypesToNodeName.Put("ConceptSchemes", SdmxStructureEnumType.ConceptScheme);
            //        structureTypesToNodeName.put("CustomTypeSchemes", SDMX_STRUCTURE_TYPE.VTL_CUSTOM_TYPE_SCHEME);
            _structureTypesToNodeName.Put("DataConstraints", SdmxStructureEnumType.ContentConstraint);
            _structureTypesToNodeName.Put("DataConsumerSchemes", SdmxStructureEnumType.DataConsumerScheme);
            _structureTypesToNodeName.Put("Dataflows", SdmxStructureEnumType.Dataflow);
            _structureTypesToNodeName.Put("DataProviderSchemes", SdmxStructureEnumType.DataProviderScheme);
            _structureTypesToNodeName.Put("DataStructures", SdmxStructureEnumType.Dsd);
            //        structureTypesToNodeName.put("GeographicCodelists", SDMX_STRUCTURE_TYPE.CODE_LIST);
            //        structureTypesToNodeName.put("GeoGridCodelists", SDMX_STRUCTURE_TYPE.CODE_LIST);
            _structureTypesToNodeName.Put("Hierarchies", SdmxStructureEnumType.HierarchyV30);
            _structureTypesToNodeName.Put("HierarchyAssociations", SdmxStructureEnumType.HierarchyAssociation);
            _structureTypesToNodeName.Put("Metadataflows", SdmxStructureEnumType.MetadataFlow);
            //        structureTypesToNodeName.put("MetadataProviderSchemes", SDMX_STRUCTURE_TYPE.METADATA_PROVIDER_SCHEME);
            //        structureTypesToNodeName.put("MetadataProvisionAgreements", SDMX_STRUCTURE_TYPE.METADATA_PROVISION);
            _structureTypesToNodeName.Put("MetadataStructures", SdmxStructureEnumType.Msd);
            //        structureTypesToNodeName.put("NamePersonalisationSchemes", SDMX_STRUCTURE_TYPE.VTL_NAME_PERSONALISATION_SCHEME);
            _structureTypesToNodeName.Put("OrganisationUnitSchemes", SdmxStructureEnumType.OrganisationUnitScheme);
            //        structureTypesToNodeName.put("Processes", SDMX_STRUCTURE_TYPE.PROCESS);
            _structureTypesToNodeName.Put("ProvisionAgreements", SdmxStructureEnumType.ProvisionAgreement);
            //        structureTypesToNodeName.put("ReportingTaxonomies", SDMX_STRUCTURE_TYPE.REPORTING_TAXONOMY);
            //        structureTypesToNodeName.put("RepresentationMaps", SDMX_STRUCTURE_TYPE.REPRESENTATION_MAP);
            //        structureTypesToNodeName.put("RulesetSchemes", SDMX_STRUCTURE_TYPE.VTL_RULESET_SCHEME);
            //        structureTypesToNodeName.put("StructureMaps", SDMX_STRUCTURE_TYPE.STRUCTURE_MAP);
            //        structureTypesToNodeName.put("TransformationSchemes", SDMX_STRUCTURE_TYPE.VTL_TRANSFORMATION_SCHEME);
            //        structureTypesToNodeName.put("UserDefinedOperatorSchemes", SDMX_STRUCTURE_TYPE.VTL_USER_DEFINED_OPERATOR_SCHEME);
            //        structureTypesToNodeName.put("ValueLists", SDMX_STRUCTURE_TYPE.VALUE_LIST);
            //        structureTypesToNodeName.put("VtlMappingSchemes", SDMX_STRUCTURE_TYPE.VTL_MAPPING_SCHEME);
        }

        private static StaxStructureReaderEngineV3 _instance;

        public static StaxStructureReaderEngineV3 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StaxStructureReaderEngineV3();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }

        public void DestroyInstance()
        {
            _instance = null;
        }

        //Private constructor - lazy load instance
        private StaxStructureReaderEngineV3() : base("Structures")
        {

            //        readerEngines.put(SDMX_STRUCTURE_TYPE.AGENCY_SCHEME, StaxAgencySchemeReaderEngineV3.Instance);
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.DATA_PROVIDER_SCHEME, StaxDataProviderSchemeReaderEngineV3.Instance);
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.METADATA_PROVIDER_SCHEME, StaxMetadataProviderSchemeReaderEngineV3.Instance);
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.DATA_CONSUMER_SCHEME, StaxDataConsumerSchemeReaderEngineV3.Instance);
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.ORGANISATION_UNIT_SCHEME, StaxOrgUnitSchemeReaderEngineV3.Instance);
            // readerEngines.Put(SdmxStructureEnumType.Categorisation, StaxCategorisationReaderEngineV3._instance);
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.CATEGORY_SCHEME, StaxCategorySchemeReaderEngineV3.Instance);
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.CODE_LIST, StaxCodelistReaderEngineV3.Instance);
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.CONCEPT_SCHEME, StaxConceptSchemeReaderEngineV3.Instance);
            //
            //
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.METADATA_FLOW, StaxMetadataflowReaderEngineV3.Instance);
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.METADATA_PROVISION, StaxMetadataProvisionReaderEngineV3.Instance);
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.DATAFLOW, StaxDataflowReaderEngineV3.Instance);
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.CONTENT_CONSTRAINT, StaxContentConstraintReaderEngineV3.Instance);
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.ADVANCED_RELEASE_CALENDAR, StaxContentConstraintReaderEngineV3.Instance);
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.DSD, StaxDsdReaderEngineV3.Instance);
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.HIERARCHY_ASSOCIATION, StaxHierarchyAssociationReaderEngineV3.Instance);
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.HIERARCHY, StaxHclReaderEngineV3.Instance);
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.MSD, StaxMSDReaderEngineV3.Instance);
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.PROCESS, StaxProcessReaderEngineV3.Instance);
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.PROVISION_AGREEMENT, StaxProvisionReaderEngineV3.Instance);
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.REGISTRATION, StaxRegistrationReaderEngineV3.Instance);
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.STRUCTURE_MAP, StaxStructureMapReaderEngineV3.Instance);
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.REPRESENTATION_MAP, StaxRepresentationMapReaderEngineV3.Instance);
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.REPORTING_TAXONOMY, StaxReportingTaxonomyReaderEngineV3.Instance);
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.VALUE_LIST, StaxValueListReaderEngineV3.Instance);
            //
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.VTL_CUSTOM_TYPE_SCHEME, StaxCustomTypeSchemeReaderEngineV3.Instance);
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.VTL_NAME_PERSONALISATION_SCHEME, StaxNamePersonalisationSchemeReaderEngineV3.Instance);
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.VTL_RULESET_SCHEME, StaxRulesetSchemeReaderEngineV3.Instance);
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.VTL_TRANSFORMATION_SCHEME, StaxTransformationSchemeReaderEngineV3.Instance);
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.VTL_USER_DEFINED_OPERATOR_SCHEME, StaxUserDefinedOperatorSchemeReaderEngineV3.Instance);
            //        readerEngines.put(SDMX_STRUCTURE_TYPE.VTL_MAPPING_SCHEME, StaxVtlMappingSchemeReaderEngineV3.Instance);
        }

        protected override SdmxStructureEnumType GetStructureType(IStaxReader reader)
        {
            return _structureTypesToNodeName[reader.ElementName];
        }

        protected override SdmxSchema OutputVersion => SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionThree);

        protected override IHeader ProcessHeader(IStaxReader reader)
        {
            return StaxHeaderReaderEngineV3.ReaderHeader(reader);
        }
    }
}