// -----------------------------------------------------------------------
// <copyright file="AbstractStaxStructureReaderEngineV3.cs" company="EUROSTAT">
//   Date Created : 2023-06-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3
{
    using System.Collections.Generic;
    using Io.Sdmx.Api.Xml;
    using Io.Sdmx.Format.Ml.Api.Engine;
    using Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Categorisation;
    using Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.CategoryScheme;
    using Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Constraint;
    using Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Hcl;
    using Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Metadata;
    using Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.OrgScheme;
    using Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Provision;
    using Io.Sdmx.Utils.Core.Xml;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using SdmxCore.Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Hcl;
    using SdmxCore.Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.OrgScheme;

#pragma warning disable CS0308 // Warning CS0308: The non-generic type 'System.Collections.ArrayList' cannot be used with type arguments
    public abstract class AbstractStaxStructureReaderEngineV3 : IStaxStructureReaderEngine
    {
        private string _startNode;

        public AbstractStaxStructureReaderEngineV3(string startNode)
        {
            this._startNode = startNode;
        }

        /// <summary> 
        /// Input is expected to be a version 2.1 Structure document with the layout 
        /// 
        /// <Structure> 
        ///    <Header>....</Header> 
        ///    <Structures>.[]</Structures> 
        /// </Structure> 
        /// 
        /// </summary> 
        public ISdmxObjects GetSdmxObjects(IReadableDataLocation rdl)
        {
            ISdmxObjects IsdmxObjects = new SdmxObjectsImpl();
            IStaxReader staxReader = new StaxReader(rdl);
            try
            {
                if (!staxReader.JumpToNode("Header"))
                {
                    return IsdmxObjects;
                }
                IsdmxObjects.Header = ProcessHeader(staxReader);

                while (staxReader.MoveNextStartNode())
                {
                    if (staxReader.ElementName.Equals("SubmitStructureRequest"))
                    {
                        IDictionary<string, string> attrs = staxReader.GetAttributes();
                        if (attrs.ContainsKey("action"))
                        {
                            IsdmxObjects.Action = DatasetAction.GetAction(attrs["action"]);
                        }
                    }

                    if (_startNode == null || staxReader.ElementName.Equals(_startNode))
                    {
                        if (_startNode != null)
                        {
                            staxReader.MoveNextNode();
                        }
                        do
                        {
                            if (staxReader.IsStartElement)
                            {
                                ProcessNode(staxReader, IsdmxObjects);
                            }
                            else if (staxReader.ElementName.Equals(_startNode))
                            {
                                break;
                            }
                        } while (staxReader.MoveNextNode());
                    }
                }
            }
            finally
            {
                staxReader.Close();
            }

            return IsdmxObjects;
        }

        protected abstract IHeader ProcessHeader(IStaxReader reader);

        protected void ProcessOrgansiationScheme(IStaxReader staxReader, ISdmxObjects IsdmxObjects)
        {
            if (staxReader.MoveNextStartNode())
            {
                ProcessNode(staxReader, IsdmxObjects);
            }
        }

        private void ProcessConstraint(IStaxReader staxReader, ISdmxObjects IsdmxObjects)
        {
            if (staxReader.MoveNextStartNode())
            {
                ProcessNode(staxReader, IsdmxObjects);
            }
        }

        protected void ProcessNode(IStaxReader reader, ISdmxObjects objects)
        {
            SdmxStructureEnumType structureType = GetStructureType(reader);
            if (structureType == null)
            {
                throw new SdmxSemmanticException("Unexpected node : " + reader.GetXMLPath());
            }
            //if(Arrays.binarySearch(expectedTypes, structureType) < 0) {
            //TODO Exception
            //}
            switch (structureType)
            {
                //NOTE SDMXSOURCE_SDMXCORE CHANGE implemented in different engine
                case SdmxStructureEnumType.Dataflow:
                case SdmxStructureEnumType.Dsd:
                case SdmxStructureEnumType.CodeList:
                case SdmxStructureEnumType.ConceptScheme:               
                //TODO SDMXCORE remove when implemented, source engine skips them              
                case SdmxStructureEnumType.DataConstraint:
                    //Already read from StructuresParsingEngine
                    reader.SkipNode();
                    break;
                case SdmxStructureEnumType.ContentConstraint:
                    foreach (IMaintainableObject maint in StaxContentConstraintReaderEngineV3.Instance.ProcessSDMX(reader))
                    {
                        AddIdentifiable(objects, structureType, maint);
                    }
                    break;
                case SdmxStructureEnumType.OrganisationScheme:
                    //TODO
                    break;
                //		case CONTENT_CONSTRAINT:
                //			ProcessConstraint(reader, objects);
                //			break;
                case SdmxStructureEnumType.MetadataFlow:
                    foreach (IMaintainableObject maint in StaxMetadataflowReaderEngineV3.Instance.ProcessSDMX(reader))
                    {
                        AddIdentifiable(objects, structureType, maint);
                    }
                    break;
                case SdmxStructureEnumType.ProvisionAgreement:
                    foreach (IMaintainableObject maint in StaxProvisionReaderEngineV3.Instance.ProcessSDMX(reader))
                    {
                        //NOTE SDMXSOURCE_SDMXCORE CHANGE in core, AddIdentifiable() returns true if maint exists
                        if (objects.GetMaintainables(structureType).Contains(maint))
                        {
                            throw new SdmxSemmanticException("Duplicate Structure found in message AgencyId=" + maint.AgencyId + " Id=" + maint.Id + " Version=" + maint.Version);
                        }
                        else
                        {
                            objects.AddIdentifiable(maint);
                        }
                    }
                    break;
                case SdmxStructureEnumType.Categorisation:
                    foreach (IMaintainableObject maint in StaxCategorisationReaderEngineV3.Instance.ProcessSDMX(reader))
                    {
                        AddIdentifiable(objects, structureType, maint);
                    }
                    break;
                case SdmxStructureEnumType.CategoryScheme:
                    foreach (IMaintainableObject maint in StaxCategorySchemeReaderEngineV3.Instance.ProcessSDMX(reader))
                    {
                        AddIdentifiable(objects, structureType, maint);
                    }
                    break;
                case SdmxStructureEnumType.DataProviderScheme:
                    foreach (IMaintainableObject maint in StaxDataProviderSchemeReaderEngineV3.Instance.ProcessSDMX(reader))
                    {
                        AddIdentifiable(objects, structureType, maint);
                    }
                    break;
                case SdmxStructureEnumType.DataConsumerScheme:
                    foreach (IMaintainableObject maint in StaxDataConsumerSchemeReaderEngineV3.Instance.ProcessSDMX(reader))
                    {
                        AddIdentifiable(objects, structureType, maint);
                    }
                    break;
                case SdmxStructureEnumType.OrganisationUnitScheme:
                    foreach (IMaintainableObject maint in StaxOrgUnitSchemeReaderEngineV3.Instance.ProcessSDMX(reader))
                    {
                        AddIdentifiable(objects, structureType, maint);
                    }
                    break;
                case SdmxStructureEnumType.AgencyScheme:
                    foreach (IMaintainableObject maint in StaxAgencySchemeReaderEngineV3.Instance.ProcessSDMX(reader))
                    {
                        AddIdentifiable(objects, structureType, maint);
                    }
                    break;
                case SdmxStructureEnumType.HierarchyV30:
                    foreach (IMaintainableObject maint in StaxHierarchyReaderEngineV3.Instance.ProcessSDMX(reader))
                    {
                        AddIdentifiable(objects, structureType, maint);
                    }
                    break;
                case SdmxStructureEnumType.HierarchyAssociation:
                    foreach (IMaintainableObject maint in StaxHierarchyAssociationReaderEngineV3.Instance.ProcessSDMX(reader))
                    {
                        AddIdentifiable(objects, structureType, maint);
                    }
                    break;
                case SdmxStructureEnumType.Msd:
                    foreach (IMaintainableObject maint in StaxMSDReaderEngineV3.Instance.ProcessSDMX(reader))
                    {
                        AddIdentifiable(objects, structureType, maint);
                    }
                    break;
                default:
                    reader.SkipNode(); 
                    break;
            }
        }

        private static void AddIdentifiable(ISdmxObjects obj, SdmxStructureEnumType sType, IMaintainableObject maint)
        {
            var objects = obj;
            var structureType = sType;

            //NOTE SDMXSOURCE_SDMXCORE CHANGE in core, AddIdentifiable() returns true if maint exists
            if (objects.GetMaintainables(structureType).Contains(maint))
            {
                throw new SdmxSemmanticException("Duplicate Structure found in message AgencyId=" + maint.AgencyId + " Id=" + maint.Id + " Version=" + maint.Version);
            }
            else
            {
                objects.AddIdentifiable(maint);
            }
        }

        protected abstract SdmxStructureEnumType GetStructureType(IStaxReader reader);
        protected abstract SdmxSchema OutputVersion { get; }
    }
#pragma warning restore CS0308 // Warning CS0308: The non-generic type 'System.Collections.ArrayList' cannot be used with type arguments
}