// -----------------------------------------------------------------------
// <copyright file="StaxIdentifiableUtilV3.cs" company="EUROSTAT">
//   DateTime Created : 2023-06-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Util
{
    using Io.Sdmx.Api.Xml;
    using Io.Sdmx.Format.Ml.Util;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using System.Collections.Generic;
    using System;

    public class StaxIdentifiableUtilV3
    {
        public static void ProcessObject(IIdentifiableMutableObject sdmxObject, IStaxReader reader)
        {
            ProcessAttributes(sdmxObject, reader);
            StaxAnnotableUtilV3.ProcessObject(sdmxObject, reader);
        }

        /// <summary> 
        /// populates the Identifiable with 'id' and 'uri' attribute values read from the current XML node. 
        /// </summary>
        /// <param name=" IidentifiableObject"> 
        /// </param>
        /// <param name=" staxReader"> 
        /// </param> 
        public static void ProcessAttributes(IIdentifiableMutableObject IidentifiableObject, IStaxReader staxReader)
        {
            IDictionary<string, string> attributes = staxReader.GetAttributes();
            //Id may not be mandatory for an identifiable that inherits it from a concept if not provided
            IidentifiableObject.Id = StaxStructureUtil.GetAttribute(attributes, "id", false);
            string uri = StaxStructureUtil.GetAttribute(attributes, "uri", false);
            IidentifiableObject.Uri = uri != null ? new Uri(uri) : null;
        }
    }
}