// -----------------------------------------------------------------------
// <copyright file="StaxAgencySchemeReaderEngineV3.cs" company="EUROSTAT">
//   Date Created : 2023-11-23
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.OrgScheme
{
    using System.Collections.Generic;
    using Io.Sdmx.Api.Singleton;
    using Io.Sdmx.Api.Xml;
    using Io.Sdmx.Format.Ml.Api.Engine;
    using Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Util;
    using Io.Sdmx.Format.Ml.Util;
    using Io.Sdmx.Utils.Core.Application;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    public class StaxAgencySchemeReaderEngineV3 : StaxAbstractOrgansiationReaderV3, IStaxMaintainableReaderEngine<IAgencyScheme>, IFusionSingleton
    {
        private static StaxAgencySchemeReaderEngineV3 _instance;

        //Private constructor - lazy load instance
        private StaxAgencySchemeReaderEngineV3() { }

        public static StaxAgencySchemeReaderEngineV3 Instance
        {
            get 
            {
                if (_instance == null)
                {
                    _instance = new StaxAgencySchemeReaderEngineV3();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }        
        }
  
        public void DestroyInstance()
        {
            _instance = null;
        }

        public ISdmxObjects ObtainObjectsFromSDMX(IStaxReader reader)
        {
            ISdmxObjects objects = new SdmxObjectsImpl();
            objects.AddIdentifiables(ProcessSDMX(reader));
            return objects;
        }
      
        public ISet<IAgencyScheme> ProcessSDMX(IStaxReader reader)
        {
            ISet<IAgencyScheme> returnSet = new HashSet<IAgencyScheme>();
            string exitNode = reader.ElementName;
            while (reader.MoveNextNode())
            {
                string nodeName = reader.ElementName;
                if (reader.IsStartElement)
                {
                    if (nodeName.Equals("AgencyScheme"))
                    {
                        IAgencySchemeMutableObject agencyScheme = new AgencySchemeMutableCore();
                        StaxItemSchemeUtilV3.ProcessObject(agencyScheme, reader);

                        //Expected position is now Agency
                        string elementName = reader.ElementName;
                        while (reader.IsStartElement)
                        {
                            if (!elementName.Equals("Agency"))
                            {
                                StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                            }
                            ProcessAgency(agencyScheme, reader);
                            reader.MoveNextNode();
                        }
                        IAgencyScheme acySch = agencyScheme.ImmutableInstance;
                        returnSet.Add(acySch);
                    }
                    else
                    {
                        StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                    }
                }
                else if (nodeName.Equals(exitNode))
                {
                    break;
                }
            }
            return returnSet;
        }

        private void ProcessAgency(IAgencySchemeMutableObject objectToPopulate, IStaxReader reader)
        {
            IAgencyMutableObject org = new AgencyMutableCore();
            objectToPopulate.AddItem(org);
            base.ProcessOrganisation(org, reader); ;
        }
    }
}