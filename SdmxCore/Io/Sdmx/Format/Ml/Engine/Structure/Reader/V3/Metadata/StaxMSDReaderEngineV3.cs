// -----------------------------------------------------------------------
// <copyright file="StaxMSDReaderEngineV3.cs" company="EUROSTAT">
//   DateTime Created : 2024-04-02
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Io.Sdmx.Api.Singleton;
using Io.Sdmx.Api.Xml;
using Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Util;
using Io.Sdmx.Format.Ml.Util;
using Io.Sdmx.Utils.Core.Application;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.MetadataStructure;
using SdmxCore.Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Util;
using SdmxCore.IO.Sdmx.Format.Ml.Api.Engine;

namespace Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Metadata
{
    public class StaxMSDReaderEngineV3 : AbstractStaxMaintainableReaderEngine<IMetadataStructureDefinitionObject>, IFusionSingleton
    {
        private static StaxMSDReaderEngineV3 _instance;

        private StaxMSDReaderEngineV3() { }

        public static StaxMSDReaderEngineV3 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StaxMSDReaderEngineV3();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }

        public void DestroyInstance()
        {
            _instance = null;
        }

        public override ISet<IMetadataStructureDefinitionObject> ProcessSDMX(IStaxReader reader)
        {
            HashSet<IMetadataStructureDefinitionObject> returnSet = new HashSet<IMetadataStructureDefinitionObject>();

            string containerElement = reader.ElementName;
            while (reader.MoveNextNode())
            {
                string nodeName = reader.ElementName;
                if (reader.IsStartElement)
                {
                    if (nodeName.Equals("MetadataStructure"))
                    {
                        var aBean = ProcessMSD(reader);
                        if (!returnSet.Add(aBean))
                        {
                            throw new SdmxSemmanticException("Duplicate MetadataStructureDefinitionBean found in message. AgencyId=" + aBean.AgencyId + " Id=" + aBean.Id + " Version=" + aBean.Version);
                        }
                    }
                    else
                    {
                        StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                    }
                }
                else if (nodeName.Equals(containerElement))
                {
                    break;
                }
            }
            return returnSet;
        }

        private IMetadataStructureDefinitionObject ProcessMSD(IStaxReader reader)
        {
            var msd = new MetadataStructureDefinitionMutableCore();
            StaxMaintainableUtilV3.ProcessObject(msd, reader);
            if (reader.IsStartElement)
            {
                if (!reader.ElementName.Equals("MetadataStructureComponents"))
                {
                    StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                }
                while (reader.MoveNextNode())
                {
                    string nodeName = reader.ElementName;
                    if (reader.IsStartElement)
                    {
                        if (nodeName.Equals("MetadataAttributeList"))
                        {
                            ProcessMetadataAttributes(msd, reader);
                        }
                        else
                        {
                            StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                        }
                    }
                    else if (nodeName.Equals("MetadataStructureComponents"))
                    {
                        break;
                    }
                }
            }
            return msd.ImmutableInstance;
        }

        private void ProcessMetadataAttributes(IMetadataAttributeContainerMutable attrContainer, IStaxReader reader)
        {
            StaxIdentifiableUtilV3.ProcessObject(new CodelistMutableCore(), reader);

            while (reader.IsStartElement)
            {
                string elementName = reader.ElementName;
                if (elementName.Equals("MetadataAttribute"))
                {
                    attrContainer.AddMetadataAttributes(ProcessMetadataAttribute(reader));
                }
                else
                {
                    StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                }
                reader.MoveNextNode();
            }
        }

        private IMetadataAttributeMutableObject ProcessMetadataAttribute(IStaxReader reader)
        {
            var returnBean = new MetadataAttributeMutableCore();
            IDictionary<string, string> attributes = reader.GetAttributes();

            returnBean.MinOccurs = StaxStructureUtil.GetAttributeAsInteger(attributes, "minOccurs", false);
            returnBean.MaxOccurs = StaxStructureUtil.GetAttributeAsInteger(attributes, "maxOccurs", false);
            returnBean.Presentational = StaxStructureUtil.GetAttributeAsTertiaryBoolean(attributes, "isPresentational", false);
            StaxIdentifiableUtilV3.ProcessObject(returnBean, reader);

            while (reader.IsStartElement)
            {
                string elementName = reader.ElementName;
                if (elementName.Equals("ConceptIdentity"))
                {
                    returnBean.ConceptRef = StaxStructureRefReaderV3.GetStructureReference(reader, SdmxStructureEnumType.Concept);
                }
                else if (elementName.Equals("LocalRepresentation"))
                {
                    var representation = returnBean.Representation;
                    if (representation != null)
                    {
                        StaxRepresentationUtilV3.ProcessObject(reader, representation);
                    }
                    else
                    {
                        returnBean.Representation = StaxRepresentationUtilV3.ProcessObject(reader);
                    }
                }
                else if (elementName.Equals("MetadataAttribute"))
                {
                    returnBean.MetadataAttributes.Add(ProcessMetadataAttribute(reader));
                }
                else
                {
                    StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                }
                reader.MoveNextNode();
            }
            return returnBean;
        }
    }
}
