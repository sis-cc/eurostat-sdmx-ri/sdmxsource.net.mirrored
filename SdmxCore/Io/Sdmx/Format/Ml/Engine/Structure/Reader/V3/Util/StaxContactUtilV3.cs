// -----------------------------------------------------------------------
// <copyright file="StaxContactUtilV3.cs" company="EUROSTAT">
//   Date Created : 2023-06-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Util
{
    using Io.Sdmx.Api.Xml;
    using Io.Sdmx.Format.Ml.Util;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;

    public class StaxContactUtilV3
    {
        public static void ProcessObject(IContactMutableObject contact, IStaxReader reader)
        {
            contact.Id = reader.GetAttributes().ContainsKey("id") ? reader.GetAttributes()["id"] : null;

            while (reader.MoveNextNode())
            {
                string nodeName = reader.ElementName;
                if (reader.IsStartElement)
                {
                    if (nodeName.Equals("Name"))
                    {
                        contact.AddName(StaxStructureUtil.GetText(reader));
                    }
                    else if (nodeName.Equals("Department"))
                    {
                        contact.AddDepartment(StaxStructureUtil.GetText(reader));
                    }
                    else if (nodeName.Equals("Role"))
                    {
                        contact.AddRole(StaxStructureUtil.GetText(reader));
                    }
                    else if (nodeName.Equals("Telephone"))
                    {
                        contact.AddTelephone(reader.GetElementText());
                    }
                    else if (nodeName.Equals("Fax"))
                    {
                        contact.AddFax(reader.GetElementText());
                    }
                    else if (nodeName.Equals("X400"))
                    {
                        contact.AddX400(reader.GetElementText());
                    }
                    else if (nodeName.Equals("Uri"))
                    {
                        contact.AddUri(reader.GetElementText());
                    }
                    else if (nodeName.Equals("Email"))
                    {
                        contact.AddEmail(reader.GetElementText());
                    }
                    else
                    {
                        StaxStructureUtil.ThrowUnexpectedNodeException(reader);
                    }
                }
                else if (nodeName.Equals("Contact"))
                {
                    return;
                }
            }
        }
    }
}