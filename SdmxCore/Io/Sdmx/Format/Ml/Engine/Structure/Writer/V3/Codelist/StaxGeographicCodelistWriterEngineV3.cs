// -----------------------------------------------------------------------
// <copyright file="StaxGeographicCodelistWriterEngineV3.cs" company="EUROSTAT">
//   Date Created : 2024-03-05
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Io.Sdmx.Api.Singleton;
using Io.Sdmx.Core.Sdmx.Api.Model.Structure;
using Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Util;
using Io.Sdmx.Utils.Core.Application;
using Io.Sdmx.Utils.Core.Xml;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;

namespace Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Codelist
{
    public class StaxGeographicCodelistWriterEngineV3 : AbstractV3Writer<IGeographicCodelistObject>, IFusionSingleton
    {
        private static StaxGeographicCodelistWriterEngineV3 _instance;

        public static StaxGeographicCodelistWriterEngineV3 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StaxGeographicCodelistWriterEngineV3();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }

        protected override string RootNodeName => "GeographicCodelist";

        public void DestroyInstance()
        {
            _instance = null;
        }

        protected override IDictionary<string, string> GetAdditionalMaintainableAttributes(IGeographicCodelistObject maint)
        {

            Dictionary<string, string> attributes = new Dictionary<string, string>
            {
                ["geoType"] = "GeographicCodelist"
            };

            return attributes;
        }

        protected override void WriteMaintainableInternal(IGeographicCodelistObject maint, IExternalMaintainableLinks externalLinks, StaxWriter writer)
        {
            WriteCodes(maint.Items, externalLinks, writer);
        }

        private void WriteCodes(IList<IGeoFeatureSetCode> codes, IExternalMaintainableLinks externalMaintainableLinks, StaxWriter writer)
        {
            foreach (IGeoFeatureSetCode code in codes)
            {              
                Dictionary<string, string> attributes = new Dictionary<string, string>
                {
                    ["value"] = code.GeoFeatureSet
                };

                StaxNameableWriterUtilV3.WriteObject(code, "GeoFeatureSetCode", externalMaintainableLinks, writer, attributes);
                if (code.Parent != null)
                {
                    StaxRefUtilV3.WriteLocalReference(code.ParentCode, writer, "Parent");
                }
                writer.WriteEndElement();

            }
        }

    }
}
