// -----------------------------------------------------------------------
// <copyright file="StaxTextWriterUtilV3.cs" company="EUROSTAT">
//   Date Created : 2023-06-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Util
{
    using System.Collections.Generic;
    using Io.Sdmx.Format.Ml.Constant;
    using Io.Sdmx.Utils.Core.Xml;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Util.Extensions;

    public class StaxTextWriterUtilV3
    {
        public static void WriteTextTypes(ICollection<ITextTypeWrapper> text, StaxWriter writer, string elementName)
        {
            WriteTextTypes(SdmxNamespace.GetFromEnum(SdmxNamespaceEnumType.Common3).Namespace, text, writer, elementName);
        }

        public static void WriteTextTypes(Namespace ns, ICollection<ITextTypeWrapper> text, StaxWriter writer, string elementName)
        {
            if (text != null)
            {
                foreach (ITextTypeWrapper tt in text)
                {
                    IDictionary<string, string> attributes = new Dictionary<string, string>();
                    attributes.Put("xml:lang", tt.Locale);
                    writer.WriteElement(ns, elementName, tt.Value, attributes);
                }
            }
        }
    }
}