// -----------------------------------------------------------------------
// <copyright file="StaxContactWriterUtilV3.cs" company="EUROSTAT">
//   Date Created : 2023-06-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Util
{
    using System.Collections.Generic;
    using Io.Sdmx.Format.Ml.Constant;
    using Io.Sdmx.Utils.Core.Xml;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    public class StaxContactWriterUtilV3
    {
        /// <summary> 
        /// Writes one or more Contact objects 
        /// </summary>
        /// <param name=" contacts"> 
        /// </param>
        /// <param name=" writer"> 
        /// </param> 
        public static void WriteContact(IList<IContact> contacts, StaxWriter writer)
        {
            WriteContact(SdmxNamespace.GetFromEnum(SdmxNamespaceEnumType.Structure3).Namespace, contacts, writer);
        }

        /// <summary> 
        /// The Receiver in an SDMX Header has different Namespaces to other Contacts, so gets its own method 
        /// </summary>
        /// <param name=" contacts"> 
        /// </param>
        /// <param name=" writer"> 
        /// </param> 
        public static void WriteContactForReceiver(IList<IContact> contacts, StaxWriter writer)
        {
            WriteContact(SdmxNamespace.GetFromEnum(SdmxNamespaceEnumType.Message3).Namespace, contacts, writer);
        }

        private static void WriteContact(Namespace ns, IList<IContact> contacts, StaxWriter writer)
        {
            foreach (IContact contact in contacts)
            {
                writer.WriteStartElement(ns, "Contact");
                writer.WriteAttribute("id", contact.Id);
                StaxTextWriterUtilV3.WriteTextTypes(contact.Name, writer, "Name");
                StaxTextWriterUtilV3.WriteTextTypes(ns, contact.Departments, writer, "Department");
                StaxTextWriterUtilV3.WriteTextTypes(ns, contact.Role, writer, "Role");
                WriteElements(ns, contact.Telephone, "Telephone", writer);
                WriteElements(ns, contact.Fax, "Fax", writer);
                WriteElements(ns, contact.X400, "X400", writer);
                WriteElements(ns, contact.Uri, "Uri", writer);
                WriteElements(ns, contact.Email, "Email", writer);
                writer.WriteEndElement(); //End Org
            }
        }

        private static void WriteElements(Namespace ns, IList<string> str, string elementName, StaxWriter writer)
        {
            foreach (string currentStr in str)
            {
                writer.WriteElement(ns, elementName, currentStr);
            }
        }
    }
}