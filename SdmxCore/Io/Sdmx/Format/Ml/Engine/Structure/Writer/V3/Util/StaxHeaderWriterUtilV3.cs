// -----------------------------------------------------------------------
// <copyright file="StaxHeaderWriterUtilV3.cs" company="EUROSTAT">
//   Date Created : 2023-06-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Util
{
    using Io.Sdmx.Utils.Core.Xml;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Util.Date;
    using System;
    using Org.Sdmxsource.Util;
    using System.Collections.Generic;

    public class StaxHeaderWriterUtilV3
    {
        public static void WriteHeader(StaxWriter writer, Namespace messageNs, IHeader header, bool includeReceiver, bool isRegistryInterfaceMessage)
        {
            writer.WriteStartElement(messageNs, "Header");
            writer.WriteElement(messageNs, "ID", header.Id);
            writer.WriteElement(messageNs, "Test", header.Test.ToString().ToLower());

            DateTime? prepared = header.Prepared != null ? header.Prepared : new DateTime();
            // The Prepared date must be in ISO 8601 Zulu Time
            writer.WriteElement(messageNs, "Prepared", DateUtil.FormatDate(prepared.Value) + "Z");
            writer.WriteStartElement(messageNs, "Sender");
            writer.WriteAttribute("id", header.Sender.Id);
            writer.WriteEndElement();  //End Sender
            if (includeReceiver || isRegistryInterfaceMessage)
            {
                // Write Receiver
                if (ObjectUtil.ValidCollection(header.Receiver))
                {
                    // If this is a registry message, there may only be one receiver, otherwise there can be multiple.
                    if (isRegistryInterfaceMessage)
                    {
                        // Only write the first
                        WriteReceiver(writer, messageNs, header.Receiver[0]);
                    }
                    else
                    {
                        // Write all receivers
                        IList<IParty> allReceivers = header.Receiver;
                        foreach (IParty IpartyObject in allReceivers)
                        {
                            WriteReceiver(writer, messageNs, IpartyObject);
                        }
                    }
                }
                else
                {
                    // Write "default" receiver
                    WriteReceiver(writer, messageNs, "not_supplied");
                }
            }

            writer.WriteEndElement();  //End Header
        }

        private static void WriteReceiver(StaxWriter writer, Namespace messageNs, IParty IpartyObject)
        {
            writer.WriteStartElement(messageNs, "Receiver");
            writer.WriteAttribute("id", IpartyObject.Id);

            // Write names if present
            IList<ITextTypeWrapper> names = IpartyObject.Name;
            if (ObjectUtil.ValidCollection(names))
            {
                StaxTextWriterUtilV3.WriteTextTypes(names, writer, "Name");
            }

            // Write contacts if present
            IList<IContact> contacts = IpartyObject.Contacts;
            if (ObjectUtil.ValidCollection(contacts))
            {
                StaxContactWriterUtilV3.WriteContactForReceiver(contacts, writer);
            }

            writer.WriteEndElement();  //End Receiver
        }

        private static void WriteReceiver(StaxWriter writer, Namespace messageNs, string receiverId)
        {
            writer.WriteStartElement(messageNs, "Receiver");
            writer.WriteAttribute("id", receiverId);
            writer.WriteEndElement();  //End Receiver
        }
    }
}