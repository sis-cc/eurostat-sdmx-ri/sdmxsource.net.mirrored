// -----------------------------------------------------------------------
// <copyright file="StaxIdentifiableWriterUtilV3.cs" company="EUROSTAT">
//   Date Created : 2023-06-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Util
{
    using System.Collections.Generic;
    using System.Xml;
    using Io.Sdmx.Core.Sdmx.Api.Model.Structure;
    using Io.Sdmx.Format.Ml.Constant;
    using Io.Sdmx.Utils.Core.Xml;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Util.Extensions;

    public class StaxIdentifiableWriterUtilV3
    {
        public static void WriteObject(IIdentifiableObject ident, string elementName, IExternalMaintainableLinks externalLinks, StaxWriter writer)
        {
            WriteObject(ident, elementName, externalLinks, writer, null);
        }

        public static void WriteObject(IIdentifiableObject ident, string elementName, IExternalMaintainableLinks externalLinks, 
            StaxWriter writer, IDictionary<string, string> attrs)
        {
            IDictionary<string, string> attributes = GetAttributes(ident);
            if (attrs != null)
            {
                attributes.PutAll(attrs);
            }
            writer.WriteElement(SdmxNamespace.GetFromEnum(SdmxNamespaceEnumType.Structure3).Namespace, elementName, attributes);
            StaxAnnotableWriterUtilV3.WriteObject(ident, writer);
            WriteLinks(ident, externalLinks, writer);
        }

        public static IDictionary<string, string> GetAttributes(IIdentifiableObject ident)
        {
            IDictionary<string, string> attributes = new Dictionary<string, string>();
            attributes.Add("id", ident.Id);
            if (ident.StructureType == SdmxStructureEnumType.ContentConstraint)
            {
                // TODO SDMXCORE METADATA CONSTRAINTS
                attributes.Add("urn", ident.Urn.AbsoluteUri.Replace("ContentConstraint", "DataConstraint"));
            }
            else if(ident.StructureType == SdmxStructureEnumType.Level || ident.StructureType == SdmxStructureEnumType.HierarchicalCode)
            {
               //we need to modify the urn
               var originalUrn = ident.Urn.AbsoluteUri;
               var lastIndexOfVersion = originalUrn.IndexOf(")");
               attributes.Add("urn", originalUrn.Substring(0, lastIndexOfVersion + 1) + "." + ident.Id);
            }
            else
            {
                attributes.Add("urn", ident.Urn.AbsoluteUri);
            }
            
            if (ident.Uri != null)
            {
                attributes.Add("uri", ident.Uri.ToString());
            }
            return attributes;
        }

        public static void WriteLinks(IIdentifiableObject ident, IExternalMaintainableLinks externalLinks, StaxWriter writer)
        {
            ISet<ILinkObject> links = externalLinks != null ? externalLinks.GetLinks(ident.Urn.AbsoluteUri) : null;
            WriteLinks(links, writer);
            WriteLinks(ident.Links, writer);
        }

        private static void WriteLinks(ICollection<ILinkObject> links, StaxWriter writer)
        {
            if (links != null)
            {
                foreach (ILinkObject link in links)
                {
                    writer.WriteStartElement(SdmxNamespace.GetFromEnum(SdmxNamespaceEnumType.Common3).Namespace, "Link");

                    if (link.Rel != null)
                    {
                        writer.WriteAttribute("rel", link.Rel);
                    }
                    if (link.HRef != null)
                    {
                        writer.WriteAttribute("url", link.HRef.ToString());
                    }
                    if (link.Urn != null)
                    {
                        writer.WriteAttribute("urn", link.Urn.ToString());
                    }
                    if (link.Type != null)
                    {
                        writer.WriteAttribute("type", link.Type);
                    }
                    writer.WriteEndElement();
                }
            }
        }
    }
}