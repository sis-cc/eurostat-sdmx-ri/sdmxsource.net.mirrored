// -----------------------------------------------------------------------
// <copyright file="StaxGeoCodelistWriterEngineV3.cs" company="EUROSTAT">
//   Date Created : 2024-03-13
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Codelist
{
    using System.Collections.Generic;
    using Io.Sdmx.Api.Singleton;
    using Io.Sdmx.Core.Sdmx.Api.Model.Structure;
    using Io.Sdmx.Format.Ml.Constant;
    using Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Util;
    using Io.Sdmx.Utils.Core.Application;
    using Io.Sdmx.Utils.Core.Xml;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;

    public class StaxGeoCodelistWriterEngineV3 : AbstractV3Writer<IGeoGridCodelistObject>, IFusionSingleton
    {
        private static StaxGeoCodelistWriterEngineV3 _instance;

        public static StaxGeoCodelistWriterEngineV3 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StaxGeoCodelistWriterEngineV3();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }

        protected override string RootNodeName => "GeoGridCodelist";

        public void DestroyInstance()
        {
            _instance = null;
        }

        protected override IDictionary<string, string> GetAdditionalMaintainableAttributes(IGeoGridCodelistObject maint)
        {

            Dictionary<string, string> attributes = new Dictionary<string, string>
            {
                ["geoType"] = "GeoGridCodelist"
            };

            return attributes;
        }

        protected override void WriteMaintainableInternal(IGeoGridCodelistObject maint, IExternalMaintainableLinks externalLinks, StaxWriter writer)
        {
            WriteCodes(maint.Items, externalLinks, writer);
            writer.WriteElement(StaxStructureWriterUtil.StrcutureNs3_0, "GridDefinition", maint.GridDefinition);
        }

        private void WriteCodes(IList<IGeoGridCode> codes, IExternalMaintainableLinks externalMaintainableLinks, StaxWriter writer)
        {
            foreach (IGeoGridCode code in codes)
            {               
                StaxNameableWriterUtilV3.WriteObject(code, "GeoGridCode", externalMaintainableLinks, writer);
                if (code.Parent != null)
                {
                    StaxRefUtilV3.WriteLocalReference(code.ParentCode, writer, "Parent");
                }
                writer.WriteElement(StaxStructureWriterUtil.StrcutureNs3_0, "GeoCell", code.GeoCell);
                writer.WriteEndElement();

            }
        }

    }
}
