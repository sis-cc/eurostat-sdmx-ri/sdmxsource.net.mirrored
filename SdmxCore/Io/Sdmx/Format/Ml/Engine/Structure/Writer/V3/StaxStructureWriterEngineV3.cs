// -----------------------------------------------------------------------
// <copyright file="StaxStructureWriterEngineV3.cs" company="EUROSTAT">
//   Date Created : 2023-06-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Io.Sdmx.Api.Sdmx.Manager.Output;
    using Io.Sdmx.Api.Singleton;
    using Io.Sdmx.Core.Sdmx.Api.Engine.Structure;
    using Io.Sdmx.Core.Sdmx.Api.Model.Structure;
    using Io.Sdmx.Core.Sdmx.Engine.Structure;
    using Io.Sdmx.Core.Sdmx.Util;
    using Io.Sdmx.Format.Ml.Api.Engine;
    using Io.Sdmx.Format.Ml.Constant;
    using Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Categorisation;
    using Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.CategoryScheme;
    using Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Codelist;
    using Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Msd;
    using Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.OrgScheme;
    using Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Provision;
    using Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Util;
    using Io.Sdmx.Utils.Core.Application;
    using Io.Sdmx.Utils.Core.Xml;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Util;
    using SdmxCore.Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Constraint;
    using SdmxCore.Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Hcl;
    using SdmxCore.Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Metadata;
    using SdmxCore.Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.OrgScheme;

    public class StaxStructureWriterEngineV3 : AbstractStructureWriterEngine, IStructureWriterEngine, IFusionSingleton
    {
        private static StaxStructureWriterEngineV3 _instance;
        private static StaxStructureWriterEngineV3 _prettyInstance;

        private readonly SdmxSchema _ouputVersion = SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionThree);

        private ISchemaLocationManager schemaLocationManager;
        private IHeaderRetrievalManager headerRetrievalManager;

        private IStaxMaintainableWriterEngine<ICodelistObject> codelistWriterEngine;
        private IStaxMaintainableWriterEngine<IConceptSchemeObject> conceptSchemeWriterEngine;
        private IStaxMaintainableWriterEngine<IDataflowObject> dataflowWriterEngine;
        private IStaxMaintainableWriterEngine<IDataStructureObject> dsdWriterEngine;
        private IStaxMaintainableWriterEngine<IDataProviderScheme> dataproviderSchemeEngine;

        private readonly Namespace messageNs = SdmxNamespace.GetFromEnum(SdmxNamespaceEnumType.Message3).Namespace;
        private readonly Namespace structureNs = SdmxNamespace.GetFromEnum(SdmxNamespaceEnumType.Structure3).Namespace;
        private readonly Namespace commonNs = SdmxNamespace.GetFromEnum(SdmxNamespaceEnumType.Common3).Namespace;
        private readonly Namespace registryNs = SdmxNamespace.GetFromEnum(SdmxNamespaceEnumType.Registry3).Namespace;

        private bool prettyPrint = false;

        public static StaxStructureWriterEngineV3 GetInstance(bool prettyPrint, 
            ISchemaLocationManager schemaLocationManager, IHeaderRetrievalManager headerRetrievalManager,
            IStaxMaintainableWriterEngine<ICodelistObject> codelistWriterEngine,
            IStaxMaintainableWriterEngine<IConceptSchemeObject> conceptSchemeWriterEngine,
            IStaxMaintainableWriterEngine<IDataflowObject> dataflowWriterEngine,
            IStaxMaintainableWriterEngine<IDataStructureObject> dsdWriterEngine)
        {
            if (!prettyPrint)
            {
                return GetInstance(schemaLocationManager, headerRetrievalManager,
                    codelistWriterEngine, conceptSchemeWriterEngine, dataflowWriterEngine, dsdWriterEngine);
            }
            if (_prettyInstance == null)
            {
                _prettyInstance = new StaxStructureWriterEngineV3();
                _prettyInstance.prettyPrint = true;
                _prettyInstance.schemaLocationManager = schemaLocationManager;
                _prettyInstance.headerRetrievalManager = headerRetrievalManager;

                _prettyInstance.codelistWriterEngine = codelistWriterEngine;
                _prettyInstance.conceptSchemeWriterEngine = conceptSchemeWriterEngine;
                _prettyInstance.dataflowWriterEngine = dataflowWriterEngine;
                _prettyInstance.dsdWriterEngine = dsdWriterEngine;

                IFusionObjectStore.RegisterInstance(_prettyInstance);
            }
            return _prettyInstance;
        }

        public static StaxStructureWriterEngineV3 GetInstance(
            ISchemaLocationManager schemaLocationManager, IHeaderRetrievalManager headerRetrievalManager,
            IStaxMaintainableWriterEngine<ICodelistObject> codelistWriterEngine,
            IStaxMaintainableWriterEngine<IConceptSchemeObject> conceptSchemeWriterEngine,
            IStaxMaintainableWriterEngine<IDataflowObject> dataflowWriterEngine,
            IStaxMaintainableWriterEngine<IDataStructureObject> dsdWriterEngine)
        {
            if (_instance == null)
            {
                _instance = new StaxStructureWriterEngineV3();
                _instance.prettyPrint = false;
                _instance.schemaLocationManager = schemaLocationManager;
                _instance.headerRetrievalManager = headerRetrievalManager;

                _instance.codelistWriterEngine = codelistWriterEngine;
                _instance.conceptSchemeWriterEngine = conceptSchemeWriterEngine;
                _instance.dataflowWriterEngine = dataflowWriterEngine;
                _instance.dsdWriterEngine = dsdWriterEngine;

                IFusionObjectStore.RegisterInstance(_instance);
            }
            return _instance;
        }

        //TODO SDMXRI-1785 will this work?
        public void DestroyInstance()
        {
            _instance = null;
            _prettyInstance = null;
        }

        //Private constructor - lazy load instance
        private StaxStructureWriterEngineV3() : base(true)
        {
            var supportedTypes = new List<SdmxStructureEnumType>
            {
                SdmxStructureEnumType.Categorisation,
                SdmxStructureEnumType.CodeList,
                SdmxStructureEnumType.ConceptScheme,
                SdmxStructureEnumType.Dsd,
                SdmxStructureEnumType.Dataflow,
                SdmxStructureEnumType.MetadataFlow,
                SdmxStructureEnumType.ProvisionAgreement,
                SdmxStructureEnumType.CategoryScheme,
                SdmxStructureEnumType.OrganisationUnitScheme,
                SdmxStructureEnumType.DataProviderScheme,
                SdmxStructureEnumType.AgencyScheme,              
                SdmxStructureEnumType.ContentConstraint,
                SdmxStructureEnumType.DataConsumerScheme,           
                SdmxStructureEnumType.HierarchyV30,
                SdmxStructureEnumType.HierarchyAssociation,
                SdmxStructureEnumType.Msd,
                SdmxStructureEnumType.MetadataProviderScheme

            }.Select(e => SdmxStructureType.GetFromEnum(e)).ToList();

            //        writerEngines.put(SDMX_STRUCTURE_TYPE.AGENCY_SCHEME, StaxAgencySchemeWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.DATA_PROVIDER_SCHEME, StaxDataProviderSchemeWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.DATA_CONSUMER_SCHEME, StaxDataConsumerSchemeWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.ORGANISATION_UNIT_SCHEME, StaxOrgUnitSchemeWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.CATEGORY_SCHEME, StaxCategorySchemeWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.CODE_LIST, StaxCodelistWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.CONCEPT_SCHEME, StaxConceptSchemeWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.CONTENT_CONSTRAINT, StaxContentConstraintWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.ADVANCED_RELEASE_CALENDAR, StaxContentConstraintWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.DSD, StaxDsdWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.DATAFLOW, StaxDataflowWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.MSD, StaxMetadataStructureWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.METADATA_PROVIDER_SCHEME, StaxMetadataProviderSchemeWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.METADATA_FLOW, StaxMetadataflowWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.HIERARCHY, StaxHclWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.HIERARCHY_ASSOCIATION, StaxHierarchyAssociationWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.PROCESS, StaxProcessWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.PROVISION_AGREEMENT, StaxProvisionWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.METADATA_PROVISION, StaxMetadataProvisionWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.REGISTRATION, StaxRegistrationWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.STRUCTURE_MAP, StaxStructureMapWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.REPRESENTATION_MAP, StaxRepresentationMapWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.REPORTING_TAXONOMY, StaxReportingTaxonomyWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.VALUE_LIST, StaxValuelistWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.VTL_CUSTOM_TYPE_SCHEME, StaxCustomTypeSchemeWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.VTL_NAME_PERSONALISATION_SCHEME, StaxNamePersonalisationSchemeWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.VTL_RULESET_SCHEME, StaxRulesetSchemeWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.VTL_TRANSFORMATION_SCHEME, StaxTransformationSchemeWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.VTL_USER_DEFINED_OPERATOR_SCHEME, StaxUserDefinedOperatorSchemeWriterEngineV3.Instance);
            //        writerEngines.put(SDMX_STRUCTURE_TYPE.VTL_MAPPING_SCHEME, StaxVtlMappingSchemeWriterEngineV3.Instance);

            CreateSupportedTypes(true, supportedTypes);
        }

        public void WriteStructure(IMaintainableObject sdmxObject, IExternalMaintainableLinks additionalLinks, Stream outputStream)
        {
            StructureWriterUtil.WriteStructure(this, sdmxObject, additionalLinks, outputStream);
        }

#pragma warning disable CS0649  // Disable unchecked warning
        protected void WriteRegistrations(StaxWriter writer, ISdmxObjects objects, Stream outputStream)
        {
            ISet<IRegistrationObject> registrations = objects.Registrations;
            writer.StartDocument(messageNs, "RegistryInterface");
            WriteHeader(writer, objects.Header, true);
            writer.WriteStartElement(messageNs, "QueryRegistrationResponse");


            IStaxMaintainableWriterEngine<IRegistrationObject> writerEngine = null;//TODO create StaxRegistrationWriterEngineV3.Instance;
            writer.WriteStartElement(registryNs, "StatusMessage");
            writer.WriteAttribute("status", "Success");
            writer.WriteEndElement();


            foreach (IRegistrationObject registration in registrations)
            {
                writer.WriteStartElement(registryNs, "QueryResult");
                writer.WriteAttribute("timeSeriesMatch", "false");  //TODO What is this?
                writer.WriteStartElement(registryNs, "DataResult");
                writerEngine.WriteMaintainable(registration, null, writer);
                writer.WriteEndElement(); //End Data Result
                writer.WriteEndElement(); //End Query Result
            }
            writer.WriteEndElement(); //End QueryRegistrationResponse
        }
#pragma warning restore CS0649  // Restore unchecked warning

        private string GetSchemaLocation()
        {
            string messageSchema = messageNs.GetNamespaceURL();
            string schemaName = SdmxConstants.GetSchemaName(messageSchema);
            if (schemaLocationManager != null)
            {
                return messageSchema + " " + schemaLocationManager.GetSchemaLocation(_ouputVersion) + schemaName;
            }
            return null;
        }

        protected void WriteStructuresInternal(StaxWriter writer, ISdmxObjects objects, IDictionary<string, IExternalMaintainableLinks> additionalLinks, Stream outStream)
        {
            writer.StartDocument(messageNs, "Structure");
            WriteHeader(writer, objects.Header, true);
            writer.WriteStartElement(messageNs, "Structures");
            WriteStructures(writer, objects, additionalLinks);
        }

        protected void WriteHeader(StaxWriter writer, IHeader header, bool includeReceiver)
        {
            if (header == null && headerRetrievalManager != null)
            {
                header = headerRetrievalManager.Header;
            }
            if (header == null)
            {
                header = new HeaderImpl("ZZZ");
            }
            //NOTE SDMXSOURCE_SDMXCORE CHANGE: Used v3 writer (bug in sdmxcore?)
            StaxHeaderWriterUtilV3.WriteHeader(writer, messageNs, header, includeReceiver, false);
        }

        public override bool IsSupportedType(IMaintainableObject structure)
        {
            /* TODO SDMXCORE
            if(structure.DefaultStandard != SdmxMetadataStandard.Instance) {
                return false;
            }
            */
            return base.IsSupportedType(structure);
        }

        #region write structures overloads

        /// <summary> 
        /// Writes structures, expects the document to have been started and relevant namespaces to have been added 
        /// </summary>
        /// <param name=" writer"> 
        /// </param>
        /// <param name=" objects"> 
        /// </param>
        /// <param name=" additionalLinks"> 
        /// </param> 
        private void WriteStructures(StaxWriter writer, ISdmxObjects objects, IDictionary<string, IExternalMaintainableLinks> additionalLinks)
        {
            ISet<ICodelistObject> standardCodelists = new HashSet<ICodelistObject>();           
            if (ObjectUtil.ValidCollection(objects.Codelists))
            {
                foreach (ICodelistObject cl in objects.Codelists)
                {
                    //TODO SDMXCORE GEO
                    //if (cl.isGeoCodelist())
                    //{
                    //if (cl is IGeographicCodelistObject)
                    //{
                    //    geographicalCodelists.Add((IGeographicCodelistObject)cl);
                    //}
                    //if (cl is IGeoGridCodelistObject)
                    //{
                    //    geoCodelists.Add((IGeoGridCodelistObject)cl);
                    //}
                    //else
                    //{
                    //    standardCodelists.Add(cl);
                    //}

                    standardCodelists.Add(cl);
                }                 
            
            }

            ISet<IGeographicCodelistObject> geographicalCodelists = new HashSet<IGeographicCodelistObject>();
            if (ObjectUtil.ValidCollection(objects.GeographicCodelists))
            {
                foreach (IGeographicCodelistObject item in objects.GeographicCodelists)
                {
                    geographicalCodelists.Add(item);
                }
            }

            ISet<IGeoGridCodelistObject> geoGridlCodelists = new HashSet<IGeoGridCodelistObject>();
            if (ObjectUtil.ValidCollection(objects.GeoGridCodelists))
            {
                foreach (IGeoGridCodelistObject item in objects.GeoGridCodelists)
                {
                    geoGridlCodelists.Add(item);
                }
            }

            WriteStructures(writer, "AgencySchemes", objects.AgenciesSchemes, StaxAgencySchemeWriterEngineV3.Instance, additionalLinks);
            WriteStructures(writer, "Categorisations", objects.Categorisations, StaxCategorisationWriterEngineV3.Instance, additionalLinks);
            //        //CategorySchemeMaps
            WriteStructures(writer, "CategorySchemes", objects.CategorySchemes, StaxCategorySchemeWriterEngineV3.Instance, additionalLinks);
            WriteStructures(writer, "Codelists", standardCodelists, StaxCodelistWriterEngineV3.Instance, additionalLinks);
            //        //ConceptSchemeMaps
            WriteStructures(writer, "ConceptSchemes", objects.ConceptSchemes, conceptSchemeWriterEngine, additionalLinks);
            //        WriteStructures(writer, "CustomTypeSchemes", objects, SDMX_STRUCTURE_TYPE.VTL_CUSTOM_TYPE_SCHEME, additionalLinks);
            WriteStructures(writer, "DataConstraints", objects.ContentConstraintObjects, StaxContentConstraintWriterEngineV3.Instance, additionalLinks);
            WriteStructures(writer, "DataConsumerSchemes", objects.DataConsumerSchemes, StaxDataConsumerSchemeWriterEngineV3.Instance, additionalLinks);
            WriteStructures(writer, "Dataflows", objects.Dataflows, dataflowWriterEngine, additionalLinks);
            WriteStructures(writer, "DataProviderSchemes", objects.DataProviderSchemes, StaxDataProviderSchemeWriterEngineV3.Instance, additionalLinks);
            WriteStructures(writer, "DataStructures", objects.DataStructures, dsdWriterEngine, additionalLinks);
            WriteStructures(writer, "GeographicCodelists", geographicalCodelists, StaxGeographicCodelistWriterEngineV3.Instance, additionalLinks);
            WriteStructures(writer, "GeoGridCodelists", geoGridlCodelists, StaxGeoCodelistWriterEngineV3.Instance, additionalLinks);  //TODO Other writers            
            WriteStructures(writer, "Hierarchies", objects.Hierarchies, StaxHclWriterEngineV3.Instance, additionalLinks);
            WriteStructures(writer, "HierarchyAssociations", objects.HierarchyAssociations, StaxHierarchyAssociationWriterEngineV3.Instance, additionalLinks);
            //        //MetadataConstraints
            WriteStructures(writer, "Metadataflows", objects.Metadataflows, StaxMetadataflowWriterEngineV3.Instance, additionalLinks);
            WriteStructures(writer, "MetadataProviderSchemes", objects.MetadataProviderSchemes, StaxMetadataProviderSchemeWriterEngineV3.Instance, additionalLinks);
            //        WriteStructures(writer, "MetadataProvisionAgreements", objects, SDMX_STRUCTURE_TYPE.METADATA_PROVISION, additionalLinks);
            WriteStructures(writer, "MetadataStructures", objects.MetadataStructures, StaxMetadataStructureWriterEngineV3.Instance, additionalLinks);
            //        WriteStructures(writer, "NamePersonalisationSchemes", objects, SDMX_STRUCTURE_TYPE.VTL_NAME_PERSONALISATION_SCHEME, additionalLinks);
            //        //OrganisationSchemeMaps
            WriteStructures(writer, "OrganisationUnitSchemes", objects.OrganisationUnitSchemes, StaxOrgUnitSchemeWriterEngineV3.Instance, additionalLinks);
            //        WriteStructures(writer, "Processes", objects, SDMX_STRUCTURE_TYPE.PROCESS, additionalLinks);
            WriteStructures(writer, "ProvisionAgreements", objects.ProvisionAgreements, StaxProvisionWriterEngineV3.Instance, additionalLinks);
            //        WriteStructures(writer, "ReportingTaxonomies", objects, SDMX_STRUCTURE_TYPE.REPORTING_TAXONOMY, additionalLinks);
            //        //ReportingTaxonomyMaps
            //        WriteStructures(writer, "RepresentationMaps", objects, SDMX_STRUCTURE_TYPE.REPRESENTATION_MAP, additionalLinks);
            //        WriteStructures(writer, "RulesetSchemes", objects, SDMX_STRUCTURE_TYPE.VTL_RULESET_SCHEME, additionalLinks);
            //        WriteStructures(writer, "StructureMaps", objects, SDMX_STRUCTURE_TYPE.STRUCTURE_MAP, additionalLinks);
            //        WriteStructures(writer, "TransformationSchemes", objects, SDMX_STRUCTURE_TYPE.VTL_TRANSFORMATION_SCHEME, additionalLinks);
            //        WriteStructures(writer, "UserDefinedOperatorSchemes", objects, SDMX_STRUCTURE_TYPE.VTL_USER_DEFINED_OPERATOR_SCHEME, additionalLinks);
            //
            //        WriteStructures(writer, "ValueLists", objects, SDMX_STRUCTURE_TYPE.VALUE_LIST, additionalLinks);
            //        WriteStructures(writer, "VtlMappingSchemes", objects, SDMX_STRUCTURE_TYPE.VTL_MAPPING_SCHEME, additionalLinks);
        }

        public void WriteStructures(ISdmxObjects objects, IDictionary<string, IExternalMaintainableLinks> additionalLinks, Stream outputStream)
        {
            StaxWriter writer = null;
            try
            {
                //NOTE SDMXSOURCE_SDMXCORE CHANGE
                if (objects.HasRegistrations())
                {
                    //TODO REGISTRATIONS
                    writer = new StaxWriter(outputStream, GetSchemaLocation(), prettyPrint, messageNs, registryNs, commonNs);
                    WriteRegistrations(writer, objects, outputStream);
                }
                else
                {
                    writer = new StaxWriter(outputStream, GetSchemaLocation(), prettyPrint, messageNs, structureNs, commonNs);
                    WriteStructuresInternal(writer, objects, additionalLinks, outputStream);
                }
            }
            finally
            {
                if (writer != null)
                {
                    writer.Close();
                }
            }
        }

#pragma warning disable CS0649  // Disable unchecked warning
        private void WriteStructures<T>(StaxWriter writer, string startNode, ISet<T> maints,
            IStaxMaintainableWriterEngine<T> writerEngine, IDictionary<string, IExternalMaintainableLinks> additionalLinks)
            where T : IMaintainableObject
        {
            if (additionalLinks == null)
            {
                additionalLinks = new Dictionary<string, IExternalMaintainableLinks>();
            }
            if (writerEngine != null && ObjectUtil.ValidCollection(maints))
            {
                writer.WriteStartElement(structureNs, startNode);
                foreach (T maint in maints)
                {
                    /*if (structureType == SDMX_STRUCTURE_TYPE.CODE_LIST) {
                        maint = ItemValidityPeriodHelper.PotentiallyMutateIt((ICodelistObject)maint);
                    } else if (structureType == SdmxStructureEnumType.ConceptScheme) {
                        maint = ItemValidityPeriodHelper.PotentiallyMutateIt((IConceptSchemeObject)maint);
                    }*/
                    //NOTE SDMXCORE validity is sdmx-core's custom functionality, don't need it at SdmxSource
                    /*
                    if (maint is IValidatableObject) {
                        maint = ItemValidityPeriodHelper.PotentiallyMutateIt((IValidatableObject<T>) maint);
                    }
                    */
                    var additionalLinksForMaint = additionalLinks.ContainsKey(maint.Urn.AbsoluteUri) ? additionalLinks[maint.Urn.AbsoluteUri] : null;
                    writerEngine.WriteMaintainable(maint, additionalLinksForMaint, writer);
                }
                writer.WriteEndElement();
            }
        }
#pragma warning restore CS0649  // Restore unchecked warning

        #endregion
    }
}