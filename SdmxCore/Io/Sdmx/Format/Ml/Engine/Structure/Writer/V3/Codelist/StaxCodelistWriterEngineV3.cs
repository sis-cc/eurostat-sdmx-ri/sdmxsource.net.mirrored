using System.Collections.Generic;
using Io.Sdmx.Api.Singleton;
using Io.Sdmx.Core.Sdmx.Api.Model.Structure;
using Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Util;
using Io.Sdmx.Utils.Core.Application;
using Io.Sdmx.Utils.Core.Xml;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Codelist;

namespace Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Codelist
{
    public class StaxCodelistWriterEngineV3 : AbstractV3Writer<ICodelistObject>, IFusionSingleton
    {
        private static StaxCodelistWriterEngineV3 _instance;

        public static StaxCodelistWriterEngineV3 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StaxCodelistWriterEngineV3();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }

        protected override string RootNodeName => "Codelist";

        public void DestroyInstance()
        {
            _instance = null;
        }

        protected override void WriteMaintainableInternal(ICodelistObject maint, IExternalMaintainableLinks externalLinks, StaxWriter writer)
        {
            WriteCodes(maint.Items, externalLinks, writer);       
        }

        private void WriteCodes(IList<ICode> codes, IExternalMaintainableLinks externalLinks, StaxWriter writer)
        {
            foreach (CodeCore code in codes)
            {
                StaxNameableWriterUtilV3.WriteObject(code, "Code", externalLinks, writer);
                if (code.ParentCode != null)
                {
                    StaxRefUtilV3.WriteLocalReference(code.ParentCode, writer, "Parent");
                }
                writer.WriteEndElement();
            }
        }
       
    }
}
