// -----------------------------------------------------------------------
// <copyright file="StaxCategorySchemeWriterEngineV3.cs" company="EUROSTAT">
//   Date Created : 2023-06-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Io.Sdmx.Api.Singleton;
using Io.Sdmx.Core.Sdmx.Api.Model.Structure;
using Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Util;
using Io.Sdmx.Utils.Core.Application;
using Io.Sdmx.Utils.Core.Xml;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;

namespace Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.CategoryScheme
{
    public class StaxCategorySchemeWriterEngineV3 : AbstractV3Writer<ICategorySchemeObject>, IFusionSingleton
    {
        private static StaxCategorySchemeWriterEngineV3 _instance;
     
        public static StaxCategorySchemeWriterEngineV3 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StaxCategorySchemeWriterEngineV3();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }

        protected override string RootNodeName => "CategoryScheme";

        public void DestroyInstance()
        {
            _instance = null;
        }    

        protected override void WriteMaintainableInternal(ICategorySchemeObject maint, IExternalMaintainableLinks externalLinks, StaxWriter writer)
        {
            WriteCategories((List<ICategoryObject>)maint.Items, externalLinks, writer);
        }

        private void WriteCategories(List<ICategoryObject> categories, IExternalMaintainableLinks externalLinks, StaxWriter writer)
        {
            foreach (ICategoryObject cat in categories)
            {
                StaxNameableWriterUtilV3.WriteObject(cat, "Category", externalLinks, writer); 
                WriteCategories((List<ICategoryObject>)cat.Items, externalLinks, writer);
                writer.WriteEndElement();
            }
        }
    }
}
