// -----------------------------------------------------------------------
// <copyright file="StaxHeaderWriterUtilV3.cs" company="EUROSTAT">
//   Date Created : 2023-11-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Io.Sdmx.Api.Singleton;
using Io.Sdmx.Core.Sdmx.Api.Model.Structure;
using Org.Sdmxsource.Sdmx.Api.Model.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
using Io.Sdmx.Utils.Core.Xml;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Io.Sdmx.Utils.Core.Application;

namespace SdmxCore.Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Constraint
{
    public class StaxContentConstraintWriterEngineV3 : StaxAbstractConstraintWriterEngine<IContentConstraintObject>, IFusionSingleton
    {
        private static StaxContentConstraintWriterEngineV3 _instance;

        // Private constructor - lazy load instance
        private StaxContentConstraintWriterEngineV3() { }

        public static StaxContentConstraintWriterEngineV3 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StaxContentConstraintWriterEngineV3();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }

        public void DestroyInstance()
        {
            _instance = null;
        }

        protected override string RootNodeName
        {
            get
            {
                return "DataConstraint";
            }
        }

        protected override void WriteMaintainableInternal(IContentConstraintObject maint, IExternalMaintainableLinks externalLinks, StaxWriter writer)
        {
            base.WriteMaintainableInternal(maint, externalLinks, writer);
            WriteCubeRegion(maint.IncludedCubeRegion, true, writer);
            WriteCubeRegion(maint.ExcludedCubeRegion, false, writer);
            WriteReleaseCalendar(maint.ReleaseCalendar, writer);
        }

        protected override IDictionary<string, string> GetAdditionalMaintainableAttributes(IContentConstraintObject maint)
        {
            Dictionary<string, string> returnMap = new Dictionary<string, string>();
            returnMap["role"] = maint.IsDefiningActualDataPresent ? "Actual" : "Allowed";
            return returnMap;
        }

        private void WriteReleaseCalendar(IReleaseCalendar relCal, StaxWriter writer)
        {
            if (relCal != null)
            {
                writer.WriteStartElement(Ns, "ReleaseCalendar");
                writer.WriteElement(Ns, "Periodicity", relCal.Periodicity);
                writer.WriteElement(Ns, "Offset", relCal.Offset);
                writer.WriteElement(Ns, "Tolerance", relCal.Tolerance);
                writer.WriteEndElement(); // End ReleaseCalendar
            }
        }

        private void WriteCubeRegion(ICubeRegion cubeRegion, bool isIncluded, StaxWriter writer)
        {
            if (cubeRegion != null)
            {
                writer.WriteStartElement(Ns, "CubeRegion");
                writer.WriteAttribute("include", isIncluded.ToString().ToLower());
                WriteKeyValues(cubeRegion.KeyValues, "KeyValue", writer);
                WriteKeyValues(cubeRegion.AttributeValues, "Component", writer);
                writer.WriteEndElement(); // End CubeRegion
            }
        }

        private void WriteKeyValues(IList<IKeyValues> kvsList, string nodeName, StaxWriter writer)
        {
            foreach (var kvs in kvsList)
            {
                WriteStartKeyValues(kvs, nodeName, writer);
                WriteTimeRange(kvs.TimeRange, writer);
                writer.WriteEndElement(); // End KeyValue
            }
        }

        private void WriteStartKeyValues(IKeyValues kvs, string nodeName, StaxWriter writer)
        {
            writer.WriteStartElement(Ns, nodeName);
            writer.WriteAttribute("id", kvs.Id);
            if (kvs.IsRemovePrefix)
            {
                writer.WriteAttribute("removePrefix", kvs.IsRemovePrefix.ToString().ToLower());
            }
            foreach (string val in kvs.Values)
            {
                WriteKeyValues(val, kvs.IsCascadeValue(val) ? "true" : null, kvs.GetValidityPeriod(val), nodeName, writer);
            }
            foreach (string val in kvs.CascadeValues)
            {
                if (!kvs.Values.Contains(val))
                {
                    WriteKeyValues(val, kvs.IsCascadeValue(val) ? "excluderoot" : null, kvs.GetValidityPeriod(val), nodeName, writer);
                }
            }
        }

        private void WriteKeyValues(string val, string cascde, ISdmxPeriod period, string nodeName, StaxWriter writer)
        {
            writer.WriteStartElement(Ns, "Value");
            if (cascde != null)
            {
                writer.WriteAttribute("cascadeValues", cascde);
            }
            if (period != null)
            {
                if (period.HasStartPeriod())
                {
                    writer.WriteAttribute("validFrom", period.GetStartPeriod().DateInSdmxFormat);
                }
                if (period.HasEndPeriod())
                {
                    writer.WriteAttribute("validTo", period.GetEndPeriod().DateInSdmxFormat);
                }
            }
            writer.WriteElementText(val);
            writer.WriteEndElement(); // End Value
        }

        private void WriteTimeRange(ITimeRange timeRange, StaxWriter writer)
        {
            if (timeRange != null)
            {
                writer.WriteStartElement(Ns, "TimeRange");
                if (timeRange.StartDate != null)
                {
                    string startNodeName = timeRange.Range ? "StartPeriod" : "BeforePeriod";
                    writer.WriteStartElement(Ns,startNodeName);
                    writer.WriteAttribute("isInclusive", System.Xml.XmlConvert.ToString(timeRange.StartInclusive));
                    writer.WriteElementText(timeRange.StartDate.DateInSdmxFormat);
                    writer.WriteEndElement(); // End Start|BeforePeriod
                }
                if (timeRange.EndDate != null)
                {
                    string endNodeName = timeRange.Range ? "EndPeriod" : "AfterPeriod";
                    writer.WriteStartElement(Ns, endNodeName);
                    writer.WriteAttribute("isInclusive", System.Xml.XmlConvert.ToString(timeRange.EndInclusive));
                    writer.WriteElementText(timeRange.EndDate.DateInSdmxFormat);
                    writer.WriteEndElement(); // End Start|BeforePeriod
                }
                writer.WriteEndElement(); // End TimeRange
            }
        }
    }
}
