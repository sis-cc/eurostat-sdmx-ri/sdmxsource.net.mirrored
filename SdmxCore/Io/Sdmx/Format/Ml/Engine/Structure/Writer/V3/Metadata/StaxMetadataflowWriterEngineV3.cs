namespace Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Msd
{
    using Io.Sdmx.Api.Singleton;
    using Io.Sdmx.Core.Sdmx.Api.Model.Structure;
    using Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3;
    using Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Util;
    using Io.Sdmx.Utils.Core.Application;
    using Io.Sdmx.Utils.Core.Xml;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    public class StaxMetadataflowWriterEngineV3 : AbstractV3Writer<IMetadataFlow>, IFusionSingleton
    {
        private static StaxMetadataflowWriterEngineV3 _instance;

        private StaxMetadataflowWriterEngineV3() { }

        public static StaxMetadataflowWriterEngineV3 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StaxMetadataflowWriterEngineV3();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }

        public void DestroyInstance()
        {
            _instance = null;
        }

        protected override string RootNodeName => "Metadataflow";

        protected override void WriteMaintainableInternal(IMetadataFlow maint, IExternalMaintainableLinks externalLinks, StaxWriter writer)
        {
            StaxRefUtilV3.WriteReference(maint.MetadataStructureRef, writer, "Structure");
            foreach (IStructureReference target in maint.Targets)
            {
                StaxRefUtilV3.WriteReference(target, writer, "Target");
            }
        }
    }

}