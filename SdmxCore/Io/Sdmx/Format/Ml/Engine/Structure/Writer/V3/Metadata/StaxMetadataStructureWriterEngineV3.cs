// -----------------------------------------------------------------------
// <copyright file="StaxMetadataStructureWriterEngineV3.cs" company="EUROSTAT">
//   Date Created : 2024-03-18
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace SdmxCore.Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Metadata
{
    using System.Collections.Generic;
    using global::Io.Sdmx.Api.Singleton;
    using global::Io.Sdmx.Core.Sdmx.Api.Model.Structure;
    using global::Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Util;
    using global::Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3;
    using global::Io.Sdmx.Utils.Core.Xml;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using global::Io.Sdmx.Utils.Core.Application;
    using SdmxCore.Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Hcl;
    using SdmxCore.Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Util;

    public class StaxMetadataStructureWriterEngineV3 : AbstractV3Writer<IMetadataStructureDefinitionObject>, IFusionSingleton
    {
        private static StaxMetadataStructureWriterEngineV3 _instance;

        //Private constructor - lazy load instance
        private StaxMetadataStructureWriterEngineV3() { }
        public static StaxMetadataStructureWriterEngineV3 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StaxMetadataStructureWriterEngineV3();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }

        public void DestroyInstance()
        {
            _instance = null;
        }

        protected override string RootNodeName => "MetadataStructure";

        protected override void WriteMaintainableInternal(IMetadataStructureDefinitionObject maint, IExternalMaintainableLinks externalLinks, StaxWriter writer)
        {
            writer.WriteStartElement(Ns, "MetadataStructureComponents");
            writer.WriteStartElement(Ns, "MetadataAttributeList");
            WriteMetadataAttributes(maint, externalLinks, writer);
            writer.WriteEndElement();  // END MetadataAttributeList
            writer.WriteEndElement();  // END MetadataStructureComponents
        }

        private void WriteMetadataAttributes(IMetadataAttributeContainer attributeContainer, IExternalMaintainableLinks externalLinks, StaxWriter writer)
        {
            foreach (var metadataAttribute in attributeContainer.MetadataAttributes)
            {
                writer.WriteStartElement(Ns, "MetadataAttribute");

                IDictionary<string, string> attributes = StaxIdentifiableWriterUtilV3.GetAttributes(metadataAttribute);
                if (metadataAttribute.Presentational.IsTrue)
                {
                    attributes["isPresentational"] = "true";
                }
                attributes["maxOccurs"] = "unbounded";

                var representation = metadataAttribute.Representation;
                if (representation != null)
                {
                    if (representation.MinOccurs != null && representation.MinOccurs >= 0)
                    {
                        attributes["minOccurs"] = metadataAttribute.MinOccurs.ToString();
                    }
                    var maxOccurs = representation.MaxOccurs;
                    if (maxOccurs != null && maxOccurs.Occurrences >= 1 && maxOccurs.Occurrences < int.MaxValue)
                    {
                        attributes["maxOccurs"] = metadataAttribute.MaxOccurs.ToString();
                    }
                }
                writer.WriteAttributes(attributes);
                StaxAnnotableWriterUtilV3.WriteObject(metadataAttribute, writer);
                StaxRefUtilV3.WriteReference(metadataAttribute.ConceptRef, writer, "ConceptIdentity");
                if (representation != null)
                {
                    if (representation.TextFormat != null || representation.Representation != null)
                    {
                        StaxRepresentationWriterUtilV3.WriteRepresentation(metadataAttribute.Representation, writer, "LocalRepresentation", true, true);
                    }
                }
                WriteMetadataAttributes(metadataAttribute, externalLinks, writer);

                writer.WriteEndElement(); // End MetadataAttribute
            }
        }
    }

}
