// -----------------------------------------------------------------------
// <copyright file="StaxMaintainableWriterUtilV3.cs" company="EUROSTAT">
//   Date Created : 2023-06-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Util
{
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Io.Sdmx.Core.Sdmx.Api.Model.Structure;
    using Io.Sdmx.Format.Ml.Constant;
    using Io.Sdmx.Utils.Core.Xml;
    using Org.Sdmxsource.Util.Extensions;
    using System.Collections.Generic;

    public class StaxMaintainableWriterUtilV3
    {
        public static void WriteObject(IMaintainableObject maint, string elementName, IExternalMaintainableLinks externalLinks, StaxWriter writer)
        {
            WriteObject(maint, elementName, externalLinks, writer, null);
        }

        public static void WriteObject(IMaintainableObject maint, string elementName, IExternalMaintainableLinks externalLinks, 
            StaxWriter writer, IDictionary<string, string> attributes)
        {
            if (attributes != null)
            {
                attributes.PutAll(StaxIdentifiableWriterUtilV3.GetAttributes(maint));
            }
            else
            {
                attributes = StaxIdentifiableWriterUtilV3.GetAttributes(maint);
            }
            //TODO SDMXRI-2080 fix this when implement item scheme writer
            //if (maint is IItemSchemeObject)
            //{
            //    IItemSchemeObject itemScheme = (IItemSchemeObject)maint;
            //    if (itemScheme.IsPartial())
            //    {
            //        attributes.Put("isPartial", bool.ToString(itemScheme.IsPartial()));
            //    }
            //}
            if (!maint.IsFixedVersion)
            {
                attributes.Put("version", maint.Version);
            }
            attributes.Put("agencyID", maint.AgencyId);

            // Always write the values of isFinal and isExternalReference
            attributes.Put("isExternalReference", maint.IsExternalReference.IsTrue.ToString().ToLower());

            //NOTE SDMXCORE validity is sdmx-core's custom functionality, don't need it at SdmxSource
            /*
            if(maint.HasValidityPeriod()) {
                if(maint.ValidityPeriod.HasStartPeriod()) {
                    attributes.Put("validFrom", maint.ValidityPeriod.StartPeriod.DateInSdmxFormat);
                }
                if(maint.ValidityPeriod.HasEndPeriod()) {
                    attributes.Put("validTo", maint.ValidityPeriod.EndPeriod.DateInSdmxFormat);
                }
            }
            */
            if (maint.StructureUrl != null)
            {
                attributes.Put("structureURL", maint.StructureUrl.ToString());
            }
            if (maint.ServiceUrl != null)
            {
                attributes.Put("serviceURL", maint.ServiceUrl.ToString());
            }
            writer.WriteElement(SdmxNamespace.GetFromEnum(SdmxNamespaceEnumType.Structure3).Namespace, elementName, attributes);
            StaxNameableWriterUtilV3.WriteObject(maint, externalLinks, writer);
        }
    }
}