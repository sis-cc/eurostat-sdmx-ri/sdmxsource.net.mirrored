// -----------------------------------------------------------------------
// <copyright file="StaxRepresentationWriterUtilV3.cs" company="EUROSTAT">
//   Date Created : 2024-03-18
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using Io.Sdmx.Format.Ml.Constant;
using Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Util;
using Io.Sdmx.Utils.Core.Xml;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace SdmxCore.Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Util
{
    public static class StaxRepresentationWriterUtilV3
    {
        public static void WriteRepresentation(IRepresentation representation, StaxWriter writer, string elementName)
        {
            WriteRepresentation(representation, writer, elementName, false, true);
        }

        public static void WriteRepresentation(IRepresentation representation, StaxWriter writer, string elementName, bool excludeMinMax, bool includeMultiLingual)
        {
            if (representation != null)
            {
                writer.WriteStartElement(SdmxNamespace.GetFromEnum(SdmxNamespaceEnumType.Structure3).Namespace, elementName);
                if (!excludeMinMax)
                {
                    if (representation.MinOccurs != null)
                    {
                        writer.WriteAttribute("minOccurs", representation.MinOccurs.ToString());
                    }
                    if (representation.MaxOccurs != null)
                    {
                        writer.WriteAttribute("maxOccurs", representation.MaxOccurs.ToString());
                    }
                }
                if (representation.Representation == null)
                {
                    StaxTextFormatWriterUtilV3.WriteTextFormat(representation.TextFormat, writer, "TextFormat", includeMultiLingual);
                }
                else
                {
                    StaxRefUtilV3.WriteReference(representation.Representation, writer, "Enumeration");
                    StaxTextFormatWriterUtilV3.WriteTextFormat(representation.TextFormat, writer, "EnumerationFormat", includeMultiLingual);
                }

                writer.WriteEndElement();
            }
        }
    }

}
