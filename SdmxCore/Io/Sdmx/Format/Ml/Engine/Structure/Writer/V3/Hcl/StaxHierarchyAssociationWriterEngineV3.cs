// -----------------------------------------------------------------------
// <copyright file="StaxHclWriterEngineV3.cs" company="EUROSTAT">
//   Date Created : 2024-03-15
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace SdmxCore.Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Hcl
{
    using System;
    using global::Io.Sdmx.Api.Singleton;
    using global::Io.Sdmx.Core.Sdmx.Api.Model.Structure;
    using global::Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Util;
    using global::Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3;
    using global::Io.Sdmx.Utils.Core.Xml;
    using global::Io.Sdmx.Utils.Core.Application;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;

    public class StaxHierarchyAssociationWriterEngineV3 : AbstractV3Writer<IHierarchyAssociation>, IFusionSingleton
    {
        private static StaxHierarchyAssociationWriterEngineV3 _instance;

        //Private constructor - lazy load instance
        private StaxHierarchyAssociationWriterEngineV3() { }
        public static StaxHierarchyAssociationWriterEngineV3 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StaxHierarchyAssociationWriterEngineV3();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }

        public void DestroyInstance()
        {
            _instance = null;
        }

        protected override string RootNodeName => "HierarchyAssociation";

        protected override void WriteMaintainableInternal(IHierarchyAssociation maint, IExternalMaintainableLinks externalLinks, StaxWriter writer)
        {
            StaxRefUtilV3.WriteReference(maint.HierarchyRef, writer, "LinkedHierarchy");
            StaxRefUtilV3.WriteReference(maint.LinkedStructureRef, writer, "LinkedObject");
            if (maint.ContextStructureRef != null)
            {
                StaxRefUtilV3.WriteReference(maint.ContextStructureRef, writer, "ContextObject");
            }
        }
    }

}
