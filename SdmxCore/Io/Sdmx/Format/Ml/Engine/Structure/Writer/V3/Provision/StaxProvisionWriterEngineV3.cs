// -----------------------------------------------------------------------
// <copyright file="StaxProvisionWriterEngineV3.cs" company="EUROSTAT">
//   Date Created : 2023-06-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using Io.Sdmx.Api.Singleton;
using Io.Sdmx.Core.Sdmx.Api.Model.Structure;
using Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Util;
using Io.Sdmx.Utils.Core.Application;
using Io.Sdmx.Utils.Core.Xml;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;

namespace Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Provision
{
    public class StaxProvisionWriterEngineV3 : AbstractV3Writer<IProvisionAgreementObject>, IFusionSingleton
    {      
        private static StaxProvisionWriterEngineV3 _instance;

        private StaxProvisionWriterEngineV3(){}
      
        public static StaxProvisionWriterEngineV3 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StaxProvisionWriterEngineV3();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }

        public void DestroyInstance()
        {
            _instance = null;
        }

        protected override string RootNodeName => "ProvisionAgreement";

        protected override void WriteMaintainableInternal(IProvisionAgreementObject maint, IExternalMaintainableLinks externalLinks, StaxWriter writer)
        {
            StaxRefUtilV3.WriteReference(maint.StructureUseage, writer, "Dataflow");
            StaxRefUtilV3.WriteReference(maint.DataproviderRef, writer, "DataProvider");
        }

        
    }
}
