// -----------------------------------------------------------------------
// <copyright file="StaxMetadataProviderSchemeWriterEngineV3.cs" company="EUROSTAT">
//   Date Created : 2024-04-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
using Io.Sdmx.Api.Singleton;
using Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.OrgScheme;
using Io.Sdmx.Utils.Core.Application;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace SdmxCore.Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.OrgScheme
{
    public class StaxMetadataProviderSchemeWriterEngineV3 : StaxAbstractOrganisationSchemeWriterEngine<IMetadataProviderScheme, IMetadataProvider>, IFusionSingleton
    {
        private static StaxMetadataProviderSchemeWriterEngineV3 _instance;

        //Private constructor - lazy load instance
        private StaxMetadataProviderSchemeWriterEngineV3() { }
        public static StaxMetadataProviderSchemeWriterEngineV3 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StaxMetadataProviderSchemeWriterEngineV3();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }

        public void DestroyInstance()
        {
            _instance = null;
        }



        protected override string RootNodeName => "MetadataProviderScheme";

        protected override string OrgNodeName => "MetadataProvider";
    }

}
