// -----------------------------------------------------------------------
// <copyright file="StaxHeaderWriterUtilV3.cs" company="EUROSTAT">
//   Date Created : 2023-06-09
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using global::Io.Sdmx.Core.Sdmx.Api.Model.Structure;
using global::Io.Sdmx.Format.Ml.Constant;
using global::Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Util;
using global::Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
using global::Io.Sdmx.Utils.Core.Xml;
using Org.Sdmxsource.Sdmx.Api.Constants;
using System;

namespace SdmxCore.Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Constraint
{
    public abstract class StaxAbstractConstraintWriterEngine<T> : AbstractV3Writer<T> where T : IConstraintObject
        {
            protected override void WriteMaintainableInternal(T maint, IExternalMaintainableLinks externalLinks, StaxWriter writer)
            {
                WriteConstraintAttachement(maint.ConstraintAttachment, writer);
                WriteKeySet(maint.IncludedSeriesKeys, "DataKeySet", true, writer);
                WriteKeySet(maint.ExcludedSeriesKeys, "DataKeySet", false, writer);
            }

            private void WriteConstraintAttachement(IConstraintAttachment constraintAttachment, StaxWriter writer)
            {
                if (constraintAttachment != null)
                {
                    writer.WriteStartElement(Ns,"ConstraintAttachment");

                    if (constraintAttachment.DataOrMetadataSetReference != null)
                    {
                        var dsRef = constraintAttachment.DataOrMetadataSetReference;
                        string element = dsRef.IsDataSetReference ? "DataSet" : "MetadataSet";

                        writer.WriteStartElement(Ns, element);
                        writer.WriteStartElement(StaxStructureWriterUtil.CommonNs, "DataProvider");
                        StaxRefUtilV3.WriteReference(dsRef.DataSetReference, writer);
                        writer.WriteEndElement(); // End DataProvider
                        writer.WriteElement(StaxStructureWriterUtil.CommonNs, "ID", dsRef.SetId);
                        writer.WriteEndElement(); // End DataSet
                    }

                    if (constraintAttachment.DataSources != null)
                    {
                        foreach (var ds in constraintAttachment.DataSources)
                        {
                        if (ds.SimpleDatasource)
                        {
                            writer.WriteElement(Ns, "SimpleDataSource", ds.DataUrl.ToString());
                        }
                        else
                        {
                            writer.WriteStartElement(Ns, "QueryableDataSource");
                            writer.WriteAttribute("isRESTDatasource", ds.RESTDatasource.ToString());
                            writer.WriteAttribute("isWebServiceDatasource", ds.WebServiceDatasource.ToString());
                            writer.WriteElement(Ns, "DataURL", ds.DataUrl.ToString());
                            writer.WriteEndElement(); // End QueryableDataSource
                        }
                    }
                    }

                    foreach (var xsRef in constraintAttachment.StructureReference)
                    {
                        string nodeName;
                        switch (xsRef.TargetReference.EnumType)
                        {
                            case SdmxStructureEnumType.DataProvider:
                                nodeName = "DataProvider";
                                break;
                            case SdmxStructureEnumType.Dsd:
                                nodeName = "DataStructure";
                                break;
                            case SdmxStructureEnumType.Msd:
                                nodeName = "MetadataStructure";
                                break;
                            case SdmxStructureEnumType.MetadataFlow:
                                nodeName = "Metadataflow";
                                break;
                            case SdmxStructureEnumType.Dataflow:
                                nodeName = "Dataflow";
                                break;
                            case SdmxStructureEnumType.ProvisionAgreement:
                                nodeName = "ProvisionAgreement";
                                break;
                            case SdmxStructureEnumType.Registration:
                                // ContentConstraints can be applied to Registrations but this is purely for Data Registration purposes.
                                // ContentConstraints when written out as SDMX should NOT output Registration Attachments
                                continue;
                            default:
                                throw new SdmxNotImplementedException("Writing a constraint attachment for target : " + xsRef.TargetReference);
                        }

                        StaxRefUtilV3.WriteReference(xsRef, writer, nodeName);
                    }

                    writer.WriteEndElement(); // End ConstraintAttachment
                }
            }

            private void WriteKeySet(IConstraintDataKeySet keySet, string nodeName, bool isIncluded, StaxWriter writer)
            {
                if (keySet != null)
                {
                    writer.WriteStartElement(Ns, "DataKeySet");
                    writer.WriteAttribute("isIncluded", isIncluded.ToString().ToLower());

                    foreach (IConstrainedDataKey key in keySet.ConstrainedDataKeys)
                    {
                        writer.WriteStartElement(Ns, "Key");

                        if (key.HasValidityPeriod)
                        {
                            if (key.ValidityPeriod.HasStartPeriod())
                            {
                                writer.WriteAttribute("validFrom", key.ValidityPeriod.GetStartPeriod().DateInSdmxFormat);
                            }

                            if (key.ValidityPeriod.HasEndPeriod())
                            {
                                writer.WriteAttribute("validTo", key.ValidityPeriod.GetEndPeriod().DateInSdmxFormat);
                            }
                        }

                        WriteKeySetKeyValue(key.ConstrainedKeyValues.ToList(), "KeyValue", writer);
                        WriteKeySetKeyValue(key.ComponentValues.ToList(), "Component", writer);

                        writer.WriteEndElement(); // End Key
                    }

                    writer.WriteEndElement(); // End DataKeySet
                }
            }

            private void WriteKeySetKeyValue(List<IConstrainedKeyValue> kvs, string elementName, StaxWriter writer)
            {
                foreach (IConstrainedKeyValue kv in kvs)
                {
                    writer.WriteStartElement(Ns, elementName);
                    writer.WriteAttribute("id", kv.Concept);

                    if (kv.IsRemovePrefix())
                    {
                        writer.WriteAttribute("removePrefix", kv.IsRemovePrefix().ToString().ToLower());
                    }

                    writer.WriteElement(Ns, "Value", kv.Code);
                    writer.WriteEndElement(); // End KeyValue
                }
            }
        }

}
