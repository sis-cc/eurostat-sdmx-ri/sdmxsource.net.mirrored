// -----------------------------------------------------------------------
// <copyright file="StaxHclWriterEngineV3.cs" company="EUROSTAT">
//   Date Created : 2024-03-11
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace SdmxCore.Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Hcl
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using global::Io.Sdmx.Api.Singleton;
    using global::Io.Sdmx.Core.Sdmx.Api.Model.Structure;
    using global::Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3;
    using global::Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.OrgScheme;
    using global::Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Util;
    using global::Io.Sdmx.Utils.Core.Application;
    using global::Io.Sdmx.Utils.Core.Xml;
    using Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Util;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using SdmxCore.Io.Sdmx.Format.Ml.Engine.Structure.Reader.V3.Util;

    public class StaxHclWriterEngineV3 : AbstractV3Writer<IHierarchyV30>, IFusionSingleton
    {
        private static StaxHclWriterEngineV3 _instance;

        //Private constructor - lazy load instance
        private StaxHclWriterEngineV3() { }
        public static StaxHclWriterEngineV3 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StaxHclWriterEngineV3();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }

        public void DestroyInstance()
        {
            _instance = null;
        }

        protected override string RootNodeName => "Hierarchy";

        protected override IDictionary<string, string> GetAdditionalMaintainableAttributes(IHierarchyV30 maint)
        {
            Dictionary<string, string> returnMap = new Dictionary<string, string>();
            returnMap.Add("hasFormalLevels", maint.HasFormalLevels().ToString().ToLowerInvariant());
            return returnMap;
        }

        protected override void WriteMaintainableInternal(IHierarchyV30 maint, IExternalMaintainableLinks externalLinks, StaxWriter writer)
        {
            WriteLevel(maint.Level, externalLinks, writer);
            WriteHierarchicalCode(maint.HierarchicalCodeObjects, externalLinks, writer);
        }

        private void WriteHierarchicalCode(IList<IHierarchicalCode> hCode, IExternalMaintainableLinks externalLinks, StaxWriter writer)
        {
            foreach (var currentHCode in hCode)
            {
                Dictionary<string, string> attrs = new Dictionary<string, string>();
                if (currentHCode.ValidFrom != null)
                {
                    attrs.Add("validFrom", currentHCode.ValidFrom.DateInSdmxFormat);
                }
                if (currentHCode.ValidTo != null)
                {
                    attrs.Add("validTo", currentHCode.ValidTo.DateInSdmxFormat);
                }

                StaxIdentifiableWriterUtilV3.WriteObject(currentHCode, "HierarchicalCode", externalLinks, writer, attrs);
                StaxRefUtilV3.WriteReference(currentHCode.CodeReference, writer, "Code");
                WriteHierarchicalCode(currentHCode.CodeRefs, externalLinks, writer);
                if (currentHCode.GetLevel(false) != null)
                {
                    StaxRefUtilV3.WriteLocalReference(currentHCode.GetLevel(false).Id, writer, "Level");
                }

                writer.WriteEndElement(); // End HierarchicalCode
            }
        }

        private void WriteLevel(ILevelObject level, IExternalMaintainableLinks externalLinks, StaxWriter writer)
        {
            if (level != null)
            {
                StaxNameableWriterUtilV3.WriteObject(level, "Level", externalLinks, writer);
                StaxTextFormatWriterUtilV3.WriteTextFormat(level.CodingFormat, writer, "CodingFormat");
                WriteLevel(level.ChildLevel, externalLinks, writer);
                writer.WriteEndElement(); // End Level
            }
        }
    }

}
