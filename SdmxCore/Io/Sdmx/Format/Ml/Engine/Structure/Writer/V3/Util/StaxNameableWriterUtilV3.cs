// -----------------------------------------------------------------------
// <copyright file="StaxNameableWriterUtilV3.cs" company="EUROSTAT">
//   Date Created : 2023-06-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Util
{
    using System.Collections.Generic;
    using Io.Sdmx.Core.Sdmx.Api.Model.Structure;
    using Io.Sdmx.Format.Ml.Constant;
    using Io.Sdmx.Utils.Core.Xml;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Util.Extensions;

    public class StaxNameableWriterUtilV3
    {
        public static void WriteObject(INameableObject nameable, string elementName, IExternalMaintainableLinks externalLinks, StaxWriter writer)
        {
            WriteObject(nameable, elementName, externalLinks, writer, null);
        }

        public static void WriteObject(INameableObject nameable, string elementName, IExternalMaintainableLinks externalLinks, StaxWriter writer, IDictionary<string, string> attrs)
        {
            IDictionary<string, string> attributes = StaxIdentifiableWriterUtilV3.GetAttributes(nameable);
            if (attrs != null)
            {
                attributes.PutAll(attrs);
            }
            writer.WriteElement(SdmxNamespace.GetFromEnum(SdmxNamespaceEnumType.Structure3).Namespace, elementName, attributes);
            WriteObject(nameable, externalLinks, writer);
        }

        public static void WriteObject(INameableObject nameable, IExternalMaintainableLinks externalLinks, StaxWriter writer)
        {
            StaxAnnotableWriterUtilV3.WriteObject(nameable, writer);
            StaxIdentifiableWriterUtilV3.WriteLinks(nameable, externalLinks, writer);
            StaxTextWriterUtilV3.WriteTextTypes(nameable.Names, writer, "Name");
            StaxTextWriterUtilV3.WriteTextTypes(nameable.Descriptions, writer, "Description");
        }
    }
}