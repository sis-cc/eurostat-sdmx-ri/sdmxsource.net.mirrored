using System;
using System.Collections.Generic;
using System.Text;

namespace SdmxCore.Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Util
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using global::Io.Sdmx.Format.Ml.Constant;
    using global::Io.Sdmx.Utils.Core.Xml;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    public static class StaxTextFormatWriterUtilV3
    {
        public static void WriteTextFormat(ITextFormat textFormatBean, StaxWriter writer, string elementName)
        {
            WriteTextFormat(textFormatBean, writer, elementName, true);
        }

        public static void WriteTextFormat(ITextFormat textFormatBean, StaxWriter writer, string elementName, bool includeMultiLingual)
        {
            if (textFormatBean != null)
            {
                Dictionary<string, string> attributes = new Dictionary<string, string>();
                AddToMap(attributes, "decimals", textFormatBean.Decimals);
                AddToMap(attributes, "maxLength", textFormatBean.MaxLength);
                AddToMap(attributes, "minLength", textFormatBean.MinLength);
                AddToMap(attributes, "endTime", textFormatBean.EndTime);
                AddToMap(attributes, "endValue", textFormatBean.EndValue);
                AddToMap(attributes, "interval", textFormatBean.Interval);
                AddToMap(attributes, "maxValue", textFormatBean.MaxValue);
                AddToMap(attributes, "minValue", textFormatBean.MinValue);
                if (includeMultiLingual)
                {
                    AddToMap(attributes, "isMultiLingual", textFormatBean.Multilingual);
                }
                AddToMap(attributes, "pattern", textFormatBean.Pattern);
                AddToMap(attributes, "isSequence", textFormatBean.Sequence);
                AddToMap(attributes, "startTime", textFormatBean.StartTime);
                AddToMap(attributes, "startValue", textFormatBean.StartValue);
                AddToMap(attributes, "textType", textFormatBean.TextType);
                AddToMap(attributes, "timeInterval", textFormatBean.TimeInterval);

                SdmxStructureEnumType parentStructureType = SdmxStructureEnumType.Null;
                ISdmxObject parent = textFormatBean.Parent;
                if (parent != null && parent.Parent != null)
                {
                    parentStructureType = parent.Parent.StructureType;
                }

                // Output the attributes if there are any or this is the TimeDimension
                if (attributes.Count > 0 || SdmxStructureEnumType.TimeDimension == parentStructureType)
                {
                    writer.WriteStartElement(SdmxNamespace.GetFromEnum(SdmxNamespaceEnumType.Structure3).Namespace, elementName);
                    foreach (var attribute in attributes)
                    {
                        writer.WriteAttribute(attribute.Key, attribute.Value);
                    }
                    writer.WriteEndElement();
                }
            }
        }

        private static void AddToMap(Dictionary<string, string> attributes, string attributeName, TextType textType)
        {
            if (textType != null)
            {
                attributes.Add(attributeName, textType.Sdmx3);
            }
        }

        private static void AddToMap(Dictionary<string, string> attributes, string attributeName, decimal? dec)
        {
            attributes.Add(attributeName, dec.ToString());
        }

        private static void AddToMap(Dictionary<string, string> attributes, string attributeName, string str)
        {
            if (str != null)
            {
                attributes.Add(attributeName, str);
            }
        }

        private static void AddToMap(Dictionary<string, string> attributes, string attributeName, ISdmxDate date)
        {
            if (date != null)
            {
                attributes.Add(attributeName, date.DateInSdmxFormat);
            }
        }

        private static void AddToMap(Dictionary<string, string> attributes, string attributeName, TertiaryBool boolean)
        {
            if (boolean.IsSet())
            {
                attributes.Add(attributeName, XmlConvert.ToString(boolean.IsTrue));
            }
        }
    }

}
