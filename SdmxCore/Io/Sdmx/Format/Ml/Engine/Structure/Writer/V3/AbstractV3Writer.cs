// -----------------------------------------------------------------------
// <copyright file="AbstractV3Writer.cs" company="EUROSTAT">
//   Date Created : 2023-06-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3
{
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Io.Sdmx.Core.Sdmx.Api.Model.Structure;
    using Io.Sdmx.Format.Ml.Api.Engine;
    using Io.Sdmx.Format.Ml.Constant;
    using Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3.Util;
    using Io.Sdmx.Utils.Core.Xml;
    using System.Collections.Generic;

    public abstract class AbstractV3Writer<T> : IStaxMaintainableWriterEngine<T> 
        where T : IMaintainableObject
    {
        protected static Namespace Ns = StaxStructureWriterUtil.StrcutureNs3_0;

        public virtual void WriteMaintainable(T maint, StaxWriter writer)
        {
            WriteMaintainable(maint, null, writer);
        }

        public virtual void WriteMaintainable(T maint, IExternalMaintainableLinks externalLinks, StaxWriter writer)
        {
            StaxMaintainableWriterUtilV3.WriteObject(maint, RootNodeName, externalLinks, writer, GetAdditionalMaintainableAttributes(maint));

            //Note a Provision Agreement can not be a stub according to the SDMX version 2.1 Schema
            if (maint.StructureType == SdmxStructureEnumType.ProvisionAgreement || !maint.IsExternalReference.IsTrue)
            {
                WriteMaintainableInternal(maint, externalLinks, writer);
            }
            writer.WriteEndElement(); //End Maintainable
        }

        /// <summary> 
        /// Allows sub classes to override to provide additional maintainable attributes 
        /// </summary>
        /// <returns>
        /// 
        /// </returns> 
        protected virtual IDictionary<string, string> GetAdditionalMaintainableAttributes(T maint)
        {
            return null;
        }

        protected abstract string RootNodeName { get; }


        protected abstract void WriteMaintainableInternal(T maint, IExternalMaintainableLinks externalLinks, StaxWriter writer);
    }
}