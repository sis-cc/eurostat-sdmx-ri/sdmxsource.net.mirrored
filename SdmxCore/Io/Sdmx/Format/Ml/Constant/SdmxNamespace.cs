// -----------------------------------------------------------------------
// <copyright file="SdmxNamespace.cs" company="EUROSTAT">
//   Date Created : 2023-06-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Constant
{
    using System.Collections.Generic;
    using Io.Sdmx.Utils.Core.Xml;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    public enum SdmxNamespaceEnumType
    {
        Xml,
        Xsi,
        Generic21,
        Message21,
        Msd21,
        Common21,
        Registry21,
        Structure21,
        Message3,
        Common3,
        Structure3,
        Registry3,
        Footer3_0
    }

    public class SdmxNamespace : BaseConstantType<SdmxNamespaceEnumType>
    {
        private static readonly Dictionary<SdmxNamespaceEnumType, SdmxNamespace> _instances =
            new Dictionary<SdmxNamespaceEnumType, SdmxNamespace>()
            {
                {
                    SdmxNamespaceEnumType.Xml,
                    new SdmxNamespace(SdmxNamespaceEnumType.Xml, "xml", "http://www.w3.org/XML/1998/namespace")
                },
                {
                    SdmxNamespaceEnumType.Xsi,
                    new SdmxNamespace(SdmxNamespaceEnumType.Xsi, "xsi", "http://www.w3.org/2001/XMLSchema-instance")
                },
                {
                    SdmxNamespaceEnumType.Generic21,
                    new SdmxNamespace(SdmxNamespaceEnumType.Generic21, "generic", SdmxConstants.GenericNs21)
                },
                {
                    SdmxNamespaceEnumType.Message21,
                    new SdmxNamespace(SdmxNamespaceEnumType.Message21, "message", SdmxConstants.MessageNs21)
                },
                {
                    SdmxNamespaceEnumType.Msd21,
                    new SdmxNamespace(SdmxNamespaceEnumType.Msd21, "generic", SdmxConstants.GenericMetadataNs21)
                },
                {
                    SdmxNamespaceEnumType.Common21,
                    new SdmxNamespace(SdmxNamespaceEnumType.Common21, "com", SdmxConstants.CommonNs21)
                },
                {
                    SdmxNamespaceEnumType.Registry21,
                    new SdmxNamespace(SdmxNamespaceEnumType.Registry21, "reg", SdmxConstants.RegistryNs21)
                },
                {
                    SdmxNamespaceEnumType.Structure21,
                    new SdmxNamespace(SdmxNamespaceEnumType.Structure21, "str", SdmxConstants.StructureNs21)
                },
                {
                    SdmxNamespaceEnumType.Message3,
                    new SdmxNamespace(SdmxNamespaceEnumType.Message3, "message", SdmxConstants.MessageNs300)
                },
                {
                    SdmxNamespaceEnumType.Common3,
                    new SdmxNamespace(SdmxNamespaceEnumType.Common3, "com", SdmxConstants.CommonNs300)
                },
                {
                    SdmxNamespaceEnumType.Structure3,
                    new SdmxNamespace(SdmxNamespaceEnumType.Structure3, "str", SdmxConstants.StructureNs300)
                },
                {
                    SdmxNamespaceEnumType.Registry3,
                    new SdmxNamespace(SdmxNamespaceEnumType.Registry3, "reg", SdmxConstants.RegistryNs300)
                },
                {
                    SdmxNamespaceEnumType.Footer3_0,
                    new SdmxNamespace(SdmxNamespaceEnumType.Footer3_0, "footer", "http://www.sdmx.org/resources/sdmxml/schemas/v3_0/message/footer")
                }
            };

        private readonly Namespace _ns;

        private SdmxNamespace(SdmxNamespaceEnumType enumType, string prefix, string ns)
            : base(enumType)
        {
            _ns = new Namespace(ns, prefix);
        }

        public Namespace Namespace => _ns;

        public string GetPrefix()
        {
            return _ns.GetNamespacePrefix();
        }

        public string GetNamespaceURL()
        {
            return _ns.GetNamespaceURL();
        }

        public override string ToString()
        {
            return _ns.ToString();
        }

        /// <summary>
        ///     Gets the values.
        /// </summary>
        public static IEnumerable<SdmxNamespace> Values
        {
            get
            {
                return _instances.Values;
            }
        }

        /// <summary>
        ///     Gets the instance of <see cref="SdmxNamespace" /> mapped to <paramref name="enumType" />
        /// </summary>
        /// <param name="enumType">
        ///     The <c>enum</c> type
        /// </param>
        /// <returns>
        ///     the instance of <see cref="SdmxNamespace" /> mapped to <paramref name="enumType" />
        /// </returns>
        public static SdmxNamespace GetFromEnum(SdmxNamespaceEnumType enumType)
        {
            if (_instances.TryGetValue(enumType, out SdmxNamespace output))
            {
                return output;
            }

            return null;
        }
    }
}