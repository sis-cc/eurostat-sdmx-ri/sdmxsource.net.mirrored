// -----------------------------------------------------------------------
// <copyright file="StaxStructureUtil.cs" company="EUROSTAT">
//   DateTime Created : 2023-06-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Util
{
    using Io.Sdmx.Api.Xml;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using System.Collections.Generic;
    using Org.Sdmxsource.Util;
    using System;
    using Org.Sdmxsource.Sdmx.Util.Date;

    public class StaxStructureUtil
    {
        public static void ThrowUnexpectedNodeException(IStaxReader staxReader)
        {
            throw new SdmxSemmanticException("Unexpected Xml element : " + staxReader.GetXMLPath());
        }

        public static void CheckStartNode(IStaxReader staxReader, string expectedNode)
        {
            if (!staxReader.IsStartElement)
            {
                throw new SdmxSemmanticException("Missing expected Xml element : " + expectedNode);
            }
            if (!staxReader.ElementName.Equals(expectedNode))
            {
                throw new SdmxSemmanticException("Unexpected Xml element : " + staxReader.GetXMLPath() + ". Expected Xml element: " + expectedNode);
            }
        }

        public static bool GetAttributeAsBoolean(IDictionary<string, string> attributes, string attributeId, bool isMandatory, bool defaultValue)
        {
            string asString = GetAttribute(attributes, attributeId, isMandatory);
            if (asString != null)
            {
                return bool.Parse(asString);
            }
            return defaultValue;
        }

        public static TertiaryBool GetAttributeAsTertiaryBoolean(IDictionary<string, string> attributes, string attributeId, bool isMandatory)
        {
            string asString = GetAttribute(attributes, attributeId, isMandatory);
            if (asString != null)
            {
                return TertiaryBool.ParseBoolean(bool.Parse(asString));
            }
            return TertiaryBool.GetFromEnum(TertiaryBoolEnumType.Unset);
        }

        public static TextType GetAttributeAsTextType(IDictionary<string, string> attributes, string attributeId, bool isMandatory)
        {
            string asString = GetAttribute(attributes, attributeId, isMandatory);
            if (asString != null)
            {
                return TextType.ParseString(asString);
            }
            return null;
        }

        public static long? GetAttributeAsBigInteger(IDictionary<string, string> attributes, string attributeId, bool isMandatory)
        {
            string asString = GetAttribute(attributes, attributeId, isMandatory);
            if (asString != null)
            {
                return long.Parse(asString);
            }
            return null;
        }

        public static int? GetAttributeAsInteger(IDictionary<string, string> attributes, string attributeId, bool isMandatory)
        {
            string asString = GetAttribute(attributes, attributeId, isMandatory);
            if (asString != null)
            {
                if (asString.Equals("unbounded"))
                {
                    return null;
                }
                return int.Parse(asString);
            }
            return null;
        }

        public static decimal? GetAttributeAsBigDecimal(IDictionary<string, string> attributes, string attributeId, bool isMandatory)
        {
            string asString = GetAttribute(attributes, attributeId, isMandatory);
            if (asString != null)
            {
                return decimal.Parse(asString);
            }
            return null;
        }


        public static DateTime? GetAttributeAsDate(IDictionary<string, string> attributes, string attributeId, bool isMandatory)
        {
            string asString = GetAttribute(attributes, attributeId, isMandatory);
            if (asString != null)
            {
                return DateUtil.FormatDate(asString, true);
            }
            return null;
        }

        public static string GetAttribute(IDictionary<string, string> attributes, string attributeId, bool isMandatory)
        {
            attributes.TryGetValue(attributeId, out string attr);
            
            if (isMandatory && !ObjectUtil.ValidString(attr))
            {
                throw new SdmxSemmanticException("Missing mandatory attribute: " + attributeId);
            }
            if (attr != null && attr.Length == 0)
            {
                return null;
                //throw new SdmxSemmanticException("Attribute value is missing: " + attributeId);
            }
            return attr;
        }

        public static ITextTypeWrapperMutableObject GetText(IStaxReader staxReader)
        {
            return new TextTypeWrapperMutableCore(GetLangLocale(staxReader), staxReader.GetElementText());
        }

        /// <summary> 
        /// <root-node> 
        ///    <Ref></Ref> 
        ///    <URN></URN> 
        /// </root-node> 
        /// </summary>
        /// <param name=" staxReader"> 
        /// </param>
        /// <param name=" targetStructure"> 
        /// </param>
        /// <returns>
        /// </returns> 
        public static IStructureReference GetStructureReference(IStaxReader staxReader, SdmxStructureType targetStructure)
        {
            string exitNode = staxReader.ElementName;

            staxReader.MoveNextStartNode();
            string element = staxReader.ElementName;
            if (element.Equals("Ref"))
            {
                IDictionary<string, string> attributes = staxReader.GetAttributes();

                string agencyId = GetAttribute(attributes, "agencyID", true);

                string maintId = GetAttribute(attributes, "maintainableParentID", targetStructure != null && !targetStructure.IsMaintainable);
                string version = GetAttribute(attributes, "version", false);
                string[] identifiableId = null;
                if (maintId == null)
                {
                    maintId = GetAttribute(attributes, "id", true);
                }
                else
                {
                    //Identifiable
                    version = GetAttribute(attributes, "maintainableParentVersion", false);
                    string containerId = GetAttribute(attributes, "ContainerID", false);
                    string id = GetAttribute(attributes, "id", true);
                    if (id == null)
                    {
                        throw new SdmxSemmanticException("Attribute value of 'id' is missing");
                    }
                    if (containerId != null)
                    {
                        string[] idArr = id.Split('.');
                        identifiableId = new string[idArr.Length + 1];
                        identifiableId[0] = containerId;
                        for (int i = 0; i < idArr.Length; i++)
                        {
                            identifiableId[i + 1] = idArr[i];
                        }
                    }
                    else
                    {
                        identifiableId = id.Split('.');
                    }
                }
                string refClass = GetAttribute(attributes, "class", false);
                string refPackage = GetAttribute(attributes, "package", false);
                if (refClass != null)
                {
                    if (refPackage != null)
                    {
                        targetStructure = SdmxStructureType.ParsePackageAndClass(refPackage, refClass);
                    }
                    else
                    {
                        targetStructure = SdmxStructureType.ParseClass(refClass);
                    }
                }
                if (version == null)
                {
                    version = "1.0";
                }
                staxReader.SkipToEndNode(exitNode);
                return new StructureReferenceImpl(agencyId, maintId, version, targetStructure, identifiableId);
            }
            else if (element.Equals("URN"))
            {
                string urn = staxReader.GetElementText();
                staxReader.SkipToEndNode(exitNode);
                return new StructureReferenceImpl(urn);
            }
            else
            {
                ThrowUnexpectedNodeException(staxReader);
            }
            return null;
        }

        public static string GetLocalReference(IStaxReader staxReader)
        {
            string exitNode = staxReader.ElementName;
            staxReader.MoveNextStartNode();
            string id = null;
            if (staxReader.ElementName.Equals("Ref"))
            {
                id = GetAttribute(staxReader.GetAttributes(), "id", true);
            }
            else
            {
                ThrowUnexpectedNodeException(staxReader);
            }
            staxReader.SkipToEndNode(exitNode);
            return id;
        }

        public static string GetLangLocale(IStaxReader staxReader)
        {
            string lang = staxReader.GetAttributes()["lang"];
            if (!ObjectUtil.ValidString(lang))
            {
                lang = "en";
            }
            return lang;
        }
    }
}