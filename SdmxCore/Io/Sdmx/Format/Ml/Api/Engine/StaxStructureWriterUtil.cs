// -----------------------------------------------------------------------
// <copyright file="StaxStructureWriterUtil.cs" company="EUROSTAT">
//   Date Created : 2023-06-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Format.Ml.Constant
{
    using Io.Sdmx.Utils.Core.Xml;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    public class StaxStructureWriterUtil
    {
        public static readonly Namespace XmlNs = new Namespace("http://www.w3.org/XML/1998/namespace", "xml");
        public static readonly Namespace MessageNs = new Namespace("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "mes");
        public static readonly Namespace StrcutureNs = new Namespace("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "str");
        public static readonly Namespace CommonNs = new Namespace("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "com");
        public static readonly Namespace RegistryNs = new Namespace("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", "reg");

        public static readonly Namespace MessageNs3_0 = new Namespace(SdmxConstants.MessageNs300, "mes");
        public static readonly Namespace StrcutureNs3_0 = new Namespace(SdmxConstants.StructureNs300, "str");
        public static readonly Namespace CommonNs3_0 = new Namespace(SdmxConstants.CommonNs300, "com");
        public static readonly Namespace RegistryNs3_0 = new Namespace(SdmxConstants.RegistryNs300, "reg");
    }
}