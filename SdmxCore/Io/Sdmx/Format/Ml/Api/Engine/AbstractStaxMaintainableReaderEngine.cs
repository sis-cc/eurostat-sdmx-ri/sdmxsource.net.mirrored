// -----------------------------------------------------------------------
// <copyright file="AbstractStaxMaintainableReaderEngine.cs" company="EUROSTAT">
//   DateTime Created : 2023-06-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxCore.IO.Sdmx.Format.Ml.Api.Engine
{
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using global::Io.Sdmx.Format.Ml.Api.Engine;
    using global::Io.Sdmx.Api.Xml;

    public abstract class AbstractStaxMaintainableReaderEngine<T> : IStaxMaintainableReaderEngine<T>
        where T : IMaintainableObject
    {
        public ISdmxObjects ObtainObjectsFromSDMX(IStaxReader reader)
        {
            ISdmxObjects objects = new SdmxObjectsImpl();
            objects.AddIdentifiables(ProcessSDMX(reader));
            return objects;
        }

        public abstract ISet<T> ProcessSDMX(IStaxReader reader);
    }
}
