// -----------------------------------------------------------------------
// <copyright file="JsonWriterUtil.cs" company="EUROSTAT">
//   Date Created : 2023-05-15
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Io.Sdmx.Utils.Json
{
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Io.Sdmx.Utils.Core.Object;
    using Org.Sdmxsource.Json;
    using System.IO;
    using System;
    using System.Text;
    using System.Collections.Generic;

    /// <summary> 
    /// Json Utility class for output using the JsonGenerator 
    /// </summary> 
    public class JsonWriterUtil
    {

        public static JsonGenerator CreateJsonWriter(Stream outStream)
        {
            return new JsonGenerator(new StreamWriter(outStream, new UTF8Encoding(false)));
        }

        /// <summary> 
        /// Writes an array of Strings as JSON 
        /// ["Foo","Bar"] 
        /// </summary>
        /// <param name=" jsonGenerator"> 
        /// </param>
        /// <param name=" property"> the property name of the array - if null then writes an array with no name 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static void WriteStringArray(JsonGenerator jsonGenerator, string property, string[] array)
        {
            try
            {
                if (ObjectUtilCore.ValidString(property))
                {
                    jsonGenerator.WriteArrayFieldStart(property);
                }
                else
                {
                    jsonGenerator.WriteStartArray();
                }
                if (array != null)
                {
                    foreach (string str in array)
                    {
                        jsonGenerator.WriteString(str);
                    }
                }
                jsonGenerator.WriteEndArray();
            }
            catch (IOException e)
            {
                throw new SystemException(e.Message, e);
            }
        }

        public static void WriteNumberArray(JsonGenerator jsonGenerator, string property, int[] array)
        {
            try
            {
                if (ObjectUtilCore.ValidString(property))
                {
                    jsonGenerator.WriteArrayFieldStart(property);
                }
                else
                {
                    jsonGenerator.WriteStartArray();
                }
                if (array != null)
                {
                    foreach (int index in array)
                    {
                        jsonGenerator.WriteNumber(index);
                    }
                }
                jsonGenerator.WriteEndArray();
            }
            catch (IOException e)
            {
                throw new SystemException(e.Message, e);
            }
        }

        /// <summary> 
        /// Writes an array of Strings as JSON 
        /// ["Foo","Bar"] 
        /// </summary>
        /// <param name=" jsonGenerator"> 
        /// </param>
        /// <param name=" property"> the property name of the array - if null then writes an array with no name 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static void WriteStringArray(JsonGenerator jsonGenerator, string property, ICollection<string> array)
        {
            try
            {
                if (ObjectUtilCore.ValidString(property))
                {
                    jsonGenerator.WriteArrayFieldStart(property);
                }
                else
                {
                    jsonGenerator.WriteStartArray();
                }
                if (array != null)
                {
                    foreach (string str in array)
                    {
                        jsonGenerator.WriteString(str);
                    }
                }
                jsonGenerator.WriteEndArray();
            }
            catch (IOException e)
            {
                throw new SystemException(e.Message, e);
            }
        }

        /// <summary> 
        /// Writes an map of Strings 
        /// { 
        ///   "A" : "B", 
        ///   "C" : "D" 
        /// } 
        /// 
        /// </summary>
        /// <param name=" jsonGenerator"> 
        /// </param>
        /// <param name=" property"> the property name of the array 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static void WriteStringMap(JsonGenerator jsonGenerator, string property, IDictionary<string, string> map)
        {
            try
            {
                if (ObjectUtilCore.ValidString(property))
                {
                    jsonGenerator.WriteObjectFieldStart(property);
                }
                else
                {
                    jsonGenerator.WriteStartObject();
                }
                if (map != null)
                {
                    foreach (string str in map.Keys)
                    {
                        jsonGenerator.WriteStringField(str, map[str]);
                    }
                }
                jsonGenerator.WriteEndObject();
            }
            catch (IOException e)
            {
                throw new SystemException(e.Message, e);
            }
        }

        /// <summary> 
        /// Writes the JSONObject to the JsonGenerator 
        /// </summary>
        /// <param name=" jsonGenerator"> 
        /// </param>
        /// <param name=" jObj"> 
        /// </param> 
        //TODO SDMXCORE IF NEEDED
        /*
        public static void WriteJsonObject(JsonGenerator jsonGenerator, JsonObject jObj) {
            try {
                jsonGenerator.WriteStartObject();
                Iterator<string> iter = jObj.Keys();
                while(iter.HasNext()) {
                    string aKey = iter.Next();
                    object val = jObj.Get(aKey);
                    if (val is JsonArray) {
                        WriteJsonArray(jsonGenerator, aKey, (JsonArray)val);
                    } else if (val is string) {
                        jsonGenerator.WriteStringField(aKey, (string)val);
                    } else if (val.Equals(null)) {
                        jsonGenerator.WriteStringField(aKey, null);
                    } else {
                        throw new SdmxException("Unexpected element when writing JsonObject. Element of type: " + val.Class);
                    }
                }
                jObj.Keys();
                jsonGenerator.WriteEndObject();
            } catch(Exception e) {
                throw new SdmxException(e, "Unable to add JsonObject to jsonGenerator. Error: " + e.Message);
            }
        }

         */

        /// <summary> 
        /// Writes the JSONArray to the JsonGenerator 
        /// </summary>
        /// <param name=" jsonGenerator"> 
        /// </param>
        /// <param name=" arrayName"> 
        /// </param>
        /// <param name=" jArr"> 
        /// </param> 
        /*
        public static void WriteJsonArray(JsonGenerator jsonGenerator, string arrayName, JsonArray jArr) {
            try {
                jsonGenerator.WriteArrayFieldStart(arrayName);
                for (int i=0; i < jArr.Length(); i++) {
                    object obj = jArr.Get(i);
                    if (obj is JsonObject) {
                        WriteJsonObject(jsonGenerator, (JsonObject)jArr.Get(i));
                    } else if (obj is string) {
                        jsonGenerator.WriteString((string)obj);
                    } else if (obj.Equals(null)) {
                        jsonGenerator.WriteNull();
                    } else {
                        throw new SdmxException("Unexpected element when writing JsonArray. Element of type: " + obj.Class);
                    }
                }
                jsonGenerator.WriteEndArray();

            } catch(Exception e) {
                throw new SdmxException(e, "Unable to add JsonArray to jsonGenerator. Error: " + e.Message);
            }
        }
        */
    }

}