// -----------------------------------------------------------------------
// <copyright file="URN.cs" company="EUROSTAT">
//   Date Created : 2023-05-09
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Utils.Sdmx.Xs
{
    using System;
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;
    using SdmxCore.IO.Sdmx.Utils.Sdmx.Xs;

    public class URN : URNabstract, IURN
    {
        private static readonly IDictionary<string, IURN> cache
            = new Dictionary<string, IURN>();

        private readonly IStructureReference _sRef;
        private IVersionRef _version;
        private readonly string _urn;
        private readonly string _shortUrn;

        public static IURN GetInstance(SdmxStructureType st, string agency, string id, string version, params string[] identIds)
        {
            if (st == null)
            {
                st = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Any);
            }
            if (agency == null)
            {
                agency = "*";
            }
            if (id == null)
            {
                id = "*";
            }
            if (version == null)
            {
                version = "*";
            }
            return GetInstance(st.GenerateUrn(agency, id, version, identIds));
        }

        public static IURN GetInstance(Uri urn)
        {
            string stringUri = urn.AbsoluteUri;
            if (!cache.TryGetValue(stringUri, out IURN iurn))
            {
                iurn = new URN(stringUri);
                cache.Add(stringUri, new URN(stringUri));
            }
            return iurn;
        }

        private URN(string urn)
        {
            if (urn == null)
            {
                throw new ArgumentException("urn can not be null");
            }
            this._urn = urn;
            this._shortUrn = urn.Split('=')[0];
            this._sRef = new StructureReferenceImpl(urn);
        }

        public string GetURN()
        {
            return this._urn;
        }

        public override IStructureReference StructureReference => _sRef;

        public string ShortURN => this._shortUrn;

        public IVersionRef Version
        {
            get
            {
                if (_version == null)
                {
                    _version = VersionRef.GetInstance(_sRef.Version);
                }
                return _version;
            }
        }

        public override int GetHashCode()
        {
            return GetURN().GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == this)
            {
                return true;
            }
            if (obj is IURN)
            {
                IURN that = (IURN)obj;
                if (!ObjectUtil.Equivalent(this.GetURN(), that.GetURN()))
                {
                    return false;
                }
                return true;
            }
            return false;
        }

        public override string ToString()
        {
            return this.GetURN();
        }
    }
}