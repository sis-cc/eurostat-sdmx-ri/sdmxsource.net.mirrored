using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

namespace SdmxCore.IO.Sdmx.Utils.Sdmx.Xs
{
    public abstract class URNabstract
    {
        public abstract IStructureReference StructureReference { get; }

        public string AgencyId => StructureReference.AgencyId;

        public string MaintId => StructureReference.MaintainableId;

        public string IdentifiableId => StructureReference.FullId;

        public SdmxStructureType TargetStructure => StructureReference.TargetReference;
    }
}
