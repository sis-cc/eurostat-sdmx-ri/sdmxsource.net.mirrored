// -----------------------------------------------------------------------
// <copyright file="VersionRef.cs" company="EUROSTAT">
//   Date Created : 2023-05-09
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Utils.Sdmx.Xs
{
    using System;
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary> 
    /// A reference to a specific version or range of versions of artifacts, using the semantic versioning syntax 
    /// </summary> 
    public class VersionRef : IVersionRef
    {
        private static readonly IDictionary<string, VersionRef> _refIDictionary = new Dictionary<string, VersionRef>();
        private readonly string _versionRef;
        private readonly bool _isAll = false;
        private readonly bool _isLatest = false;
        private readonly bool _isAbsolute = true;
        private readonly string _versionSuffix;
        private readonly bool _allSuffix = false;
        private readonly ISdmxVersion _minVersion;
        private readonly ISdmxVersion _maxVersion;

        public static VersionRef GetInstance(string versionRef)
        {
            if (versionRef == null)
            {
                versionRef = "*";
            }
            lock (_refIDictionary)
            {
                VersionRef instance = _refIDictionary[versionRef];
                if (instance == null)
                {
                    instance = new VersionRef(versionRef);
                    _refIDictionary.Add(versionRef, instance);
                }
                return instance;
            }
        }

        /// <summary> 
        /// Syntax 2 part: 
        /// <ol> 
        ///   <li><b>1.+</b> latest stable 1.x, e.g. 1.4</li> 
        ///   <li><b>1.~</b> latest 2 part stable or draft  1.x, e.g. 1.4-draft</li> 
        ///   <li><b>1.*</b> all 1.x versions, e.g. 1.0, 1.1, 1.2</li> 
        /// </ol> 
        /// 
        /// Syntax 3 part only stable: 
        /// <ol> 
        ///   <li><b>1.2.0</b>  explicit reference</li> 
        ///   <li><b>+</b>latest stable version</li> 
        ///   <li><b>1.2.+</b> latest stable 1.2.x</li> 
        /// </ol> 
        /// 
        /// Syntax 3 part stable or unstable: 
        /// <ol> 
        ///   <li><b>~</b>latest stable or draft version</li> 
        ///   <li><b>1.~.0</b> latest 3 part stable or draft  1.x.x</li> 
        ///   <li><b>1.2.~</b> latest stable or draft 1.2.x</li> 
        /// </ol> 
        /// 
        /// Syntax 3 all: 
        /// <ol> 
        ///   <li><b>*</b>all stable or draft version, e.g. 1.0, 1.0.0, 1.1.0-draft </li> 
        ///   <li><b>1.2.*</b> all 3 part versions, 1.2.0, 1.2.2, 1.2.1</li> 
        ///   <li><b>1.*.0</b> minimum version 1.0.0 with a 1 major, e.g. 1.0.0, 1.1.0, 1.2.0, 1.2.1, 1.2.2, 1.3.0 and 1.3.1-draft</li> 
        ///   <li><b>1.2*.0</b> minimum version 1.2.0 with a 1 major, e.g. 1.2.0, 1.2.1, 1.2.2, 1.3.0 and 1.3.1-draft</li> 
        /// </ol> 
        /// 
        /// 
        /// </summary>
        /// <param name=" versionRef"> 
        /// </param> 
        private VersionRef(string versionRef)
        {
            this._versionRef = versionRef;
            string[] suffixSplit = versionRef.Split("-".ToCharArray(), 2);

            this._versionSuffix = suffixSplit.Length > 1 ? suffixSplit[1] : null;

            int[] fromVersion = new int[3];
            int[] toVersion = new int[3];
            for (int i = 0; i < 3; i++)
            {  //initialise arrays to -1
                fromVersion[i] = -1;
                toVersion[i] = -1;
            }

            string versionPart = suffixSplit[0];
            if (versionPart.Equals("+") || versionPart.Equals("~") || versionPart.Equals("*"))
            {
                _isAbsolute = false;
                //just a 1 part reference
                if (!versionPart.EndsWith("+"))
                { //stable only
                    _allSuffix = true;
                    _versionSuffix = "*";         //not stable only, so any version suffix
                }
                _isLatest = !versionPart.EndsWith("*");
                _isAll = !_isLatest;
                this._minVersion = SdmxVersion.GetInstance(0, 0, 0);
                this._maxVersion = SdmxVersion.GetInstance(int.MaxValue, int.MaxValue, int.MaxValue);
            }
            else
            {
                //multipart reference
                string[] versionSplit = suffixSplit[0].Split('.');
                if (versionSplit.Length > 3)
                {
                    throw new SdmxSemmanticException("Illegal version reference '" + versionRef + "' a maximum of 3 parts expected, where each part is separated by a period seperator");
                }
                for (int i = 0; i < versionSplit.Length; i++)
                {
                    string part = versionSplit[i];
                    if (part.EndsWith("+") || part.EndsWith("~") || part.EndsWith("*"))
                    {
                        _isAbsolute = false;
                        if (!part.EndsWith("+"))
                        { //stable only
                            _allSuffix = true;
                            _versionSuffix = "*";         //not stable only, so any version suffix
                        }
                        _isLatest = !part.EndsWith("*");
                        fromVersion[i] = part.Length > 1 ? int.Parse(part.Substring(0, part.Length - 1)) : 0;
                        toVersion[i] = int.MaxValue;
                        i++;
                        while (i < versionSplit.Length)
                        {
                            fromVersion[i] = int.Parse(versionSplit[i]);
                            toVersion[i] = int.MaxValue;
                            i++;
                        }
                    }
                    else
                    {
                        try
                        {
                            int parsedInt = int.Parse(versionSplit[i]);
                            fromVersion[i] = parsedInt;
                            toVersion[i] = parsedInt;
                        }
                        catch (FormatException)
                        {
                            throw new SdmxSemmanticException("Illegal version reference '" + versionRef + "'");
                        }
                    }
                }
                this._minVersion = SdmxVersion.GetInstance(fromVersion[0], fromVersion[1], fromVersion[2]);
                this._maxVersion = SdmxVersion.GetInstance(toVersion[0], toVersion[1], toVersion[2]);
            }
        }

        public bool IsLatest()
        {
            return _isLatest;
        }
        
        public bool IsAbsolute()
        {
            return _isAbsolute;
        }
        
        public bool IsAll()
        {
            return _isAll;
        }
        
        public string VersionSuffix => _versionSuffix;
        
        public ISdmxVersion MinVersion => _minVersion;
 
        public ISdmxVersion MaxVersion => _maxVersion;

        public bool IsMatch(ISdmxVersion version)
        {
            if (_versionSuffix == null && version.Suffix != null)
            {
                return false;
            }
            if (_versionSuffix != null && !_allSuffix && !_versionSuffix.Equals(version.Suffix))
            {
                return false;
            }
            if (_isAll)
            {
                return true;
            }

            //Check Major
            if ((version.Major < this._minVersion.Major) || (version.Major > this._maxVersion.Major))
            {
                return false;
            }
            if (version.Major == this._minVersion.Major)
            {
                if ((version.Minor < this._minVersion.Minor) || (version.Minor > this._maxVersion.Minor))
                {
                    return false;
                }
            }
            if (version.Minor == this._minVersion.Minor)
            {
                if ((version.Patch < this._minVersion.Patch) || (version.Patch > this._maxVersion.Patch))
                {
                    return false;
                }
            }
            return true;
        }

        public bool IsMatch(IVersionRef vRef)
        {
            if (_isAll || vRef.IsAll())
            {
                return true;
            }
            ISdmxVersion minVersion = vRef.MinVersion;
            ISdmxVersion maxVersion = vRef.MaxVersion;

            return IsInside(minVersion) || IsInside(maxVersion);
        }

        /// <summary> 
        /// </summary>
        /// <param name=" version"> 
        /// </param>
        /// <returns>
        /// true if the SDMX version lies on or inside this version reference 
        /// </returns> 
        private bool IsInside(ISdmxVersion version)
        {
            if (version.Major == this._minVersion.Major || version.Major == this._maxVersion.Major)
            {
                if (version.Minor == this._minVersion.Minor || version.Minor == this._maxVersion.Minor)
                {
                    if (version.Patch == this._minVersion.Patch || version.Patch == this._maxVersion.Patch)
                    {
                        return true;
                    }
                    else
                    {
                        if (version.Patch < this._maxVersion.Patch && version.Patch > this._minVersion.Patch)
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    if (version.Minor < this._maxVersion.Minor && version.Minor > this._minVersion.Minor)
                    {
                        return true;
                    }
                }
            }
            else
            {
                if (version.Major < this._maxVersion.Major && version.Major > this._minVersion.Major)
                {
                    return true;
                }
            }
            return false;
        }

        public bool Equals(object obj)
        {
            if (obj is VersionRef)
            {
                return obj.ToString().Equals(this.ToString());
            }
            return false;
        }

        public int HashCode()
        {
            return _versionRef.GetHashCode();
        }

        public string ToString()
        {
            return _versionRef;
        }
    }
}