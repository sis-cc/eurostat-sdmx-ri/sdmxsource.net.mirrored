// -----------------------------------------------------------------------
// <copyright file="SdmxVersion.cs" company="EUROSTAT">
//   Date Created : 2023-05-09
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Utils.Sdmx.Xs
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    public class SdmxVersion : ISdmxVersion
    {
        private static readonly IDictionary<string, SdmxVersion> _cache = new Dictionary<string, SdmxVersion>();
        private readonly string _version;
        private readonly string _suffix;
        private readonly int _major;
        private readonly int _minor = -1;
        private readonly int _patch = -1;

        public static SdmxVersion GetInstance(int major, int minor, int patch)
        {
            var sb = new StringBuilder();
            sb.Append(major);
            if (minor >= 0)
            {
                sb.Append("." + minor);
                if (patch >= 0)
                {
                    sb.Append("." + patch);
                }
            }
            return GetInstance(sb.ToString());
        }

        public static SdmxVersion GetInstance(string version)
        {
            SdmxVersion sdmxVersion;
            lock (_cache)
            {
                if (!_cache.TryGetValue(version, out sdmxVersion))
                {
                    sdmxVersion = new SdmxVersion(version);
                    _cache[version] = sdmxVersion;
                }
            }
            return sdmxVersion;
        }

        private SdmxVersion(string version)
        {
            this._version = version;
            string[] splitSuffix = version.Split("-".ToCharArray(), 2);
            if (splitSuffix.Length > 1)
            {
                this._suffix = splitSuffix[1];
            }
            string[] versionParts = splitSuffix[0].Split('.');
            if (versionParts.Length > 3)
            {
                throw new SdmxSemmanticException("Unsupported version syntax '" + version + "'. Only 3 version parts supported major.minor.patch e.g. 1.0.0");
            }
            try
            {
                _major = int.Parse(versionParts[0]);
                if (versionParts.Length > 1)
                {
                    _minor = int.Parse(versionParts[1]);
                }
                if (versionParts.Length > 2)
                {
                    _patch = int.Parse(versionParts[2]);
                }
            }
            catch (FormatException)
            {
                throw new SdmxSemmanticException("Unable to parse version '" + version + "'");
            }
        }

        public int CompareTo(ISdmxVersion that)
        {
            int i = this._major - that.Major;
            if (i == 0)
            {
                i = this._minor - that.Minor;
            }
            if (i == 0)
            {
                i = this._patch - that.Patch;
            }
            if (i == 0)
            {
                if (this._suffix != null)
                {
                    if (that.Suffix != null)
                    {
                        return this._suffix.CompareTo(that.Suffix);
                    }
                    return -1;
                }
                if (that.Suffix != null)
                {
                    return 1;
                }
            }
            return i;
        }

        public bool IsLater(ISdmxVersion that)
        {
            return this.CompareTo(that) > 0;
        }

        public bool IsStable()
        {
            return _suffix == null && IsTwoPart() == false;
        }

        public bool IsTwoPart()
        {
            return _patch < 0;
        }

        public int Major => _major;

        public int Minor => _minor;

        public int Patch => _patch;

        public string Suffix => _suffix;

        public int HashCode()
        {
            return ToString().GetHashCode();
        }

        public string ToString()
        {
            return _version;
        }

        public bool Equals(object obj)
        {
            if (this == obj)
            {
                return true;
            }
            if (obj is SdmxVersion)
            {
                SdmxVersion that = (SdmxVersion)obj;
                return this._version.Equals(that._version);
            }
            return false;
        }
    }

}