// -----------------------------------------------------------------------
// <copyright file="ObjectUtilCore.cs" company="EUROSTAT">
//   Date Created : 2023-05-15
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Io.Sdmx.Utils.Core.Object
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary> 
    /// Utility class providing helper methods for generic Objects. 
    /// </summary> 
    public class ObjectUtilCore
    {
        public static readonly string PoundSign = ((char)163).ToString();
        public static readonly string PrivateUseOne = ((char)8216).ToString(); //LEFT SINGLE QUOTATION MARK
        public static readonly string PrivateUseTwo = ((char)8217).ToString(); //RIGHT SINGLE QUOTATION MARK

        private static readonly string[] Alphabet = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

        public static void AssertTrue(bool value)
        {
            AssertTrue(null, value);
        }

        public static void AssertTrue(string message, bool? value)
        {
            if (value == null || value != true)
            {
                throw new ArgumentException(message != null ? message : "True Expected");
            }
        }
        public static void AssertFalse(string message, bool? value)
        {
            if (value == null || value != false)
            {
                throw new ArgumentException(message != null ? message : "False Expected");
            }
        }

        public static void AssertFalse(bool value)
        {
            AssertFalse(null, value);
        }

        public static void AssertEquals(object o1, object o2)
        {
            AssertEquals(null, o1, o2);
        }

        public static void AssertCollectionEquals<T>(ICollection<T> c1, ICollection<T> c2)
        {
            AssertCollectionEquals(null, c1, c2);
        }

        public static void AssertCollectionEquals<T>(string message, ICollection<T> c1, ICollection<T> c2)
        {
            if (!ObjectUtilCore.EquivalentCollection(c1, c2))
            {
                throw new ArgumentException(message != null ? message : "Collectionsa are not equal");
            }
        }

        public static void AssertEquals(string message, object o1, object o2)
        {
            if (!ObjectUtilCore.Equivalent(o1, o2))
            {
                throw new ArgumentException(message != null ? message : "Not equal: " + o1 + " and " + o2);
            }
        }

        public static T AssertNotNull<T>(T obj)
        {
            return AssertNotNull(null, obj);
        }

        public static T AssertNotNull<T>(string message, T obj)
        {
            if (obj == null)
            {
                throw new SystemException(message != null ? message : "Required object is null");
            }
            return obj;
        }

        public static void AssertNull(object obj)
        {
            AssertNotNull(null, obj);
        }

        public static void AssertNull(string message, object obj)
        {
            if (obj == null)
            {
                throw new SystemException(message != null ? message : "Expected null");
            }
        }

        /// <summary> 
        /// Takes a positive int, zero indexed, and converts it to an Excel style alpha. For example A, B, C,params  D.[]AA, AB, AC 
        /// </summary>
        /// <param name=" idx"> 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static string IntToAlpha(int idx)
        {
            string letter = "";
            int major = (int)Math.Floor((double)(idx / 26));
            int minor = idx - (major * 26);
            if (major > 0)
            {
                letter += Alphabet[major - 1];
            }
            letter += Alphabet[minor];
            return letter;
        }

        /// <summary> 
        /// 
        /// </summary>
        /// <param name=" str"> 
        /// </param>
        /// <param name=" defaultIfNull"> 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static int ToInteger(string str, int defaultIfNull)
        {
            if (!ValidString(str))
            {
                return defaultIfNull;
            }
            if (!IsInteger(str))
            {
                throw new SdmxSemmanticException(str + " is not a valid int");
            }
            try
            {
                return int.Parse(str);
            }
            catch (Exception e)
            {
                throw new SdmxSemmanticException(str + " is not a valid int");
            }
        }

        public static bool IsInteger(string s)
        {
            return IsInteger(s, 10);
        }

        public static bool IsInteger(string s, int radix)
        {
            if (s.Any())
            {
                return false;
            }

            for (int i = 0; i < s.Length; i++)
            {
                if (i == 0 && s[i] == '-')
                {
                    if (s.Length == 1)
                    {
                        return false;
                    }
                    else
                    {
                        continue;
                    }
                }
                if (Convert.ToInt32(s[i].ToString(), radix) < 0)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary> 
        /// Creates a new Array, and copies the contents from a1 if it is not null, and then copies the conents from a2 if it is not null 
        /// into the copy, and returns the copy. 
        /// 
        /// Example 
        /// a1 = [A, null, B] 
        /// a2 = [null, X, null] 
        /// return [A, X, B] 
        /// 
        /// </summary>
        /// <param name=" a1"> 
        /// </param>
        /// <param name=" a2"> 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static T[] MergeArrays<T>(T[] a1, T[] a2)
        {
            if (a1 != null && a2 != null)
            {
                bool cloneA1 = a1.Length >= a2.Length;
                T[] returnArr = (T[])(cloneA1 ? a1.Clone() : a2.Clone());
                if (cloneA1)
                {
                    for (int i = 0; i < a2.Length; i++)
                    {
                        if (a2[i] != null)
                        {
                            returnArr[i] = a2[i];
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < a1.Length; i++)
                    {
                        if (a1[i] != null)
                        {
                            returnArr[i] = a1[i];
                        }
                    }
                }
                return returnArr;
            }
            else if (a1 != null)
            {
                return (T[])a1.Clone();
            }
            else if (a2 != null)
            {
                return (T[])a2.Clone();
            }
            return null;
        }

        /// <summary> 
        /// Creates a new Array, and copies the contents from a1 if it is not null and not an empty string, and then copies the contents from a2 if it is not null 
        /// into the copy, and returns the copy. 
        /// 
        /// Example 
        /// a1 = [A, "", B] 
        /// a2 = [null, X, null] 
        /// return [A, X, B] 
        /// 
        /// </summary>
        /// <param name=" a1"> 
        /// </param>
        /// <param name=" a2"> 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static string[] MergeStringArrays(string[] a1, string[] a2)
        {
            if (a1 != null && a2 != null)
            {
                bool cloneA1 = a1.Length >= a2.Length;
                string[] returnArr = (string[])(cloneA1 ? a1.Clone() : a2.Clone());
                if (cloneA1)
                {
                    for (int i = 0; i < a2.Length; i++)
                    {
                        if (ObjectUtilCore.ValidString(a2[i]))
                        {
                            returnArr[i] = a2[i];
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < a1.Length; i++)
                    {
                        if (ObjectUtilCore.ValidString(a1[i]))
                        {
                            returnArr[i] = a1[i];
                        }
                    }
                }
                return returnArr;
            }
            else if (a1 != null)
            {
                return (string[])a1.Clone();
            }
            else if (a2 != null)
            {
                return (string[])a2.Clone();
            }
            return null;
        }


        public static ISet<T> ToSet<T>(T[] o)
        {
            return new HashSet<T>(o);
        }

        public static string ToString<T>(ICollection<T> collection)
        {
            return "[" + string.Join(", ", collection) + "]";
        }

        public static IDictionary<V, K> Invert<V, K>(IDictionary<K, V> map)
        {
            IDictionary<V, K> inv = new Dictionary<V, K>();

            foreach (var entry in map)
            {
                inv.Put(entry.Value, entry.Key);
            }

            return inv;
        }

        /// <summary> 
        /// Returns whether all of the strings are not null and have a length of greater than zero 
        /// after trimming the leading and trailing whitespace. 
        /// </summary>
        /// <param name=" strings"> 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static bool ValidString(params string[] strings)
        {
            if (strings == null)
            {
                return false;
            }
            foreach (string str in strings)
            {
                if (str == null || str.Length == 0)
                {
                    return false;
                }
                if (str.Trim().Length == 0)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary> 
        /// Returns whether at least one of the strings is not null and has a length of greater than zero 
        /// after trimming the leading and trailing whitespace 
        /// </summary>
        /// <param name=" strings"> 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static bool ValidOneString(params string[] strings)
        {
            if (strings == null)
            {
                return false;
            }
            foreach (string str in strings)
            {
                if (str != null && str.Length > 0)
                {
                    if (str.Trim().Length == 0)
                    {
                        return false;
                    }
                    return true;
                }
            }
            return false;
        }

        /// <summary> 
        /// Returns whether all of the objects are not null 
        /// </summary>
        /// <param name=" objs"> 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static bool ValidObject(params object[] objs)
        {
            if (objs == null)
            {
                return false;
            }
            foreach (object obj in objs)
            {
                if (obj == null)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary> 
        /// Returns true if at least one of the objects are not null 
        /// </summary>
        /// <param name=" objs"> 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static bool ValidOneObject(params object[] objs)
        {
            if (objs == null)
            {
                return false;
            }
            foreach (object obj in objs)
            {
                if (obj != null)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary> 
        /// Returns true if: 
        /// <ul> 
        ///   <li>Both Strings are null</li> 
        ///   <li>Both Strings are equal</li> 
        /// </ul> 
        /// </summary>
        /// <param name=" s1"> 
        /// </param>
        /// <param name=" s2"> 
        /// </param>
        /// <returns>
        /// </returns> 
        [Obsolete]
        public static bool EquivalentString(string s1, string s2)
        {
            return Equivalent(s1, s2);
        }

        /// <summary> 
        /// True is arrays are either: 
        /// <ol> 
        /// <li>both null</li> 
        /// <li>both same length and every object is ObjectUtil.equivalent</li> 
        /// </ol> 
        /// </summary>
        /// <param name=" o1"> 
        /// </param>
        /// <param name=" o2"> 
        /// </param>
        /// <param name=" skipIndexes"> an optional array of indexes to skip in the checks 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static bool EquivalentArray(object[] o1, object[] o2, params int[] skipIndexes)
        {
            if (o1 == null && o2 == null)
            {
                return true;
            }
            if (o1 == null && o2 != null)
            {
                return false;
            }
            if (o2 == null && o1 != null)
            {
                return false;
            }
            if (o1.Length != o2.Length)
            {
                return false;
            }

            for (int i = 0; i < o1.Length; i++)
            {
                if (skipIndexes != null && skipIndexes.Contains(i))
                {
                    continue;
                }
                if (!Equivalent(o1[i], o2[i]))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary> 
        /// Returns true if: 
        /// <ul> 
        ///   <li>Both Strings are null</li> 
        ///   <li>Both Strings are equal</li> 
        /// </ul> 
        /// </summary>
        /// <param name=" o1"> 
        /// </param>
        /// <param name=" o2"> 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static bool Equivalent(object o1, object o2)
        {
            if (o1 == null && o2 == null)
            {
                return true;
            }
            if (o1 == null && o2 != null)
            {
                return false;
            }
            if (o2 == null && o1 != null)
            {
                return false;
            }

            return o1.Equals(o2);
        }

        /// <summary> 
        /// Ensures the collection contains both the same content, and is in the same order 
        /// </summary>
        /// <param name=" c1"> 
        /// </param>
        /// <param name=" c2"> 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static bool EquivalentCollection<T>(ICollection<T> c1, ICollection<T> c2)
        {
            if (c1 == null && c2 == null)
            {
                return true;
            }
            if (c2 == null && c1 != null)
            {
                return false;
            }
            if (c1 == null && c2 != null)
            {
                return false;
            }
            if (!ContainsAll(c1, c2))
            {
                return false;
            }
            IEnumerator<T> it1 = c1.GetEnumerator();
            IEnumerator<T> it2 = c2.GetEnumerator();
            while (it1.MoveNext())
            {
                object obj1 = it1.Current;
                object obj2 = it2.Current;
                if (!Equivalent(obj1, obj2))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary> 
        /// Ensures the sets contain the same content, order is not important 
        /// </summary>
        /// <param name=" c1"> 
        /// </param>
        /// <param name=" c2"> 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static bool EquivalentSet<T>(ISet<T> c1, ISet<T> c2)
        {
            return ContainsAll(c1, c2);
        }

        /// <summary> 
        /// Ensures the maps contain the same content, by applying a equals to each value 
        /// </summary>
        /// <param name=" c1"> 
        /// </param>
        /// <param name=" c2"> 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static bool EquivalentMap<T, R>(IDictionary<T, R> c1, IDictionary<T, R> c2)
        {
            if (c1 == null && c2 == null)
            {
                return true;
            }
            if (c2 == null && c1 != null)
            {
                return false;
            }
            if (c1 == null && c2 != null)
            {
                return false;
            }
            if (!ContainsAll<T>(c1.Keys, c2.Keys))
            {
                return false;
            }
            foreach (T k in c1.Keys)
            {
                if (!Equivalent(c1[k], c2[k]))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary> 
        /// Trims the string in a string to string mapping 
        /// </summary>
        /// <param name=" map"> 
        /// </param>
        /// <returns>
        /// new map, trimmed 
        /// </returns> 
        public static IDictionary<string, string> TrimMap(IDictionary<string, string> map)
        {
            IDictionary<string, string> returnMap = new Dictionary<string, string>();
            foreach (string key in map.Keys)
            {
                returnMap.Put(SafeTrim(key), SafeTrim(map[key]));
            }
            return returnMap;
        }

        /// <summary> 
        /// Only trims not null strings if null then null is returned 
        /// </summary>
        /// <param name=" str"> 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static string SafeTrim(string str)
        {
            if (str == null)
            {
                return null;
            }
            return str.Trim();
        }

        public static bool ContainsAll<T>(ICollection<T> c1, ICollection<T> c2)
        {
            if (c1 == null && c2 == null)
            {
                return true;
            }
            if (c2 == null && c1 != null)
            {
                return false;
            }
            if (c1 == null && c2 != null)
            {
                return false;
            }
            if (c1.Count != c2.Count)
            {
                return false;
            }
            if (!c1.ContainsAll(c2))
            {
                return false;
            }
            if (!c2.ContainsAll(c1))
            {
                return false;
            }
            return true;
        }

        /// <summary> 
        /// Returns true if the collection is not null and has a size greater than zero. 
        /// </summary>
        /// <param name=" collection"> 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static bool ValidCollection<T>(ICollection<T> collection)
        {
            return collection != null && collection.Any();
        }

        /// <summary> 
        /// Does the specified Iterable only contain nulls ? 
        /// </summary>
        /// <param name=" array"> The Iterable object 
        /// </param>
        /// <returns>
        /// true if there are only nulls in the Iterable or there are no elements in the Iterable 
        /// </returns> 
        public static bool IsAllNulls<T>(IEnumerable<T> array)
        {
            foreach (object element in array)
            {
                if (element != null)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary> 
        /// Returns true if the array is not null and has a size greater than zero. 
        /// </summary>
        /// <param name=" arr"> 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static bool ValidArray(object[] arr)
        {
            return arr != null && arr.Length > 0;
        }

        /// <summary> 
        /// Returns true if the IDictionary is not null and has a size greater than zero. 
        /// </summary>
        /// <param name=" map"> 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static bool ValidMap<T, R>(IDictionary<T, R> map)
        {
            return map != null && map.Count > 0;
        }

        public static bool GetBooleanValue(string str)
        {
            if (str.Equals("true", StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }
            if (str.Equals("false", StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }
            throw new ArgumentException("Illegal value specified: " + str);
        }

        /// <summary> 
        /// Collect the elements in this stream to a list. 
        /// <p>This is a terminal operation.</p> 
        /// </summary>
        /// <param name=" str"> 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static List<T> CollectToList<T>(IEnumerable<T> stream)
        {
            return stream.ToList();
        }

        /// <summary> 
        /// Collect the elements in this stream to a ISet. 
        /// <p>This is a terminal operation.</p> 
        /// </summary>
        /// <param name=" str"> 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static ISet<T> CollectToSet<T>(IEnumerable<T> stream)
        {
            return new HashSet<T>(stream);
        }
    }
}