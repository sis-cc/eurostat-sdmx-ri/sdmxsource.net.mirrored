// -----------------------------------------------------------------------
// <copyright file="StringUtil.cs" company="EUROSTAT">
//   Date Created : 2023-05-15
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Io.Sdmx.Utils.Core.Object
{
    public class StringUtil
    {
        public static readonly string Empty = "";
        public static readonly char LF = '\n';

        /// <summary> 
        /// {@code \u000d} carriage return CR ('\r'). 
        /// 
        /// @see <a href="http://docs.oracle.com/javase/specs/jls/se7/html/jls-3.html#jls-3.10.6">JLF: Escape Sequences 
        ///      for Character and string Literals</a> 
        /// @since 2.2 
        /// </summary> 
        public static readonly char CR = '\r';
        private static readonly IDictionary<string, WeakReference<string>> s_manualCache
            = new ConcurrentDictionary<string, WeakReference<string>>(Environment.ProcessorCount, 100000);

        /// <summary> 
        /// Get string from the local cache so that only one copy of the string exists 
        /// </summary>
        /// <param name=" str"> 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static string ManualIntern(string str)
        {
            if (str == null)
            {
                return str;
            }
            WeakReference<string> cached = s_manualCache.ContainsKey(str) ? s_manualCache[str] : null;
            if (cached != null)
            {
                if (cached.TryGetTarget(out string value))
                {
                    return value;
                }
            }
            s_manualCache.Add(str, new WeakReference<string>(str));
            return str;
        }
        public static string[] ManualIntern(string[] str)
        {
            if (str != null)
            {
                for (int i = 0; i < str.Length; i++)
                {
                    str[i] = StringUtil.ManualIntern(str[i]);
                }
            }
            return str;
        }

        /// <summary> 
        /// Trims the string, removes line breaks and carriage returns, does a manual intern 
        /// </summary>
        /// <param name=" inputString"> 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static string CleanString(string inputString)
        {
            if (inputString == null)
            {
                return null;
            }
            return ManualIntern(RemoveLineBreaks(inputString).Trim());
        }

        public static string StripBOM(string aString)
        {
            if (aString == null)
            {
                return null;
            }
            byte[] asBytes = Encoding.Default.GetBytes(aString);
            if (asBytes.Length > 1 && asBytes[0] == 63)
            {
                // byte 63 is an undisplayable character, formally known as a BOM (converted using a )
                // There may be a number of 63 characters (encoding: ANSI_X3.4-1968) , so strip them all
                int startPos = 1;
                while (startPos < asBytes.Length && asBytes[startPos] == 63)
                {
                    startPos++;
                }
                byte[] slice = new byte[asBytes.Length - startPos];
                Array.Copy(asBytes, startPos, slice, 0, slice.Length);
                return Encoding.Default.GetString(slice);
            }
            if (asBytes.Length > 2)
            {
                if (asBytes[0] == 254 && asBytes[1] == 255)
                {
                    //BOM UTF-16 (Windows)
                    byte[] slice = new byte[asBytes.Length - 2];
                    Array.Copy(asBytes, 2, slice, 0, slice.Length);
                    return Encoding.Unicode.GetString(slice);
                }
            }

            if (asBytes.Length > 3)
            {
                if (asBytes[0] == 239 && asBytes[1] == 187 && asBytes[2] == 191)
                {
                    //BOM UTF-8 (Windows)
                    byte[] slice = new byte[asBytes.Length - 3];
                    Array.Copy(asBytes, 3, slice, 0, slice.Length);
                    return Encoding.UTF8.GetString(slice);
                }
            }

            if (asBytes.Length > 4)
            {
                if (asBytes[0] == 239 && asBytes[1] == 187 && asBytes[2] == 191)
                {
                    //BOM UTF-16 (Unix)
                    byte[] slice = new byte[asBytes.Length - 4];
                    Array.Copy(asBytes, 4, slice, 0, slice.Length);
                    return Encoding.Unicode.GetString(slice);
                }
            }

            if (asBytes.Length > 6)
            {
                if (asBytes[0] == 239 && asBytes[1] == 191 && asBytes[2] == 189 &&
                    asBytes[3] == 190 && asBytes[4] == 189 && asBytes[5] == 191)
                {
                    //BOM UTF-8 (Unix)
                    byte[] slice = new byte[asBytes.Length - 6];
                    Array.Copy(asBytes, 6, slice, 0, slice.Length);
                    return Encoding.UTF8.GetString(slice);
                }
            }
            return aString;
        }

        public static bool HasBOM(string aString)
        {
            if (aString == null)
            {
                return false;
            }
            byte[] asBytes = Encoding.UTF8.GetBytes(aString);
            if (asBytes.Length > 1 && asBytes[0] == 63)
            {
                // byte 63 is an undisplayable character, formally known as a BOM (converted using a )
                // There may be a number of 63 characters (encoding: ANSI_X3.4-1968) , so strip them all
                return true;
            }
            if (asBytes.Length > 2)
            {
                if (asBytes[0] == 254 && asBytes[1] == 255)
                {
                    //BOM UTF-16 (Windows)
                    return true;
                }
            }

            if (asBytes.Length > 3)
            {
                if (asBytes[0] == 239 && asBytes[1] == 187 && asBytes[2] == 191)
                {
                    //BOM UTF-8 (Windows)
                    return true;
                }
            }

            if (asBytes.Length > 4)
            {
                if (asBytes[0] == 255 && asBytes[1] == 254 && asBytes[2] == 0 && asBytes[3] == 0)
                {
                    //BOM UTF-32 (Windows)
                    return true;
                }
            }

            if (asBytes.Length > 4)
            {
                if (asBytes[0] == 0 && asBytes[1] == 0 && asBytes[2] == 254 && asBytes[3] == 255)
                {
                    //BOM UTF-32 Big-Endian (Windows)
                    return true;
                }
            }

            if (asBytes.Length > 4)
            {
                if (asBytes[0] == 255 && asBytes[1] == 254 && asBytes[2] == 0 && asBytes[3] != 0)
                {
                    //BOM UTF-16 (Unix)
                    return true;
                }
            }

            if (asBytes.Length > 6)
            {
                if (asBytes[0] == 0 && asBytes[1] == 0 && asBytes[2] == 254 && asBytes[3] == 255 && asBytes[4] != 0 && asBytes[5] != 0)
                {
                    //BOM UTF-32 Big-Endian (Unix)
                    return true;
                }
            }

            if (asBytes.Length > 6)
            {
                if (asBytes[0] == 239 && asBytes[1] == 187 && asBytes[2] == 191 && asBytes[3] != 0)
                {
                    //BOM UTF-8 (Unix)
                    return true;
                }
            }

            return false;
        }

        public static string RemoveLineBreaks(string inputString)
        {
            if (inputString == null)
            {
                return null;
            }
            inputString = inputString.Replace("\\u000a", "");    // Removes all new-line characters
            inputString = inputString.Replace("\\u000d", "");    // Removes all carriage-return characters
            return inputString;
        }

        private static class WhiteSpace
        {
            private static readonly string WHITESPACE_CHARS =
                "\u2002\u3000\r\u0085\u200A\u2005\u2000\u3000\u2029\u000B\u3000\u2008\u2003\u205F\u3000\u1680" +
                "\u0009\u0020\u2006\u2001\u202F\u00A0\u000C\u2009\u3000\u2004\u3000\u3000\u2028\n\u2007\u3000";

            private static readonly int SHIFT = 32 - CountLeadingZeros(WHITESPACE_CHARS.Length - 1);
            private static readonly int MULTIPLIER = 1682554634;

            public static bool Matches(char c)
            {
                return WHITESPACE_CHARS[(MULTIPLIER * c) >> SHIFT] == c;
            }

            private static int CountLeadingZeros(int value)
            {
                if (value == 0)
                {
                    return 32;
                }

                int count = 0;
                while ((value & 0x80000000) == 0)
                {
                    value <<= 1;
                    count++;
                }
                return count;
            }
        }

        /// <summary> 
        /// Trims all whitespace characters from a string 
        /// </summary>
        /// <param name=" string"> 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static string Trim(string value)
        {
            int len = value.Length;
            int first;
            int last;

            for (first = 0; first < len; first++)
            {
                if (!WhiteSpace.Matches(value[first]))
                {
                    break;
                }
            }
            for (last = len - 1; last > first; last--)
            {
                if (!WhiteSpace.Matches(value[last]))
                {
                    break;
                }
            }

            return value.Substring(first, last + 1).ToString();
        }

        /// <summary> 
        /// \h is a horizontal whitespace character: [ \t\xA0\u1680\u180e\u2000-\u200a\u202f\u205f\u3000] 
        /// 
        /// </summary>
        /// <param name=" inputString"> 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static string RemoveHorizontalLineBreaks(string inputString)
        {
            if (inputString == null)
            {
                return null;
            }
            return inputString.Trim().Replace("(^\\h*)|(\\h*$)", "");
        }

        // Chopping
        //-----------------------------------------------------------------------
        /// <summary> 
        /// <p>Remove the last character from a string.</p> 
        /// 
        /// <p>If the string ends in {@code \r\n}, then remove both 
        /// of them.</p> 
        /// 
        /// <pre> 
        /// StringUtils.Chop(null)          = null 
        /// StringUtils.Chop("")            = "" 
        /// StringUtils.Chop("abc \r")      = "abc " 
        /// StringUtils.Chop("abc\n")       = "abc" 
        /// StringUtils.Chop("abc\r\n")     = "abc" 
        /// StringUtils.Chop("abc")         = "ab" 
        /// StringUtils.Chop("abc\nabc")    = "abc\nab" 
        /// StringUtils.Chop("a")           = "" 
        /// StringUtils.Chop("\r")          = "" 
        /// StringUtils.Chop("\n")          = "" 
        /// StringUtils.Chop("\r\n")        = "" 
        /// </pre> 
        /// 
        /// </summary>
        /// <param name=" str">  the string to chop last character from, may be null 
        /// </param>
        /// <returns>
        /// string without last character, {@code null} if null string input 
        /// </returns> 
        public static string Chop(string str)
        {
            if (str == null)
            {
                return null;
            }
            int strLen = str.Length;
            if (strLen < 2)
            {
                return Empty;
            }
            int lastIdx = strLen - 1;
            string ret = str.Substring(0, lastIdx);
            char last = str[lastIdx];
            if (last == LF && ret[lastIdx - 1] == CR)
            {
                return ret.Substring(0, lastIdx - 1);
            }
            return ret;
        }

        /// <summary> 
        /// Splits a string which may include regex, and ensures that the string does not split 
        /// on anything contained in the regex.  Example: 
        /// 
        /// (.*):\\1[^:]:C 
        /// 
        /// Would result in the following if split on the colon 
        /// <ol> 
        ///   <li>(.*)</li> 
        ///   <li>\\1[^:]</li> 
        ///   <li>C</li> 
        /// </ol> 
        /// 
        /// 
        /// </summary>
        /// <param name=" source"> 
        /// </param>
        /// <param name=" splitOn"> 
        /// </param>
        /// <param name=" limit"> - the max split items, -1 to indicate no limit 
        /// </param>
        /// <returns>
        /// 
        /// </returns> 
        public static string[] SplitWithRegex(string source, string splitOn, int limit)
        {
            if (limit == 1)
            {
                return new string[] { source };
            }
            IList<string> targetsList = new List<string>(5);
            StringBuilder sbx = new StringBuilder();
            char colon = splitOn[0];
            char openbracket = '[';
            char openparenthisis = '(';
            char closebracket = ']';
            char closeparenthisis = ')';

            int inbracket = 0;
            bool limitHit = false;
            foreach (char c in source.ToCharArray())
            {
                if (!limitHit)
                {
                    if (c == colon && inbracket == 0)
                    {
                        targetsList.Add(sbx.ToString());
                        sbx = new StringBuilder();
                        limitHit = targetsList.Count == limit;
                        continue;
                    }
                    else if (c == openbracket || c == openparenthisis)
                    {
                        inbracket++;
                    }
                    else if (c == closebracket || c == closeparenthisis)
                    {
                        inbracket--;
                    }
                }
                sbx.Append(c);
            }
            targetsList.Add(sbx.ToString());
            return targetsList.ToArray();
        }
    }
}