// -----------------------------------------------------------------------
// <copyright file="StaxWriter.cs" company="EUROSTAT">
//   Date Created : 2023-06-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Utils.Core.Xml
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;
    using Estat.Sri.SdmxXmlConstants;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Util;

    public class StaxWriter
    {
        //NAMESPACES
        public static readonly string XmlNs = "http://www.w3.org/XML/1998/namespace";
        public static readonly string XsiNs = "http://www.w3.org/2001/XMLSchema-instance";

        private readonly XmlWriter _writer;
        private readonly Namespace[] _namespaces;
        private readonly string _schemaLocation;

        public StaxWriter(Stream outStream, string schemaLocation, bool prettyPrint, params Namespace[] ns)
        {
            _namespaces = ns;
            XmlWriterSettings settings = new XmlWriterSettings
            {
                Encoding = Encoding.UTF8,
                Indent = prettyPrint
            };
            try
            {
                _writer = XmlWriter.Create(outStream, settings);
                _schemaLocation = schemaLocation;

            }
            catch (ArgumentNullException e)
            {
                throw new SdmxException("Could not create Xml Writer", e);
            }
        }

        public XmlWriter GetWriter()
        {
            return _writer;
        }

        public void StartDocument(Namespace ns, string element)
        {
            try
            {
                _writer.WriteStartDocument();
                if (ns != null)
                {
                    _writer.WriteStartElement(ns.GetNamespacePrefix(), element, ns.GetNamespaceURL());
                }
                else
                {
                    _writer.WriteStartElement(element);
                }
                WriteNamespace("xsi", XsiNs);
                WriteNamespace("xml", XmlNs);
                if (_namespaces != null)
                {
                    foreach (Namespace currnetNs in _namespaces)
                    {
                        WriteNamespace(currnetNs.GetNamespacePrefix(), currnetNs.GetNamespaceURL());
                    }
                }
                if (ObjectUtil.ValidString(_schemaLocation))
                {
                    _writer.WriteAttributeString(XsiNs, "schemaLocation", _schemaLocation);
                }
            }
            catch (Exception e)
            {
                throw new SdmxException("Could not create Xml Writer", e);
            }
        }

        public void WriteNamespace(string prefix, string url)
        {
            if (ObjectUtil.ValidString(prefix) && ObjectUtil.ValidString(url))
            {
                try
                {
                    _writer.WriteAttributeString(XmlConstants.Xmlns, prefix, null, url);
                }
                catch (Exception e)
                {
                    throw new SdmxException("Could not create Xml Writer", e);
                }
            }
        }

        public void WriteNamespace(Namespace ns)
        {
            if (ns != null)
            {
                WriteNamespace(ns.GetNamespacePrefix(), ns.GetNamespaceURL());
            }
        }

        public void WriteAttribute(string id, bool? value)
        {
            if (value != null)
            {
                WriteAttribute(id, value.ToString());
            }
        }

        public void WriteAttribute(string id, int? value)
        {
            if (value != null)
            {
                WriteAttribute(id, value.ToString());
            }
        }

        public void WriteAttribute(string id, string value)
        {
            if (ObjectUtil.ValidString(value))
            {
                try
                {
                    // this is a hack to allow writing attributes with a namespace prefix
                    // example the language attribute "xml:lang" (see StaxTextWriterUtilV3)
                    var split = id.Split(':');
                    if (split.Length == 2)
                    {
                        _writer.WriteAttributeString(split[0], split[1], null, value);
                    }
                    else
                    {
                        _writer.WriteAttributeString(id, value);
                    }
                }
                catch (Exception e)
                {
                    throw new SdmxException("Could not write attribute '" + id + "' with value '" + value + "'", e);
                }
            }
        }
        public void WriteXMLLang(string value)
        {
            if (ObjectUtil.ValidString(value))
            {
                try
                {
                    _writer.WriteAttributeString("xml", XmlNs, "lang", value);
                }
                catch (Exception e)
                {
                    throw new SdmxException("Could not write xml:lang with value '" + value + "'", e);
                }
            }
        }

        public void WriteAttributes(IDictionary<string, string> attributes)
        {
            if (attributes != null)
            {
                foreach (string key in attributes.Keys)
                {
                    WriteAttribute(key, attributes[key]);
                }
            }
        }

        /// <summary> 
        /// Writes an element with the given text, if the given text is not present, then the element will not be written 
        /// </summary>
        /// <param name=" ns"> 
        /// </param>
        /// <param name=" elementName"> 
        /// </param>
        /// <param name=" elementText"> 
        /// </param> 
        public void WriteElement(Namespace ns, string elementName, string elementText)
        {
            if (ObjectUtil.ValidString(elementText))
            {
                WriteElement(ns, elementName, elementText, null);
            }
        }

        public void WriteElement(Namespace ns, string elementName, IDictionary<string, string> attributes)
        {
            WriteElement(ns, elementName, null, attributes, false);
        }

        public void WriteElement(Namespace ns, string elementName, string elementText, IDictionary<string, string> attributes)
        {
            WriteElement(ns, elementName, elementText, attributes, true);
        }

        private void WriteElement(Namespace ns, string elementName, string elementText, IDictionary<string, string> attributes, bool endElement)
        {
            if (elementName != null)
            {
                WriteStartElement(ns, elementName);
            }
            WriteAttributes(attributes);
            if (elementText != null)
            {
                //TODO SDMXRI-2080 : check if this is correct
                _writer.WriteString(elementText);
            }
            if (endElement)
            {
                WriteEndElement();
            }
        }

        public void WriteStartElement(Namespace ns, string elementName)
        {
            try
            {
                if (ns != null)
                {
                    _writer.WriteStartElement(ns.GetNamespacePrefix(), elementName, ns.GetNamespaceURL());
                }
                else
                {
                    _writer.WriteStartElement(elementName);
                }
            }
            catch (Exception e)
            {
                throw new SdmxException("Could not create Xml Writer", e);
            }
        }

        public void WriteEndElement()
        {
            try
            {
                _writer.WriteEndElement();
            }
            catch (Exception e)
            {
                throw new SdmxException("Could not create Xml Writer", e);
            }
        }

        public void Close()
        {
            try
            {
                _writer.WriteEndDocument();
                _writer.Close();
            }
            catch (Exception e)
            {
                throw new SdmxException("Could not close Xml Writer", e);
            }
        }

        internal void WriteElementText(string val)
        {
            if (val != null)
            {
                try
                {
                    _writer.WriteString(val);
                }
                catch (Exception e)
                {
                    throw new SdmxException("Could not create XML Writer", e);
                }
            }
        }
    }
}