// -----------------------------------------------------------------------
// <copyright file="StaxReader.cs" company="EUROSTAT">
//   Date Created : 2023-06-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Utils.Core.Xml
{
    using Io.Sdmx.Api.Xml;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Xml;
    using Org.Sdmxsource.Util.Io;
    using Org.Sdmxsource.Util.Extensions;
    using System.Reflection;

    public class StaxReader : IStaxReader
    {
        private Stack<string> _stack = new Stack<string>();
        private readonly IReadableDataLocation _rdl;
        private Stream _stream;
        private XmlReader _streamReader;

        private IDictionary<string, string> _attributes;
        private string _elementName;
        private bool _isStartElement;
        private bool _pop;
        private bool _hasDocType = false;

        public StaxReader(IReadableDataLocation rdl)
        {
            this._rdl = rdl;
            Reset();
        }

        public void Reset()
        {
            _pop = false;
            _hasDocType = false;
            _stack = new Stack<string>();
            XmlReaderSettings settings = new XmlReaderSettings
            {
                DtdProcessing = DtdProcessing.Ignore
            };

            try
            {
                _stream = _rdl.InputStream;
                _streamReader = XmlReader.Create(
                    new StreamReader(_stream, Encoding.GetEncoding("UTF-8")), settings);
            }
            catch (XmlException e)
            {
                Close();
                throw new StaxException(e);
            }
        }

        public bool MoveNextNode()
        {
            _attributes = null;
            _elementName = null;

            if (_pop)
            {
                // Pop as there was a call to get text from the node which moved onto the end node
                _stack.Pop();
                _pop = false;
            }

            try
            {
                while (_streamReader.Read())
                {
                    XmlNodeType eventType = _streamReader.NodeType;
                    if (eventType == XmlNodeType.Element)
                    {
                        _isStartElement = true;
                        _elementName = _streamReader.LocalName;
                        _stack.Push(_elementName);
                        return true;
                    }
                    if (eventType == XmlNodeType.EndElement)
                    {
                        _elementName = _streamReader.LocalName;
                        if (_stack.Count == 0)
                        {
                            return false;
                        }
                        else
                        {
                            _stack.Pop();
                        }
                        _isStartElement = false;
                        return true;
                    }
                    if (eventType == XmlNodeType.DocumentType)
                    {
                        _hasDocType = true;
                    }
                }
                return false;
            }
            catch (XmlException e)
            {
                Close();
                throw new StaxException(e);
            }
        }

        public bool MoveNextStartNode()
        {
            while (MoveNextNode())
            {
                if (IsStartElement)
                {
                    return true;
                }
            }
            return false;
        }

        public string GetElementNamespace()
        {
            return _streamReader.NamespaceURI;
        }

        public void SkipCurrentNode()
        {
            SkipNode();
        }

        public bool JumpToNode(string findNodeName)
        {
            return JumpToNode(findNodeName, null);
        }

        /// <summary> 
        /// Moves the parser position forward to the point after this node and all children of this node 
        /// </summary>
        /// <param name=" parser"> 
        /// </param> 
        public void SkipNode()
        {
            while (MoveNextNode())
            {
                if (IsStartElement)
                {
                    SkipNode();
                }
                else
                {
                    return;
                }
            }
        }

        /// <summary> 
        /// Moves the parser position forward to the next instance of the node with the given name 
        /// </summary>
        /// <param name=" parser"> 
        /// </param> 
        public bool SkipToNode(string nodeName)
        {
            while (MoveNextStartNode())
            {
                if (ElementName.Equals(nodeName))
                {
                    return true;
                }
            }
            return false;
        }

        public bool JumpToNode(string findNodeName, string doNotProcessPastNodeName)
        {
            string nodeName;
            while (MoveNextStartNode())
            {
                nodeName = ElementName;
                if (IsStartElement)
                {
                    if (nodeName.Equals(findNodeName))
                    {
                        return true;
                    }
                }
                else
                {
                    if (doNotProcessPastNodeName != null)
                    {
                        if (nodeName.Equals(doNotProcessPastNodeName))
                        {
                            return false;
                        }
                    }
                }
            }
            return false;
        }

        /// <summary> 
        /// Moves the parser position forward to the next instance of the end node given name 
        /// </summary>
        /// <param name=" parser"> 
        /// </param> 
        /// <exception cref="XmlException"></exception>
        public bool SkipToEndNode(string nodeName)
        {
            while (MoveNextNode())
            {
                if (!IsStartElement && ElementName.Equals(nodeName))
                {
                    return true;
                }
            }
            return false;
        }

        public string GetAttributeValue(string attrId)
        {
            return GetAttributeValue(null, attrId);
        }

        public string GetAttributeValue(string ns, string attrId)
        {
            if (_attributes == null)
            {
                GetAttributes();
            }
            return _attributes[attrId];
        }


        /// <summary> 
        /// Returns the XML path to the current element, for example Root/Element1/ChildElemet 
        /// </summary>
        /// <returns>
        /// 
        /// </returns> 
        public string GetXMLPath()
        {
            StringBuilder sb = new StringBuilder();
            string concat = "";
            foreach (string stackEl in _stack)
            {
                sb.Append(concat + stackEl);
                concat = "/";
            }
            return sb.ToString();
        }

        public string ElementName => _elementName;

        public string GetElementText()
        {
            try
            {
                if (_streamReader.NodeType != XmlNodeType.Element)
                {
                    _streamReader.MoveToContent();
                }
                return _streamReader.ReadElementContentAsString();
            }
            catch (XmlException e)
            {
                Close();
                throw new StaxException(e);
            }
            finally
            {
                _pop = true;
            }
        }

        public bool IsStartElement => _isStartElement;

        public bool IsEmptyElement => _streamReader.IsEmptyElement;

        public IDictionary<string, string> GetAttributes()
        {
            if (_attributes == null)
            {
                _attributes = new Dictionary<string, string>();
            }
            for (int i = 0; i < AttributeCount; i++)
            {
                string attrName = GetAttributeLocalName(i);
                _attributes.Put(attrName, GetAttributeValue(i));
            }
            return new Dictionary<string, string>(_attributes);
        }

        public bool HasDocType => _hasDocType;

        public int AttributeCount => _streamReader.AttributeCount;

        public string GetAttributeLocalName(int i)
        {
            _streamReader.MoveToAttribute(i);
            return _streamReader.LocalName;
        }

        public string GetAttributeValue(int i)
        {
            return _streamReader.GetAttribute(i);
        }

        public string GetAttributePrefix(int i)
        {
            _streamReader.MoveToAttribute(i);
            return _streamReader.Prefix;
        }

        public void Close()
        {
            StreamUtil.CloseStream(_stream);
        }
    }
}