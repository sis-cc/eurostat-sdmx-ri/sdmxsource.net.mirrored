// -----------------------------------------------------------------------
// <copyright file="XmlUtils.cs" company="EUROSTAT">
//   Date Created : 2023-06-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Utils.Core.Xml
{
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using System;
    using System.IO;
    using System.Text;
    using System.Xml;

    public class XmlUtils
    {
        public static XmlDocument Document(Stream inputStream)
        {
            try
            {
                XmlDocument document = new XmlDocument();
                document.XmlResolver = new XmlSecureResolver(new XmlUrlResolver(), string.Empty);
                document.Load(inputStream);
                return document;
            }
            catch (XmlException e)
            {
                throw new SdmxSemmanticException("Could not read xml document", e);
            }
        }

        public static XmlNodeList SearchForNodeList(XmlDocument document, string xpath)
        {
            try
            {
                return (XmlNodeList)document.CreateNavigator().Evaluate(xpath);
            }
            catch (Exception e)
            {
                throw new SdmxSemmanticException("Could not read xml document", e);
            }
        }

        /// <summary> 
        /// Returns true if the specified ReadableDataLocation contains valid XML. 
        /// </summary>
        /// <param name=" sourceData"> The ReadableDataLocation to test 
        /// </param>
        /// <returns>
        /// true if the specified  ReadableDataLocation contains valid XML. 
        /// </returns> 
        /* TODO SDMXCORE
        public static bool IsXML(ReadableDataLocation rdl) {
            bool wasDisabled  = SdmxException.IsDisabledExceptionTrace();
            SdmxException.DisableExceptionTrace(true);
            StaxReader reader = null;
            try {
                //Try and read it as a JSON dataset
                reader = new StaxReader(rdl);
                reader.MoveNextNode();
                return true;
            } catch(Throwable th) {
                return false;
            } finally {
                if(reader != null) {
                    reader.Close();
                }
                SdmxException.DisableExceptionTrace(wasDisabled);
            }
        }

         */

        /// <summary> 
        /// Formats the specified XML string so it has indentation 
        /// </summary>
        /// <param name=" unformattedXml"> A string representation of some XML 
        /// </param>
        /// <returns>
        /// a formatted version of the input XML. 
        /// </returns> 
        public static string FormatXml(string unformattedXml)
        {
            try
            {
                XmlDocument document = new XmlDocument();
                document.XmlResolver = new XmlSecureResolver(new XmlUrlResolver(), string.Empty);
                document.LoadXml(unformattedXml);

                XmlWriterSettings settings = new XmlWriterSettings
                {
                    Indent = true,
                    IndentChars = "\t",
                    Encoding = Encoding.UTF8
                };

                StringBuilder stringBuilder = new StringBuilder();
                using (XmlWriter writer = XmlWriter.Create(stringBuilder, settings))
                {
                    document.Save(writer);
                }

                return stringBuilder.ToString();
            }
            catch (Exception e)
            {
                throw new SdmxException("Could not format xml document", e);
            }
        }
    }
}