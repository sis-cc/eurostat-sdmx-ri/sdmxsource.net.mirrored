// -----------------------------------------------------------------------
// <copyright file="StaxException.cs" company="EUROSTAT">
//   Date Created : 2023-06-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Utils.Core.Xml
{
    using System.Xml;
    using Org.Sdmxsource.Sdmx.Api.Exception;

    public class StaxException : SdmxSyntaxException
    {
        private int _lineNumber;
        private int _column;

        public StaxException(XmlException e)
            : this(e.LineNumber, e.LinePosition, e.Message)
        {
        }

        private StaxException(int lineNumber, int column, string str) : base(str)
        {
            this._lineNumber = lineNumber;
            this._column = column;
        }

        public int GetLineNumber()
        {
            return _lineNumber;
        }

        public int GetColumn()
        {
            return _column;
        }
    }
}