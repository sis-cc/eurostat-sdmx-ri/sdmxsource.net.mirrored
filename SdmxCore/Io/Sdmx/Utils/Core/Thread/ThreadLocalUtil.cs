// -----------------------------------------------------------------------
// <copyright file="ThreadLocalUtil.cs" company="EUROSTAT">
//   Date Created : 2023-05-16
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Utils.Core.Thread
{
    using System;
    using System.Collections.Generic;
    using System.Threading;

    public class ThreadLocalUtil
    {
        public static readonly string ClientRequest = "CLIENT_REQUEST";
        public static readonly string Session = "SESSION";
        public static readonly string WebServiceType = "WEB_SERVICE_TYPE";
        public static readonly string Action = "ACTION";
        public static readonly string UuidKey = "UUID";
        public static readonly string RequestOrigin = "REQUEST_ORIGIN";

        private static LocalObjectStore _localObjectStore = new LocalObjectStore();

        private class LocalObjectStore : ThreadLocal<IDictionary<object, object>>
        {
            public LocalObjectStore()
            {
                Value = new Dictionary<object, object>();
            }
        }

        private ThreadLocalUtil() { }

        public static void CreateUid()
        {
            StoreOnThread(UuidKey, Guid.NewGuid().ToString());
        }

        public static void RemoveUid()
        {
            RetrieveFromThread(UuidKey);
        }

        /// <summary> 
        /// Gets a Unique id for this thread 
        /// </summary>
        /// <returns>
        /// 
        /// </returns> 
        public static string GetUid()
        {
            if (Contains(UuidKey))
            {
                return (string)_localObjectStore.Value[UuidKey];
            }
            return null;
        }

        //TODO SDMXCORE IF NEEDED
        /*
        public static void StoreClientRequest(ClientRequest clientRequest) {
            StoreOnThread(ClientRequest, clientRequest);
        }

        public static ClientRequest ClientRequest => (ClientRequest) localObjectStore.Get().Get(ClientRequest);
            return null;
        }

         */

        public static void StoreOnThread(object key, object obj)
        {
            _localObjectStore.Value.Add(key, obj);
        }

        public static object RetrieveFromThread(object key)
        {
            return _localObjectStore.Value[key];
        }

        public static bool Contains(object key)
        {
            return _localObjectStore.Value.ContainsKey(key);
        }

        public static void ClearFromThread(object key)
        {
            _localObjectStore.Value.Remove(key);
        }

        /// <summary> 
        /// Remove everything stored on this thread 
        /// </summary> 
        public static void ClearThread()
        {
            _localObjectStore.Value.Clear();
        }

        /// <summary> 
        /// Returns a copy of everything stored on the local thread 
        /// </summary>
        /// <returns>
        /// </returns> 
        public static IDictionary<object, object> ThreadContents => new Dictionary<object, object>(_localObjectStore.Value);

        public static void Close()
        {
            _localObjectStore = null;
        }
    }
}