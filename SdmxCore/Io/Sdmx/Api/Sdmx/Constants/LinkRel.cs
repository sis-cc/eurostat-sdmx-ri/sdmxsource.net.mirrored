// -----------------------------------------------------------------------
// <copyright file="LinkRel.cs" company="EUROSTAT">
//   Date Created : 2023-05-09
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Api.Sdmx.Constants
{
    using System;
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary> 
    ///  A list of rel types for the {@link ILinkobject} 
    /// </summary> 
    public enum LinkRelEnumType
    {
        Agencyscheme,
        Alternate,
        Appendix,
        Bookmark,
        Categorisation,
        Categoryscheme,
        Chapter,
        Codelist,
        Conceptscheme,
        Constraint,
        Contents,
        Copyright,
        Current,
        Dataflow,
        Dataconsumerscheme,
        Dataproviderscheme,
        Datastructure,
        Describedby,
        Edit,
        EditMedia,
        Enclosure,
        First,
        Glossary,
        Help,
        Hierarchicalcodelist,
        Hub,
        Index,
        Last,
        LatestVersion,
        License,
        Metadata,
        Metadataflow,
        Next,
        NextArchive,
        Organisationunitscheme,
        Payment,
        Prev,
        PredecessorVersion,
        Previous,
        PrevArchive,
        Process,
        Provisionagreement,
        Related,
        Replies,
        Reportingtaxonomy,
        Section,
        Self,
        Service,
        Start,
        Structure,
        Structureset,
        Stylesheet,
        Subsection,
        SuccessorVersion,
        UP,
        VersionHistory,
        Via,
        WorkingCopy,
        WorkingCopyOF
    }

    public class LinkRel : BaseConstantType<LinkRelEnumType>
    {
        private static readonly Dictionary<LinkRelEnumType, LinkRel> _instances =
            new Dictionary<LinkRelEnumType, LinkRel>()
            {
                { 
                    LinkRelEnumType.Agencyscheme,
                    new LinkRel(LinkRelEnumType.Agencyscheme, "agencyscheme")
                },
                { 
                    LinkRelEnumType.Alternate,
                    new LinkRel(LinkRelEnumType.Alternate, "alternate")
                },
                { 
                    LinkRelEnumType.Appendix,
                    new LinkRel(LinkRelEnumType.Appendix, "appendix") }
                ,
                { 
                    LinkRelEnumType.Bookmark,
                    new LinkRel(LinkRelEnumType.Bookmark, "bookmark")
                },
                { 
                    LinkRelEnumType.Categorisation,
                    new LinkRel(LinkRelEnumType.Categorisation, "categorisation")
                },
                { 
                    LinkRelEnumType.Categoryscheme,
                    new LinkRel(LinkRelEnumType.Categoryscheme, "categoryscheme")
                },
                { 
                    LinkRelEnumType.Chapter,
                    new LinkRel(LinkRelEnumType.Chapter, "chapter")
                },
                { 
                    LinkRelEnumType.Codelist,
                    new LinkRel(LinkRelEnumType.Codelist, "codelist") 
                },
                { 
                    LinkRelEnumType.Conceptscheme,
                    new LinkRel(LinkRelEnumType.Conceptscheme, "conceptscheme") 
                },
                { 
                    LinkRelEnumType.Constraint,
                    new LinkRel(LinkRelEnumType.Constraint, "constraint") 
                },
                { 
                    LinkRelEnumType.Contents,
                    new LinkRel(LinkRelEnumType.Contents, "contents") 
                },
                { 
                    LinkRelEnumType.Copyright,
                    new LinkRel(LinkRelEnumType.Copyright, "copyright") 
                },
                { 
                    LinkRelEnumType.Current,
                    new LinkRel(LinkRelEnumType.Current, "current") 
                },
                { 
                    LinkRelEnumType.Dataflow,
                    new LinkRel(LinkRelEnumType.Dataflow, "dataflow") 
                },
                { 
                    LinkRelEnumType.Dataconsumerscheme,
                    new LinkRel(LinkRelEnumType.Dataconsumerscheme, "dataconsumerscheme") 
                },
                { 
                    LinkRelEnumType.Dataproviderscheme,
                    new LinkRel(LinkRelEnumType.Dataproviderscheme, "dataproviderscheme") 
                },
                { 
                    LinkRelEnumType.Datastructure,
                    new LinkRel(LinkRelEnumType.Datastructure, "datastructure") 
                },
                { 
                    LinkRelEnumType.Describedby,
                    new LinkRel(LinkRelEnumType.Describedby, "describedby") 
                },
                { 
                    LinkRelEnumType.Edit,
                    new LinkRel(LinkRelEnumType.Edit, "edit") 
                },
                { 
                    LinkRelEnumType.EditMedia,
                    new LinkRel(LinkRelEnumType.EditMedia, "edit-media") 
                },
                { 
                    LinkRelEnumType.Enclosure,
                    new LinkRel(LinkRelEnumType.Enclosure, "enclosure") 
                },
                { 
                    LinkRelEnumType.First,
                    new LinkRel(LinkRelEnumType.First, "first") 
                },
                { 
                    LinkRelEnumType.Glossary,
                    new LinkRel(LinkRelEnumType.Glossary, "glossary") 
                },
                { 
                    LinkRelEnumType.Help,
                    new LinkRel(LinkRelEnumType.Help, "help") 
                },
                { 
                    LinkRelEnumType.Hierarchicalcodelist,
                    new LinkRel(LinkRelEnumType.Hierarchicalcodelist, "hierarchicalcodelist") 
                },
                { 
                    LinkRelEnumType.Hub,
                    new LinkRel(LinkRelEnumType.Hub, "hub") 
                },
                { 
                    LinkRelEnumType.Index,
                    new LinkRel(LinkRelEnumType.Index, "index") 
                },
                { 
                    LinkRelEnumType.Last,
                    new LinkRel(LinkRelEnumType.Last, "last") 
                },
                { 
                    LinkRelEnumType.LatestVersion,
                    new LinkRel(LinkRelEnumType.LatestVersion, "latest-version") 
                },
                { 
                    LinkRelEnumType.License,
                    new LinkRel(LinkRelEnumType.License, "license") 
                },
                { 
                    LinkRelEnumType.Metadata,
                    new LinkRel(LinkRelEnumType.Metadata, "metadata") 
                },
                { 
                    LinkRelEnumType.Metadataflow,
                    new LinkRel(LinkRelEnumType.Metadataflow, "metadataflow") 
                },
                { 
                    LinkRelEnumType.Next,
                    new LinkRel(LinkRelEnumType.Next, "next") 
                },
                { 
                    LinkRelEnumType.NextArchive,
                    new LinkRel(LinkRelEnumType.NextArchive, "next-archive") 
                },
                { 
                    LinkRelEnumType.Organisationunitscheme,
                    new LinkRel(LinkRelEnumType.Organisationunitscheme, "organisationunitscheme") 
                },
                { 
                    LinkRelEnumType.Payment,
                    new LinkRel(LinkRelEnumType.Payment, "payment") 
                },
                { 
                    LinkRelEnumType.Prev,
                    new LinkRel(LinkRelEnumType.Prev, "prev") 
                },
                { 
                    LinkRelEnumType.PredecessorVersion,
                    new LinkRel(LinkRelEnumType.PredecessorVersion, "predecessor-version") 
                },
                { 
                    LinkRelEnumType.Previous,
                    new LinkRel(LinkRelEnumType.Previous, "previous") 
                },
                { 
                    LinkRelEnumType.PrevArchive,
                    new LinkRel(LinkRelEnumType.PrevArchive, "prev-archive") 
                },
                { 
                    LinkRelEnumType.Process,
                    new LinkRel(LinkRelEnumType.Process, "process") 
                },
                { 
                    LinkRelEnumType.Provisionagreement,
                    new LinkRel(LinkRelEnumType.Provisionagreement, "provisionagreement") 
                },
                { 
                    LinkRelEnumType.Related,
                    new LinkRel(LinkRelEnumType.Related, "related") 
                },
                { 
                    LinkRelEnumType.Replies,
                    new LinkRel(LinkRelEnumType.Replies, "replies") 
                },
                { 
                    LinkRelEnumType.Reportingtaxonomy,
                    new LinkRel(LinkRelEnumType.Reportingtaxonomy, "reportingtaxonomy") 
                },
                { 
                    LinkRelEnumType.Section,
                    new LinkRel(LinkRelEnumType.Section, "section") 
                },
                { 
                    LinkRelEnumType.Self,
                    new LinkRel(LinkRelEnumType.Self, "self") 
                },
                { 
                    LinkRelEnumType.Service,
                    new LinkRel(LinkRelEnumType.Service, "service") 
                },
                { 
                    LinkRelEnumType.Start,
                    new LinkRel(LinkRelEnumType.Start, "start") 
                },
                { 
                    LinkRelEnumType.Structure,
                    new LinkRel(LinkRelEnumType.Structure, "structure") 
                },
                { 
                    LinkRelEnumType.Structureset,
                    new LinkRel(LinkRelEnumType.Structureset, "structureset") 
                },
                { 
                    LinkRelEnumType.Stylesheet,
                    new LinkRel(LinkRelEnumType.Stylesheet, "stylesheet") 
                },
                { 
                    LinkRelEnumType.Subsection,
                    new LinkRel(LinkRelEnumType.Subsection, "subsection") 
                },
                { 
                    LinkRelEnumType.SuccessorVersion,
                    new LinkRel(LinkRelEnumType.SuccessorVersion, "successor-version") 
                },
                { 
                    LinkRelEnumType.UP,
                    new LinkRel(LinkRelEnumType.UP, "up") 
                },
                { 
                    LinkRelEnumType.VersionHistory,
                    new LinkRel(LinkRelEnumType.VersionHistory, "version-history") 
                },
                { 
                    LinkRelEnumType.Via,
                    new LinkRel(LinkRelEnumType.Via, "via") },
                { 
                    LinkRelEnumType.WorkingCopy,
                    new LinkRel(LinkRelEnumType.WorkingCopy, "working-copy") 
                },
                { 
                    LinkRelEnumType.WorkingCopyOF,
                    new LinkRel(LinkRelEnumType.WorkingCopyOF, "working-copy-of") 
                }
            };

        private readonly string _stringValue;

        private LinkRel(LinkRelEnumType enumType, string stringValue)
            : base(enumType)
        {
            _stringValue = stringValue;
        }

        public override string ToString()
        {
            return _stringValue;
        }

        public static LinkRel Fromstring(string text)
        {
            foreach (LinkRel b in Values)
            {
                if (b._stringValue.Equals(text, StringComparison.OrdinalIgnoreCase))
                {
                    return b;
                }
            }
            throw new ArgumentException("No constant with text " + text + " found");
        }

        /// <summary>
        ///     Gets the values.
        /// </summary>
        public static IEnumerable<LinkRel> Values
        {
            get
            {
                return _instances.Values;
            }
        }

        /// <summary>
        ///     Gets the instance of <see cref="LinkRel" /> mapped to <paramref name="enumType" />
        /// </summary>
        /// <param name="enumType">
        ///     The <c>enum</c> type
        /// </param>
        /// <returns>
        ///     the instance of <see cref="LinkRel" /> mapped to <paramref name="enumType" />
        /// </returns>
        public static LinkRel GetFromEnum(LinkRelEnumType enumType)
        {
            if (_instances.TryGetValue(enumType, out LinkRel output))
            {
                return output;
            }

            return null;
        }
    }
}