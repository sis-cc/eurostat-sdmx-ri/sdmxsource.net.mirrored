// -----------------------------------------------------------------------
// <copyright file="ISchemaLocationManager.cs" company="EUROSTAT">
//   Date Created : 2023-06-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Api.Sdmx.Manager.Output
{
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    public interface ISchemaLocationManager
    {
        /// <summary> 
        /// Returns the schema location for the given schema version 
        /// </summary>
        /// <param name=" schemaVersion"> 
        /// </param>
        /// <returns>
        /// the schema location 
        /// </returns> 
        string GetSchemaLocation(SdmxSchema schemaVersion);

        /// <summary> 
        /// Returns the structure specific schema location for the given SDMX version 
        /// </summary>
        /// <param name=" cons"> 
        /// </param>
        /// <param name=" schemaVersion"> 
        /// </param>
        /// <returns>
        /// the structure specific schema location 
        /// </returns> 
        string GetStructureSpecificSchemaLocation(IConstrainableObject cons, SdmxSchema schemaVersion);
    }
}