// -----------------------------------------------------------------------
// <copyright file="IMetadataStandard.cs" company="EUROSTAT">
//   Date Created : 2023-06-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Api.Sdmx.Model
{
    /// <summary> 
    /// Represents a metadata standards (e.g SDMX v3.0 or DDI) - used by objects 
    /// to indicate which standards they are compatible with 
    /// 
    /// </summary> 
    public interface IMetadataStandard
    {
        /// <summary> 
        /// </summary>
        /// <returns>
        /// unique Id for the standard, i.e SDMX 
        /// </returns> 
        string Id { get; }
    }
}