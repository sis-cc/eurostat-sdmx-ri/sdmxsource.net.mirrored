// -----------------------------------------------------------------------
// <copyright file="IStaxReader.cs" company="EUROSTAT">
//   DateTime Created : 2023-06-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Api.Xml
{
    using System.Collections.Generic;

    public interface IStaxReader
    {
        void Reset();

        bool MoveNextStartNode();

        string GetElementNamespace();

        void SkipCurrentNode();

        void SkipNode();

        /// <summary> 
        /// Moves the parser position forward to the next instance of the node with the given name 
        /// </summary>
        /// <param name=" parser"> 
        /// @ 
        /// </param> 
        bool SkipToNode(string nodeName);

        bool JumpToNode(string findNodeName, string doNotProcessPastNodeName);

        bool JumpToNode(string findNodeName);

        /// <summary> 
        /// Moves the parser position forward to the next instance of the end node given name 
        /// </summary>
        /// <param name=" parser"> 
        /// @ 
        /// </param> 
        bool SkipToEndNode(string nodeName);

        string GetAttributeValue(string attrId);

        string GetAttributeValue(string ns, string attrId);

        /// <summary> 
        /// Moves to the next start or end node 
        /// </summary>
        /// <returns>
        /// 
        /// </returns> 
        bool MoveNextNode();

        /// <summary> 
        /// Returns the XML path to the current element, for example Root/Element1/ChildElemet 
        /// </summary>
        /// <returns>
        /// 
        /// </returns> 
        string GetXMLPath();

        string ElementName { get; }

        string GetElementText();

        bool IsStartElement { get; }

        bool IsEmptyElement { get; }

        IDictionary<string, string> GetAttributes();

        bool HasDocType { get; }

        int AttributeCount { get; }

        string GetAttributeLocalName(int i);

        string GetAttributeValue(int i);

        string GetAttributePrefix(int i);

        void Close();
    }
}