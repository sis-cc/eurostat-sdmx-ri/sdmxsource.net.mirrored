/*******************************************************************************
 *  This software and the accompanying materials of the FMR tools are licensed under the
 *  Apache Licence, version 2.0 (the “Licence”) which accompanies the distribution and is
 *  available at: http://www.apache.org/licenses/LICENSE-2.0
 *  You may not use this file except in compliance with the Licence.
 *  This file is part of the FMR tools and source code libraries.
 *  The software is distributed on an “AS IS” BASIS, WITHOUT WARRANTIES OR
 *  CONDITIONS OF ANY KIND, either express or implied. See the Apache 2.0 Licence for
 *  more details regarding permissions and limitations under the Licence.
 * Copyright © 2022, Bank for International Settlements
 ******************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Model.Header;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Util;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;
using SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader;
using SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2;
using SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2.Util;

namespace SdmxCore.Io.Sdmx.Parsers
{
    public abstract class AbstractJsonStructureReaderManager : IJsonStructureReaderManager
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(AbstractJsonStructureReaderManager));
        private readonly Dictionary<SdmxStructureEnumType, IJsonMaintainableBeanReaderEngine<IMaintainableObject>> readers = new Dictionary<SdmxStructureEnumType, IJsonMaintainableBeanReaderEngine<IMaintainableObject>>();

        public void RegisterReader(IJsonMaintainableBeanReaderEngine<IMaintainableObject> reader)
        {
            readers.Add(reader.SupportedType, reader);
        }

        public ISdmxObjects ReadJson(IReadableDataLocation rdl, ISdmxObjectRetrievalManager beanRetrievalManager)
        {
            using (JsonReader jReader = new JsonReader(rdl))
            {
                var sdmxBeans = new SdmxObjectsImpl();
                //   jReader.Reset();
                var toProcess = new Dictionary<SdmxStructureEnumType, IJsonMaintainableBeanReaderEngine<IMaintainableObject>>();
                HashSet<SdmxStructureEnumType> processedTypes = new HashSet<SdmxStructureEnumType>();

                var wrappedRetrievalManager = beanRetrievalManager;
                if (!SkipToData(jReader, sdmxBeans))
                {
                    // throw error
                }

                do
                {
                    string fieldName = jReader.GetCurrentFieldName();
                    var reader = GetReader(fieldName);
                    if (reader == null)
                    {
                        _log.Warn("Fusion JSON structure reader, skipping unsupported structure: " + fieldName);
                        jReader.MoveToEndArray();
                        continue;
                    }

                    var reliesOn = reader.GetReliesOn();
                    if (!reliesOn.Any() || reliesOn.All(processedTypes.Contains))
                    {
                        sdmxBeans.AddIdentifiables(reader.BuildMaintainable(jReader, wrappedRetrievalManager));
                        processedTypes.Add(reader.SupportedType);
                    }
                    else
                    {
                        toProcess.Add(reader.SupportedType, reader);
                        jReader.MoveToEndArray();
                    }
                } while (jReader.MoveNextStartArray());

                foreach (var maintainableType in SdmxStructureType.MaintainableStructureTypes)
                {
                    if (!processedTypes.Contains(maintainableType) && !toProcess.ContainsKey(maintainableType))
                    {
                        processedTypes.Add(maintainableType);
                    }
                }

                while (toProcess.Any())
                {
                    jReader.Reset();

                    if (!SkipToData(jReader, sdmxBeans))
                    {
                        // throw error
                    }

                    do
                    {
                        string fieldName = jReader.GetCurrentFieldName();

                        /* TODO SDMXCORE
                        if(this instanceof SdmxJsonStructureReaderManagerV1) {
                            fieldName = StringUtil.chop(fieldName); // SDMX-JSON has 's' at the end of each structure
                        }
                        */
                        SdmxStructureType sdmxStructure = null;
                        try
                        {
                            sdmxStructure = SdmxStructureType.ParseClass(fieldName);
                        }
                        catch (ArgumentException e)
                        {
                            // Unknown class type
                        }

                        if (sdmxStructure != null && toProcess.ContainsKey(sdmxStructure))
                        {
                            IJsonMaintainableBeanReaderEngine<IMaintainableObject> reader = toProcess[sdmxStructure];

                            if (reader.GetReliesOn().All(processedTypes.Contains))
                            {
                                sdmxBeans.AddIdentifiables(reader.BuildMaintainable(jReader, wrappedRetrievalManager));
                                processedTypes.Add(reader.SupportedType);
                                toProcess.Remove(reader.SupportedType);

                                if (!toProcess.Any())
                                {
                                    break;
                                }
                            }
                            else
                            {
                                jReader.MoveToEndArray();
                            }
                        }
                        else
                        {
                            jReader.MoveToEndArray();
                        }

                    } while (jReader.MoveNextStartArray());
                }

                return sdmxBeans;
            }
        }

        protected abstract bool SkipToData(JsonReader jReader, ISdmxObjects sdmxBeans);

        protected virtual IJsonMaintainableBeanReaderEngine<IMaintainableObject> GetReader(string fieldName)
        {
            SdmxStructureType sdmxStructure = null;
            try
            {
                sdmxStructure = SdmxStructureType.ParseClass(fieldName);
            }
            catch (ArgumentException e)
            {
                // Unknown class type
                _log.Warn("Fusion JSON structure reader, unknown structure: " + fieldName);
            }

            return sdmxStructure == null ? null : readers[sdmxStructure];
        }

        protected void ReadMetaInfo(JsonReader jReader, ISdmxObjects sdmxBeans)
        {
            var header = new HeaderImpl("Registry");

            while (jReader.MoveNext())
            {
                if (jReader.IsEndObject())
                {
                    break;
                }

                string key = jReader.GetCurrentFieldName();

                switch (key)
                {
                    case "id":
                        string id = jReader.GetValueAsString();
                        header.Id = id;
                        break;

                    case "test":
                        bool test = jReader.GetValueAsBoolean().Value;
                        header.Test = test;
                        break;

                    case "names":
                        List<ITextTypeWrapperMutableObject> names = SdmxJsonNameableUtil.ReadTextTypeWrappers(jReader);

                        foreach (ITextTypeWrapperMutableObject nameMut in names)
                        {
                            // NOTE SDMXSOURCE_SDMXCORE CHANGE
                            ITextTypeWrapper ttw = new TextTypeWrapperImpl(nameMut, null);
                            header.AddName(ttw);
                        }
                        break;

                    case "sender":
                        IParty sender = ReadParty(jReader);

                        if (sender != null)
                        {
                            header.Sender = sender;
                        }
                        break;

                    case "receivers":
                        while (jReader.MoveNext())
                        {
                            if (jReader.IsEndArray())
                            {
                                break;
                            }

                            IParty pBean = ReadParty(jReader);

                            if (pBean != null)
                            {
                                header.AddReciever(pBean);
                            }
                        }
                        break;
                }
            }
            sdmxBeans.Header = header;
        }

        private List<ITextTypeWrapper> ConvertTTwToImutable(List<ITextTypeWrapperMutableObject> names, string prop)
        {
            List<ITextTypeWrapper> ret = new List<ITextTypeWrapper>();

            foreach (ITextTypeWrapperMutableObject mut in names)
            {
                // NOTE SDMXSOURCE_SDMXCORE CHANGE
                ret.Add(new TextTypeWrapperImpl(mut, null));
            }

            return ret;
        }
        class PartyWrapper
        {
            public List<ITextTypeWrapper> Names { get; set; }
            public string Id { get; set; }
            public IList<IContact> Contacts { get; set; }
        }
        private IParty ReadParty(JsonReader jReader)
        {


            PartyWrapper pWrapper = null;

            while (jReader.MoveNext())
            {
                if (jReader.IsEndObject())
                {
                    break;
                }

                pWrapper = new PartyWrapper();
                string key = jReader.GetCurrentFieldName();

                switch (key)
                {
                    case "id":
                        string id = jReader.GetValueAsString();
                        pWrapper.Id = id;
                        break;

                    case "names":
                        var names = SdmxJsonNameableUtil.ReadTextTypeWrappers(jReader);
                        pWrapper.Names = ConvertTTwToImutable(names, "names");
                        break;

                    case "contacts":
                        var contactImut = SdmxJsonContactUtil.ReadContacts(jReader);
                        var contacts = contactImut.Select(contact => new ContactCore(contact));
                        pWrapper.Contacts = contacts.ToList<IContact>();
                        break;
                }
            }

            if (pWrapper != null)
            {
                return new PartyCore(pWrapper.Names, pWrapper.Id, pWrapper.Contacts, null);
            }

            return null;
        }
    }
}