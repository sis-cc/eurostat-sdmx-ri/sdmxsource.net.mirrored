using System;
using System.Collections.Generic;
using Io.Sdmx.Api.Singleton;
using Io.Sdmx.Format.Json.Engine.Structure.Writer.Sdmx.V2;
using Io.Sdmx.Utils.Core.Application;
using Io.Sdmx.Utils.Core.Object;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader;
using SdmxCore.Io.Sdmx.Format.Json.Engine.Structure.Reader.V2;

namespace SdmxCore.Io.Sdmx.Parsers
{
    public class SdmxJsonStructureReaderManagerV2 : AbstractJsonStructureReaderManager, IFusionSingleton
    {
        private static SdmxJsonStructureReaderManagerV2 _instance;

        public static SdmxJsonStructureReaderManagerV2 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SdmxJsonStructureReaderManagerV2();
                    IFusionObjectStore.RegisterInstance(_instance);
                }
                return _instance;
            }
        }

        public void DestroyInstance()
        {
            _instance = null;
        }

        private SdmxJsonStructureReaderManagerV2()
        {
            // Register your reader engines here
            // RegisterReader(SdmxJsonAgencySchemeReaderEngineV2.GetInstance());
            RegisterReader(SdmxJsonCategorisationReaderEngineV2.Instance);
            RegisterReader(SdmxJsonCategorySchemeReaderEngineV2.Instance);
            RegisterReader(SdmxJsonCodelistReaderEngineV2.Instance);
            RegisterReader(SdmxJsonConceptSchemeReaderEngineV2.Instance);
            // RegisterReader(SdmxJsonContentConstraintReaderEngineV2.GetInstance());
            // RegisterReader(SdmxJsonDataConsumerSchemeReaderEngineV2.GetInstance());
            // RegisterReader(SdmxJsonDataProviderSchemeReaderEngineV2.GetInstance());
            RegisterReader(SdmxJsonDataStructureReaderEngineV2.Instance);
            RegisterReader(SdmxJsonDataflowReaderEngineV2.Instance);
            // RegisterReader(SdmxJsonHierarchicalCodelistReaderEngineV2.GetInstance());
            // RegisterReader(SdmxJsonMetadataflowReaderEngineV2.GetInstance());
            // RegisterReader(SdmxJsonMetadataStructureReaderEngineV2.GetInstance());
            // RegisterReader(SdmxJsonOrganisationUnitSchemeReaderEngineV2.GetInstance());
            // RegisterReader(SdmxJsonProvisionAgreementReaderEngineV2.GetInstance());
            // RegisterReader(SdmxJsonPDQReaderEngineV2.GetInstance());
            // RegisterReader(SdmxJsonStructureMapReaderEngineV2.GetInstance());
            // RegisterReader(SdmxJsonRepresentationMapReaderEngineV2.GetInstance());
        }

        //private void RegisterReader(IJsonMaintainableBeanReaderEngine<IDataStructureObject> instance)
        //{
        //    throw new NotImplementedException();
        //}

        protected override IJsonMaintainableBeanReaderEngine<IMaintainableObject> GetReader(string fieldName)
        {
            /* TODO: Handle specific cases for different readers, e.g., geoGridCodelists, geographicCodelists, representationMaps, structureMaps, etc. */
            return base.GetReader(StringUtil.Chop(fieldName));
        }

        protected override bool SkipToData(JsonReader jReader, ISdmxObjects sdmxBeans)
        {
            while (jReader.MoveNextStartObject())
            {
                while (jReader.MoveNext())
                {
                    string currentFieldName = jReader.GetCurrentFieldName();
                    switch (currentFieldName)
                    {
                        case "meta":
                            ReadMetaInfo(jReader, sdmxBeans);
                            break;
                        case "errors":
                            // Process error info
                            continue;
                        case "data":
                            jReader.MoveNextStartArray();
                            return true;
                    }
                }
            }
            return false;
        }
    }
}
