/*******************************************************************************
 *  This software and the accompanying materials of the FMR tools are licensed under the
 *  Apache Licence, version 2.0 (the “Licence”) which accompanies the distribution and is
 *  available at: http://www.apache.org/licenses/LICENSE-2.0
 *  You may not use this file except in compliance with the Licence.
 *  This file is part of the FMR tools and source code libraries.
 *  The software is distributed on an “AS IS” BASIS, WITHOUT WARRANTIES OR
 *  CONDITIONS OF ANY KIND, either express or implied. See the Apache 2.0 Licence for
 *  more details regarding permissions and limitations under the Licence.
 * Copyright © 2022, Bank for International Settlements
 ******************************************************************************/
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Util;

namespace SdmxCore.Io.Sdmx.Parsers
{

    /**
     * Responsible for reading Structures in JSON format
     */
    public interface IJsonStructureReaderManager
    {

        /**
         * Reads JSON to return SDMX Beans
         * @param rdl
         * @return
         */
        ISdmxObjects ReadJson(IReadableDataLocation rdl, ISdmxObjectRetrievalManager beanRetrievalManager);
    }
}