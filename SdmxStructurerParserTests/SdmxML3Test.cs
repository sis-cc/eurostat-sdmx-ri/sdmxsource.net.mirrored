// -----------------------------------------------------------------------
// <copyright file="SdmxML3Test.cs" company="EUROSTAT">
//   Date Created : 2022-01-11
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxStructureParserTests
{
    using System.IO;
    using System.Linq;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Sdmx.SdmxObjects.TestUtils;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Util.Io;
    using Org.Sdmxsource.XmlHelper;

    [TestFixture]
    public class SdmxML3Test
    {
        readonly string TEST_OUTPUT_FOLDER = "target/tests-output/3/xml";

        private readonly IStructureParsingManager _structureParsingManager = new StructureParsingManager();
        private readonly IReadableDataLocationFactory _readableDataLocationFactory = new ReadableDataLocationFactory();
        private readonly IStructureWriterManager _structureWriterManager = new StructureWriterManager();

        [Test]
        public void Read21andwrite300()
        {
            ISdmxObjects readBeans = ReadStructures("tests/v21/Structure/registry.sdmx.org.xml", SdmxSchemaEnumType.VersionTwoPointOne);
            WriteStructures(readBeans, "registry.sdmx.org");
        }

        [Test]
        public void Write300VersionNotfinal()
        {
            // Create a datastructure sample with version 2.4.0 and final flag = false.
            // In 3.0 schema this should be final version, but since the final flag is false and the version is valid for 2.x schemas,
            // the application must treat is as 2.x.
            IDataStructureMutableObject mutable = SampleObjectsBuilder.BuildDataStructureMutableObject();
            mutable.Version = "2.4.0";
            mutable.FinalStructure = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.False);
            IDataStructureObject immutable = mutable.ImmutableInstance;
            Assert.AreEqual(mutable.FinalStructure, immutable.IsFinal);

            // write the object as 3.0 xml
            ISdmxObjects writeObjects = new SdmxObjectsImpl(immutable);
            string fileName = "TestwithVersionMajorMinorPatchNotFinal";
            WriteStructures(writeObjects, fileName);
            // read it back and check that version was written with the semantic addition "-draft"
            ISdmxObjects readObjects = ReadStructures(Path.Combine(TEST_OUTPUT_FOLDER, $"{fileName}.xml"), SdmxSchemaEnumType.VersionThree);
            Assert.That(readObjects.DataStructures, Has.Count.EqualTo(1));
            var dataStructure = readObjects.DataStructures.Single();
            Assert.AreEqual($"{mutable.Version}-draft", dataStructure.Version);
        }

        [Test]
        public void Read300samplefromgithub()
        {
            ISdmxObjects sdmxObjects = ReadStructures("tests/v30/ECB_EXR.xml", SdmxSchemaEnumType.VersionThree);
            Assert.That(sdmxObjects.DataStructures, Has.Count.EqualTo(1));
        }

        [Test]
        public void Read300samplecodelistUnion()
        {
            ISdmxObjects sdmxObjects = ReadStructures("tests/v30/samples/Codelist/codelist - discriminated union.xml", SdmxSchemaEnumType.VersionThree);
            Assert.That(sdmxObjects.Codelists, Has.Count.EqualTo(1));
            Assert.That(sdmxObjects.DataStructures, Has.Count.EqualTo(1));
            Assert.That(sdmxObjects.Dataflows, Has.Count.EqualTo(2));
            Assert.That(sdmxObjects.ConceptSchemes, Has.Count.EqualTo(1));
        }

        [Test]
        public void Read300samplecodelistExtended()
        {
            ISdmxObjects sdmxObjects = ReadStructures("tests/v30/samples/Codelist/codelist - extended.xml", SdmxSchemaEnumType.VersionThree);
            Assert.That(sdmxObjects.Codelists, Has.Count.EqualTo(2));
        }

        [Test]
        public void Read300sampleConcept()
        {
            ISdmxObjects sdmxObjects = ReadStructures("tests/v30/samples/Concept Scheme/conceptscheme.xml", SdmxSchemaEnumType.VersionThree);
            Assert.That(sdmxObjects.ConceptSchemes, Has.Count.EqualTo(1));
            WriteStructures(sdmxObjects, "concept-write");
        }

        [Test]
        public void ReadWrite300samplecodelistUnion()
        {
            ISdmxObjects sdmxObjects = ReadStructures("tests/v30/samples/Codelist/codelist - discriminated union.xml", SdmxSchemaEnumType.VersionThree);
            Assert.That(sdmxObjects.Codelists, Has.Count.EqualTo(1));
            Assert.That(sdmxObjects.DataStructures, Has.Count.EqualTo(1));
            Assert.That(sdmxObjects.Dataflows, Has.Count.EqualTo(2));
            Assert.That(sdmxObjects.ConceptSchemes, Has.Count.EqualTo(1));
            WriteStructures(sdmxObjects, "codelist-disc");
        }

        [Test]
        public void ReadWrite300samplecodelistExtended()
        {
            ISdmxObjects sdmxObjects = ReadStructures("tests/v30/samples/Codelist/codelist - extended.xml", SdmxSchemaEnumType.VersionThree);
            Assert.That(sdmxObjects.Codelists, Has.Count.EqualTo(2));
            WriteStructures(sdmxObjects, "codelist-extended");
        }

        [Test]
        public void ReadWrite300sampleConcept()
        {
            ISdmxObjects sdmxObjects = ReadStructures("tests/v30/samples/Concept Scheme/conceptscheme.xml", SdmxSchemaEnumType.VersionThree);
            Assert.That(sdmxObjects.ConceptSchemes, Has.Count.EqualTo(1));
            WriteStructures(sdmxObjects, "concept-scheme");
        }

        [Test]
        public void Read300samplewithmultiplemeasures()
        {
            ISdmxObjects sdmxObjects = ReadStructures("tests/v30/datastructure-3.0.xml", SdmxSchemaEnumType.VersionThree);
            Assert.That(sdmxObjects.DataStructures, Has.Count.EqualTo(1));
            IDataStructureObject dsd = sdmxObjects.DataStructures.FirstOrDefault();
            Assert.That(dsd, Is.Not.Null);
            Assert.That(dsd.Measures, Has.Count.EqualTo(3));
        }

        /// <summary>
        /// Tests that for sdmx 2.1 dsd, the primary measure will be put in the measures list
        /// </summary>
        [Test]
        public void Read21samplewithPrimaryMeasure()
        {
            ISdmxObjects sdmxObjects = ReadStructures("tests/v21/DataStructure-IMF.BOP(1.4).xml", SdmxSchemaEnumType.VersionTwoPointOne);
            Assert.That(sdmxObjects.DataStructures, Has.Count.EqualTo(1));
            IDataStructureObject dsd = sdmxObjects.DataStructures.FirstOrDefault();
            Assert.That(dsd, Is.Not.Null);
            Assert.That(dsd.PrimaryMeasure.ConceptRef.IdentifiableIds[0], Is.EqualTo("OBS_VALUE"));
            Assert.That(dsd.Measures, Has.Count.EqualTo(1));
            Assert.That(dsd.Measures.First().Id, Is.EqualTo("OBS_VALUE"));
        }

        [Test]
        public void Readwrite300samplefromgithub()
        {
            ISdmxObjects sdmxObjects = ReadStructures("tests/v30/ECB_EXR.xml", SdmxSchemaEnumType.VersionThree);
            Assert.That(sdmxObjects.DataStructures, Has.Count.EqualTo(1));
            WriteStructures(sdmxObjects, "ECB_EXR-write");
        }

        [Test]
        public void Readwrite300samplewithmultiplemeasures()
        {
            ISdmxObjects sdmxObjects = ReadStructures("tests/v30/datastructure-3.0.xml", SdmxSchemaEnumType.VersionThree);
            Assert.That(sdmxObjects.DataStructures, Has.Count.EqualTo(1));
            IDataStructureObject dsd = sdmxObjects.DataStructures.FirstOrDefault();
            Assert.That(dsd, Is.Not.Null);
            Assert.That(dsd.Measures, Has.Count.EqualTo(3));
            WriteStructures(sdmxObjects, "datastructure-write");
        }

        [Test]
        public void TestXmlWithMultilangual()
        {
            ISdmxObjects sdmxObjects = ReadStructures("tests/v30/ECB_EXR.xml", SdmxSchemaEnumType.VersionThree);
            Assert.True(sdmxObjects.DataStructures.First().GetAttribute("TITLE").Representation.TextFormat.Multilingual.IsTrue);
        }

        [Test]
        public void Read300samplewithSentinelValues()
        {
            ISdmxObjects sdmxObjects = ReadStructures("tests/v30/datastructure-3.0.xml", SdmxSchemaEnumType.VersionThree);
            IDataStructureObject dsd = sdmxObjects.DataStructures.First();
            IAttributeObject attribute = dsd.GetAttribute("OBS_PRE_BREAK");
            Assert.NotNull(attribute.Representation);
            Assert.NotNull(attribute.Representation.TextFormat);
            Assert.AreEqual("-1", attribute.Representation.TextFormat.SentinelValues[0].Value);
            Assert.AreEqual("en", attribute.Representation.TextFormat.SentinelValues[0].Names[0].Locale);
            Assert.AreEqual("Non applicable value", attribute.Representation.TextFormat.SentinelValues[0].Names[0].Value);
        }

        private void WriteStructures(ISdmxObjects readObjects, string outputName)
        {
            if (!Directory.Exists(TEST_OUTPUT_FOLDER))
            {
                Directory.CreateDirectory(TEST_OUTPUT_FOLDER);
            }
            string outputFile = Path.Combine(TEST_OUTPUT_FOLDER, $"{outputName}.xml");
            if (File.Exists(outputFile))
            {
                File.Delete(outputFile);
            }

            using (FileStream outputStream = File.Create(outputFile))
            {
                IStructureFormat outputFormat = new SdmxStructureWithExtraInfoFormat(
                    StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV3StructureDocument), true, "junit", "5.0.0");
                WriteStructureToFile(readObjects, outputFormat, outputStream);
            }

            ValidateStructures(outputFile, SdmxSchemaEnumType.VersionThree);
        }

        private ISdmxObjects ReadStructures(string filepath, SdmxSchemaEnumType sdmxSchema)
        {
            FileInfo fileInfo = new FileInfo(filepath);
            using (IReadableDataLocation rdl = _readableDataLocationFactory.GetReadableDataLocation(fileInfo))
            {
                IStructureWorkspace workspace = _structureParsingManager.ParseStructures(rdl);

                ISdmxObjects structureBeans = workspace.GetStructureObjects(false);

                XMLParser.ValidateXml(rdl, sdmxSchema);

                return structureBeans;
            }
        }

        private void ValidateStructures(string filepath, SdmxSchemaEnumType sdmxSchema)
        {
            FileInfo fileInfo = new FileInfo(filepath);
            using (IReadableDataLocation rdl = _readableDataLocationFactory.GetReadableDataLocation(fileInfo))
            {
                Assert.DoesNotThrow(() => XMLParser.ValidateXml(rdl, sdmxSchema), filepath);
            }
        }

        private void WriteStructureToFile(ISdmxObjects objects, IStructureFormat outputFormat, FileStream outStream)
        {
            _structureWriterManager.WriteStructures(objects, outputFormat, outStream);
        }
    }
}
