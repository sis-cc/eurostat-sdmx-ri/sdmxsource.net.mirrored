// -----------------------------------------------------------------------
// <copyright file="DataStructureSuperObjectBuilderTests.cs" company="EUROSTAT">
//   Date Created : 2016-02-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParserTests.
//     SdmxStructureParserTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParserTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParserTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Globalization;
using System.Linq;
using FluentAssertions;

using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects;
using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.TestUtils;
using Org.Sdmxsource.Sdmx.Structureparser.Builder.SuperObjects;
using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

namespace SdmxStructureParserTests
{
    [TestFixture]
    //@ExtendWith(SpringExtension.class)
    //@ContextConfiguration(locations={ "file:src/test/java/spring/structureParserWriter-beans.xml"})
    class DataStructureSuperObjectBuilderTests
    {
        public DataStructureSuperObjectBuilder CreateSUT()
        {
            return new DataStructureSuperObjectBuilder();
        }


        public class When_building_a_DataStructureSuperObject : DataStructureSuperObjectBuilderTests
        {
            private ISdmxObjectRetrievalManager retrievalManager;

            [SetUp]
            public void SetUp()
            {
                retrievalManager = new InMemoryRetrievalManager(Utils.Structures);
            }

            [Test]
            public void Should_create_a_instance_of_DataStructureSuperObject()
            {
                var sut = CreateSUT();
                var dataflowSuperObject = sut.Build(Utils.DataStructure, retrievalManager, null);
                dataflowSuperObject.Should().BeOfType<DataStructureSuperObject>();
            }
        }

        private readonly SuperObjectsBuilder _superObjectsBuilder = new SuperObjectsBuilder();

        [Test]
        public void DsdWithPrimaryMeasure()
        {
            ISdmxObjects testISdmxObjects = BuildDataStructureMutableBeanWithPrimaryMeasure();
            ISuperObjects superBeans = _superObjectsBuilder.Build(testISdmxObjects);

            Assert.AreEqual(1, superBeans.DataStructures.Count);

            IDataStructureSuperObject dataStructureSuperBean = superBeans.DataStructures.First();

            //Assertions for primary measure
            Assert.AreEqual(2, dataStructureSuperBean.ReferencedConcepts.Count);
            Assert.True(dataStructureSuperBean.ReferencedConcepts.Any(c => c.Id.Equals("OBS_VALUE")));

            //Assertions for dimension
            Assert.True(dataStructureSuperBean.ReferencedConcepts.Any(c => c.Id.Equals(SampleObjectsBuilder.TEST_CONCEPT_ID)));
            Assert.AreEqual(1, dataStructureSuperBean.ReferencedCodelists.Count);
            Assert.True(dataStructureSuperBean.ReferencedCodelists.Any(c => c.Id.Equals(SampleObjectsBuilder.TEST_CODELIST_ID)));
        }

        [Test]
        public void DsdWithMultipleMeasures()
        {
            ISdmxObjects testISdmxObjects = BuildDataStructureMutableBeanWithMultipleMeasures();
            ISuperObjects superBeans = _superObjectsBuilder.Build(testISdmxObjects);

            Assert.AreEqual(1, superBeans.DataStructures.Count);

            IDataStructureSuperObject dataStructureSuperBean = superBeans.DataStructures.First();

            //Assertions for measures
            Assert.AreEqual(2, dataStructureSuperBean.Measures.Count);
            Assert.True(dataStructureSuperBean.Measures.Any(p => p.Id.Equals("MEASURE") && p.Concept.Id.Equals("M")));
            Assert.True(dataStructureSuperBean.Measures.Any(p => p.Id.Equals("MEASURE1") && p.Concept.Id.Equals("M1") && 
                p.GetCodelist(true).Id.Equals(SampleObjectsBuilder.TEST_CODELIST_ID)));

            //Assertions for dimension
            Assert.True(dataStructureSuperBean.ReferencedConcepts.Any(c => c.Id.Equals(SampleObjectsBuilder.TEST_CONCEPT_ID)));
            Assert.AreEqual(1, dataStructureSuperBean.ReferencedCodelists.Count);
            Assert.True(dataStructureSuperBean.ReferencedCodelists.Any(c => c.Id.Equals(SampleObjectsBuilder.TEST_CODELIST_ID)));
        }

        [Test]
        public void DsdWithAnnotationsURLs()
        {
            ISdmxObjects testISdmxObjects = BuildDataStructureWithAnnotationURLs();
            ISuperObjects superBeans = _superObjectsBuilder.Build(testISdmxObjects);

            Assert.AreEqual(1, superBeans.DataStructures.Count);

            IDataStructureSuperObject dataStructureSuperBean = superBeans.DataStructures.First();
            var annotation = dataStructureSuperBean.Annotations.First();
            var url = annotation.Urls.First();
            var enKey = "en";
            var enUri = new Uri("https://some.example.ie/en");

            //Assertions for annotations
            Assert.That(annotation.Urls, Has.Count.EqualTo(1));
            Assert.AreEqual(annotation.Value, "annotationValue");
            Assert.AreEqual(enKey, url.Locale);
            Assert.AreEqual(enUri, url.Uri);
            Assert.AreEqual(annotation.Url, enUri);
        }

        [Test]
        public void DsdWithAnnotationsUri()
        {
            ISdmxObjects testISdmxObjects = BuildDataStructureWithAnnotationUri();
            ISuperObjects superBeans = _superObjectsBuilder.Build(testISdmxObjects);

            Assert.AreEqual(1, superBeans.DataStructures.Count);

            IDataStructureSuperObject dataStructureSuperBean = superBeans.DataStructures.First();
            var annotation = dataStructureSuperBean.Annotations.First();
            var enUri = new Uri("https://some.example.ie/en");

            //Assertions for annotations
            Assert.AreEqual(annotation.Url, enUri);
            Assert.AreEqual(annotation.Value, "annotationValue");
            Assert.That(annotation.Urls, Has.Count.EqualTo(1));
            Assert.AreEqual(null, annotation.Urls.First().Locale);
            Assert.AreEqual(enUri, annotation.Urls.First().Uri);
        }

        private static ISdmxObjects BuildDataStructureMutableBeanWithPrimaryMeasure()
        {
            IMutableObjects mutableBeans = new MutableObjectsImpl();

            IDataStructureMutableObject dsd = SampleObjectsBuilder.BuildDataStructureMutableObject();
            IStructureReference primaryConceptRef = new StructureReferenceImpl(SampleObjectsBuilder.TEST_AGENCY_ID, 
                SampleObjectsBuilder.TEST_CONCEPT_SCHEME_ID, SampleObjectsBuilder.TEST_VERSION, SdmxStructureEnumType.Concept, "OBS_VALUE");
            IPrimaryMeasureMutableObject primaryMeasureMutableBean = new PrimaryMeasureMutableCore();
            primaryMeasureMutableBean.ConceptRef = primaryConceptRef;
            dsd.PrimaryMeasure = primaryMeasureMutableBean;
            mutableBeans.AddDataStructure(dsd);

            IConceptSchemeMutableObject conceptSchemeMutableBean = SampleObjectsBuilder.BuildConceptSchemeMutableObject();
            conceptSchemeMutableBean.CreateItem("OBS_VALUE", "OBS_VALUE concept");
            mutableBeans.AddConceptScheme(conceptSchemeMutableBean);

            ICodelistMutableObject codelist = SampleObjectsBuilder.BuildCodelistMutableObject();
            mutableBeans.AddCodelist(codelist);

            return mutableBeans.ImmutableObjects;
        }

        private static ISdmxObjects BuildDataStructureMutableBeanWithMultipleMeasures()
        {
            IDataStructureMutableObject dsd = SampleObjectsBuilder.BuildDataStructureMutableObject();

            IStructureReference conceptRef = new StructureReferenceImpl(SampleObjectsBuilder.TEST_AGENCY_ID, 
                SampleObjectsBuilder.TEST_CONCEPT_SCHEME_ID, SampleObjectsBuilder.TEST_VERSION, SdmxStructureEnumType.Concept, "M");
            IMeasureMutableObject measureMutableBean = new MeasureMutableCore();
            measureMutableBean.Id = "MEASURE";
            measureMutableBean.ConceptRef = conceptRef;
            dsd.AddMeasure(measureMutableBean);

            IStructureReference conceptRef1 = new StructureReferenceImpl(SampleObjectsBuilder.TEST_AGENCY_ID, 
                SampleObjectsBuilder.TEST_CONCEPT_SCHEME_ID, SampleObjectsBuilder.TEST_VERSION, SdmxStructureEnumType.Concept, "M1");
            IMeasureMutableObject measureMutableBean1 = new MeasureMutableCore();
            measureMutableBean1.Id = "MEASURE1";
            measureMutableBean1.ConceptRef = conceptRef1;

            IRepresentationMutableObject representationMutableBean = SampleObjectsBuilder.BuildMutableRepresentationObjectWithCodelist();
            measureMutableBean1.Representation = representationMutableBean;
            dsd.AddMeasure(measureMutableBean1);

            IMutableObjects mutableBeans = new MutableObjectsImpl();
            mutableBeans.AddDataStructure(dsd);

            IConceptSchemeMutableObject conceptSchemeMutableBean = SampleObjectsBuilder.BuildConceptSchemeMutableObject();

            conceptSchemeMutableBean.CreateItem("M", "sample concept for measure");
            conceptSchemeMutableBean.CreateItem("M1", "sample concept for measure1");

            mutableBeans.AddConceptScheme(conceptSchemeMutableBean);

            ICodelistMutableObject codelist = SampleObjectsBuilder.BuildCodelistMutableObject();
            mutableBeans.AddCodelist(codelist);

            return mutableBeans.ImmutableObjects;
        }

        private static ISdmxObjects BuildDataStructureWithAnnotationURLs()
        {
            IMutableObjects mutableBeans = new MutableObjectsImpl();

            IDataStructureMutableObject dsd = SampleObjectsBuilder.BuildDataStructureMutableObject();
            IAnnotationMutableObject annotation = new AnnotationMutableCore()
            {
                Id = "annotationId",
                Title = "annotationTitle",
                Type = "annotationType",
                Value = "annotationValue"
            };
            annotation.AddUrl("en", new Uri("https://some.example.ie/en"));
            dsd.AddAnnotation(annotation);
            mutableBeans.AddDataStructure(dsd);

            IConceptSchemeMutableObject conceptSchemeMutableBean = SampleObjectsBuilder.BuildConceptSchemeMutableObject();
            conceptSchemeMutableBean.CreateItem("OBS_VALUE", "OBS_VALUE concept");
            mutableBeans.AddConceptScheme(conceptSchemeMutableBean);

            ICodelistMutableObject codelist = SampleObjectsBuilder.BuildCodelistMutableObject();
            mutableBeans.AddCodelist(codelist);

            return mutableBeans.ImmutableObjects;
        }

        private static ISdmxObjects BuildDataStructureWithAnnotationUri()
        {
            IMutableObjects mutableBeans = new MutableObjectsImpl();

            IDataStructureMutableObject dsd = SampleObjectsBuilder.BuildDataStructureMutableObject();
            IAnnotationMutableObject annotation = new AnnotationMutableCore()
            {
                Id = "annotationId",
                Title = "annotationTitle",
                Type = "annotationType",
                Value = "annotationValue",
                Uri = new Uri("https://some.example.ie/en")
            };
            dsd.AddAnnotation(annotation);
            mutableBeans.AddDataStructure(dsd);

            IConceptSchemeMutableObject conceptSchemeMutableBean = SampleObjectsBuilder.BuildConceptSchemeMutableObject();
            conceptSchemeMutableBean.CreateItem("OBS_VALUE", "OBS_VALUE concept");
            mutableBeans.AddConceptScheme(conceptSchemeMutableBean);

            ICodelistMutableObject codelist = SampleObjectsBuilder.BuildCodelistMutableObject();
            mutableBeans.AddCodelist(codelist);

            return mutableBeans.ImmutableObjects;
        }
    }
}