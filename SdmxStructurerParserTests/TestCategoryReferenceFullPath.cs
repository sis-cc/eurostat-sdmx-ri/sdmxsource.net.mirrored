﻿// -----------------------------------------------------------------------
// <copyright file="TestCategoryReferenceFullPath.cs" company="EUROSTAT">
//   Date Created : 2016-09-23
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParserTests.
// 
//     SdmxStructureParserTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParserTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParserTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxStructureParserTests
{
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// Test related to category reference including full path
    /// </summary>
    [TestFixture]
    public class TestCategoryReferenceFullPath
    {
        /// <summary>
        /// The _writer manager
        /// </summary>
        private readonly IStructureWriterManager _writerManager = new StructureWriterManager();

        /// <summary>
        /// The _parsing manager
        /// </summary>
        private readonly IStructureParsingManager _parsingManager = new StructureParsingManager();
        
        /// <summary>
        /// Tests the writing a category reference with full path.
        /// </summary>
        [Test]
        public void TestWritingACategoryReferenceWithFullPath()
        {
            ICategorisationMutableObject categorisation = new CategorisationMutableCore();
            categorisation.Id = "TEST_CATEGORISATION";
            categorisation.AgencyId = "TEST";
            categorisation.Version = "1.0";
            categorisation.AddName("en", "Test Categorisation");
            categorisation.CategoryReference = new StructureReferenceImpl("TEST", "TEST_CATSCH", "1.0", SdmxStructureEnumType.Category, "FULL", "PATH", "TO", "ID");
            categorisation.StructureReference = new StructureReferenceImpl("TEST", "TEST_DF", "1.0", SdmxStructureEnumType.Dataflow);

            FileInfo file = new FileInfo("TestWritingACategoryReferenceWithFullPath.xml");

            using (var stream = file.Create())
            {
                this._writerManager.WriteStructure(categorisation.ImmutableInstance, new HeaderImpl("TEST_ID", "ZZ9"), new SdmxStructureFormat(StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument)), stream);
                stream.Flush();
            }

            using (var stream = file.OpenRead())
            {
                var xDocument = XDocument.Load(stream);

                var refElement = xDocument.Descendants().Where(element => element.Name.LocalName.Equals("Target")).Elements().First(element => element.Name.LocalName.Equals("Ref"));

                var xAttribute = refElement.Attribute(XName.Get("id"));
                Assert.That(xAttribute, Has.Property("Value").EqualTo("FULL.PATH.TO.ID"));
            }
        }

        /// <summary>
        /// Tests the parsing XML with category full path.
        /// </summary>
        /// <param name="file">The file. Must have categorisations where the category reference id has full path.</param>
        [TestCase("tests/v21/Structure/esms.xml")]
        public void TestParsingXmlWithCategoryFullPath(string file)
        {
            var sdmxObjects = new FileInfo(file).GetSdmxObjects(this._parsingManager);
            Assert.That(sdmxObjects.Categorisations, Is.Not.Empty);
            foreach (var categorisationObject in sdmxObjects.Categorisations)
            {
                Assert.That(categorisationObject.CategoryReference.ChildReference.ChildReference, Is.Not.Null);
            }
        }
    }
}