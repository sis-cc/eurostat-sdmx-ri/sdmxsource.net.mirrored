// -----------------------------------------------------------------------
// <copyright file="XmlWriterParserRoundtripTests.cs" company="EUROSTAT">
//   Date Created : 2022-03-01
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxStructureParserTests.XmlRoundtripTests
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Util.Io;
    using Org.Sdmxsource.XmlHelper;
    using Sdmx.TestUtils.XmlRoundtripTests;

    [TestFixture]
    public class XmlWriterParserRoundtripTests
    {
        readonly string TEST_OUTPUT_FOLDER = "target/tests-output/{0}/xml";

        private readonly IStructureParsingManager _structureParsingManager = new StructureParsingManager();
        private readonly IReadableDataLocationFactory _readableDataLocationFactory = new ReadableDataLocationFactory();
        private readonly IStructureWriterManager _structureWriterManager = new StructureWriterManager();

        private readonly SdmxXmlCompareFactory _xmlCompareFactory = new SdmxXmlCompareFactory();

        [Test]
        [TestCaseSource(typeof(StructureXmlSamples), nameof(StructureXmlSamples.GetV30samples), 
            new object[] { new string[] { "valuelist.xml", "codelist - discriminated union.xml", "XHTML.xml" } })]
        public void WriterParserRoundtripTest(string filepath, SdmxSchemaEnumType sdmxSchemaVersion)
        {
            ISdmxObjects sdmxObjects = ReadStructures(filepath, sdmxSchemaVersion);
            string filename = Path.GetFileNameWithoutExtension(filepath);
            string targetFilepath = WriteStructures(sdmxObjects, filename, sdmxSchemaVersion);

            var testEngine = _xmlCompareFactory.GetEngine(sdmxSchemaVersion);
            RoundtripComparisonResult result = testEngine.AreEqual(filepath, targetFilepath);
            Assert.True(result.IsEqual, result.GetResultString());
            Assert.True(testEngine.IsValid(filepath));
        }

        private string WriteStructures(ISdmxObjects readObjects, string outputName, SdmxSchemaEnumType sdmxSchemaVersion)
        {
            string targetPath = string.Format(TEST_OUTPUT_FOLDER, sdmxSchemaVersion);
            if (!Directory.Exists(targetPath))
            {
                Directory.CreateDirectory(targetPath);
            }
            string outputFile = Path.Combine(targetPath, $"{outputName}.xml");
            if (File.Exists(outputFile))
            {
                File.Delete(outputFile);
            }

            StructureOutputFormatEnumType structureOutputFormat;
            switch (sdmxSchemaVersion)
            {
                case SdmxSchemaEnumType.VersionThree:
                    structureOutputFormat = StructureOutputFormatEnumType.SdmxV3StructureDocument;
                    break;
                case SdmxSchemaEnumType.VersionTwoPointOne:
                    structureOutputFormat = StructureOutputFormatEnumType.SdmxV21StructureDocument;
                    break;
                case SdmxSchemaEnumType.VersionTwo:
                    structureOutputFormat = StructureOutputFormatEnumType.SdmxV2StructureDocument;
                    break;
                case SdmxSchemaEnumType.VersionOne:
                    structureOutputFormat = StructureOutputFormatEnumType.SdmxV1StructureDocument;
                    break;
                default:
                    throw new ArgumentException($"{sdmxSchemaVersion} is not a valid value for schema version.");
            }

            using (FileStream outputStream = File.Create(outputFile))
            {
                IStructureFormat outputFormat = new SdmxStructureWithExtraInfoFormat(
                    StructureOutputFormat.GetFromEnum(structureOutputFormat), true, "junit", "5.0.0");
                WriteStructureToFile(readObjects, outputFormat, outputStream);
            }

            ValidateStructures(outputFile, sdmxSchemaVersion);

            return outputFile;
        }

        private ISdmxObjects ReadStructures(string filepath, SdmxSchemaEnumType sdmxSchema)
        {
            FileInfo fileInfo = new FileInfo(filepath);
            using (IReadableDataLocation rdl = _readableDataLocationFactory.GetReadableDataLocation(fileInfo))
            {
                IStructureWorkspace workspace = _structureParsingManager.ParseStructures(rdl);

                ISdmxObjects structureBeans = workspace.GetStructureObjects(false);

                XMLParser.ValidateXml(rdl, sdmxSchema);

                return structureBeans;
            }
        }

        private void ValidateStructures(string filepath, SdmxSchemaEnumType sdmxSchema)
        {
            FileInfo fileInfo = new FileInfo(filepath);
            using (IReadableDataLocation rdl = _readableDataLocationFactory.GetReadableDataLocation(fileInfo))
            {
                Assert.DoesNotThrow(() => XMLParser.ValidateXml(rdl, sdmxSchema), filepath);
            }
        }

        private void WriteStructureToFile(ISdmxObjects objects, IStructureFormat outputFormat, FileStream outStream)
        {
            _structureWriterManager.WriteStructures(objects, outputFormat, outStream);
        }
    }
}
