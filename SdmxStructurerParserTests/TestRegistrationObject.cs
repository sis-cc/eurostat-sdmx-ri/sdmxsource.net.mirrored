// -----------------------------------------------------------------------
// <copyright file="TestRegistrationObject.cs" company="EUROSTAT">
//   Date Created : 2018-7-24
//   Copyright (c) 2018 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParserTests.
//     SdmxStructureParserTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParserTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParserTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxStructureParserTests
{
    using System.IO;
    using System.Linq;

    using NUnit.Framework;

    using Org.Sdmxsource.Util.Io;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization;

    [TestFixture]
    public class TestRegistrationObject
    {
        /// <summary>
        /// The submit registration response builder
        /// </summary>
        private readonly SubmitRegistrationResponseBuilder _submitRegistrationResponseBuilder = new SubmitRegistrationResponseBuilder();


        [TestCase("tests/v21/submit_registration_response.xml", "ded510863efdb24")]
        public void ShouldMaintainIdFromResponse(string response, string expectedId)
        {
            var file = new FileInfo(response);
            using (var dataLocation = new FileReadableDataLocation(file))
            {
                var responses = this._submitRegistrationResponseBuilder.Build(dataLocation);
                var ri = responses.First();
                Assert.That(ri.Registration.Id, Is.EqualTo(expectedId));

            }
        }
    }
}