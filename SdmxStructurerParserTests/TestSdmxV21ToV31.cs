// -----------------------------------------------------------------------
// <copyright file="TestSdmxV20ToV21.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParserTests.
//     SdmxStructureParserTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParserTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParserTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxStructureParserTests
{
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Util.Io;
    using Org.Sdmxsource.XmlHelper;

    /// <summary>
    ///     Test unit for <see cref="StructureWriterManager" />
    /// </summary>
    [TestFixture]
    public class TestSdmxV21ToV31
    {
        #region Public Methods and Operators

        /// <summary>
        /// Test unit for <see cref="StructureWriterManager.WriteStructures"/>
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        [TestCase("tests/v21/Structure/ESTAT+STS+2.0-DF-DSD-full.xml")]
        public void TestWriteStructuresTo31(string file)
        {
            var structureReader = new StructureParsingManager();
            var fileInfo = new FileInfo(file);
            IStructureWorkspace structureWorkspace;
            using (var readable = new FileReadableDataLocation(fileInfo))
            {
                structureWorkspace = structureReader.ParseStructures(readable);
            }

            ISdmxObjects structureBeans = structureWorkspace.GetStructureObjects(false);

            string output = string.Format(CultureInfo.InvariantCulture, "test-sdmxv3.0-{0}", fileInfo.Name);
            var writtingManager = new StructureWriterManager();
            using (var outputStream = new FileStream(output, FileMode.Create))
            {
                writtingManager.WriteStructures(structureBeans, new SdmxStructureFormat(StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV3StructureDocument)), outputStream);
            }

            using (var readable = new FileReadableDataLocation(output))
            {
                XMLParser.ValidateXml(readable, SdmxSchemaEnumType.VersionThree);
                var structures = structureReader.ParseStructures(readable);
                Assert.NotNull(structures);
            }
        }

        /// <summary>
        /// Test unit for <see cref="StructureWriterManager.WriteStructures"/>
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        [TestCase("tests/v21/Structure/ESTAT+STS+2.0-DF-DSD-full.xml")]
        public void TestWriteStructuresTo31AddMeasures(string file)
        {
            var structureReader = new StructureParsingManager();
            var fileInfo = new FileInfo(file);
            IStructureWorkspace structureWorkspace;
            using (var readable = new FileReadableDataLocation(fileInfo))
            {
                structureWorkspace = structureReader.ParseStructures(readable);
            }

            ISdmxObjects structureBeans = structureWorkspace.GetStructureObjects(false);

            IDataStructureObject dataStructureObject = structureBeans.DataStructures.First();
            structureBeans.DataStructures.Remove(dataStructureObject);

            var mutable = dataStructureObject.MutableInstance;
            var oldPrimaryMeasure = mutable.PrimaryMeasure;
            mutable.PrimaryMeasure = null;
            mutable.Measures.Clear();
            IMeasureMutableObject measure1 = new MeasureMutableCore();
            measure1.Id = "MEASURE_1";
            measure1.ConceptRef = oldPrimaryMeasure.ConceptRef;
            measure1.Representation = new RepresentationMutableCore() {  MaxOccurs = new FiniteOccurenceObjectCore(2)};
            measure1.Representation.TextFormat = new TextFormatMutableCore() { TextType = TextType.GetFromEnum(TextEnumType.Double) };
            mutable.AddMeasure(measure1);

             IMeasureMutableObject measure2 = new MeasureMutableCore();
            measure2.Id = "MEASURE_2";
            measure2.ConceptRef = oldPrimaryMeasure.ConceptRef;
            measure2.Representation = oldPrimaryMeasure.Representation;
            mutable.AddMeasure(measure2);

            // get the first coded attribute. A bit random
            var arrayValueAttr = mutable.Attributes.First(x => x.Representation?.Representation != null);
            arrayValueAttr.Representation.MinOccurs = 2;
            arrayValueAttr.Representation.MaxOccurs = UnboundedOccurenceObjectCore.Instance;

            structureBeans.AddDataStructure(mutable.ImmutableInstance);
            
            string output = string.Format(CultureInfo.InvariantCulture, "test-sdmxv3.0.0-measures-array-{0}", fileInfo.Name);
            var writtingManager = new StructureWriterManager();
            using (var outputStream = new FileStream(output, FileMode.Create))
            {
                writtingManager.WriteStructures(structureBeans, new SdmxStructureFormat(StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV3StructureDocument)), outputStream);
            }

            using (var readable = new FileReadableDataLocation(output))
            {
                XMLParser.ValidateXml(readable, SdmxSchemaEnumType.VersionThree);
                var structures = structureReader.ParseStructures(readable);
                Assert.NotNull(structures);
            }
        }
      /// <summary>
        /// Test unit for <see cref="StructureWriterManager.WriteStructures"/>
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        [TestCase("tests/v21/Structure/ESTAT+STS+2.0-DF-DSD-full.xml")]
        public void TestWriteStructuresTo31ExtraComplex(string file)
        {
            var structureReader = new StructureParsingManager();
            var fileInfo = new FileInfo(file);
            IStructureWorkspace structureWorkspace;
            using (var readable = new FileReadableDataLocation(fileInfo))
            {
                structureWorkspace = structureReader.ParseStructures(readable);
            }

            ISdmxObjects structureBeans = structureWorkspace.GetStructureObjects(false);

            IDataStructureObject dataStructureObject = structureBeans.DataStructures.First();
            structureBeans.DataStructures.Remove(dataStructureObject);

            var mutable = dataStructureObject.MutableInstance;
            var oldPrimaryMeasure = mutable.PrimaryMeasure;
            mutable.PrimaryMeasure = null;
            mutable.Measures.Clear();
            IMeasureMutableObject measure1 = new MeasureMutableCore();
            measure1.Id = "MEASURE_1";
            measure1.ConceptRef = oldPrimaryMeasure.ConceptRef;
            measure1.Representation = new RepresentationMutableCore() {  MaxOccurs = new FiniteOccurenceObjectCore(2)};
            measure1.Representation.TextFormat = new TextFormatMutableCore() { TextType = TextType.GetFromEnum(TextEnumType.Double) };
            mutable.AddMeasure(measure1);

             IMeasureMutableObject measure2 = new MeasureMutableCore();
            measure2.Id = "MEASURE_2";
            measure2.ConceptRef = oldPrimaryMeasure.ConceptRef;
            measure2.Representation = oldPrimaryMeasure.Representation;
            mutable.AddMeasure(measure2);

            // get the first coded attribute. A bit random
            var arrayValueAttr = mutable.Attributes.First(x => x.Representation?.Representation != null);
            arrayValueAttr.Representation.MinOccurs = 2;
            arrayValueAttr.Representation.MaxOccurs = UnboundedOccurenceObjectCore.Instance;
            
            // get the first ucoded attribute for sentinel value
            arrayValueAttr = mutable.Attributes.First(x => x.Representation?.Representation == null);
            if (arrayValueAttr.Representation?.TextFormat == null)
            {
                arrayValueAttr.Representation = new RepresentationMutableCore();
                arrayValueAttr.Representation.TextFormat = new TextFormatMutableCore();
            }

            arrayValueAttr.Representation.TextFormat.SentinelValues.Add(new SentinelValueMutableCore() { Value = "NOT_APPLICABLE", Names = new TextTypeWrapperMutableCore[] { new TextTypeWrapperMutableCore() { Locale = "en", Value = "Special value for not applicable" } } });


            // first group attribute
            arrayValueAttr = mutable.Attributes.First(x => x.AttachmentLevel == AttributeAttachmentLevel.Group);
            if (arrayValueAttr.Representation == null)
            {
                arrayValueAttr.Representation = new RepresentationMutableCore();
                arrayValueAttr.Representation.TextFormat = new TextFormatMutableCore();
            }

            arrayValueAttr.Representation.MinOccurs = 1;
            arrayValueAttr.Representation.MaxOccurs = new FiniteOccurenceObjectCore(2);

            // first series attribute
            arrayValueAttr = mutable.Attributes.First(x => x.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup);
            if (arrayValueAttr.Representation == null)
            {
                arrayValueAttr.Representation = new RepresentationMutableCore();
                arrayValueAttr.Representation.TextFormat = new TextFormatMutableCore();
            }


            arrayValueAttr.Representation.MinOccurs = 3;
            arrayValueAttr.Representation.MaxOccurs = UnboundedOccurenceObjectCore.Instance;


            // add dataset attribute
            IAttributeMutableObject datasetAttribute = new AttributeMutableCore();
            datasetAttribute.Id = "DATASET_ATTRIBUTE";
            datasetAttribute.ConceptRef = structureBeans.ConceptSchemes.First().Items.First().AsReference;
            datasetAttribute.Representation = new RepresentationMutableCore();
            datasetAttribute.Representation.MaxOccurs = new FiniteOccurenceObjectCore(5);
            datasetAttribute.Representation.TextFormat = new TextFormatMutableCore();
            datasetAttribute.Representation.TextFormat.TextType = TextType.GetFromEnum(TextEnumType.Integer);
            datasetAttribute.Usage = UsageType.Mandatory;
            datasetAttribute.AttachmentLevel = AttributeAttachmentLevel.DataSet;
            mutable.AddAttribute(datasetAttribute);

            structureBeans.AddDataStructure(mutable.ImmutableInstance);
            
            string output = string.Format(CultureInfo.InvariantCulture, "test-sdmxv3.0.0-extracomplex-{0}", fileInfo.Name);
            var writtingManager = new StructureWriterManager();
            using (var outputStream = new FileStream(output, FileMode.Create))
            {
                writtingManager.WriteStructures(structureBeans, new SdmxStructureFormat(StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV3StructureDocument)), outputStream);
            }

            using (var readable = new FileReadableDataLocation(output))
            {
                XMLParser.ValidateXml(readable, SdmxSchemaEnumType.VersionThree);
                var structures = structureReader.ParseStructures(readable);
                Assert.NotNull(structures);
            }
        }


        #endregion
    }
}