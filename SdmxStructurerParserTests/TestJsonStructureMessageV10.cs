// -----------------------------------------------------------------------
// <copyright file="TestJsonStructureWritingMessage.cs" company="EUROSTAT">
//   Date Created : 2016-07-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParserTests.
//     SdmxStructureParserTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParserTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParserTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Linq;

namespace SdmxStructureParserTests
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Structureparser.Factory;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Translator;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Extension;

    /// <summary>
    /// Unit Test for Json Structure message.
    /// </summary>
    [TestFixture]
    public class TestJsonStructureMessageV10
    {
        /// <summary>
        /// The _parsing manager
        /// </summary>
        private readonly IStructureParsingManager _parsingManager = new StructureParsingManager();

        private static PartyCore BuildSender()
        {
            var senderContact1Mutable = new ContactMutableObjectCore();
            senderContact1Mutable.AddDepartment(new TextTypeWrapperMutableCore("en", "A department"));
            senderContact1Mutable.AddName(new TextTypeWrapperMutableCore("en", "a contact name"));
            senderContact1Mutable.AddName(new TextTypeWrapperMutableCore("it", "a contact name"));
            senderContact1Mutable.AddEmail("sender@example.com");
            senderContact1Mutable.AddTelephone("+12 (0)34567890");
            var senderContact2Mutable = new ContactMutableObjectCore();
            senderContact2Mutable.AddRole(new TextTypeWrapperMutableCore("en", "A role"));
            senderContact2Mutable.AddRole(new TextTypeWrapperMutableCore("it", "A role"));
            senderContact1Mutable.AddEmail("sender2@example.com");

            IContact senderContact1 = new ContactCore(senderContact1Mutable);
            IContact senderContact2 = new ContactCore(senderContact2Mutable);
            IList<IContact> senderContacts = new[] { senderContact1, senderContact2 };
            var sender = new PartyCore(null, "TestSender", senderContacts, null);
            return sender;
        }

        [TestCase("tests/DFsWithAllRefs.xml")]
        [TestCase("tests/AgencySchemes.xml")]
        [TestCase("tests/CategorySchemesWithAllRefs.xml")]
        [TestCase("tests/CodeListsWithRef.xml")]
        [TestCase("tests/v21/demography2.xml")]
        [TestCase("tests/CS_HIERARCHY+OECD+1.0.xml")]
        [TestCase("tests/codelist_ECB_CL_CURRENCY.xml")]
        [TestCase("tests/ESTAT_DSD.xml")]
        [TestCase("tests/DSD_withConstraints.xml")]
        [TestCase("tests/Constraints.xml")]
        [TestCase("tests/v21/TEST_ANN.xml")]
        [TestCase("tests/v21/264D_264_SALDI2_ATTRIBUTE_TEST.xml")]
        [TestCase("tests/v21/Structure/HierarchicalCodelists.xml")]
        [TestCase("tests/v21/Structure/StructureSet-sdmxv2.1-ESTAT+STS+2.0.xml")]
        [TestCase("tests/v21/Structure/MetadataflowWithMSDv21-TEST.xml")]
        [TestCase("tests/v21/Structure/test-pa.xml")]
        public void TestWriteJsonFormatV10(string file)
        {
            //0: Read from xml file source
            var sourceSdmxObjects = new FileInfo(file).GetSdmxObjects(this._parsingManager);

            sourceSdmxObjects.Header.Sender = BuildSender();

            var preferedLanguageTranslator = new PreferedLanguageTranslator(
                new List<CultureInfo>()
                {
                    CultureInfo.GetCultureInfo("it"),
                    CultureInfo.GetCultureInfo("fr"),
                    CultureInfo.GetCultureInfo("en")
                },
                new List<CultureInfo>()
                {
                    CultureInfo.GetCultureInfo("fr"),
                    CultureInfo.GetCultureInfo("en")
                },
                CultureInfo.GetCultureInfo("en"));

            SdmxStructureJsonFormat sdmxStructureJsonFormat = new SdmxStructureJsonFormat(preferedLanguageTranslator, StructureOutputFormatEnumType.JsonV10);
            
            var outputFileName = string.Format(CultureInfo.InvariantCulture, "{0}-output.json", file);

            IStructureWriterManager jsonStructureWritingManager = new StructureWriterManager(new SdmxJsonStructureWriterFactory());

            //Write result to Json file
            using (FileStream stream = new FileStream(outputFileName, FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                jsonStructureWritingManager.WriteStructures(sourceSdmxObjects, sdmxStructureJsonFormat, stream);
            }
            
            //Get the sdmx objects from the newly created JSON file
            var jsonResultobjects = new FileInfo(outputFileName).GetSdmxObjects(this._parsingManager);

            // dataflows

            Assert.AreEqual(sourceSdmxObjects.Dataflows.Count, jsonResultobjects.Dataflows.Count);

            foreach(var sourceDataflow in sourceSdmxObjects.Dataflows)
            {
                var jsonDataflow = jsonResultobjects.Dataflows.FirstOrDefault(x =>
                    x.Id.Equals(sourceDataflow.Id, StringComparison.InvariantCultureIgnoreCase) && 
                    x.AgencyId.Equals(sourceDataflow.AgencyId, StringComparison.InvariantCultureIgnoreCase) &&
                    x.Version.Equals(sourceDataflow.Version, StringComparison.InvariantCultureIgnoreCase));

                Assert.IsNotNull(jsonDataflow);

                // currently json writes only 1 language so includeFinalProperties=false to ignore it.

                Assert.IsTrue(sourceDataflow.DeepEquals(jsonDataflow, false), jsonDataflow.AsReference.MaintainableReference.ToString());
            }
        }

        [TestCase("tests/DFsWithAllRefs.xml")]
        [TestCase("tests/AgencySchemes.xml")]
        [TestCase("tests/CategorySchemesWithAllRefs.xml")]
        [TestCase("tests/CodeListsWithRef.xml")]
        [TestCase("tests/v21/demography2.xml")]
        [TestCase("tests/CS_HIERARCHY+OECD+1.0.xml")]
        [TestCase("tests/codelist_ECB_CL_CURRENCY.xml")]
        [TestCase("tests/ESTAT_DSD.xml")]
        [TestCase("tests/DSD_withConstraints.xml")]
        [TestCase("tests/Constraints.xml")]
        [TestCase("tests/v21/TEST_ANN.xml")]
        [TestCase("tests/v21/264D_264_SALDI2_ATTRIBUTE_TEST.xml")]
        [TestCase("tests/v21/264D_264_SALDI2_BEST_LANG_MATCH_TEST.xml")]
        public void TestWriteJsonFormatV10WithBestMatchLanguageTranslator(string file)
        {
            //0: Read from xml file source
            var sdmxObjects = new FileInfo(TestContext.CurrentContext.TestDirectory + "\\" + file).GetSdmxObjects(this._parsingManager);

            sdmxObjects.Header.Sender = BuildSender();

            var preferredLanguageTranslator = new BestMatchLanguageTranslator(
                new List<CultureInfo>()
                {
                    CultureInfo.GetCultureInfo("hu"),
                    CultureInfo.GetCultureInfo("hu-HU"),
                    CultureInfo.GetCultureInfo("fr"),
                    CultureInfo.GetCultureInfo("en-US"),
                    CultureInfo.GetCultureInfo("en-GB")
                },
                sdmxObjects,
                CultureInfo.GetCultureInfo("en"));

            SdmxStructureJsonFormat sdmxStructureJsonFormat = new SdmxStructureJsonFormat(preferredLanguageTranslator, StructureOutputFormatEnumType.JsonV10);

            var outputFileName = TestContext.CurrentContext.TestDirectory + "\\" + string.Format(CultureInfo.InvariantCulture, "{0}-output-bestlangmatch.json", file);
            if (File.Exists(outputFileName))
                File.Delete(outputFileName);

            IStructureWriterManager jsonStructureWritingManager = new StructureWriterManager(new SdmxJsonStructureWriterFactory());

            //Write result to Json file
            using (FileStream stream = File.Create(outputFileName))
            {
                jsonStructureWritingManager.WriteStructures(sdmxObjects, sdmxStructureJsonFormat, stream);
            }

            var parsingManager = new StructureParsingJsonManager(sdmxStructureJsonFormat);

            //Get the sdmx objects from the newly created JSON file
            var jsonResultobjects = new FileInfo(outputFileName).GetSdmxObjects(parsingManager);

            //Test if the newly created SDMX-JSON objects has same number of objects as the first SDMX-ML source objects
            //TODO: this fails because the SDMXObjectJsonV1Builder doesn't return Constraints
            //Assert.AreEqual(sdmxObjects.GetAllMaintainables().Count, jsonResultobjects.GetAllMaintainables().Count);

        }

    }
}