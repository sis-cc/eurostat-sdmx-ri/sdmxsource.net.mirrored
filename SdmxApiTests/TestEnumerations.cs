// -----------------------------------------------------------------------
// <copyright file="TestEnumerations.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxApiTests.
//     SdmxApiTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxApiTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxApiTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxApiTests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

    /// <summary>
    ///     Tests for Enumerations
    /// </summary>
    [TestFixture]
    public class TestEnumerations
    {
        #region Public Methods and Operators

        /// <summary>
        /// The test base data format.
        /// </summary>
        [Test]
        public void TestBaseDataFormat()
        {
            IEnumerable<BaseDataFormat> values = BaseDataFormat.Values;
            foreach (BaseDataFormat baseDataFormat in values)
            {
                string s = baseDataFormat.ToString();
                Assert.NotNull(s);
                Trace.WriteLine(baseDataFormat + ":" + s);
                string rootNode = baseDataFormat.RootNode;
                Assert.IsTrue(rootNode == null || rootNode.Length > 0);
                Trace.WriteLine(baseDataFormat + ":" + rootNode);
            }
        }

        /// <summary>
        /// The test sdmx structure dict type.
        /// </summary>
        [Test]
        public void TestSdmxStructureDictType()
        {
            for (int i = 0; i < 1000000; i++)
            {
                SdmxStructureType x = SdmxStructureType.ParseClass("OrganisationMap");
                foreach (SdmxStructureType sdmxStructureType in SdmxStructureType.Values)
                {
                    SdmxStructureType parentStructureType = sdmxStructureType.ParentStructureType;

                    string v2Class = sdmxStructureType.V2Class;
                }
            }
        }

        /// <summary>
        /// The test sdmx structure type.
        /// </summary>
        [Test]
        public void TestSdmxStructureType()
        {
            for (int i = 0; i < 1000000; i++)
            {
                SdmxStructureType x = SdmxStructureType.ParseClass("OrganisationMap");
                foreach (SdmxStructureType sdmxStructureType in SdmxStructureType.Values)
                {
                    SdmxStructureType parentStructureType = sdmxStructureType.ParentStructureType;

                    string v2Class = sdmxStructureType.V2Class;
                }
            }
        }

        [TestCaseSource(nameof(MaintainableTestCases))]
        public void TestSdmxStructureTypeMaintainables(SdmxStructureEnumType enumType, 
            bool isMaintainable, Type maintainableInterface, SdmxStructureType maintainableStructureType, int nestedDepth)
        {
            var controlType = SdmxStructureType.GetFromEnum(enumType);

            Assert.AreEqual(isMaintainable, controlType.IsMaintainable);
            Assert.AreEqual(nestedDepth, controlType.NestedDepth);
            Assert.AreEqual(maintainableInterface, controlType.MaintainableInterface);
            Assert.AreEqual(maintainableStructureType?.EnumType, controlType.MaintainableStructureType?.EnumType);
        }

        private static object[] MaintainableTestCases()
        {
            return new object[]
            {
                new object[] { SdmxStructureEnumType.Annotation, false, null, null, 0 }, // plain type
                new object[] { SdmxStructureEnumType.AgencyScheme, true, typeof(IAgencyScheme), 
                    SdmxStructureType.GetFromEnum(SdmxStructureEnumType.AgencyScheme), 0 }, // maintainable type
                new object[] { SdmxStructureEnumType.Agency, false, typeof(IAgencyScheme), 
                    SdmxStructureType.GetFromEnum(SdmxStructureEnumType.AgencyScheme), 0 }, // child of maintainable type
                new object[] { SdmxStructureEnumType.DataAttribute, false, typeof(IDataStructureObject),
                    SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd), 1 }, // child of child type
            };
        }

        #endregion
    }
}