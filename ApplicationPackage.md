Application package /rollout  contains:
* app:  Folder containing the SdmxSource libraries
* doc:
..* ApiDocumentation.chm
..* SDMX RI developers migration guide.doc
..* ProgrammersGuide_.Net.docx
* lib: Folder containing the dll dependencies of the application
* src: Folder containing the full source code of the application
