using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.Api.Factory;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.Api.Model.Header;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.DataParser.Manager;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Sdmx.Structureparser.Workspace;
using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Util.Date;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;
using Org.Sdmxsource.Util.Io;

namespace SdmxDataParserTests
{
    [TestFixture]
    public class StructureSpecific30WriterTest
    {
        private DataType dataFormatType = DataType.GetFromEnum(DataEnumType.Compact30);

        private static string STRUCTURE_FILES_DIRECTORY = "tests/v30/samples/";

        private string V30_DSD_SIMPLE = STRUCTURE_FILES_DIRECTORY + "Data Structure Definition/EXR.xml";
        private string V30_DSD_COMPLEX = STRUCTURE_FILES_DIRECTORY + "Data - Complex Data Attributes/ECB_EXR_CA_DSD.xml";
        private string V30_DSD_MULTIPLE_MEASURES = "tests/v30/datastructure-3.0.xml"; 
        private static string V30_MULTI_MEASURE_COMPLEX = "tests/v30/test-sdmxv3.0.0-measures-array-ESTAT+STS+2.0-DF-DSD-full.xml";
        private static string V30_MULTI_MEASURE_COMPLEX_XL = "tests/v30/TEST_XL_COMPL+STS+2.0-DF-DSD-full.xml";
        private readonly IReadableDataLocationFactory _dataLocationFactory = new ReadableDataLocationFactory();

        [Test]
        public void SimpleDataASttributesTest()
        {

            // WRITE XML
            String mode = "simple";
            var dataFormat = new SdmxDataFormatCore(dataFormatType);
            var structureObjects = GetSdmxObjects(V30_DSD_SIMPLE);
            var dataStructure = structureObjects.DataStructures.FirstOrDefault();
            var dataflow = structureObjects.Dataflows.FirstOrDefault();
            var datasetHeaderBean = new DatasetHeaderCore("SOME_DATASET_ID", DatasetAction.GetFromEnum(DatasetActionEnumType.Information), new DatasetStructureReferenceCore(dataStructure.AsReference));

            var dataWriterManager = new DataWriterManager();
            var startTime = new DateTime(2005, 1, 1);
            using (Stream writer = File.Create(mode))
            {
                using (IDataWriterEngine dataWriter = dataWriterManager.GetDataWriterEngine(dataFormat, writer))
                {
                    var header = GetHeader(dataStructure);
                    dataWriter.WriteHeader(header);
                    dataWriter.StartDataset(dataflow, dataStructure, datasetHeaderBean);

                    dataWriter.StartSeries();
                    dataWriter.WriteSeriesKeyValue("FREQ", "A");
                    dataWriter.WriteSeriesKeyValue("CURRENCY", "CAD");
                    dataWriter.WriteSeriesKeyValue("CURRENCY_DENOM", "EUR");
                    dataWriter.WriteSeriesKeyValue("EXR_TYPE", "SP00");
                    dataWriter.WriteSeriesKeyValue("EXR_SUFFIX", "A");
                    dataWriter.WriteSeriesKeyValue("TIME_FORMAT", "P1Y");
                    dataWriter.WriteAttributeValue("COLLECTION", "A");
                    dataWriter.WriteAttributeValue("DECIMALS", "4");
                    dataWriter.WriteAttributeValue("SOURCE_AGENCY", "4F0");
                    dataWriter.WriteAttributeValue("TITLE", "Canadian dollar/Euro");
                    dataWriter.WriteAttributeValue("TITLE_COMPL", "ECB reference exchange rate, Canadian dollar/Euro, 2:15 pm (C.E.T.)");
                    dataWriter.WriteAttributeValue("UNIT", "CAD");
                    dataWriter.WriteAttributeValue("UNIT_MULT", "0");

                    List<String> periods = GetPeriods(10, "A", startTime);

                    for (int i = 0; i < 10; i++)
                    {
                        String period = periods.ElementAt(i);
                        dataWriter.WriteObservation(period, "1." + i);
                        dataWriter.WriteAttributeValue("OBS_STATUS", "A");
                    }
                }
            }

            // READ & VERIFY XML


            using (var sourceData = _dataLocationFactory.GetReadableDataLocation(new FileInfo(mode)))
            using (var reader = new DataReaderManager().GetDataReaderEngine(sourceData, structureObjects.DataStructures.FirstOrDefault(), null))
            {
                while (reader.MoveNextDataset())
                {
                    while (reader.MoveNextKeyable())
                    {
                        if (reader.KeyablePosition == 0)
                        {
                            var currentKey = reader.CurrentKey;
                            Assert.AreEqual(currentKey.GetKeyValue("FREQ"), "A");
                            Assert.AreEqual(currentKey.GetKeyValue("CURRENCY"), "CAD");
                            Assert.AreEqual(currentKey.GetKeyValue("CURRENCY_DENOM"), "EUR");
                            Assert.AreEqual(currentKey.GetKeyValue("EXR_TYPE"), "SP00");
                            Assert.AreEqual(currentKey.GetKeyValue("EXR_SUFFIX"), "A");
                            Assert.AreEqual(currentKey.GetAttribute("TIME_FORMAT").Code, "P1Y");
                            Assert.AreEqual(currentKey.GetAttribute("COLLECTION").Code, "A");
                            Assert.AreEqual(currentKey.GetAttribute("DECIMALS").Code, "4");
                            Assert.AreEqual(currentKey.GetAttribute("TITLE").Code, "Canadian dollar/Euro");
                            Assert.AreEqual(currentKey.GetAttribute("TITLE_COMPL").Code, "ECB reference exchange rate, Canadian dollar/Euro, 2:15 pm (C.E.T.)");
                            Assert.AreEqual(currentKey.GetAttribute("UNIT").Code, "CAD");
                            Assert.AreEqual(currentKey.GetAttribute("UNIT_MULT").Code, "0");
                            if (currentKey.Series)
                            {
                                reader.MoveNextObservation();
                                var currentObservation = reader.CurrentObservation;
                                Assert.AreEqual(currentObservation.ObsTime, "2005");
                                Assert.AreEqual(currentObservation.ObservationValue, "1.0");
                                Assert.AreEqual(currentObservation.GetAttribute("OBS_STATUS").Code, "A");
                                while (reader.MoveNextObservation())
                                {

                                }
                            }
                        }
                    }
                }
            }
        }

        [Test]
        public void ComplexDataAttributesTest()
        {

            // WRITE XML
            String mode = "complex";
            var dataFormat = new SdmxDataFormatCore(dataFormatType);
            var structureObjects = GetSdmxObjects(V30_DSD_COMPLEX);
            var dataStructure = structureObjects.DataStructures.FirstOrDefault();
            var dataflow = structureObjects.Dataflows.FirstOrDefault();
            var datasetHeaderBean = new DatasetHeaderCore("SOME_DATASET_ID", DatasetAction.GetFromEnum(DatasetActionEnumType.Information), new DatasetStructureReferenceCore(dataStructure.AsReference)); var startTime = new DateTime(2005, 1, 1);

            List<IComplexNodeValue> complexNodes1 = new List<IComplexNodeValue>();

            List<ITextTypeWrapper> wrapperList1 = new List<ITextTypeWrapper>();
            var engTitle1 = new TextTypeWrapperImpl("en", "Some English Text", null);
            var frTitle1 = new TextTypeWrapperImpl("fr", "Quelques textes en anglais", null);
            wrapperList1.Add(engTitle1);
            wrapperList1.Add(frTitle1);
            ComplexNodeValue node1 = new ComplexNodeValue(typeof(List<ITextTypeWrapper>), null, wrapperList1);
            complexNodes1.Add(node1);

            List<ITextTypeWrapper> wrapperList2 = new List<ITextTypeWrapper>();
            ITextTypeWrapper engTitle2 = new TextTypeWrapperImpl(null, "Additional English Text where lang defaults to en", null);
            ITextTypeWrapper frTitle2 = new TextTypeWrapperImpl("fr", "Texte anglais supplémentaire", null);
            wrapperList2.Add(engTitle2);
            wrapperList2.Add(frTitle2);
            ComplexNodeValue node2 = new ComplexNodeValue(typeof(List<ITextTypeWrapper>), null, wrapperList2);
            complexNodes1.Add(node2);

            List<IComplexNodeValue> complexNodes2 = new List<IComplexNodeValue>();

            ComplexNodeValue node3 = new ComplexNodeValue(typeof(string), "4F0", null);
            ComplexNodeValue node4 = new ComplexNodeValue(typeof(string), "4D0", null);
            ComplexNodeValue node5 = new ComplexNodeValue(typeof(string), "CZ2", null);
            complexNodes2.Add(node3);
            complexNodes2.Add(node4);
            complexNodes2.Add(node5);

            List<IComplexNodeValue> complexNodes3 = new List<IComplexNodeValue>();

            IComplexNodeValue obsNode1 = new ComplexNodeValue(typeof(string), "J", null);
            IComplexNodeValue obsNode2 = new ComplexNodeValue(typeof(string), "U", null);
            IComplexNodeValue obsNode3 = new ComplexNodeValue(typeof(string), "N", null);
            complexNodes3.Add(obsNode1);
            complexNodes3.Add(obsNode2);
            complexNodes3.Add(obsNode3);


            var keyValue1 = new KeyValueImpl("TITLE", complexNodes1);

            var keyValue2 = new KeyValueImpl("SOURCE_AGENCY", complexNodes2);

            var obsKeyValue = new KeyValueImpl("OBS_STATUS", complexNodes3);

            var dataWriterManager = new DataWriterManager();

            using (Stream writer = File.Create(mode))
            using (IDataWriterEngine dataWriter = dataWriterManager.GetDataWriterEngine(dataFormat, writer))
            {

                var header = GetHeader(dataStructure);
                dataWriter.WriteHeader(header);
                dataWriter.StartDataset(dataflow, dataStructure, datasetHeaderBean);

                dataWriter.StartSeries();
                dataWriter.WriteSeriesKeyValue("FREQ", "A");
                dataWriter.WriteSeriesKeyValue("CURRENCY", "CAD");
                dataWriter.WriteSeriesKeyValue("CURRENCY_DENOM", "EUR");
                dataWriter.WriteSeriesKeyValue("EXR_TYPE", "SP00");
                dataWriter.WriteSeriesKeyValue("EXR_SUFFIX", "A");
                dataWriter.WriteSeriesKeyValue("TIME_FORMAT", "P1Y");
                dataWriter.WriteAttributeValue("COLLECTION", "A");
                dataWriter.WriteAttributeValue("DECIMALS", "4");
                dataWriter.WriteAttributeValue("TITLE_COMPL", "ECB reference exchange rate, Canadian dollar/Euro, 2:15 pm (C.E.T.)");
                dataWriter.WriteAttributeValue("UNIT", "CAD");
                dataWriter.WriteAttributeValue("UNIT_MULT", "0");

                dataWriter.WriteComplexAttributeValue(keyValue1);

                dataWriter.WriteComplexAttributeValue(keyValue2);

                List<String> periods = GetPeriods(10, "A", startTime);

                for (int i = 0; i < 1; i++)
                {
                    String period = periods.ElementAt(i);
                    dataWriter.WriteObservation(period, "1." + i);
                    dataWriter.WriteComplexAttributeValue(obsKeyValue);
                }

            }


            // READ & VERIFY XML

            using (var sourceData = _dataLocationFactory.GetReadableDataLocation(new FileInfo(mode)))
            using (var reader = new DataReaderManager().GetDataReaderEngine(sourceData, structureObjects.DataStructures.FirstOrDefault(), null))
            {
                while (reader.MoveNextDataset())
                {
                    while (reader.MoveNextKeyable())
                    {
                        if (reader.KeyablePosition == 0)
                        {
                            var currentKey = reader.CurrentKey;
                            Assert.AreEqual(currentKey.GetKeyValue("FREQ"), "A");
                            Assert.AreEqual(currentKey.GetKeyValue("CURRENCY"), "CAD");
                            Assert.AreEqual(currentKey.GetKeyValue("CURRENCY_DENOM"), "EUR");
                            Assert.AreEqual(currentKey.GetKeyValue("EXR_TYPE"), "SP00");
                            Assert.AreEqual(currentKey.GetKeyValue("EXR_SUFFIX"), "A");
                            Assert.AreEqual(currentKey.GetAttribute("TIME_FORMAT").Code, "P1Y");
                            Assert.AreEqual(currentKey.GetAttribute("COLLECTION").Code, "A");
                            Assert.AreEqual(currentKey.GetAttribute("DECIMALS").Code, "4");
                            Assert.AreEqual(currentKey.GetAttribute("TITLE_COMPL").Code, "ECB reference exchange rate, Canadian dollar/Euro, 2:15 pm (C.E.T.)");
                            Assert.AreEqual(currentKey.GetAttribute("UNIT").Code, "CAD");
                            Assert.AreEqual(currentKey.GetAttribute("UNIT_MULT").Code, "0");

                            // verify complex attributes

                            Assert.AreEqual(currentKey.GetAttribute("TITLE").ComplexNodeValues[0].Code, null);
                            Assert.AreEqual(currentKey.GetAttribute("TITLE").ComplexNodeValues[0].TextValues[0].Locale, "en");
                            Assert.AreEqual(currentKey.GetAttribute("TITLE").ComplexNodeValues[0].TextValues[0].Value, "Some English Text");
                            Assert.AreEqual(currentKey.GetAttribute("TITLE").ComplexNodeValues[0].TextValues[1].Locale, "fr");
                            Assert.AreEqual(currentKey.GetAttribute("TITLE").ComplexNodeValues[0].TextValues[1].Value, "Quelques textes en anglais");
                            Assert.AreEqual(currentKey.GetAttribute("TITLE").ComplexNodeValues[1].TextValues[0].Locale, "en");
                            Assert.AreEqual(currentKey.GetAttribute("TITLE").ComplexNodeValues[1].TextValues[0].Value, "Additional English Text where lang defaults to en");
                            Assert.AreEqual(currentKey.GetAttribute("TITLE").ComplexNodeValues[1].TextValues[1].Locale, "fr");
                            Assert.AreEqual(currentKey.GetAttribute("TITLE").ComplexNodeValues[1].TextValues[1].Value, "Texte anglais supplémentaire");

                            Assert.AreEqual(currentKey.GetAttribute("SOURCE_AGENCY").ComplexNodeValues[0].Code, "4F0");
                            Assert.AreEqual(currentKey.GetAttribute("SOURCE_AGENCY").ComplexNodeValues[1].Code, "4D0");
                            Assert.AreEqual(currentKey.GetAttribute("SOURCE_AGENCY").ComplexNodeValues[2].Code, "CZ2");


                            if (currentKey.Series)
                            {
                                reader.MoveNextObservation();
                                var currentObservation = reader.CurrentObservation;
                                Assert.AreEqual(currentObservation.ObsTime, "2005");
                                Assert.AreEqual(currentObservation.ObservationValue, "1.0");
                                Assert.AreEqual(currentObservation.GetAttribute("OBS_STATUS").ComplexNodeValues.Count, 3);
                                Assert.AreEqual(currentObservation.GetAttribute("OBS_STATUS").ComplexNodeValues[0].Code, "J");
                                Assert.AreEqual(currentObservation.GetAttribute("OBS_STATUS").ComplexNodeValues[1].Code, "U");
                                Assert.AreEqual(currentObservation.GetAttribute("OBS_STATUS").ComplexNodeValues[2].Code, "N");
                            }
                        }
                    }
                }

            }

        }


        [Test]
        public void MultipleMeasuresTest()
        {

            // WRITE XML
            String mode = "multMeasures";
            var dataFormat = new SdmxDataFormatCore(dataFormatType);
            var structureObjects = GetSdmxObjects(V30_DSD_MULTIPLE_MEASURES);
            var dataStructure = structureObjects.DataStructures.FirstOrDefault();
            var dataflow = structureObjects.Dataflows.FirstOrDefault();
            var datasetHeaderBean = new DatasetHeaderCore("SOME_DATASET_ID", DatasetAction.GetFromEnum(DatasetActionEnumType.Information), new DatasetStructureReferenceCore(dataStructure.AsReference)); var startTime = new DateTime(2005, 1, 1);


            List<IComplexNodeValue> complexNodes1 = new List<IComplexNodeValue>();

            ComplexNodeValue node1 = new ComplexNodeValue(typeof(string),"2.3", null);
            ComplexNodeValue node11 = new ComplexNodeValue(typeof(string),"NaN", null);
            complexNodes1.Add(node1);
            complexNodes1.Add(node11);

            List<IComplexNodeValue> complexNodes2 = new List<IComplexNodeValue>();

            ComplexNodeValue node2 = new ComplexNodeValue(typeof(string),"30000", null);
            ComplexNodeValue node22 = new ComplexNodeValue(typeof(string),"INF", null);
            complexNodes2.Add(node2);
            complexNodes2.Add(node22);

            var obsKeyValue1 = new KeyValueImpl("MEASURE2",complexNodes1);

            var obsKeyValue2 = new KeyValueImpl("MEASURE2",complexNodes2);

            using (Stream writer = File.Create(mode))
            using (IDataWriterEngine dataWriter = new DataWriterManager().GetDataWriterEngine(dataFormat, writer))
            {

                var header = GetHeader(dataStructure);
                dataWriter.WriteHeader(header);
                dataWriter.StartDataset(dataflow, dataStructure, datasetHeaderBean);

                dataWriter.StartSeries();
                dataWriter.WriteSeriesKeyValue("FREQ", "A");
                dataWriter.WriteSeriesKeyValue("CURRENCY", "CAD");
                dataWriter.WriteSeriesKeyValue("TIME_FORMAT", "P1Y");

                dataWriter.WriteObservation("1999");
                dataWriter.WriteMeasureValue("OBS_VALUE", "1.583993822393823");
                dataWriter.WriteMeasureValue("MEASURE1", "123.456");
                dataWriter.WriteComplexMeasureValue(obsKeyValue1);

                dataWriter.WriteObservation("2000");
                dataWriter.WriteMeasureValue("OBS_VALUE", "1.37058431372549");
                dataWriter.WriteMeasureValue("MEASURE1", "13.456");
                dataWriter.WriteComplexMeasureValue(obsKeyValue2);


            }

            // READ & VERIFY XML


            using (var sourceData = _dataLocationFactory.GetReadableDataLocation(new FileInfo(mode)))
            using (var reader = new DataReaderManager().GetDataReaderEngine(sourceData, structureObjects.DataStructures.FirstOrDefault(), null))
            {
                while (reader.MoveNextDataset())
                {
                    while (reader.MoveNextKeyable())
                    {
                        if (reader.KeyablePosition == 0)
                        {
                            var currentKey = reader.CurrentKey;
                            if (currentKey.Series)
                            {
                                reader.MoveNextObservation();
                                var currentObservation = reader.CurrentObservation;
                                Assert.AreEqual(currentObservation.ObsTime, "1999");
                                // SDMX 3.0.0 has no Observation Value ( -> PrimaryMeasure) only measures
                                Assert.IsNull(currentObservation.ObservationValue);
                                Assert.AreEqual(currentObservation.GetMeasure("OBS_VALUE").Code, "1.583993822393823");
                                Assert.AreEqual(currentObservation.GetMeasure("MEASURE1").Code, "123.456");
                                Assert.AreEqual(currentObservation.GetMeasure("MEASURE2").ComplexNodeValues[0].Code, "2.3");
                                Assert.AreEqual(currentObservation.GetMeasure("MEASURE2").ComplexNodeValues[1].Code, "NaN");

                                reader.MoveNextObservation();
                                var currentObservation2 = reader.CurrentObservation;
                                Assert.AreEqual(currentObservation2.ObsTime, "2000");
                                Assert.AreEqual(currentObservation2.GetMeasure("OBS_VALUE").Code, "1.37058431372549");
                                Assert.AreEqual(currentObservation2.GetMeasure("MEASURE1").Code, "13.456");
                                Assert.AreEqual(currentObservation2.GetMeasure("MEASURE2").ComplexNodeValues[0].Code, "30000");
                                Assert.AreEqual(currentObservation2.GetMeasure("MEASURE2").ComplexNodeValues[1].Code, "INF");
                            }
                        }
                    }
                }

            }
        }
        private static ISdmxObjects GetSdmxObjects(string fileName)
        {
            ISdmxObjects objects;
            var file = new FileInfo(fileName);
            IStructureParsingManager manager = new StructureParsingManager(SdmxSchemaEnumType.Null);
            using (var readable = new FileReadableDataLocation(file))
            {
                IStructureWorkspace structureWorkspace = manager.ParseStructures(readable);
                objects = structureWorkspace.GetStructureObjects(false);
            }

            return objects;
        }

        private static IHeader GetHeader(IDataStructureObject dataStructureObject)
        {
            IList<IDatasetStructureReference> structures = new List<IDatasetStructureReference> { new DatasetStructureReferenceCore(dataStructureObject.AsReference) };
            IList<IParty> receiver = new List<IParty> { new PartyCore(new List<ITextTypeWrapper>(), "ZZ9", new List<IContact>(), null) };
            var sender = new PartyCore(new List<ITextTypeWrapper> { new TextTypeWrapperImpl("en", "TEST SENDER", null) }, "ZZ1", null, null);
            IHeader header = new HeaderImpl(
                null,
                structures,
                null,
                DatasetAction.GetFromEnum(DatasetActionEnumType.Information),
                "TEST_DATAFLOW",
                "DATASET_ID",
                null,
                DateTime.Now,
                DateTime.Now,
                null,
                null,
                new List<ITextTypeWrapper> { new TextTypeWrapperImpl("en", "test header name", null) },
                new List<ITextTypeWrapper> { new TextTypeWrapperImpl("en", "source 1", null) },
                receiver,
                sender,
                true);
            return header;
        }

        private static List<string> GetPeriods(int nrOfPeriods, string key, DateTime startTime)
        {
            List<string> periods = new List<string>();


            for (int i = 0; i < nrOfPeriods; i++)
            {
                DateTime periodTime;
                switch (key)
                {
                    case "Q":
                        periodTime = startTime.AddMonths(3 * i);
                        periods.Add(DateUtil.FormatDate(periodTime, TimeFormatEnumType.QuarterOfYear));
                        break;
                    case "A":
                        periodTime = startTime.AddMonths(12 * i);
                        periods.Add(DateUtil.FormatDate(periodTime, TimeFormatEnumType.Year));
                        break;
                    case "M":
                        periodTime = startTime.AddMonths(i + 1);
                        periods.Add(DateUtil.FormatDate(periodTime, TimeFormatEnumType.Month));
                        break;
                    default:
                        break;
                }
            }

            return periods;
        }
      [Test]
        public void MultipleMeasuresComplexTest()
        {

            // WRITE XML
            String mode = "multMeasuresComplexr";
            var dataFormat = new SdmxDataFormatCore(dataFormatType);
            var structureObjects = GetSdmxObjects(V30_MULTI_MEASURE_COMPLEX);
            var dataStructure = structureObjects.DataStructures.FirstOrDefault();
            var dataflow = structureObjects.Dataflows.FirstOrDefault();
            var datasetHeaderBean = new DatasetHeaderCore("SOME_DATASET_ID", DatasetAction.GetFromEnum(DatasetActionEnumType.Information), new DatasetStructureReferenceCore(dataStructure.AsReference)); var startTime = new DateTime(2005, 1, 1);


            List<IComplexNodeValue> complexNodes1 = new List<IComplexNodeValue>();

            ComplexNodeValue node1 = new ComplexNodeValue(typeof(string),"2.3", null);
            ComplexNodeValue node11 = new ComplexNodeValue(typeof(string),"NaN", null);
            complexNodes1.Add(node1);
            complexNodes1.Add(node11);

            List<IComplexNodeValue> complexNodes2 = new List<IComplexNodeValue>();

            ComplexNodeValue node2 = new ComplexNodeValue(typeof(string),"30000", null);
            ComplexNodeValue node22 = new ComplexNodeValue(typeof(string),"INF", null);
            complexNodes2.Add(node2);
            complexNodes2.Add(node22);

            var obsKeyValue1 = new KeyValueImpl("MEASURE_1",complexNodes1);

            var obsKeyValue2 = new KeyValueImpl("MEASURE_1",complexNodes2);

            var obsConfValues = new List<IComplexNodeValue>();
            obsConfValues.Add(new ComplexNodeValue(typeof(string),"N"));
            obsConfValues.Add(new ComplexNodeValue(typeof(string),"C"));
            obsConfValues.Add(new ComplexNodeValue(typeof(string),"R"));

            var obsConfKeyValue = new KeyValueImpl("OBS_CONF", obsConfValues);

            using (Stream writer = File.Create(mode))
            using (IDataWriterEngine dataWriter = new DataWriterManager().GetDataWriterEngine(dataFormat, writer))
            {

                var header = GetHeader(dataStructure);
                dataWriter.WriteHeader(header);
                dataWriter.StartDataset(dataflow, dataStructure, datasetHeaderBean);

                dataWriter.StartSeries();
                dataWriter.WriteSeriesKeyValue("FREQ", "M");
                dataWriter.WriteSeriesKeyValue("REF_AREA", "GR");
                dataWriter.WriteSeriesKeyValue("ADJUSTMENT", "N");
                dataWriter.WriteSeriesKeyValue("STS_INDICATOR", "PROD");
                dataWriter.WriteSeriesKeyValue("STS_ACTIVITY", "NS0030");
                dataWriter.WriteSeriesKeyValue("STS_INSTITUTION", "1");
                dataWriter.WriteSeriesKeyValue("STS_BASE_YEAR", "2000");
                dataWriter.WriteAttributeValue("TIME_FORMAT", "P1Y");

                dataWriter.WriteObservation("1999-M01");
                dataWriter.WriteMeasureValue("MEASURE_2", "123.456");
                dataWriter.WriteComplexMeasureValue(obsKeyValue1);
                dataWriter.WriteComplexAttributeValue(obsConfKeyValue);

                dataWriter.WriteObservation("2000-M03");
                dataWriter.WriteMeasureValue("MEASURE_2", "13.456");
                dataWriter.WriteComplexMeasureValue(obsKeyValue2);
                dataWriter.WriteComplexAttributeValue(obsConfKeyValue);


            }

            // READ & VERIFY XML


            using (var sourceData = _dataLocationFactory.GetReadableDataLocation(new FileInfo(mode)))
            using (var reader = new DataReaderManager().GetDataReaderEngine(sourceData, structureObjects.DataStructures.FirstOrDefault(), null))
            {
                while (reader.MoveNextDataset())
                {
                    while (reader.MoveNextKeyable())
                    {
                        if (reader.KeyablePosition == 0)
                        {
                            var currentKey = reader.CurrentKey;
                            if (currentKey.Series)
                            {
                                reader.MoveNextObservation();
                                var currentObservation = reader.CurrentObservation;
                                Assert.AreEqual(currentObservation.ObsTime, "1999-M01");
                                Assert.AreEqual(currentObservation.GetMeasure("MEASURE_2").Code, "123.456");
                                Assert.AreEqual(currentObservation.GetMeasure("MEASURE_1").ComplexNodeValues[0].Code, "2.3");
                                Assert.AreEqual(currentObservation.GetMeasure("MEASURE_1").ComplexNodeValues[1].Code, "NaN");

                                reader.MoveNextObservation();
                                var currentObservation2 = reader.CurrentObservation;
                                Assert.AreEqual(currentObservation2.ObsTime, "2000-M03");
                                Assert.AreEqual(currentObservation2.GetMeasure("MEASURE_2").Code, "13.456");
                                Assert.AreEqual(currentObservation2.GetMeasure("MEASURE_1").ComplexNodeValues[0].Code, "30000");
                                Assert.AreEqual(currentObservation2.GetMeasure("MEASURE_1").ComplexNodeValues[1].Code, "INF");
                            }
                        }
                    }
                }

            }
        }

      [Test]
        public void MultipleMeasuresExtraComplexTest()
        {

            // WRITE XML
            String mode = "multMeasuresComplexr-xl.xml";
            var dataFormat = new SdmxDataFormatCore(dataFormatType);
            var structureObjects = GetSdmxObjects(V30_MULTI_MEASURE_COMPLEX_XL);
            var dataStructure = structureObjects.DataStructures.FirstOrDefault();
            var dataflow = structureObjects.Dataflows.FirstOrDefault();
            var datasetHeaderBean = new DatasetHeaderCore("SOME_DATASET_ID", DatasetAction.GetFromEnum(DatasetActionEnumType.Information), new DatasetStructureReferenceCore(dataStructure.AsReference)); var startTime = new DateTime(2005, 1, 1);


            List<IComplexNodeValue> complexNodes1 = new List<IComplexNodeValue>();

            ComplexNodeValue node1 = new ComplexNodeValue(typeof(string),"2.3", null);
            ComplexNodeValue node11 = new ComplexNodeValue(typeof(string),"NaN", null);
            complexNodes1.Add(node1);
            complexNodes1.Add(node11);

            List<IComplexNodeValue> complexNodes2 = new List<IComplexNodeValue>();

            ComplexNodeValue node2 = new ComplexNodeValue(typeof(string),"30000", null);
            ComplexNodeValue node22 = new ComplexNodeValue(typeof(string),"INF", null);
            complexNodes2.Add(node2);
            complexNodes2.Add(node22);

            var obsKeyValue1 = new KeyValueImpl("MEASURE_1",complexNodes1);

            var obsKeyValue2 = new KeyValueImpl("MEASURE_1",complexNodes2);

            var obsConfValues = new List<IComplexNodeValue>
            {
                new ComplexNodeValue(typeof(string), "N"),
                new ComplexNodeValue(typeof(string), "C"),
                new ComplexNodeValue(typeof(string), "R")
            };

            var obsConfKeyValue = new KeyValueImpl("OBS_CONF", obsConfValues);

            using (Stream writer = File.Create(mode))
            using (IDataWriterEngine dataWriter = new DataWriterManager().GetDataWriterEngine(dataFormat, writer))
            {

                var header = GetHeader(dataStructure);
                dataWriter.WriteHeader(header);
                dataWriter.StartDataset(dataflow, dataStructure, datasetHeaderBean);

                dataWriter.WriteComplexAttributeValue(KeyValueImpl.BuildComplex("DATASET_ATTRIBUTE", "1", "2", "3", "4"));

                dataWriter.StartGroup("Sibling");
                dataWriter.WriteGroupKeyValue("REF_AREA", "GR");
                dataWriter.WriteGroupKeyValue("ADJUSTMENT", "N");
                dataWriter.WriteGroupKeyValue("STS_INDICATOR", "PROD");
                dataWriter.WriteGroupKeyValue("STS_ACTIVITY", "NS0030");
                dataWriter.WriteGroupKeyValue("STS_INSTITUTION", "1");
                dataWriter.WriteGroupKeyValue("STS_BASE_YEAR", "2000");
                dataWriter.WriteAttributeValue("DECIMALS", "3");
                dataWriter.WriteComplexAttributeValue(KeyValueImpl.BuildComplex("COMPILATION", "Max Occurs=2", "1.1.2"));

                dataWriter.StartSeries();
                dataWriter.WriteSeriesKeyValue("FREQ", "M");
                dataWriter.WriteSeriesKeyValue("REF_AREA", "GR");
                dataWriter.WriteSeriesKeyValue("ADJUSTMENT", "N");
                dataWriter.WriteSeriesKeyValue("STS_INDICATOR", "PROD");
                dataWriter.WriteSeriesKeyValue("STS_ACTIVITY", "NS0030");
                dataWriter.WriteSeriesKeyValue("STS_INSTITUTION", "1");
                dataWriter.WriteSeriesKeyValue("STS_BASE_YEAR", "2000");
                dataWriter.WriteAttributeValue("TIME_FORMAT", "P1Y");
                dataWriter.WriteComplexAttributeValue(KeyValueImpl.BuildComplex("AVAILABILITY", "A", "B", "C"));

                dataWriter.WriteObservation("1999-M01");
                dataWriter.WriteMeasureValue("MEASURE_2", "123.456");
                dataWriter.WriteComplexMeasureValue(obsKeyValue1);
                dataWriter.WriteComplexAttributeValue(obsConfKeyValue);

                dataWriter.WriteObservation("2000-M03");
                dataWriter.WriteMeasureValue("MEASURE_2", "13.456");
                dataWriter.WriteComplexMeasureValue(obsKeyValue2);
                dataWriter.WriteComplexAttributeValue(obsConfKeyValue);

                dataWriter.StartSeries();
                dataWriter.WriteSeriesKeyValue("FREQ", "M");
                dataWriter.WriteSeriesKeyValue("REF_AREA", "IT");
                dataWriter.WriteSeriesKeyValue("ADJUSTMENT", "N");
                dataWriter.WriteSeriesKeyValue("STS_INDICATOR", "PROD");
                dataWriter.WriteSeriesKeyValue("STS_ACTIVITY", "NS0030");
                dataWriter.WriteSeriesKeyValue("STS_INSTITUTION", "1");
                dataWriter.WriteSeriesKeyValue("STS_BASE_YEAR", "2000");
                dataWriter.WriteAttributeValue("TIME_FORMAT", "P1Y");

                // start observation
                dataWriter.WriteObservation("2002-M01");

                // write measure 2
                dataWriter.WriteMeasureValue("MEASURE_2", "123.456");
                // write measure 1
                dataWriter.WriteComplexMeasureValue(obsKeyValue1);
                dataWriter.WriteComplexAttributeValue(obsConfKeyValue);

                // start observation
                dataWriter.WriteObservation("2005-M03");
                // write measure 2
                dataWriter.WriteMeasureValue("MEASURE_2", "13.456");
                // write measure 1
                dataWriter.WriteComplexMeasureValue(obsKeyValue2);
                dataWriter.WriteComplexAttributeValue(obsConfKeyValue);


            }

            // READ & VERIFY XML

            int numberOfObs = 0;
            int numberOfSeries = 0;
            int numberOfGroup = 0;
            int numberOfDatasets = 0;

            using (var sourceData = _dataLocationFactory.GetReadableDataLocation(new FileInfo(mode)))
            using (var reader = new DataReaderManager().GetDataReaderEngine(sourceData, structureObjects.DataStructures.FirstOrDefault(), null))
            {
                while (reader.MoveNextDataset())
                {
                    numberOfDatasets++;
                    Assert.That(reader.DatasetAttributes, Is.Not.Null.And.Not.Empty);
                    Assert.That(reader.DatasetAttributes[0].Concept, Is.EqualTo("DATASET_ATTRIBUTE"));
                    Assert.That(reader.DatasetAttributes[0].ComplexNodeValues.Count, Is.EqualTo(4));
                    Assert.That(reader.DatasetAttributes[0].ComplexNodeValues[0].Code, Is.EqualTo("1"));
                    Assert.That(reader.DatasetAttributes[0].ComplexNodeValues[1].Code, Is.EqualTo("2"));
                    Assert.That(reader.DatasetAttributes[0].ComplexNodeValues[2].Code, Is.EqualTo("3"));
                    Assert.That(reader.DatasetAttributes[0].ComplexNodeValues[3].Code, Is.EqualTo("4"));

                    while (reader.MoveNextKeyable())
                    {
                        var currentKey = reader.CurrentKey;
                        if (reader.CurrentKey.Series)
                        {

                            numberOfSeries++;
                            if (numberOfSeries == 1)
                            {
                                Assert.That(reader.CurrentKey.Attributes, Is.Not.Null.And.Not.Empty);
                                Assert.That(reader.CurrentKey.Attributes.Count, Is.EqualTo(2));
                                Assert.That(reader.CurrentKey.Attributes[0].Concept, Is.EqualTo("TIME_FORMAT"));
                                Assert.That(reader.CurrentKey.Attributes[0].Code, Is.EqualTo("P1Y"));

                                Assert.That(reader.CurrentKey.Attributes[1].Concept, Is.EqualTo("AVAILABILITY"));
                                Assert.That(reader.CurrentKey.Attributes[1].ComplexNodeValues.Count, Is.EqualTo(3));
                                Assert.That(reader.CurrentKey.Attributes[1].ComplexNodeValues[0].Code, Is.EqualTo("A"));
                                Assert.That(reader.CurrentKey.Attributes[1].ComplexNodeValues[1].Code, Is.EqualTo("B"));
                                Assert.That(reader.CurrentKey.Attributes[1].ComplexNodeValues[2].Code, Is.EqualTo("C"));


                                Assert.IsTrue(reader.MoveNextObservation());
                                numberOfObs++;
                                var currentObservation = reader.CurrentObservation;
                                Assert.AreEqual(currentObservation.ObsTime, "1999-M01");
                                Assert.AreEqual(currentObservation.GetMeasure("MEASURE_2").Code, "123.456");
                                Assert.AreEqual(currentObservation.GetMeasure("MEASURE_1").ComplexNodeValues[0].Code, "2.3");
                                Assert.AreEqual(currentObservation.GetMeasure("MEASURE_1").ComplexNodeValues[1].Code, "NaN");
                                Assert.That(currentObservation.Attributes, Is.Not.Null.And.Not.Empty);
                                Assert.That(currentObservation.Attributes[0].Concept, Is.EqualTo("OBS_CONF"));
                                Assert.That(currentObservation.Attributes[0].ComplexNodeValues.Count, Is.EqualTo(3));
                                Assert.That(currentObservation.Attributes[0].ComplexNodeValues[0].Code, Is.EqualTo("N"));
                                Assert.That(currentObservation.Attributes[0].ComplexNodeValues[1].Code, Is.EqualTo("C"));
                                Assert.That(currentObservation.Attributes[0].ComplexNodeValues[2].Code, Is.EqualTo("R"));
                                Assert.IsTrue(reader.MoveNextObservation());
                                numberOfObs++;
                                var currentObservation2 = reader.CurrentObservation;
                                Assert.AreEqual(currentObservation2.ObsTime, "2000-M03");
                                Assert.AreEqual(currentObservation2.GetMeasure("MEASURE_2").Code, "13.456");
                                Assert.AreEqual(currentObservation2.GetMeasure("MEASURE_1").ComplexNodeValues[0].Code, "30000");
                                Assert.AreEqual(currentObservation2.GetMeasure("MEASURE_1").ComplexNodeValues[1].Code, "INF");
                                Assert.That(currentObservation2.Attributes, Is.Not.Null.And.Not.Empty);
                                Assert.That(currentObservation2.Attributes[0].Concept, Is.EqualTo("OBS_CONF"));
                                Assert.That(currentObservation2.Attributes[0].ComplexNodeValues.Count, Is.EqualTo(3));
                                Assert.That(currentObservation2.Attributes[0].ComplexNodeValues[0].Code, Is.EqualTo("N"));
                                Assert.That(currentObservation2.Attributes[0].ComplexNodeValues[1].Code, Is.EqualTo("C"));
                                Assert.That(currentObservation2.Attributes[0].ComplexNodeValues[2].Code, Is.EqualTo("R"));
                            }
                            else
                            {
                                Assert.That(reader.CurrentKey.Attributes, Is.Not.Null.And.Not.Empty);
                                Assert.That(reader.CurrentKey.Attributes.Count, Is.EqualTo(1));
                                Assert.That(reader.CurrentKey.Attributes[0].Concept, Is.EqualTo("TIME_FORMAT"));
                                Assert.That(reader.CurrentKey.Attributes[0].Code, Is.EqualTo("P1Y"));

                                IObservation observation;
                                while (reader.MoveNextObservation())
                                {
                                    // this will read the observation and move the pointer.
                                    observation = reader.CurrentObservation;
                                    numberOfObs++;
                                }
                            }

                        }
                        else
                        {
                            numberOfGroup++;
                            Assert.That(reader.CurrentKey.Attributes, Is.Not.Null.And.Not.Empty);
                            Assert.That(reader.CurrentKey.Attributes.Count, Is.EqualTo(2));
                            Assert.That(reader.CurrentKey.Attributes[0].Concept, Is.EqualTo("DECIMALS"));
                            Assert.That(reader.CurrentKey.Attributes[0].Code, Is.EqualTo("3"));

                            Assert.That(reader.CurrentKey.Attributes[1].Concept, Is.EqualTo("COMPILATION"));
                            Assert.That(reader.CurrentKey.Attributes[1].ComplexNodeValues.Count, Is.EqualTo(2));
                            Assert.That(reader.CurrentKey.Attributes[1].ComplexNodeValues[0].Code, Is.EqualTo("Max Occurs=2"));
                            Assert.That(reader.CurrentKey.Attributes[1].ComplexNodeValues[1].Code, Is.EqualTo("1.1.2"));
                        }

                    }
                }
                Assert.AreEqual(1, numberOfDatasets);
                Assert.AreEqual(1, numberOfGroup);
                Assert.AreEqual(2, numberOfSeries);
                Assert.AreEqual(4, numberOfObs); 

            }
        }


    } 
}
