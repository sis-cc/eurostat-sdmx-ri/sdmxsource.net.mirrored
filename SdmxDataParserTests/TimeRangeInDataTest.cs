// -----------------------------------------------------------------------
// <copyright file="TimeRangeInDataTest.cs" company="EUROSTAT">
//   Date Created : 2018-4-6
//   Copyright (c) 2018 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjectsTests.
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxDataParserTests
{
    using System.IO;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;
    using Org.Sdmxsource.Sdmx.DataParser.Transform;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    using Org.Sdmxsource.Util.Io;
    using Org.Sdmxsource.Util.ResourceBundle;

    [TestFixture]
    public class TimeRangeInDataTest
    {
        /// <summary>
        /// The _factory
        /// </summary>
        private readonly IReadableDataLocationFactory _factory;

        private readonly IStructureParsingManager _structureParsingManager;

        private readonly IDataReaderManager _dataReaderManager = new DataReaderManager();
        private readonly IDataWriterManager _dataWriterManager = new DataWriterManager();
        private readonly IDataReaderWriterTransform _dataReaderWriterTransform = new DataReaderWriterTransform();

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeRangeInDataTest"/> class.
        /// </summary>
        public TimeRangeInDataTest()
        {
            this._factory = new ReadableDataLocationFactory();
            _structureParsingManager = new StructureParsingManager();
            SdmxException.SetMessageResolver(new MessageDecoder());
        }

        [TestCase(@"tests\Data\ESTAT_NA_MAIN_1_0-Range.xml", @"tests\v21\Structure\ESTAT+NA_MAIN+1.0.xml", DataEnumType.Compact21)]
        [TestCase(@"tests\Data\ESTAT_NA_MAIN_1_0-Range.xml", @"tests\v21\Structure\ESTAT+NA_MAIN+1.0.xml", DataEnumType.Generic21)]
        public void ShouldWriteSdmx21DataWithTimeRange(string file, string dsd, DataEnumType dataEnumType)
        {
            var retrievalManager = new InMemoryRetrievalManager();
            retrievalManager.SaveStructures(new FileInfo(dsd).GetSdmxObjects(this._structureParsingManager));
            var outputFile = new FileInfo(file + ".output.xml");
            var dataformat = new SdmxDataFormatCore(DataType.GetFromEnum(dataEnumType));

            using (var sourceData = this._factory.GetReadableDataLocation(new FileInfo(file)))
            using (var dataReaderEngine = this._dataReaderManager.GetDataReaderEngine(sourceData, retrievalManager))
            using (var outputStream = outputFile.Create())
            using (var dataWriterEngine = this._dataWriterManager.GetDataWriterEngine(dataformat, outputStream))
            {
                this._dataReaderWriterTransform.CopyToWriter(dataReaderEngine, dataWriterEngine, true, true);
                outputStream.Flush();
            }

            outputFile.Refresh();

            int obsCount = 0;
            using (var sourceData = this._factory.GetReadableDataLocation(outputFile))
            using (var dataReaderEngine = this._dataReaderManager.GetDataReaderEngine(sourceData, retrievalManager))
            {
                while (dataReaderEngine.MoveNextKeyable())
                {
                    var currentKey = dataReaderEngine.CurrentKey;
                    if (currentKey.Series)
                    {
                        while (dataReaderEngine.MoveNextObservation())
                        {
                            var obs = dataReaderEngine.CurrentObservation;
                            Assert.That(obs.ObsTimeFormat, Is.EqualTo(TimeFormat.TimeRange));
                            obsCount++;
                        }
                    }
                }
            }

            Assert.That(obsCount, Is.GreaterThan(0));
        }

        [TestCase(@"tests\Data\ESTAT_NA_MAIN_1_0-Range.xml", @"tests\v21\Structure\ESTAT+NA_MAIN+1.0.xml")]
        public void ShouldReadSdmx21DataWithTimeRange(string file, string dsd)
        {
            var retrievalManager = new InMemoryRetrievalManager();
            retrievalManager.SaveStructures(new FileInfo(dsd).GetSdmxObjects(this._structureParsingManager));
            int obsCount = 0;

            using (var sourceData = this._factory.GetReadableDataLocation(new FileInfo(file)))
            using (var dataReaderEngine = this._dataReaderManager.GetDataReaderEngine(sourceData, retrievalManager))
            {
                while (dataReaderEngine.MoveNextKeyable())
                {
                    var currentKey = dataReaderEngine.CurrentKey;
                    if (currentKey.Series)
                    {
                        while (dataReaderEngine.MoveNextObservation())
                        {
                            var obs = dataReaderEngine.CurrentObservation;
                            Assert.That(obs.ObsTimeFormat, Is.EqualTo(TimeFormat.TimeRange));
                            obsCount++;
                        }
                    }
                }
            }

            Assert.That(obsCount, Is.GreaterThan(0));
        }
    }
}