// -----------------------------------------------------------------------
// <copyright file="TestDataWriters.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxDataParserTests.
//     SdmxDataParserTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxDataParserTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParserTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using Newtonsoft.Json;
using NSubstitute;
using Org.Sdmx.Resources.SdmxMl.Schemas.V10.Common;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.DataParser.Engine.JsonSupport;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Codelist;
using Org.Sdmxsource.Sdmx.Util.Date;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

namespace SdmxDataParserTests
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;

    using Newtonsoft.Json.Linq;
    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Sdmx.DataParser.Factory;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.SuperObjects;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Translator;
    using Org.Sdmxsource.Util.Io;

    using DataType = Org.Sdmxsource.Sdmx.Api.Constants.DataType;

    /// <summary>
    ///     Test unit for SDMX Data Writers
    /// </summary>
    [TestFixture]
    public class TestJsonV20DataWriters
    {
        [TestCase("tests/v21/264D_264_SALDI2_ATTRIBUTE_TEST.xml", "tests/Data/test_264D_264_SALDI2_2-generic_output.series.xml", DatasetStructureReference.AllDimensions)]
        [TestCase("tests/v21/264D_264_SALDI2_ATTRIBUTE_TEST.xml", "tests/Data/test_264D_264_SALDI2_2-generic_output.series.xml", null)]
        public void TestGroupAttributes(string dsdFile, string dataFile, string dimensionAtObservation)
        {
            var dataType = DataType.GetFromEnum(DataEnumType.Json20);
            var sdmxObjects = GetSdmxObjects(dsdFile);
            var dsd = sdmxObjects.DataStructures.First();

            var fileName = dataFile.Replace(".xml", ".json");
            if (dimensionAtObservation == DatasetStructureReference.AllDimensions)
            {
                fileName = fileName.Replace(".series.", ".flat.");
            }

            var preferedLanguageTranslator = new BestMatchLanguageTranslator(
                new List<CultureInfo>() {CultureInfo.GetCultureInfo("it-IT"), CultureInfo.GetCultureInfo("fr"), CultureInfo.GetCultureInfo("en-US")},
                sdmxObjects,
                CultureInfo.GetCultureInfo("en"));

            var sdmxJsonFormat = new SdmxJsonDataFormat(
                new InMemorySdmxSuperObjectRetrievalManager(new SuperObjectsBuilder(), sdmxObjects),
                dataType,
                preferedLanguageTranslator,
                new InMemoryRetrievalManager(sdmxObjects)
            );

            var sdmxDataReaderFactory = new SdmxDataReaderFactory(new DataInformationManager(new FixedConceptEngine(), new ReportedDateEngine()), null);
            var factory = new ReadableDataLocationFactory();
            var dataWriterManager = new DataWriterManager(new SdmxJsonDataWriterFactory()
            {
                UseHierarchicalCodelists = true
            });

            using (Stream writer = File.Create(fileName))
            using (IDataWriterEngine dataWriter = dataWriterManager.GetDataWriterEngine(sdmxJsonFormat, writer))
            {
                // write the same dataset twice to test support of multiple structures in json ver 2.0
                for (var i = 0; i < 2; i++)
                {
                    using (var sourceData = factory.GetReadableDataLocation(new FileInfo(dataFile)))
                    using (var dataReaderEngine = sdmxDataReaderFactory.GetDataReaderEngine(sourceData, dsd, null))
                    {
                        if (i == 0)
                        {
                            dataWriter.WriteHeader(dataReaderEngine.Header);
                        }

                        if (string.IsNullOrEmpty(dimensionAtObservation))
                        {
                            dimensionAtObservation = dataReaderEngine.Header.Structures.First().DimensionAtObservation;
                        }

                        var dsRef = new DatasetStructureReferenceCore("Test", dsd.AsReference, null, null, dimensionAtObservation); //"TIME_PERIOD"
                        var datasetHeader = new DatasetHeaderCore("DS12345", DatasetActionEnumType.Information, dsRef);

                        dataWriter.StartDataset(null, dsd, datasetHeader, null);

                        //--
                        if (dataReaderEngine.MoveNextDataset())
                        {
                            foreach (var att in dataReaderEngine.DatasetAttributes)
                            {
                                dataWriter.WriteAttributeValue(att.Concept, att.Code);
                            }

                            while (dataReaderEngine.MoveNextKeyable())
                            {
                                var currentKey = dataReaderEngine.CurrentKey;

                                if (currentKey.Series)
                                {
                                    dataWriter.StartSeries(currentKey.Annotations.ToArray());

                                    foreach (var kv in currentKey.Key)
                                    {
                                        dataWriter.WriteSeriesKeyValue(kv.Concept, kv.Code);
                                    }

                                    foreach (var att in currentKey.Attributes)
                                    {
                                        dataWriter.WriteAttributeValue(att.Concept, att.Code);
                                    }

                                    while (dataReaderEngine.MoveNextObservation())
                                    {
                                        IObservation currentObservation = dataReaderEngine.CurrentObservation;

                                        dataWriter.WriteObservation(DimensionObject.TimeDimensionFixedId, currentObservation.ObsTime, currentObservation.ObservationValue);

                                        foreach (var att in currentObservation.Attributes)
                                        {
                                            dataWriter.WriteAttributeValue(att.Concept, att.Code);
                                        }
                                    }
                                }
                                else
                                {
                                    var arr = new List<IAnnotation>()
                                    {
                                        new AnnotationObjectCore(
                                            new AnnotationType()
                                            {
                                                AnnotationTitle = "Test",
                                                AnnotationText = new List<Org.Sdmx.Resources.SdmxMl.Schemas.V10.Common.TextType>()
                                                {
                                                    new Org.Sdmx.Resources.SdmxMl.Schemas.V10.Common.TextType() {lang = "en", TypedValue = "some value here"}
                                                },
                                                AnnotationType1 = "Some"
                                            },
                                            currentKey.DataStructure)
                                    };

                                    dataWriter.StartGroup(currentKey.GroupName, arr.ToArray()); //currentKey.Annotations.ToArray()

                                    foreach (var kv in currentKey.Key)
                                    {
                                        dataWriter.WriteGroupKeyValue(kv.Concept, kv.Code);
                                    }

                                    foreach (var att in currentKey.Attributes)
                                    {
                                        dataWriter.WriteAttributeValue(att.Concept, att.Code);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            JObject result = JObject.Parse(File.ReadAllText(fileName), new JsonLoadSettings() {LineInfoHandling = LineInfoHandling.Ignore});

            VerifyJsonResult(result);
        }

        [TestCase("tests/v21/CsvV2.xml", "tests/CsvV2.json")]
        public void TestMetaAttributes(string sdmxStructureFile, string outputFile)
        {
            var dataType = DataType.GetFromEnum(DataEnumType.Json20);
            var sdmxObjects = GetSdmxObjects(sdmxStructureFile);
            var dsd = sdmxObjects.DataStructures.First();

            var preferedLanguageTranslator = new BestMatchLanguageTranslator(
                new List<CultureInfo>() { CultureInfo.GetCultureInfo("it-IT"), CultureInfo.GetCultureInfo("fr"), CultureInfo.GetCultureInfo("en-US") },
                sdmxObjects,
                CultureInfo.GetCultureInfo("en"));

            var sdmxObjectRetriever = new InMemoryRetrievalManager(sdmxObjects);

            var sdmxJsonFormat = new SdmxJsonDataFormat(
                new InMemorySdmxSuperObjectRetrievalManager(new SuperObjectsBuilder(), sdmxObjects),
                dataType,
                preferedLanguageTranslator,
                sdmxObjectRetriever
            );

            using (Stream writer = File.Create(outputFile))
            using (IDataWriterEngine dataWriter = new DataWriterManager(new SdmxJsonDataWriterFactory()).GetDataWriterEngine(sdmxJsonFormat, writer))
            {
                var dsRef = new DatasetStructureReferenceCore("Test", dsd.AsReference, null, null, "TIME_PERIOD");
                var datasetHeader = new DatasetHeaderCore("DS12345", DatasetActionEnumType.Information, dsRef);

                dataWriter.StartDataset(null, dsd, datasetHeader, null);

                // metadata -------------------------------------------

                var jsonValue = "en:\"<a href=\\\"mailto:STAT.Contact@oecd.org\\\">Balazs.Test@oecd.org</a>\",fr: \"<a href=\\\"mailto:STAT.Contact@oecd.org\\\">STAT.Contact@oecd.org</a>\"";

                dataWriter.StartGroup("meta-wildcard");
                dataWriter.WriteAttributeValue("STRING_MULTILANG_TYPE", jsonValue);

                dataWriter.StartGroup("meta-partial");
                dataWriter.WriteGroupKeyValue("FREQ", "M");
                dataWriter.WriteAttributeValue("STRING_MULTILANG_TYPE", jsonValue);

                dataWriter.StartGroup("meta-partial");
                dataWriter.WriteGroupKeyValue("FREQ", "A");
                dataWriter.WriteSeriesKeyValue("SERIES", "SI_POV_EMP1");
                dataWriter.WriteSeriesKeyValue("REF_AREA", "234");
                dataWriter.WriteAttributeValue("STRING_MULTILANG_TYPE", jsonValue);

                dataWriter.StartGroup("meta-full-key");
                dataWriter.WriteGroupKeyValue("FREQ", "M");
                dataWriter.WriteGroupKeyValue("REPORTING_TYPE", "N");
                dataWriter.WriteGroupKeyValue("SERIES", "SI_POV_DAY1");
                dataWriter.WriteGroupKeyValue("REF_AREA", "233");
                dataWriter.WriteGroupKeyValue("SEX", "M");
                dataWriter.WriteAttributeValue("STRING_MULTILANG_TYPE", jsonValue);
                dataWriter.WriteAttributeValue("CONTACT.CONTACT_NAME", "John Doe");
                dataWriter.WriteAttributeValue("CONTACT.CONTACT_EMAIL", "john.doe@hotmail.com");
                dataWriter.WriteAttributeValue("CONTACT.CONTACT_PHONE", "(1)2334-114-444");

                // data ---------------------------------------

                dataWriter.StartSeries();
                dataWriter.WriteSeriesKeyValue("FREQ", "A");
                dataWriter.WriteSeriesKeyValue("REPORTING_TYPE", "N");
                dataWriter.WriteSeriesKeyValue("SERIES", "SI_POV_DAY1");
                dataWriter.WriteSeriesKeyValue("REF_AREA", "233");
                dataWriter.WriteSeriesKeyValue("SEX", "M");

                dataWriter.WriteObservation("TIME_PERIOD", "2022", "1");
                dataWriter.WriteAttributeValue("OBS_STATUS", "Z");
                dataWriter.WriteAttributeValue("UNIT_MULT", "5");
                dataWriter.WriteAttributeValue("UNIT_MEASURE", "IX");
                dataWriter.WriteAttributeValue("NATURE", "E");
                dataWriter.WriteAttributeValue("COMMENT_OBS", "Some comment here");

                dataWriter.WriteObservation("TIME_PERIOD", "2023", "2");
                dataWriter.WriteObservation("TIME_PERIOD", "2024", "3");
                dataWriter.WriteObservation("TIME_PERIOD", "2025", "4");

            }
        }

        [TestCase("tests/v21/TEST,HCL_TEST,1.0.xml|tests/v21/TEST,DF_AREA,1.0.xml", "DF_AREA_LOCALISED_HIERARCHY", true, 5)]
        [TestCase("tests/v21/TEST,HCL_TEST,1.0.xml|tests/v21/TEST,DF_AREA,1.0.xml", "DF_AREA_LOCALISED_HIERARCHY", false, 1)]
        public void TestHierarchicalCodelist(string pathToStructures, string dfId, bool useHcl, int expectedCodeCount)
        {
            var sdmxObjects = GetSdmxObjects(pathToStructures);

            var objectRetrievalManager = new InMemoryRetrievalManager(sdmxObjects);
            var dataflow = sdmxObjects.Dataflows.First(x => x.Id == dfId);
            var dsd = objectRetrievalManager.GetMaintainableObject<IDataStructureObject>(dataflow.DataStructureRef);

            Assert.IsNotNull(dataflow);
            Assert.IsNotNull(dsd);

            // -----------------------

            var preferedLanguageTranslator = new BestMatchLanguageTranslator(
                new List<CultureInfo>() { CultureInfo.GetCultureInfo("it-IT"), CultureInfo.GetCultureInfo("fr"), CultureInfo.GetCultureInfo("en-US") },
                sdmxObjects,
                CultureInfo.GetCultureInfo("en"));

            var sdmxJsonFormat = new SdmxJsonDataFormat(
                new InMemorySdmxSuperObjectRetrievalManager(new SuperObjectsBuilder(), sdmxObjects),
                DataType.GetFromEnum(DataEnumType.Json20),
                preferedLanguageTranslator,
                objectRetrievalManager
            );

            var dataWriterManager = new DataWriterManager(new SdmxJsonDataWriterFactory()
            {
                UseHierarchicalCodelists = useHcl
            });
            // -----------------------

            var outputFile = "tests/v21/hcl_tests.json";

            using (Stream writer = File.Create(outputFile))
            using (IDataWriterEngine dataWriter = dataWriterManager.GetDataWriterEngine(sdmxJsonFormat, writer))
            {
                var dimensionAtObservation = DatasetStructureReference.AllDimensions;
                var dsRef = new DatasetStructureReferenceCore("Test", dsd.AsReference, null, null, dimensionAtObservation); //"TIME_PERIOD"
                var datasetHeader = new DatasetHeaderCore("DS12345", DatasetActionEnumType.Information, dsRef);

                dataWriter.StartDataset(dataflow, dsd, datasetHeader, null);

                // series
                dataWriter.StartSeries();
                dataWriter.WriteSeriesKeyValue("REF_AREA", "USA");
                dataWriter.WriteSeriesKeyValue("MEASURE", "M1");
                dataWriter.WriteSeriesKeyValue("FREQ", "A");

                // observations
                dataWriter.WriteObservation(DimensionObject.TimeDimensionFixedId, "2021", "1");
                dataWriter.WriteObservation(DimensionObject.TimeDimensionFixedId, "2022", "2");
                dataWriter.WriteObservation(DimensionObject.TimeDimensionFixedId, "2023", "3");
            }

            var jObject = JObject.Parse(File.ReadAllText(outputFile), new JsonLoadSettings() { LineInfoHandling = LineInfoHandling.Ignore });

            Assert.IsNotNull(jObject);

            var refArea = jObject.SelectToken("data.structures[0].dimensions.observation[0]");

            Assert.IsNotNull(refArea);

            Assert.AreEqual("REF_AREA", refArea["id"]?.Value<string>());
            Assert.AreEqual(expectedCodeCount, ((JArray) refArea["values"])?.Count);
            Assert.AreEqual("USA", ((JArray)refArea["values"])[0]["id"]?.Value<string>());
            Assert.AreEqual(useHcl, ((JArray)refArea["values"])[0]["parents"] != null);
        }

        [TestCase("tests/v21/TEST,HCL_TEST,1.0.xml", "CONT_EN", "USA", 2, "USA/A2/A/W")]
        [TestCase("tests/v21/TEST,HCL_TEST,1.0.xml", "CONT_EN", "FRA", 2, "FRA/OECD")]
        [TestCase("tests/v21/TEST,HCL_TEST,1.0.xml", "CONT_FR", "USA", 2, "USA/A2/A/W")]
        [TestCase("tests/v21/TEST,HCL_TEST,1.0.xml", "CONT_FR", "GBR", 1, "GBR/OECD")]
        public void TestHclRetrieval(string hclStructurePath, string hierarchy, string code, int parentCount, string longestPath)
        {
            var sdmxObjects = GetSdmxObjects(hclStructurePath);
            var hcl = sdmxObjects.HierarchicalCodelists.First(x=>x.Id == "HCL_TEST");
            var dict = HclSupport.RetrieveHclCodes(hcl, hierarchy);

            Assert.IsNotNull(dict);
            Assert.IsTrue(dict.TryGetValue(code, out var hclCode));
            Assert.AreEqual(parentCount, hclCode.Parents.Count);

            var (height, path) = HclSupport.MaxHeight(dict, code);

            Assert.AreEqual(longestPath, path);
        }

        [TestCase("tests/v21/TEST,DF_AREA,1.0.xml", "CL_DEG_URB", "URB", 0)]
        [TestCase("tests/v21/TEST,DF_AREA,1.0.xml", "CL_DEG_URB", "TSUB", 1)]
        [TestCase("tests/v21/TEST,DF_AREA,1.0.xml", "CL_DEG_URB", "DTOW", 1)]
        public void TestCodelistParentsRetrieval(string dsdStructurePath, string codelistId, string codeId, int parentAmount)
        {
            var dsdSdmxObjects = GetSdmxObjects(dsdStructurePath);
            var codelist = dsdSdmxObjects.Codelists.First(x => x.Id == codelistId);
            var dict = HclSupport.RetrieveCodes(new CodelistSuperObject(codelist));

            Assert.IsNotNull(dict);
            Assert.IsTrue(dict.TryGetValue(codeId, out var hclCode));
            Assert.AreEqual(parentAmount, hclCode.Parents.Count);
        }

        [TestCase("tests/v21/TEST,HCL_TEST,1.0.xml", "tests/v21/TEST,DF_AREA,1.0.xml", "DF_AREA_LOCALISED_HIERARCHY", "en", 1)]
        [TestCase("tests/v21/TEST,HCL_TEST,1.0.xml", "tests/v21/TEST,DF_AREA,1.0.xml", "DF_AREA_LOCALISED_HIERARCHY", "ee", 0)]
        [TestCase("tests/v21/TEST,HCL_TEST,1.0.xml", "tests/v21/TEST,DF_AREA,1.0.xml", "DF_AREA_NONLOCALISED_HIERARCHY", "en", 2)]
        [TestCase("tests/v21/TEST,HCL_TEST,1.0.xml", "tests/v21/TEST,DF_AREA,1.0.xml", "DF_AREA_NONLOCALISED_HIERARCHY", "lv", 2)]
        [TestCase("tests/v21/TEST,HCL_TEST,1.0.xml", "tests/v21/TEST,DF_AREA,1.0.xml", "DF_AREA_DSD_LOCALISED_HIERARCHY", "en", 1)]
        [TestCase("tests/v21/TEST,HCL_TEST,1.0.xml", "tests/v21/TEST,DF_AREA,1.0.xml", "DF_AREA_DSD_LOCALISED_HIERARCHY", "hu", 0)]
        [TestCase("tests/v21/TEST,HCL_TEST,1.0.xml", "tests/v21/TEST,DF_AREA,1.0.xml", "DF_AREA_DSD_NONLOCALISED_HIERARCHY", "en", 1)]
        [TestCase("tests/v21/TEST,HCL_TEST,1.0.xml", "tests/v21/TEST,DF_AREA,1.0.xml", "DF_AREA_DSD_NONLOCALISED_HIERARCHY", "hu", 1)]
        [TestCase("tests/v21/TEST,HCL_TEST,1.0.xml", "tests/v21/TEST,DF_AREA,1.0.xml", "DF_AREA_NO_HIERARCHY", "en", 0)]
        [TestCase("tests/v21/TEST,HCL_TEST,1.0.xml", "tests/v21/TEST,DF_AREA,1.0.xml", "DF_AREA_HYBRID", "ee", 1)]
        [TestCase("tests/v21/TEST,HCL_TEST,1.0.xml", "tests/v21/TEST,DF_AREA,1.0.xml", "DF_AREA_HYBRID", "fr", 1)]
        [TestCase("tests/v21/TEST,HCL_TEST,1.0.xml", "tests/v21/TEST,DF_AREA,1.0.xml", "DF_AREA_HYBRID", null, 2)]
        [TestCase("tests/v21/TEST,HCL_TEST,1.0.xml", "tests/v21/TEST,DF_AREA,1.0.xml", "DF_AREA_HYBRID", "en", 2)]
        public void AnnotationReadTests(string hclStructurePath, string dsdStructurePath, string dfId, string language, int expectedCount)
        {
            var hclSdmxObjects = GetSdmxObjects(hclStructurePath);
            var dsdSdmxObjects = GetSdmxObjects(dsdStructurePath);

            dsdSdmxObjects.Merge(hclSdmxObjects);

            var objectRetrievalManager = new InMemoryRetrievalManager(dsdSdmxObjects);

            var dataflow = dsdSdmxObjects.Dataflows.First(x => x.Id == dfId);
            var dsd = objectRetrievalManager.GetMaintainableObject<IDataStructureObject>(dataflow.DataStructureRef);

            Assert.IsNotNull(dataflow);
            Assert.IsNotNull(dsd);

            var dict = HclSupport.RetrieveHclCodes(dataflow, dsd, objectRetrievalManager, language);

            Assert.AreEqual(expectedCount, dict.Count);

            if (expectedCount > 0)
            {
                Assert.IsTrue(dict.ContainsKey("REF_AREA"));
                Assert.AreEqual("USA/A2/A/W", HclSupport.MaxHeight(dict["REF_AREA"], "USA").path);
            }
        }

        [TestCase("REF_AREA:TEST:HCL_TEST(1.0).CONT_EN", true)]
        [TestCase("REF_AREA:TEST/HCL_TEST(1.0).CONT_EN", false)]
        public void HclReferenceParseTest(string reference, bool isValid)
        {
            var hclRef = HclSupport.ParseHclPath(reference);

            Assert.AreEqual(isValid, hclRef != null);
        }
        

        [Test]
        public void ParseMetaJson()
        {
            var json = "{en: \"<a href=\\\"mailto:STAT.Contact@oecd.org\\\">STAT.Contact@oecd.org</a>\", fr: \"<a href=\\\"mailto:STAT.Contact@oecd.org\\\">STAT.Contact@oecd.org</a>\"}";
            var token = JToken.Parse(json);

            Assert.IsNotNull(token);
        }

        /// <summary>
        /// Gets the SDMX objects.
        /// </summary>
        /// <returns>The <see cref="ISdmxObjects"/>.</returns>
        private static ISdmxObjects GetSdmxObjects(string path)
        {
            ISdmxObjects objects = new SdmxObjectsImpl();
            
            foreach (var filename in path.Split('|'))
            {
                IStructureParsingManager manager = new StructureParsingManager(SdmxSchemaEnumType.Null);
                using (var readable = new FileReadableDataLocation(new FileInfo(filename)))
                {
                    IStructureWorkspace structureWorkspace = manager.ParseStructures(readable);
                    objects.Merge(structureWorkspace.GetStructureObjects(false));
                }
            }
            
            return objects;
        }

        private static void VerifyJsonResult(JObject jObject)
        {
            Assert.IsNotNull(jObject.SelectToken("meta.id"));
            Assert.IsNotNull(jObject.SelectToken("meta.schema"));
            Assert.IsNotNull(jObject.SelectToken("meta.prepared").Value<DateTime>());
            Assert.AreEqual(false, jObject.SelectToken("meta.test").Value<bool>());

            VerifyJObject("\"ESTAT\"", jObject, "meta.sender.id");
        }

        private static void VerifyJObject(string expected, JObject jObject, string path)
        {
            Assert.IsNotNull(jObject.SelectToken(path));
            Assert.AreEqual(expected, jObject.SelectToken(path).ToString(Newtonsoft.Json.Formatting.None));
        }
    }
}