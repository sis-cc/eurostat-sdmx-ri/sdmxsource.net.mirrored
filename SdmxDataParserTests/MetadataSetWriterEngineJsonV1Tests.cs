using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
using Org.Sdmx.Resources.SdmxMl.Schemas.V21.MetaData.Generic;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.Api.Factory;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Model.Metadata;
using Org.Sdmxsource.Sdmx.DataParser.Engine;
using Org.Sdmxsource.Sdmx.DataParser.Factory;
using Org.Sdmxsource.Sdmx.DataParser.Manager;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Metadata;
using Org.Sdmxsource.Util.Io;

namespace SdmxDataParserTests
{
    /// <summary>
    /// Test unit for MetadataSetWriterEngineJsonV1 <see cref="SdmxException"/>
    /// </summary>
    [TestFixture]
    public class MetadataSetWriterEngineJsonV1Tests
    {
        [Test]
        public void TestMetadataSetReportingDates()
        {
            var metadataSet = new Org.Sdmx.Resources.SdmxMl.Schemas.V21.MetaData.Generic.MetadataSetType();

            metadataSet.reportingBeginDate = new DateTime(2020,02,01);

            metadataSet.reportingEndDate = "2020-02-01";
            metadataSet.publicationPeriod = "Q1 2005";
            metadataSet.structureRef = "MSD";
            metadataSet.PublicationYearString = "2004";
            ReportType report = new ReportType();
            report.id = "Test_report";
            report.Target = new Org.Sdmx.Resources.SdmxMl.Schemas.V21.MetaData.Generic.TargetType();
            report.Target.id = "TEST_TARGET";
            ReferenceValueType referenceType = new ReferenceValueType();
            referenceType.id = "REFERNCE_TYPE";
            referenceType.ObjectReference = new ObjectReferenceType();
            referenceType.ObjectReference.URN = new[] { new Uri("urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS(2.0)")  };
            report.Target.ReferenceValue.Add(referenceType);
            ReportedAttributeType attribute = new ReportedAttributeType();
            attribute.id = "TEST_ATTR";
            attribute.value = "123";
            report.AttributeSet = new AttributeSetType();
            report.AttributeSet.ReportedAttribute.Add(attribute);
            metadataSet.Report.Add(report);
            GenericMetadata metadata = new GenericMetadata();
            metadata.Content.Header = new GenericMetadataHeaderType();
            metadata.Content.Header.ID = "IREF0001";
            metadata.Content.Header.Test = true;
            metadata.Content.Header.Prepared = "2020-11-01T00:00:00.000";

            var structureType = new GenericMetadataStructureType();
            var structure = new MetadataStructureReferenceType();
            var refType = new MetadataStructureRefType();

            var substr = "ESTAT:MSD(1.0)";
            refType.agencyID = substr.Split(':')[0];
            refType.id = substr.Split(':')[1].Split('(')[0];
            refType.version = substr.Split('(')[1].Split(')')[0];
            structure.SetTypedRef(refType);
            structureType.Structure = structure;
            //            structureType.structureID = $"urn:sdmx:org.sdmx.infomodel.metadatastructure.MetadataStructure={refType.agencyID}:{refType.id}({refType.version})";
            structureType.structureID = metadataSet.structureRef;
            metadata.Content.Header.Structure.Add(structureType);

            metadata.Content.DataSet.Add(metadataSet);

            IMetadata metadataObject = new MetadataObjectCore(metadata);
            Assert.That(metadataObject.MetadataSet[0].ReportingBeginDate, Is.Not.Null);
            Assert.That(metadataObject.MetadataSet[0].ReportingEndDate, Is.Not.Null);
            Assert.That(metadataObject.MetadataSet[0].PublicationYear, Is.Not.Null);
            Assert.That(metadataObject.MetadataSet[0].PublicationYear.DateInSdmxFormat, Is.EqualTo("2004"));
        }

        [Test]
        public void TestSerializingMetadataSet()
        {
            #region Create Metdataset

            GenericMetadata metadata = new GenericMetadata();
            var header = new GenericMetadataHeaderType();
            var structureType = new GenericMetadataStructureType();
            var structure = new MetadataStructureReferenceType();
            var refType = new MetadataStructureRefType();
            refType.agencyID = "ESTAT";
            refType.id = "NATACC_MSD";
            refType.version = "1.5";
            structure.SetTypedRef(refType);
            structureType.Structure = structure;
            structureType.structureID = "urn:sdmx:org.sdmx.infomodel.metadatastructure.MetadataStructure=ESTAT:NATACC_MSD(1.5)";
            header.Structure.Add(structureType);
            metadata.Content.Header = header;

            // TODO add DataSet to metadata
            var newDataSet = new Org.Sdmx.Resources.SdmxMl.Schemas.V21.MetaData.Generic.MetadataSetType();

            newDataSet.action = "Replace";
            newDataSet.setID = "TEST_PRES";

            newDataSet.publicationPeriod = "20200201-20201201";
            //newDataSet.publicationYear
            newDataSet.reportingBeginDate = "2020-02-01";
            newDataSet.reportingEndDate = "2020-02-01";
            newDataSet.validFromDate = new System.DateTime(2020, 2, 1);
            newDataSet.validToDate = new System.DateTime(2020, 12, 31);

            newDataSet.Name.Add(new Name() { lang = "it", TypedValue = "test isPresentational" });


            newDataSet.structureRef = "urn:sdmx:org.sdmx.infomodel.metadatastructure.MetadataStructure=ESTAT:NATACC_MSD(1.5)";


            newDataSet.Annotations = new Annotations();
            newDataSet.Annotations.Annotation = new List<AnnotationType>();
            var mainItemAnnotationType = new AnnotationType();
            mainItemAnnotationType.id = "TEstAnnotationCreation";
            mainItemAnnotationType.AnnotationText.Add(new Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common.TextType { TypedValue = "aaaTEst test", lang = "en" });
            newDataSet.Annotations.Annotation.Add(mainItemAnnotationType);

            var newReport = new Org.Sdmx.Resources.SdmxMl.Schemas.V21.MetaData.Generic.ReportType();
            newReport.id = "A";

            //START Annotation
            var itemReportedAttributeType = new Org.Sdmx.Resources.SdmxMl.Schemas.V21.MetaData.Generic.ReportedAttributeType();
            itemReportedAttributeType.Annotations = new Annotations();

            itemReportedAttributeType.Annotations.Annotation = new List<AnnotationType>();

            var itemAnnotationType = new AnnotationType();
            itemAnnotationType.id = "ReportId";
            itemAnnotationType.AnnotationType1 = "testforType";
            itemAnnotationType.AnnotationText.Add(new Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common.TextType { TypedValue = "18", lang = "en" });
            itemReportedAttributeType.Annotations.Annotation.Add(itemAnnotationType);

            itemAnnotationType = new AnnotationType();
            itemAnnotationType.id = "MetadataSetId";
            itemAnnotationType.AnnotationText.Add(new Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common.TextType { TypedValue = "6", lang = "en" });
            itemReportedAttributeType.Annotations.Annotation.Add(itemAnnotationType);

            itemAnnotationType = new AnnotationType();
            itemAnnotationType.id = "ReportStateId";
            itemAnnotationType.AnnotationText.Add(new Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common.TextType { TypedValue = "4", lang = "en" });
            itemReportedAttributeType.Annotations.Annotation.Add(itemAnnotationType);

            itemAnnotationType = new AnnotationType();
            itemAnnotationType.id = "ReportStateName";
            itemAnnotationType.AnnotationText.Add(new Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common.TextType { TypedValue = "DRAFT", lang = "en" });
            itemReportedAttributeType.Annotations.Annotation.Add(itemAnnotationType);

            newReport.Annotations = itemReportedAttributeType.Annotations;
            //END Annotation

            //START Target

            var mainTarget = new Org.Sdmx.Resources.SdmxMl.Schemas.V21.MetaData.Generic.TargetType();
            mainTarget.id = "NATACC";
            newReport.Target = mainTarget;
            var listReferenceValueType = new List<Org.Sdmx.Resources.SdmxMl.Schemas.V21.MetaData.Generic.ReferenceValueType>();

            var itemReferenceValueType = new Org.Sdmx.Resources.SdmxMl.Schemas.V21.MetaData.Generic.ReferenceValueType();
            itemReferenceValueType.id = "DATA_PROVIDER";
            var itemObjectReferenceType = new ObjectReferenceType();
            itemObjectReferenceType.URN = new List<Uri>();
            itemObjectReferenceType.URN.Add(new System.Uri("urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ECB:ECB_EXR1(1.0)"));
            itemReferenceValueType.ObjectReference = itemObjectReferenceType;
            listReferenceValueType.Add(itemReferenceValueType);

            itemReferenceValueType = new Org.Sdmx.Resources.SdmxMl.Schemas.V21.MetaData.Generic.ReferenceValueType();
            itemReferenceValueType.id = "TIME_PERIOD";
            itemObjectReferenceType = new ObjectReferenceType();
            itemObjectReferenceType.URN = new List<Uri>();
            itemObjectReferenceType.URN.Add(new System.Uri("urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ECB:ECB_EXR1(1.0)"));
            itemReferenceValueType.ObjectReference = itemObjectReferenceType;
            listReferenceValueType.Add(itemReferenceValueType);

            itemReferenceValueType = new Org.Sdmx.Resources.SdmxMl.Schemas.V21.MetaData.Generic.ReferenceValueType();
            itemReferenceValueType.id = "DATAFLOW";
            itemObjectReferenceType = new ObjectReferenceType();
            itemObjectReferenceType.URN = new List<Uri>();
            itemObjectReferenceType.URN.Add(new Uri("urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=IT1:AAA(1.0)"));
            itemReferenceValueType.ObjectReference = itemObjectReferenceType;
            listReferenceValueType.Add(itemReferenceValueType);

            mainTarget.ReferenceValue = listReferenceValueType;
            //END Target


            //START AttributeSet
            var itemAttributeSetType = new Org.Sdmx.Resources.SdmxMl.Schemas.V21.MetaData.Generic.AttributeSetType();
            itemReportedAttributeType = new Org.Sdmx.Resources.SdmxMl.Schemas.V21.MetaData.Generic.ReportedAttributeType();
            itemReportedAttributeType.id = "COMMENT_DSET";
            itemReportedAttributeType.value = "348";
            itemAttributeSetType.ReportedAttribute.Add(itemReportedAttributeType);
            itemAttributeSetType = new Org.Sdmx.Resources.SdmxMl.Schemas.V21.MetaData.Generic.AttributeSetType();
            itemReportedAttributeType = new Org.Sdmx.Resources.SdmxMl.Schemas.V21.MetaData.Generic.ReportedAttributeType();
            itemReportedAttributeType.id = "COMMENT_DSET";
            itemReportedAttributeType.value = "349";

            itemAnnotationType = new AnnotationType();
            itemAnnotationType.id = "ReportStateName";
            itemAnnotationType.AnnotationText.Add(new Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common.TextType { TypedValue = "testprova", lang = "en" });
            itemAnnotationType.AnnotationText.Add(new Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common.TextType { TypedValue = "testprova ITA", lang = "it" });
            itemReportedAttributeType.Annotations = new Annotations();
            itemReportedAttributeType.Annotations.Annotation = new List<AnnotationType>();
            itemReportedAttributeType.Annotations.Annotation.Add(itemAnnotationType);

            itemAttributeSetType.ReportedAttribute.Add(itemReportedAttributeType);
            newReport.AttributeSet = itemAttributeSetType;
            //END AttributeSet

            //var newDataProvider = new Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common.DataProviderReferenceType();
            //newDataSet.DataProvider = newDataProvider;

            newDataSet.Report.Add(newReport);
            metadata.Content.DataSet.Add(newDataSet);

            IMetadata metadataObject = new MetadataObjectCore(metadata);

            #endregion



            var format = new SdmxJsonMetadataFormat();

            //Serializer
            var stream = new MemoryStream();
            IMetadataWriterEngine swe = new MetadataSetWriterEngineJsonV1(stream, format);
            swe.WriteMetadataSet(null, metadataObject.MetadataSet.ToArray());
            
            var strJson = Encoding.UTF8.GetString(stream.ToArray());


            //Deserializer
            Encoding encoding = Encoding.UTF8;
            MemoryReadableLocation mrl = new MemoryReadableLocation(encoding.GetBytes(strJson));


            var sdmxJsonMetadatasetsParserFactory = new List<IMetadataParsingFactory> { new SdmxJsonMetadatasetsParserFactory() };
            IMetadatasetsParsingManager metaParsing = new MetadatasetsParsingJsonManager(sdmxJsonMetadatasetsParserFactory);
            IMetadata metadataObjectParsed = metaParsing.BuildMetadataset(mrl, format);


            //Serializer
            stream = new MemoryStream();
            swe = new MetadataSetWriterEngineJsonV1(stream, format);
            swe.WriteMetadataSet(null, metadataObject.MetadataSet.ToArray());

            var strJson2 = Encoding.UTF8.GetString(stream.ToArray());


            Assert.AreEqual(NormalizeJson(strJson), NormalizeJson(strJson2));
        }

        private string NormalizeJson(string json)
        {
            json = json.Replace("{\"meta\":{\"schema\":\"https://raw.githubusercontent.com/sdmx-twg/sdmx-json/develop/metadata-message/tools/schemas/1.0/sdmx-json-metadata-schema.json\",\"id\":", "");
            return json.Substring(45);
        }
    }
}

