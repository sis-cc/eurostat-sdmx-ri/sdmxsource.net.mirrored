using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Model.Header;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.DataParser.Engine.Csv;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.DataStructure;
using Org.Sdmxsource.Sdmx.Structureparser.Builder.SuperObjects;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Translator;
using Org.Sdmxsource.Util.Io;

namespace SdmxDataParserTests
{
    [TestFixture]
    public class CSVWriterTests
    {
        
        private CsvDataWriterEngine _csvWriter;
        private IDataStructureObject _dataStructure;
        private IDatasetHeader _header;
        private IDataflowObject _dataflow;
        private Stream _outputStream;

        public CSVWriterTests()
        {
            var file = new FileInfo("tests/DSD_with_groups.xml");
            ISdmxObjects objects = null;
            IStructureParsingManager manager = new StructureParsingManager(SdmxSchemaEnumType.Null);
            using (var readable = new FileReadableDataLocation(file))
            {
                IStructureWorkspace structureWorkspace = manager.ParseStructures(readable);
                objects = structureWorkspace.GetStructureObjects(false);
            }
            _outputStream = new FileStream("Output.csv",FileMode.Create);
            Mock<ISdmxSuperObjectRetrievalManager> superObjectRetrievalManage = new Mock<ISdmxSuperObjectRetrievalManager>(); ;
            Org.Sdmxsource.Translator.ITranslator translator = new PreferedLanguageTranslator(
                new List<CultureInfo>()
                {
                    CultureInfo.GetCultureInfo("it"),
                    CultureInfo.GetCultureInfo("fr"),
                    CultureInfo.GetCultureInfo("en")
                },
                new List<CultureInfo>()
                {
                    CultureInfo.GetCultureInfo("fr"),
                    CultureInfo.GetCultureInfo("en")
                },
                CultureInfo.GetCultureInfo("en"));
            this._dataflow = objects.Dataflows.First();
            this._dataStructure = objects.DataStructures.First();
            var superObjects = new SuperObjectsBuilder().Build(objects);
            var superStructure = superObjects.DataStructures.First();
            superObjectRetrievalManage.Setup(x => x.GetDataStructureSuperObject(It.IsAny<IMaintainableRefObject>())).Returns(superStructure);
            this._csvWriter = new CsvDataWriterEngine(_outputStream, superObjectRetrievalManage.Object, new SdmxCsvOptions(null, null, true), translator);
        }
        [TearDown]
        public void TearDown()
        {
            if (File.Exists("Output.csv"))
            {
                File.Delete("Output.csv");
            }
        }

        [Test]
        public void TestGroups()
        {
            this._csvWriter.StartDataset(this._dataflow,this._dataStructure, null, null);
            this._csvWriter.StartGroup("Siblings", null);
            this._csvWriter.WriteGroupKeyValue("REF_AREA", "IT");
            this._csvWriter.WriteGroupKeyValue("ADJUSTMENT", "W");
            this._csvWriter.WriteGroupKeyValue("STS_INDICATOR", "PROD");
            this._csvWriter.WriteGroupKeyValue("STS_ACTIVITY", "NS0020");
            this._csvWriter.WriteGroupKeyValue("STS_INSTITUTION", "1");
            this._csvWriter.WriteGroupKeyValue("STS_BASE_YEAR", "2000");
            this._csvWriter.WriteAttributeValue("TITLE", "Testing compare contains with slash in it");
            this._csvWriter.WriteAttributeValue("UNIT_MULT", "0");

            this._csvWriter.StartGroup("Siblings", null);
            this._csvWriter.WriteGroupKeyValue("REF_AREA", "IT");
            this._csvWriter.WriteGroupKeyValue("ADJUSTMENT", "W");
            this._csvWriter.WriteGroupKeyValue("STS_INDICATOR", "PROD");
            this._csvWriter.WriteGroupKeyValue("STS_ACTIVITY", "NS0030");
            this._csvWriter.WriteGroupKeyValue("STS_INSTITUTION", "1");
            this._csvWriter.WriteGroupKeyValue("STS_BASE_YEAR", "2000");
            this._csvWriter.WriteAttributeValue("TITLE", "Testing compare contains with star in it");
            this._csvWriter.WriteAttributeValue("UNIT_MULT", "1");

            this._csvWriter.StartGroup("Mitsos", null);
            this._csvWriter.WriteGroupKeyValue("REF_AREA", "IT");
            this._csvWriter.WriteGroupKeyValue("STS_INDICATOR", "PROD");
            this._csvWriter.WriteGroupKeyValue("STS_ACTIVITY", "NS0020");
            this._csvWriter.WriteAttributeValue("UNIT", "PCI");
            this._csvWriter.WriteAttributeValue("DECIMALS", "3");

            this._csvWriter.StartGroup("Mitsos", null);
            this._csvWriter.WriteGroupKeyValue("REF_AREA", "IT");
            this._csvWriter.WriteGroupKeyValue("STS_INDICATOR", "PROD");
            this._csvWriter.WriteGroupKeyValue("STS_ACTIVITY", "NS0030");
            this._csvWriter.WriteAttributeValue("UNIT", "PC");
            this._csvWriter.WriteAttributeValue("DECIMALS", "2");

            this._csvWriter.StartSeries(null);

            this._csvWriter.WriteSeriesKeyValue("FREQ", "M");
            this._csvWriter.WriteSeriesKeyValue("REF_AREA", "IT");
            this._csvWriter.WriteSeriesKeyValue("ADJUSTMENT", "W");
            this._csvWriter.WriteSeriesKeyValue("STS_INDICATOR", "PROD");
            this._csvWriter.WriteSeriesKeyValue("STS_ACTIVITY", "NS0020");
            this._csvWriter.WriteSeriesKeyValue("STS_INSTITUTION", "1");
            this._csvWriter.WriteSeriesKeyValue("STS_BASE_YEAR", "2000");
           
            this._csvWriter.WriteAttributeValue("AVAILABILITY", "B");
            this._csvWriter.WriteAttributeValue("TIME_FORMAT", "P1M");

           
            this._csvWriter.WriteObservation("2005-08", "1.53");
            this._csvWriter.WriteAttributeValue("OBS_STATUS", "A");
            this._csvWriter.WriteAttributeValue("OBS_CONF", "F");

            this._csvWriter.WriteObservation("2005-12", "1.54");
            this._csvWriter.WriteAttributeValue("OBS_STATUS", "A");
            this._csvWriter.WriteAttributeValue("OBS_CONF", "F");

            this._csvWriter.WriteObservation("2005-01", "1.51");
            this._csvWriter.WriteAttributeValue("OBS_STATUS", "A");
            this._csvWriter.WriteAttributeValue("OBS_CONF", "F");

            this._csvWriter.WriteObservation("2005-03", "1.52");
            this._csvWriter.WriteAttributeValue("OBS_STATUS", "A");
            this._csvWriter.WriteAttributeValue("OBS_CONF", "F");

            this._csvWriter.WriteObservation("2006-01", "1.61");
            this._csvWriter.WriteAttributeValue("OBS_STATUS", "A");
            this._csvWriter.WriteAttributeValue("OBS_CONF", "F");
            this._csvWriter.StartSeries(null);

            this._csvWriter.WriteSeriesKeyValue("FREQ", "M");
            this._csvWriter.WriteSeriesKeyValue("REF_AREA", "IT");
            this._csvWriter.WriteSeriesKeyValue("ADJUSTMENT", "W");
            this._csvWriter.WriteSeriesKeyValue("STS_INDICATOR", "PROD");
            this._csvWriter.WriteSeriesKeyValue("STS_ACTIVITY", "NS0030");
            this._csvWriter.WriteSeriesKeyValue("STS_INSTITUTION", "1");
            this._csvWriter.WriteSeriesKeyValue("STS_BASE_YEAR", "2000");

            this._csvWriter.WriteAttributeValue("AVAILABILITY", "B");
            this._csvWriter.WriteAttributeValue("TIME_FORMAT", "P1M");


            this._csvWriter.WriteObservation("2005-04", "1.52");
            this._csvWriter.WriteAttributeValue("OBS_STATUS", "A");
            this._csvWriter.WriteAttributeValue("OBS_CONF", "C");

            this._csvWriter.WriteObservation("2005-02", "1.51");
            this._csvWriter.WriteAttributeValue("OBS_STATUS", "A");
            this._csvWriter.WriteAttributeValue("OBS_CONF", "F");

            this._csvWriter.WriteObservation("2005-05", "1.53");
            this._csvWriter.WriteAttributeValue("OBS_STATUS", "A");
            this._csvWriter.WriteAttributeValue("OBS_CONF", "C");

            this._csvWriter.WriteObservation("2005-10", "1.54");
            this._csvWriter.WriteAttributeValue("OBS_STATUS", "A");
            this._csvWriter.WriteAttributeValue("OBS_CONF", "C");

            this._csvWriter.WriteObservation("2006-02", "1.61");
            this._csvWriter.WriteAttributeValue("OBS_STATUS", "A");
            this._csvWriter.WriteAttributeValue("OBS_CONF", "F");
            
            this._csvWriter.Dispose();
            var lines = new List<string>();
            using (var file = new StreamReader("Output.csv"))
            { 
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    lines.Add(line);
                } 
            }

            for(var i = 1; i < 11; i++)
            {
                if (i < 6)
                { 
                    Assert.True(lines[i].Contains("Testing compare contains with slash in it;PCI;0;3")); 
                }
                else
                {
                    Assert.True(lines[i].Contains("Testing compare contains with star in it;PC;1;2"));
                }
            }
        }
    }
}
