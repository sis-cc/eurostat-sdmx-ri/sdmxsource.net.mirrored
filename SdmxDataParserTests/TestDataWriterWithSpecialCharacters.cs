// -----------------------------------------------------------------------
// <copyright file="TestDataWriterWithSpecialCharacters.cs" company="EUROSTAT">
//   Date Created : 2022-6-1
//   Copyright (c) 2022 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxDataParserTests.
//     SdmxDataParserTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxDataParserTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParserTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Model.Header;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.DataParser.Manager;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Util.Io;

namespace SdmxDataParserTests
{
    class TestDataWriterWithSpecialCharacters
    {
        /// <summary>
        /// Regular expression for xs:ID used by <c>structureID</c> and <c>structureRef</c>
        /// </summary>
        private static readonly Regex idRegex = new Regex("^[A-Za-z_][\\w-]*$");
        /// <summary>
        /// Test unit for <see cref="CompactDataWriterEngine"/>
        /// </summary>
        /// <param name="format">
        /// The format.
        /// </param>
        [TestCase(DataEnumType.Compact21, "A@1_.GENC$Y.A", "DF@123@56$")]
        [TestCase(DataEnumType.Generic21, "A@1_.GENC$Y.A", "DF@123@56$")]
        [TestCase(DataEnumType.Compact21, "E-STAT", "DF@-123@56$")]
        [TestCase(DataEnumType.Generic21, "E-STAT", "DF@-123@56$")]
        public void DataflowHasStrangeChars(DataEnumType format, string agencyId, string id)
        {
            var uuid = Guid.NewGuid().ToString();
            string outfile = string.Format(System.Globalization.CultureInfo.InvariantCulture, "DataflowHasStrangeChars.{0}{1}.xml", format, uuid);
            DataType fromEnum = DataType.GetFromEnum(format);
            var objects = GetSdmxObjects("tests/v21/Structure/test-sdmxv2.1-ESTAT+STS+2.0.xml");
            IDataStructureObject dataStructureObject = objects.DataStructures.First();
            IDataflowMutableObject dataflow = new DataflowMutableCore();
            dataflow.AgencyId = agencyId;
            dataflow.Id = id;
            dataflow.Version = "1.0";
            dataflow.DataStructureRef = dataStructureObject.AsReference;
            dataflow.AddName("en", "Test dataflow");
            IDataflowObject immutableInstance = dataflow.ImmutableInstance;
            var dataWriterManager = new DataWriterManager();
            using (Stream writer = File.Create(outfile))
            {
                using (IDataWriterEngine dataWriter = dataWriterManager.GetDataWriterEngine(new SdmxDataFormatCore(fromEnum), writer))
                {
                    var header = GetHeader(immutableInstance);
                    dataWriter.WriteHeader(header);

                    dataWriter.StartDataset(immutableInstance, dataStructureObject, null);

                    WriteSampleData(dataWriter);

                }

            }
            VerifyStructureIdRef(outfile);

        }

        /// <summary>
        /// Test unit for <see cref="CompactDataWriterEngine"/>
        /// </summary>
        /// <param name="format">
        /// The format.
        /// </param>
        [TestCase(DataEnumType.Compact21, "A@1_.GENC$Y.A", "DF@123@56$")]
        [TestCase(DataEnumType.Generic21, "A@1_.GENC$Y.A", "DF@123@56$")]
        [TestCase(DataEnumType.Compact21, "E-STAT", "DF@-123@56$")]
        [TestCase(DataEnumType.Generic21, "E-STAT", "DF@-123@56$")]
        public void DsdHasStrangeChars(DataEnumType format, string agencyId, string id)
        {
            var uuid = Guid.NewGuid().ToString();
            string outfile = string.Format(System.Globalization.CultureInfo.InvariantCulture, "DsdHasStrangeChars.{0}{1}.xml", format, uuid);
            DataType fromEnum = DataType.GetFromEnum(format);
            var objects = GetSdmxObjects("tests/v21/Structure/test-sdmxv2.1-ESTAT+STS+2.0.xml");
            IDataStructureObject dataStructureObject = objects.DataStructures.First();
            IDataStructureMutableObject dsd = dataStructureObject.MutableInstance;
            dsd.AgencyId = agencyId;
            dsd.Id = id;
            IDataStructureObject immutableInstance = dsd.ImmutableInstance;
            var dataWriterManager = new DataWriterManager();
            using (Stream writer = File.Create(outfile))
            {
                using (IDataWriterEngine dataWriter = dataWriterManager.GetDataWriterEngine(new SdmxDataFormatCore(fromEnum), writer))
                {
                    var header = GetHeader(immutableInstance);
                    dataWriter.WriteHeader(header);

                    dataWriter.StartDataset(null, dataStructureObject, null);

                    WriteSampleData(dataWriter);

                }

            }
            VerifyStructureIdRef(outfile);

        }


        private static void WriteSampleData(IDataWriterEngine dataWriter)
        {
            dataWriter.StartSeries();
            dataWriter.WriteSeriesKeyValue("FREQ", "A");
            dataWriter.WriteSeriesKeyValue("REF_AREA", "DE");
            dataWriter.WriteSeriesKeyValue("ADJUSTMENT", "N");
            dataWriter.WriteSeriesKeyValue("STS_INDICATOR", "PROD");
            dataWriter.WriteSeriesKeyValue("STS_ACTIVITY", "NS0020");
            dataWriter.WriteSeriesKeyValue("STS_INSTITUTION", "1");
            dataWriter.WriteSeriesKeyValue("STS_BASE_YEAR", "2000");
            dataWriter.WriteAttributeValue("TIME_FORMAT", "P1Y");

            dataWriter.WriteObservation("2002", "1.234");
            dataWriter.WriteAttributeValue("OBS_STATUS", "A");
        }

        private static void VerifyStructureIdRef(string outfile)
        {
            string structureId = null;
            string structureRef = null;
            using (XmlReader xmlReader = XmlReader.Create(outfile))
            {

                while (xmlReader.Read())
                {
                    XmlNodeType nodeType = xmlReader.NodeType;
                    switch (nodeType)
                    {
                        case XmlNodeType.Element:

                            string localname = xmlReader.LocalName;
                            switch (localname)
                            {
                                case "Structure":
                                    string v = xmlReader.GetAttribute("structureID");
                                    if (v != null)
                                    {
                                        structureId = v;
                                    }
                                    break;
                                case "DataSet":

                                    v = SearchAttributeByLocalname(xmlReader, "structureRef");
                                    if (v != null)
                                    {
                                        structureRef = v;
                                    }
                                    break;
                            }
                            break;
                    }
                }
            }
            Assert.IsNotNull(structureId);
            Assert.IsNotNull(structureRef);
            Assert.IsTrue(idRegex.IsMatch(structureId));
            Assert.IsTrue(idRegex.IsMatch(structureRef));
            Assert.AreEqual(structureId, structureRef);
        }

        private static string SearchAttributeByLocalname(XmlReader reader, string attributeLocalName)
        {
            while (reader.MoveToNextAttribute())
            {
                if (reader.LocalName.Equals(attributeLocalName))
                {
                    return reader.Value;
                }
            }

            return null;
        }
        /// <summary>
        /// Gets the SDMX objects.
        /// </summary>
        /// <returns>The <see cref="ISdmxObjects"/>.</returns>
        private static ISdmxObjects GetSdmxObjects(string fileName)
        {
            ISdmxObjects objects;
            var file = new FileInfo(fileName);
            IStructureParsingManager manager = new StructureParsingManager(SdmxSchemaEnumType.Null);
            using (var readable = new FileReadableDataLocation(file))
            {
                IStructureWorkspace structureWorkspace = manager.ParseStructures(readable);
                objects = structureWorkspace.GetStructureObjects(false);
            }

            return objects;
        }
          /// <summary>
        /// Gets the header.
        /// </summary>
        /// <param name="dataStructureObject">The data structure object.</param>
        /// <returns>The <see cref="IHeader"/>.</returns>
        private static IHeader GetHeader(IMaintainableObject dataStructureObject)
        {
            IList<IDatasetStructureReference> structures = new List<IDatasetStructureReference> { new DatasetStructureReferenceCore(dataStructureObject.AsReference) };
            IList<IParty> receiver = new List<IParty> { new PartyCore(new List<ITextTypeWrapper>(), "ZZ9", new List<IContact>(), null) };
            var sender = new PartyCore(new List<ITextTypeWrapper> { new TextTypeWrapperImpl("en", "TEST SENDER", null) }, "ZZ1", null, null);
            IHeader header = new HeaderImpl(
                additionalAttributes: null,
                structures: structures,
                dataProviderReference: null,
                datasetAction: DatasetAction.GetFromEnum(DatasetActionEnumType.Information),
                id: "TEST_DATAFLOW",
                datasetId: "DATASET_ID",
                embargoDate: null,
                extracted: DateTime.Now,
                prepared: DateTime.Now,
                reportingBegin: null,
                reportingEnd: null,
                name: new List<ITextTypeWrapper> { new TextTypeWrapperImpl("en", "test header name", null) },
                source: new List<ITextTypeWrapper> { new TextTypeWrapperImpl("en", "source 1", null) },
                receiver: receiver,
                sender: sender,
                test: true);
            return header;
        }


    }
}
