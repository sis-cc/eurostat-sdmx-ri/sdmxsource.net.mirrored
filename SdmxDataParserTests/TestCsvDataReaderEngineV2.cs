namespace SdmxDataParserTests
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.DataParser.Factory;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Util.Io;

    using Is = NUnit.Framework.Is;

    /// <summary>
    /// Unit tests for <see cref="IDataReaderEngine"/>
    /// </summary>
    [TestFixture]
    public class TestCsvDataReaderEngineV2
    {
        /// <summary>
        /// The _factory
        /// </summary>
        private readonly IReadableDataLocationFactory _factory;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestCsvDataReaderEngineV2"/> class.
        /// </summary>
        public TestCsvDataReaderEngineV2()
        {
            this._factory = new ReadableDataLocationFactory();
        }

        /// <summary>
        /// Tests the csv v2 data reader.
        /// </summary>
        [TestCase("tests/v21/CsvV2.xml", "tests/Data/CsvV2_with_obs_value_all_dims.csv", 5,3, 4, 15)]
        [TestCase("tests/v21/CsvV2.xml", "tests/Data/CsvV2_without_obs_value.csv", 5, 3, 4, 15)]
        [TestCase("tests/v21/CsvV2.xml", "tests/Data/CsvV2_only_metadata.csv", 0,1, 1, 3)]
        [TestCase("tests/v21/CsvV2.xml", "tests/Data/CsvV2_one_dim_only_metadata_without_action.csv", 1, 1, 1, 3)]
        [TestCase("tests/v21/CsvV2.xml", "tests/Data/CsvV2_only_metadata_without_subfielddelimeter.csv", 0, 1, 1, 2)]
        [TestCase("tests/v21/DF_POP_COAST_TEST.xml", "tests/Data/CsvV2_attributes_all_dims.csv", 4, 7, 21, 105)]
        [TestCase("tests/v21/DF_POP_COAST_TEST.xml", "tests/Data/CsvV2_attribute_no_missing_dim.csv", 1, 2, 2, 2)]
        [TestCase("tests/v21/DF_POP_COAST_TEST.xml", "tests/Data/CsvV2_group_attribute_missing_dim.csv", 4, 2, 2, 2)]
        public void TestCsvV2DataReaderCount(string dsdFile, string dataFile, int expectedDimensions, int expectedSeries, int expectedObservations, int expectedAttributes)
        {
            var retrievalManager = new InMemoryRetrievalManager();
            retrievalManager.SaveStructures(GetSdmxObjects(dsdFile));

            var seriesCount = 0;
            var observationCount = 0;
            var attributeCount = 0;

            using (var sourceData = this._factory.GetReadableDataLocation(new FileInfo(dataFile)))
            {
                using (var dataReaderEngine = new SdmxDataReaderFactory(new DataInformationManager(), null).GetDataReaderEngine(sourceData, retrievalManager))
                {
                    if (dataReaderEngine.MoveNextDataset())
                    {
                        Assert.AreEqual(DatasetActionEnumType.Information, dataReaderEngine.CurrentDatasetHeader.Action.EnumType);
                        Assert.AreEqual(0, dataReaderEngine.DatasetAttributes.Count);

                        while (dataReaderEngine.MoveNextKeyable())
                        {
                            seriesCount++;

                            Assert.AreEqual(expectedDimensions, dataReaderEngine.CurrentKey.Key.Count);

                            while (dataReaderEngine.MoveNextObservation())
                            {
                                Assert.That(dataReaderEngine.CurrentObservation, Is.Not.Null);

                                observationCount++;
                                attributeCount += dataReaderEngine.CurrentObservation.Attributes.Count(x => !string.IsNullOrEmpty(x.Code));
                                //dataReaderEngine.CurrentKey.Attributes
                            }
                        }
                    }
                }
            }

            Assert.AreEqual(expectedSeries, seriesCount);
            Assert.AreEqual(expectedObservations, observationCount);
            Assert.AreEqual(expectedAttributes, attributeCount);
        }

        /// <summary>
        /// Tests the csv v2 data reader with multiple actions
        /// </summary>
        [Test]
        public void TestCsvV2DataReaderMultipleActions()
        {
            var retrievalManager = new InMemoryRetrievalManager();
            retrievalManager.SaveStructures(GetSdmxObjects("tests/v21/DF_POP_COAST_TEST.xml"));

            var informationCount = 0;
            var appendCount = 0;
            var replaceCount = 0;
            var deleteCount = 0;
            var mergeCount = 0;

            using var sourceData = this._factory.GetReadableDataLocation(new FileInfo("tests/Data/CsvV2_multiple_actions.csv"));
            using var dataReaderEngine = new SdmxDataReaderFactory(new DataInformationManager(), null).GetDataReaderEngine(sourceData, retrievalManager);
            if (dataReaderEngine.MoveNextDataset())
            {
                Assert.AreEqual(DatasetActionEnumType.Delete, dataReaderEngine.CurrentDatasetHeader.Action.EnumType);
                Assert.AreEqual(DatasetActionEnumType.Delete, dataReaderEngine.CurrentAction.EnumType);

                while (dataReaderEngine.MoveNextKeyable())
                {
                    while (dataReaderEngine.MoveNextObservation())
                    {
                        switch (dataReaderEngine.CurrentAction.EnumType)
                        {
                            case DatasetActionEnumType.Information: informationCount++;
                                break;
                            case DatasetActionEnumType.Append: appendCount++;
                                break;
                            case DatasetActionEnumType.Replace: replaceCount++;
                                break;
                            case DatasetActionEnumType.Delete: deleteCount++;
                                break;
                            case DatasetActionEnumType.Merge: mergeCount++;
                                break;
                        }
                    }
                }
            }

            Assert.AreEqual(2, informationCount);
            Assert.AreEqual(3, appendCount);
            Assert.AreEqual(6, replaceCount);
            Assert.AreEqual(4, deleteCount);
            Assert.AreEqual(6, mergeCount);
        }

        [Test]
        public void TestCsvV2DataReaderMultipleDatasetAttributeValues()
        {
            var retrievalManager = new InMemoryRetrievalManager();
            retrievalManager.SaveStructures(GetSdmxObjects("tests/v21/DF_POP_COAST_TEST.xml"));

            using var sourceData = this._factory.GetReadableDataLocation(new FileInfo("tests/Data/CsvV2_multiple_dataset_attributes.csv"));
            using var dataReaderEngine = new SdmxDataReaderFactory(new DataInformationManager(), null).GetDataReaderEngine(sourceData, retrievalManager);

            dataReaderEngine.MoveNextDataset();
            Assert.AreEqual("FIRST_COMMENT", dataReaderEngine.DatasetAttributes.First().Code);

            dataReaderEngine.MoveNextKeyable();

            dataReaderEngine.MoveNextObservation();
            Assert.AreEqual("FIRST_COMMENT", dataReaderEngine.DatasetAttributes.First().Code);
            dataReaderEngine.MoveNextObservation();
            Assert.AreEqual("SECOND_COMMENT", dataReaderEngine.DatasetAttributes.First().Code);
        }

        /// <summary>
        /// Tests the csv v2 data reader with observation values without all dimensions
        /// </summary>
        [Test]
        public void TestCsvV2DataReaderWithObsValueWithoutAllDimensions()
        {
            var retrievalManager = new InMemoryRetrievalManager();
            retrievalManager.SaveStructures(GetSdmxObjects("tests/v21/CsvV2.xml"));

            using (var sourceData = this._factory.GetReadableDataLocation(new FileInfo("tests/Data/CsvV2_with_obs_value_not_all_dims.csv")))
            {
                using (var dataReaderEngine = new SdmxDataReaderFactory(new DataInformationManager(), null).GetDataReaderEngine(sourceData, retrievalManager))
                {
                    Assert.Throws<SdmxSemmanticException>(() => dataReaderEngine.MoveNextDataset());
                }
            }
        }

        /// <summary>
        /// Tests the csv v2 data reader.
        /// </summary>
        /// <param name="dsdFile"></param>
        /// <param name="dataFile"></param>
        [TestCase("tests/v21/CsvV2.xml", "tests/Data/CsvV2_with_labels.csv")]
        public void TestCsvV2DataReader(string dsdFile, string dataFile)
        {
            var retrievalManager = new InMemoryRetrievalManager();
            retrievalManager.SaveStructures(GetSdmxObjects(dsdFile));

            using (var sourceData = this._factory.GetReadableDataLocation(new FileInfo(dataFile)))
            {
                using (var dataReaderEngine = new SdmxDataReaderFactory(new DataInformationManager(), null).GetDataReaderEngine(sourceData, retrievalManager))
                {
                    dataReaderEngine.MoveNextDataset();
                    Assert.AreEqual(DatasetActionEnumType.Information, dataReaderEngine.CurrentDatasetHeader.Action.EnumType);

                    dataReaderEngine.MoveNextKeyable();
                    dataReaderEngine.MoveNextObservation();

                    VerifyObservation(dataReaderEngine.CurrentObservation, 
                        "series  FREQ:D,REPORTING_TYPE:N,SERIES:SI_POV_NAHC,REF_AREA:ET,SEX:_T", 
                        "2009-01", 
                        "1");

                    VerifyAttributes(dataReaderEngine.CurrentObservation.Attributes, new Dictionary<string, string>() {
                            {"OBS_STATUS", "A"}, 
                            {"STRING_TYPE", "STRING_TYPE"}, 
                            {"ALPHANUMERIC_TYPE", "ALPHANUMERICTYPE897"}});

                    dataReaderEngine.MoveNextObservation();

                    VerifyObservation(dataReaderEngine.CurrentObservation,
                        "series  FREQ:D,REPORTING_TYPE:N,SERIES:SI_POV_NAHC,REF_AREA:ET,SEX:_T",
                        string.Empty,
                        "2");

                    VerifyAttributes(dataReaderEngine.CurrentObservation.Attributes, new Dictionary<string, string>() {
                        {"OBS_STATUS", "B"},
                        {"BOOLEAN_TYPE", "TRUE"},
                        {"STRING_MULTILANG_TYPE", "\"en:\"\"Value X\"\";fr:Valeur X\";\"de:Wert Y;en:Value Y;fr:Valeur Y\""}});

                    dataReaderEngine.MoveNextKeyable();

                    dataReaderEngine.MoveNextObservation();

                    VerifyObservation(dataReaderEngine.CurrentObservation,
                        "series  FREQ:,REPORTING_TYPE:,SERIES:,REF_AREA:,SEX:_T",
                        string.Empty,
                        "3");

                    VerifyAttributes(dataReaderEngine.CurrentObservation.Attributes, new Dictionary<string, string>() {
                        {"OBS_STATUS", "D"},
                        {"XHTML_TYPE", "<p>This is some \"xhtml\" with a line\r\nbreak</p>"},
                        {"INTEGER_TYPE", "999"},
                        {"DECIMAL_TYPE", "99.9"},
                        {"DATETIME_TYPE", "2009-11-28T23:59:59"}});

                    dataReaderEngine.MoveNextKeyable();

                    dataReaderEngine.MoveNextObservation();

                    VerifyObservation(dataReaderEngine.CurrentObservation,
                        "series  FREQ:,REPORTING_TYPE:,SERIES:,REF_AREA:,SEX:",
                        string.Empty,
                        "4");

                    VerifyAttributes(dataReaderEngine.CurrentObservation.Attributes, new Dictionary<string, string>() {
                        {"OBS_STATUS", "E"},
                        {"CONTACT.CONTACT_NAME", "Jens;Lolita"},
                        {"CONTACT.CONTACT_EMAIL", "\"jens.dosse@oecd.org;jens.dosse@ocde.org\";\"Lolita.BOUMELIT@oecd.org\""},
                        {"CONTACT.CONTACT_ORGANISATION", "OECD;OECD"}});
                }
            }
        }

        /// <summary>
        /// Tests the csv v2 data reader attribute verification.
        /// </summary>
        [TestCase("tests/v21/DF_POP_COAST_TEST.xml", "tests/Data/CsvV2_attributes_all_dims.csv", false)]
        [TestCase("tests/v21/DF_POP_COAST_TEST.xml", "tests/Data/CsvV2_attribute_no_missing_dim.csv", false)]
        [TestCase("tests/v21/DF_POP_COAST_TEST.xml", "tests/Data/CsvV2_attribute_missing_dim.csv", true)]
        [TestCase("tests/v21/DF_POP_COAST_TEST.xml", "tests/Data/CsvV2_attribute_missing_dim_2.csv", true)]
        public void TestCsvV2DataReaderAttributes(string dsdFile, string dataFile, bool shouldThrowMissingDimException)
        {
            var retrievalManager = new InMemoryRetrievalManager();
            retrievalManager.SaveStructures(GetSdmxObjects(dsdFile));

            using (var sourceData = this._factory.GetReadableDataLocation(new FileInfo(dataFile)))
            {
                using (var dataReaderEngine = new SdmxDataReaderFactory(new DataInformationManager(), null).GetDataReaderEngine(sourceData, retrievalManager))
                {
                    if (shouldThrowMissingDimException)
                    {
                        Assert.Throws<SdmxSemmanticException>(() => dataReaderEngine.MoveNextDataset());
                    }
                    else
                    {
                        Assert.DoesNotThrow(() => dataReaderEngine.MoveNextDataset());
                    }
                }
            }
        }

        /// <summary>
        /// Gets the SDMX objects.
        /// </summary>
        /// <returns>The <see cref="ISdmxObjects"/>.</returns>
        private static ISdmxObjects GetSdmxObjects(string fileName)
        {
            ISdmxObjects objects;
            var file = new FileInfo(fileName);
            IStructureParsingManager manager = new StructureParsingManager(SdmxSchemaEnumType.Null);
            using (var readable = new FileReadableDataLocation(file))
            {
                IStructureWorkspace structureWorkspace = manager.ParseStructures(readable);
                objects = structureWorkspace.GetStructureObjects(false);
            }

            return objects;
        }

        public void VerifyObservation(IObservation observation, string expectedSeriesKey, string expectedTime, string expectedValue)
        {
            Assert.AreEqual(expectedSeriesKey, observation.SeriesKey.ToString());
            Assert.AreEqual(expectedTime, observation.ObsTime);
            Assert.AreEqual(expectedValue, observation.ObservationValue);
        }

        public void VerifyAttributes(IList<IKeyValue> attributeList, Dictionary<string, string> expectedAttributesWithValues)
        {
            foreach (var attribute in attributeList.Where(x => !expectedAttributesWithValues.ContainsKey(x.Concept)))
            {
                Assert.AreEqual(string.Empty, attribute.Code);
            }

            foreach (var attribute in attributeList.Where(x => expectedAttributesWithValues.ContainsKey(x.Concept)))
            {
                Assert.AreEqual(expectedAttributesWithValues[attribute.Concept], attribute.Code);
            }
        }
    }
}