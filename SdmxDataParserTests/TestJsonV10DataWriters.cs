// -----------------------------------------------------------------------
// <copyright file="TestDataWriters.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxDataParserTests.
//     SdmxDataParserTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxDataParserTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParserTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using Org.Sdmxsource.Sdmx.DataParser.Engine.JsonSupport;
using Org.Sdmxsource.Sdmx.Util.Date;

namespace SdmxDataParserTests
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;

    using Newtonsoft.Json.Linq;
    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Sdmx.DataParser.Factory;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.SuperObjects;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Translator;
    using Org.Sdmxsource.Util.Io;

    using DataType = Org.Sdmxsource.Sdmx.Api.Constants.DataType;

    /// <summary>
    ///     Test unit for SDMX Data Writers
    /// </summary>
    [TestFixture]
    public class TestJsonV10DataWriters
    {
        /// <summary>
        /// 
        /// </summary>
        [TestCaseSource("TestCases")]
        public void TestJsonV10DataWriterEngine(string dsdFile, string dataFile, string dimensionAtObservation, Action<JObject> validate)
        {
            var dataType = DataType.GetFromEnum(DataEnumType.Json10);
            var sdmxObjects = GetSdmxObjects(dsdFile);
            var dsd = sdmxObjects.DataStructures.First();
            var dataflow = sdmxObjects.Dataflows.First();
            IHeader header = null;

            var dataWriterManager = new DataWriterManager(new SdmxJsonDataWriterFactory());

            var fileName = dataFile.Replace(".xml",".json");
            if (dimensionAtObservation == DatasetStructureReference.AllDimensions)
            {
                fileName = fileName.Replace(".series.", ".flat.");
            }

            var preferedLanguageTranslator = new PreferedLanguageTranslator(
                new List<CultureInfo>()
                {
                    CultureInfo.GetCultureInfo("it"),
                    CultureInfo.GetCultureInfo("fr"),
                    CultureInfo.GetCultureInfo("en")
                },
                new List<CultureInfo>()
                {
                    CultureInfo.GetCultureInfo("fr"),
                    CultureInfo.GetCultureInfo("en")
                },
                CultureInfo.GetCultureInfo("en"));

            var sdmxDataReaderFactory = new SdmxDataReaderFactory(new DataInformationManager(new FixedConceptEngine(), new ReportedDateEngine()), null);
            IReadableDataLocationFactory factory = new ReadableDataLocationFactory();

            using (var sourceData = factory.GetReadableDataLocation(new FileInfo(dataFile)))
            {
                using (var dataReaderEngine = sdmxDataReaderFactory.GetDataReaderEngine(sourceData, dsd, null))
                {
                    header = dataReaderEngine.Header;

                    using (Stream writer = File.Create(fileName))
                    {
                        using (IDataWriterEngine dataWriter = dataWriterManager.GetDataWriterEngine(
                            new SdmxJsonDataFormat(new InMemorySdmxSuperObjectRetrievalManager(new SuperObjectsBuilder(), sdmxObjects),
                                dataType,
                                preferedLanguageTranslator),
                            writer))
                        {
                            dataWriter.WriteHeader(dataReaderEngine.Header);

                            if (string.IsNullOrEmpty(dimensionAtObservation))
                            {
                                dimensionAtObservation = dataReaderEngine.Header.Structures.First().DimensionAtObservation;
                            }

                            IDatasetStructureReference dsRef = new DatasetStructureReferenceCore("Test", dsd.AsReference, null, null, dimensionAtObservation);
                            IDatasetHeader datasetHeader = new DatasetHeaderCore("DS12345", DatasetActionEnumType.Information, dsRef);

                            dataWriter.StartDataset(dataflow, dsd, datasetHeader);

                            if (dataReaderEngine.MoveNextDataset())
                            {
                                foreach (var att in dataReaderEngine.DatasetAttributes)
                                {
                                    dataWriter.WriteAttributeValue(att.Concept, att.Code);
                                }

                                var hasSeriesStarted = false;

                                while (dataReaderEngine.MoveNextKeyable())
                                {
                                    IKeyable currentKey = dataReaderEngine.CurrentKey;

                                    if (currentKey.Series)
                                    {
                                        if (!hasSeriesStarted)
                                        {
                                            // Once series nodes started no more group nodes are allowed according to generic XML data message specification
                                            dataWriter.StartSeries(currentKey.Annotations.ToArray());

                                            hasSeriesStarted = true;
                                        }

                                        foreach (var kv in currentKey.Key)
                                        {
                                            dataWriter.WriteSeriesKeyValue(kv.Concept, kv.Code);
                                        }
                                    }
                                    else
                                    {
                                        dataWriter.StartGroup(currentKey.GroupName, currentKey.Annotations.ToArray());

                                        foreach (var kv in currentKey.Key)
                                        {
                                            dataWriter.WriteGroupKeyValue(kv.Concept, kv.Code);
                                        }
                                    }

                                    foreach (var att in currentKey.Attributes)
                                    {
                                        dataWriter.WriteAttributeValue(att.Concept, att.Code);
                                    }

                                    while (dataReaderEngine.MoveNextObservation())
                                    {
                                        IObservation currentObservation = dataReaderEngine.CurrentObservation;
                                        
                                        dataWriter.WriteObservation(DimensionObject.TimeDimensionFixedId, currentObservation.ObsTime, currentObservation.ObservationValue);

                                        foreach (var att in currentObservation.Attributes)
                                        {
                                            dataWriter.WriteAttributeValue(att.Concept, att.Code);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            JObject result = JObject.Parse(File.ReadAllText(fileName), new JsonLoadSettings() { LineInfoHandling = LineInfoHandling.Ignore });

            Assert.IsNotNull(result.SelectToken("meta.id"));
            Assert.IsNotNull(result.SelectToken("meta.schema"));
            Assert.IsNotNull(result.SelectToken("meta.prepared").Value<DateTime>());
            Assert.AreEqual(header.Test, result.SelectToken("meta.test").Value<bool>());
            Assert.AreEqual(header.Sender.Id, result.SelectToken("meta.sender.id").Value<string>());

            validate(result);
        }

        public static IEnumerable<TestCaseData> TestCases
        {
            get
            {
                yield return new TestCaseData(
                    "tests/v21/264D_264_SALDI2_ATTRIBUTE_TEST.xml",
                    "tests/Data/test_264D_264_SALDI2_2-generic_output.series.xml",
                    DatasetStructureReference.AllDimensions,
                    new Action<JObject>(ValidateFlat)
                );

                yield return new TestCaseData(
                    "tests/v21/264D_264_SALDI2_ATTRIBUTE_TEST.xml",
                    "tests/Data/test_264D_264_SALDI2_2-generic_output.series.xml",
                    null,
                    new Action<JObject>(ValidateSeries)
                );

                yield return new TestCaseData(
                    "tests/v21/TEST_ANN.xml",
                    "tests/Data/TEST_ANN_generic_2.1.xml",
                    DatasetStructureReference.AllDimensions,
                    new Action<JObject>(ValidateAnnotationsFlat)
                );

                yield return new TestCaseData(
                    "tests/v21/264D_264_SALDI2_ATTRIBUTE_TEST.xml",
                    "tests/Data/test_264D_264_SALDI2_2-generic_output.series.GroupAttr.xml",
                    null,
                    new Action<JObject>(ValidateSeriesWithGroupAttrSeries)
                );

                yield return new TestCaseData(
                    "tests/v21/264D_264_SALDI2_ATTRIBUTE_TEST.xml",
                    "tests/Data/test_264D_264_SALDI2_2-generic_output.series.GroupAttr.xml",
                    DatasetStructureReference.AllDimensions,
                    new Action<JObject>(ValidateSeriesWithGroupAttrFlat)
                );

            }
        }

        private static void ValidateFlat(JObject result)
        {
            // Common checks
            VerifyJObject("\"Information\"", result, "data.dataSets[0].action");

            VerifyJObject("\"Monthly seasonally adjusted and not adjusted balances #2\"", result, "data.structure.name");

            VerifyJObject("{\"id\":\"COLLECTION\",\"name\":\"Collection\",\"names\":{\"en\":\"Collection\"},\"roles\":[\"COLLECTION\"],\"relationship\":{\"none\":{}},\"values\":[{\"id\":\"B\",\"order\":1,\"name\":\"Beginning of period\",\"names\":{\"en\":\"Beginning of period\"}}]}",
                result, "data.structure.attributes.dataSet[0]");

            VerifyJObject("{\"id\":\"COMMENT\",\"name\":\"Comment\",\"names\":{\"en\":\"Comment\"},\"roles\":[\"COMMENT\"],\"relationship\":{\"primaryMeasure\":\"OBS_VALUE\"},\"values\":[{\"id\":\"This is a sample comment - 10101010\",\"name\":\"This is a sample comment - 10101010\",\"names\":{\"en\":\"This is a sample comment - 10101010\"}},{\"id\":\"This is a sample comment - 2018-Q1\",\"name\":\"This is a sample comment - 2018-Q1\",\"names\":{\"en\":\"This is a sample comment - 2018-Q1\"}}]}",
                result, "data.structure.attributes.observation[0]");
            //VerifyJObject("{\"id\":\"COMMENT\",\"name\":\"Comment\",\"names\":{\"en\":\"Comment\"},\"roles\":[\"COMMENT\"],\"relationship\":{\"primaryMeasure\":\"OBS_VALUE\"},\"values\":[{\"name\":\"This is a sample comment - 10101010\",\"names\":{\"it\":\"This is a sample comment - 10101010\",\"en\":\"This is a sample comment - 10101010\"}},{\"name\":\"This is a sample comment - 2018-Q1\",\"names\":{\"it\":\"This is a sample comment - 2018-Q1\",\"en\":\"This is a sample comment - 2018-Q1\"}}]}",
            //    result, "data.structure.attributes.observation[0]");

            // -------

            VerifyJObject("{\"id\":\"FREQ\",\"name\":\"Frequency\",\"names\":{\"en\":\"Frequency\"},\"keyPosition\":0,\"roles\":[\"FREQ\"],\"values\":[{\"id\":\"A\",\"order\":0,\"name\":\"annual\",\"names\":{\"en\":\"annual\"},\"annotations\":[0]},{\"id\":\"Q\",\"order\":6,\"name\":\"quarterly\",\"names\":{\"en\":\"quarterly\"},\"annotations\":[1]}]}",
                result, "data.structure.dimensions.observation[0]");

            VerifyJObject("{\"id\":\"TITLE\",\"name\":\"Title\",\"names\":{\"en\":\"Title\"},\"roles\":[\"TITLE\"],\"relationship\":{\"dimensions\":[\"FREQ\",\"ADJUSTMENT\"]},\"values\":[{\"id\":\"This is a sample TITLE\",\"name\":\"This is a sample TITLE\",\"names\":{\"en\":\"This is a sample TITLE\"}}]}",
                result, "data.structure.attributes.observation[8]");

            VerifyJObject("\"A\"", result, "data.structure.dimensions.observation[0].values[0].id");

            VerifyJObject("\"2002\"", result, "data.structure.dimensions.observation[9].values[0].name");
        }

        private static void ValidateSeries(JObject result)
        {
            VerifyJObject("\"Information\"", result, "data.dataSets[0].action");

            VerifyJObject("\"Monthly seasonally adjusted and not adjusted balances #2\"", result, "data.structure.name");

            VerifyJObject("{\"id\":\"COLLECTION\",\"name\":\"Collection\",\"names\":{\"en\":\"Collection\"},\"roles\":[\"COLLECTION\"],\"relationship\":{\"none\":{}},\"values\":[{\"id\":\"B\",\"order\":1,\"name\":\"Beginning of period\",\"names\":{\"en\":\"Beginning of period\"}}]}",
                result, "data.structure.attributes.dataSet[0]");

            VerifyJObject("{\"id\":\"COMMENT\",\"name\":\"Comment\",\"names\":{\"en\":\"Comment\"},\"roles\":[\"COMMENT\"],\"relationship\":{\"primaryMeasure\":\"OBS_VALUE\"},\"values\":[{\"id\":\"This is a sample comment - 10101010\",\"name\":\"This is a sample comment - 10101010\",\"names\":{\"en\":\"This is a sample comment - 10101010\"}},{\"id\":\"This is a sample comment - 2018-Q1\",\"name\":\"This is a sample comment - 2018-Q1\",\"names\":{\"en\":\"This is a sample comment - 2018-Q1\"}}]}",
                result, "data.structure.attributes.observation[0]");

            // -------

            VerifyJObject("{\"id\":\"TIME_PERIOD\",\"name\":\"Time period\",\"names\":{\"en\":\"Time period\"},\"keyPosition\":9,\"roles\":[\"TIME_PERIOD\"],\"values\":[{\"start\":\"2002-01-01T00:00:00\",\"end\":\"2002-12-31T23:59:59\",\"id\":\"2002\",\"name\":\"2002\",\"names\":{\"en\":\"2002\"}},{\"start\":\"2002-07-01T00:00:00\",\"end\":\"2002-09-30T00:00:00\",\"id\":\"2002-Q3\",\"name\":\"2002-Q3\",\"names\":{\"en\":\"2002-Q3\"}},{\"start\":\"2012-01-01T00:00:00\",\"end\":\"2012-12-31T23:59:59\",\"id\":\"2012\",\"name\":\"2012\",\"names\":{\"en\":\"2012\"}},{\"start\":\"2017-01-01T00:00:00\",\"end\":\"2017-12-31T23:59:59\",\"id\":\"2017\",\"name\":\"2017\",\"names\":{\"en\":\"2017\"}},{\"start\":\"2018-01-01T00:00:00\",\"end\":\"2018-12-31T23:59:59\",\"id\":\"2018\",\"name\":\"2018\",\"names\":{\"en\":\"2018\"}},{\"start\":\"2018-01-01T00:00:00\",\"end\":\"2018-03-31T00:00:00\",\"id\":\"2018-Q1\",\"name\":\"2018-Q1\",\"names\":{\"en\":\"2018-Q1\"}}]}",
                result, "data.structure.dimensions.observation[0]");

            VerifyJObject("{\"id\":\"TITLE\",\"name\":\"Title\",\"names\":{\"en\":\"Title\"},\"roles\":[\"TITLE\"],\"relationship\":{\"dimensions\":[\"FREQ\",\"ADJUSTMENT\"]},\"values\":[{\"id\":\"This is a sample TITLE\",\"name\":\"This is a sample TITLE\",\"names\":{\"en\":\"This is a sample TITLE\"}}]}",
                result, "data.structure.attributes.series[0]");

            VerifyJObject("\"2002\"", result, "data.structure.dimensions.observation[0].values[0].id");

            VerifyJObject("\"2002\"", result, "data.structure.dimensions.observation[0].values[0].name");
        }

        private static void ValidateSeriesWithGroupAttrSeries(JObject result)
        {
            VerifyJObject("\"Information\"", result, "data.dataSets[0].action");

            VerifyJObject("\"Monthly seasonally adjusted and not adjusted balances #2\"", result, "data.structure.name");

            VerifyJObject("{\"id\":\"COLLECTION\",\"name\":\"Collection\",\"names\":{\"en\":\"Collection\"},\"roles\":[\"COLLECTION\"],\"relationship\":{\"none\":{}},\"values\":[{\"id\":\"B\",\"order\":1,\"name\":\"Beginning of period\",\"names\":{\"en\":\"Beginning of period\"}}]}",
                result, "data.structure.attributes.dataSet[0]");

            VerifyJObject("{\"id\":\"COMMENT\",\"name\":\"Comment\",\"names\":{\"en\":\"Comment\"},\"roles\":[\"COMMENT\"],\"relationship\":{\"primaryMeasure\":\"OBS_VALUE\"},\"values\":[{\"id\":\"This is a sample comment - 10101010\",\"name\":\"This is a sample comment - 10101010\",\"names\":{\"en\":\"This is a sample comment - 10101010\"}},{\"id\":\"This is a sample comment - 2018-Q1\",\"name\":\"This is a sample comment - 2018-Q1\",\"names\":{\"en\":\"This is a sample comment - 2018-Q1\"}}]}",
                result, "data.structure.attributes.observation[0]");

            // -------
            VerifyJObject("{\"id\":\"TIME_PERIOD\",\"name\":\"Time period\",\"names\":{\"en\":\"Time period\"},\"keyPosition\":9,\"roles\":[\"TIME_PERIOD\"],\"values\":[{\"start\":\"2002-01-01T00:00:00\",\"end\":\"2002-12-31T23:59:59\",\"id\":\"2002\",\"name\":\"2002\",\"names\":{\"en\":\"2002\"}},{\"start\":\"2018-01-01T00:00:00\",\"end\":\"2018-12-31T23:59:59\",\"id\":\"2018\",\"name\":\"2018\",\"names\":{\"en\":\"2018\"}},{\"start\":\"2019-01-01T00:00:00\",\"end\":\"2019-12-31T23:59:59\",\"id\":\"2019\",\"name\":\"2019\",\"names\":{\"en\":\"2019\"}},{\"start\":\"2018-01-01T00:00:00\",\"end\":\"2018-03-31T00:00:00\",\"id\":\"2018-Q1\",\"name\":\"2018-Q1\",\"names\":{\"en\":\"2018-Q1\"}}]}",
                result, "data.structure.dimensions.observation[0]");

            VerifyJObject("{\"id\":\"TEST_ATTR_GROUP_CODED\",\"name\":\"Reference metadata at dataflow level (in English)\",\"names\":{\"en\":\"Reference metadata at dataflow level (in English)\"},\"roles\":[\"METADATA_EN\"],\"relationship\":{\"dimensions\":[\"FREQ\",\"ADJUSTMENT\"]},\"values\":[{\"id\":\"P1D\",\"order\":5,\"name\":\"Daily\",\"names\":{\"en\":\"Daily\"}},{\"id\":\"P7D\",\"order\":4,\"name\":\"Weekly\",\"names\":{\"en\":\"Weekly\"}}]}",
                result, "data.structure.attributes.series[4]");

            VerifyJObject("\"2002\"", result, "data.structure.dimensions.observation[0].values[0].id");

            VerifyJObject("\"2002\"", result, "data.structure.dimensions.observation[0].values[0].name");

            //XML source: FREQ:A, ADJUSTMENT:C, OBS_VAL:55551, TITLE:series, METADATA_EN:group, METADATA_IT:null, TEST_ATTR_DIM_CODED:series, TEST_ATTR_GROUP_CODED:group
            VerifyJObject("[0,0,null,0,0]", result, "data.dataSets[0].series.0:0:0:0:0:0:0:0:0.attributes");
            //XML source: FREQ:A, ADJUSTMENT:C, OBS_VAL:55552, TITLE:series , METADATA_EN:group, METADATA_IT:series, TEST_ATTR_DIM_CODED:series, TEST_ATTR_GROUP_CODED:group
            VerifyJObject("[0,0,0,0,0]", result, "data.dataSets[0].series.0:1:0:0:0:0:0:0:0.attributes");
            //XML source: FREQ:A, ADJUSTMENT:X, OBS_VAL:55553, TITLE:null , METADATA_EN:series, METADATA_IT:null, TEST_ATTR_DIM_CODED:series, TEST_ATTR_GROUP_CODED:null
            VerifyJObject("[null,2,null,0,null]", result, "data.dataSets[0].series.0:2:0:1:0:0:0:0:0.attributes");
            //XML source: FREQ:A, ADJUSTMENT:X, OBS_VAL:55554, TITLE:null , METADATA_EN:null, METADATA_IT:null, TEST_ATTR_DIM_CODED:null, TEST_ATTR_GROUP_CODED:null
            VerifyJObject("[null,null,null,null,null]", result, "data.dataSets[0].series.0:3:0:1:0:0:0:0:0.attributes");
            //XML source: FREQ:Q, ADJUSTMENT:C, OBS_VAL:55555, TITLE:null , METADATA_EN:group, METADATA_IT:null, TEST_ATTR_DIM_CODED:series, TEST_ATTR_GROUP_CODED:series
            VerifyJObject("[null,1,null,0,1]", result, "data.dataSets[0].series.1:2:0:0:0:0:0:0:0.attributes");
            //XML source: FREQ:Q, ADJUSTMENT:C, OBS_VAL:55556, TITLE:null , METADATA_EN:group, METADATA_IT:null, TEST_ATTR_DIM_CODED:null, TEST_ATTR_GROUP_CODED:null
            VerifyJObject("[null,1,null,null,null]", result, "data.dataSets[0].series.1:3:0:0:0:0:0:0:0.attributes");
        }

        private static void ValidateSeriesWithGroupAttrFlat(JObject result)
        {
            VerifyJObject("\"Information\"", result, "data.dataSets[0].action");

            VerifyJObject("\"Monthly seasonally adjusted and not adjusted balances #2\"", result, "data.structure.name");

            VerifyJObject("{\"id\":\"COLLECTION\",\"name\":\"Collection\",\"names\":{\"en\":\"Collection\"},\"roles\":[\"COLLECTION\"],\"relationship\":{\"none\":{}},\"values\":[{\"id\":\"B\",\"order\":1,\"name\":\"Beginning of period\",\"names\":{\"en\":\"Beginning of period\"}}]}",
                result, "data.structure.attributes.dataSet[0]");

            VerifyJObject("{\"id\":\"COMMENT\",\"name\":\"Comment\",\"names\":{\"en\":\"Comment\"},\"roles\":[\"COMMENT\"],\"relationship\":{\"primaryMeasure\":\"OBS_VALUE\"},\"values\":[{\"id\":\"This is a sample comment - 10101010\",\"name\":\"This is a sample comment - 10101010\",\"names\":{\"en\":\"This is a sample comment - 10101010\"}},{\"id\":\"This is a sample comment - 2018-Q1\",\"name\":\"This is a sample comment - 2018-Q1\",\"names\":{\"en\":\"This is a sample comment - 2018-Q1\"}}]}",
                result, "data.structure.attributes.observation[0]");

            // -------
            VerifyJObject("{\"id\":\"FREQ\",\"name\":\"Frequency\",\"names\":{\"en\":\"Frequency\"},\"keyPosition\":0,\"roles\":[\"FREQ\"],\"values\":[{\"id\":\"A\",\"order\":0,\"name\":\"annual\",\"names\":{\"en\":\"annual\"},\"annotations\":[0]},{\"id\":\"Q\",\"order\":6,\"name\":\"quarterly\",\"names\":{\"en\":\"quarterly\"},\"annotations\":[1]}]}",
                result, "data.structure.dimensions.observation[0]");

            VerifyJObject("{\"id\":\"TEST_ATTR_GROUP_CODED\",\"name\":\"Reference metadata at dataflow level (in English)\",\"names\":{\"en\":\"Reference metadata at dataflow level (in English)\"},\"roles\":[\"METADATA_EN\"],\"relationship\":{\"dimensions\":[\"FREQ\",\"ADJUSTMENT\"]},\"values\":[{\"id\":\"P1D\",\"order\":5,\"name\":\"Daily\",\"names\":{\"en\":\"Daily\"}},{\"id\":\"P7D\",\"order\":4,\"name\":\"Weekly\",\"names\":{\"en\":\"Weekly\"}}]}",
                result, "data.structure.attributes.observation[12]");

            VerifyJObject("\"A\"", result, "data.structure.dimensions.observation[0].values[0].id");

            VerifyJObject("\"annual\"", result, "data.structure.dimensions.observation[0].values[0].name");

            //XML source: FREQ:A, ADJUSTMENT:C, OBS_VAL:55551, TITLE:series, METADATA_EN:group, METADATA_IT:null, TEST_ATTR_DIM_CODED:series, TEST_ATTR_GROUP_CODED:group
            VerifyJObject("[55551,null,null,null,null,null,null,null,null,0,0,null,0,0]", result, "data.dataSets[0].observations.0:0:0:0:0:0:0:0:0:0");
            //XML source: FREQ:A, ADJUSTMENT:C, OBS_VAL:55552, TITLE:series , METADATA_EN:group, METADATA_IT:series, TEST_ATTR_DIM_CODED:series, TEST_ATTR_GROUP_CODED:group
            VerifyJObject("[55552,null,null,null,null,null,null,null,null,0,0,0,0,0]", result, "data.dataSets[0].observations.0:1:0:0:0:0:0:0:0:0");
            //XML source: FREQ:A, ADJUSTMENT:X, OBS_VAL:55553, TITLE:null , METADATA_EN:series, METADATA_IT:null, TEST_ATTR_DIM_CODED:series, TEST_ATTR_GROUP_CODED:null
            VerifyJObject("[55553,0,null,null,null,0,null,null,0,null,2,null,0,null]", result, "data.dataSets[0].observations.0:2:0:1:0:0:0:0:0:1");
            //XML source: FREQ:A, ADJUSTMENT:X, OBS_VAL:55554, TITLE:null , METADATA_EN:null, METADATA_IT:null, TEST_ATTR_DIM_CODED:null, TEST_ATTR_GROUP_CODED:null
            VerifyJObject("[55554,0,null,null,null,0,null,null,0,null,null,null,null,null]", result, "data.dataSets[0].observations.0:3:0:1:0:0:0:0:0:2");
            //XML source: FREQ:Q, ADJUSTMENT:C, OBS_VAL:55555, TITLE:null , METADATA_EN:group, METADATA_IT:null, TEST_ATTR_DIM_CODED:series, TEST_ATTR_GROUP_CODED:series
            VerifyJObject("[55555,1,0,0,0,1,0,0,1,null,1,null,0,1]", result, "data.dataSets[0].observations.1:2:0:0:0:0:0:0:0:3");
            //XML source: FREQ:Q, ADJUSTMENT:C, OBS_VAL:55556, TITLE:null , METADATA_EN:group, METADATA_IT:null, TEST_ATTR_DIM_CODED:null, TEST_ATTR_GROUP_CODED:null
            VerifyJObject("[55556,1,0,0,0,1,0,0,1,null,1,null,null,null]", result, "data.dataSets[0].observations.1:3:0:0:0:0:0:0:0:3");
        }

        private static void ValidateAnnotationsFlat(JObject result)
        {
            var dimensions = result.SelectToken("data.structure.dimensions.observation") as JArray;
            var attributes = result.SelectToken("data.structure.attributes.observation") as JArray;
            var annotations = result.SelectToken("data.structure.annotations") as JArray;

            Assert.IsNotNull(annotations);
            Assert.IsNotNull(dimensions);
            Assert.IsNotNull(attributes);

            Assert.AreEqual(22, annotations.Count);
            Assert.AreEqual(9, dimensions.Count);
            Assert.AreEqual(3, attributes.Count);

            // dataflow level annotations

            var dfAnn = annotations.SelectToken("$[?(@.id == 'DF_ANNOTATION')]") as JObject;
            var dataSetsAnnotations = result.SelectToken("data.dataSets[0].annotations") as JArray;

            Assert.IsNotNull(dfAnn);
            Assert.IsNotNull(dataSetsAnnotations);
            Assert.AreEqual(1, dataSetsAnnotations.Count);
            Assert.AreEqual(annotations.IndexOf(dfAnn), dataSetsAnnotations[0].Value<int>());

            // component level, dimension annotations

            var annRow = annotations.SelectToken("$[?(@.id == 'ANN_ROW')]") as JObject;
            var dimAnn = annotations.SelectToken("$[?(@.id == 'DIM_ANN')]") as JObject;

            Assert.IsNotNull(annRow);
            Assert.IsNotNull(dimAnn);

            Assert.AreEqual("LAYOUT_ROW", annRow["type"].Value<string>());
            Assert.AreEqual("FULL_NAME", dimAnn["type"].Value<string>());

            var dimensionRow = dimensions.SelectToken("$[?(@.id == 'DIMENSION_ROW')]") as JObject;

            Assert.IsNotNull(dimensionRow);

            var dimensionRowAnnotations = dimensionRow["annotations"] as JArray;

            Assert.IsNotNull(dimensionRowAnnotations);
            Assert.AreEqual(2, dimensionRowAnnotations.Count);
            Assert.AreEqual(annotations.IndexOf(annRow), dimensionRowAnnotations[0].Value<int>());
            Assert.AreEqual(annotations.IndexOf(dimAnn), dimensionRowAnnotations[1].Value<int>());
        }
        /// <summary>
        /// 
        /// </summary>
        [TestCase("tests/v21/264D_264_SALDI2_ATTRIBUTE_TEST.xml", "tests/Data/test_264D_264_SALDI2_2-generic_output.series.xml", DatasetStructureReference.AllDimensions)]
        [TestCase("tests/v21/264D_264_SALDI2_ATTRIBUTE_TEST.xml", "tests/Data/test_264D_264_SALDI2_2-generic_output.series.xml", null)]
        public void TestJsonV10DataWriterEngineBestMatchLanguageTranslator(string dsdFile, string dataFile, string dimensionAtObservation)
        {
            DataType dataType = DataType.GetFromEnum(DataEnumType.Json10);
            ISdmxObjects sdmxObjects = GetSdmxObjects(dsdFile);
            IDataStructureObject dsd = sdmxObjects.DataStructures.First();

            var dataWriterManager = new DataWriterManager(new SdmxJsonDataWriterFactory());

            var fileName = dataFile.Replace(".xml",".json");
            if (dimensionAtObservation == DatasetStructureReference.AllDimensions)
            {
                fileName = fileName.Replace(".series.", ".flat.");
            }

            var preferedLanguageTranslator = new BestMatchLanguageTranslator(
                new List<CultureInfo>()
                {
                    CultureInfo.GetCultureInfo("it-IT"),
                    CultureInfo.GetCultureInfo("fr"),
                    CultureInfo.GetCultureInfo("en-US")
                },
                sdmxObjects,
                CultureInfo.GetCultureInfo("en"));

            var sdmxDataReaderFactory = new SdmxDataReaderFactory(new DataInformationManager(new FixedConceptEngine(), new ReportedDateEngine()), null);
            IReadableDataLocationFactory factory = new ReadableDataLocationFactory();

            using (var sourceData = factory.GetReadableDataLocation(new FileInfo(dataFile)))
            {
                using (var dataReaderEngine = sdmxDataReaderFactory.GetDataReaderEngine(sourceData, dsd, null))
                {
                    using (Stream writer = File.Create(fileName))
                    {
                        using (IDataWriterEngine dataWriter = dataWriterManager.GetDataWriterEngine(
                            new SdmxJsonDataFormat(new InMemorySdmxSuperObjectRetrievalManager(new SuperObjectsBuilder(), sdmxObjects),
                                dataType,
                                preferedLanguageTranslator),
                            writer))
                        {
                            dataWriter.WriteHeader(dataReaderEngine.Header);

                            if (string.IsNullOrEmpty(dimensionAtObservation))
                            {
                                dimensionAtObservation = dataReaderEngine.Header.Structures.First().DimensionAtObservation;
                            }

                            IDatasetStructureReference dsRef = new DatasetStructureReferenceCore("Test", dsd.AsReference, null, null, dimensionAtObservation); //"TIME_PERIOD"
                            IDatasetHeader datasetHeader = new DatasetHeaderCore("DS12345", DatasetActionEnumType.Information, dsRef);

                            dataWriter.StartDataset(null, dsd, datasetHeader, null);

                            //--
                            if (dataReaderEngine.MoveNextDataset())
                            {
                                foreach (var att in dataReaderEngine.DatasetAttributes)
                                {
                                    dataWriter.WriteAttributeValue(att.Concept, att.Code);
                                }

                                dataWriter.StartSeries();

                                while (dataReaderEngine.MoveNextKeyable())
                                {
                                    IKeyable currentKey = dataReaderEngine.CurrentKey;
                                    
                                    if (currentKey.Series)
                                    {
                                        foreach (var kv in currentKey.Key)
                                        {
                                            dataWriter.WriteSeriesKeyValue(kv.Concept, kv.Code);
                                        }

                                        foreach (var att in currentKey.Attributes)
                                        {
                                            dataWriter.WriteAttributeValue(att.Concept, att.Code);
                                        }

                                        while (dataReaderEngine.MoveNextObservation())
                                        {
                                            IObservation currentObservation = dataReaderEngine.CurrentObservation;
                                            
                                            dataWriter.WriteObservation(DimensionObject.TimeDimensionFixedId, currentObservation.ObsTime, currentObservation.ObservationValue);

                                            foreach (var att in currentObservation.Attributes)
                                            {
                                                dataWriter.WriteAttributeValue(att.Concept, att.Code);
                                            }
                                        }
                                    }

                                }
                            }

                            dataWriter.Close();
                        }
                    }
                }
            }

            JObject result = JObject.Parse(File.ReadAllText(fileName), new JsonLoadSettings() { LineInfoHandling = LineInfoHandling.Ignore });

            VerifyJsonResult(result);

            // Common checks
            VerifyJObject("\"Information\"", result, "data.dataSets[0].action");

            VerifyJObject("\"Condizioni economiche delle famiglie e disuguaglianze - Fiducia dei consumatori\"", result, "data.structure.name");
            VerifyJObject("\"Condizioni economiche delle famiglie e disuguaglianze - Fiducia dei consumatori\"", result, "data.structure.names.it");
            VerifyJObject("\"Households Economic Conditions and Disparities - Consumer confidence\"", result, "data.structure.names.en");

            VerifyJObject("{\"id\":\"COLLECTION\",\"name\":\"Collection\",\"names\":{\"it\":\"Collection\",\"en\":\"Collection\"},\"roles\":[\"COLLECTION\"],\"relationship\":{\"none\":{}},\"values\":[{\"id\":\"B\",\"order\":1,\"name\":\"Beginning of period\",\"names\":{\"it\":\"Beginning of period\",\"en\":\"Beginning of period\"}}]}",
                result, "data.structure.attributes.dataSet[0]");

            VerifyJObject("{\"id\":\"COMMENT\",\"name\":\"Commento\",\"names\":{\"it\":\"Commento\",\"en\":\"Comment\"},\"roles\":[\"COMMENT\"],\"relationship\":{\"primaryMeasure\":\"OBS_VALUE\"},\"values\":[{\"id\":\"This is a sample comment - 10101010\",\"name\":\"This is a sample comment - 10101010\",\"names\":{\"en\":\"This is a sample comment - 10101010\"}},{\"id\":\"This is a sample comment - 2018-Q1\",\"name\":\"This is a sample comment - 2018-Q1\",\"names\":{\"en\":\"This is a sample comment - 2018-Q1\"}}]}",
                result, "data.structure.attributes.observation[0]");

            // Format specific checks
            if (dimensionAtObservation == DatasetStructureReference.AllDimensions)
            {
                VerifyJObject("{\"id\":\"FREQ\",\"name\":\"Periodicità\",\"names\":{\"it\":\"Periodicità\",\"en\":\"Frequency\"},\"keyPosition\":0,\"roles\":[\"FREQ\"],\"values\":[{\"id\":\"A\",\"order\":0,\"name\":\"annuale\",\"names\":{\"it\":\"annuale\",\"en\":\"annual\"},\"annotations\":[0]},{\"id\":\"Q\",\"order\":6,\"name\":\"trimestrale\",\"names\":{\"it\":\"trimestrale\",\"en\":\"quarterly\"},\"annotations\":[1]}]}",
                result, "data.structure.dimensions.observation[0]");

                VerifyJObject("{\"id\":\"TITLE\",\"name\":\"Titolo\",\"names\":{\"it\":\"Titolo\",\"en\":\"Title\"},\"roles\":[\"TITLE\"],\"relationship\":{\"dimensions\":[\"FREQ\",\"ADJUSTMENT\"]},\"values\":[{\"id\":\"This is a sample TITLE\",\"name\":\"This is a sample TITLE\",\"names\":{\"en\":\"This is a sample TITLE\"}}]}",
                    result, "data.structure.attributes.observation[8]");

                VerifyJObject("\"A\"", result, "data.structure.dimensions.observation[0].values[0].id");

                VerifyJObject("\"2002\"", result, "data.structure.dimensions.observation[9].values[0].name");

                VerifyJObject("{\"en\":\"2002\"}", result, "data.structure.dimensions.observation[9].values[0].names");

            }
            else
            {
                VerifyJObject("{\"id\":\"TIME_PERIOD\",\"name\":\"Periodo di tempo\",\"names\":{\"it\":\"Periodo di tempo\",\"en\":\"Time period\"},\"keyPosition\":9,\"roles\":[\"TIME_PERIOD\"],\"values\":[{\"start\":\"2002-01-01T00:00:00\",\"end\":\"2002-12-31T23:59:59\",\"id\":\"2002\",\"name\":\"2002\",\"names\":{\"en\":\"2002\"}},{\"start\":\"2002-07-01T00:00:00\",\"end\":\"2002-09-30T00:00:00\",\"id\":\"2002-Q3\",\"name\":\"2002-Q3\",\"names\":{\"en\":\"2002-Q3\"}},{\"start\":\"2012-01-01T00:00:00\",\"end\":\"2012-12-31T23:59:59\",\"id\":\"2012\",\"name\":\"2012\",\"names\":{\"en\":\"2012\"}},{\"start\":\"2017-01-01T00:00:00\",\"end\":\"2017-12-31T23:59:59\",\"id\":\"2017\",\"name\":\"2017\",\"names\":{\"en\":\"2017\"}},{\"start\":\"2018-01-01T00:00:00\",\"end\":\"2018-12-31T23:59:59\",\"id\":\"2018\",\"name\":\"2018\",\"names\":{\"en\":\"2018\"}},{\"start\":\"2018-01-01T00:00:00\",\"end\":\"2018-03-31T00:00:00\",\"id\":\"2018-Q1\",\"name\":\"2018-Q1\",\"names\":{\"en\":\"2018-Q1\"}}]}",
                result, "data.structure.dimensions.observation[0]");

                VerifyJObject("{\"id\":\"TITLE\",\"name\":\"Titolo\",\"names\":{\"it\":\"Titolo\",\"en\":\"Title\"},\"roles\":[\"TITLE\"],\"relationship\":{\"dimensions\":[\"FREQ\",\"ADJUSTMENT\"]},\"values\":[{\"id\":\"This is a sample TITLE\",\"name\":\"This is a sample TITLE\",\"names\":{\"en\":\"This is a sample TITLE\"}}]}",
                    result, "data.structure.attributes.series[0]");

                VerifyJObject("\"2002\"", result, "data.structure.dimensions.observation[0].values[0].id");

                VerifyJObject("\"2002\"", result, "data.structure.dimensions.observation[0].values[0].name");

                VerifyJObject("{\"en\":\"2002\"}", result, "data.structure.dimensions.observation[0].values[0].names");
            }
        }

        [TestCase("tests/v21/264D_264_SALDI2_BEST_LANG_MATCH_TEST.xml", "tests/Data/test_264D_264_SALDI2_2-generic_output.series.xml", DatasetStructureReference.AllDimensions)]
        [TestCase("tests/v21/264D_264_SALDI2_BEST_LANG_MATCH_TEST.xml", "tests/Data/test_264D_264_SALDI2_2-generic_output.series.xml", null)]
        public void TestJsonV10DataWriterEngineBestMatchLanguageTranslator2(string dsdFile, string dataFile, string dimensionAtObservation)
        {
            DataType dataType = DataType.GetFromEnum(DataEnumType.Json10);
            ISdmxObjects sdmxObjects = GetSdmxObjects(dsdFile);
            IDataStructureObject dsd = sdmxObjects.DataStructures.First();

            var dataWriterManager = new DataWriterManager(new SdmxJsonDataWriterFactory());

            var fileName = dataFile.Replace(".xml", ".json");
            if (dimensionAtObservation == DatasetStructureReference.AllDimensions)
            {
                fileName = fileName.Replace(".series.", ".flat.");
            }

            var preferedLanguageTranslator = new BestMatchLanguageTranslator(
                new List<CultureInfo>()
                {
                    CultureInfo.GetCultureInfo("hu"),
                    CultureInfo.GetCultureInfo("hu-HU"),
                    CultureInfo.GetCultureInfo("fr"),
                    CultureInfo.GetCultureInfo("en-US"),
                    CultureInfo.GetCultureInfo("en-GB")
                },
                sdmxObjects,
                CultureInfo.GetCultureInfo("en"));

            var sdmxDataReaderFactory = new SdmxDataReaderFactory(new DataInformationManager(new FixedConceptEngine(), new ReportedDateEngine()), null);
            IReadableDataLocationFactory factory = new ReadableDataLocationFactory();

            using (var sourceData = factory.GetReadableDataLocation(new FileInfo(dataFile)))
            {
                using (var dataReaderEngine = sdmxDataReaderFactory.GetDataReaderEngine(sourceData, dsd, null))
                {
                    using (Stream writer = File.Create(fileName))
                    {
                        using (IDataWriterEngine dataWriter = dataWriterManager.GetDataWriterEngine(
                            new SdmxJsonDataFormat(new InMemorySdmxSuperObjectRetrievalManager(new SuperObjectsBuilder(), sdmxObjects),
                                dataType,
                                preferedLanguageTranslator),
                            writer))
                        {
                            dataWriter.WriteHeader(dataReaderEngine.Header);

                            if (string.IsNullOrEmpty(dimensionAtObservation))
                            {
                                dimensionAtObservation = dataReaderEngine.Header.Structures.First().DimensionAtObservation;
                            }

                            IDatasetStructureReference dsRef = new DatasetStructureReferenceCore("Test", dsd.AsReference, null, null, dimensionAtObservation); //"TIME_PERIOD"
                            IDatasetHeader datasetHeader = new DatasetHeaderCore("DS12345", DatasetActionEnumType.Information, dsRef);

                            dataWriter.StartDataset(null, dsd, datasetHeader, null);

                            //--
                            if (dataReaderEngine.MoveNextDataset())
                            {
                                foreach (var att in dataReaderEngine.DatasetAttributes)
                                {
                                    dataWriter.WriteAttributeValue(att.Concept, att.Code);
                                }

                                dataWriter.StartSeries();

                                while (dataReaderEngine.MoveNextKeyable())
                                {
                                    IKeyable currentKey = dataReaderEngine.CurrentKey;

                                    if (currentKey.Series)
                                    {
                                        foreach (var kv in currentKey.Key)
                                        {
                                            dataWriter.WriteSeriesKeyValue(kv.Concept, kv.Code);
                                        }

                                        foreach (var att in currentKey.Attributes)
                                        {
                                            dataWriter.WriteAttributeValue(att.Concept, att.Code);
                                        }

                                        while (dataReaderEngine.MoveNextObservation())
                                        {
                                            IObservation currentObservation = dataReaderEngine.CurrentObservation;

                                            dataWriter.WriteObservation(DimensionObject.TimeDimensionFixedId, currentObservation.ObsTime, currentObservation.ObservationValue);

                                            foreach (var att in currentObservation.Attributes)
                                            {
                                                dataWriter.WriteAttributeValue(att.Concept, att.Code);
                                            }
                                        }
                                    }

                                }
                            }

                            dataWriter.Close();
                        }
                    }
                }
            }

            JObject result = JObject.Parse(File.ReadAllText(fileName), new JsonLoadSettings() { LineInfoHandling = LineInfoHandling.Ignore });

            VerifyJsonResult(result);

            // Common checks
            VerifyJObject("\"Information\"", result, "data.dataSets[0].action");

            VerifyJObject("\"Households Economic Conditions and Disparities - Consumer confidence\"", result, "data.structure.name");

            Assert.IsNull(result.SelectToken("data.structure.names.it"));

            VerifyJObject("\"Households Economic Conditions and Disparities - Consumer confidence\"", result, "data.structure.names.en");

            VerifyJObject("{\"id\":\"COLLECTION\",\"name\":\"Collection\",\"names\":{\"en\":\"Collection\"},\"roles\":[\"COLLECTION\"],\"relationship\":{\"none\":{}},\"values\":[{\"id\":\"B\",\"order\":1,\"name\":\"Beginning of period\",\"names\":{\"en\":\"Beginning of period\"}}]}",
                result, "data.structure.attributes.dataSet[0]");

            VerifyJObject("{\"id\":\"COMMENT\",\"name\":\"Megjegyzés\",\"names\":{\"hu\":\"Megjegyzés\",\"en\":\"Comment\"},\"roles\":[\"COMMENT\"],\"relationship\":{\"primaryMeasure\":\"OBS_VALUE\"},\"values\":[{\"id\":\"This is a sample comment - 10101010\",\"name\":\"This is a sample comment - 10101010\",\"names\":{\"en\":\"This is a sample comment - 10101010\"}},{\"id\":\"This is a sample comment - 2018-Q1\",\"name\":\"This is a sample comment - 2018-Q1\",\"names\":{\"en\":\"This is a sample comment - 2018-Q1\"}}]}",
                result, "data.structure.attributes.observation[0]");

            // Format specific checks
                if (dimensionAtObservation == DatasetStructureReference.AllDimensions)
            {
                VerifyJObject("{\"id\":\"FREQ\",\"name\":\"Frequency\",\"names\":{\"en\":\"Frequency\"},\"keyPosition\":0,\"roles\":[\"FREQ\"],\"values\":[{\"id\":\"A\",\"order\":0,\"name\":\"annual\",\"names\":{\"en\":\"annual\"}},{\"id\":\"Q\",\"order\":6,\"name\":\"quarterly\",\"names\":{\"en\":\"quarterly\"}}]}",
                result, "data.structure.dimensions.observation[0]");

                VerifyJObject("{\"id\":\"TITLE\",\"name\":\"Cim\",\"names\":{\"hu\":\"Cim\",\"hu-HU\":\"Cim (HU)\",\"en\":\"Title\"},\"roles\":[\"TITLE\"],\"relationship\":{\"dimensions\":[\"FREQ\",\"ADJUSTMENT\"]},\"values\":[{\"id\":\"This is a sample TITLE\",\"name\":\"This is a sample TITLE\",\"names\":{\"en\":\"This is a sample TITLE\"}}]}",
                    result, "data.structure.attributes.observation[8]");

                VerifyJObject("\"A\"", result, "data.structure.dimensions.observation[0].values[0].id");

                VerifyJObject("\"2002\"", result, "data.structure.dimensions.observation[9].values[0].name");

                VerifyJObject("{\"en\":\"2002\"}", result, "data.structure.dimensions.observation[9].values[0].names");

            }
            else
            {
                VerifyJObject("{\"id\":\"TIME_PERIOD\",\"name\":\"Time period\",\"names\":{\"en\":\"Time period\"},\"keyPosition\":9,\"roles\":[\"TIME_PERIOD\"],\"values\":[{\"start\":\"2002-01-01T00:00:00\",\"end\":\"2002-12-31T23:59:59\",\"id\":\"2002\",\"name\":\"2002\",\"names\":{\"en\":\"2002\"}},{\"start\":\"2002-07-01T00:00:00\",\"end\":\"2002-09-30T00:00:00\",\"id\":\"2002-Q3\",\"name\":\"2002-Q3\",\"names\":{\"en\":\"2002-Q3\"}},{\"start\":\"2012-01-01T00:00:00\",\"end\":\"2012-12-31T23:59:59\",\"id\":\"2012\",\"name\":\"2012\",\"names\":{\"en\":\"2012\"}},{\"start\":\"2017-01-01T00:00:00\",\"end\":\"2017-12-31T23:59:59\",\"id\":\"2017\",\"name\":\"2017\",\"names\":{\"en\":\"2017\"}},{\"start\":\"2018-01-01T00:00:00\",\"end\":\"2018-12-31T23:59:59\",\"id\":\"2018\",\"name\":\"2018\",\"names\":{\"en\":\"2018\"}},{\"start\":\"2018-01-01T00:00:00\",\"end\":\"2018-03-31T00:00:00\",\"id\":\"2018-Q1\",\"name\":\"2018-Q1\",\"names\":{\"en\":\"2018-Q1\"}}]}",
                result, "data.structure.dimensions.observation[0]");

                VerifyJObject("{\"id\":\"TITLE\",\"name\":\"Cim\",\"names\":{\"hu\":\"Cim\",\"hu-HU\":\"Cim (HU)\",\"en\":\"Title\"},\"roles\":[\"TITLE\"],\"relationship\":{\"dimensions\":[\"FREQ\",\"ADJUSTMENT\"]},\"values\":[{\"id\":\"This is a sample TITLE\",\"name\":\"This is a sample TITLE\",\"names\":{\"en\":\"This is a sample TITLE\"}}]}",
                    result, "data.structure.attributes.series[0]");

                VerifyJObject("\"2002\"", result, "data.structure.dimensions.observation[0].values[0].id");

                VerifyJObject("\"2002\"", result, "data.structure.dimensions.observation[0].values[0].name");

                VerifyJObject("{\"en\":\"2002\"}", result, "data.structure.dimensions.observation[0].values[0].names");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [TestCase("tests/v21/264D_264_SALDI2_ATTRIBUTE_TEST.xml", "tests/Data/test_264D_264_SALDI2_2-generic_output.series.xml", DatasetStructureReference.AllDimensions)]
        [TestCase("tests/v21/264D_264_SALDI2_ATTRIBUTE_TEST.xml", "tests/Data/test_264D_264_SALDI2_2-generic_output.series.xml", null)]
        public void TestJsonV10DataWriterEngineWithPreferedLanguageTranslator(string dsdFile, string dataFile, string dimensionAtObservation)
        {
            DataType dataType = DataType.GetFromEnum(DataEnumType.Json10);
            ISdmxObjects sdmxObjects = GetSdmxObjects(dsdFile);
            IDataStructureObject dsd = sdmxObjects.DataStructures.First();

            var dataWriterManager = new DataWriterManager(new SdmxJsonDataWriterFactory());

            var fileName = dataFile.Replace(".xml", ".json");
            if (dimensionAtObservation == DatasetStructureReference.AllDimensions)
            {
                fileName = fileName.Replace(".series.", ".flat.");
            }
            var preferedLanguageTranslator = new PreferedLanguageTranslator(
                new List<CultureInfo>()
                {
                    CultureInfo.GetCultureInfo("it"),
                    CultureInfo.GetCultureInfo("fr"),
                    CultureInfo.GetCultureInfo("en")
                },
                new List<CultureInfo>()
                {
                    CultureInfo.GetCultureInfo("fr"),
                    CultureInfo.GetCultureInfo("en")
                },
                CultureInfo.GetCultureInfo("en"));

            var sdmxDataReaderFactory = new SdmxDataReaderFactory(new DataInformationManager(new FixedConceptEngine(), new ReportedDateEngine()), null);
            IReadableDataLocationFactory factory = new ReadableDataLocationFactory();

            using (var sourceData = factory.GetReadableDataLocation(new FileInfo(dataFile)))
            {
                using (var dataReaderEngine = sdmxDataReaderFactory.GetDataReaderEngine(sourceData, dsd, null))
                {
                    using (Stream writer = File.Create(fileName))
                    {
                        using (IDataWriterEngine dataWriter = dataWriterManager.GetDataWriterEngine(
                            new SdmxJsonDataFormat(new InMemorySdmxSuperObjectRetrievalManager(new SuperObjectsBuilder(), sdmxObjects),
                                dataType,
                                preferedLanguageTranslator),
                            writer))
                        {
                            dataWriter.WriteHeader(dataReaderEngine.Header);

                            if (string.IsNullOrEmpty(dimensionAtObservation))
                            {
                                dimensionAtObservation = dataReaderEngine.Header.Structures.First().DimensionAtObservation;
                            }

                            IDatasetStructureReference dsRef = new DatasetStructureReferenceCore("Test", dsd.AsReference, null, null, dimensionAtObservation); //"TIME_PERIOD"
                            IDatasetHeader datasetHeader = new DatasetHeaderCore("DS12345", DatasetActionEnumType.Information, dsRef);

                            dataWriter.StartDataset(null, dsd, datasetHeader, null);

                            //--
                            if (dataReaderEngine.MoveNextDataset())
                            {
                                foreach (var att in dataReaderEngine.DatasetAttributes)
                                {
                                    dataWriter.WriteAttributeValue(att.Concept, att.Code);
                                }

                                dataWriter.StartSeries();

                                while (dataReaderEngine.MoveNextKeyable())
                                {
                                    IKeyable currentKey = dataReaderEngine.CurrentKey;

                                    if (currentKey.Series)
                                    {
                                        foreach (var kv in currentKey.Key)
                                        {
                                            dataWriter.WriteSeriesKeyValue(kv.Concept, kv.Code);
                                        }

                                        foreach (var att in currentKey.Attributes)
                                        {
                                            dataWriter.WriteAttributeValue(att.Concept, att.Code);
                                        }

                                        while (dataReaderEngine.MoveNextObservation())
                                        {
                                            IObservation currentObservation = dataReaderEngine.CurrentObservation;

                                            dataWriter.WriteObservation(DimensionObject.TimeDimensionFixedId, currentObservation.ObsTime, currentObservation.ObservationValue);

                                            foreach (var att in currentObservation.Attributes)
                                            {
                                                dataWriter.WriteAttributeValue(att.Concept, att.Code);
                                            }
                                        }
                                    }

                                }
                            }

                            dataWriter.Close();
                        }
                    }
                }
            }

            JObject result = JObject.Parse(File.ReadAllText(fileName), new JsonLoadSettings() { LineInfoHandling = LineInfoHandling.Ignore });

            VerifyJsonResult(result);

            // Common checks
            VerifyJObject("\"Information\"", result, "data.dataSets[0].action");

            VerifyJObject("\"Households Economic Conditions and Disparities - Consumer confidence\"", result, "data.structure.name");

            VerifyJObject("{\"id\":\"COLLECTION\",\"name\":\"Collection\",\"names\":{\"en\":\"Collection\"},\"roles\":[\"COLLECTION\"],\"relationship\":{\"none\":{}},\"values\":[{\"id\":\"B\",\"order\":1,\"name\":\"Beginning of period\",\"names\":{\"en\":\"Beginning of period\"}}]}",
                result, "data.structure.attributes.dataSet[0]");

            VerifyJObject("{\"id\":\"COMMENT\",\"name\":\"Comment\",\"names\":{\"en\":\"Comment\"},\"roles\":[\"COMMENT\"],\"relationship\":{\"primaryMeasure\":\"OBS_VALUE\"},\"values\":[{\"id\":\"This is a sample comment - 10101010\",\"name\":\"This is a sample comment - 10101010\",\"names\":{\"en\":\"This is a sample comment - 10101010\"}},{\"id\":\"This is a sample comment - 2018-Q1\",\"name\":\"This is a sample comment - 2018-Q1\",\"names\":{\"en\":\"This is a sample comment - 2018-Q1\"}}]}",
                result, "data.structure.attributes.observation[0]");

            // Format specific checks
            if (dimensionAtObservation == DatasetStructureReference.AllDimensions)
            {
                VerifyJObject("{\"id\":\"FREQ\",\"name\":\"Frequency\",\"names\":{\"en\":\"Frequency\"},\"keyPosition\":0,\"roles\":[\"FREQ\"],\"values\":[{\"id\":\"A\",\"order\":0,\"name\":\"annual\",\"names\":{\"en\":\"annual\"},\"annotations\":[0]},{\"id\":\"Q\",\"order\":6,\"name\":\"quarterly\",\"names\":{\"en\":\"quarterly\"},\"annotations\":[1]}]}",
                result, "data.structure.dimensions.observation[0]");

                VerifyJObject("{\"id\":\"TITLE\",\"name\":\"Title\",\"names\":{\"en\":\"Title\"},\"roles\":[\"TITLE\"],\"relationship\":{\"dimensions\":[\"FREQ\",\"ADJUSTMENT\"]},\"values\":[{\"id\":\"This is a sample TITLE\",\"name\":\"This is a sample TITLE\",\"names\":{\"en\":\"This is a sample TITLE\"}}]}",
                    result, "data.structure.attributes.observation[8]");

                VerifyJObject("\"A\"", result, "data.structure.dimensions.observation[0].values[0].id");

                VerifyJObject("\"2002\"", result, "data.structure.dimensions.observation[9].values[0].name");

                VerifyJObject("{\"en\":\"2002\"}", result, "data.structure.dimensions.observation[9].values[0].names");
            }
            else
            {
                VerifyJObject("{\"id\":\"TIME_PERIOD\",\"name\":\"Time period\",\"names\":{\"en\":\"Time period\"},\"keyPosition\":9,\"roles\":[\"TIME_PERIOD\"],\"values\":[{\"start\":\"2002-01-01T00:00:00\",\"end\":\"2002-12-31T23:59:59\",\"id\":\"2002\",\"name\":\"2002\",\"names\":{\"en\":\"2002\"}},{\"start\":\"2002-07-01T00:00:00\",\"end\":\"2002-09-30T00:00:00\",\"id\":\"2002-Q3\",\"name\":\"2002-Q3\",\"names\":{\"en\":\"2002-Q3\"}},{\"start\":\"2012-01-01T00:00:00\",\"end\":\"2012-12-31T23:59:59\",\"id\":\"2012\",\"name\":\"2012\",\"names\":{\"en\":\"2012\"}},{\"start\":\"2017-01-01T00:00:00\",\"end\":\"2017-12-31T23:59:59\",\"id\":\"2017\",\"name\":\"2017\",\"names\":{\"en\":\"2017\"}},{\"start\":\"2018-01-01T00:00:00\",\"end\":\"2018-12-31T23:59:59\",\"id\":\"2018\",\"name\":\"2018\",\"names\":{\"en\":\"2018\"}},{\"start\":\"2018-01-01T00:00:00\",\"end\":\"2018-03-31T00:00:00\",\"id\":\"2018-Q1\",\"name\":\"2018-Q1\",\"names\":{\"en\":\"2018-Q1\"}}]}",
                result, "data.structure.dimensions.observation[0]");

                VerifyJObject("{\"id\":\"TITLE\",\"name\":\"Title\",\"names\":{\"en\":\"Title\"},\"roles\":[\"TITLE\"],\"relationship\":{\"dimensions\":[\"FREQ\",\"ADJUSTMENT\"]},\"values\":[{\"id\":\"This is a sample TITLE\",\"name\":\"This is a sample TITLE\",\"names\":{\"en\":\"This is a sample TITLE\"}}]}",
                    result, "data.structure.attributes.series[0]");

                VerifyJObject("\"2002\"", result, "data.structure.dimensions.observation[0].values[0].id");

                VerifyJObject("\"2002\"", result, "data.structure.dimensions.observation[0].values[0].name");

                VerifyJObject("{\"en\":\"2002\"}", result, "data.structure.dimensions.observation[0].values[0].names");
            }
        }

        /// <summary>
        /// Test unit for <see cref="JsonV10DataWriterEngine"/>
        /// </summary>
        /// <param name="dimensionAtObservation">
        /// The dimension at observation. When value is DatasetStructureReference.AllDimensions then output format is flat, othwerwise series.
        /// </param>
        [TestCase(DatasetStructureReference.AllDimensions)]
        [TestCase(null)]
        public void TestJsonV10DataWriterEngineMultiDataset(string dimensionAtObservation)
        {
            string outFileName = $"multi-dataset-{(dimensionAtObservation == DatasetStructureReference.AllDimensions ? "flat" : "series")}.json";

            FileInfo outJson = new FileInfo(outFileName);

            DataType dataType = DataType.GetFromEnum(DataEnumType.Json10);

            var sdmxObjects = GetSdmxObjects("tests/v21/Structure/test-sdmxv2.1-ESTAT+STS+2.0.xml");
            // The DSD definition does not define representation of primary measure, so the default "string" text type gets applied
            IDataStructureObject dataStructureObject = sdmxObjects.DataStructures.First();

            var dataWriterManager = new DataWriterManager(new SdmxJsonDataWriterFactory());
            var startTime = new DateTime(2012, 1, 1);

            var preferedLanguageTranslator = new PreferedLanguageTranslator(
                new List<CultureInfo>()
                {
                    CultureInfo.GetCultureInfo("en")
                },
                new List<CultureInfo>()
                {
                    CultureInfo.GetCultureInfo("en")
                },
                CultureInfo.GetCultureInfo("en"));

            using (Stream writer = outJson.Create())
            {
                using (IDataWriterEngine dataWriter = dataWriterManager.GetDataWriterEngine(
                    new SdmxJsonDataFormat(
                        new InMemorySdmxSuperObjectRetrievalManager(new SuperObjectsBuilder(), sdmxObjects),
                        dataType,
                        preferedLanguageTranslator),
                    writer))
                {
                    IDatasetStructureReference dsRef = new DatasetStructureReferenceCore("Test", dataStructureObject.AsReference, null, null, dimensionAtObservation); //"TIME_PERIOD"

                    var header = new HeaderImpl("IDTEST", "ESTAT");
                    dataWriter.WriteHeader(header);

                    IDatasetHeader dataSetHeader = new DatasetHeaderCore("UPDATE", DatasetAction.GetFromEnum(DatasetActionEnumType.Replace), dsRef);
                    dataWriter.StartDataset(null, dataStructureObject, dataSetHeader);
                    dataWriter.StartSeries();
                    dataWriter.WriteSeriesKeyValue("FREQ", "M");
                    dataWriter.WriteSeriesKeyValue("REF_AREA", "DE");
                    dataWriter.WriteSeriesKeyValue("ADJUSTMENT", "N");
                    dataWriter.WriteSeriesKeyValue("STS_INDICATOR", "PROD");
                    dataWriter.WriteSeriesKeyValue("STS_ACTIVITY", "NS0030");
                    dataWriter.WriteSeriesKeyValue("STS_INSTITUTION", "1");
                    dataWriter.WriteSeriesKeyValue("STS_BASE_YEAR", "2000");
                    var getPeriod1 = GetPeriodFunc("M", startTime);
                    dataWriter.WriteAttributeValue("TIME_FORMAT", GetTimeFormat("M"));

                    for (int i = 0; i < 5; i++)
                    {
                        string period = getPeriod1(i);
                        dataWriter.WriteObservation(DimensionObject.TimeDimensionFixedId, period, i.ToString(CultureInfo.InvariantCulture));
                        dataWriter.WriteAttributeValue("OBS_STATUS", "A");
                    }

                    IDatasetHeader dataSetHeaderDelete = new DatasetHeaderCore("DELETE", DatasetAction.GetFromEnum(DatasetActionEnumType.Delete), dsRef);
                    dataWriter.StartDataset(null, dataStructureObject, dataSetHeaderDelete);
                    dataWriter.StartSeries();
                    dataWriter.WriteSeriesKeyValue("FREQ", "A");
                    dataWriter.WriteSeriesKeyValue("REF_AREA", "DE");
                    dataWriter.WriteSeriesKeyValue("ADJUSTMENT", "N");
                    dataWriter.WriteSeriesKeyValue("STS_INDICATOR", "PROD");
                    dataWriter.WriteSeriesKeyValue("STS_ACTIVITY", "NS0030");
                    dataWriter.WriteSeriesKeyValue("STS_INSTITUTION", "1");
                    dataWriter.WriteSeriesKeyValue("STS_BASE_YEAR", "2000");

                    dataWriter.WriteAttributeValue("TIME_FORMAT", GetTimeFormat("M"));

                    var getPeriod = GetPeriodFunc("M", new DateTime(2006, 1, 1));

                    for (int i = 0; i < 2; i++)
                    {
                        string period = getPeriod(i);
                        dataWriter.WriteObservation(DimensionObject.TimeDimensionFixedId, period, "NaN");
                    }
                }
            }

            JObject result = JObject.Parse(File.ReadAllText(outFileName), new JsonLoadSettings() { LineInfoHandling = LineInfoHandling.Ignore });

            VerifyJsonResult(result);

            // Common checks
            VerifyJObject("\"Replace\"", result, "data.dataSets[0].action");

            VerifyJObject("\"Delete\"", result, "data.dataSets[1].action");

            VerifyJObject("\"Short Term Statistics\"", result, "data.structure.name");

            // Format specific checks
            if (dimensionAtObservation == DatasetStructureReference.AllDimensions)
            {
                VerifyJObject("\"4\"", result, "data.dataSets[0].observations.0:0:0:0:0:0:0:4[0]");

                VerifyJObject("\"NaN\"", result, "data.dataSets[1].observations.0:0:0:0:0:0:0:0[0]");
            }
            else
            {
                VerifyJObject("\"4\"", result, "data.dataSets[0].series.0:0:0:0:0:0:0.observations.4[0]");

                VerifyJObject("\"NaN\"", result, "data.dataSets[1].series.0:0:0:0:0:0:0.observations.0[0]");
            }
        }

        [TestCase("OBS_TYPE_DSD_STRING", true, "String ")]
        [TestCase("OBS_TYPE_DSD_ALPHA", true, "AlphaOnly")]
        [TestCase("OBS_TYPE_DSD_ALPHANUMERIC", true, "AlphaNumeric1234")]
        [TestCase("OBS_TYPE_DSD_NUMERIC", true, "00002132132")]
        [TestCase("OBS_TYPE_DSD_BIGINTEGER", false, "12345678901234567890")]
        [TestCase("OBS_TYPE_DSD_LONG", false, "9223372036854775807")]
        [TestCase("OBS_TYPE_DSD_INTEGER", false, "2147483647")]
        [TestCase("OBS_TYPE_DSD_SHORT", false, "32767")]
        [TestCase("OBS_TYPE_DSD_DECIMAL", false, "234457.9864")]
        [TestCase("OBS_TYPE_DSD_FLOAT", false, "3.33E+38")]
        [TestCase("OBS_TYPE_DSD_DOUBLE", false, "1.79E+308")]
        [TestCase("OBS_TYPE_DSD_BOOLEAN", true, "true")]
        [TestCase("OBS_TYPE_DSD_URI", true, "http://test.sdmx.org")]
        [TestCase("OBS_TYPE_DSD_COUNT", false, "24")]
        [TestCase("OBS_TYPE_DSD_INCLUSIVEVALUERANGE", false, "334457.9864")]
        [TestCase("OBS_TYPE_DSD_EXCLUSIVEVALUERANGE", false, "434457.9864")]
        [TestCase("OBS_TYPE_DSD_INCREMENTAL", false, "78")]
        [TestCase("OBS_TYPE_DSD_OBSERVATIONALTIMEPERIOD", true, "")]
        [TestCase("OBS_TYPE_DSD_STANDARDTIMEPERIOD", true, "2020-09-10Z")]
        [TestCase("OBS_TYPE_DSD_BASICTIMEPERIOD", true, "2020-09-10Z")]
        [TestCase("OBS_TYPE_DSD_GREGORIANTIMEPERIOD", true, "2020-09-10Z")]
        [TestCase("OBS_TYPE_DSD_GREGORIANYEAR", true, "2020Z")]
        [TestCase("OBS_TYPE_DSD_GREGORIANYEARMONTH", true, "2020-09Z")]
        [TestCase("OBS_TYPE_DSD_GREGORIANDAY", true, "2020-09-10Z")]
        [TestCase("OBS_TYPE_DSD_REPORTINGTIMEPERIOD", true, "")]
        [TestCase("OBS_TYPE_DSD_REPORTINGYEAR", true, "2020-A1")]
        [TestCase("OBS_TYPE_DSD_REPORTINGSEMESTER", true, "2020-S2")]
        [TestCase("OBS_TYPE_DSD_REPORTINGTRIMESTER", true, "2020-T3")]
        [TestCase("OBS_TYPE_DSD_REPORTINGQUARTER", true, "2020-Q3")]
        [TestCase("OBS_TYPE_DSD_REPORTINGMONTH", true, "2020-M09")]
        [TestCase("OBS_TYPE_DSD_REPORTINGWEEK", true, "2020-W37")]
        [TestCase("OBS_TYPE_DSD_REPORTINGDAY", true, "2020-D254")]
        [TestCase("OBS_TYPE_DSD_DATETIME", true, "2020-09-10T14:13:12Z")]
        [TestCase("OBS_TYPE_DSD_TIMERANGE", true, "2020-09-10T14:13:12Z/P1Y2M3DT4H5M6.7S")]
        [TestCase("OBS_TYPE_DSD_MONTH", true, "--09Z")]
        [TestCase("OBS_TYPE_DSD_MONTHDAY", true, "--09-10Z")]
        [TestCase("OBS_TYPE_DSD_DAY", true, "---10Z")]
        [TestCase("OBS_TYPE_DSD_TIME", true, "14:13:12Z")]
        [TestCase("OBS_TYPE_DSD_DURATION", true, "-P1Y2M3DT4H5M6.7S")]
        [TestCase("OBS_TYPE_DSD_CODED", true, "3C")]
        [TestCase("OBS_TYPE_DSD_INTEGER", false, "")]
        [TestCase("OBS_TYPE_DSD_STRING", true, "")]
        [TestCase("OBS_TYPE_DSD_INTEGER", false, null)]
        [TestCase("OBS_TYPE_DSD_STRING", true, null)]
        public void TestJsonV10DataWriterEngineMeasureTypes(string dsdId, bool isStringType, string observationValue)
        {
            var sdmxObjects = GetSdmxObjects("tests/v21/Structure/test-sdmxv2.1-MeasureTypes.xml");

            var preferedLanguageTranslator = new PreferedLanguageTranslator(
                new List<CultureInfo>() {CultureInfo.GetCultureInfo("en")},
                new List<CultureInfo>() {CultureInfo.GetCultureInfo("en")},
                CultureInfo.GetCultureInfo("en"));

            var dataWriterManager = new DataWriterManager(new SdmxJsonDataWriterFactory());

            // Series format
            TestMeasureType(dsdId, isStringType, observationValue, sdmxObjects, dataWriterManager, preferedLanguageTranslator, null);
            
            // Flat format
            TestMeasureType(dsdId, isStringType, observationValue, sdmxObjects, dataWriterManager, preferedLanguageTranslator, DatasetStructureReference.AllDimensions);
        }

        private void TestMeasureType(string dsdId, bool isStringType, string observationValue, ISdmxObjects sdmxObjects, DataWriterManager dataWriterManager, PreferedLanguageTranslator preferedLanguageTranslator,
            string dimensionAtObservation)
        {
            var isFlat = (dimensionAtObservation == DatasetStructureReference.AllDimensions);

            var dataStructureObject = sdmxObjects.DataStructures.FirstOrDefault(dsd => dsd.Id.Equals(dsdId));

            string outFileName = $"{dsdId}-measure-type-{(dimensionAtObservation == DatasetStructureReference.AllDimensions ? "flat" : "series")}.json";

            FileInfo outJson = new FileInfo(outFileName);

            var noOfObservations = 2;

            using (Stream writer = outJson.Create())
            {
                using (IDataWriterEngine dataWriter = dataWriterManager.GetDataWriterEngine(
                    new SdmxJsonDataFormat(
                        new InMemorySdmxSuperObjectRetrievalManager(new SuperObjectsBuilder(), sdmxObjects),
                        DataType.GetFromEnum(DataEnumType.Json10),
                        preferedLanguageTranslator),
                    writer))
                {
                    IDatasetStructureReference dsRef = new DatasetStructureReferenceCore("Test", dataStructureObject.AsReference, null, null, dimensionAtObservation); //"TIME_PERIOD"

                    var header = new HeaderImpl("IDTEST", "ESTAT");
                    dataWriter.WriteHeader(header);

                    IDatasetHeader dataSetHeader = new DatasetHeaderCore("UPDATE", DatasetAction.GetFromEnum(DatasetActionEnumType.Replace), dsRef);
                    dataWriter.StartDataset(null, dataStructureObject, dataSetHeader);
                    dataWriter.StartSeries();
                    dataWriter.WriteSeriesKeyValue("FREQ", "A");
                    var getPeriod = GetPeriodFunc("M", new DateTime(2006, 1, 1));

                    for (int i = 0; i < noOfObservations; i++)
                    {
                        string period = getPeriod(i);
                        dataWriter.WriteObservation(DimensionObject.TimeDimensionFixedId, period, observationValue);
                    }
                }
            }

            JObject result = null;

            Assert.DoesNotThrow(() => { result = JObject.Parse(File.ReadAllText(outFileName), new JsonLoadSettings() {LineInfoHandling = LineInfoHandling.Ignore}); });

            VerifyJsonResult(result);

            // When string-type, null and empty string is differentiated, when non-string then empty string should be written as "null" in json data message
            var expectedResult = isStringType ? observationValue == null ? "null" : $"\"{observationValue}\"" : string.IsNullOrEmpty(observationValue) ? "null" : observationValue;

            var seriesKey = isFlat ? "observations.0:" : "series.0.observations.";

            for (int i = 0; i < noOfObservations; i++)
            {
                VerifyJObject(expectedResult, result, $"data.dataSets[0].{seriesKey}{i}[0]");
            }
        }

        /// <summary>
        /// Gets the SDMX objects.
        /// </summary>
        /// <returns>The <see cref="ISdmxObjects"/>.</returns>
        private static ISdmxObjects GetSdmxObjects(string fileName)
        {
            ISdmxObjects objects;
            var file = new FileInfo(fileName);
            IStructureParsingManager manager = new StructureParsingManager(SdmxSchemaEnumType.Null);
            using (var readable = new FileReadableDataLocation(file))
            {
                IStructureWorkspace structureWorkspace = manager.ParseStructures(readable);
                objects = structureWorkspace.GetStructureObjects(false);
            }

            return objects;
        }

        private static void VerifyJsonResult(JObject jObject)
        {
            Assert.IsNotNull(jObject.SelectToken("meta.id"));
            Assert.IsNotNull(jObject.SelectToken("meta.schema"));
            Assert.IsNotNull(jObject.SelectToken("meta.prepared").Value<DateTime>());
            Assert.AreEqual(false, jObject.SelectToken("meta.test").Value<bool>());

            VerifyJObject("\"ESTAT\"", jObject, "meta.sender.id");
        }

        private static void VerifyJObject(string expected, JObject jObject, string path)
        {
            Assert.IsNotNull(jObject.SelectToken(path));
            Assert.AreEqual(expected, jObject.SelectToken(path).ToString(Newtonsoft.Json.Formatting.None));
        }

        /// <summary>
        /// Gets the period function.
        /// </summary>
        /// <param name="key">
        /// The frequency code.
        /// </param>
        /// <param name="startTime">
        /// The start time.
        /// </param>
        /// <returns>
        /// The method that returns the period.
        /// </returns>
        private static Func<int, string> GetPeriodFunc(string key, DateTime startTime)
        {
            Func<int, string> getPeriod = null;
            switch (key)
            {
                case "Q":
                    getPeriod = i =>
                    {
                        DateTime months = startTime.AddMonths(3 * i);
                        return DateUtil.FormatDate(months, TimeFormatEnumType.QuarterOfYear);
                    };
                    break;
                case "A":
                    getPeriod = i =>
                    {
                        DateTime months = startTime.AddMonths(12 * i);
                        return DateUtil.FormatDate(months, TimeFormatEnumType.Year);
                    };
                    break;
                case "M":
                    getPeriod = i =>
                    {
                        DateTime months = startTime.AddMonths(i + 1);
                        return DateUtil.FormatDate(months, TimeFormatEnumType.Month);
                    };
                    break;
                default:
                    Assert.Fail("Test bug. Check CL_FREQ codes");
                    break;
            }

            return getPeriod;
        }

        /// <summary>
        /// Writes the time format.
        /// </summary>
        /// <param name="key">The frequency code.</param>
        /// <returns>The TIme format.</returns>
        private static string GetTimeFormat(string key)
        {
            switch (key)
            {
                case "Q":

                    return "P3M";
                case "A":

                    return "P1Y";
                case "M":

                    return "P1M";
                default:
                    Assert.Fail("Test bug. Check CL_FREQ codes");
                    break;
            }

            return null;
        }
    }
}