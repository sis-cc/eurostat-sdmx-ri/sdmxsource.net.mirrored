using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Factory;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.Api.Util;
using Org.Sdmxsource.Sdmx.DataParser.Engine;
using Org.Sdmxsource.Sdmx.DataParser.Engine.Reader;
using Org.Sdmxsource.Sdmx.DataParser.Factory;
using Org.Sdmxsource.Sdmx.DataParser.Manager;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Util.Date;
using Org.Sdmxsource.Util.Io;

namespace SdmxDataParserTests
{
    [TestFixture]
    public class TestDataReaderForStructureSpecificData
    {
        /// <summary>
        /// The _factory
        /// </summary>
        private readonly IReadableDataLocationFactory _factory;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestDataReaderEngine"/> class.
        /// </summary>
        public TestDataReaderForStructureSpecificData()
        {
            this._factory = new ReadableDataLocationFactory();
        }

        [TestCase(@"tests\v30\samples\Data - Simple Data Attributes\\ECB_EXR.xml", @"tests\v30\samples\Dataflow\dataflow.xml", @"tests\v30\samples\Data Structure Definition\EXR.xml")]
        public void TestSimpleDataAttributes(string file,string dataflow, string dsdFile)
        {
            var retrievalManager = new InMemoryRetrievalManager();
            using (IReadableDataLocation dataLocation = _factory.GetReadableDataLocation(new FileInfo(dsdFile)))
            using (IReadableDataLocation dataLocationDataflow = _factory.GetReadableDataLocation(new FileInfo(dataflow)))
            {
                IStructureParsingManager manager = new StructureParsingManager();
                var structureWorkspace = manager.ParseStructures(dataLocation);
                var structureWorkspaceDataflow = manager.ParseStructures(dataLocationDataflow);
                var structures = structureWorkspace.GetStructureObjects(false);
                structures.Merge(structureWorkspaceDataflow.GetStructureObjects(false));
                retrievalManager.SaveStructures(structures);
            }

       

            using (var sourceData = this._factory.GetReadableDataLocation(new FileInfo(file)))
            using (var dataReaderEngine = new CompactDataReaderEngine(sourceData, retrievalManager, null, null))
            {
                Assert.NotNull(dataReaderEngine.Header);
                var readerIndex = 0;
                while (dataReaderEngine.MoveNextDataset())
                {
                    while (dataReaderEngine.MoveNextKeyable())
                    {
                        //only test for first key,first observation
                        if (readerIndex == 0)
                        {
                            var currentKey = dataReaderEngine.CurrentKey;
                            //<Series FREQ="A" CURRENCY="CAD" CURRENCY_DENOM="EUR" EXR_TYPE="SP00" EXR_SUFFIX="A" TIME_FORMAT="P1Y" COLLECTION="A" DECIMALS="4" SOURCE_AGENCY="4F0" TITLE="Canadian dollar/Euro" TITLE_COMPL="ECB reference exchange rate, Canadian dollar/Euro, 2:15 pm (C.E.T.)" UNIT="CAD" UNIT_MULT="0">
                            Assert.AreEqual(currentKey.GetKeyValue("FREQ"), "A");
                            Assert.AreEqual(currentKey.GetKeyValue("CURRENCY"), "CAD");
                            Assert.AreEqual(currentKey.GetKeyValue("CURRENCY_DENOM"), "EUR");
                            Assert.AreEqual(currentKey.GetKeyValue("EXR_TYPE"), "SP00");
                            Assert.AreEqual(currentKey.GetKeyValue("EXR_SUFFIX"), "A");
                            Assert.AreEqual(currentKey.GetAttribute("TIME_FORMAT").Code, "P1Y");
                            Assert.AreEqual(currentKey.GetAttribute("COLLECTION").Code, "A");
                            Assert.AreEqual(currentKey.GetAttribute("DECIMALS").Code, "4");
                            Assert.AreEqual(currentKey.GetAttribute("SOURCE_AGENCY").Code, "4F0");
                            Assert.AreEqual(currentKey.GetAttribute("TITLE").Code, "Canadian dollar/Euro");
                            Assert.AreEqual(currentKey.GetAttribute("TITLE_COMPL").Code, "ECB reference exchange rate, Canadian dollar/Euro, 2:15 pm (C.E.T.)");
                            Assert.AreEqual(currentKey.GetAttribute("UNIT").Code, "CAD");
                            Assert.AreEqual(currentKey.GetAttribute("UNIT_MULT").Code, "0");

                            if (currentKey.Series)
                            {
                                dataReaderEngine.MoveNextObservation();
                                //<Obs TIME_PERIOD="1999" OBS_VALUE="1.583993822393823" OBS_STATUS="A" />
                                var currentObservation = dataReaderEngine.CurrentObservation;
                                Assert.AreEqual(currentObservation.ObsTime, "1999");
                                Assert.AreEqual(currentObservation.ObservationValue, "1.583993822393823");
                                Assert.AreEqual(currentObservation.GetAttribute("OBS_STATUS").Code, "A");
                                while (dataReaderEngine.MoveNextObservation())
                                {
                                    
                                }
                            }
                        }

                        readerIndex++;
                    }
                }
            }
        }


        [TestCase(@"tests\v30\samples\Data - Complex Data Attributes\ECB_EXR_CA.xml", @"tests\v30\samples\Data - Complex Data Attributes\dataflow.xml", @"tests\v30\samples\Data - Complex Data Attributes\ECB_EXR_CA_DSD.xml")]
        public void TestComplexDataAttributes(string file, string dataflow, string dsdFile)
        {
            var retrievalManager = new InMemoryRetrievalManager();
            using (IReadableDataLocation dataLocation = _factory.GetReadableDataLocation(new FileInfo(dsdFile)))
            using (IReadableDataLocation dataLocationDataflow = _factory.GetReadableDataLocation(new FileInfo(dataflow)))
            {
                IStructureParsingManager manager = new StructureParsingManager();
                var structureWorkspace = manager.ParseStructures(dataLocation);
                var structureWorkspaceDataflow = manager.ParseStructures(dataLocationDataflow);
                var structures = structureWorkspace.GetStructureObjects(false);
                structures.Merge(structureWorkspaceDataflow.GetStructureObjects(false));
                retrievalManager.SaveStructures(structures);
            }



            using (var sourceData = this._factory.GetReadableDataLocation(new FileInfo(file)))
            using (var dataReaderEngine = new CompactDataReaderEngine(sourceData, retrievalManager, null, null))
            {
                Assert.NotNull(dataReaderEngine.Header);
                var readerIndex = 0;
                while (dataReaderEngine.MoveNextDataset())
                {
                    while (dataReaderEngine.MoveNextKeyable())
                    {
                        //only test for first key,first observation
                        if (readerIndex == 0)
                        {
                            var currentKey = dataReaderEngine.CurrentKey;
                            //<Series FREQ="A" CURRENCY="CAD" CURRENCY_DENOM="EUR" EXR_TYPE="SP00" EXR_SUFFIX="A" TIME_FORMAT="P1Y" COLLECTION="A" DECIMALS="4" SOURCE_AGENCY="4F0" TITLE="Canadian dollar/Euro" TITLE_COMPL="ECB reference exchange rate, Canadian dollar/Euro, 2:15 pm (C.E.T.)" UNIT="CAD" UNIT_MULT="0">
                            Assert.AreEqual(currentKey.GetKeyValue("FREQ"), "A");
                            Assert.AreEqual(currentKey.GetKeyValue("CURRENCY"), "CAD");
                            Assert.AreEqual(currentKey.GetKeyValue("CURRENCY_DENOM"), "EUR");
                            Assert.AreEqual(currentKey.GetKeyValue("EXR_TYPE"), "SP00");
                            Assert.AreEqual(currentKey.GetKeyValue("EXR_SUFFIX"), "A");
                            Assert.AreEqual(currentKey.GetAttribute("TIME_FORMAT").Code, "P1Y");
                            Assert.AreEqual(currentKey.GetAttribute("COLLECTION").Code, "A");
                            Assert.AreEqual(currentKey.GetAttribute("DECIMALS").Code, "4");
                            Assert.AreEqual(currentKey.GetAttribute("TITLE_COMPL").Code, "ECB reference exchange rate, Canadian dollar/Euro, 2:15 pm (C.E.T.)");
                            Assert.AreEqual(currentKey.GetAttribute("UNIT").Code, "CAD");
                            Assert.AreEqual(currentKey.GetAttribute("UNIT_MULT").Code, "0");

                            Assert.AreEqual(currentKey.GetAttribute("TITLE").ComplexNodeValues.Count(), 2);
                            Assert.AreEqual(currentKey.GetAttribute("TITLE").ComplexNodeValues.ElementAt(1).TextValues.ElementAt(0).Locale, "en");
                            Assert.AreEqual(currentKey.GetAttribute("SOURCE_AGENCY").ComplexNodeValues.ElementAt(0).Code, "4F0");


                            if (currentKey.Series)
                            {
                                dataReaderEngine.MoveNextObservation();
                                var currentObservation = dataReaderEngine.CurrentObservation;
                                Assert.AreEqual(currentObservation.ObsTime, "2017");
                                Assert.AreEqual(currentObservation.ObservationValue, "1.46472274509804");
                                Assert.IsNotNull(currentObservation.GetAttribute("OBS_STATUS"));
                                Assert.AreEqual(currentObservation.GetAttribute("OBS_STATUS").ComplexNodeValues.ElementAt(0).Code, "A");

                                while (dataReaderEngine.MoveNextObservation())
                                {

                                }
                            }
                        }

                        readerIndex++;
                    }
                }
            }
        }

        [TestCase(@"tests\v30\samples\Data - Simple Data Attributes\\ECB_EXR.xml", @"tests\v30\samples\Dataflow\dataflow.xml", @"tests\v30\samples\Data Structure Definition\EXR.xml")]
        public void TestDataReaderFactory(string file, string dataflow, string dsdFile)
        {
            var retrievalManager = new InMemoryRetrievalManager();
            using (IReadableDataLocation dataLocation = _factory.GetReadableDataLocation(new FileInfo(dsdFile)))
            using (IReadableDataLocation dataLocationDataflow = _factory.GetReadableDataLocation(new FileInfo(dataflow)))
            {
                IStructureParsingManager manager = new StructureParsingManager();
                var structureWorkspace = manager.ParseStructures(dataLocation);
                var structureWorkspaceDataflow = manager.ParseStructures(dataLocationDataflow);
                var structures = structureWorkspace.GetStructureObjects(false);
                structures.Merge(structureWorkspaceDataflow.GetStructureObjects(false));
                retrievalManager.SaveStructures(structures);
            }
            using (var sourceData = this._factory.GetReadableDataLocation(new FileInfo(file)))
            using (var dataReaderEngine = new SdmxDataReaderFactory(new DataInformationManager(), null).GetDataReaderEngine(sourceData, retrievalManager))
            {
                Assert.NotNull(dataReaderEngine);

                var readerIndex = 0;
                while (dataReaderEngine.MoveNextDataset())
                {
                    while (dataReaderEngine.MoveNextKeyable())
                    {
                        //only test for first key,first observation
                        if (readerIndex == 0)
                        {
                            var currentKey = dataReaderEngine.CurrentKey;
                            //<Series FREQ="A" CURRENCY="CAD" CURRENCY_DENOM="EUR" EXR_TYPE="SP00" EXR_SUFFIX="A" TIME_FORMAT="P1Y" COLLECTION="A" DECIMALS="4" SOURCE_AGENCY="4F0" TITLE="Canadian dollar/Euro" TITLE_COMPL="ECB reference exchange rate, Canadian dollar/Euro, 2:15 pm (C.E.T.)" UNIT="CAD" UNIT_MULT="0">
                            Assert.AreEqual(currentKey.GetKeyValue("FREQ"), "A");
                            Assert.AreEqual(currentKey.GetKeyValue("CURRENCY"), "CAD");
                            Assert.AreEqual(currentKey.GetKeyValue("CURRENCY_DENOM"), "EUR");
                            Assert.AreEqual(currentKey.GetKeyValue("EXR_TYPE"), "SP00");
                            Assert.AreEqual(currentKey.GetKeyValue("EXR_SUFFIX"), "A");
                            Assert.AreEqual(currentKey.GetAttribute("TIME_FORMAT").Code, "P1Y");
                            Assert.AreEqual(currentKey.GetAttribute("COLLECTION").Code, "A");
                            Assert.AreEqual(currentKey.GetAttribute("DECIMALS").Code, "4");
                            Assert.AreEqual(currentKey.GetAttribute("SOURCE_AGENCY").Code, "4F0");
                            Assert.AreEqual(currentKey.GetAttribute("TITLE").Code, "Canadian dollar/Euro");
                            Assert.AreEqual(currentKey.GetAttribute("TITLE_COMPL").Code, "ECB reference exchange rate, Canadian dollar/Euro, 2:15 pm (C.E.T.)");
                            Assert.AreEqual(currentKey.GetAttribute("UNIT").Code, "CAD");
                            Assert.AreEqual(currentKey.GetAttribute("UNIT_MULT").Code, "0");

                            if (currentKey.Series)
                            {
                                dataReaderEngine.MoveNextObservation();
                                //<Obs TIME_PERIOD="1999" OBS_VALUE="1.583993822393823" OBS_STATUS="A" />
                                var currentObservation = dataReaderEngine.CurrentObservation;
                                Assert.AreEqual(currentObservation.ObsTime, "1999");
                                Assert.AreEqual(currentObservation.ObservationValue, "1.583993822393823");
                                Assert.AreEqual(currentObservation.GetAttribute("OBS_STATUS").Code, "A");
                                while (dataReaderEngine.MoveNextObservation())
                                {

                                }
                            }
                        }

                        readerIndex++;
                    }
                }
            }
        }

        [TestCase(@"tests\v30\samples\Confluence\DATA_DSD_NEW_FEATURE.xml", @"tests\v30\samples\Confluence\datastructure.xml")]
        public void TestMultipleMeasuresDataAttributes(string file, string structuresFile)
        {
            var retrievalManager = new InMemoryRetrievalManager();
            using (IReadableDataLocation dataLocation = _factory.GetReadableDataLocation(new FileInfo(structuresFile)))
            {
                IStructureParsingManager manager = new StructureParsingManager();
                var structureWorkspace = manager.ParseStructures(dataLocation);
                var structures = structureWorkspace.GetStructureObjects(false);
                retrievalManager.SaveStructures(structures);
            }

            using (var sourceData = this._factory.GetReadableDataLocation(new FileInfo(file)))
            using (var dataReaderEngine = new CompactDataReaderEngine(sourceData, retrievalManager, null, null))
            {
                Assert.NotNull(dataReaderEngine.Header);
                var readerIndex = 0;
                while (dataReaderEngine.MoveNextDataset())
                {
                    while (dataReaderEngine.MoveNextKeyable())
                    {
                        //only test for first key,first observation
                        if (readerIndex == 0)
                        {
                            var currentKey = dataReaderEngine.CurrentKey;
                            //<Series FREQ="A" CURRENCY="CAD" CURRENCY_DENOM="EUR" EXR_TYPE="SP00" EXR_SUFFIX="A" TIME_FORMAT="P1Y" COLLECTION="A" DECIMALS="4" SOURCE_AGENCY="4F0" TITLE="Canadian dollar/Euro" TITLE_COMPL="ECB reference exchange rate, Canadian dollar/Euro, 2:15 pm (C.E.T.)" UNIT="CAD" UNIT_MULT="0">
                            Assert.AreEqual(currentKey.GetKeyValue("FREQ"), "A");
                            Assert.AreEqual(currentKey.GetKeyValue("CURRENCY"), "CAD");
                            Assert.AreEqual(currentKey.GetAttribute("TIME_FORMAT").Code, "P1Y");

                            if (currentKey.Series)
                            {
                                dataReaderEngine.MoveNextObservation();
                                var currentObservation = dataReaderEngine.CurrentObservation;
                                Assert.AreEqual(currentObservation.GetAttribute("OBS_STATUS").ComplexNodeValues[0].Code, "A");
                                Assert.AreEqual(currentObservation.GetAttribute("OBS_COM").ComplexNodeValues[0].TextValues[0].Value, "This is a comment in English");
                                Assert.IsNotNull(currentObservation.GetMeasure("MEASURE2"));
                                Assert.AreEqual(currentObservation.GetMeasure("MEASURE2").ComplexNodeValues[0].Code, "2.3");
                                Assert.IsNotNull(currentObservation.GetMeasure("MEASURE1"));
                                Assert.AreEqual(currentObservation.GetMeasure("MEASURE1").Code, "123.456");
                                dataReaderEngine.MoveNextObservation();
                                currentObservation = dataReaderEngine.CurrentObservation;
                                Assert.AreEqual(currentObservation.GetAttribute("OBS_STATUS").ComplexNodeValues.Count(), 3);
                                Assert.AreEqual(currentObservation.GetAttribute("OBS_STATUS").ComplexNodeValues[0].Code, "N");
                                Assert.IsNotNull(currentObservation.GetMeasure("MEASURE2"));
                                Assert.AreEqual(currentObservation.GetMeasure("MEASURE2").ComplexNodeValues[0].Code, "30000");
                                Assert.IsNotNull(currentObservation.GetMeasure("OBS_VALUE"));
                                Assert.AreEqual(currentObservation.GetMeasure("OBS_VALUE").Code, "1.37058431372549");
                                // There is no primary measure in SDMX 3.0.0 formats
//                                Assert.AreEqual(currentObservation.ObservationValue, "1.37058431372549");
                                dataReaderEngine.MoveNextObservation();
                                currentObservation = dataReaderEngine.CurrentObservation;
                                Assert.AreEqual(currentObservation.GetAttribute("OBS_STATUS").ComplexNodeValues[0].Code, "M");
                                Assert.IsNotNull(currentObservation.GetMeasure("MEASURE2"));
                                Assert.AreEqual(currentObservation.GetMeasure("MEASURE2").ComplexNodeValues[0].Code, "30000");
                            }
                        }

                        readerIndex++;
                    }
                }
            }
        }
    }
}
