// -----------------------------------------------------------------------
// <copyright file="TextType.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Constants
{
    using System;
    #region Using directives

    using System.Collections.Generic;

    #endregion

    /// <summary>
    ///     Contains all the SDMX Text Types
    /// </summary>
    public enum TextEnumType
    {
        /// <summary>
        ///     Null value; Can be used to check if the value is not set;
        /// </summary>
        Null = 0,

        /// <summary>
        ///     The alpha.
        /// </summary>
        Alpha,

        /// <summary>
        ///     The alpha numeric.
        /// </summary>
        Alphanumeric,

        /// <summary>
        ///     The attachment constraint reference.
        /// </summary>
        AttachmentConstraintReference,

        /// <summary>
        ///     The basic time period.
        /// </summary>
        BasicTimePeriod,

        /// <summary>
        ///     The string.
        /// </summary>
        String, // ALPHA

        /// <summary>
        ///     The big integer.
        /// </summary>
        BigInteger, // NUM

        /// <summary>
        ///     The integer.
        /// </summary>
        Integer, // NUM

        /// <summary>
        ///     The long.
        /// </summary>
        Long, // NUM

        /// <summary>
        ///     The short.
        /// </summary>
        Short, // NUM

        /// <summary>
        ///     The decimal.
        /// </summary>
        Decimal, // NUM

        /// <summary>
        ///     The float.
        /// </summary>
        Float, // NUM

        /// <summary>
        ///     The geospatial information
        /// </summary>
        GeospatialInformation,

        /// <summary>
        ///     The double.
        /// </summary>
        Double, // NUM

        /// <summary>
        ///     The boolean.
        /// </summary>
        Boolean,

        /// <summary>
        ///     The date time.
        /// </summary>
        DateTime,

        /// <summary>
        ///     The date.
        /// </summary>
        Date,

        /// <summary>
        ///     The time.
        /// </summary>
        Time,

        /// <summary>
        ///     The year.
        /// </summary>
        Year,

        /// <summary>
        ///     The month.
        /// </summary>
        Month,

        /// <summary>
        ///     The numeric.
        /// </summary>
        Numeric,

        /// <summary>
        ///     The day.
        /// </summary>
        Day,

        /// <summary>
        ///     The month day.
        /// </summary>
        MonthDay,

        /// <summary>
        ///     The year month.
        /// </summary>
        YearMonth,

        /// <summary>
        ///     The duration.
        /// </summary>
        Duration,

        /// <summary>
        ///     The uri.
        /// </summary>
        Uri,

        /// <summary>
        ///     The timespan.
        /// </summary>
        Timespan,

        /// <summary>
        ///     The count.
        /// </summary>
        Count,

        /// <summary>
        ///     The data set reference.
        /// </summary>
        DataSetReference,

        /// <summary>
        ///     The inclusive value range.
        /// </summary>
        InclusiveValueRange,

        /// <summary>
        ///     The exclusive value range.
        /// </summary>
        ExclusiveValueRange,

        /// <summary>
        ///     The incremental.
        /// </summary>
        Incremental,

        /// <summary>
        ///     The observational time period.
        /// </summary>
        ObservationalTimePeriod,

        /// <summary>
        ///     The key values.
        /// </summary>
        KeyValues,

        /// <summary>
        ///     The time period.
        /// </summary>
        TimePeriod,

        /// <summary>
        ///     The gregorian day.
        /// </summary>
        GregorianDay,

        /// <summary>
        ///     The gregorian time period.
        /// </summary>
        GregorianTimePeriod,

        /// <summary>
        ///     The gregorian year.
        /// </summary>
        GregorianYear,

        /// <summary>
        ///     The gregorian year month.
        /// </summary>
        GregorianYearMonth,

        /// <summary>
        ///     The reporting day.
        /// </summary>
        ReportingDay,

        /// <summary>
        ///     The reporting month.
        /// </summary>
        ReportingMonth,

        /// <summary>
        ///     The reporting quarter.
        /// </summary>
        ReportingQuarter,

        /// <summary>
        ///     The reporting semester.
        /// </summary>
        ReportingSemester,

        /// <summary>
        ///     The reporting time period.
        /// </summary>
        ReportingTimePeriod,

        /// <summary>
        ///     The reporting trimester.
        /// </summary>
        ReportingTrimester,

        /// <summary>
        ///     The reporting week.
        /// </summary>
        ReportingWeek,

        /// <summary>
        ///     The reporting year.
        /// </summary>
        ReportingYear,

        /// <summary>
        ///     The standard time period.
        /// </summary>
        StandardTimePeriod,

        /// <summary>
        ///     The times range.
        /// </summary>
        TimesRange,

        /// <summary>
        ///     The identifiable reference.
        /// </summary>
        IdentifiableReference,

        /// <summary>
        ///     The XHTML.
        /// </summary>
        Xhtml
    }

    /// <summary>
    ///     The text type.
    /// </summary>
    public class TextType : BaseConstantType<TextEnumType>
    {
        /// <summary>
        ///     The _instances.
        /// </summary>
        private static readonly Dictionary<TextEnumType, TextType> Instances = new Dictionary<TextEnumType, TextType>
        {
            {
              TextEnumType.Alpha,new TextType(TextEnumType.Alpha,false, sdmx21: "Alpha")
            },
            {
              TextEnumType.Alphanumeric,new TextType(TextEnumType.Alphanumeric,false, sdmx21: "AlphaNumeric")
            },
            {
              TextEnumType.BasicTimePeriod,new TextType(TextEnumType.BasicTimePeriod,true, sdmx21: "BasicTimePeriod")
            },
            {
              TextEnumType.String,new TextType(TextEnumType.String,false, sdmx21: "String")
            },
            {
              TextEnumType.BigInteger,new TextType(TextEnumType.BigInteger,false,true, sdmx21: "BigInteger")
            },
            {
              TextEnumType.Integer,new TextType(TextEnumType.Integer,false,true, sdmx21: "Integer")
            },
            {
              TextEnumType.Long,new TextType(TextEnumType.Long,false,true, sdmx21: "Long")
            },
            {
              TextEnumType.Short,new TextType(TextEnumType.Short,false,true, sdmx21: "Short")
            },
            {
              TextEnumType.Decimal,new TextType(TextEnumType.Decimal,false,true, sdmx21: "Decimal")
            },
            {
              TextEnumType.Float,new TextType(TextEnumType.Float,false,true, sdmx21: "Float")
            },
            {
              TextEnumType.Double,new TextType(TextEnumType.Double,false,true, sdmx21: "Double")
            },
            {
              TextEnumType.Boolean,new TextType(TextEnumType.Boolean,false, sdmx21: "Boolean")
            },
            {
              TextEnumType.DateTime,new TextType(TextEnumType.DateTime,true, sdmx21: "DateTime")
            },
            {
              TextEnumType.Date,new TextType(TextEnumType.Date,false, sdmx21: "DateTime")
            },
            {
              TextEnumType.Time,new TextType(TextEnumType.Time,false, sdmx21: "Time")
            },
            {
              TextEnumType.Year,new TextType(TextEnumType.Year,false, sdmx21: "GregorianYear")
            },
            {
              TextEnumType.Month,new TextType(TextEnumType.Month,false, sdmx21: "Month")
            },
            {
              TextEnumType.Numeric,new TextType(TextEnumType.Numeric,false, sdmx21: "Numeric")
            },
            {
              TextEnumType.Day,new TextType(TextEnumType.Day,false, sdmx21: "Day")
            },
            {
              TextEnumType.MonthDay,new TextType(TextEnumType.MonthDay,false, sdmx21: "MonthDay")
            },
            {
              TextEnumType.YearMonth,new TextType(TextEnumType.YearMonth,false, sdmx21: "GregorianYearMonth")
            },
            {
              TextEnumType.Duration,new TextType(TextEnumType.Duration,false, sdmx21: "Duration")
            },
            {
              TextEnumType.Uri,new TextType(TextEnumType.Uri,false, sdmx21: "URI")
            },
            {
              TextEnumType.Timespan,new TextType(TextEnumType.Timespan,false, sdmx21: "GregorianTimePeriod")
            },
            {
              TextEnumType.Count,new TextType(TextEnumType.Count,false,true, sdmx21: "Count")
            },
            {
              TextEnumType.DataSetReference,new TextType(TextEnumType.DataSetReference,false, sdmx21: "DataSetReference")
            },
            {
              TextEnumType.InclusiveValueRange,new TextType(TextEnumType.InclusiveValueRange,false, sdmx21: "InclusiveValueRange")
            },
            {
              TextEnumType.ExclusiveValueRange,new TextType(TextEnumType.ExclusiveValueRange,false, sdmx21: "ExclusiveValueRange")
            },
            {
              TextEnumType.Incremental,new TextType(TextEnumType.Incremental,false, sdmx21: "Incremental")
            },
            {
              TextEnumType.ObservationalTimePeriod,new TextType(TextEnumType.ObservationalTimePeriod,true, sdmx21: "ObservationalTimePeriod")
            },
            {
              TextEnumType.TimePeriod,new TextType(TextEnumType.TimePeriod,false, sdmx21: "TimeRange")
            },
            {
                // In Java this is Geo
              TextEnumType.GeospatialInformation,new TextType(TextEnumType.GeospatialInformation,false, sdmx21: nameof(TextEnumType.GeospatialInformation))
            },

            {
              TextEnumType.GregorianDay,new TextType(TextEnumType.GregorianDay,true, sdmx21: "GregorianDay")
            },
            {
              TextEnumType.GregorianTimePeriod,new TextType(TextEnumType.GregorianTimePeriod,true, sdmx21: "GregorianTimePeriod")
            },
            {
              TextEnumType.GregorianYear,new TextType(TextEnumType.GregorianYear,true, sdmx21: "GregorianYear")
            },
            {
              TextEnumType.GregorianYearMonth,new TextType(TextEnumType.GregorianYearMonth,true, sdmx21: "GregorianYearMonth")
            },
            {
              TextEnumType.ReportingDay,new TextType(TextEnumType.ReportingDay,true, sdmx21: "ReportingDay")
            },
            {
              TextEnumType.ReportingMonth,new TextType(TextEnumType.ReportingMonth,true, sdmx21: "ReportingMonth")
            },
            {
              TextEnumType.ReportingQuarter,new TextType(TextEnumType.ReportingQuarter,true, sdmx21: "ReportingQuarter")
            },
            {
              TextEnumType.ReportingSemester,new TextType(TextEnumType.ReportingSemester,true, sdmx21: "ReportingSemester")
            },
            {
              TextEnumType.ReportingTimePeriod,new TextType(TextEnumType.ReportingTimePeriod,true, sdmx21: "ReportingTimePeriod")
            },
            {
              TextEnumType.ReportingTrimester,new TextType(TextEnumType.ReportingTrimester,true, sdmx21: "ReportingTrimester")
            },
            {
              TextEnumType.ReportingWeek,new TextType(TextEnumType.ReportingWeek,true, sdmx21: "ReportingWeek")
            },
            {
              TextEnumType.ReportingYear,new TextType(TextEnumType.ReportingYear,true, sdmx21: "ReportingYear")
            },
            {
              TextEnumType.StandardTimePeriod,new TextType(TextEnumType.StandardTimePeriod,true, sdmx21: "StandardTimePeriod")
            },
            {
              TextEnumType.TimesRange,new TextType(TextEnumType.TimesRange,true, sdmx21: "TimeRange")
            },
            {
              TextEnumType.Xhtml,new TextType(TextEnumType.Xhtml,false, sdmx21: "XHTML")
            },
            {
              TextEnumType.AttachmentConstraintReference,new TextType(TextEnumType.AttachmentConstraintReference,false)
            },
            {
              TextEnumType.KeyValues,new TextType(TextEnumType.KeyValues,false)
            },
            {
              TextEnumType.IdentifiableReference,new TextType(TextEnumType.IdentifiableReference,false)
            }
        };

        /// <summary>
        ///     The _is valid time dimension text type.
        /// </summary>
        private readonly bool _isValidTimeDimensionTextType;

        /// <summary>
        ///     The _is numeric text type.
        /// </summary>
        private readonly bool _isNumericTextType;

        private readonly string _sdmx21;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TextType" /> class.
        /// </summary>
        /// <param name="enumType">
        ///     The enumeration type.
        /// </param>
        /// <param name="isValidTimeDimensionTextType">
        ///     The is valid time dimension text type.
        /// </param>
        /// <param name="isNumericTextType">
        ///     The is numeric text type.
        /// </param>
        /// <param name="sdmx21"></param>
        private TextType(TextEnumType enumType, bool isValidTimeDimensionTextType, bool isNumericTextType = false, string sdmx21 = null)
            : base(enumType)
        {
            this._isValidTimeDimensionTextType = isValidTimeDimensionTextType;
            this._isNumericTextType = isNumericTextType;
            this._sdmx21 = sdmx21;
        }

        /// <summary>
        ///     Gets the values.
        /// </summary>
        public static IEnumerable<TextType> Values
        {
            get
            {
                return Instances.Values;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether is valid time dimension text type.
        /// </summary>
        public bool IsValidTimeDimensionTextType
        {
            get
            {
                return this._isValidTimeDimensionTextType;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether is numeric text type.
        /// </summary>
        public bool IsNumericTextType
        {
            get
            {
                return this._isNumericTextType;
            }
        }

        /// <summary>
        /// SDMX v2.1 Standard name
        /// </summary>
        //public string Sdmx21 { get => _sdmx21; }

        /// <summary>
        /// SDMX 3.0.0 Standard name
        /// sdmx30 is same with sdmx 21 apart from GeospatialInformation that is only on 3.0.0 and AttachmentConstraintReference that is only on 2.1
        /// so use sdmx21 for sdmx 3.0.0 too
        /// </summary>
        public string Sdmx3 { get => _sdmx21; }


        /// <summary>
        ///     Gets the instance of <see cref="TextType" /> mapped to <paramref name="enumType" />
        /// </summary>
        /// <param name="enumType">
        ///     The <c>enum</c> type
        /// </param>
        /// <returns>
        ///     the instance of <see cref="TextType" /> mapped to <paramref name="enumType" />
        /// </returns>
        public static TextType GetFromEnum(TextEnumType enumType)
        {
            TextType output;
            if (Instances.TryGetValue(enumType, out output))
            {
                return output;
            }

            return null;
        }

        /// <summary>
        /// Returns the TextType from a string. The match is insensitive.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public static TextType ParseString(string value)
        {
            foreach (TextType ts in TextType.Values)
            {
                if (ts.ToString().Equals(value, StringComparison.OrdinalIgnoreCase) 
                    || ts.Sdmx3.Equals(value, StringComparison.OrdinalIgnoreCase))
                {
                    return ts;
                }
            }

            throw new ArgumentException("TextType.ParseString unknown value: " + value);
        }
    }
}
