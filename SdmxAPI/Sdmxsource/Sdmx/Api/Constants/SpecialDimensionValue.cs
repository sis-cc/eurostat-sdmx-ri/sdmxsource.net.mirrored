// -----------------------------------------------------------------------
// <copyright file="SpecialDimensionValue.cs" company="EUROSTAT">
//   Date Created : 2023-05-26
//   Copyright (c) 2012, 2023 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;

namespace Org.Sdmxsource.Sdmx.Api.Constants
{
    #region Using directives

    using System;
    using System.Collections.Generic;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Exception;

    #endregion

    /// <summary>
    ///     The special dimension values.
    /// </summary>
    public enum SpecialDimensionValueEnumType
    {
        /// <summary>
        ///     Switched-off dimension value - data or metadata which is not attached to this dimension (SQL notion: IS NULL).
        /// </summary>
        SwitchedOff = 0,

        /// <summary>
        ///     Wild-carded dimension value - any valid value (SQL notion: IS NOT NULL).
        /// </summary>
        WildCarded
    }

    /// <summary>
    ///     The special dimension value.
    /// </summary>
    public class SpecialDimensionValue : BaseConstantType<SpecialDimensionValueEnumType>
    {
        /// <summary>
        ///     The _instances.
        /// </summary>
        private static readonly Dictionary<SpecialDimensionValueEnumType, SpecialDimensionValue> _instances =
            new Dictionary<SpecialDimensionValueEnumType, SpecialDimensionValue>
                {
                    {
                        SpecialDimensionValueEnumType.SwitchedOff, 
                        new SpecialDimensionValue(
                            SpecialDimensionValueEnumType.SwitchedOff, 
                            "~")
                    },
                    {
                        SpecialDimensionValueEnumType.WildCarded,
                        new SpecialDimensionValue(
                            SpecialDimensionValueEnumType.WildCarded,
                            "*")
                    }
                };

        /// <summary>
        ///     The dimension value.
        /// </summary>
        private readonly string _dimensionValue;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SpecialDimensionValue" /> class.
        /// </summary>
        /// <param name="enumType">
        ///     The enumeration type
        /// </param>
        /// <param name="dimensionValue">
        ///     The dimension value
        /// </param>
        private SpecialDimensionValue(SpecialDimensionValueEnumType enumType, string dimensionValue)
            : base(enumType)
        {
            this._dimensionValue = dimensionValue;
        }

        /// <summary>
        ///     Gets the instances of <see cref="SpecialDimensionValue" />
        /// </summary>
        public static IEnumerable<SpecialDimensionValue> Values => _instances.Values;

        /// <summary>
        ///     Gets the dimension value.
        /// </summary>
        public string DimensionValue => this._dimensionValue;

        /// <summary>
        ///     Gets the instance of <see cref="SpecialDimensionValue" /> mapped to <paramref name="enumType" />
        /// </summary>
        /// <param name="enumType">
        ///     The <c>enum</c> type
        /// </param>
        /// <returns>
        ///     the instance of <see cref="SpecialDimensionValue" /> mapped to <paramref name="enumType" />
        /// </returns>
        public static SpecialDimensionValue GetFromEnum(SpecialDimensionValueEnumType enumType)
        {
            return _instances.TryGetValue(enumType, out var output) ? output : null;
        }

        /// <summary>
        ///     Parses the string.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        ///     The <see cref="value" /> .
        /// </returns>
        /// <exception cref="Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException">
        ///     Unknown Parameter  + value +  allowed
        ///     parameters:  string builder
        /// </exception>
        /// <exception cref="SdmxSemmanticException">Throws Validation Exception</exception>
        public static SpecialDimensionValue ParseString(string value)
        {
            foreach (var specialDimensionValue in Values)
            {
                if (specialDimensionValue.DimensionValue.Equals(value, StringComparison.OrdinalIgnoreCase))
                {
                    return specialDimensionValue;
                }
            }

            var sb = new StringBuilder();
            var concat = string.Empty;

            foreach (var specialDimensionValue in Values)
            {
                sb.Append(concat + specialDimensionValue.DimensionValue);
                concat = ", ";
            }

            throw new SdmxSemmanticException("Unknown Parameter " + value + " allowed parameters: " + sb);
        }

        private static HashSet<string> _specialDimensionValues = new HashSet<string>(Values.Select(x => x.DimensionValue));
        
        /// <summary>
        /// Is special dimension value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsSpecialDimensionValue(string value) => _specialDimensionValues.Contains(value);
    }
}