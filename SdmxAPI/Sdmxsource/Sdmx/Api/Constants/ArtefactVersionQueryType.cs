// -----------------------------------------------------------------------
// <copyright file="ArtefactVersionQueryType.cs" company="EUROSTAT">
//   Date Created : 2021-07-23
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Api.Constants
{
    using System.Collections.Generic;

    /// <summary>
    /// Contains the artefact version query types (other than specific versions).
    /// </summary>
    public enum ArtefactVersionQueryEnumType
    {
        /// <summary>
        ///     Null value; Can be used to check if the value is not set;
        /// </summary>
        Null = 0,

        /// <summary>
        /// Query for latest artefact version
        /// </summary>
        Latest,

        /// <summary>
        /// Query for stable artefact version
        /// </summary>
        Stable
    }

    /// <summary>
    /// Class to handle <see cref="ArtefactVersionQueryEnumType"/>
    /// </summary>
    public class ArtefactVersionQueryType : BaseConstantType<ArtefactVersionQueryEnumType>
    {
        /// <summary>
        ///     The _instances.
        /// </summary>
        private static readonly Dictionary<ArtefactVersionQueryEnumType, ArtefactVersionQueryType> Instances =
            new Dictionary<ArtefactVersionQueryEnumType, ArtefactVersionQueryType>
                {
                    {
                        ArtefactVersionQueryEnumType.Latest,
                        new ArtefactVersionQueryType(
                        ArtefactVersionQueryEnumType.Latest,
                        "latest")
                    },
                    {
                        ArtefactVersionQueryEnumType.Stable,
                        new ArtefactVersionQueryType(
                        ArtefactVersionQueryEnumType.Stable,
                        "stable")
                    }
                };

        /// <summary>
        ///     The _type.
        /// </summary>
        private readonly string _versionType;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ArtefactVersionQueryType" /> class.
        /// </summary>
        /// <param name="enumType">The enumeration type.</param>
        /// <param name="versionType">The type.</param>
        private ArtefactVersionQueryType(ArtefactVersionQueryEnumType enumType, string versionType)
            : base(enumType)
        {
            this._versionType = versionType;
        }

        /// <summary>
        ///     Gets the instances of <see cref="ArtefactVersionQueryType" />
        /// </summary>
        public static IEnumerable<ArtefactVersionQueryType> Values
        {
            get
            {
                return Instances.Values;
            }
        }

        /// <summary>
        ///     Gets the type.
        /// </summary>
        /// <remarks>
        ///     This is the corresponds to <c>getType()</c> in Java. Renamed to <c>VersionType</c> to conform to <c>FxCop</c>
        ///     rule CA1721
        /// </remarks>
        public string VersionType
        {
            get
            {
                return this._versionType;
            }
        }

        /// <summary>
        ///     Gets the instance of <see cref="ArtefactVersionQueryType" /> mapped to <paramref name="enumType" />
        /// </summary>
        /// <param name="enumType">
        ///     The <c>enum</c> type
        /// </param>
        /// <returns>
        ///     the instance of <see cref="ArtefactVersionQueryType" /> mapped to <paramref name="enumType" />
        /// </returns>
        public static ArtefactVersionQueryType GetFromEnum(ArtefactVersionQueryEnumType enumType)
        {
            ArtefactVersionQueryType output;
            if (Instances.TryGetValue(enumType, out output))
            {
                return output;
            }

            return null;
        }
    }
}
