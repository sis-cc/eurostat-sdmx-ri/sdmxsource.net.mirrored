// -----------------------------------------------------------------------
// <copyright file="CsvLabels.cs" company="EUROSTAT">
//   Date Created : 2022-06-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Constants
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Exception;

    /// <summary>
    /// The csv labels enum type.
    /// </summary>
    public enum CsvLabelsEnumType
    {
        /// <summary>
        /// Id.
        /// </summary>
        Id,

        /// <summary>
        /// Name.
        /// </summary>
        Name,

        /// <summary>
        /// Both.
        /// </summary>
        Both
    }

    /// <summary>
    /// The csv labels class.
    /// </summary>
    /// <seealso cref="CsvLabelsEnumType" />
    public sealed class CsvLabels : BaseConstantType<CsvLabelsEnumType>
    {
        /// <summary>
        ///     The instances.
        /// </summary>
        private static readonly IDictionary<CsvLabelsEnumType, CsvLabels> Instances =
            new Dictionary<CsvLabelsEnumType, CsvLabels>
            {
                {CsvLabelsEnumType.Id, new CsvLabels(CsvLabelsEnumType.Id, "id")},
                {CsvLabelsEnumType.Name, new CsvLabels(CsvLabelsEnumType.Name, "name")},
                {CsvLabelsEnumType.Both, new CsvLabels(CsvLabelsEnumType.Both, "both")}
            };

        /// <summary>
        ///     The _header parameter.
        /// </summary>
        private readonly string _headerParam;

        /// <summary>
        ///     Initializes a new instance of the <see cref="CsvLabels" /> class.
        /// </summary>
        /// <param name="enumType">Type of The enumeration.</param>
        /// <param name="headerParam">The headerParam.</param>
        private CsvLabels(CsvLabelsEnumType enumType, string headerParam)
            : base(enumType)
        {
            this._headerParam = headerParam;
        }

        /// <summary>
        ///     Gets all instances
        /// </summary>
        public static IEnumerable<CsvLabels> Values => Instances.Values;

        /// <summary>
        ///     Gets the header parameter.
        /// </summary>
        /// <value>
        ///     The value.
        /// </value>
        public string HeaderParam => this._headerParam;

        /// <summary>
        ///     Gets the instance of <see cref="CsvLabels" /> mapped to <paramref name="enumType" />
        /// </summary>
        /// <param name="enumType">
        ///     The <c>enum</c> type
        /// </param>
        /// <returns>
        ///     the instance of <see cref="CsvLabels" /> mapped to <paramref name="enumType" />
        /// </returns>
        public static CsvLabels GetFromEnum(CsvLabelsEnumType enumType)
        {
            return Instances.TryGetValue(enumType, out var output) ? output : null;
        }

        /// <summary>
        ///     Parses the string.
        /// </summary>
        /// <param name="value">The string value.</param>
        /// <returns>
        ///     The <see cref="CsvLabels" /> .
        /// </returns>
        /// <exception cref="SdmxSemmanticException">Throws Validation Exception</exception>
        /// <exception cref="ArgumentNullException"><paramref name="value"/> is <see langword="null" />.</exception>
        public static CsvLabels ParseString(string value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }

            foreach (CsvLabels currentCsvLabels in Values)
            {
                if (currentCsvLabels.HeaderParam.Equals(value, StringComparison.OrdinalIgnoreCase))
                {
                    return currentCsvLabels;
                }
            }

            var sb = new StringBuilder();
            string concat = string.Empty;

            foreach (CsvLabels currentCsvLabels in Values)
            {
                sb.Append(concat + currentCsvLabels.HeaderParam);
                concat = ", ";
            }

            throw new SdmxSemmanticException("Unknown Parameter " + value + " allowed parameters: " + sb);
        }
    }
}