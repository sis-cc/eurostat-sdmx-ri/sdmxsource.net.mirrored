// -----------------------------------------------------------------------
// <copyright file="RESTQueryOperator.cs" company="EUROSTAT">
//   Date Created : 2021-08-18
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Api.Constants
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Contains the possible operators that can be used to compare a value against a query parameter.
    /// </summary>
    public enum RESTQueryOperatorEnumType
    {
        /// <summary>
        ///     Null value; Can be used to check if the value is not set;
        /// </summary>
        Null = 0,

        /// <summary>
        /// Equals
        /// </summary>
        Equals,

        /// <summary>
        /// Not equal to
        /// </summary>
        NotEqualTo,

        /// <summary>
        /// Less than
        /// </summary>
        LessThan,

        /// <summary>
        /// Less than or equal to
        /// </summary>
        LessThanOrEqualTo,

        /// <summary>
        /// Greater than
        /// </summary>
        GreaterThan,

        /// <summary>
        /// Greater than or equal to
        /// </summary>
        GreaterThanOrEqualTo,

        /// <summary>
        /// Contains
        /// </summary>
        Contains,

        /// <summary>
        /// Does not contain
        /// </summary>
        DoesNotContain,

        /// <summary>
        /// Starts with
        /// </summary>
        StartsWith,

        /// <summary>
        /// Ends with
        /// </summary>
        EndsWith
    }

    /// <summary>
    /// Class to handle <see cref="RESTQueryOperatorEnumType"/>
    /// </summary>
    public class RESTQueryOperator : BaseConstantType<RESTQueryOperatorEnumType>
    {
        /// <summary>
        ///     The _instances.
        /// </summary>
        private static readonly Dictionary<RESTQueryOperatorEnumType, RESTQueryOperator> Instances =
            new Dictionary<RESTQueryOperatorEnumType, RESTQueryOperator>
                {
                    {
                        RESTQueryOperatorEnumType.Equals,
                        new RESTQueryOperator(
                        RESTQueryOperatorEnumType.Equals,
                        "eq")
                    },
                    {
                        RESTQueryOperatorEnumType.NotEqualTo,
                        new RESTQueryOperator(
                        RESTQueryOperatorEnumType.NotEqualTo,
                        "ne")
                    },
                    {
                        RESTQueryOperatorEnumType.LessThan,
                        new RESTQueryOperator(
                        RESTQueryOperatorEnumType.LessThan,
                        "lt")
                    },
                    {
                        RESTQueryOperatorEnumType.LessThanOrEqualTo,
                        new RESTQueryOperator(
                        RESTQueryOperatorEnumType.LessThanOrEqualTo,
                        "le")
                    },
                    {
                        RESTQueryOperatorEnumType.GreaterThan,
                        new RESTQueryOperator(
                        RESTQueryOperatorEnumType.GreaterThan,
                        "gt")
                    },
                    {
                        RESTQueryOperatorEnumType.GreaterThanOrEqualTo,
                        new RESTQueryOperator(
                        RESTQueryOperatorEnumType.GreaterThanOrEqualTo,
                        "ge")
                    },
                    {
                        RESTQueryOperatorEnumType.Contains,
                        new RESTQueryOperator(
                        RESTQueryOperatorEnumType.Contains,
                        "co")
                    },
                    {
                        RESTQueryOperatorEnumType.DoesNotContain,
                        new RESTQueryOperator(
                        RESTQueryOperatorEnumType.DoesNotContain,
                        "nc")
                    },
                    {
                        RESTQueryOperatorEnumType.StartsWith,
                        new RESTQueryOperator(
                        RESTQueryOperatorEnumType.StartsWith,
                        "sw")
                    },
                    {
                        RESTQueryOperatorEnumType.EndsWith,
                        new RESTQueryOperator(
                        RESTQueryOperatorEnumType.EndsWith,
                        "ew")
                    }
                };

        /// <summary>
        ///     The _type.
        /// </summary>
        private readonly string _operatorType;

        /// <summary>
        ///     Initializes a new instance of the <see cref="RESTQueryOperator" /> class.
        /// </summary>
        /// <param name="enumType">The enumeration type.</param>
        /// <param name="operatorType">The type.</param>
        private RESTQueryOperator(RESTQueryOperatorEnumType enumType, string operatorType)
            : base(enumType)
        {
            this._operatorType = operatorType;
        }

        /// <summary>
        ///     Gets the instances of <see cref="RESTQueryOperator" />
        /// </summary>
        public static IEnumerable<RESTQueryOperator> Values
        {
            get
            {
                return Instances.Values;
            }
        }

        /// <summary>
        ///     Gets the type.
        /// </summary>
        /// <remarks>
        ///     This is the corresponds to <c>getType()</c> in Java. Renamed to <c>VersionType</c> to conform to <c>FxCop</c>
        ///     rule CA1721
        /// </remarks>
        public string OperatorType
        {
            get
            {
                return this._operatorType;
            }
        }

        /// <summary>
        ///     Gets the instance of <see cref="RESTQueryOperator" /> mapped to <paramref name="enumType" />
        /// </summary>
        /// <param name="enumType">
        ///     The <c>enum</c> type
        /// </param>
        /// <returns>
        ///     the instance of <see cref="RESTQueryOperator" /> mapped to <paramref name="enumType" />
        /// </returns>
        public static RESTQueryOperator GetFromEnum(RESTQueryOperatorEnumType enumType)
        {
            RESTQueryOperator output;
            if (Instances.TryGetValue(enumType, out output))
            {
                return output;
            }

            return null;
        }

        /// <summary>
        ///     Gets the instance of <see cref="RESTQueryOperator" /> mapped to <paramref name="operatorType" />
        /// </summary>
        /// <param name="operatorType">
        ///     The <see cref="OperatorType"/>
        /// </param>
        /// <returns>
        ///     the instance of <see cref="RESTQueryOperator" /> mapped to <paramref name="operatorType" />
        /// </returns>
        public static RESTQueryOperator GetFromOperatorType(string operatorType)
        {
            return Instances.SingleOrDefault(i => i.Value.OperatorType.Equals(operatorType, StringComparison.OrdinalIgnoreCase)).Value;
        }
    }
}
