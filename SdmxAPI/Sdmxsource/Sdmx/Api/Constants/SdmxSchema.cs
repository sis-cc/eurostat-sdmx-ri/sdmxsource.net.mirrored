// -----------------------------------------------------------------------
// <copyright file="SdmxSchema.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Constants
{
    #region Using directives

    using System.Collections.Generic;

    #endregion

    /// <summary>
    ///     Defines the different versions of the SDMX-ML schema + EDI / Delimited
    /// </summary>
    public enum SdmxSchemaEnumType
    {
        /// <summary>
        ///     Null value; Can be used to check if the value is not set;
        /// </summary>
        Null = 0, 

        /// <summary>
        ///     The version one.
        /// </summary>
        VersionOne, 

        /// <summary>
        ///     The version two.
        /// </summary>
        VersionTwo, 

        /// <summary>
        ///     The version two point one.
        /// </summary>
        VersionTwoPointOne,
        
        /// <summary>
        ///     SDMX Version 3.0.0
        /// </summary>
        VersionThree,

        /// <summary>
        ///     The EDI.
        /// </summary>
        Edi, 

        /// <summary>
        ///     The ECV. Not a SDMX Standard format.
        /// </summary>
        Ecv, 

        /// <summary>
        ///     The SDMX-CSV v1.0
        /// </summary>
        Csv,

        /// <summary>
        ///     The SDMX-CSV v2.0.
        /// </summary>
        CsvV20,

        /// <summary>
        ///     the SDMX-JSON v1.0
        /// </summary>
        Json, 

        /// <summary>
        ///     SDMX JSON 2.0.0, compatible with SDMX version 3.0.0
        /// </summary>
        JsonV20,

        /// <summary>
        ///     The XLSX Not an SDMX Standard format
        /// </summary>
        Xlsx
    }

    /// <summary>
    ///     The sdmx schema.
    /// </summary>
    public sealed class SdmxSchema : BaseConstantType<SdmxSchemaEnumType>
    {
        /// <summary>
        ///     The _instances.
        /// </summary>
        private static readonly IDictionary<SdmxSchemaEnumType, SdmxSchema> Instances =
            new Dictionary<SdmxSchemaEnumType, SdmxSchema>
                {
                    {
                        SdmxSchemaEnumType.VersionOne, 
                        new SdmxSchema(SdmxSchemaEnumType.VersionOne, true, SdmxStandard.VersionOne)
                    }, 
                    {
                        SdmxSchemaEnumType.VersionTwo, 
                        new SdmxSchema(SdmxSchemaEnumType.VersionTwo, true, SdmxStandard.VersionTwo)
                    }, 
                    {
                        SdmxSchemaEnumType.VersionTwoPointOne, 
                        new SdmxSchema(
                        SdmxSchemaEnumType.VersionTwoPointOne, 
                        true, SdmxStandard.VersionTwoPointOne)
                    },
                    {
                        SdmxSchemaEnumType.VersionThree,
                        new SdmxSchema(SdmxSchemaEnumType.VersionThree, true, SdmxStandard.VersionThree)
                    },
                    {
                        SdmxSchemaEnumType.Edi, 
                        new SdmxSchema(SdmxSchemaEnumType.Edi, false, SdmxStandard.VersionTwo)
                    }, 
                    {
                        SdmxSchemaEnumType.Ecv, 
                        new SdmxSchema(SdmxSchemaEnumType.Ecv, false, SdmxStandard.Null)
                    }, 
                    {
                        SdmxSchemaEnumType.Csv, 
                        new SdmxSchema(SdmxSchemaEnumType.Csv, false, SdmxStandard.VersionTwoPointOne)
                    },
                    {
                        SdmxSchemaEnumType.CsvV20,
                        new SdmxSchema(SdmxSchemaEnumType.CsvV20, false, SdmxStandard.VersionThree)
                    },
                    {
                        SdmxSchemaEnumType.Json, 
                        new SdmxSchema(SdmxSchemaEnumType.Json, false, SdmxStandard.VersionTwoPointOne)
                    },
                    {
                        SdmxSchemaEnumType.JsonV20,
                        new SdmxSchema(SdmxSchemaEnumType.JsonV20, false, SdmxStandard.VersionThree)
                    },
                    {
                        SdmxSchemaEnumType.Xlsx, 
                        new SdmxSchema(SdmxSchemaEnumType.Xlsx, false, SdmxStandard.Null)
                    }
                };

        /// <summary>
        ///     The _xml format.
        /// </summary>
        private readonly bool _xmlFormat;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxSchema" /> class.
        /// </summary>
        /// <param name="format">
        ///     The format.
        /// </param>
        /// <param name="xmlFormat">
        ///     The xml format.
        /// </param>
        /// <param name="sdmxStandard">The SDMX Standard</param>
        private SdmxSchema(SdmxSchemaEnumType format, bool xmlFormat, SdmxStandard sdmxStandard)
            : base(format)
        {
            this._xmlFormat = xmlFormat;
            SdmxStandard = sdmxStandard;
        }

        /// <summary>
        ///     Gets all instances
        /// </summary>
        public static IEnumerable<SdmxSchema> Values
        {
            get
            {
                return Instances.Values;
            }
        }

        /// <summary>
        /// The SDMX standard regardless of the schema or the version
        /// </summary>
        public SdmxStandard SdmxStandard { get; }

        /// <summary>
        ///     Gets the instance of <see cref="SdmxSchema" /> mapped to <paramref name="enumType" />
        /// </summary>
        /// <param name="enumType">
        ///     The <c>enum</c> type
        /// </param>
        /// <returns>
        ///     the instance of <see cref="SdmxSchema" /> mapped to <paramref name="enumType" />
        /// </returns>
        public static SdmxSchema GetFromEnum(SdmxSchemaEnumType enumType)
        {
            SdmxSchema output;
            if (Instances.TryGetValue(enumType, out output))
            {
                return output;
            }

            return null;
        }

        /// <summary>
        ///     Gets true is this SDMX_SCHEMA is representing an XML (SDMX) format.
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public bool IsXmlFormat()
        {
            return this._xmlFormat;
        }

        /// <summary>
        ///     Gets a slightly more human-readable version of the toString() method.
        /// </summary>
        /// <returns> A string representing this Enumeration </returns>
        public string ToEnglishString()
        {
            switch (this.EnumType)
            {
                case SdmxSchemaEnumType.VersionOne:
                case SdmxSchemaEnumType.VersionTwo:
                case SdmxSchemaEnumType.VersionTwoPointOne:
                case SdmxSchemaEnumType.VersionThree:
                    return "SDMX " + this;
            }

            return this.ToString();
        }

        /// <summary>
        ///     The to string.
        /// </summary>
        /// <returns>
        ///     The <see cref="string" /> .
        /// </returns>
        public override string ToString()
        {
            switch (this.EnumType)
            {
                case SdmxSchemaEnumType.VersionOne:
                    return "1.0";
                case SdmxSchemaEnumType.VersionTwo:
                    return "2.0";
                case SdmxSchemaEnumType.VersionTwoPointOne:
                    return "2.1";
                case SdmxSchemaEnumType.VersionThree:
                    return "3.0.0";
                case SdmxSchemaEnumType.Edi:
                    return "SDMX-EDI";
                case SdmxSchemaEnumType.Ecv:
                    return "ECV";
                case SdmxSchemaEnumType.Csv:
                    return "CSV";
                case SdmxSchemaEnumType.CsvV20:
                    return "CSV-V20";
                case SdmxSchemaEnumType.Json:
                    return "JSON";
                case SdmxSchemaEnumType.JsonV20:
                    return "JSONV20";
                case SdmxSchemaEnumType.Xlsx:
                    return "XLSX";
            }

            return this.EnumType.ToString();
        }
    }
}