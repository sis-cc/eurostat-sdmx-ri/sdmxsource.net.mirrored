// -----------------------------------------------------------------------
// <copyright file="VersionQueryTypeEnum.cs" company="EUROSTAT">
//   Date Created : 2022-05-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Api.Constants
{
    /// <summary>
    /// Querying for artefact versions can be made using the criteria enumerated here.
    /// </summary>
    public enum VersionQueryTypeEnum
    {
        /// <summary>
        /// the '*' in SDMX REST v2.0.0 API (SDMX 3.0.0)
        /// The 'all' keyword in SDMX REST v1.x API (SDMX 2.1)
        /// </summary>
        All,

        /// <summary>
        /// the '*+' in SDMX REST v2.0.0 API (SDMX 3.0.0)
        /// Feature not present in SDMX REST API v1.x
        /// </summary>
        AllStable,

        /// <summary>
        /// the '~' in SDMX REST v2.0.0 API (SDMX 3.0.0) (DEFAULT)
        /// The 'latest' keyword in SDMX REST v1.x API (SDMX 2.1) (DEFAULT)
        /// </summary>
        Latest,

        /// <summary>
        /// the '+' in SDMX REST v2.0.0 API (SDMX 3.0.0)
        /// Feature not present in SDMX REST API v1.x
        /// </summary>
        LatestStable
    }
}
