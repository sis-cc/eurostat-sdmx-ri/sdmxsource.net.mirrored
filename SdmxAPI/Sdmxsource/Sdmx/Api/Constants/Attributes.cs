// -----------------------------------------------------------------------
// <copyright file="Attributes.cs" company="EUROSTAT">
//   Date Created : 2021-11-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Exception;

namespace Org.Sdmxsource.Sdmx.Api.Constants
{
    using System.Collections.Generic;

    /// <summary>
    /// The attributes enum type.
    /// </summary>
    public enum AttributesEnumType
    {
        /// <summary>
        /// Dsd type.
        /// </summary>
        Dsd, 

        /// <summary>
        /// Msd type.
        /// </summary>
        Msd,

        /// <summary>
        /// Dataset type.
        /// </summary>
        Dataset,

        /// <summary>
        /// Series type.
        /// </summary>
        Series,

        /// <summary>
        /// Obs type.
        /// </summary>
        Obs,

        /// <summary>
        /// All type.
        /// </summary>
        All,

        /// <summary>
        /// None type.
        /// </summary>
        None,

        /// <summary>
        /// Specific attribute ids 
        /// </summary>
        Specific
    }

    /// <summary>
    /// The attributes class.
    /// </summary>
    /// <seealso cref="AttributesEnumType" />
    public sealed class Attributes : BaseConstantType<AttributesEnumType>
    {
        /// <summary>
        ///     The instances.
        /// </summary>
        private static readonly IDictionary<AttributesEnumType, Attributes> Instances =
            new Dictionary<AttributesEnumType, Attributes>
            {
                {AttributesEnumType.Dsd, new Attributes(AttributesEnumType.Dsd, "dsd")},
                {AttributesEnumType.Msd, new Attributes(AttributesEnumType.Msd, "msd")},
                {AttributesEnumType.Dataset, new Attributes(AttributesEnumType.Dataset, "dataset")},
                {AttributesEnumType.Series, new Attributes(AttributesEnumType.Series, "series")},
                {AttributesEnumType.Obs, new Attributes(AttributesEnumType.Obs, "obs")},
                {AttributesEnumType.All, new Attributes(AttributesEnumType.All, "all")},
                {AttributesEnumType.None, new Attributes(AttributesEnumType.None, "none")},
                {AttributesEnumType.Specific, new Attributes(AttributesEnumType.Specific, "specific")}
            };

        /// <summary>
        ///     The _rest parameter.
        /// </summary>
        private readonly string _restParam;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Attributes" /> class.
        /// </summary>
        /// <param name="enumType">Type of The enumeration.</param>
        /// <param name="restParam">The restParam.</param>
        private Attributes(AttributesEnumType enumType, string restParam)
            : base(enumType)
        {
            this._restParam = restParam;
        }

        /// <summary>
        ///     Gets all instances
        /// </summary>
        public static IEnumerable<Attributes> Values
        {
            get
            {
                return Instances.Values;
            }
        }

        /// <summary>
        ///     Gets the rest parameter.
        /// </summary>
        /// <value>
        ///     The value.
        /// </value>
        public string RestParam
        {
            get
            {
                return this._restParam;
            }
        }

        /// <summary>
        ///     Gets the instance of <see cref="Attributes" /> mapped to <paramref name="enumType" />
        /// </summary>
        /// <param name="enumType">
        ///     The <c>enum</c> type
        /// </param>
        /// <returns>
        ///     the instance of <see cref="Attributes" /> mapped to <paramref name="enumType" />
        /// </returns>
        public static Attributes GetFromEnum(AttributesEnumType enumType)
        {
            Attributes output;
            if (Instances.TryGetValue(enumType, out output))
            {
                return output;
            }

            return null;
        }

        /// <summary>
        ///     Parses the string.
        /// </summary>
        /// <param name="value">The string value.</param>
        /// <returns>
        ///     The <see cref="Attributes" /> .
        /// </returns>
        /// <exception cref="SdmxSemmanticException">Throws Validation Exception</exception>
        /// <exception cref="ArgumentNullException"><paramref name="value"/> is <see langword="null" />.</exception>
        public static Attributes ParseString(string value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }

            foreach (Attributes currentAttribute in Values)
            {
                if (currentAttribute.RestParam.Equals(value, StringComparison.OrdinalIgnoreCase))
                {
                    return currentAttribute;
                }
            }

            // must assume it is specific values
            return new Attributes(AttributesEnumType.Specific, value);
        }
    }
}