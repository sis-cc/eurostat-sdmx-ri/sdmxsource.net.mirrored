﻿// -----------------------------------------------------------------------
// <copyright file="ComponentRole.cs" company="EUROSTAT">
//   Date Created : 2016-06-08
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Api.Constants
{
    /// <summary>
    /// The SDMX v2.0 Dimensions and Attribute bool XML attributes.
    /// </summary>
    public enum ComponentRole
    {
        /// <summary>
        /// No attribute is present
        /// </summary>
        None,

        /// <summary>
        /// The SDMX v2.0 component flag <c>isCountDimension</c> or <c>isCountAttribute</c>
        /// </summary>
        Count,

        /// <summary>
        /// The SDMX v2.0 component flag <c>isNonObservationalTimeDimension</c> or <c>isNonObservationalTimeAttribute</c>
        /// </summary>
        NonObservationalTime,

        /// <summary>
        /// The SDMX v2.0 component flag <c>isIdentityDimension</c> or <c>isIdentityAttribute</c>
        /// </summary>
        Identity,

        /// <summary>
        /// The SDMX v2.0 component flag <c>isEntityDimension</c> or <c>isEntityAttribute</c>
        /// </summary>
        Entity,

        /// <summary>
        /// The SDMX v2.0 component flag <c>isFrequencyDimension</c> or <c>isFrequencyAttribute</c>
        /// </summary>
        Frequency,
    }
}