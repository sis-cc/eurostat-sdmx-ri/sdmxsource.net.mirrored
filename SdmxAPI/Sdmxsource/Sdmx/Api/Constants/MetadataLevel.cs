// -----------------------------------------------------------------------
// <copyright file="MetadataLevel.cs" company="EUROSTAT">
//   Date Created : 2022-06-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Constants
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Exception;

    /// <summary>
    /// The metadata level enum type.
    /// </summary>
    public enum MetadataLevelEnumType
    {
        /// <summary>
        /// All.
        /// </summary>
        All,

        /// <summary>
        /// Upper only.
        /// </summary>
        UpperOnly,

        /// <summary>
        /// Current only.
        /// </summary>
        CurrentOnly
    }

    /// <summary>
    /// The metadata level class.
    /// </summary>
    /// <seealso cref="MetadataLevelEnumType" />
    public sealed class MetadataLevel : BaseConstantType<MetadataLevelEnumType>
    {
        /// <summary>
        ///     The instances.
        /// </summary>
        private static readonly IDictionary<MetadataLevelEnumType, MetadataLevel> Instances =
            new Dictionary<MetadataLevelEnumType, MetadataLevel>
            {
                {MetadataLevelEnumType.All, new MetadataLevel(MetadataLevelEnumType.All, "all")},
                {MetadataLevelEnumType.UpperOnly, new MetadataLevel(MetadataLevelEnumType.UpperOnly, "upperOnly")},
                {MetadataLevelEnumType.CurrentOnly, new MetadataLevel(MetadataLevelEnumType.CurrentOnly, "currentOnly")}
            };

        /// <summary>
        ///     The _header parameter.
        /// </summary>
        private readonly string _headerParam;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataLevel" /> class.
        /// </summary>
        /// <param name="enumType">Type of The enumeration.</param>
        /// <param name="headerParam">The headerParam.</param>
        private MetadataLevel(MetadataLevelEnumType enumType, string headerParam)
            : base(enumType)
        {
            this._headerParam = headerParam;
        }

        /// <summary>
        ///     Gets all instances
        /// </summary>
        public static IEnumerable<MetadataLevel> Values => Instances.Values;

        /// <summary>
        ///     Gets the header parameter.
        /// </summary>
        /// <value>
        ///     The value.
        /// </value>
        public string HeaderParam => this._headerParam;

        /// <summary>
        ///     Gets the instance of <see cref="MetadataLevel" /> mapped to <paramref name="enumType" />
        /// </summary>
        /// <param name="enumType">
        ///     The <c>enum</c> type
        /// </param>
        /// <returns>
        ///     the instance of <see cref="MetadataLevel" /> mapped to <paramref name="enumType" />
        /// </returns>
        public static MetadataLevel GetFromEnum(MetadataLevelEnumType enumType)
        {
            return Instances.TryGetValue(enumType, out var output) ? output : null;
        }

        /// <summary>
        ///     Parses the string.
        /// </summary>
        /// <param name="value">The string value.</param>
        /// <returns>
        ///     The <see cref="MetadataLevel" /> .
        /// </returns>
        /// <exception cref="SdmxSemmanticException">Throws Validation Exception</exception>
        /// <exception cref="ArgumentNullException"><paramref name="value"/> is <see langword="null" />.</exception>
        public static MetadataLevel ParseString(string value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }

            foreach (MetadataLevel currentMetadataLevel in Values)
            {
                if (currentMetadataLevel.HeaderParam.Equals(value, StringComparison.OrdinalIgnoreCase))
                {
                    return currentMetadataLevel;
                }
            }

            var sb = new StringBuilder();
            string concat = string.Empty;

            foreach (MetadataLevel currentMetadataLevel in Values)
            {
                sb.Append(concat + currentMetadataLevel.HeaderParam);
                concat = ", ";
            }

            throw new SdmxSemmanticException("Unknown Parameter " + value + " allowed parameters: " + sb);
        }
    }
}