// -----------------------------------------------------------------------
// <copyright file="TimeFormat.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union.
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Constants
{
    #region Using directives

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    #endregion

    /// <summary>
    ///     Defines different time formats supported by SDMX-ML
    /// </summary>
    public enum TimeFormatEnumType
    {
        /// <summary>
        ///     Null value; Can be used to check if the value is not set;
        /// </summary>
        Null = 0,

        /// <summary>
        ///  The Gregorian year <c>(yyyy)</c>.
        /// </summary>
        Year,

        /// <summary>
        ///   The SDMX v2.0 half of year <c>(yyyy-Bs)</c> and SDMX v2.1 Reporting semester <c>(yyyy-Ss)</c>.
        /// </summary>
        HalfOfYear,

        /// <summary>
        ///   The SDMX v2.0 third of year and SDMX v2.1 Reporting trimester <c>(yyyy-Tt)</c>.
        /// </summary>
        ThirdOfYear,

        /// <summary>
        ///   The SDMX v2.0 quarter of year and SDMX v2.1 Reporting quarter <c>(yyyy-Qq)</c>.
        /// </summary>
        QuarterOfYear,

        /// <summary>
        ///   The Gregorian year month <c>(yyyy-mm)</c>.
        /// </summary>
        Month,

        /// <summary>
        /// The SDMX v2.1 Reporting month <c>(yyyy-Mmm)</c>
        /// </summary>
        ReportingMonth,

        /// <summary>
        /// The SDMX v2.1 Reporting year <c>(yyyy-A1)</c>
        /// </summary>
        ReportingYear,

        /// <summary>
        /// The SDMX v2.1 Reporting day <c>(yyyy-Dddd)</c>
        /// </summary>
        ReportingDay,

        /// <summary>
        /// The SDMX v2.1 Time Range: Start time and duration <c>(YYYY-MMDD(Thh:mm:ss)?/&lt;duration&gt;)</c>
        /// See also ISO 8601 Time intervals and duration. Only a subset is supported. 
        /// </summary>
        TimeRange,

        /// <summary>
        ///   The SDMX v2.0 week of year <c>(yyyy-W1-9 yyyy-W10-52)</c> and SDMX v2.1 Reporting week <c>(yyyy-Www)</c>.
        /// </summary>
        Week,

        /// <summary>
        ///   The SDMX v2.0 week of year <c>(yyyy-W1-9 yyyy-W10-52)</c> and SDMX v2.1 Reporting week <c>(yyyy-Www)</c>.
        /// </summary>
        Date,

        /// <summary>
        ///     The hour.
        /// </summary>
        Hour,

        /// <summary>
        ///     The date time <c>(ISO 8601 date time)</c>.
        /// </summary>
        DateTime
    }

    /// <summary>
    ///     Defines different time formats supported by SDMX-ML
    /// </summary>
    public class TimeFormat : BaseConstantType<TimeFormatEnumType>
    {
        /// <summary>
        ///     The _instances.
        /// </summary>
        private static readonly Dictionary<TimeFormatEnumType, TimeFormat> _instances;
       
        /// <summary>
        ///  The Gregorian year (yyyy).
        /// </summary>
        private static readonly TimeFormat _year;

        /// <summary>
        ///   The SDMX v2.0 half of year <c>(yyyy-Bs)</c> and SDMX v2.1 Reporting semester <c>(yyyy-Ss)</c>.
        /// </summary>
        private static readonly TimeFormat _halfOfYear;

        /// <summary>
        ///   The SDMX v2.0 third of year and SDMX v2.1 Reporting trimester <c>(yyyy-Tt)</c>.
        /// </summary>
        private static readonly TimeFormat _thirdOfYear;

        /// <summary>
        ///   The SDMX v2.0 quarter of year and SDMX v2.1 Reporting quarter <c>(yyyy-Qq)</c>.
        /// </summary>
        private static readonly TimeFormat _quarterOfYear;

        /// <summary>
        ///   The Gregorian year month <c>(yyyy-mm)</c>.
        /// </summary>
        private static readonly TimeFormat _month;

        /// <summary>
        ///   The SDMX v2.0 week of year <c>(yyyy-W1-9 yyyy-W10-52)</c> and SDMX v2.1 Reporting week <c>(yyyy-Www)</c>.
        /// </summary>
        private static readonly TimeFormat _week;

        /// <summary>
        ///   The SDMX v2.0 week of year <c>(yyyy-W1-9 yyyy-W10-52)</c> and SDMX v2.1 Reporting week <c>(yyyy-Www)</c>.
        /// </summary>
        private static readonly TimeFormat _date;

        /// <summary>
        ///     The hour.
        /// </summary>
        private static readonly TimeFormat _hour;

        /// <summary>
        ///     The date time <c>(ISO 8601 date time)</c>.
        /// </summary>
        private static readonly TimeFormat _dateTime;

        /// <summary>
        /// The SDMX v2.1 Reporting month <c>(yyyy-Mmm)</c>
        /// </summary>
        private static readonly TimeFormat _reportingMonth;

        /// <summary>
        /// The SDMX v2.1 Reporting year <c>(yyyy-A1)</c>
        /// </summary>
        private static readonly TimeFormat _reportingYear;

        /// <summary>
        /// The SDMX v2.1 Reporting day <c>(yyyy-Dddd)</c>
        /// </summary>
        private static readonly TimeFormat _reportingDay;

        /// <summary>
        /// The SDMX v2.1 Time Range: Start time and duration <c>(YYYY-MMDD(Thh:mm:ss)?/&lt;duration&gt;)</c>
        /// See also ISO 8601 Time intervals and duration. Only a subset is supported. 
        /// </summary>
        private static readonly TimeFormat _timeRange;

        /// <summary>
        /// The instances
        /// </summary>
        private static readonly TimeFormat[] _values;

        /// <summary>
        ///     The _frequency code.
        /// </summary>
        private readonly string _frequencyCode;

        /// <summary>
        ///     The _readable code.
        /// </summary>
        private readonly string _readableCode;

        /// <summary>
        /// Gets the format default ISO 8601 duration, it might be null
        /// </summary>
        private readonly string _isoDuration;
        
        /// <summary>
        /// Initializes static members of the <see cref="TimeFormat"/> class.
        /// </summary>
        static TimeFormat()
        {
            _year = new TimeFormat(TimeFormatEnumType.Year, "A", "Yearly", "P1Y");
            _halfOfYear = new TimeFormat(TimeFormatEnumType.HalfOfYear, "S", "Half Yearly", "P6M");
            _thirdOfYear = new TimeFormat(TimeFormatEnumType.ThirdOfYear, "T", "Trimesterly", "P4M");
            _quarterOfYear = new TimeFormat(TimeFormatEnumType.QuarterOfYear, "Q", "Quarterly", "P3M");
            _month = new TimeFormat(TimeFormatEnumType.Month, "M", "Monthly", "P1M");
            _week = new TimeFormat(TimeFormatEnumType.Week, "W", "Weekly", "P7D");
            _date = new TimeFormat(TimeFormatEnumType.Date, "D", "Daily", "P1D");
            _hour = new TimeFormat(TimeFormatEnumType.Hour, "H", "Hourly", "PT1H");
            _dateTime = new TimeFormat(TimeFormatEnumType.DateTime, "I", "Date Time", "PT1S");

            // SDMX v2.1 specific formats. They do not exist in SdmxSource proper. Do not delete after re-sync
            _reportingMonth = new TimeFormat(TimeFormatEnumType.ReportingMonth, "M", "Reporting Month", "P1M");
            _reportingYear = new TimeFormat(TimeFormatEnumType.ReportingYear, "A", "Reporting Year", "P1Y");
            _reportingDay = new TimeFormat(TimeFormatEnumType.ReportingDay, "D", "Reporting Day", "P1D");
            _timeRange = new TimeFormat(TimeFormatEnumType.TimeRange, "R", "Time Range", null);

            _values = new[] { _year, _halfOfYear, _thirdOfYear, _quarterOfYear, _month, _week, _date, _hour, _dateTime, _reportingYear, _reportingMonth, _reportingDay, _timeRange };

            _instances = _values.ToDictionary(k => k.EnumType);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="TimeFormat" /> class.
        /// </summary>
        /// <param name="enumType">
        ///     The enumeration type.
        /// </param>
        /// <param name="frequencyCode">
        ///     The frequency code.
        /// </param>
        /// <param name="readableCode">
        ///     The readable code.
        /// </param>
        /// <param name="isoDuration">
        ///    The default ISO 8601 Duration, except for Time Range
        /// </param>
        private TimeFormat(TimeFormatEnumType enumType, string frequencyCode, string readableCode, string isoDuration)
            : base(enumType)
        {
            this._frequencyCode = frequencyCode;
            this._readableCode = readableCode;
            this._isoDuration = isoDuration;
        }

        /// <summary>
        ///  Gets the Gregorian year <c>(yyyy)</c> time format.
        /// </summary>
        public static TimeFormat Year
        {
            get
            {
                return _year;
            }
        }

        /// <summary>
        /// Gets the SDMX v2.0 half of year <c>(yyyy-Bs)</c> and SDMX v2.1 Reporting semester <c>(yyyy-Ss)</c> time format.
        /// </summary>
        public static TimeFormat HalfOfYear
        {
            get
            {
                return _halfOfYear;
            }
        }

        /// <summary>
        ///   Gets the time format for SDMX v2.0 third of year and SDMX v2.1 Reporting trimester <c>(yyyy-Tt)</c>.
        /// </summary>
        public static TimeFormat ThirdOfYear
        {
            get
            {
                return _thirdOfYear;
            }
        }

        /// <summary>
        ///   Gets the time format for SDMX v2.0 quarter of year and SDMX v2.1 Reporting quarter <c>(yyyy-Qq)</c>.
        /// </summary>
        public static TimeFormat QuarterOfYear
        {
            get
            {
                return _quarterOfYear;
            }
        }

        /// <summary>
        ///   Gets the time format for Gregorian year month <c>(yyyy-mm)</c>.
        /// </summary>
        public static TimeFormat Month
        {
            get
            {
                return _month;
            }
        }

        /// <summary>
        ///   Gets the time format for SDMX v2.0 week of year <c>(yyyy-W1-9 yyyy-W10-52)</c> and SDMX v2.1 Reporting week <c>(yyyy-Www)</c>.
        /// </summary>
        public static TimeFormat Week
        {
            get
            {
                return _week;
            }
        }

        /// <summary>
        ///     Gets the time format for gregorian date <c>(yyyy-mm-dd)</c>. Distinct Point: date-time <c>(YYYY-MM-DDThh:mm:ss)</c>
        /// </summary>
        public static TimeFormat Date
        {
            get
            {
                return _date;
            }
        }

        /// <summary>
        ///     Gets the time format for hour.
        /// </summary>
        public static TimeFormat Hour
        {
            get
            {
                return _hour;
            }
        }

        /// <summary>
        ///     Gets the time format for date time <c>(ISO 8601 date time)</c>.
        /// </summary>
        public static TimeFormat DateTime
        {
            get
            {
                return _dateTime;
            }
        }

        /// <summary>
        /// Gets the time format for SDMX v2.1 Reporting month <c>(yyyy-Mmm)</c>
        /// </summary>
        public static TimeFormat ReportingMonth
        {
            get
            {
                return _reportingMonth;
            }
        }

        /// <summary>
        /// Gets the time format for SDMX v2.1 Reporting year <c>(yyyy-A1)</c>
        /// </summary>
        public static TimeFormat ReportingYear
        {
            get
            {
                return _reportingYear;
            }
        }

        /// <summary>
        /// Gets the time format for SDMX v2.1 Reporting day <c>(yyyy-Dddd)</c>
        /// </summary>
        public static TimeFormat ReportingDay
        {
            get
            {
                return _reportingDay;
            }
        }

        /// <summary>
        /// Gets the time format for SDMX v2.1 Time Range: Start time and duration <c>(YYYY-MMDD(Thh:mm:ss)?/&lt;duration&gt;)</c>
        /// See also ISO 8601 Time intervals and duration. Only a subset is supported. 
        /// </summary>
        public static TimeFormat TimeRange
        {
            get
            {
                return _timeRange;
            }
        }

        /// <summary>
        ///     Gets the values.
        /// </summary>
        public static IEnumerable<TimeFormat> Values
        {
            get
            {
                return _values;
            }
        }

        /// <summary>
        ///     Gets the Period indicator id representation of this TIME_FORMAT:
        ///     Please note this is not a unique representation
        ///     <ul>
        ///         <li>A = TimeFormat.Year</li>
        ///         <li>A = TimeFormat.ReportingYear</li>
        ///         <li>S = TimeFormat.HalfOfYear</li>
        ///         <li>T = TimeFormat.ThirdOfYear</li>
        ///         <li>Q = TimeFormat.QuarterOfYear</li>
        ///         <li>M = TimeFormat.Month</li>
        ///         <li>M = TimeFormat.ReportingMonth</li>
        ///         <li>W = TimeFormat.Week</li>
        ///         <li>D = TimeFormat.Date</li>
        ///         <li>D = TimeFormat.ReportingDate</li>
        ///         <li>H = TimeFormat.Hour</li>
        ///         <li>I = TimeFormat.DateTime</li>
        ///         <li>R = TimeFormat.TimeRange</li>
        ///     </ul>
        /// </summary>
        public string FrequencyCode
        {
            get
            {
                return this._frequencyCode;
            }
        }

        /// <summary>
        ///     Gets a human readable string representation of this time format
        /// </summary>
        public string ReadableCode
        {
            get
            {
                return this._readableCode;
            }
        }

        /// <summary>
        /// Gets the default ISO 8601 Duration. It can be null.
        /// </summary>
        public string IsoDuration
        {
            get
            {
                return this._isoDuration;
            }
        }

        /// <summary>
        ///     Gets the instance of <see cref="TimeFormat" /> mapped to <paramref name="enumType" />
        /// </summary>
        /// <param name="enumType">
        ///     The <c>enum</c> type
        /// </param>
        /// <returns>
        ///     the instance of <see cref="TimeFormat" /> mapped to <paramref name="enumType" />
        /// </returns>
        public static TimeFormat GetFromEnum(TimeFormatEnumType enumType)
        {
            TimeFormat output;
            if (_instances.TryGetValue(enumType, out output))
            {
                return output;
            }

            return null;
        }

        /// <summary>
        ///     Gets the first time format from the code Id (case sensitive) - the code ids are listed in the <see cref="FrequencyCode"/>
        /// </summary>
        /// <param name="codeId">Code Id </param>
        /// <returns>
        ///     The <see cref="TimeFormat" /> .
        /// </returns>
        public static IList<TimeFormat> GetTimeFormatsFromCodeId(string codeId)
        {
            var formats = _values.Where(t => t.FrequencyCode.Equals(codeId)).ToArray();
            if (formats.Length > 0)
            {
                return formats;
            }

            var sb = new StringBuilder();
            string concat = string.Empty;
            foreach (TimeFormat currentTimeFormat in Values)
            {
                sb.Append(concat + currentTimeFormat.FrequencyCode);
                concat = ",";
            }

            throw new ArgumentException(
                "Time format can not be found for code id : " + codeId + " allowed values are : " + sb);
        }

        /// <summary>
        ///     Gets the first time format from the code Id (case sensitive) - the code ids are listed in the <see cref="FrequencyCode"/>
        /// </summary>
        /// <param name="codeId">Code Id </param>
        /// <returns>
        ///     The <see cref="TimeFormat" /> .
        /// </returns>
        public static TimeFormat GetTimeFormatFromCodeId(string codeId)
        {
            return GetTimeFormatsFromCodeId(codeId).First();
        }
    }
}