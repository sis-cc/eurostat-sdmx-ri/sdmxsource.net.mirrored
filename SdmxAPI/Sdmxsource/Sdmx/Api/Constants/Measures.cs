// -----------------------------------------------------------------------
// <copyright file="Measures.cs" company="EUROSTAT">
//   Date Created : 2021-11-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Exception;

namespace Org.Sdmxsource.Sdmx.Api.Constants
{
    using System.Collections.Generic;

    /// <summary>
    /// The measures enum type.
    /// </summary>
    public enum MeasuresEnumType
    {
        /// <summary>
        /// All type.
        /// </summary>
        All,

        /// <summary>
        /// None type.
        /// </summary>
        None,

        /// <summary>
        /// Specific Measure ids 
        /// </summary>
        Specific
    }

    /// <summary>
    /// The measures class.
    /// </summary>
    /// <seealso cref="MeasuresEnumType" />
    public sealed class Measures : BaseConstantType<MeasuresEnumType>
    {
        /// <summary>
        ///     The instances.
        /// </summary>
        private static readonly IDictionary<MeasuresEnumType, Measures> Instances =
            new Dictionary<MeasuresEnumType, Measures>
            {
                {MeasuresEnumType.All, new Measures(MeasuresEnumType.All, "all")},
                {MeasuresEnumType.None, new Measures(MeasuresEnumType.None, "none")},
                {MeasuresEnumType.Specific, new Measures(MeasuresEnumType.Specific, "specific")}
            };

        /// <summary>
        ///     The _rest parameter.
        /// </summary>
        private readonly string _restParam;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Measures" /> class.
        /// </summary>
        /// <param name="enumType">Type of The enumeration.</param>
        /// <param name="restParam">The restParam.</param>
        private Measures(MeasuresEnumType enumType, string restParam)
            : base(enumType)
        {
            this._restParam = restParam;
        }

        /// <summary>
        ///     Gets all instances
        /// </summary>
        public static IEnumerable<Measures> Values
        {
            get
            {
                return Instances.Values;
            }
        }

        /// <summary>
        ///     Gets the rest parameter.
        /// </summary>
        /// <value>
        ///     The value.
        /// </value>
        public string RestParam
        {
            get
            {
                return this._restParam;
            }
        }

        /// <summary>
        ///     Gets the instance of <see cref="Measures" /> mapped to <paramref name="enumType" />
        /// </summary>
        /// <param name="enumType">
        ///     The <c>enum</c> type
        /// </param>
        /// <returns>
        ///     the instance of <see cref="Measures" /> mapped to <paramref name="enumType" />
        /// </returns>
        public static Measures GetFromEnum(MeasuresEnumType enumType)
        {
            Measures output;
            if (Instances.TryGetValue(enumType, out output))
            {
                return output;
            }

            return null;
        }

        /// <summary>
        ///     Parses the string.
        /// </summary>
        /// <param name="value">The string value.</param>
        /// <returns>
        ///     The <see cref="Measures" /> .
        /// </returns>
        /// <exception cref="SdmxSemmanticException">Throws Validation Exception</exception>
        /// <exception cref="ArgumentNullException"><paramref name="value"/> is <see langword="null" />.</exception>
        public static Measures ParseString(string value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }

            foreach (Measures currentMeasure in Values)
            {
                if (currentMeasure.RestParam.Equals(value, StringComparison.OrdinalIgnoreCase))
                {
                    return currentMeasure;
                }
            }          

            // assuming specific values
            return new Measures(MeasuresEnumType.Specific, value);
        }
    }
}