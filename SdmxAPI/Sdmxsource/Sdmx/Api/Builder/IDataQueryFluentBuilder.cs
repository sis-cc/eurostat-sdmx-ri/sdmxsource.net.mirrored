﻿// -----------------------------------------------------------------------
// <copyright file="IDataQueryFluentBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-03-28
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Builder
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

    /// <summary>
    ///     TODO: Update summary.
    /// </summary>
    public interface IDataQueryFluentBuilder
    {
        /// <summary>
        ///     Materialize the object construction
        /// </summary>
        /// <returns>Returns a new instance of DataQuery</returns>
        IDataQuery Build();

        /// <summary>
        ///     Initialize the object to create a new DataQuery instance
        /// </summary>
        /// <param name="dataStructure">The data structure.</param>
        /// <param name="dataflow">The dataflow.</param>
        /// <returns>The data query fluent builder</returns>
        IDataQueryFluentBuilder Initialize(IDataStructureObject dataStructure, IDataflowObject dataflow);

        /// <summary>
        ///     Add Data Providers
        /// </summary>
        /// <param name="dataProviders">The data providers.</param>
        /// <returns>The data query fluent builder</returns>
        IDataQueryFluentBuilder WithDataProviders(ISet<IDataProvider> dataProviders);

        /// <summary>
        ///     Add Data Query Detail
        /// </summary>
        /// <param name="dataQueryDetail">The data query detail.</param>
        /// <returns>The data query fluent builder</returns>
        IDataQueryFluentBuilder WithDataQueryDetail(DataQueryDetail dataQueryDetail);

        /// <summary>
        ///     Add Data Query Selection Group
        /// </summary>
        /// <param name="dataQuerySelectionGroups">The data query selection groups.</param>
        /// <returns>The data query fluent builder</returns>
        IDataQueryFluentBuilder WithDataQuerySelectionGroup(
            ICollection<IDataQuerySelectionGroup> dataQuerySelectionGroups);

        /// <summary>
        ///     Add Data Query Selections
        /// </summary>
        /// <param name="dataQuerySelections">The data query selections.</param>
        /// <returns>The data query fluent builder</returns>
        IDataQueryFluentBuilder WithDataQuerySelections(ISet<IDataQuerySelection> dataQuerySelections);

        /// <summary>
        ///     Add Date From
        /// </summary>
        /// <param name="dateFrom">The date from.</param>
        /// <returns>The data query fluent builder</returns>
        IDataQueryFluentBuilder WithDateFrom(DateTime dateFrom);

        /// <summary>
        ///     Add Date To
        /// </summary>
        /// <param name="dateTo">The date to.</param>
        /// <returns>The data query fluent builder</returns>
        IDataQueryFluentBuilder WithDateTo(DateTime dateTo);

        /// <summary>
        ///     Add Dimension Observation At
        /// </summary>
        /// <param name="dimensionAtObservation">The dimension at observation.</param>
        /// <returns>The data query fluent builder</returns>
        IDataQueryFluentBuilder WithDimensionAtObservation(string dimensionAtObservation);

        /// <summary>
        ///     Add First N Observations
        /// </summary>
        /// <param name="firstNObs">The first n observations</param>
        /// <returns>The data query fluent builder</returns>
        IDataQueryFluentBuilder WithFirstNObs(int firstNObs);

        /// <summary>
        ///     Add Last N Observations
        /// </summary>
        /// <param name="lastNObs">The last n observations</param>
        /// <returns>The data query fluent builder</returns>
        IDataQueryFluentBuilder WithLastNObs(int lastNObs);

        /// <summary>
        ///     Add Last Updated parameter
        /// </summary>
        /// <param name="lastUpdated">The last updated.</param>
        /// <returns>The data query fluent builder</returns>
        IDataQueryFluentBuilder WithLastUpdated(ISdmxDate lastUpdated);

        /// <summary>
        ///     Add Max Observations
        /// </summary>
        /// <param name="maxObs">The maximum observations</param>
        /// <returns>The data query fluent builder</returns>
        IDataQueryFluentBuilder WithMaxObservations(int maxObs);

        /// <summary>
        ///     Add Order Ascending
        /// </summary>
        /// <param name="orderAsc">if set to <c>true</c> [order ascending].</param>
        /// <returns>The data query fluent builder</returns>
        IDataQueryFluentBuilder WithOrderAsc(bool orderAsc);
    }
}