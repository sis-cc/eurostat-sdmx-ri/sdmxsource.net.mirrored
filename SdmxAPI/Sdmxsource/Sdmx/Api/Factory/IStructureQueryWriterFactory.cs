using Org.Sdmxsource.Sdmx.Api.Builder;
using Org.Sdmxsource.Sdmx.Api.Model.Format;

namespace Org.Sdmxsource.Sdmx.Api.Factory
{
    public interface IStructureQueryWriterFactory
    {

        /**
         * Returns a StructureQueryBuilder only if this factory understands the StructureQueryFormat.  If the format is unknown, null will be returned
         *
         * @param format The format with the writer
         * @return StructureQueryWriter is this factory knows how to build this query format, or null if it doesn't
         */
        IStructureQueryWriter<T> GetStructureQueryWriter<T>(IStructureQueryWithWriterFormat<T> format);
    }
}
