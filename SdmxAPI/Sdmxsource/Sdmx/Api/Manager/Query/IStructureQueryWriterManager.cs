using Org.Sdmxsource.Sdmx.Api.Model.Format;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;

namespace Org.Sdmxsource.Sdmx.Api.Manager.Query
{
    public interface IStructureQueryWriterManager
    {

        /// <summary>
        /// Builds a structure query in the requested format
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="structureQuery"></param>
        /// <param name="structureQueryFormat"></param>

        void WriteStructureQuery<T>(ICommonStructureQuery structureQuery, IStructureQueryWithWriterFormat<T> structureQueryFormat);
    }
}
