// -----------------------------------------------------------------------
// <copyright file="ISdmxSuperObjectRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Manager.Retrieval
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Registry;

    #endregion

    /// <summary>
    ///     Manages the retrieval of structures and returns the responses as SDMX SuperObjects
    /// </summary>
    public interface ISdmxSuperObjectRetrievalManager
    {
        /// <summary>
        ///     Gets a single ICategorySchemeSuperObject , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        ///     If no category scheme is found with the given reference, then null is returned
        /// </summary>
        /// <param name="reference">
        ///     The maintainable reference.
        /// </param>
        /// <returns>
        ///     The <see cref="ICategorySchemeSuperObject" /> .
        /// </returns>
        ICategorySchemeSuperObject GetCategorySchemeSuperObject(IMaintainableRefObject reference);

        /// <summary>
        ///     Gets ICategorySchemeSuperObject that match the parameters in the ref maintainableObject.  If the ref
        ///     maintainableObject is null or
        ///     has no attributes set, then this will be interpreted as a search for all ICategorySchemeSuperObject
        /// </summary>
        /// <param name="reference">
        ///     The maintainable reference.
        /// </param>
        /// <returns>
        ///     list of Objects that match the search criteria
        /// </returns>
        ISet<ICategorySchemeSuperObject> GetCategorySchemeSuperObjects(IMaintainableRefObject reference);

        /// <summary>
        ///     Gets a single ICodelistSuperObject , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        ///     If no codelist is found with the given reference, then null is returned
        /// </summary>
        /// <param name="reference">
        ///     The maintainable reference.
        /// </param>
        /// <returns>
        ///     The <see cref="ICodelistSuperObject" /> .
        /// </returns>
        ICodelistSuperObject GetCodelistSuperObject(IMaintainableRefObject reference);

        /// <summary>
        ///     Gets ICodelistSuperObject that match the parameters in the ref maintainableObject.  If the ref maintainableObject
        ///     is null or
        ///     has no attributes set, then this will be interpreted as a search for all ICodelistSuperObject
        /// </summary>
        /// <param name="reference">
        ///     The maintainable reference.
        /// </param>
        /// <returns>
        ///     list of Objects that match the search criteria
        /// </returns>
        ISet<ICodelistSuperObject> GetCodelistSuperObjects(IMaintainableRefObject reference);

        /// <summary>
        ///     Gets a single IConceptSchemeSuperObject , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        ///     If no concept scheme is found with the given reference, then null is returned
        /// </summary>
        /// <param name="reference">
        ///     The maintainable reference.
        /// </param>
        /// <returns>
        ///     The <see cref="IConceptSchemeSuperObject" /> .
        /// </returns>
        IConceptSchemeSuperObject GetConceptSchemeSuperObject(IMaintainableRefObject reference);

        /// <summary>
        ///     Gets IConceptSchemeSuperObject that match the parameters in the ref maintainableObject.  If the ref
        ///     maintainableObject is null or
        ///     has no attributes set, then this will be interpreted as a search for all ConceptSchemeSuperObjects
        /// </summary>
        /// <param name="reference">
        ///     The maintainable reference.
        /// </param>
        /// <returns>
        ///     list of Objects that match the search criteria
        /// </returns>
        ISet<IConceptSchemeSuperObject> GetConceptSchemeSuperObjects(IMaintainableRefObject reference);

        /// <summary>
        ///     Gets a single Dataflow , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        ///     If no dataflow is found with the given reference, then null is returned
        /// </summary>
        /// <param name="reference">
        ///     The maintainable reference.
        /// </param>
        /// <returns>
        ///     The <see cref="IDataflowSuperObject" /> .
        /// </returns>
        IDataflowSuperObject GetDataflowSuperObject(IMaintainableRefObject reference);

        /// <summary>
        ///     Gets IDataflowSuperObject that match the parameters in the ref maintainableObject.  If the ref maintainableObject
        ///     is null or
        ///     has no attributes set, then this will be interpreted as a search for all IDataflowSuperObject
        /// </summary>
        /// <param name="reference">
        ///     The maintainable reference.
        /// </param>
        /// <returns>
        ///     list of Objects that match the search criteria
        /// </returns>
        ISet<IDataflowSuperObject> GetDataflowSuperObjects(IMaintainableRefObject reference);

        /// <summary>
        ///     Gets a single IDataStructureSuperObject , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        ///     If no DSD is found with the given reference, then null is returned
        /// </summary>
        /// <param name="reference">
        ///     The maintainable reference.
        /// </param>
        /// <returns>
        ///     The <see cref="IDataStructureSuperObject" /> .
        /// </returns>
        IDataStructureSuperObject GetDataStructureSuperObject(IMaintainableRefObject reference);

        /// <summary>
        ///     Gets DataStructureSuperObjects that match the parameters in the ref maintainableObject.  If the ref
        ///     maintainableObject is null or
        ///     has no attributes set, then this will be interpreted as a search for all DataStructureSuperObjects.
        /// </summary>
        /// <param name="reference">
        ///     The maintainable reference.
        /// </param>
        /// <returns>
        ///     list of Objects that match the search criteria
        /// </returns>
        ISet<IDataStructureSuperObject> GetDataStructureSuperObjects(IMaintainableRefObject reference);

        /// <summary>
        ///     Gets a single HierarchicCodeList , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        ///     If no hierarchical codelist is found with the given reference, then null is returned
        /// </summary>
        /// <param name="reference">
        ///     The maintainable reference.
        /// </param>
        /// <returns>
        ///     The <see cref="IHierarchicalCodelistSuperObject" /> .
        /// </returns>
        IHierarchicalCodelistSuperObject GetHierarchicCodeListSuperObject(IMaintainableRefObject reference);

        /// <summary>
        ///     Gets IHierarchicalCodelistSuperObject that match the parameters in the ref maintainableObject.  If the ref
        ///     maintainableObject is null or
        ///     has no attributes set, then this will be interpreted as a search for all IHierarchicalCodelistSuperObject
        /// </summary>
        /// <param name="reference">
        ///     The maintainable reference.
        /// </param>
        /// <returns>
        ///     list of Objects that match the search criteria
        /// </returns>
        ISet<IHierarchicalCodelistSuperObject> GetHierarchicCodeListSuperObjects(IMaintainableRefObject reference);

        /// <summary>
        ///     Gets a single IProvisionAgreementSuperObject , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        ///     If no Provision Agreement is found with the given reference, then null is returned
        /// </summary>
        /// <param name="reference">
        ///     The maintainable reference.
        /// </param>
        /// <returns>
        ///     The <see cref="IProvisionAgreementSuperObject" /> .
        /// </returns>
        IProvisionAgreementSuperObject GetProvisionAgreementSuperObject(IMaintainableRefObject reference);

        /// <summary>
        ///     Gets ProvisionAgreementSuperObjects that match the parameters in the ref maintainableObject.  If the ref
        ///     maintainableObject is null or
        ///     has no attributes set, then this will be interpreted as a search for all ProvisionAgreementSuperObjects
        /// </summary>
        /// <param name="reference">
        ///     The maintainable reference.
        /// </param>
        /// <returns>
        ///     list of Objects that match the search criteria
        /// </returns>
        ISet<IProvisionAgreementSuperObject> GetProvisionAgreementSuperObjects(IMaintainableRefObject reference);
    }
}