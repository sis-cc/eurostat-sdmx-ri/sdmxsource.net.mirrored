// -----------------------------------------------------------------------
// <copyright file="SdmxNotAcceptableException.cs" company="EUROSTAT">
//   Date Created : 2019-01-17
//   Copyright (c) 2012, 2019 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System.Runtime.Serialization;
using Org.Sdmxsource.Sdmx.Api.Constants;

namespace Org.Sdmxsource.Sdmx.Api.Exception
{
    /// <summary>
    /// This exception represents the HTTP Status 406 Not Acceptable.
    /// The 406 (Not Acceptable) status code indicates that the target resource does not have a current representation that would be
    ///  acceptable to the user agent, according to the proactive negotiation header fields received in the request (Accept headers)
    /// </summary>
    public class SdmxNotAcceptableException : SdmxException
    {
        private static readonly SdmxErrorCode _errorCode;

        /// <summary>
        /// Initializes static members of the <see cref="SdmxNotAcceptableException"/>
        /// </summary>
        static SdmxNotAcceptableException()
        {
            _errorCode = SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.NotAcceptable);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SdmxNotAcceptableException"/> class.
        /// </summary>
        public SdmxNotAcceptableException() : base(_errorCode.ErrorString, _errorCode)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SdmxNotAcceptableException"/> class.
        /// </summary>
        /// <param name="errorMessage">The error message</param>
        public SdmxNotAcceptableException(string errorMessage) : base(errorMessage, _errorCode)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SdmxNotAcceptableException"/> class.
        /// </summary>
        /// <param name="errorMessage">The error message</param>
        /// <param name="innerException">The inner exception</param>
        public SdmxNotAcceptableException(string errorMessage, System.Exception innerException) : base(innerException, _errorCode, errorMessage)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SdmxNotAcceptableException"/> class.
        /// </summary>
        /// <param name="code">
        ///     The exception code
        /// </param>
        /// <param name="args">
        ///     The arguments to the <see cref="ExceptionCode"/>
        /// </param>
        public SdmxNotAcceptableException(ExceptionCode code, params object[] args) : base(_errorCode, code, args)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxNotAcceptableException" /> class.
        /// </summary>
        /// <param name="nestedException">
        ///     The exception
        /// </param>
        /// <param name="code">
        ///     The exception code
        /// </param>
        /// <param name="args">
        ///     The arguments
        /// </param>
        public SdmxNotAcceptableException(System.Exception nestedException,  ExceptionCode code, params object[] args) : base(nestedException, _errorCode, code, args)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxConflictException" /> class.
        /// </summary>
        /// <param name="info">
        ///     The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the
        ///     exception being thrown.
        /// </param>
        /// <param name="context">
        ///     The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the
        ///     source or destination.
        /// </param>
        /// <exception cref="T:System.ArgumentNullException">
        ///     The <paramref name="info" /> parameter is null.
        /// </exception>
        /// <exception cref="T:System.Runtime.Serialization.SerializationException">
        ///     The class name is null or <see cref="P:System.Exception.HResult" /> is zero (0).
        /// </exception>
        protected SdmxNotAcceptableException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}