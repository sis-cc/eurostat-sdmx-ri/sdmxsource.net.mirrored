using System;
using System.Collections.Generic;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace Org.Sdmxsource.Sdmx.Api.Model.Diff // Replace with your actual namespace
{
    /// <summary>
    /// The diff report.
    /// </summary>
    public interface IDiffReport
    {
        /// <summary>
        /// Gets the source.
        /// </summary>
        /// <returns>An IMaintainableObject.</returns>
        IMaintainableObject Source { get; }
        /// <summary>
        /// Gets the target.
        /// </summary>
        /// <returns>An IMaintainableObject.</returns>
        IMaintainableObject Target { get; }
        /// <summary>
        /// Adds the diff.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="property">The property.</param>
        /// <param name="sourceVal">The source val.</param>
        /// <param name="targetVal">The target val.</param>
        void AddDiff(ISdmxObject source, string property, object sourceVal, object targetVal);
        /// <summary>
        /// Gets the differences.
        /// </summary>
        /// <param name="modificationType">The modification type.</param>
        /// <returns>A SortedSet.</returns>
        ISet<IDiff> GetDifferences(ModificationAction modificationType);
        /// <summary>
        /// Gets the differences.
        /// </summary>
        /// <returns>An ISet.</returns>
        ISet<IDiff> GetDifferences();
    }
}
