using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

namespace Org.Sdmxsource.Sdmx.Api.Model.Diff
{
    /// <summary>
    /// The diff.
    /// </summary>
    public interface IDiff
    {
        /// <summary>
        /// Gets the structure reference.
        /// </summary>
        /// <returns>An IStructureReference.</returns>
        IStructureReference StructureReference { get; }

        /// <summary>
        /// Gets the property.
        /// </summary>
        /// <returns>A string.</returns>
        string Property { get; }

        /// <summary>
        /// Gets the source value.
        /// </summary>
        /// <returns>A string.</returns>
        string SourceValue { get; }

        /// <summary>
        /// Gets the target value.
        /// </summary>
        /// <returns>A string.</returns>
        string TargetValue { get; }


        /// <summary>
        /// Returns the modification, addition is where there is a source value and
        /// no target value, removal is the opposite, and modification is where the two
        /// values are different
        /// </summary>
        /// <returns></returns>
        ModificationAction Modification { get; }
    }
}
