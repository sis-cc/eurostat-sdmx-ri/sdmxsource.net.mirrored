namespace Org.Sdmxsource.Sdmx.Api.Model.Diff
{
    public enum ModificationAction
    {
        Addition,
        Removal,
        Edit
    }
}
