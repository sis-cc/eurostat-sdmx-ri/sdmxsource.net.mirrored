// -----------------------------------------------------------------------
// <copyright file="IMetadataAttributeContainer.cs" company="EUROSTAT">
//   Date Created : 2024-03-19
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure
{
    using System.Collections.Generic;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;

    /// <summary>
    /// a container of metadata attributes
    /// </summary>
    public interface IMetadataAttributeContainer
    {
        /// <summary>
        /// Gets an immutable list of metadata attributes, or an empty list if none exist
        /// </summary>
        /// <returns>An immutable list of metadata attributes</returns>
        IList<IMetadataAttributeObject> MetadataAttributes { get; }
    }

}
