// -----------------------------------------------------------------------
// <copyright file="IMetadataContainer.cs" company="EUROSTAT">
//   Date Created : 2023-05-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Api.Model.Objects
{
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;

    /// <summary> 
    /// Container for metadata (structure, reference metadata) 
    /// </summary> 
    public interface IMetadataContainer
    {
        /// <summary> 
        /// Returns a new, read-only identifier for this objects container 
        /// </summary>
        /// <returns>
        /// 
        /// </returns> 
        string Id { get; }

        /// <summary> 
        /// Returns the action for this container 
        /// </summary>
        /// <returns>
        /// 
        /// </returns> 
        DatasetAction Action { get; }

        /// <summary> 
        /// Sets the action for this container 
        /// </summary>
        /// <param name=" action"> 
        /// </param> 
        void SetAction(DatasetAction action);

        /// <summary> 
        /// Returns the header for this objects container. Returns null if there is no header associated with this container. 
        /// </summary>
        /// <returns>
        /// 
        /// </returns> 
        IHeader Header { get; }

        /// <summary> 
        /// Sets the header on this set of objects 
        /// </summary>
        /// <returns>
        /// 
        /// </returns> 
        void SetHeader(IHeader header);

        void SetSelectedLocales(ISet<string> selectedLocales);

        /// <summary> 
        /// </summary>
        /// <returns>
        /// set of unique locales in the container 
        /// </returns> 
        ISet<string> AllLocales { get; }
    }
}