// -----------------------------------------------------------------------
// <copyright file="IAttributeList.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    #endregion

    /// <summary>
    ///     Represents an SDMX Attribute List
    /// </summary>
    public interface IAttributeList : IIdentifiableObject
    {
        /// <summary>
        ///     Gets the list of attributes that this attribute list is holding.
        ///     <p />
        ///     The returned list will be a copy of the underlying list, and is expected to have at least one AttributeObject
        /// </summary>
        /// <value> </value>
        IList<IAttributeObject> Attributes { get; }

        /// <summary>
        ///     Gets the MetadataAttributeUsage that refines the details of how a metadata attribute from the metadata structure
	    ///     referenced from the data structure is used.By default, metadata attributes can be expressed at any level of the
	    ///     data.This allows an attribute relationship to be defined in order restrict the reporting of a metadata attribute
	    ///     to a specific part of the data.
        ///     Introduced in SDMX 3.0.0
	    ///     Returns The unmodifiable list of Metadata Attribute Usage; or an empty list
        /// </summary>
        IList<IMetadataAttributeUsage> MetadataAttributes { get; }
    }
}