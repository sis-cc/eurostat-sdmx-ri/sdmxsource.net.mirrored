// -----------------------------------------------------------------------
// <copyright file="IMetadataAttributeUsage.cs" company="EUROSTAT">
//   Date Created : 2021-10-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure
{
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    /// Metadata Attribute Usage refines the details of how a metadata attribute from the metadata structure referenced from
    /// the data structure is used.By default, metadata attributes can be expressed at any level of the data.
    /// This allows an attribute relationship to be defined in order restrict the reporting of a metadata attribute to a
    /// specific part of the data.
    /// Introduced in SDMX 3.0.0
    /// <p/>
    /// This is an immutable bean - this bean can not be modified
    /// </summary>
    public interface IMetadataAttributeUsage : IAnnotableObject, IAttributeRelationship
    {
        /// <summary>
        /// MetadataAttributeReference is a local reference to a metadata attribute defined in the metadata structure referenced by this data structure.
	    /// Returns The ID of the metadata attribute that is referenced
        /// </summary>
        string MetadataAttributeReference { get; }
    }
}
