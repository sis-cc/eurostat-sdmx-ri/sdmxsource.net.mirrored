// -----------------------------------------------------------------------
// <copyright file="IMeasureList.cs" company="EUROSTAT">
//   Date Created : 2021-10-18
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure
{
    #region Using directives

    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    #endregion

    /// <summary>
    ///     Represents an SDMX Measure List.
    ///     Introduced in SDMX 3.0.0
    /// </summary>
    public interface IMeasure : IComponent, IUsage, IPrimaryMeasure
    {
        /// <summary>
        /// Returns a list of cross references to concepts that are used to define the role(s) of this measure.
        /// The returned list is a copy of the underlying list
        /// <p/>
        /// Returns an empty list if this attribute does not reference any concept roles
        /// </summary>
        /// <returns>The immutable list of concept roles.</returns>
        IList<ICrossReference> ConceptRoles { get; }

        /// <summary>
        /// Get the current measure as a SDMX 3.0.0 measure
        /// If the <see cref="ISdmxObject.StructureType"/> of the current instance is already 
        /// <see cref="SdmxStructureEnumType.Measure"/> it returns the current instance
        /// else if it is <see cref="SdmxStructureEnumType.PrimaryMeasure"/> it will return
        /// a new instance with <see cref="SdmxStructureEnumType.Measure"/>
        /// </summary>
        /// <returns>The current instance if it already a SDMX 3.0.0 measure; else a new instance </returns>
        IMeasure ToSDMX3Measure();
    }
}