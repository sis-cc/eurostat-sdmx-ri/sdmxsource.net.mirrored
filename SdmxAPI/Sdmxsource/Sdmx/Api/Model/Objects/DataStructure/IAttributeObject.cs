// -----------------------------------------------------------------------
// <copyright file="IAttributeObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure
{
    #region Using directives

    using System;
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    #endregion

    /// <summary>
    ///     Attribute describes the definition of a data attribute, which is defined as a characteristic of an object or
    ///     entity.
    ///     <p />
    ///     This is an immutable Object - this Object can not be modified
    /// </summary>
    public interface IAttributeObject : IComponent, IAttributeRelationship
    {
        /// <summary>
        ///     Gets the assignmentStatus attribute indicates whether a
        ///     value must be provided for the attribute when sending documentation along with the data.
        /// </summary>
        /// <value> </value>
        [Obsolete("It is replaced by IUsage.Usage which uses an Enum instead of magic strings")]
        string AssignmentStatus { get; }

        /// <summary>
        /// MeasureRelationship identifies the measures that the attribute applies to. If this is not used, the attribute
	    /// is assumed to apply to all measures.
	    /// Introduced in SDMX 3.0.0
	    /// Returns the list of measure id's ; else an empty set
        /// </summary>
        IList<string> MeasureRelationships { get; }

        /// <summary>
        ///     Gets a list of cross references to concepts that are used to define the role(s) of this attribute.
        ///     The returned list is a copy of the underlying list
        ///     <p />
        ///     Gets an empty list if this attribute does not reference any concept roles
        /// </summary>
        /// <value> </value>
        IList<ICrossReference> ConceptRoles { get; }

        /// <summary>
        ///     Gets a value indicating whether this Id = TIME_FORMAT
        /// </summary>
        /// <value> </value>
        bool TimeFormat { get; }
    }
}