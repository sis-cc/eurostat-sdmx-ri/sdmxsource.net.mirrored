// -----------------------------------------------------------------------
// <copyright file="IGeographicCodelistObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist
{
    using System;
    #region Using directives

    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    #endregion

    /// <summary>
    ///     Geographic codelist - each code provides a geo feature set, 
    ///     representing a set of points defining  a feature in a format defined a predefined pattern
    /// </summary>
    public interface IGeographicCodelistObject : IItemSchemeObject<IGeoFeatureSetCode>
    {
     
        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        IGeographicCodelistObject CloneForDate(DateTime date);

        
        /// <summary>
        ///     Gets a representation of itself in a @object which can be modified, modifications to the mutable @object
        ///     are not reflected in the instance of the IMaintainableObject.
        /// </summary>
        /// <value> </value>
        new IGeographicCodelistMutableObject MutableInstance { get; }
      
        /// <summary>
        ///     Gets the item with the given id, returns null if there is no item with the id provided
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="IGeoFeatureSetCode" /> .
        /// </returns>
        IGeoFeatureSetCode GetCodeById(string id);


        /// <summary>
        ///     Gets a stub reference of itself.
        ///     <p />
        ///     A stub @object only contains Maintainable and Identifiable Attributes, not the composite Objects that are
        ///     contained within the Maintainable
        /// </summary>
        /// <returns>
        ///     The <see cref="IGeographicCodelistObject" /> .
        /// </returns>
        /// <param name="actualLocation">
        ///     the Uri indicating where the full structure can be returned from
        /// </param>
        /// <param name="isServiceUrl">
        ///     if true this Uri will be present on the serviceURL attribute, otherwise it will be treated as a structureURL
        ///     attribute
        /// </param>
        new IGeographicCodelistObject GetStub(Uri actualLocation, bool isServiceUrl);


        /// <summary>
        ///   Returns a new item scheme based on the item filters, which are either removed or kept depending on the isKeepSet argument
        /// </summary>
        /// <param name="items"></param>
        /// <param name="isKeepSet"></param>
        /// <returns></returns>
        IGeographicCodelistObject FilterItems(IList<string> items, bool isKeepSet);

        /// <summary>
        /// Gets the codelist extensions if any.
        /// allows for the extension of codelists by referencing the codelists to be extended and providing
        /// inclusion/exclusion rules for selecting the codes to extend.
        /// The order of these is important as it is indicates the order of precedence of the extended codelists for conflict
        /// resolution of codes.However, the prefix property can be used to ensure uniqueness of inherited codes in the
        /// extending codelist, in case conflicting codes must be included.
        /// Introduced in SDMX 3.0.0
        /// </summary>
        IList<ICodelistInheritanceRule> CodelistExtensions { get; }

    }
}