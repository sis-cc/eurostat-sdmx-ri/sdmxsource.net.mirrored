using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist
{
    public interface IGeoFeatureSetCode : IHierarchicalItemObject<IGeoFeatureSetCode>
    {
        /// <summary>
        ///     Gets or sets the parent code.
        /// </summary>
        string ParentCode { get; }

        /// <summary>
        /// a set of points defining a feature in a format defined a predefined pattern
        /// </summary>
        /// <returns></returns>
        string GeoFeatureSet { get; }
    }
}
