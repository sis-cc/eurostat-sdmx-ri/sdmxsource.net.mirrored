// -----------------------------------------------------------------------
// <copyright file="IGeoGridCodelistObject.cs" company="EUROSTAT">
//   Date Created : 2024-03-13
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist
{
    using System;
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    public interface IGeoGridCodelistObject : IItemSchemeObject<IGeoGridCode>
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        IGeoGridCodelistObject CloneForDate(DateTime date);

        /// <summary>
        ///  A stub bean only contains Maintainable and Identifiable Attributes, not the composite Objects that are
        ///  contained within the Maintainable
        /// </summary>
        /// <param name="actualLocation"></param>
        /// <param name="isServiceUrl"></param>      
        /// <returns></returns>
        new IGeoGridCodelistObject GetStub(Uri actualLocation, bool isServiceUrl);

        /// <summary>
        ///   Returns the item with the given id, returns null if there is no item with the id provided
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        IGeoGridCode GetItemById(string id);

        /// <summary>
        ///   Returns a representation of itself in a bean which can be modified, modifications to the mutable bean
	    ///   are not reflected in the instance of the MaintainableBean.
        /// </summary>
        new IGeoGridCodelistMutableObject MutableInstance { get; }

        /// <summary>
        ///   Returns a new item scheme based on the item filters, which are either removed or kept depending on the isKeepSet argument
        /// </summary>
        /// <param name="filterIds"></param>
        /// <param name="isKeepSet"></param>
        /// <returns></returns>
        IGeoGridCodelistObject FilterItems(IList<string> filterIds, bool isKeepSet);

        /// <summary>
        /// Gets the codelist extensions if any.
        /// allows for the extension of codelists by referencing the codelists to be extended and providing
        /// inclusion/exclusion rules for selecting the codes to extend.
        /// The order of these is important as it is indicates the order of precedence of the extended codelists for conflict
        /// resolution of codes.However, the prefix property can be used to ensure uniqueness of inherited codes in the
        /// extending codelist, in case conflicting codes must be included.
        /// Introduced in SDMX 3.0.0
        /// </summary>
        IList<ICodelistInheritanceRule> CodelistExtensions { get; }

        /// <summary>
        ///  The GridDefinition is a string expression with a defined syntax:
        ///  
        ///  [CRS]:[REFERENCE_CORNER];[REFERENCE_COORDINATES];[CELL_WIDTH],[CELL_HEIGHT]:[GEO_STD]
        ///   
        ///   CRS - Optional: 
        ///   The code of the Coordinate Reference System is used to reference the coordinates in the flow. 
        ///   The code of the CRS will be as it appears in the EPSG Geodetic Parameter Registry ( ) 
        ///   maintained by the International Association of Oil and Gas Producers. 
        ///   If this component is omitted, by default the CRS will be the World Geodetic System 1984 (WGS 84, EPSG:4326).
        ///   
        ///   
        ///   REFERENCE_CORNER - Optional:
        ///   A code composed of two characters to define the position of the coordinates that will be used as a starting reference to locate the cells. 
        ///   The possible values of this code can be UL (Upper Left), UR (Upper Right), LL (Lower Left), or LR (Lower Right).
        ///   If this component is omitted the value LL (Lower Left) will be taken by default. 
        ///   
        ///   
        ///   REFERENCE_COORDINATES -  This element is mandatory if GEO_STD is omitted.:
        ///   Represents the starting point to reference the cells of the grid, accordingly to the CRS and the REFERENCE_CORNER. 
        ///   It is represented by an individual set of coordinates composed by the X_COORDINATE (X) and Y_COORDINATE (Y) in the following way "X,Y". 
        ///   
        ///   
        ///   CELL_WIDTH -  This element is mandatory if GEO_STD is omitted: 
        ///   The size in meters of a horizontal side of the cells in the grid. 
        ///   
        ///   
        ///   CELL_HEIGHT -  This element is mandatory if GEO_STD is omitted: 
        ///   The size in meters of a vertical side of the cells in the grid.
        ///   
        ///   
        ///   GEO_STD - Optional: 
        ///   A restricted text value expressing that the cells in the grid will provide information 
        ///   about matching codes existing in another reference system that establishes a mechanism to define the grid. 
        ///   
        ///  
        ///  Accepted values for the GEO_STD component are included in the Geographical Grids Codelist (CL_GEO_GRIDS). Examples contained in the mentioned document are: 
        ///    
        ///  GEOHASH : GeoHash	  
        ///	 
        ///  GEOREF : World Geographic Reference System
        ///    
        ///  MGRS : Military Grid Reference System
        ///    
        ///  OLC : Open Location Code / Plus Code
        ///    
        ///  QTH : Maidenhead Locator System /QTH Locator / IAURU Locator
        ///    
        ///  W3W : What3words
        ///    
        ///  WOEID : 	Where On Earth Identifier		  
        ///	 
        ///  Example: 4326:UL;9.580078,55.103516;1000,1000:W3W
        /// </summary>
        string GridDefinition {  get; }
    }
   
}
