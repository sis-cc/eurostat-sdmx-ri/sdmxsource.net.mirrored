// -----------------------------------------------------------------------
// <copyright file="ICodeSelection.cs" company="EUROSTAT">
//   Date Created : 2021-10-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist
{
    using System.Collections.Generic;

    /// <summary>
    /// Selection of codes from the referenced (extended) codelist to be included or excluded in the extending codelist.
    /// </summary>
    public interface ICodeSelection
    {
        /// <summary>
        /// Gets a value indicating whether this Code Selection is inclusive or exclusive
        /// </summary>
        bool IsInclusive { get; }

        /// <summary>
        /// Gets the list of member values or an empty list
        /// An explicit or wildcard selection of a code(s) from the codelist selected for inclusion/exclusion.
        /// If a wildcard expression is used, it is evaluated to determine codes selected for inclusion/exclusion.
        /// Otherwise, each member value is a distinct code.If the extended list is hierarchical,
        /// this can indicate whether child codes are to be included.
        /// </summary>
        IList<IMemberValue> MemberValues { get; }
    }
}
