// -----------------------------------------------------------------------
// <copyright file="IMemberValue.cs" company="EUROSTAT">
//   Date Created : 2021-10-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist
{
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// Allows for a distinct reference or a wildcard expression for selecting codes from a codelist.
    /// </summary>
    public interface IMemberValue
    {
        /// <summary>
        /// Gets a value that indicates whether child codes should be selected when the codelist is hierarchical. Possible values are: 
        /// true (include the selected and child codes), 
        /// false (only include the selected code(s)), and 
        /// excluderoot(include the children but not the selected code(s)).
        /// </summary>
        CascadeSelection Cascade { get; }

        /// <summary>
        /// Gets the distinct reference or wildcard expression for selecting codes
        /// It allows for an optional wildcard characters('%') in an identifier
        /// </summary>
        string Value { get; }
    }
}
