// -----------------------------------------------------------------------
// <copyright file="IConstrainedDataKeyObject.cs" company="EUROSTAT">
//   Date Created : 2013-11-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry
{
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    /// Constrains a series key (or wildcarded series key) by providing a list of key value pairs
    /// </summary>
    public interface IConstrainedDataKeyObject : ISdmxStructure
    {

        /// <summary>
        /// The list of key values
        /// </summary>
        /// <returns></returns>       
        IList<IKeyValue> KeyValues { get; }

        /// <summary>
        /// Returns the key value for the given dimension, returns null if none exist
        /// </summary>
        /// <param name="dimensionId"></param>
        /// <returns></returns>
        IKeyValue GetKeyValue(string dimensionId);

        /// <summary>
        /// Returns the list of Component values which are non-dimension (attribute or measure)
        /// </summary>
        /// <returns></returns>
        List<IConstrainedKeyValue> GetComponentValues();

        /// <summary>
        /// Returns the validity period (valid from and to dates)
        /// </summary>
        /// <returns></returns>
        ISdmxPeriod GetValidityPeriod();
    }
}
