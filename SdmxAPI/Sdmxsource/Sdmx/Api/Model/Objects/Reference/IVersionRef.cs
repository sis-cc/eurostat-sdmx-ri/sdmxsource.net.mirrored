// -----------------------------------------------------------------------
// <copyright file="IVersionRef.cs" company="EUROSTAT">
//   Date Created : 2023-05-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference
{
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary> 
    /// References one or more versions 
    /// </summary> 
    public interface IVersionRef
    {
        /// <summary> 
        /// </summary>
        /// <returns>
        /// true if the latest version in the min/max range is being referenced if false then all versions are matched 
        /// </returns> 
        bool IsLatest();

        /// <summary> 
        /// </summary>
        /// <returns>
        /// true if this is referencing an absolute version, with no latest/range logic (min version == max version, islatest==false) 
        /// </returns> 
        bool IsAbsolute();

        /// <summary> 
        /// </summary>
        /// <returns>
        /// true if this will match on any version, does not take into account drafts i.e. if All may return true but then return false 
        /// on a draft because it has a null version suffix.  Check with version suffix to ensure it is all drafts as well 
        /// </returns> 
        bool IsAll();

        /// <summary> 
        /// optional, used to reference versions that include a suffix, if null no suffix should be present, if /// any suffix (of lack of) is matched 
        /// </summary>
        /// <returns>
        /// 
        /// </returns> 
        string VersionSuffix { get; }

        /// <summary> 
        /// </summary>
        /// <returns>
        /// minimum version being referenced 
        /// </returns> 
        ISdmxVersion MinVersion { get; }

        /// <summary> 
        /// </summary>
        /// <returns>
        /// maximum version being referenced 
        /// </returns> 
        ISdmxVersion MaxVersion { get; }

        /// <summary> 
        /// </summary>
        /// <param name=" version"> 
        /// </param>
        /// <returns>
        /// true if the version passed in matches the rules of this reference 
        /// </returns> 
        bool IsMatch(ISdmxVersion version);

        /// <summary> 
        /// </summary>
        /// <param name=" vRef"> 
        /// </param>
        /// <returns>
        /// true if the version passed in matches the rules of this reference 
        /// </returns> 
        bool IsMatch(IVersionRef vRef);

        /// <summary> 
        /// </summary>
        /// <returns>
        /// string representation of the reference, e.g.e 1+2.0 
        /// </returns> 
        string ToString();
    }

}