// -----------------------------------------------------------------------
// <copyright file="ICommonStructureQuery.cs" company="EUROSTAT">
//   Date Created : 2022-05-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common
{
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// Contains all properties needed for a structure query.
    /// Use this instead of <see cref="IRestStructureQuery"/> and <see cref="IComplexStructureQuery"/>interface.
    /// </summary>
    public interface ICommonStructureQuery
    {
        /// <summary>
        /// The requested structure format
        /// </summary>
        // TODO common structure query: this is part of the headers. Maybe it should be with the other header parts.
        // Currently the output format is not taken from this property, so it can be removed. 
        StructureOutputFormat StructureOutputFormat { get; }

        /// <summary>
        /// Gets the source of the query, REST, SOAP etc
        /// </summary>
        CommonStructureQueryType QueryType { get; }

        /// <summary>
        /// Gets the type of references requested, e.g. children, parents etc
        /// </summary>
        StructureReferenceDetail References { get; }

        /// <summary>
        /// Gets a list of specific artefact type references to return when <see cref="References"/> is set to <see cref="StructureReferenceDetailEnumType.Specific"/>
        /// </summary>
        IList<SdmxStructureType> SpecificReferences { get; }

        /// <summary>
        /// Gets or sets a value indicating whether the query has requested specific Agency ID(s)
        /// Return True if it requests specific agency id (s) or false if it was not set or set to all or '*'
        /// </summary>
        bool HasSpecificAgencyId { get; }

        /// <summary>
        /// Gets a value indicating whether the query has requested specific maintainable ID(s)
        /// Return True if it requests specific agencies or false if it was not set or set to all or '*'
        /// </summary>
        bool HasSpecificMaintainableId { get; }

        /// <summary>
        /// Gets the list of the requested specific agency ID(s)
        /// </summary>
        IList<string> AgencyIds { get; }

        /// <summary>
        /// Gets the list of the requested specific maintainable ID(s)
        /// </summary>
        IList<string> MaintainableIds { get; }

        /// <summary>
        /// Gets the list of version requests made for this structure query.
        /// </summary>
        IList<IVersionRequest> VersionRequests { get; }

        /// <summary>
        /// Gets the level of detail requested.
        /// </summary>
        ComplexStructureQueryDetail RequestedDetail { get; }

        /// <summary>
        /// Gets the artefact annotations.
        /// </summary>
        IComplexAnnotationReference AnnotationReference { get; }

        /// <summary>
        /// The query may request to filter down the items of the structure to specific items.
        /// </summary>
        IList<IComplexIdentifiableReferenceObject> SpecificItems { get; }

        /// <summary>
        /// The type of the requested structure.
        /// </summary>
        SdmxStructureType MaintainableTarget { get; }

        /// <summary>
        /// Whether to return full artefacts, stubs or complete artefacts
        /// This applies to all artefacts regardless how they are in the DB
        /// </summary>
        ComplexMaintainableQueryDetail ReferencedDetail { get; }

        /// <summary>
        /// Cascade item query. Valid only if <see cref="SpecificItems"/> is not empty.
        /// For REST it is always false (at least until v2.0.0).
        /// For SOAP 2.1 it depends on the request.
        /// </summary>
        CascadeSelection ItemCascade { get; }

    }
}
