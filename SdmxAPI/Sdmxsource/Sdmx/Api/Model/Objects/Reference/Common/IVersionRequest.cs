// -----------------------------------------------------------------------
// <copyright file="IVersionRequest.cs" company="EUROSTAT">
//   Date Created : 2022-05-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common
{
    using Sdmx.Api.Constants;

    /// <summary>
    /// Represents a request for the desired version(s) of the artefact(s).
    /// </summary>
    public interface IVersionRequest
    {
        /// <summary>
        /// Whether the request asks for a (major) version.
        /// </summary>
        bool SpecifiesVersion { get; }

        /// <summary>
        /// TODO: this is a bit confusing what it should used for.
        /// </summary>
        VersionQueryTypeEnum VersionQueryType { get; }

        /// <summary>
        /// Gets the version extension (if any)
        /// </summary>
        string Extension { get; }

        /// <summary>
        /// Gets the part of the version specifed by <paramref name="position"/>.
        /// </summary>
        /// <param name="position">The semantic position to get the version part for.</param>
        /// <returns>The number of the version part, or <c>null</c> if this part has not been specified int he request.</returns>
        int? VersionPart(VersionPosition position);

        /// <summary>
        /// Gets the number of the major version, or <c>null</c> if the major part is not specified int he request.
        /// </summary>
        int? Major { get; }

        /// <summary>
        /// Gets the number of the minor version, or <c>null</c> if the minor part is not specified int he request.
        /// </summary>
        int? Minor { get; }

        /// <summary>
        /// Gets the number of the patch version, or <c>null</c> if the patch part is not specified int he request.
        /// </summary>
        int? Patch { get; }

        /// <summary>
        /// Gets the wildcard that has been used in the version request 
        /// and the position in the semantic version that the wildcard was used in.
        /// </summary>
        IVersionWildcard WildCard { get; }
    }
}
