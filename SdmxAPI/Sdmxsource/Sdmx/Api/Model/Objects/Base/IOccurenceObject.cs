// -----------------------------------------------------------------------
// <copyright file="IOccurenceObject.cs" company="EUROSTAT">
//   Date Created : 2021-10-18
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Base
{
    using System;
    using Sdmxsource.Sdmx.Api.Exception;

    /// <summary>
    /// OccurrenceBean is used to express the maximum occurrence of an object. It combines an integer, equal or greater than 1,
    /// and the literal text, "unbounded", for objects which have no upper limit on its occurrence
    /// </summary>
    public interface IOccurenceObject
    {
        /// <summary>
        ///     Gets the number that expresses the maximum occurrence of an object.
        /// </summary>
        /// <returns>
        ///     The number of max occurrences of an object, larger than 0
        /// </returns>
        /// <exception cref="SdmxSemmanticException">The <see cref="IsUnbounded"/> is <c>true</c>.</exception>
        int Occurrences { get; }

        /// <summary>
        ///     Gets a value indicating whether the maximum occurrence is unbounded
        /// </summary>
        /// <returns>
        ///     <c>true</c> if it is unbounded; else <c>false</c> and is expected that <see cref="Occurrences"/> value is present.
        /// </returns>
        bool IsUnbounded { get; }
    }
}
