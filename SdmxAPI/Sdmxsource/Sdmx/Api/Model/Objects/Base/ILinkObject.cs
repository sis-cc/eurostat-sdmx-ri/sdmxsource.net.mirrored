// -----------------------------------------------------------------------
// <copyright file="ILinkObject.cs" company="EUROSTAT">
//   Date Created : 2023-05-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Base
{
    using System;
    using System.Collections.Generic;

    /// <summary> 
    /// Allows for the linking of other resources to identifiable objects. 
    /// 
    /// For example, if there is reference metadata associated with a structure, 
    /// a link to the metadata report can be dynamically inserted in the structure metadata. 
    /// </summary> 
    public interface ILinkObject
    {
        /// <summary> 
        /// </summary>
        /// <returns>
        /// not null - The type of object that is being linked to 
        /// </returns> 
        string Rel { get; }

        /// <summary> 
        /// </summary>
        /// <returns>
        /// not null - the URL of the object being linked 
        /// </returns> 
        Uri HRef { get; }

        /// <summary> 
        /// </summary>
        /// <returns>
        /// optional - the URL of the object being linked 
        /// </returns> 
        Uri Uri { get; }

        /// <summary> 
        /// </summary>
        /// <returns>
        /// optional - a registry URN of the object being linked 
        /// </returns> 
        IURN Urn { get; }

        /// <summary> 
        /// </summary>
        /// <returns>
        /// optional - the type of link (e.g. The type of link (e.g. PDF, text, HTML, reference metadata) 
        /// </returns> 
        string Type { get; }

        /// <summary> 
        /// </summary>
        /// <param name=" disableLocaleFilter"> if true, all titles will be returned, 
        /// if false, if a LocaleFilter is in effect then the returned IList may be filtered by Locale 
        /// </param>
        /// <returns>
        /// not null list of titles 
        /// </returns> 
        IList<ITextTypeWrapper> GetTitles(bool disableLocaleFilter);

        /// <summary> 
        /// </summary>
        /// <returns>
        /// optional title in the default locale 
        /// </returns> 
        string Title { get; }

        /// <summary> 
        /// </summary>
        /// <returns>
        /// optional the name in the given locale 
        /// </returns> 
        string GetTitle(string locale);
    }
}