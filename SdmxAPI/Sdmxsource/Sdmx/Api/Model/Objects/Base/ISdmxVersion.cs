// -----------------------------------------------------------------------
// <copyright file="ISdmxVersion.cs" company="EUROSTAT">
//   Date Created : 2023-05-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Base
{
    using System;

    /// <summary> 
    /// Represents a version number, with a minimum of 1 part, and a maximum of 3, where each part is period separated, e.g. 1, 1.0, 1.0.0 
    /// 
    /// Comparable by returning +1 for a higher version, -1 for lower, and 0 for the same 
    /// </summary> 
    public interface ISdmxVersion : IComparable<ISdmxVersion>
    {
        /// <summary> 
        /// </summary>
        /// <returns>
        /// true if this version is later then the version passed in, a stable version is deemed later then a draft,
        /// drafts comparisons are based on a string compare of the suffix 
        /// </returns> 
        bool IsLater(ISdmxVersion version);

        /// <summary> 
        /// </summary>
        /// <returns>
        /// true for a three part version with no suffix 
        /// </returns> 
        bool IsStable();

        /// <summary> 
        /// </summary>
        /// <returns>
        /// true if the version only has 2 parts 
        /// </returns> 
        bool IsTwoPart();

        /// <summary> 
        /// </summary>
        /// <returns>
        /// major version 
        /// </returns> 
        int Major { get; }

        /// <summary> 
        /// </summary>
        /// <returns>
        /// minor version or -1 if there is no patch version 
        /// </returns> 
        int Minor { get; }

        /// <summary> 
        /// </summary>
        /// <returns>
        /// patch version or -1 if there is no patch version 
        /// </returns> 
        int Patch { get; }

        /// <summary> 
        /// If the version has a suffix, e.g. 1.2.1-draft 
        /// 
        /// </summary>
        /// <returns>
        /// the version suffix, or null if there is not one 
        /// </returns> 
        string Suffix { get; }

        /// <summary> 
        /// </summary>
        /// <returns>
        /// version as string e.g. 1.2.3 
        /// </returns> 
        string ToString();
    }
}