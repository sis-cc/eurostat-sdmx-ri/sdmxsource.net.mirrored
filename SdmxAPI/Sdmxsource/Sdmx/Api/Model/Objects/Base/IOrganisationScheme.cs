﻿// -----------------------------------------------------------------------
// <copyright file="IOrganisationScheme.cs" company="EUROSTAT">
//   Date Created : 2016-03-10
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Base
{
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;

    /// <summary>
    ///     The organisation scheme interface.
    /// </summary>
    /// <typeparam name="T">The generic type</typeparam>
    /// <seealso cref="Org.Sdmxsource.Sdmx.Api.Model.Objects.Base.IItemSchemeObject{T}" />
    public interface IOrganisationScheme<T> : IItemSchemeObject<T>
        where T : IItemObject
    {
        /// <summary>
        ///     Gets a representation of itself in a bean which can be modified, modifications to the mutable bean
        ///     are not reflected in the instance of the MaintainableObject.
        /// </summary>
        /// <value>
        ///     The mutable instance.
        /// </value>
        new IOrganisationSchemeMutableObject<IOrganisationScheme<T>, IOrganisationMutableObject> MutableInstance
        {
            get;
        }
    }
}