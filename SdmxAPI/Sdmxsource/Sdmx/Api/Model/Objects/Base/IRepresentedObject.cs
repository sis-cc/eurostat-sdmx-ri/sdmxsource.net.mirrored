
// -----------------------------------------------------------------------
// <copyright file="IRepresentedObject.cs" company="EUROSTAT">
//   Date Created : 2023-05-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Base
{
    using System;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    //NOTE SDMXSOURCE_SDMXCORE CHANGE
    /// <summary> 
    /// Represents a Object which optionally defines its representation by a text format and/or enumerated type 
    /// </summary> 
    public interface IRepresentedObject
    {
        /// <summary> 
        /// Gets the minimum allowed occurrences for an attribute or measure value in a dataset 
        /// This is just a shortcut to <see cref="IRepresentation.MinOccurs"/>
        /// Introduced in SDMX 3.0.0 
        /// </summary>
        /// <returns>
        /// the minimum number of a value that must be reported; default is 1 
        /// </returns> 
        int RepresentationMinOccurs { get; }

        /// <summary> 
        /// The maxOccurs attribute indicates the maximum number of values that can be reported for the component. 
        /// This is just a shortcut to <see cref="IRepresentation.MaxOccurs"/>
        /// Introduced in SDMX 3.0.0 
        /// When it is larger than 1, then it means that the component values are to reported as an array in the dataset 
        /// </summary>
        /// <returns>
        /// the maximum number of a value that must be reported; default is 1 
        /// </returns> 
        IOccurenceObject RepresentationMaxOccurs { get; }

        /// <summary> 
        /// </summary>
        /// <returns>
        /// true if the text format allows for multilingual text 
        /// </returns> 
        bool IsMultilingual { get; }

        /*    bool IsUnboundedMax() {}*/

        /// <summary> 
        /// Returns the text format, which is typically (but does not have to be) present for an uncoded component (one which does not reference an enumerated list).
        /// Returns null if there is no text format present 
        /// </summary>
        ITextFormat TextFormat { get; }


        /// <summary> 
        /// Returns the text type, which is typically (but does not have to be) present for an uncoded component (one which does not reference an enumerated list). 
        /// Returns null if there is no text format present 
        /// </summary>
        TextType TextType { get; }

        /// <summary> 
        /// Returns the core representation for this concept. 
        /// </summary>
        /// <returns>
        /// null if no core representation is defined 
        /// </returns> 
        [Obsolete("use Representation instead")]
        IRepresentation CoreRepresentation { get; }


        /// <summary> 
        /// </summary>
        /// <returns>
        /// reference to an enumerated list of allowable values, or null if there is no such reference 
        /// </returns> 
        ICrossReference EnumeratedRepresentation { get; }


        /// <summary> 
        /// </summary>
        /// <returns>
        /// the representation for this component, returns null if there is no representation 
        /// </returns> 
        IRepresentation Representation { get; }


        /// <summary>
        /// return true if this identifier component has coded representation
        /// </summary>
        /// <returns></returns>
        bool HasCodedRepresentation();
    }
}