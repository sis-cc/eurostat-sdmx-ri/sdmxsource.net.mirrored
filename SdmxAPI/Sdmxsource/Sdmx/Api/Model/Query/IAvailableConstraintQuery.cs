// -----------------------------------------------------------------------
// <copyright file="IAvailableConstraintQuery.cs" company="EUROSTAT">
//   Date Created : 2018-04-28
//   Copyright (c) 2018 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Api.Model.Query
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;

    /// <summary>
    /// Dynamic constraint query
    /// </summary>
    public interface IAvailableConstraintQuery
    {
        /// <summary>
        /// Gets the component identifiers.
        /// </summary>
        /// <value>
        /// The component identifier.
        /// </value>
        IList<IComponent> Components { get; }

        /// <summary>
        ///     Gets the Dataflow that the query is returning data for.
        /// </summary>
        IDataflowObject Dataflow { get; }

        /// <summary>
        ///     Gets the data provider(s) that the query is for, an empty list represents ALL
        /// </summary>
        ISet<IDataProvider> DataProvider { get; }

        /// <summary>
        ///     Gets the Key Family (Data Structure Definition) that this query is returning data for.
        /// </summary>
        IDataStructureObject DataStructure { get; }

        /// <summary>
        ///     Gets the date from in this selection group
        /// </summary>
        /// <value> </value>
        ISdmxDate DateFrom { get; }

        /// <summary>
        ///     Gets the date to in this selection group
        /// </summary>
        /// <value> </value>
        ISdmxDate DateTo { get; }

        /// <summary>
        ///     Gets last updated date is a filter on the data meaning 'only return data that was updated after this time'
        ///     <p />
        ///     If this attribute is used, the returned message should
        ///     only include the latest version of what has changed in the database since that point in time (updates and
        ///     revisions).
        ///     <p />
        ///     This should include:
        ///     <ul>
        ///         <li>Observations that have been added since the last time the query was performed (INSERT)</li>
        ///         <li>Observations that have been revised since the last time the query was performed (UPDATE)</li>
        ///         <li>Observations that have been revised since the last time the query was performed (UPDATE)</li>
        ///         <li>Observations that have been deleted since the last time the query was performed (DELETE)</li>
        ///     </ul>
        ///     If no offset is specified, default to local
        ///     <p />
        ///     Gets null if unspecified
        /// </summary>
        /// <value> </value>
        ISdmxDate LastUpdatedDate { get; }

        /// <summary>
        /// The mmode (TODO Parameter name might change) of the query
        /// </summary>
        AvailableConstraintQueryMode Mode { get; }

        /// <summary>
        ///     Gets the value of firstNObservations parameter.
        /// </summary>
        int? FirstNObservations { get; }

        /// <summary>
        ///     Gets the value of lastNObservations parameter.
        /// </summary>
        int? LastNObservations { get; }

        /// <summary>
        ///     Gets or sets the value of dimensionAtObservation data query parameter.
        /// </summary>
        string DimensionAtObservation { get; }

        /// <summary>
        ///     Gets the set of selections for this group - these are implicitly ANDED
        /// </summary>
        /// <value> </value>
        IList<IKeyValues> Selections { get; }

        /// <summary>
        ///     Gets the specific structure reference if reference is specified
        /// </summary>
        SdmxStructureType SpecificStructureReference { get; }

        /// <summary>
        ///     Gets the selection(s) for the given dimension id, returns null if no selection exist for the dimension id
        /// </summary>
        /// <param name="componentId">The component Id. </param>
        /// <returns>
        ///     The <see cref="IKeyValues" /> .
        /// </returns>
        IKeyValues GetSelectionsForConcept(string componentId);

        /// <summary>
        ///     Gets a value indicating whether the selections exist for this dimension Id
        /// </summary>
        /// <param name="dimensionId">
        ///     The dimension Id.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        bool HasSelectionForConcept(string dimensionId);
    }
}