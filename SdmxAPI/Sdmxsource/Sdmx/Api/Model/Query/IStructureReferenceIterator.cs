// -----------------------------------------------------------------------
// <copyright file="IStructureReferenceIterator.cs" company="EUROSTAT">
//   Date Created : 2021-08-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

namespace Org.Sdmxsource.Sdmx.Api.Model.Query
{
    /// <summary>
    /// Used to iterate over <see cref="IStructureReference"/>s in a REST query
    /// </summary>
    public interface IStructureReferenceIterator
    {
        /// <summary>
        /// Returns the <see cref="IStructureReference"/> in the current position.
        /// </summary>
        IStructureReference Current { get; }

        /// <summary>
        /// Returns the total number of <see cref="IStructureReference"/> items in the iterator.
        /// </summary>
        int Count { get; }

        /// <summary>
        /// Moves to next <see cref="IStructureReference"/>.
        /// </summary>
        /// <returns><c>true</c> if next <see cref="IStructureReference"/> is found, <c>false</c> if end of stream.</returns>
        bool MoveNext();

        /// <summary>
        /// Resets the iterator to start over.
        /// </summary>
        void Reset();
    }
}
