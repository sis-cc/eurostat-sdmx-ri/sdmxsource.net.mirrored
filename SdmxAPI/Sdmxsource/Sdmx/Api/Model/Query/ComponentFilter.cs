// -----------------------------------------------------------------------
// <copyright file="ComponentFilter.cs" company="EUROSTAT">
//   Date Created : 2021-08-18
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Api.Model.Query
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// Represents a filter for a component in DDB
    /// A composite pattern is used to handle many layers of filters.
    /// </summary>
    public abstract class ComponentFilter
    {
        /// <summary>
        /// The component name.
        /// There can be <see cref="ComponentFilter"/>s with the same <see cref="ComponentName"/>.
        /// </summary>
        public string ComponentName { get; protected set; }

        /// <summary>
        /// Indicates whether this is a component for the time range.
        /// </summary>
        public virtual bool IsTimeRange { get; protected set; }

        /// <summary>
        /// The comparison operator between the filter values and the data.
        /// </summary>
        public RESTQueryOperator OperatorType { get; protected set; }

        /// <summary>
        /// The logical operator between the filters
        /// 0 for AND, 1 for OR
        /// </summary>
        public int? LogicalOperator { get; protected set; }

        /// <summary>
        /// The parent filter.
        /// </summary>
        public ComponentFilter Parent { get; protected set; }

        /// <summary>
        /// The filter values.
        /// In case of multiple values, the OR operator must be used
        /// </summary>
        public readonly List<string> Values = new List<string>();

        /// <summary>
        /// There can be components that accept array values.
        /// </summary>
        public readonly List<List<string>> ArrayValues = new List<List<string>>();

        /// <summary>
        /// Indicates whether the filter has array values.
        /// </summary>
        public bool HasArrayValues => ArrayValues.Any();

        private readonly Lazy<List<ComponentFilter>> _componentFilters = new Lazy<List<ComponentFilter>>();
        /// <summary>
        /// Component filters in depth.
        /// </summary>
        protected List<ComponentFilter> InnerFilters => _componentFilters.Value;

        /// <summary>
        /// Returns the values for the time range component (if any)
        /// </summary>
        /// <returns>a list of one or two values for the time range.</returns>
        public abstract List<string> GetTimeRangeValues();

        /// <summary>
        /// Returns the next Filter in the stack.
        /// </summary>
        /// <returns></returns>
        public abstract ComponentFilter Next();

        /// <summary>
        /// Starts the iterator for the tree of filters.
        /// </summary>
        /// <returns></returns>
        public abstract IComponentFilterIterator StartIterator();
    }
}
