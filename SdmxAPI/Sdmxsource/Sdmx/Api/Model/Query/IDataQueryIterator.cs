// -----------------------------------------------------------------------
// <copyright file="IDataQueryIterator.cs" company="EUROSTAT">
//   Date Created : 2021-08-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Api.Model.Query
{
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;

    /// <summary>
    /// The main use of this iterator is to build the info of a <see cref="IDataQuery"/>
    /// for multiple <see cref="IRestDataQuery.FlowRef"/>.
    /// </summary>
    public interface IDataQueryIterator
    {
        /// <summary>
        /// Returns the total number of iteration to be performed.
        /// </summary>
        int Count { get; }

        /// <summary>
        /// Moves to the next build of the <see cref="IDataQuery"/> based on a new <see cref="IRestDataQuery.FlowRef"/>.
        /// </summary>
        /// <returns><c>true</c> if next <see cref="IRestDataQuery.FlowRef"/> is found, <c>false</c> if end of iteration.</returns>
        bool MoveNext();

        /// <summary>
        /// Resets the iterator to start over.
        /// </summary>
        void Reset();
    }
}
