// -----------------------------------------------------------------------
// <copyright file="IAnnotationUrlSuperObject.cs" company="EUROSTAT">
//   Date Created : 2022-03-24
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Introduced in SDMX v3.0 to support localized annotation urls.
    /// </summary>
    public interface IAnnotationUrlSuperObject
    {
        /// <summary>
        /// Indicates the language of the resources at the URL, if it is localised. If this does not exist, the resource is not localised
        /// </summary>
        string Locale { get; }

        /// <summary>
        ///  The URL to the external resource
        /// </summary>
        Uri Uri { get; }
    }
}
