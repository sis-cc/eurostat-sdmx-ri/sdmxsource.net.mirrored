// -----------------------------------------------------------------------
// <copyright file="ISuperObjects.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.SuperObjects
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Registry;

    #endregion

    /// <summary>
    ///     SuperObjects is a container for objects of type <b>SuperObjects</b>
    /// </summary>
    public interface ISuperObjects
    {
        /// <summary>
        ///     Gets a <b>copy</b> of the IMaintainableSuperObject Set.  Gets an empty set if no MaintainableBaseObjects are stored
        ///     in this container
        /// </summary>
        /// <value> </value>
        ISet<IMaintainableSuperObject> AllMaintainables { get; }

        /// <summary>
        ///     Gets a <b>copy</b> of the ICategorySchemeSuperObject Set.  Gets an empty set if no CategorySchemeBaseObjects are
        ///     stored in this container
        /// </summary>
        /// <value> </value>
        ISet<ICategorySchemeSuperObject> CategorySchemes { get; }

        /// <summary>
        ///     Gets a <b>copy</b> of the ICodelistSuperObject Set.  Gets an empty set if no CodelistBaseObjects are stored in this
        ///     container
        /// </summary>
        /// <value> </value>
        ISet<ICodelistSuperObject> Codelists { get; }

        /// <summary>
        ///     Gets a <b>copy</b> of the IConceptSchemeSuperObject Set.  Gets an empty set if no ConceptSchemeBaseObjects are
        ///     stored in this container
        /// </summary>
        /// <value> </value>
        ISet<IConceptSchemeSuperObject> ConceptSchemes { get; }

        /// <summary>
        ///     Gets a <b>copy</b> of the IDataflowSuperObject Set.  Gets an empty set if no DataflowBaseObjects are stored in this
        ///     container
        /// </summary>
        /// <value> </value>
        ISet<IDataflowSuperObject> Dataflows { get; }

        /// <summary>
        ///     Gets a <b>copy</b> of the KeyFamilyBaseObjects Set.  Gets an empty set if no KeyFamilyBaseObjects are stored in
        ///     this container
        /// </summary>
        /// <value> </value>
        ISet<IDataStructureSuperObject> DataStructures { get; }

        /// <summary>
        ///     Gets a <b>copy</b> of the IHierarchicalCodelistSuperObject Set.  Gets an empty set if no
        ///     HierarchicalCodelistBaseObjects are stored in this container
        /// </summary>
        /// <value> </value>
        ISet<IHierarchicalCodelistSuperObject> HierarchicalCodelists { get; }

        /// <summary>
        ///     Gets a <b>copy</b> of the IProcessSuperObject Set.  Gets an empty set if no ProcessBaseObjects are stored in this
        ///     container
        /// </summary>
        /// <value> </value>
        ISet<IProcessSuperObject> Processes { get; }

        /// <summary>
        ///     Gets a <b>copy</b> of the IProvisionAgreementSuperObject Set.  Gets an empty set if no
        ///     ProvisionAgreementBaseObjects are stored in this container
        /// </summary>
        /// <value> </value>
        ISet<IProvisionAgreementSuperObject> Provisions { get; }

        /// <summary>
        ///     Gets a <b>copy</b> of the IRegistrationSuperObject Set.  Gets an empty set if no RegistrationBaseObjects are stored
        ///     in this container
        /// </summary>
        /// <value> </value>
        ISet<IRegistrationSuperObject> Registartions { get; }

        /// <summary>
        ///     Add a ICategorySchemeSuperObject to this container, overwrite if one already exists with the same URN
        /// </summary>
        /// <param name="categorySchemeObject">The object. </param>
        void AddCategoryScheme(ICategorySchemeSuperObject categorySchemeObject);

        /// <summary>
        ///     Add a ICodelistSuperObject to this container, overwrite if one already exists with the same URN
        /// </summary>
        /// <param name="codelistObject">Codelist object. </param>
        void AddCodelist(ICodelistSuperObject codelistObject);

        /// <summary>
        ///     Add a IConceptSchemeSuperObject to this container, overwrite if one already exists with the same URN
        /// </summary>
        /// <param name="conceptSchemeObject"> ConceptScheme object </param>
        void AddConceptScheme(IConceptSchemeSuperObject conceptSchemeObject);

        /// <summary>
        ///     Add a IDataflowSuperObject to this container, overwrite if one already exists with the same URN
        /// </summary>
        /// <param name="dataflowObject">Dataflow Object </param>
        void AddDataflow(IDataflowSuperObject dataflowObject);

        /// <summary>
        ///     Add a data structure object to this container, overwrite if one already exists with the same URN
        /// </summary>
        /// <param name="dataStructureObject">Data structure object. </param>
        void AddDataStructure(IDataStructureSuperObject dataStructureObject);

        /// <summary>
        ///     Add a IHierarchicalCodelistSuperObject to this container, overwrite if one already exists with the same URN
        /// </summary>
        /// <param name="hierarchicalCodelistObject"> HierarchicalCodelist Object </param>
        void AddHierarchicalCodelist(IHierarchicalCodelistSuperObject hierarchicalCodelistObject);

        /// <summary>
        ///     The add maintainable.
        /// </summary>
        /// <param name="maintainableObject">
        ///     The categorySchemeObject.
        /// </param>
        void AddMaintainable(IMaintainableSuperObject maintainableObject);

        /// <summary>
        ///     Add a IProcessSuperObject to this container, overwrite if one already exists with the same URN
        /// </summary>
        /// <param name="processObject"> Process Object </param>
        void AddProcess(IProcessSuperObject processObject);

        /// <summary>
        ///     Add a IProvisionAgreementSuperObject to this container, overwrite if one already exists with the same URN
        /// </summary>
        /// <param name="provisionAgreementObject"> ProvisionAgreement Object </param>
        void AddProvision(IProvisionAgreementSuperObject provisionAgreementObject);

        /// <summary>
        ///     Add a IRegistrationSuperObject to this container, overwrite if one already exists with the same URN
        /// </summary>
        /// <param name="registrationObject">Registration Object </param>
        void AddRegistration(IRegistrationSuperObject registrationObject);

        /// <summary>
        ///     Merges the super
        /// </summary>
        /// <param name="superObjects"> objects Base </param>
        void Merge(ISuperObjects superObjects);

        // REMOVE

        /// <summary>
        ///     Remove the given ICategorySchemeSuperObject from this container, do nothing if it is not in this container
        /// </summary>
        /// <param name="categorySchemeObject">CategoryScheme Object </param>
        void RemoveCategoryScheme(ICategorySchemeSuperObject categorySchemeObject);

        /// <summary>
        ///     Remove the given ICategorySchemeSuperObject from this container, do nothing if it is not in this container
        /// </summary>
        /// <param name="codelistObject">codelist Object </param>
        void RemoveCodelist(ICodelistSuperObject codelistObject);

        /// <summary>
        ///     Remove the given ICategorySchemeSuperObject from this container, do nothing if it is not in this container
        /// </summary>
        /// <param name="conceptSchemeObject">ConceptScheme Object </param>
        void RemoveConceptScheme(IConceptSchemeSuperObject conceptSchemeObject);

        /// <summary>
        ///     Remove the given IDataflowSuperObject from this container, do nothing if it is not in this container
        /// </summary>
        /// <param name="dataflowObject"> Dataflow Object </param>
        void RemoveDataflow(IDataflowSuperObject dataflowObject);

        /// <summary>
        ///     Remove the given IDataStructureSuperObject from this container, do nothing if it is not in this container
        /// </summary>
        /// <param name="dataStructureObject">DataStructure Object </param>
        void RemoveDataStructure(IDataStructureSuperObject dataStructureObject);

        /// <summary>
        ///     Remove the given IHierarchicalCodelistSuperObject from this container, do nothing if it is not in this container
        /// </summary>
        /// <param name="hierarchicalCodelistObject">HierarchicalCodelist Object </param>
        void RemoveHierarchicalCodelist(IHierarchicalCodelistSuperObject hierarchicalCodelistObject);

        /// <summary>
        ///     Remove the given IProcessSuperObject from this container, do nothing if it is not in this container
        /// </summary>
        /// <param name="processObject">Process Object </param>
        void RemoveProcess(IProcessSuperObject processObject);

        /// <summary>
        ///     Remove the given IProvisionAgreementSuperObject from this container, do nothing if it is not in this container
        /// </summary>
        /// <param name="provisionAgreementObject">ProvisionAgreement Object </param>
        void RemoveProvision(IProvisionAgreementSuperObject provisionAgreementObject);

        /// <summary>
        ///     Remove the given IRegistrationSuperObject from this container, do nothing if it is not in this container
        /// </summary>
        /// <param name="registrationObject">Registration Object </param>
        void RemoveRegistration(IRegistrationSuperObject registrationObject);

        // ADD
    }
}