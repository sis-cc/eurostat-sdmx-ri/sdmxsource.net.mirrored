/*******************************************************************************
 *  This software and the accompanying materials of the FMR tools are licensed under the
 *  Apache Licence, version 2.0 (the “Licence”) which accompanies the distribution and is
 *  available at: http://www.apache.org/licenses/LICENSE-2.0
 *  You may not use this file except in compliance with the Licence.
 *  This file is part of the FMR tools and source code libraries.
 *  The software is distributed on an “AS IS” BASIS, WITHOUT WARRANTIES OR
 *  CONDITIONS OF ANY KIND, either express or implied. See the Apache 2.0 Licence for
 *  more details regarding permissions and limitations under the Licence.
 * Copyright © 2022, Bank for International Settlements
 ******************************************************************************/

namespace Org.Sdmxsource.Sdmx.Api.Model.Base
{
    /// <summary>
    /// Defines how two periods overlap.
    /// </summary>
    /// <remarks>
    /// The overlap can be one of the following values:
    /// <list type="bullet">
    ///   <item><see cref="PeriodOverlap.NotInPeriod"/> - Two periods have no overlap.</item>
    ///   <item><see cref="PeriodOverlap.ExactMatch"/> - Two periods match start and end dates exactly.</item>
    ///   <item><see cref="PeriodOverlap.Subset"/> - One period is fully contained within the other; it is a subset.</item>
    ///   <item><see cref="PeriodOverlap.Superset"/> - One period fully contains the other; it is a superset.</item>
    ///   <item><see cref="PeriodOverlap.Overlap"/> - The two periods overlap at either the beginning or the end of the periods; the start and end periods are NOT the same.</item>
    /// </list>
    /// </remarks>

    public enum PeriodOverlap
    {
        NotInPeriod,
        ExactMatch,
        Subset,
        Superset,
        Overlap
    }

}
