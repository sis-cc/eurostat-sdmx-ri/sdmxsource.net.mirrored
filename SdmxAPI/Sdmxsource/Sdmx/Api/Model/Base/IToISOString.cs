namespace Org.Sdmxsource.Sdmx.Api.Model.Base
{
    public interface IToISOString
    {
       
        /// <summary>
        ///  a period string in the syntax start period/end period, exmaple 2001/2009
        /// </summary>
        /// <returns></returns>
        string ToISOString();
    }
}
