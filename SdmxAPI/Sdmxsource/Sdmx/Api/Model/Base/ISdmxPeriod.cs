namespace Org.Sdmxsource.Sdmx.Api.Model.Base
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Defines a StartPeriod and EndPeriod, both start period and end period are optional, however an SdmxPeriod must contain at least one or the other (or both).
    /// Can be compared by first comparing the start period, and then the end period, where a null start period is seen as earlier then a specified start period, and
    /// a null end period is seen as later then a specified end period
    /// </summary>
    public interface ISdmxPeriod : IComparable<ISdmxPeriod>, IToISOString
    {
        /// <summary>
        /// Compares this period with the one provided and returns the result of the comparison.
        /// </summary>
        /// <param name="that">The period to compare this period to. A null value results in a comparison of PERIOD_OVERLAP.NOT_IN_PERIOD.</param>
        /// <returns>A value of the PERIOD_OVERLAP enumeration indicating how this period compares with the one passed in. For example, "Subset" means this period is a subset of that.</returns>
        PeriodOverlap GetOverlap(ISdmxPeriod that);

        /// <summary>
        /// Splits this period into multiple new periods based on the provided period, creating one continuous period.
        /// </summary>
        /// <remarks>
        /// For example, if this period is <strong>2010 - 2020</strong> and the period passed in is <strong>2008 - 2015</strong>,
        /// the returned periods are:
        /// <list type="bullet">
        ///   <item>2008-2009</item>
        ///   <item>2010-2015</item>
        ///   <item>2016-2020</item>
        /// </list>
        /// Where the start of the period and end of the period are applied to the start and end dates, respectively.
        ///
        /// If the passed-in period is an exact match with this period, the returned collection contains only this period.
        /// If the passed-in period does not overlap with this period, both this and that periods are returned in the set.
        /// If the passed-in period is null, this period is returned.
        /// </remarks>
        /// <param name="that">The period to split this period based on.</param>
        /// <returns>A collection of periods resulting from the split operation.</returns>

        ISet<ISdmxPeriod> Split(ISdmxPeriod that);

        /// <summary>
        /// Checks if this period represents all periods.
        /// </summary>
        /// <returns><c>true</c> if this period represents all periods; otherwise, <c>false</c>.</returns>
        bool IsAllPeriods();


        /// <summary>
        /// Returns the start of the period or null if no start is specified.
        /// </summary>
        /// <returns>The start of the period or null.</returns>
        ISdmxDate GetStartPeriod();

        /// <summary>
        /// Returns the end of the period or null if no end is specified.
        /// </summary>
        /// <returns>The end of the period or null.</returns>
        ISdmxDate GetEndPeriod();

        /// <summary>
        /// Checks if there is a start period.
        /// </summary>
        /// <returns><c>true</c> if there is a start period; otherwise, <c>false</c>.</returns>
        bool HasStartPeriod();

        /// <summary>
        /// Checks if there is an end period.
        /// </summary>
        /// <returns><c>true</c> if there is an end period; otherwise, <c>false</c>.</returns>
        bool HasEndPeriod();

        /// <summary>
        /// Checks if the given date is after the end period. If there is no end period, this is deemed as the end of time.
        /// </summary>
        /// <param name="date">The date to compare.</param>
        /// <returns><c>true</c> if the given date is after the end period or if there is no end period; otherwise, <c>false</c>.</returns>
        bool IsBeforeStartPeriod(DateTime date);

        /// <summary>
        /// Determines if the given date is after the end period. If there is no end period, it is considered as the end of time.
        /// </summary>
        /// <param name="date">The date to compare.</param>
        /// <returns><c>true</c> if the given date is after the end period or if there is no end period; otherwise, <c>false</c>.</returns>
        bool IsAfterEndPeriod(DateTime date);

        /// <summary>
        /// Determines if the given date is in the specified period.
        /// </summary>
        /// <param name="date">The date to check.</param>
        /// <returns><c>true</c> if the given date is in the specified period; otherwise, <c>false</c>.</returns>
        bool IsInPeriod(DateTime date);

        /// <summary>
        /// Determines if either of the dates lies within this specified period.
        /// </summary>
        /// <param name="dateFrom">The start date to check.</param>
        /// <param name="dateTo">The end date to check.</param>
        /// <returns><c>true</c> if either of the dates lies within the specified period; otherwise, <c>false</c>.</returns>
        bool IsInPeriod(DateTime dateFrom, DateTime dateTo);

        /// <summary>
        /// Determines if the passed-in SdmxPeriod is not null and the dates specified by the SdmxPeriod argument lie inside or overlap this specified period.
        /// </summary>
        /// <param name="period">The SdmxPeriod to check.</param>
        /// <returns><c>true</c> if the passed-in SdmxPeriod is not null and its dates lie inside or overlap this specified period; otherwise, <c>false</c>.</returns>
        bool IsInPeriod(ISdmxPeriod period);


        /// <summary>
        /// Determines if this period has the same start time as the one passed in.
        /// </summary>
        /// <param name="period">The SdmxPeriod to compare the start time with.</param>
        /// <returns><c>true</c> if this period has the same start time as the one passed in; otherwise, <c>false</c>.</returns>
        bool IsStartEqual(ISdmxPeriod period);

        /// <summary>
        /// Determines if this period starts before the passed-in period. A null start date is treated as starting from the beginning of time.
        /// If the two periods share the same start date, this will return <c>false</c>.
        /// </summary>
        /// <param name="period">The SdmxPeriod to compare the start time with.</param>
        /// <returns><c>true</c> if this period starts before the passed-in period; otherwise, <c>false</c>.</returns>
        bool IsBeforePeriod(ISdmxPeriod period);

        /// <summary>
        /// Determines if this period has the same end time as the one passed in.
        /// </summary>
        /// <param name="period">The SdmxPeriod to compare the end time with.</param>
        /// <returns><c>true</c> if this period has the same end time as the one passed in; otherwise, <c>false</c>.</returns>
        bool IsEndEqual(ISdmxPeriod period);

        /// <summary>
        /// Determines if this period ends after the passed-in period. A null end date is treated as ending at the end of time.
        /// If the two periods share the same end date, this will return <c>false</c>.
        /// </summary>
        /// <param name="period">The SdmxPeriod to compare the end time with.</param>
        /// <returns><c>true</c> if this period ends after the passed-in period; otherwise, <c>false</c>.</returns>
        bool IsAfterPeriod(ISdmxPeriod period);


        /// <summary>
        /// Determines if the passed-in SdmxPeriod is not null and if the dates specified by the SdmxPeriod argument are fully contained within this SdmxPeriod.
        /// </summary>
        /// <param name="period">The SdmxPeriod to check for containment.</param>
        /// <returns><c>true</c> if the passed-in SdmxPeriod is not null and its dates are fully contained within this SdmxPeriod; otherwise, <c>false</c>.</returns>
        bool IsFullyInPeriod(ISdmxPeriod period);
    }
}
