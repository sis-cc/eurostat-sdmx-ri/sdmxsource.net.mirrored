// -----------------------------------------------------------------------
// <copyright file="TimeRange.cs" company="EUROSTAT">
//   Date Created : 2018-04-03
//   Copyright (c) 2012, 2018 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Base
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Text;
    using System.Xml;

    /// <summary>
    /// Holds and parsers the XML subset <c>xs:duration</c> of ISO 8601 duration. SDMX v2.1 supports only the 
    /// </summary>
    public struct TimeDuration
    {
        /// <summary>
        /// The duration designator order
        /// </summary>
        private static readonly char[] _validOrder = new[] { 'Y', 'M', 'D' };

        /// <summary>
        /// The _years.
        /// </summary>
        private readonly int _years;

        /// <summary>
        /// The _months.
        /// </summary>
        private readonly int _months;

        /// <summary>
        /// The _days to seconds.
        /// </summary>
        private readonly TimeSpan _daysToSeconds;

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeDuration"/> struct.
        /// </summary>
        /// <param name="years">The years.</param>
        /// <param name="months">The months.</param>
        /// <param name="daysToSeconds">The days to seconds.</param>
        public TimeDuration(int years, int months, TimeSpan daysToSeconds)
            : this()
        {
            this._years = years;
            this._months = months;
            this._daysToSeconds = daysToSeconds;
        }

        /// <summary>
        /// Gets the Years of the Duration
        /// </summary>
        public int Years
        {
            get
            {
                return this._years;
            }
        }

        /// <summary>
        /// Gets the Months of the Duration
        /// </summary>
        public int Months
        {
            get
            {
                return this._months;
            }
        }

        /// <summary>
        /// Gets the days of the Duration
        /// </summary>
        public int Days
        {
            get
            {
                return this._daysToSeconds.Days;
            }
        }

        /// <summary>
        /// Gets the Hours of the Duration
        /// </summary>
        public int Hours
        {
            get
            {
                return this._daysToSeconds.Hours;
            }
        }

        /// <summary>
        /// Gets the Minutes of the Duration
        /// </summary>
        public int Minutes
        {
            get
            {
                return this._daysToSeconds.Minutes;
            }
        }
        
        /// <summary>
        /// Gets the seconds of the Duration
        /// </summary>
        public int Seconds
        {
            get
            {
                return this._daysToSeconds.Seconds;
            }
        }

        /// <summary>
        /// Gets the milliseconds of the Duration
        /// </summary>
        public int Milliseconds
        {
            get
            {
                return this._daysToSeconds.Milliseconds;
            }
        }
        
        /// <summary>
        /// Implements the == operator.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>The result of the operator.</returns>
        public static bool operator ==(TimeDuration left, TimeDuration right)
        {
            return left.Equals(right);
        }

        /// <summary>
        /// Implements the != operator.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>The result of the operator.</returns>
        public static bool operator !=(TimeDuration left, TimeDuration right)
        {
            return !left.Equals(right);
        }

        /// <summary>
        /// Parse a <c>xs:duration</c> and create a <see cref="TimeDuration"/>
        /// </summary>
        /// <param name="isoDuration">The ISO 8601 Duration, only the <c>xsd:duration</c> subset is supported.</param>
        /// <returns>The <see cref="TimeDuration"/></returns>
        public static bool Check(string isoDuration)
        {
            if (string.IsNullOrWhiteSpace(isoDuration))
            {
                return false;
            }

            var validOrder = new Queue<char>(_validOrder);
            var designatorValues = new List<KeyValuePair<char, int>>();
            using (var pointer = isoDuration.GetEnumerator())
            {
                if (!pointer.MoveNext())
                {
                    return false;
                }

                // negative duration is not supported in SDMX v2.1
                if (pointer.Current == 'P')
                {
                    while (pointer.MoveNext())
                    {
                        if (pointer.Current == 'T')
                        {
                            var timeDurationStart = isoDuration.IndexOf('T');
                            var timeDuration = "P" + isoDuration.Substring(timeDurationStart);
                            try
                            {
                                XmlConvert.ToTimeSpan(timeDuration);
                            }
                            catch (FormatException)
                            {
                                return false;
                            }

                            return true;
                        }
                        else
                        {
                            var pair = GetDesignatorValuePair(pointer);
                            if (pair.Key == default(char))
                            {
                                return false;
                            }

                            if (!SetDesignatorValues(validOrder, pair, designatorValues))
                            {
                                return false;
                            }
                        }
                    }

                    if (designatorValues.Count == 0)
                    {
                        return false;
                    }

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Parse a <c>xs:duration</c> and create a <see cref="TimeDuration"/>
        /// </summary>
        /// <param name="isoDuration">The ISO 8601 Duration, only the <c>xsd:duration</c> subset is supported.</param>
        /// <returns>The <see cref="TimeDuration"/></returns>
        public static TimeDuration Parse(string isoDuration)
        {
            if (isoDuration == null)
            {
                throw new ArgumentNullException("isoDuration");
            }

            if (string.IsNullOrWhiteSpace(isoDuration))
            {
                throw new ArgumentException("Value cannot be null or whitespace.", "isoDuration");
            }

            var validOrder = new Queue<char>(_validOrder);
            var designatorValues = new List<KeyValuePair<char, int>>();
            using (var pointer = isoDuration.GetEnumerator())
            {
                // negative duration is not supported in SDMX v2.1
                if (pointer.MoveNext() && pointer.Current == 'P')
                {
                    while (pointer.MoveNext())
                    {
                        if (pointer.Current == 'T')
                        {
                            var timeDurationStart = isoDuration.IndexOf('T');
                            var timeDuration = "P" + isoDuration.Substring(timeDurationStart);
                            var timeSpan = XmlConvert.ToTimeSpan(timeDuration);
                            return BuildTimeDuration(designatorValues, timeSpan);
                        }
                        else
                        {
                            var pair = GetDesignatorValuePair(pointer);
                            if (pair.Key == default(char))
                            {
                                throw new FormatException("Invalid Duration, missing designator");
                            }

                            if (!SetDesignatorValues(validOrder, pair, designatorValues))
                            {
                                throw new FormatException("Unexpected designator");
                            }
                        }
                    }

                    if (designatorValues.Count == 0)
                    {
                        throw new FormatException("No designator found");
                    }

                    return BuildTimeDuration(designatorValues, TimeSpan.Zero);
                }
            }

            throw new FormatException("Cannot parse provided duration. It must start with 'P' and Negative are not supported.");
        }

        /// <summary>
        /// Gets the duration as timespan.
        /// </summary>
        /// <returns>The <see cref="TimeSpan"/> representation</returns>
        public TimeSpan AsTimeSpan()
        {
            var days = (365 * this._years) + (30 * this._months);
            return this._daysToSeconds.Add(new TimeSpan(days, 0, 0));
        }

        /// <inheritdoc />
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("P");
            if (this._years > 0)
            {
                sb.AppendFormat(CultureInfo.InvariantCulture, "{0}Y", this._years);
            }

            if (this._months > 0)
            {
                sb.AppendFormat(CultureInfo.InvariantCulture, "{0}M", this._months);
            }

            if (this._daysToSeconds != TimeSpan.Zero)
            {
                var daysToSeconds = XmlConvert.ToString(this._daysToSeconds);

                sb.Append(daysToSeconds.Substring(1));
            }

            return sb.ToString();
        }

        /// <summary>
        /// Determines whether the specified <see cref="TimeDuration" /> is equal to this instance.
        /// </summary>
        /// <param name="other">Another object to compare to</param>
        /// <returns><c>true</c> if the specified <see cref="TimeDuration" /> is equal to this instance; otherwise, <c>false</c>.</returns>
        public bool Equals(TimeDuration other)
        {
            return this._years == other._years && this._months == other._months && this._daysToSeconds.Equals(other._daysToSeconds);
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            return obj is TimeDuration && this.Equals((TimeDuration)obj);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = this._years;
                hashCode = (hashCode * 397) ^ this._months;
                hashCode = (hashCode * 397) ^ this._daysToSeconds.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// Sets the designator values.
        /// </summary>
        /// <param name="validOrder">The valid order.</param>
        /// <param name="pair">The pair.</param>
        /// <param name="designatorValues">The designator values.</param>
        /// <returns><c>true</c> if there is valid designator, <c>false</c> otherwise.</returns>
        private static bool SetDesignatorValues(Queue<char> validOrder, KeyValuePair<char, int> pair, List<KeyValuePair<char, int>> designatorValues)
        {
            while (validOrder.Count > 0)
            {
                var current = validOrder.Dequeue();
                if (current == pair.Key)
                {
                    designatorValues.Add(pair);
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Builds the duration of the time.
        /// </summary>
        /// <param name="designatorValues">
        /// The designator values.
        /// </param>
        /// <param name="timeSpan">
        /// The time span.
        /// </param>
        /// <returns>
        /// The <see cref="TimeDuration"/>.
        /// </returns>
        private static TimeDuration BuildTimeDuration(List<KeyValuePair<char, int>> designatorValues, TimeSpan timeSpan)
        {
            var years = GetYears(designatorValues);
            var months = GetMonths(designatorValues);
            var daySpan = GetDaySpan(designatorValues);
            var timeSpanWithDays = timeSpan.Add(daySpan);
            return new TimeDuration(years, months, timeSpanWithDays);
        }

        /// <summary>
        /// Gets the day span.
        /// </summary>
        /// <param name="designatorValues">
        /// The designator values.
        /// </param>
        /// <returns>
        /// The <see cref="TimeSpan"/>.
        /// </returns>
        private static TimeSpan GetDaySpan(List<KeyValuePair<char, int>> designatorValues)
        {
            var days = designatorValues.Find(x => x.Key == 'D').Value;
            if (days > 0)
            {
                var daySpan = new TimeSpan(days, 0, 0, 0);
                return daySpan;
            }

            return TimeSpan.Zero;
        }

        /// <summary>
        /// Gets the months.
        /// </summary>
        /// <param name="designatorValues">The designator values.</param>
        /// <returns>The number of months.</returns>
        private static int GetMonths(List<KeyValuePair<char, int>> designatorValues)
        {
            return designatorValues.Find(x => x.Key == 'M').Value;
        }

        /// <summary>
        /// Gets the years.
        /// </summary>
        /// <param name="designatorValues">The designator values.</param>
        /// <returns>The number of years .</returns>
        private static int GetYears(List<KeyValuePair<char, int>> designatorValues)
        {
            return designatorValues.Find(x => x.Key == 'Y').Value;
        }

        /// <summary>
        /// Gets the designator value pair.
        /// </summary>
        /// <param name="pointer">The pointer.</param>
        /// <returns>The ISO 8601 duration designator and value.</returns>
        private static KeyValuePair<char, int> GetDesignatorValuePair(CharEnumerator pointer)
        {
            var numberBuffer = new StringBuilder();
            while (char.IsNumber(pointer.Current))
            {
                numberBuffer.Append(pointer.Current);
                if (!pointer.MoveNext())
                {
                    return default(KeyValuePair<char, int>);
                }
            }

            int number;
            if (int.TryParse(numberBuffer.ToString(), NumberStyles.None, CultureInfo.InvariantCulture, out number))
            {
                return new KeyValuePair<char, int>(pointer.Current, number);
            }

            return default(KeyValuePair<char, int>);
        }
    }
}