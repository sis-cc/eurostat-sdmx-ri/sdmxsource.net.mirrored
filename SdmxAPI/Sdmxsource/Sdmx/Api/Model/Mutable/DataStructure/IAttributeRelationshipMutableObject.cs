// -----------------------------------------------------------------------
// <copyright file="IAttributeRelationshipMutableObject.cs" company="EUROSTAT">
//   Date Created : 2021-10-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure
{
    using System;
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// The mutable attribute relationship
    /// </summary>
    public interface IAttributeRelationshipMutableObject
    {
        /// <summary>
        /// Gets or sets the list of dimension ids that this attribute references, this is only relevant if <see cref="AttachmentLevel"/> is
        /// <see cref="AttributeAttachmentLevel.DimensionGroup"/>.The returned list is a copy of the underlying list
        /// <p/>
        /// Returns an empty list if this attribute does not reference any dimensions
        /// </summary>
        IList<string> DimensionReferences { get; set; }

        /// <summary>
        /// Gets or sets the attachment group. If <see cref="AttachmentLevel"/> is <see cref="AttributeAttachmentLevel.Group"/> then returns a reference to the Group id that this attribute is attached to.  
        /// Returns null if <see cref="AttachmentLevel"/> is not <see cref="AttributeAttachmentLevel.Group"/>
        /// </summary>
        string AttachmentGroup { get; set; }

        /// <summary>
        /// Gets or sets the primary measure reference. If <see cref="AttachmentLevel"/> is <see cref="AttributeAttachmentLevel.Observation"/> then returns a reference to the Primary Measure id that this attribute is attached to.  
        /// Returns null if <see cref="AttachmentLevel"/> is not <see cref="AttributeAttachmentLevel.Observation"/>
        /// </summary>
        [Obsolete("Not really needed as in SDMX 2.1 it is always 'OBS_VALUE' and in SDMX 3.0.0 is not used")]
        string PrimaryMeasureReference { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="AttributeAttachmentLevel"/> attribute indicating the level to which the attribute is attached in time-series formats
        /// (generic, compact, utility data formats).
        /// Attributes with an attachment level of Group are only available if the data is organised in groups,
        /// and should be used appropriately, as the values may not be communicated if the data is not grouped.
        /// </summary>
        AttributeAttachmentLevel AttachmentLevel { get; set; }
    }
}
