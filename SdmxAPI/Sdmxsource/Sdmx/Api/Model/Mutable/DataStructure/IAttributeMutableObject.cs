// -----------------------------------------------------------------------
// <copyright file="IAttributeMutableObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure
{
    #region Using directives

    using System;
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    #endregion

    /// <summary>
    ///     The AttributeMutableObject interface.
    /// </summary>
    public interface IAttributeMutableObject : IComponentMutableObject, IAttributeRelationshipMutableObject, IUsageMutableObject
    {
        /// <summary>
        ///     Gets or sets the assignment status.
        ///     The assignmentStatus attribute indicates whether a value must be provided for the attribute when sending
        ///     documentation along with the data.
        /// </summary>
        [Obsolete("It is replaced by IUsage.Usage which uses an Enum instead of magic strings")]
        string AssignmentStatus { get; set; }

        /// <summary>
        ///     Gets the concept roles.
        /// </summary>
        IList<IStructureReference> ConceptRoles { get; }

        /// <summary>
        /// MeasureRelationship identifies the measures that the attribute applies to. If this is not used, the attribute
	    /// is assumed to apply to all measures.
	    /// Introduced in SDMX 3.0.0
	    /// Returns the list of measure id's ; else an empty set
        /// </summary>
        ISet<string> MeasureRelationships { get; }

        /// <summary>
        /// Add a measure relationship to this attribute
        /// </summary>
        /// <param name="measure">The measure id</param>
        void AddMeasureRelationship(string measure);

        /// <summary>
        /// Add a Dimension reference
        /// </summary>
        /// <param name="dimensionRef">the dimension id</param>
        /// <returns>This instance</returns>
        IAttributeMutableObject AddDimensionReferences(string dimensionRef);
    }
}