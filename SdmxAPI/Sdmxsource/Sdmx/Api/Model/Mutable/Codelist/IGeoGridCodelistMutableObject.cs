// -----------------------------------------------------------------------
// <copyright file="IGeoGridCodelistMutableObject.cs" company="EUROSTAT">
//   Date Created : 2024-03-13
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist
{
    using System.Collections.Generic;
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;

    #endregion

    /// <summary>
    ///     The CodeMutableObject interface.
    /// </summary>
    public interface IGeoGridCodelistMutableObject : IItemSchemeMutableObject<IGeoGridCodeMutableObject>
    {
        /// <summary>
        ///  Sets or Gets a regular expression string corresponding to the grid definition for the GeoGrid Codelist
        /// </summary>
        string GridDefinition { get; set; }

        /// <summary>
        ///  Gets the Immutable instance
        /// </summary>
        new IGeoGridCodelistObject ImmutableInstance { get; }

        /// <summary>
        ///     Returns the code with the given id, returns null if no such code exists
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The code mutable object</returns>
        IGeoGridCodeMutableObject GetCodeById(string id);

        /// <summary>
        /// Gets the codelist extensions if any.
        /// <p>
        /// Use the <see cref="IList{T}"/> methods to add/remove items.
        /// </p>
	    /// allows for the extension of codelists by referencing the codelists to be extended and providing
        /// inclusion/exclusion rules for selecting the codes to extend.
        /// The order of these is important as it is indicates the order of precedence of the extended codelists for conflict
        /// resolution of codes.However, the prefix property can be used to ensure uniqueness of inherited codes in the
        /// extending codelist, in case conflicting codes must be included.
        /// Introduced in SDMX 3.0.0
        /// </summary>
        IList<ICodelistInheritanceRuleMutableObject> CodelistExtensions { get; }
    }
}