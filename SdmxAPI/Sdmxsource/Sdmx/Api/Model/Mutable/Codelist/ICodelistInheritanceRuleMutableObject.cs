// -----------------------------------------------------------------------
// <copyright file="ICodelistInheritanceRuleMutableObject.cs" company="EUROSTAT">
//   Date Created : 2021-10-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist
{
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;

    /// <summary>
    /// CodelistExtension allows for the extension of codelists by referencing the codelists to be extended and providing
    /// inclusion/exclusion rules for selecting the codes to extend.The order of these is important as it is indicates the
    /// order of precedence of the extended codelists for conflict resolution of codes.However, the prefix property can be
    /// used to ensure uniqueness of inherited codes in the extending codelist, in case conflicting codes must be included.
    /// </summary>
    public interface ICodelistInheritanceRuleMutableObject : IMutableObject
    {
        /// <summary>
        /// Gets or sets the codelist being extended
        /// </summary>
        IStructureReference CodelistRef { get; set; }

        /// <summary>
        /// Gets the prefix.
        /// A reference to a codelist may contain a prefix.
        /// If a prefix is provided, this prefix will be applied to all the codes in the codelist before they are imported into the extended codelist.
        /// Returns The prefix if set; else <c>null</c>
        /// </summary>
        string Prefix { get; set; }

        /// <summary>
        /// Gets or sets the optional code selection.
        /// It contains selection of codes from the referenced(extended) codelist to be included or excluded in the extending codelist.
        /// Returns The code selection if defined; else <c>null</c>
        /// </summary>
        ICodeSelectionMutableObject CodeSelection { get; set; }
    }
}
