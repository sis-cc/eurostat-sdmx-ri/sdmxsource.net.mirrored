using System;
using System.Collections.Generic;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;

namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist
{
    public interface IGeoGridCodeMutableObject : IItemMutableObject
    {
        /// <summary>
        ///     Gets or sets the parent code.
        /// </summary>
        string ParentCode { get; set; }

        /// <summary>
        ///   Sets or Gets the value used to align the code to one cell in the grid
        /// </summary>
        string GeoCell {  get; set; }
    }

}
