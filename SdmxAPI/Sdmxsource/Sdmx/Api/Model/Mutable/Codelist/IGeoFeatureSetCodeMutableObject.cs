using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;

namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist
{
    /// <summary>
    ///  The IGeoFeatureSetCodeMutableObject interface
    /// </summary>
    public interface IGeoFeatureSetCodeMutableObject : IItemMutableObject
    {
        /// <summary>
        ///     Gets or sets the parent code.
        /// </summary>
        string ParentCode { get; set; }

        /// <summary>
        /// geo feature set, representing a set of points defining a feature in a format defined a predefined pattern
        /// </summary>
        string GeoFeatureSet { get; set; }
    }
}
