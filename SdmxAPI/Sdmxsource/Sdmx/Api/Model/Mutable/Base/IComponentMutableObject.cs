// -----------------------------------------------------------------------
// <copyright file="IComponentMutableObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base
{
    using Org.Sdmxsource.Sdmx.Api.Constants;
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    #endregion

    /// <summary>
    ///     The ComponentMutableObject interface.
    /// </summary>
    public interface IComponentMutableObject : IIdentifiableMutableObject, IUsageMutableObject
    {
        /// <summary>
        ///     Gets or sets the concept ref.
        /// </summary>
        IStructureReference ConceptRef { get; set; }

        /// <summary>
        ///     Gets or sets the representation.
        /// </summary>
        IRepresentationMutableObject Representation { get; set; }

        /// <summary>
        ///     Adds a concept role 
        /// </summary>
        /// <param name="conceptRef">The structure reference.</param>
        void AddConceptRole(IStructureReference conceptRef);

        /// <summary>
        /// proxy on IRepresentationMutableObject
        /// </summary>
        IComponentMutableObject SetRepresentationMinOccurs(int? num);
        /// <summary>
        /// proxy on IRepresentationMutableObject
        /// </summary>
        IComponentMutableObject SetRepresentationMaxOccurs(int? num);

        /// <summary>
        /// bypass the IsetRepresentationMutableObject (creates it if null)
        /// <param name="textFormat">.</param>
        /// </summary>
        void SetTextFormat(ITextFormatMutableObject textFormat);

        /// <summary>
        /// bypass the IsetRepresentationMutableObject (creates it if null)
        /// <param name="representationRef">.</param>
        /// </summary>
        void SetEnumeration(IStructureReference representationRef);

        /// <summary>
        /// bypass the IsetRepresentationMutableObject (creates it if null)
        /// <param name="tt">.</param>
        /// </summary>
        ITextFormatMutableObject SetTextType(TextType tt);

        /// <summary>
        /// The assignmentStatus attribute indicates whether a
        /// value must be provided for the attribute when sending documentation along with the data.
        /// @return
        /// </summary>
        bool Mandatory { get; }

        /// <summary>
        /// Set a value indicating the mandatory value
        /// </summary>
        /// <param name="assignmentStatus">True for Mandatory , False for Optional</param>
        /// <returns></returns>
        IComponentMutableObject SetMandatory(bool assignmentStatus);
    }
}