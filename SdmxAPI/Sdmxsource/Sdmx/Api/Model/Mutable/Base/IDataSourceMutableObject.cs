// -----------------------------------------------------------------------
// <copyright file="IDataSourceMutableObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base
{
    using System;

    /// <summary>
    ///     The DataSourceMutableObject interface.
    /// </summary>
    public interface IDataSourceMutableObject : IMutableObject
    {
        /// <summary>
        ///     Gets or sets the data url.
        /// </summary>
        Uri DataUrl { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether rest data source.
        /// </summary>
        bool RESTDatasource { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether simple data source.
        /// </summary>
        bool SimpleDatasource { get; set; }

        /// <summary>
        ///     Gets or sets the WADL url.
        /// </summary>
        Uri WADLUrl { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether web service data source.
        /// </summary>
        bool WebServiceDatasource { get; set; }

        /// <summary>
        ///     Gets or sets the WSDL url.
        /// </summary>
        Uri WSDLUrl { get; set; }
    }
}