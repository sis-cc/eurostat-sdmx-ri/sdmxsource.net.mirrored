// -----------------------------------------------------------------------
// <copyright file="IHierarchyMutableObjectV30.cs" company="EUROSTAT">
//   DateTime Created : 2024-03-06
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Reference;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;

namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public interface IHierarchyMutableObjectV30 : IMaintainableMutableObject
    {
        IHierarchyMutableObjectV30 StripItems(DateTime aDate);

        List<ICodeRefMutableObject> GetHierarchicalCodeBeans();

        void SetHierarchicalCodeBeans(List<ICodeRefMutableObject> codeRefs);

        void AddHierarchicalCodeBean(ICodeRefMutableObject codeRef);

        ILevelMutableObject GetChildLevel();

        void SetChildLevel(ILevelMutableObject level);

        /**
         * If true this indicates that the hierarchy has formal levels. In this case, every code should have a level associated with it.
         * 
         * If false this does not have formal levels.  This hierarchy may still have levels, getLevel() may still return a value, the levels are not formal and the call to a code for its level may return null.
         */
        bool IsFormalLevels();

        void SetFormalLevels(bool value);

        /**
         * Returns a representation of itself in a bean which can not be modified, modifications to the mutable bean
         * after this point will not be reflected in the returned instance of the MaintainableBean.
         * @return
         */
        IHierarchyV30 ImmutableInstance { get; }
    }
}
