// -----------------------------------------------------------------------
// <copyright file="ValidityPeriodMutableObject.cs" company="EUROSTAT">
//   Date Created : 2024-03-13
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
using System;
using Org.Sdmxsource.Sdmx.Api.Model.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;

namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.ValidityPeriod
{
    public interface IValidityPeriodMutableObject : IMutableObject
    {
        IValidityPeriodMutableObject SetStartDate(DateTime startDate);
        IValidityPeriodMutableObject SetEndDate(DateTime endDate);

        DateTime StartDate { get; }
        DateTime EndDate { get; }

        /// <summary>
        ///  Convenience  method to parse this ValidityPeriodBean's start and end date into an  Sdmx Period
        /// </summary>
        ISdmxPeriod SdmxPeriod { get; }

        bool HasValidityPeriod { get; }
    }
}
