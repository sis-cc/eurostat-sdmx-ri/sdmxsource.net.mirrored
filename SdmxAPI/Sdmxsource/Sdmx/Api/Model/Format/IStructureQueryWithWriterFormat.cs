using System;
using System.Collections.Generic;
using System.Text;

namespace Org.Sdmxsource.Sdmx.Api.Model.Format
{
    /// <summary>
    ///  Marker interface, used to describe a structure query format, the implementation is expected to offer more information describing the format
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IStructureQueryWithWriterFormat<T> : IStructureQueryFormat<T>
    {
        /// <summary>
        /// Get the writer
        /// </summary>
        /// <returns></returns>
        T GetWriter();
    }
}
