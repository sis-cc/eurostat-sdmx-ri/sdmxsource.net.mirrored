// -----------------------------------------------------------------------
// <copyright file="IDataQueryV2.cs" company="EUROSTAT">
//   Date Created : 2021-09-07
//   Copyright (c) 2012, 2021 by the European Union.
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
// 
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Api.Model.Data.Query
{
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    
    /// <summary>
    /// This interface extends the <see cref="IDataQuery"/> for the SDMX REST 2.0 data query.
    /// </summary>
    public interface IDataQueryV2 : IDataQuery
    {
        /// <summary>
        /// Allows for the <see cref="IDataQuery"/> to build from multiple rest queries.
        /// </summary>
        IDataQueryIterator StartIterator();

        /// <summary>
        /// A collection of filters for the components
        /// </summary>
        IList<ComponentFilter> ComponentFilters { get; }
    }
}