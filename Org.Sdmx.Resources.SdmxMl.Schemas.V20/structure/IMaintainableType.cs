﻿// -----------------------------------------------------------------------
// <copyright file="IMaintainableType.cs" company="EUROSTAT">
//   Date Created : 2016-03-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of Org.Sdmx.Resources.SdmxMl.Schemas.V20.
//     Org.Sdmx.Resources.SdmxMl.Schemas.V20 is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     Org.Sdmx.Resources.SdmxMl.Schemas.V20 is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with Org.Sdmx.Resources.SdmxMl.Schemas.V20.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common;

    /// <summary>
    /// A common interface for handling of the different Maintainable types in XSD generated classes.
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Reviewed. Suppression is OK here.")]
    public interface IMaintainableType
    {
        /// <summary>
        /// Gets or sets the agency identifier.
        /// </summary>
        /// <value>The agency identifier.</value>
        string agencyID { get; set; }

        /// <summary>
        /// Gets or sets the annotations.
        /// </summary>
        /// <value>The annotations.</value>
        AnnotationsType Annotations { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        IList<TextType> Description { get; set; }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        string id { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is external reference.
        /// </summary>
        /// <value><c>null</c> if [is external reference] contains no value, <c>true</c> if [is external reference]; otherwise, <c>false</c>.</value>
        bool? isExternalReference { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is final.
        /// </summary>
        /// <value><c>null</c> if [is final] contains no value, <c>true</c> if [is final]; otherwise, <c>false</c>.</value>
        bool? isFinal { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        IList<TextType> Name { get; set; }

        /// <summary>
        /// Gets or sets the URI.
        /// </summary>
        /// <value>The URI.</value>
        Uri uri { get; set; }

        /// <summary>
        /// Gets or sets the urn.
        /// </summary>
        /// <value>The urn.</value>
        Uri urn { get; set; }

        /// <summary>
        /// Gets or sets the valid from.
        /// </summary>
        /// <value>The valid from.</value>
        object validFrom { get; set; }

        /// <summary>
        /// Gets or sets the valid to.
        /// </summary>
        /// <value>The valid to.</value>
        object validTo { get; set; }

        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        /// <value>The version.</value>
        string version { get; set; }
    }
}