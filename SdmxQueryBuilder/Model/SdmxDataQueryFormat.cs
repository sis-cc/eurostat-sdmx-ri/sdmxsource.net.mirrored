﻿// -----------------------------------------------------------------------
// <copyright file="SdmxDataQueryFormat.cs" company="EUROSTAT">
//   Date Created : 2013-05-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxQueryBuilder.
//     SdmxQueryBuilder is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxQueryBuilder is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxQueryBuilder.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxQueryBuilder.Model
{
    #region Using Directives

    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;

    #endregion

    /// <summary>
    ///     SdmxDataQueryFormat class
    /// </summary>
    /// <typeparam name="T">
    ///     The type of the output
    /// </typeparam>
    public class SdmxDataQueryFormat<T> : IDataQueryFormat<T>
    {
        /// <summary>
        ///     The _version
        /// </summary>
        private readonly SdmxSchema _version;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxDataQueryFormat{T}" /> class.
        /// </summary>
        /// <param name="version">The version</param>
        /// <exception cref="ArgumentNullException">the version exception</exception>
        public SdmxDataQueryFormat(SdmxSchema version)
        {
            this._version = version;

            if (version == null)
            {
                throw new ArgumentNullException("version");
            }
        }

        /// <summary>
        ///     Gets the version
        /// </summary>
        public SdmxSchema Version
        {
            get
            {
                return this._version;
            }
        }

        /// <summary>
        ///     Get the version string
        /// </summary>
        /// <returns>
        ///     The version string
        /// </returns>
        public override string ToString()
        {
            return this._version.ToString();
        }
    }
}