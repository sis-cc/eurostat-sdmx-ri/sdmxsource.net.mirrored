using System;
using System.Collections.Generic;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Model.Format;

namespace Org.Sdmxsource.Sdmx.SdmxQueryBuilder.Model
{
    public class RestV2QueryWithWriterFormat: IStructureQueryWithWriterFormat<StringBuilder>
    {
        private StringBuilder stringBuilder;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sb"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public RestV2QueryWithWriterFormat(StringBuilder sb)
        {
            if (sb == null)
            {
                throw new ArgumentNullException();
            }

            stringBuilder = sb;
        }

        public StringBuilder GetWriter()
        {
            return stringBuilder;
        }
    }
}
