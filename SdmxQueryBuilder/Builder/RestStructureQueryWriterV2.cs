// -----------------------------------------------------------------------
// <copyright file="StructureQueryBuilderRest.cs" company="EUROSTAT">
//   Date Created : 2013-05-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxQueryBuilder.
//     SdmxQueryBuilder is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxQueryBuilder is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxQueryBuilder.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxQueryBuilder.Builder
{
    using System.Collections.Generic;
    using System.Security.Policy;
    using System.Text;
    using System;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
    using Org.Sdmxsource.Sdmx.SdmxQueryBuilder.Properties;
    using System.Linq;

    public class RestStructureQueryWriterV2 : IStructureQueryWriter<StringBuilder>
    {

        private static Dictionary<SdmxStructureEnumType, string> maint2PathV2 = new Dictionary<SdmxStructureEnumType, string>()
        {
            {SdmxStructureEnumType.DataConsumerScheme, "dataconsumerscheme"},
            {SdmxStructureEnumType.OrganisationUnitScheme, "organisationunitscheme"},
            {SdmxStructureEnumType.Dataflow, "dataflow"},
            {SdmxStructureEnumType.MetadataFlow, "metadataflow"},
            {SdmxStructureEnumType.ReportingTaxonomy, "reportingtaxonomy"},
            {SdmxStructureEnumType.ProvisionAgreement, "provisionagreement"},
            {SdmxStructureEnumType.Process, "process"},
            {SdmxStructureEnumType.Categorisation, "categorisation"},
            {SdmxStructureEnumType.HierarchyV30, "hierarchy"},
            {SdmxStructureEnumType.HierarchyAssociation, "hierarchyassociation"},
            {SdmxStructureEnumType.ValueList, "valuelist"},
            {SdmxStructureEnumType.StructureMapV30, "structuremap"},
            {SdmxStructureEnumType.Dsd, "datastructure" },
            {SdmxStructureEnumType.Msd, "metadatastructure" },
            {SdmxStructureEnumType.CategoryScheme, "categoryscheme" },
            {SdmxStructureEnumType.ConceptScheme, "conceptscheme" },
            {SdmxStructureEnumType.CodeList, "codelist" },
            {SdmxStructureEnumType.OrganisationScheme, "organisationscheme" },
            {SdmxStructureEnumType.AgencyScheme, "agencyscheme"},
            {SdmxStructureEnumType.DataProviderScheme, "dataproviderscheme"},
            {SdmxStructureEnumType.RepresentationMapV30, "representationmap"},
            {SdmxStructureEnumType.CategorySchemeMapV30, "categoryschememap"},
            {SdmxStructureEnumType.ConceptSchemeMapV30, "conceptschememap"},
            {SdmxStructureEnumType.OrganisationSchemeMapV30, "organisationschememap"},
            {SdmxStructureEnumType.ContentConstraint, "dataconstraint"},
            {SdmxStructureEnumType.MetadataConstraint, "metadataconstraint"},
            {SdmxStructureEnumType.Any, "*"},
        };
        private StringBuilder stringBuilder;

        public RestStructureQueryWriterV2(StringBuilder stringBuilder)
        {
            if (stringBuilder == null)
            {
                throw new ArgumentNullException();
            }

            this.stringBuilder = stringBuilder;
        }
        private string GetResource(ICommonStructureQuery structureQuery)
        {
            maint2PathV2.TryGetValue(structureQuery.MaintainableTarget, out var resource);
            if (ObjectUtil.ValidString(resource))
            {
                return resource;
            }
            // copied from SdmxCore
            return structureQuery.MaintainableTarget.UrnClass.ToLower();
        }
        private void AppendPathPart(StringBuilder sb, string append)
        {
            sb.Append(append).Append("/");
        }

        private void AppendPathPart(StringBuilder sb, IList<string> append)
        {
            AppendPathPart(sb, string.Join(",", append));
        }

        private void AppendPathWildCard(StringBuilder sb)
        {
            AppendPathPart(sb, "*");
        }

        public StringBuilder WriteStructureQuery(ICommonStructureQuery query)
        {
            StringBuilder stringBuilder = this.stringBuilder;
            AppendPathPart(stringBuilder, "structure");
            AppendPathPart(stringBuilder, GetResource(query));
            if (query.HasSpecificAgencyId)
            {
                AppendPathPart(stringBuilder, query.AgencyIds);
            }
            else
            {
                AppendPathWildCard(stringBuilder);
            }
            if (query.HasSpecificMaintainableId)
            {
                AppendPathPart(stringBuilder, query.MaintainableIds);
            }
            else
            {
                AppendPathWildCard(stringBuilder);
            }
            if (ObjectUtil.ValidCollection(query.VersionRequests))
            {
                List<string> versions = new List<string>();
                foreach (IVersionRequest versionRequest in query.VersionRequests)
                {
                    if (versionRequest.SpecifiesVersion)
                    {
                        versions.Add(versionRequest.ToString());
                    }
                    else
                    {
                        switch (versionRequest.VersionQueryType)
                        {
                            case VersionQueryTypeEnum.All:
                                versions.Add("*");
                                break;
                            case VersionQueryTypeEnum.AllStable:
                                versions.Add("*+");
                                break;
                            case VersionQueryTypeEnum.Latest:
                                versions.Add("~");
                                break;
                            case VersionQueryTypeEnum.LatestStable:
                                versions.Add("+");
                                break;
                        }
                    }
                }

                AppendPathPart(stringBuilder, versions);
            }
            else
            {
                AppendPathPart(stringBuilder, "~");
            }

            if (ObjectUtil.ValidCollection(query.SpecificItems))
            {
                string itemId = GetItemId(query);
                AppendPathPart(stringBuilder, itemId);
            }

            stringBuilder.Append("?references=");
            switch (query.References.EnumType)
            {
                case StructureReferenceDetailEnumType.Specific:
                    stringBuilder.Append(query.SpecificReferences.Select(x => x.UrnClass).Aggregate((i, j) => i + "," + j).TrimEnd(','));
                    ;
                    break;
                case StructureReferenceDetailEnumType.None:
                case StructureReferenceDetailEnumType.Parents:
                case StructureReferenceDetailEnumType.ParentsSiblings:
                case StructureReferenceDetailEnumType.Ancestors:
                case StructureReferenceDetailEnumType.Children:
                case StructureReferenceDetailEnumType.Descendants:
                case StructureReferenceDetailEnumType.All:
                    stringBuilder.Append(query.References.ToString());
                    break;
            }

            var restDetail = GetStructureQueryDetail(query);
            stringBuilder.Append("&detail=");
            stringBuilder.Append(restDetail.ToString());

            return stringBuilder;
        }

        private static StructureQueryDetail GetStructureQueryDetail(ICommonStructureQuery structureQuery)
        {
            StructureQueryDetail restDetail;

            // In REST request details also apply automatically to
            switch (structureQuery.RequestedDetail.EnumType)
            {
                case ComplexStructureQueryDetailEnumType.Stub:
                    restDetail = StructureQueryDetail.GetFromEnum(StructureQueryDetailEnumType.AllStubs);
                    break;
                case ComplexStructureQueryDetailEnumType.CompleteStub:
                    restDetail = StructureQueryDetail.GetFromEnum(StructureQueryDetailEnumType.AllCompleteStubs);
                    break;
                case ComplexStructureQueryDetailEnumType.Full:
                default:
                    switch (structureQuery.ReferencedDetail.EnumType)
                    {
                        case ComplexMaintainableQueryDetailEnumType.Full:
                            restDetail = StructureQueryDetail.GetFromEnum(StructureQueryDetailEnumType.Full);
                            break;
                        case ComplexMaintainableQueryDetailEnumType.Stub:
                            restDetail = StructureQueryDetail.GetFromEnum(StructureQueryDetailEnumType.ReferencedStubs);
                            break;
                        case ComplexMaintainableQueryDetailEnumType.CompleteStub:
                            restDetail = StructureQueryDetail.GetFromEnum(StructureQueryDetailEnumType.AllCompleteStubs);
                            break;
                        case ComplexMaintainableQueryDetailEnumType.ReferencePartial:
                            restDetail = StructureQueryDetail.GetFromEnum(StructureQueryDetailEnumType.ReferencePartial);
                            break;
                        default:
                            throw new ArgumentException("Unexpected value: " + structureQuery.ReferencedDetail);
                    }
                    break;
            }
            return restDetail;
        }

        private static string GetItemId(ICommonStructureQuery structureQuery)
        {
            List<string> items = new List<string>();
            foreach (var item in structureQuery.SpecificItems)
            {
                List<string> itemPath = new List<string>();
                itemPath.Add(item.Id.SearchParameter);

                while (item.ChildReference != null)
                {
                    var currentItem = item.ChildReference;
                    itemPath.Add(currentItem.Id.SearchParameter);
                }
                items.Add(string.Join(".", itemPath));
            }

            return string.Join(",", items);
        }


    }
}
