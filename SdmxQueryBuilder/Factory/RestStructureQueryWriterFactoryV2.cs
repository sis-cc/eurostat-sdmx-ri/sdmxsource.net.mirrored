using System;
using System.Collections.Generic;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Builder;
using Org.Sdmxsource.Sdmx.Api.Factory;
using Org.Sdmxsource.Sdmx.Api.Model.Format;
using Org.Sdmxsource.Sdmx.SdmxQueryBuilder.Builder;
using Org.Sdmxsource.Sdmx.SdmxQueryBuilder.Model;

namespace Org.Sdmxsource.Sdmx.SdmxQueryBuilder.Factory
{
    public class RestStructureQueryWriterFactoryV2 : IStructureQueryWriterFactory
    {
        public IStructureQueryWriter<T> GetStructureQueryWriter<T>(IStructureQueryWithWriterFormat<T> format)
        {
            if (format is RestV2QueryWithWriterFormat) {
                return (IStructureQueryWriter<T>) new RestStructureQueryWriterV2(((RestV2QueryWithWriterFormat)format).GetWriter());
            }

            return null;
        }

    }
}
