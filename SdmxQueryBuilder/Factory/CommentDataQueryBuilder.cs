﻿// -----------------------------------------------------------------------
// <copyright file="CommentDataQueryBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-08-17
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxQueryBuilder.
// 
//     SdmxQueryBuilder is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxQueryBuilder is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxQueryBuilder.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxQueryBuilder.Factory
{
    using System.Xml.Linq;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Structureparser.Model;

    /// <summary>
    /// A <see cref="IDataQueryBuilder{XDocument}" /> that can insert comments.
    /// </summary>
    public class CommentDataQueryBuilder : IDataQueryBuilder<XDocument>
    {
        /// <summary>
        /// The _data query builder.
        /// </summary>
        private readonly IDataQueryBuilder<XDocument> _dataQueryBuilder;

        /// <summary>
        /// The _tool indicator.
        /// </summary>
        private readonly IToolIndicator _toolIndicator;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommentDataQueryBuilder"/> class. 
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        /// <param name="toolIndicator">
        /// The tool Indicator.
        /// </param>
        /// <param name="dataQueryBuilder">
        /// The data Query Builder.
        /// </param>
        public CommentDataQueryBuilder(IToolIndicator toolIndicator, IDataQueryBuilder<XDocument> dataQueryBuilder)
        {
            this._toolIndicator = toolIndicator;
            this._dataQueryBuilder = dataQueryBuilder;
        }

        /// <summary>
        /// Builds a DataQuery that matches the passed in format
        /// </summary>
        /// <param name="query">
        /// The query
        /// </param>
        /// <returns>
        /// The data query
        /// </returns>
        public XDocument BuildDataQuery(IDataQuery query)
        {
            var doc = this._dataQueryBuilder.BuildDataQuery(query);
            if (this._toolIndicator != null)
            {
                var comm = new XComment(this._toolIndicator.GetComment());
                if (doc.Root != null)
                {
                    doc.Root.FirstNode.AddBeforeSelf(comm);
                }
            }

            return doc;
        }
    }
}