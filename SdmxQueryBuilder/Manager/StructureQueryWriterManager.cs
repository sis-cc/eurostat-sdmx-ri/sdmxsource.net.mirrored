using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Builder;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Factory;
using Org.Sdmxsource.Sdmx.Api.Manager.Query;
using Org.Sdmxsource.Sdmx.Api.Model.Format;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;

namespace Org.Sdmxsource.Sdmx.SdmxQueryBuilder.Manager
{
    public class StructureQueryWriterManager : IStructureQueryWriterManager
    {
        private IStructureQueryWriterFactory[] structureQueryCommonFactories;

	    public StructureQueryWriterManager(IStructureQueryWriterFactory[] structureQueryCommonFactories)
        {
            if(structureQueryCommonFactories == null)
            {
                throw new ArgumentNullException();
            }
            
            if (structureQueryCommonFactories.Count() == 0)
            {
                throw new ArgumentException("Empty structure query common factories");
            }

            this.structureQueryCommonFactories = structureQueryCommonFactories;
        }
 
        public void WriteStructureQuery<T>(ICommonStructureQuery structureQuery, IStructureQueryWithWriterFormat<T> structureQueryFormat)
        {
            foreach (IStructureQueryWriterFactory currentFactory in structureQueryCommonFactories)
            {
                var builder = currentFactory.GetStructureQueryWriter(structureQueryFormat);
                if (builder != null)
                {
                    builder.WriteStructureQuery(structureQuery);
                    return;
                }
            }
            throw new SdmxUnauthorisedException("Unsupported StructureQueryFormat: " + structureQueryFormat);
        }
    }
}
