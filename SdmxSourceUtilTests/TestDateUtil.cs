// -----------------------------------------------------------------------
// <copyright file="TestDateUtil.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtilTests.
//     SdmxSourceUtilTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtilTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtilTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxSourceUtilTests
{
    using System;
    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Date;

    /// <summary>
    /// Test unit for <see cref="DateUtil"/> 
    /// </summary>
    [TestFixture]
    public class TestDateUtil
    {
        /// <summary>
        /// Test the <see cref="DateUtil.GetTimeFormatOfDate"/>
        /// </summary>
        /// <param name="dateStr">
        /// The date string.
        /// </param>
        /// <param name="timeFormat">
        /// The expected time Format.
        /// </param>
        [Test]
        [TestCase("2003", TimeFormatEnumType.Year)]
        [TestCase("1900", TimeFormatEnumType.Year)]
        [TestCase("2500", TimeFormatEnumType.Year)]
        [TestCase("2003-01", TimeFormatEnumType.Month)]
        [TestCase("2003-12", TimeFormatEnumType.Month)]
        [TestCase("2004-12", TimeFormatEnumType.Month)]
        [TestCase("2004-01", TimeFormatEnumType.Month)]
        [TestCase("2004-02", TimeFormatEnumType.Month)]
        [TestCase("2003-02", TimeFormatEnumType.Month)]
        [TestCase("2012-02", TimeFormatEnumType.Month)]
        [TestCase("2004-06", TimeFormatEnumType.Month)]
        [TestCase("2004-W02", TimeFormatEnumType.Week)]
        [TestCase("2004-Q2", TimeFormatEnumType.QuarterOfYear)]
        [TestCase("2004-Q1", TimeFormatEnumType.QuarterOfYear)]
        [TestCase("2004-Q2", TimeFormatEnumType.QuarterOfYear)]
        [TestCase("2004-Q3", TimeFormatEnumType.QuarterOfYear)]
        [TestCase("2012-Q2", TimeFormatEnumType.QuarterOfYear)]
        [TestCase("2012-Q3", TimeFormatEnumType.QuarterOfYear)]
        [TestCase("2004-Q4", TimeFormatEnumType.QuarterOfYear)]
        [TestCase("2004-B1", TimeFormatEnumType.HalfOfYear)]
        [TestCase("2004-B2", TimeFormatEnumType.HalfOfYear)]
        [TestCase("2004-S1", TimeFormatEnumType.HalfOfYear)]
        [TestCase("2004-S2", TimeFormatEnumType.HalfOfYear)]
        [TestCase("2004-W3", TimeFormatEnumType.Week)]
        [TestCase("2004-W33", TimeFormatEnumType.Week)]
        [TestCase("2004-W52", TimeFormatEnumType.Week)]
        [TestCase("2004-W53", TimeFormatEnumType.Week)]
        [TestCase("2004-W01", TimeFormatEnumType.Week)]
        [TestCase("2003-T1", TimeFormatEnumType.ThirdOfYear)]
        [TestCase("2003-T2", TimeFormatEnumType.ThirdOfYear)]
        [TestCase("2003-T3", TimeFormatEnumType.ThirdOfYear)]
        [TestCase("2003-01-02", TimeFormatEnumType.Date)]
        [TestCase("2003-01-01", TimeFormatEnumType.Date)]
        [TestCase("2012-01-01", TimeFormatEnumType.Date)]
        [TestCase("2012-03-01", TimeFormatEnumType.Date)]
        [TestCase("2003-03-01", TimeFormatEnumType.Date)]
        [TestCase("2012-02-29", TimeFormatEnumType.Date)]
        [TestCase("2002-02-28", TimeFormatEnumType.Date)]
        [TestCase("2003-12-31", TimeFormatEnumType.Date)]
        [TestCase("2012-12-31", TimeFormatEnumType.Date)]
        [TestCase("2012-12-31T23:59:59", TimeFormatEnumType.Hour)]
        [TestCase("2012-12-31T00:00:00", TimeFormatEnumType.Hour)]
        [TestCase("2012-01-01T00:00:00", TimeFormatEnumType.Hour)]
        [TestCase("2012-01-01T13:00:00", TimeFormatEnumType.Hour)]
        [TestCase("2012-01-01T13:00:59", TimeFormatEnumType.Hour)]
        [TestCase("2012-01-01T13:00:59Z", TimeFormatEnumType.Hour)]
        [TestCase("2012-01-01T13:00:59-02:00", TimeFormatEnumType.Hour)]
        [TestCase("2012-01-01T13:00:59+02:00", TimeFormatEnumType.Hour)]
        [TestCase("2021-05-04T17:16:19.420+02:00", TimeFormatEnumType.Hour)]
        [TestCase("1920-01", TimeFormatEnumType.Month)]
        [TestCase("1920-M01", TimeFormatEnumType.ReportingMonth)]
        [TestCase("1920-A1", TimeFormatEnumType.ReportingYear)]
        [TestCase("1920-D001", TimeFormatEnumType.ReportingDay)]
        [TestCase("2020-D366", TimeFormatEnumType.ReportingDay)]
        [TestCase("1920-D365", TimeFormatEnumType.ReportingDay)]
        [TestCase("1920-M12", TimeFormatEnumType.ReportingMonth)]
        [TestCase("1920-01-04/P1Y", TimeFormatEnumType.TimeRange)]
        [TestCase("1920-01-04/P1Y3D", TimeFormatEnumType.TimeRange)]
        [TestCase("1920-01-04/P9M", TimeFormatEnumType.TimeRange)]
        [TestCase("1920-01-04/P7D", TimeFormatEnumType.TimeRange)]
        [TestCase("1920-01-04T12:40:30/P7D", TimeFormatEnumType.TimeRange)]
        [TestCase("1920-01-04T12:40:30/P2Y5M9DT12H", TimeFormatEnumType.TimeRange)]
        [TestCase("1920-01-04T12:40:30/PT1H5M2.2S", TimeFormatEnumType.TimeRange)]
        public void TestGetTimeFormatOfDate(string dateStr, TimeFormatEnumType timeFormat)
        {
            var timeFormatOfDate = DateUtil.GetTimeFormatOfDate(dateStr);
            Assert.NotNull(timeFormatOfDate);
            Assert.AreEqual(timeFormat, timeFormatOfDate.EnumType);
        }

        [TestCase("2004-13", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-00", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-1", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-B3", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-Q5", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-Q4PAOKOLE", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-Q0", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-Q", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("20040", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2003-T4", TimeFormatEnumType.ThirdOfYear, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2003-T11", TimeFormatEnumType.ThirdOfYear, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2003-T01", TimeFormatEnumType.ThirdOfYear, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2003-W53", TimeFormatEnumType.Week, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2003-W500", TimeFormatEnumType.Week, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1920-M00", TimeFormatEnumType.ReportingMonth, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("190-M01", TimeFormatEnumType.ReportingMonth, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-M13", TimeFormatEnumType.ReportingMonth, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-M0001", TimeFormatEnumType.ReportingMonth, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-A11", TimeFormatEnumType.ReportingYear, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-A", TimeFormatEnumType.ReportingYear, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-A0", TimeFormatEnumType.ReportingYear, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-D1", TimeFormatEnumType.ReportingDay, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-D11", TimeFormatEnumType.ReportingDay, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-D1111", TimeFormatEnumType.ReportingDay, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-D0001", TimeFormatEnumType.ReportingDay, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-D366", TimeFormatEnumType.ReportingDay, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-W366", TimeFormatEnumType.Week, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-001-02/P1Y", TimeFormatEnumType.TimeRange, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("190-01-02/P1Y", TimeFormatEnumType.TimeRange, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-01-02/1Y", TimeFormatEnumType.TimeRange, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-01-02/P1M1Y", TimeFormatEnumType.TimeRange, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-01-02/-P9D", TimeFormatEnumType.TimeRange, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-01-02/P", TimeFormatEnumType.TimeRange, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-01-00", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-00-01", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-13-01", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-02-30", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-09-31", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-08-32", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2003-02-29", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2012-02-29T24:00:00", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2012-02-29 13:00:00", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        public void TestGetTimeFormatOfDateException(string dateStr, TimeFormatEnumType timeFormat, Type type)
        {
            Assert.Throws(type,() => DateUtil.GetTimeFormatOfDate(dateStr));
        }

        /// <summary>
        /// Test the <see cref="DateUtil"/> <c>FormatDate</c> overloads
        /// </summary>
        /// <param name="dateStr">
        /// The date string.
        /// </param>
        /// <param name="timeFormat">
        /// The expected time Format.
        /// </param>
        [Test]
        [TestCase("2003", TimeFormatEnumType.Year)]
        [TestCase("1900", TimeFormatEnumType.Year)]
        [TestCase("2500", TimeFormatEnumType.Year)]
        [TestCase("2003-01", TimeFormatEnumType.Month)]
        [TestCase("2003-12", TimeFormatEnumType.Month)]
        [TestCase("2004-12", TimeFormatEnumType.Month)]
        [TestCase("2004-01", TimeFormatEnumType.Month)]
        [TestCase("2004-02", TimeFormatEnumType.Month)]
        [TestCase("2003-02", TimeFormatEnumType.Month)]
        [TestCase("2012-02", TimeFormatEnumType.Month)]
        [TestCase("2004-06", TimeFormatEnumType.Month)]
        [TestCase("2004-W02", TimeFormatEnumType.Week)]
        [TestCase("2004-Q2", TimeFormatEnumType.QuarterOfYear)]
        [TestCase("2004-Q1", TimeFormatEnumType.QuarterOfYear)]
        [TestCase("2004-Q2", TimeFormatEnumType.QuarterOfYear)]
        [TestCase("2004-Q3", TimeFormatEnumType.QuarterOfYear)]
        [TestCase("2012-Q2", TimeFormatEnumType.QuarterOfYear)]
        [TestCase("2012-Q3", TimeFormatEnumType.QuarterOfYear)]
        [TestCase("2004-Q4", TimeFormatEnumType.QuarterOfYear)]
        ////[TestCase("2004-B1", TimeFormatEnumType.HalfOfYear)]
        ////[TestCase("2004-B2", TimeFormatEnumType.HalfOfYear)]
        [TestCase("2004-S1", TimeFormatEnumType.HalfOfYear)]
        [TestCase("2004-S2", TimeFormatEnumType.HalfOfYear)]
        [TestCase("2004-W03", TimeFormatEnumType.Week)]
        [TestCase("2004-W33", TimeFormatEnumType.Week)]
        [TestCase("2004-W52", TimeFormatEnumType.Week)]
        [TestCase("2004-W53", TimeFormatEnumType.Week)]
        [TestCase("2004-W01", TimeFormatEnumType.Week)]
        [TestCase("2003-T1", TimeFormatEnumType.ThirdOfYear)]
        [TestCase("2003-T2", TimeFormatEnumType.ThirdOfYear)]
        [TestCase("2003-T3", TimeFormatEnumType.ThirdOfYear)]
        [TestCase("2003-01-02", TimeFormatEnumType.Date)]
        [TestCase("2003-01-01", TimeFormatEnumType.Date)]
        [TestCase("2012-01-01", TimeFormatEnumType.Date)]
        [TestCase("2012-03-01", TimeFormatEnumType.Date)]
        [TestCase("2003-03-01", TimeFormatEnumType.Date)]
        [TestCase("2012-02-29", TimeFormatEnumType.Date)]
        [TestCase("2002-02-28", TimeFormatEnumType.Date)]
        [TestCase("2003-12-31", TimeFormatEnumType.Date)]
        [TestCase("2012-12-31", TimeFormatEnumType.Date)]
        [TestCase("2012-12-31T23:59:59", TimeFormatEnumType.Hour)]
        [TestCase("2012-12-31T00:00:00", TimeFormatEnumType.Hour)]
        [TestCase("2012-01-01T00:00:00", TimeFormatEnumType.Hour)]
        [TestCase("2012-01-01T13:00:00", TimeFormatEnumType.Hour)]
        [TestCase("2012-01-01T13:00:59", TimeFormatEnumType.Hour)]
        [TestCase("1920-01", TimeFormatEnumType.Month)]
        [TestCase("1920-M01", TimeFormatEnumType.ReportingMonth)]
        [TestCase("1920-A1", TimeFormatEnumType.ReportingYear)]
        [TestCase("1920-D001", TimeFormatEnumType.ReportingDay)]
        [TestCase("1920-D365", TimeFormatEnumType.ReportingDay)]
        [TestCase("1920-M12", TimeFormatEnumType.ReportingMonth)]
        public void TestFormatDateRoundTrip(string dateStr, TimeFormatEnumType timeFormat)
        {
            var dateTime = DateUtil.FormatDate(dateStr, true);
            var roundTrip = DateUtil.FormatDate(dateTime, timeFormat);
            Assert.That(roundTrip, Is.EqualTo(dateStr));
        }

        [TestCase("2004-Q4PAOKOLE", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2003-W500", TimeFormatEnumType.Week, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-01-00", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-00-01", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-13-01", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-02-30", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-09-31", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-08-32", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2003-02-29", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2012-02-29T24:00:00", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2012-02-29 13:00:00", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-13", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-00", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-1", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-B3", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-Q5", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-Q0", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-Q", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("20040", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2003-T4", TimeFormatEnumType.ThirdOfYear, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2003-T11", TimeFormatEnumType.ThirdOfYear, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2003-T01", TimeFormatEnumType.ThirdOfYear, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2003-W53", TimeFormatEnumType.Week, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1920-M00", TimeFormatEnumType.ReportingMonth, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("190-M01", TimeFormatEnumType.ReportingMonth, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-M13", TimeFormatEnumType.ReportingMonth, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-M0001", TimeFormatEnumType.ReportingMonth, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-A11", TimeFormatEnumType.ReportingYear, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-A", TimeFormatEnumType.ReportingYear, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-A0", TimeFormatEnumType.ReportingYear, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-D1", TimeFormatEnumType.ReportingDay, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-D11", TimeFormatEnumType.ReportingDay, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-D1111", TimeFormatEnumType.ReportingDay, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-D0001", TimeFormatEnumType.ReportingDay, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-D366", TimeFormatEnumType.ReportingDay, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-W366", TimeFormatEnumType.Week, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        public void TestFormatDateRoundTripException(string dateStr, TimeFormatEnumType timeFormat,Type type)
        {
            Assert.Throws(type, () =>
            {
                var dateTime = DateUtil.FormatDate(dateStr, false);
                var roundTrip = DateUtil.FormatDate(dateTime, timeFormat);
            });
            
        }

        /// <summary>
        /// Using https://www.epochconverter.com/weeks/2000 (and for the other years)
        /// </summary>
        /// <param name="givenDate"></param>
        /// <param name="expectedWeek"></param>
        // [TestCase("2005-01-01", "2004-W53")]
        [TestCase("2005-01-01", "2005-W01")] // 2005-01-01 falls in 2004-W53 so we use the next week
        [TestCase("2001-01-01", "2001-W01")]
        // [TestCase("2000-12-31", "2000-W52")]
        [TestCase("2000-12-31", "2001-W01")] // 2000-12-31 falls in 2000-W52 so we use the next week
        [TestCase("2007-12-31", "2008-W01")]
        [TestCase("2018-05-14", "2018-W20")]
        [TestCase("2018-05-21", "2018-W21")]
        [TestCase("2018-01-01", "2018-W01")]
        public void TestWeekly(string givenDate, string expectedWeek)
        {
            var sdmxDate = new SdmxDateCore(givenDate);
            var date = sdmxDate.Date;
            var week = new SdmxDateCore(date, TimeFormatEnumType.Week);
            Assert.That(week.DateInSdmxFormat, Is.EqualTo(expectedWeek));
        }

        /// <summary>
        /// Test the <see cref="DateUtil.GetTimeFormatOfDate"/>
        /// </summary>
        /// <param name="dateStr">
        /// The date string.
        /// </param>
        /// <param name="timeFormat">
        /// The expected time Format.
        /// </param>
        [Test]
        [TestCase("2003", TimeFormatEnumType.Year)]
        [TestCase("1900", TimeFormatEnumType.Year)]
        [TestCase("2500", TimeFormatEnumType.Year)]
        [TestCase("2003-01", TimeFormatEnumType.Month)]
        [TestCase("2003-12", TimeFormatEnumType.Month)]
        [TestCase("2004-12", TimeFormatEnumType.Month)]
        [TestCase("2004-01", TimeFormatEnumType.Month)]
        [TestCase("2004-02", TimeFormatEnumType.Month)]
        [TestCase("2003-02", TimeFormatEnumType.Month)]
        [TestCase("2012-02", TimeFormatEnumType.Month)]
        [TestCase("2004-06", TimeFormatEnumType.Month)]
        [TestCase("2004-W02", TimeFormatEnumType.Week)]
        [TestCase("2004-Q2", TimeFormatEnumType.QuarterOfYear)]
        [TestCase("2004-Q1", TimeFormatEnumType.QuarterOfYear)]
        [TestCase("2004-Q2", TimeFormatEnumType.QuarterOfYear)]
        [TestCase("2004-Q3", TimeFormatEnumType.QuarterOfYear)]
        [TestCase("2012-Q2", TimeFormatEnumType.QuarterOfYear)]
        [TestCase("2012-Q3", TimeFormatEnumType.QuarterOfYear)]
        [TestCase("2004-Q4", TimeFormatEnumType.QuarterOfYear)]
        ////[TestCase("2004-B1", TimeFormatEnumType.HalfOfYear)]
        ////[TestCase("2004-B2", TimeFormatEnumType.HalfOfYear)]
        [TestCase("2004-S1", TimeFormatEnumType.HalfOfYear)]
        [TestCase("2004-S2", TimeFormatEnumType.HalfOfYear)]
        [TestCase("2004-W03", TimeFormatEnumType.Week)]
        [TestCase("2004-W33", TimeFormatEnumType.Week)]
        [TestCase("2004-W52", TimeFormatEnumType.Week)]
        [TestCase("2004-W53", TimeFormatEnumType.Week)]
        [TestCase("2004-W01", TimeFormatEnumType.Week)]
        [TestCase("2003-T1", TimeFormatEnumType.ThirdOfYear)]
        [TestCase("2003-T2", TimeFormatEnumType.ThirdOfYear)]
        [TestCase("2003-T3", TimeFormatEnumType.ThirdOfYear)]
        [TestCase("2003-01-02", TimeFormatEnumType.Date)]
        [TestCase("2003-01-01", TimeFormatEnumType.Date)]
        [TestCase("2012-01-01", TimeFormatEnumType.Date)]
        [TestCase("2012-03-01", TimeFormatEnumType.Date)]
        [TestCase("2003-03-01", TimeFormatEnumType.Date)]
        [TestCase("2012-02-29", TimeFormatEnumType.Date)]
        [TestCase("2002-02-28", TimeFormatEnumType.Date)]
        [TestCase("2003-12-31", TimeFormatEnumType.Date)]
        [TestCase("2012-12-31", TimeFormatEnumType.Date)]
        [TestCase("2012-12-31T23:59:59", TimeFormatEnumType.Hour)]
        [TestCase("2012-12-31T00:00:00", TimeFormatEnumType.Hour)]
        [TestCase("2012-01-01T00:00:00", TimeFormatEnumType.Hour)]
        [TestCase("2012-01-01T13:00:00", TimeFormatEnumType.Hour)]
        [TestCase("2012-01-01T13:00:59", TimeFormatEnumType.Hour)]
        [TestCase("2012-01-01T13:00:59-02:00", TimeFormatEnumType.Hour)]
        [TestCase("2012-01-01T13:00:59+02:00", TimeFormatEnumType.Hour)]
        [TestCase("2021-05-04T17:16:19.420+02:00", TimeFormatEnumType.Hour, Ignore ="Millisecond writting not supported")]
        [TestCase("1920-01", TimeFormatEnumType.Month)]
        [TestCase("1920-M01", TimeFormatEnumType.ReportingMonth)]
        [TestCase("1920-A1", TimeFormatEnumType.ReportingYear)]
        [TestCase("1920-D001", TimeFormatEnumType.ReportingDay)]
        [TestCase("1920-D365", TimeFormatEnumType.ReportingDay)]
        [TestCase("1920-M12", TimeFormatEnumType.ReportingMonth)]
        public void TestateOffsetDurationRoundTrip(string dateStr, TimeFormatEnumType timeFormat)
        {
            var dateTime = DateUtil.FormatDateOffSetDuration(dateStr);
            var roundTrip = DateUtil.FormatDate(dateTime, TimeFormat.GetFromEnum(timeFormat));
            Assert.That(roundTrip, Is.EqualTo(dateStr));
        }

        [TestCase("2004-Q4PAOKOLE", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2003-W500", TimeFormatEnumType.Week, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-13", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-00", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-1", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-B3", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-Q5", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-Q0", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-Q", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("20040", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-01-00", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-00-01", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-13-01", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-02-30", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-09-31", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-08-32", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2003-02-29", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2012-02-29T24:00:00", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2012-02-29 13:00:00", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2003-T4", TimeFormatEnumType.ThirdOfYear, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2003-T11", TimeFormatEnumType.ThirdOfYear, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2003-T01", TimeFormatEnumType.ThirdOfYear, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1920-M00", TimeFormatEnumType.ReportingMonth, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("190-M01", TimeFormatEnumType.ReportingMonth, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-M13", TimeFormatEnumType.ReportingMonth, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-M0001", TimeFormatEnumType.ReportingMonth, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-A11", TimeFormatEnumType.ReportingYear, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-A", TimeFormatEnumType.ReportingYear, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-A0", TimeFormatEnumType.ReportingYear, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-D1", TimeFormatEnumType.ReportingDay, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-D11", TimeFormatEnumType.ReportingDay, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-D1111", TimeFormatEnumType.ReportingDay, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-D0001", TimeFormatEnumType.ReportingDay, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-D366", TimeFormatEnumType.ReportingDay, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-W366", TimeFormatEnumType.Week, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        public void TestateOffsetDurationRoundTripException(string dateStr, TimeFormatEnumType timeFormat,Type type)
        {
            Assert.Throws(type, () =>
            {
                var dateTime = DateUtil.FormatDateOffSetDuration(dateStr);
                var roundTrip = DateUtil.FormatDate(dateTime, TimeFormat.GetFromEnum(timeFormat));
            });
            
        }

        /// <summary>
        /// Test the <see cref="DateUtil.GetTimeFormatOfDate"/>
        /// </summary>
        /// <param name="dateStr">
        /// The date string.
        /// </param>
        /// <param name="timeFormat">
        /// The expected time Format.
        /// </param>
        [Test]
        [TestCase("2003", TimeFormatEnumType.Year)]
        [TestCase("1900", TimeFormatEnumType.Year)]
        [TestCase("2500", TimeFormatEnumType.Year)]
        [TestCase("2003-01", TimeFormatEnumType.Month)]
        [TestCase("2003-12", TimeFormatEnumType.Month)]
        [TestCase("2004-12", TimeFormatEnumType.Month)]
        [TestCase("2004-01", TimeFormatEnumType.Month)]
        [TestCase("2004-02", TimeFormatEnumType.Month)]
        [TestCase("2003-02", TimeFormatEnumType.Month)]
        [TestCase("2012-02", TimeFormatEnumType.Month)]
        [TestCase("2004-06", TimeFormatEnumType.Month)]
        [TestCase("2004-W02", TimeFormatEnumType.Week)]
        [TestCase("2004-Q2", TimeFormatEnumType.QuarterOfYear)]
        [TestCase("2004-Q1", TimeFormatEnumType.QuarterOfYear)]
        [TestCase("2004-Q2", TimeFormatEnumType.QuarterOfYear)]
        [TestCase("2004-Q3", TimeFormatEnumType.QuarterOfYear)]
        [TestCase("2012-Q2", TimeFormatEnumType.QuarterOfYear)]
        [TestCase("2012-Q3", TimeFormatEnumType.QuarterOfYear)]
        [TestCase("2004-Q4", TimeFormatEnumType.QuarterOfYear)]
        ///// SDMX v2.0 format
        ////[TestCase("2004-B1", TimeFormatEnumType.HalfOfYear)]
        ////[TestCase("2004-B2", TimeFormatEnumType.HalfOfYear)]
        [TestCase("2004-S1", TimeFormatEnumType.HalfOfYear)]
        [TestCase("2004-S2", TimeFormatEnumType.HalfOfYear)]
        [TestCase("2004-W03", TimeFormatEnumType.Week)]
        [TestCase("2004-W33", TimeFormatEnumType.Week)]
        [TestCase("2004-W52", TimeFormatEnumType.Week)]
        [TestCase("2004-W53", TimeFormatEnumType.Week)]
        [TestCase("2004-W01", TimeFormatEnumType.Week)]
        [TestCase("2003-T1", TimeFormatEnumType.ThirdOfYear)]
        [TestCase("2003-T2", TimeFormatEnumType.ThirdOfYear)]
        [TestCase("2003-T3", TimeFormatEnumType.ThirdOfYear)]
        [TestCase("2003-01-02", TimeFormatEnumType.Date)]
        [TestCase("2003-01-01", TimeFormatEnumType.Date)]
        [TestCase("2012-01-01", TimeFormatEnumType.Date)]
        [TestCase("2012-03-01", TimeFormatEnumType.Date)]
        [TestCase("2003-03-01", TimeFormatEnumType.Date)]
        [TestCase("2012-02-29", TimeFormatEnumType.Date)]
        [TestCase("2002-02-28", TimeFormatEnumType.Date)]
        [TestCase("2003-12-31", TimeFormatEnumType.Date)]
        [TestCase("2012-12-31", TimeFormatEnumType.Date)]
        [TestCase("2012-12-31T23:59:59", TimeFormatEnumType.Hour)]
        [TestCase("2012-12-31T00:00:00", TimeFormatEnumType.Hour)]
        [TestCase("2012-01-01T00:00:00", TimeFormatEnumType.Hour)]
        [TestCase("2012-01-01T13:00:00", TimeFormatEnumType.Hour)]
        [TestCase("2012-01-01T13:00:59", TimeFormatEnumType.Hour)]
        [TestCase("2012-01-01T13:00:59-02:00", TimeFormatEnumType.Hour)]
        [TestCase("2012-01-01T13:00:59+02:00", TimeFormatEnumType.Hour)]
        [TestCase("1920-01", TimeFormatEnumType.Month)]
        [TestCase("1920-M01", TimeFormatEnumType.ReportingMonth)]
        [TestCase("1920-A1", TimeFormatEnumType.ReportingYear)]
        [TestCase("1920-D001", TimeFormatEnumType.ReportingDay)]
        [TestCase("1920-D365", TimeFormatEnumType.ReportingDay)]
        [TestCase("1920-M12", TimeFormatEnumType.ReportingMonth)]
        public void TestFormatDateOffsetRoundTrip(string dateStr, TimeFormatEnumType timeFormat)
        {
            var dateTime = DateUtil.FormatDateWithOffSet(dateStr, true);
            var roundTrip = DateUtil.FormatDate(dateTime, timeFormat);
            Assert.That(roundTrip, Is.EqualTo(dateStr));
        }

        [TestCase("2012-12-31T00:00:00/P1M", "P1M")]
        [TestCase("2012-12-31T00:00:00/P1Y1M", "P1Y1M")]
        [TestCase("2012-12-31T00:00:00/P1Y1MT22H", "P1Y1MT22H")]
        public void TestFormatDateOffsetTimeRangeDuration(string dateStr, string expectedDuration)
        {
            var dateTime = DateUtil.FormatDateOffSetDuration(dateStr);
            Assert.That(dateTime.Duration.ToString(), Is.EqualTo(expectedDuration));
        }

        [TestCase("2012-12-31T00:00:00/P1M", 2012, 12, 31)]
        [TestCase("2012-12-31T00:00:00Z/P1M", 2012, 12, 31)]
        [TestCase("2012-12-31T00:00:00+03:00/P1M", 2012, 12, 31)]
        [TestCase("2012-12-31/P1M", 2012, 12, 31)]
        [TestCase("2012-01-01T00:00:00/P1Y1M", 2012, 01, 01)]
        [TestCase("2020-02-29T00:00:00/P1Y1MT22H", 2020, 02, 29)]
        [TestCase("2020-02-29/P1Y1MT22H", 2020, 02, 29)]
        public void TestFormatDateOffsetTimeRangeDate(string dateStr, int expectedYear, int expectedMonth, int expectedDate)
        {
            var dateTime = DateUtil.FormatDateOffSetDuration(dateStr);
            Assert.That(dateTime.Date.Year, Is.EqualTo(expectedYear));
            Assert.That(dateTime.Date.Month, Is.EqualTo(expectedMonth));
            Assert.That(dateTime.Date.Day, Is.EqualTo(expectedDate));
        }

        [TestCase("2012-12-31T00:00:00/P1M", 0)]
        [TestCase("2012-12-31T00:00:00Z/P1M", 0)]
        [TestCase("2012-12-31T00:00:00+03:00/P1M", 3)]
        [TestCase("2012-12-31T00:00:00-03:00/P1M", -3)]
        [TestCase("2012-12-31/P1M", 0)]
        [TestCase("2012-01-01T00:00:00/P1Y1M", 0)]
        [TestCase("2020-02-29T00:00:00/P1Y1MT22H", 0)]
        [TestCase("2020-02-29/P1Y1MT22H", 0)]
        public void TestFormatDateOffsetTimeRangeOffset(string dateStr, int expectedHourOffset)
        {
            var dateTime = DateUtil.FormatDateOffSetDuration(dateStr);
            Assert.That(dateTime.Date.Offset.Hours, Is.EqualTo(expectedHourOffset));
        }

        [TestCase("2004-Q4PAOKOLE", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2003-W500", TimeFormatEnumType.Week, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-13", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-00", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-1", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-B3", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-Q5", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-Q0", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-Q", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("20040", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-01-00", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-00-01", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-13-01", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-02-30", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-09-31", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2004-08-32", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2003-02-29", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2012-02-29T24:00:00", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2012-02-29 13:00:00", TimeFormatEnumType.Null, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2003-W53", TimeFormatEnumType.Week, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2003-T4", TimeFormatEnumType.ThirdOfYear, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2003-T11", TimeFormatEnumType.ThirdOfYear, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("2003-T01", TimeFormatEnumType.ThirdOfYear, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1920-M00", TimeFormatEnumType.ReportingMonth, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("190-M01", TimeFormatEnumType.ReportingMonth, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-M13", TimeFormatEnumType.ReportingMonth, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-M0001", TimeFormatEnumType.ReportingMonth, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-A11", TimeFormatEnumType.ReportingYear, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-A", TimeFormatEnumType.ReportingYear, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-A0", TimeFormatEnumType.ReportingYear, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-D1", TimeFormatEnumType.ReportingDay, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-D11", TimeFormatEnumType.ReportingDay, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-D1111", TimeFormatEnumType.ReportingDay, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-D0001", TimeFormatEnumType.ReportingDay, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-D366", TimeFormatEnumType.ReportingDay, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        [TestCase("1990-W366", TimeFormatEnumType.Week, typeof(Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException))]
        public void TestFormatDateOffsetRoundTripException(string dateStr, TimeFormatEnumType timeFormat,Type type)
        {
            Assert.Throws(type, () =>
            {
                var dateTime = DateUtil.FormatDateWithOffSet(dateStr, true);
                var roundTrip = DateUtil.FormatDate(dateTime, timeFormat);
            });
            
        }

        [TestCase("P1Y", 1, 0, 0, 0, 0, 0, 0)]
        [TestCase("P9Y", 9, 0, 0, 0, 0, 0, 0)]
        [TestCase("P9YT3M", 9, 0, 0, 0, 3, 0, 0)]
        [TestCase("P9Y3M", 9, 3, 0, 0, 0, 0, 0)]
        [TestCase("P3M", 0, 3, 0, 0, 0, 0, 0)]
        [TestCase("P3M7D", 0, 3, 7, 0, 0, 0, 0)]
        [TestCase("P1Y7D", 1, 0, 7, 0, 0, 0, 0)]
        [TestCase("PT12H30M", 0, 0, 0, 12, 30, 0, 0)]
        [TestCase("P1YT12H30M", 1, 0, 0, 12, 30, 0, 0)]
        [TestCase("P1Y1M1DT12H30M", 1, 1, 1, 12, 30, 0, 0)]
        [TestCase("P1Y1M1DT12H30M20.4S", 1, 1, 1, 12, 30, 20, 400)]
        public void ShouldParseValidDuration(string isoDuration, int years, int months, int days, int hours, int minutes, int seconds,  int milliseconds)
        {
            var duration = TimeDuration.Parse(isoDuration);
            Assert.That(duration.Years, Is.EqualTo(years));
            Assert.That(duration.Months, Is.EqualTo(months));
            Assert.That(duration.Days, Is.EqualTo(days));
            Assert.That(duration.Hours, Is.EqualTo(hours));
            Assert.That(duration.Minutes, Is.EqualTo(minutes));
            Assert.That(duration.Seconds, Is.EqualTo(seconds));
            Assert.That(duration.Milliseconds, Is.EqualTo(milliseconds));
        }

        [TestCase("P1Y", true)]
        [TestCase("P9Y", true)]
        [TestCase("P9YT3M", true)]
        [TestCase("P9Y3M", true)]
        [TestCase("P3M", true)]
        [TestCase("P3M7D", true)]
        [TestCase("P1Y7D", true)]
        [TestCase("PT12H30M", true)]
        [TestCase("P1YT12H30M", true)]
        [TestCase("P1Y1M1DT12H30M", true)]
        [TestCase("P1Y1M1DT12H30M20.4S", true)]
        [TestCase("-P1Y", false)]
        [TestCase("P-9Y", false)]
        [TestCase("PT3M1Y", false)]
        [TestCase("P3M3Y", false)]
        [TestCase("PM3M", false)]
        [TestCase("P3MM", false)]
        [TestCase("PM3", false)]
        [TestCase("P3", false)]
        [TestCase("P7DT3", false)]
        [TestCase("PT12H30M1Y", false)]
        [TestCase("PT12H30M1D", false)]
        [TestCase("P12H30M", false)]
        [TestCase("P", false)]
        [TestCase("P1M1Y", false)]
        [TestCase("P1Y1.1M1DT12H30M20.4S", false)]
        public void ShouldTimeDurationCheck(string isoDuration, bool isValid)
        {
            var check = TimeDuration.Check(isoDuration);
            Assert.That(check, Is.EqualTo(isValid));
        }

        [TestCase("-P1Y")]
        [TestCase("P-9Y")]
        [TestCase("PT3M1Y")]
        [TestCase("P3M3Y")]
        [TestCase("PM3M")]
        [TestCase("P3MM")]
        [TestCase("PM3")]
        [TestCase("P3")]
        [TestCase("P7DT3")]
        [TestCase("PT12H30M1Y")]
        [TestCase("PT12H30M1D")]
        [TestCase("P12H30M")]
        [TestCase("P")]
        [TestCase("P1M1Y")]
        [TestCase("P1Y1.1M1DT12H30M20.4S")]
        public void ShouldThrowTimeDuration(string isoDuration)
        {
            Assert.Throws<FormatException>(() => TimeDuration.Parse(isoDuration));
        }

        [TestCase("2005-01-01/2005-01-31", "2005-01-01/P30D")]
        [TestCase("2005-01-01/2005-02-01", "2005-01-01/P1M")]
        [TestCase("2005-11-28/2005-12-01", "2005-11-28/P3D")]
        [TestCase("2015-01-01/2016-01-01", "2015-01-01/P1Y")]
        [TestCase("2015-01-01/2015-12-31", "2015-01-01/P11M30D")]
        [TestCase("2015-01-01/2015-01-02", "2015-01-01/P1D")]
        [TestCase("2015-01-01/2015-02-02", "2015-01-01/P1M1D")]
        [TestCase("2007-03-01T13:00:00Z/2008-05-11T15:30:00Z", "2007-03-01T13:00:00/P1Y2M10DT2H30M")]
        [TestCase("P1Y2M10DT2H30M/2008-05-11T15:30:00Z", "2007-03-01T13:00:00/P1Y2M10DT2H30M")]
        public void ShouldNormizeTimeRange(string timeRange, string expectedValue)
        {
            var normalizeTimeRange = DateUtil.NormalizeTimeRange(timeRange);
            var result = DateUtil.FormatDate(normalizeTimeRange, TimeFormat.TimeRange);
            Assert.That(result, Is.EqualTo(expectedValue));
        }

        [TestCase("2015", "2015-01-01")]
        [TestCase("2015-S1", "2015-01-01")]
        [TestCase("2016-S2", "2016-07-01")]
        [TestCase("2015-Q1", "2015-01-01")]
        [TestCase("2015-Q2", "2015-04-01")]
        [TestCase("2015-Q3", "2015-07-01")]
        [TestCase("2015-Q4", "2015-10-01")]
        [TestCase("2015-02" ,"2015-02-01")]
        [TestCase("2015-10", "2015-10-01")]
        [TestCase("2016-02", "2016-02-01")]
        [TestCase("2016-12", "2016-12-01")]
        [TestCase("2004-W01", "2003-12-29")]
        [TestCase("2017-W01", "2017-01-02")]
        [TestCase("2019-W01", "2018-12-31")]
        [TestCase("2015-01-04", "2015-01-04")]
        public void PeriodStart(string dateStr, string expectedDateString)
        {
            var resultDateString = DateUtil.FormatDate(DateUtil.FormatDateWithOffSet(dateStr, true), TimeFormatEnumType.Date);
            Assert.AreEqual(expectedDateString, resultDateString);
        }


        [TestCase("2015", "2015-12-31")]
        [TestCase("2015-S1", "2015-06-30")]
        [TestCase("2016-S2", "2016-12-31")]
        [TestCase("2015-Q1", "2015-03-31")]
        [TestCase("2015-Q2", "2015-06-30")]
        [TestCase("2015-Q3", "2015-09-30")]
        [TestCase("2015-Q4", "2015-12-31")]
        [TestCase("2015-02", "2015-02-28")]
        [TestCase("2015-10", "2015-10-31")]
        [TestCase("2016-02", "2016-02-29")]
        [TestCase("2016-12", "2016-12-31")]
        [TestCase("2004-W01", "2004-01-04")]
        [TestCase("2017-W01", "2017-01-08")]
        [TestCase("2019-W01", "2019-01-06")]
        [TestCase("2015-01-04", "2015-01-04")]
        public void PeriodEnd(string dateStr, string expectedDateString)
        {
            var resultDateString = DateUtil.FormatDate(DateUtil.FormatDateWithOffSet(dateStr, false), TimeFormatEnumType.Date);
            Assert.AreEqual(expectedDateString, resultDateString);
        }

        /*
762 Gregorian Period
763 Query Parameter: Greater than 2010
764 Literal Interpretation: Any data where the start period occurs after 2010-12-
765 31T23:59:59.
766 Example Matches:
767 • 2011 or later
768 • 2011-01 or later
769 • 2011-01-01 or later
770 • 2011-01-01/P[Any Duration] or any later start date
771 • 2011-[Any reporting period] (any reporting year start day)
772 • 2010-S2 (reporting year start day --07-01 or later)
773 • 2010-T3 (reporting year start day --07-01 or later)
774 • 2010-Q3 or later (reporting year start day --07-01 or later)
775 • 2010-M07 or later (reporting year start day --07-01 or later)
776 • 2010-W28 or later (reporting year start day --07-01 or later)
777 • 2010-D185 or later (reporting year start day --07-01 or later)
778
779 Reporting Period
26
780 Query Parameter: Greater than or equal to 2010-Q3
781 Literal Interpretation: Any data with a reporting period where the start period is on
782 or after the start period of 2010-Q3 for the same reporting year start day, or and
783 data where the start period is on or after 2010-07-01.
784 Example Matches:
785 • 2011 or later
786 • 2010-07 or later
787 • 2010-07-01 or later
788 • 2010-07-01/P[Any Duration] or any later start date
789 • 2011-[Any reporting period] (any reporting year start day)
790 • 2010-S2 (any reporting year start day)
791 • 2010-T3 (any reporting year start day)
792 • 2010-Q3 or later (any reporting year start day)
793 • 2010-M07 or later (any reporting year start day)
794 • 2010-W27 or later (reporting year start day --01-01)5 
795 • 2010-D182 or later (reporting year start day --01-01)
796 • 2010-W28 or later (reporting year start day --07-01)6 
797 • 2010-D185 or later (reporting year start day --07-01)
        */

        // TODO the following tests are correct but some will fail due to 
        [TestCase("2001-10", TimeFormatEnumType.Year, "2002")]
        [TestCase("2001-10", TimeFormatEnumType.ThirdOfYear, "2002-T1")]
        [TestCase("2001-10-01", TimeFormatEnumType.Year, "2002")]
        [TestCase("2001-10-05", TimeFormatEnumType.Month, "2001-11")]
        [TestCase("2001-10-05", TimeFormatEnumType.ReportingMonth, "2001-M11")]
        [TestCase("2001-10", TimeFormatEnumType.Week, "2001-W40")]
        [TestCase("2010-Q3", TimeFormatEnumType.Year, "2011")]
        [TestCase("2010-Q3", TimeFormatEnumType.ReportingYear, "2011-A1")]
        [TestCase("2010-Q3", TimeFormatEnumType.Month, "2010-07")]
        [TestCase("2010-Q3", TimeFormatEnumType.ReportingMonth, "2010-M07")]
        [TestCase("2010-Q3", TimeFormatEnumType.Date, "2010-07-01")]
        [TestCase("2010-Q3", TimeFormatEnumType.HalfOfYear, "2010-S2")]
        [TestCase("2010-Q3", TimeFormatEnumType.ThirdOfYear, "2010-T3")]
        [TestCase("2010-Q3", TimeFormatEnumType.Week, "2010-W27")]
        [TestCase("2010-Q3", TimeFormatEnumType.ReportingDay, "2010-D182")]
        [TestCase("2010-Q1", TimeFormatEnumType.Year, "2010")]
        [TestCase("2010-Q1", TimeFormatEnumType.Month, "2010-01")]
        [TestCase("2010-Q1", TimeFormatEnumType.ReportingMonth, "2010-M01")]
        [TestCase("2010-Q1", TimeFormatEnumType.Date, "2010-01-01")]
        [TestCase("2010-Q1", TimeFormatEnumType.HalfOfYear, "2010-S1")]
        [TestCase("2010-Q1", TimeFormatEnumType.ThirdOfYear, "2010-T1")]
        [TestCase("2010-Q1", TimeFormatEnumType.Week, "2010-W01")]
        [TestCase("2010-Q1", TimeFormatEnumType.ReportingDay, "2010-D001")]
        [TestCase("2001-01-04", TimeFormatEnumType.HalfOfYear, "2001-S2")]
        [TestCase("2001-01-04", TimeFormatEnumType.QuarterOfYear, "2001-Q2")]
        [TestCase("2001-01-04", TimeFormatEnumType.Year, "2002")]

        public void StartTimeQueryShouldTransformCorrectly(string query, TimeFormatEnumType frequencyToTest, string expectedPeriod)
        {
            var givenDate = new SdmxDateCore(query);

            var resultDateString = DateUtil.FormatDate(givenDate.Date, frequencyToTest);
            Assert.AreEqual(expectedPeriod, resultDateString);
        }
    }
}