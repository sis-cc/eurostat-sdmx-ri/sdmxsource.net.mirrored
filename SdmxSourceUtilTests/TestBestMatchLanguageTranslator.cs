namespace SdmxSourceUtilTests
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Translator;

    /// <summary>
    /// Test unit for <see cref="BestMatchLanguageTranslator"/>.
    /// </summary>
    [TestFixture]
    public class TestBestMatchLanguageTranslator
    {
        private readonly CultureInfo _huCultureInfo = CultureInfo.GetCultureInfo("hu");
        private readonly CultureInfo _huHuCultureInfo = CultureInfo.GetCultureInfo("hu-HU");
        private readonly CultureInfo _frCultureInfo = CultureInfo.GetCultureInfo("fr");
        private readonly CultureInfo _frFrCultureInfo = CultureInfo.GetCultureInfo("fr-Fr");
        private readonly CultureInfo _enCultureInfo = CultureInfo.GetCultureInfo("en");
        private readonly CultureInfo _enGbCultureInfo = CultureInfo.GetCultureInfo("en-GB");
        private readonly CultureInfo _enUsCultureInfo = CultureInfo.GetCultureInfo("en-US");
        private const string HuValue = "huValue";
        private const string HuHuValue = "hu-HuValue";
        private const string FrValue = "frValue";
        private const string FrFrValue = "frValue";
        private const string EnValue = "enValue";
        private const string EnGbValue = "en-GBValue";
        private const string EnUsValue = "en-USValue";

        /// <summary>
        /// Test <see cref="PreferedLanguageTranslator"/> constructor.
        /// </summary>
        [Test]
        public void TestBestMatchLanguageTranslatorValidation()
        {
            Assert.DoesNotThrow(() => new BestMatchLanguageTranslator(new List<CultureInfo>() { _enCultureInfo }, new List<CultureInfo>() { _enCultureInfo }, _enCultureInfo));
            Assert.DoesNotThrow(() => new BestMatchLanguageTranslator(null, (ISdmxObjects) null, _enCultureInfo));
            Assert.DoesNotThrow(() => new BestMatchLanguageTranslator(null, new List<CultureInfo>() { _enCultureInfo }, _enCultureInfo));

            Assert.Throws<ArgumentNullException>(() => new BestMatchLanguageTranslator(new List<CultureInfo>() { _enCultureInfo }, (List<CultureInfo>) null, null));
            Assert.Throws<ArgumentNullException>(() => new BestMatchLanguageTranslator(new List<CultureInfo>() { _enCultureInfo }, new List<CultureInfo>() { _enCultureInfo }, null));
            Assert.Throws<ArgumentNullException>(() => new BestMatchLanguageTranslator(null, (ISdmxObjects) null, null));
        }

        /// <summary>
        /// Test GetTranslationFromCultureInfoDictionary method.
        /// </summary>
        [Test]
        public void TestGetTranslationFromCultureInfoDictionary()
        {
            var translator = new BestMatchLanguageTranslator(
                new List<CultureInfo>() {
                    _enUsCultureInfo,
                    _enCultureInfo,
                    _huCultureInfo
                },
                new List<CultureInfo>() {
                    _enUsCultureInfo,
                    _enCultureInfo,
                    _huCultureInfo
                },
                _enCultureInfo);

            var dict = new Dictionary<CultureInfo, string>(){{_frCultureInfo, FrValue}};
            Assert.AreEqual(string.Empty, translator.GetTranslation(dict));

            dict.Add(_huHuCultureInfo, HuHuValue);
            Assert.AreEqual(HuHuValue, translator.GetTranslation(dict));

            dict.Add(_enCultureInfo, EnValue);
            Assert.AreEqual(EnValue, translator.GetTranslation(dict));

            dict.Add(_enGbCultureInfo, EnGbValue);
            Assert.AreEqual(EnValue, translator.GetTranslation(dict));

            dict.Add(_enUsCultureInfo, EnUsValue);
            Assert.AreEqual(EnUsValue, translator.GetTranslation(dict));
        }

        /// <summary>
        /// Test GetTranslationFromTextTypeWrapperList method.
        /// </summary>
        [Test]
        public void TestGetTranslationFromTextTypeWrapperList()
        {
            var translator = new BestMatchLanguageTranslator(
                new List<CultureInfo>() {
                    _enUsCultureInfo,
                    _enCultureInfo,
                    _huCultureInfo
                },
                new List<CultureInfo>() {
                    _enUsCultureInfo,
                    _enCultureInfo,
                    _huCultureInfo
                },
                _enCultureInfo);


            var textTypeWrapperList = new List<ITextTypeWrapper> {
                new TextTypeWrapperImpl(_frCultureInfo.Name, FrValue, null)
            };
            Assert.AreEqual(string.Empty, translator.GetTranslation(textTypeWrapperList));

            textTypeWrapperList.Add(new TextTypeWrapperImpl(_huHuCultureInfo.Name, HuHuValue, null));
            Assert.AreEqual(HuHuValue, translator.GetTranslation(textTypeWrapperList));

            textTypeWrapperList.Add(new TextTypeWrapperImpl(_huCultureInfo.Name, HuValue, null));
            Assert.AreEqual(HuValue, translator.GetTranslation(textTypeWrapperList));

            textTypeWrapperList.Add(new TextTypeWrapperImpl(_enCultureInfo.Name, EnValue, null));
            Assert.AreEqual(EnValue, translator.GetTranslation(textTypeWrapperList));

            textTypeWrapperList.Add(new TextTypeWrapperImpl(_enGbCultureInfo.Name, EnGbValue, null));
            Assert.AreEqual(EnValue, translator.GetTranslation(textTypeWrapperList));

            textTypeWrapperList.Add(new TextTypeWrapperImpl(_enUsCultureInfo.Name, EnUsValue, null));
            Assert.AreEqual(EnUsValue, translator.GetTranslation(textTypeWrapperList));
        }
    }
}