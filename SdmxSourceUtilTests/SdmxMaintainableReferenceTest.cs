// -----------------------------------------------------------------------
// <copyright file="SdmxMaintainableReferenceTest.cs" company="EUROSTAT">
//   Date Created : 2019-11-25
//   Copyright (c) 2012, 2019 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtilTests.
// 
//     SdmxSourceUtilTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtilTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtilTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Util.Sdmx;

namespace SdmxSourceUtilTests
{
    public class SdmxMaintainableReferenceTest
    {
        // TODO better checks
        [TestCase(SdmxStructureEnumType.CodeList, SdmxStructureEnumType.Dsd, 3, 0)]
        [TestCase(SdmxStructureEnumType.CodeList, SdmxStructureEnumType.ConceptScheme, 5, 0)]
        [TestCase(SdmxStructureEnumType.Dataflow, SdmxStructureEnumType.Dsd, 2, 1)]
        [TestCase(SdmxStructureEnumType.ContentConstraint, SdmxStructureEnumType.CodeList, 1, 12)]
        public void ShouldGetCorrectPath(SdmxStructureEnumType source, SdmxStructureEnumType target, int expectedParentAndSiblingPathCount, int expectedDescendantPathCount)
        {
            var fullPaths = SdmxMaintainableReferenceTree.GetSpecificPath(SdmxStructureType.GetFromEnum(source), SdmxStructureType.GetFromEnum(target));
            Assert.That(fullPaths.ParentsAndSiblings.Count, Is.EqualTo(expectedParentAndSiblingPathCount));
            Assert.That(fullPaths.Descendants.Count, Is.EqualTo(expectedDescendantPathCount));
        }
    }
}