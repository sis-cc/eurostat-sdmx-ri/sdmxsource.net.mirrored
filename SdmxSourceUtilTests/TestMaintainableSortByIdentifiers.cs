// -----------------------------------------------------------------------
// <copyright file="TestMaintainableSortByIdentifiers.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtilTests.
//     SdmxSourceUtilTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtilTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtilTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using NSubstitute;

namespace SdmxSourceUtilTests
{
    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Util.Sort;

    /// <summary>
    ///     Test unit class for <see cref="MaintainableSortByIdentifiers{T}" />
    /// </summary>
    [TestFixture]
    public class TestMaintainableSortByIdentifiers
    {
        #region Public Methods and Operators

        /// <summary>
        /// Test method for <see cref="MaintainableSortByIdentifiers{T}.Compare" />
        /// </summary>
        [Test]
        public void TestCompare()
        {
            var o = Substitute.For<ICodelistObject>();
            o.StructureType.Returns(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList));
            o.Id.Returns("Test");
            o.AgencyId.Returns("TestAgency");
            o.Version.Returns("1.0");

            var comp = new MaintainableSortByIdentifiers<ICodelistObject>();
            Assert.IsTrue(comp.Compare(o, o) == 0);

            var o2 = Substitute.For<ICodelistObject>();
            o2.StructureType.Returns(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList));
            o2.Id.Returns("Test");
            o2.AgencyId.Returns("TestAgency");
            o2.Version.Returns("1.0");

            // when they are different object but have the same agency, id and version it would return -1. TODO check with MT.
            Assert.IsTrue(comp.Compare(o, o2) == -1);

            var o3 = Substitute.For<ICodelistObject>();
            o3.StructureType.Returns(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList));
            o3.Id.Returns("Test4");
            o3.AgencyId.Returns("TestAgency");
            o3.Version.Returns("1.0");
            Assert.IsFalse(comp.Compare(o, o3) == 0);
            o3.Id.Returns("Test");
            o3.AgencyId.Returns("TestAgencyA");
            Assert.IsFalse(comp.Compare(o, o3) == 0);
            o3.AgencyId.Returns("TestAgency");
            o3.Version.Returns("2.0");
            Assert.IsFalse(comp.Compare(o, o3) == 0);
            Assert.IsFalse(comp.Compare(o, null) == 0);
            Assert.IsFalse(comp.Compare(null, o3) == 0);
            Assert.IsTrue(comp.Compare(null, null) == 0);
        }

        #endregion
    }
}