namespace SdmxSourceUtilTests
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Translator;

    /// <summary>
    /// Test unit for <see cref="LanguageTagComparer"/>.
    /// </summary>
    [TestFixture]
    public class TestLanguageTagComparer
    {

        /// <summary>
        /// Test <see cref="LanguageTagComparer"/> constructor.
        /// </summary>
        [Test]
        public void TestLanguageTagComparer4CompareLanguageAndRegionTag()
        {
            Assert.AreEqual(LanguageTagMatch.None, LanguageTagComparer.CompareLanguageAndRegionTag("en", "hu"));
            Assert.AreEqual(LanguageTagMatch.LanguageAndRegionMatch, LanguageTagComparer.CompareLanguageAndRegionTag("en", "en"));
            Assert.AreEqual(LanguageTagMatch.LanguageMatchOnly, LanguageTagComparer.CompareLanguageAndRegionTag("en", "en-GB"));
            Assert.AreEqual(LanguageTagMatch.LanguageAndRegionMatch, LanguageTagComparer.CompareLanguageAndRegionTag("en-gb", "en-GB"));
            Assert.AreEqual(LanguageTagMatch.LanguageMatchOnly, LanguageTagComparer.CompareLanguageAndRegionTag("en-US", "en-GB"));
        }
    }
}