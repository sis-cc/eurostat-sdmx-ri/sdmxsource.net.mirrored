namespace SdmxSourceUtilTests
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Translator;

    /// <summary>
    /// Test unit for <see cref="PreferedLanguageTranslator"/>.
    /// </summary>
    [TestFixture]
    public class TestPreferedLanguageTranslator
    {
        private readonly CultureInfo _itCultureInfo = CultureInfo.GetCultureInfo("it");
        private readonly CultureInfo _frCultureInfo = CultureInfo.GetCultureInfo("fr");
        private readonly CultureInfo _enCultureInfo = CultureInfo.GetCultureInfo("en");
        private const string ItValue = "itValue";
        private const string FrValue = "frValue";
        private const string EnValue = "enValue";

        /// <summary>
        /// Test <see cref="PreferedLanguageTranslator"/> constructor.
        /// </summary>
        [Test]
        public void TestPreferedLanguageTranslatorValidation()
        {
            Assert.DoesNotThrow(() => new PreferedLanguageTranslator(null, null, _enCultureInfo));
            Assert.Throws<ArgumentNullException>(() => new PreferedLanguageTranslator(null, null, null));
        }

        /// <summary>
        /// Test SelectedLanguage property.
        /// </summary>
        [Test]
        public void TestSelectedLanguage()
        {
            Assert.AreEqual(_enCultureInfo, new PreferedLanguageTranslator(null, null, _enCultureInfo).SelectedLanguage);
            Assert.AreEqual(_enCultureInfo, new PreferedLanguageTranslator(null, new List<CultureInfo>() {_enCultureInfo}, _enCultureInfo).SelectedLanguage);
            Assert.AreEqual(_enCultureInfo, new PreferedLanguageTranslator(null, new List<CultureInfo>() {_frCultureInfo, _enCultureInfo}, _enCultureInfo).SelectedLanguage);
            Assert.AreEqual(_itCultureInfo, new PreferedLanguageTranslator(null, new List<CultureInfo>() {_frCultureInfo, _enCultureInfo}, _itCultureInfo).SelectedLanguage);

            Assert.AreEqual(_enCultureInfo, new PreferedLanguageTranslator(new List<CultureInfo>() {_enCultureInfo}, null, _enCultureInfo).SelectedLanguage);
            Assert.AreEqual(_enCultureInfo, new PreferedLanguageTranslator(new List<CultureInfo>() {_frCultureInfo}, null, _enCultureInfo).SelectedLanguage);

            Assert.AreEqual(_enCultureInfo, new PreferedLanguageTranslator(
                new List<CultureInfo>() {_itCultureInfo},
                new List<CultureInfo>() {_frCultureInfo},
                _enCultureInfo).SelectedLanguage);

            Assert.AreEqual(_frCultureInfo, new PreferedLanguageTranslator(
                new List<CultureInfo>() {_itCultureInfo, _frCultureInfo},
                new List<CultureInfo>() {_frCultureInfo},
                _enCultureInfo).SelectedLanguage);

            Assert.AreEqual(_frCultureInfo, new PreferedLanguageTranslator(
                new List<CultureInfo>() {_itCultureInfo, _frCultureInfo, _enCultureInfo},
                new List<CultureInfo>() {_frCultureInfo},
                _enCultureInfo).SelectedLanguage);

            Assert.AreEqual(_itCultureInfo, new PreferedLanguageTranslator(
                new List<CultureInfo>() {_itCultureInfo, _frCultureInfo, _enCultureInfo},
                new List<CultureInfo>() {_frCultureInfo, _itCultureInfo},
                _enCultureInfo).SelectedLanguage);

            Assert.AreEqual(_frCultureInfo, new PreferedLanguageTranslator(
                new List<CultureInfo>() {_itCultureInfo, _frCultureInfo, _enCultureInfo},
                new List<CultureInfo>() {_enCultureInfo, _frCultureInfo},
                _enCultureInfo).SelectedLanguage);
        }

        /// <summary>
        /// Test GetTranslationFromCultureInfoDictionary method.
        /// </summary>
        [Test]
        public void TestGetTranslationFromCultureInfoDictionary()
        {
            Assert.AreEqual(string.Empty, new PreferedLanguageTranslator(null, null, _itCultureInfo)
                .GetTranslation(new Dictionary<CultureInfo, string>() {{_frCultureInfo, FrValue}}));

            Assert.AreEqual(ItValue, new PreferedLanguageTranslator(null, null, _itCultureInfo)
                .GetTranslation(new Dictionary<CultureInfo, string>() {{_itCultureInfo, ItValue}}));

            Assert.AreEqual(EnValue, new PreferedLanguageTranslator(null, null, _enCultureInfo)
                .GetTranslation(new Dictionary<CultureInfo, string>()
                {
                    {_frCultureInfo, FrValue},
                    {_enCultureInfo, EnValue}
                }));

            Assert.AreEqual(EnValue, new PreferedLanguageTranslator(null, null, _enCultureInfo)
                .GetTranslation(new Dictionary<CultureInfo, string>()
                {
                    {_frCultureInfo, FrValue},
                    {_enCultureInfo, EnValue},
                    {_itCultureInfo, ItValue}
                }));
        }

        /// <summary>
        /// Test GetTranslationFromTextTypeWrapperList method.
        /// </summary>
        [Test]
        public void TestGetTranslationFromTextTypeWrapperList()
        {
            Assert.AreEqual(string.Empty, new PreferedLanguageTranslator(null, null, _itCultureInfo)
                .GetTranslation(new List<ITextTypeWrapper> {new TextTypeWrapperImpl(_frCultureInfo.TwoLetterISOLanguageName, FrValue, null)}));

            Assert.AreEqual(ItValue, new PreferedLanguageTranslator(null, null, _itCultureInfo)
                .GetTranslation(new List<ITextTypeWrapper> {new TextTypeWrapperImpl(_itCultureInfo.TwoLetterISOLanguageName, ItValue, null)}));

            Assert.AreEqual(EnValue, new PreferedLanguageTranslator(null, null, _enCultureInfo)
                .GetTranslation(new List<ITextTypeWrapper>
                {
                    new TextTypeWrapperImpl(_frCultureInfo.TwoLetterISOLanguageName, FrValue, null),
                    new TextTypeWrapperImpl(_enCultureInfo.TwoLetterISOLanguageName, EnValue, null)
                }));

            Assert.AreEqual(EnValue, new PreferedLanguageTranslator(null, null, _enCultureInfo)
                .GetTranslation(new List<ITextTypeWrapper>
                {
                    new TextTypeWrapperImpl(_frCultureInfo.TwoLetterISOLanguageName, FrValue, null),
                    new TextTypeWrapperImpl(_enCultureInfo.TwoLetterISOLanguageName, EnValue, null),
                    new TextTypeWrapperImpl(_itCultureInfo.TwoLetterISOLanguageName, ItValue, null)
                }));
        }
    }
}