// -----------------------------------------------------------------------
// <copyright file="TestVersionableUtil.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtilTests.
//     SdmxSourceUtilTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtilTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtilTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxSourceUtilTests
{
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// Test unit for <see cref="VersionableUtil"/>
    /// </summary>
    [TestFixture]
    public class TestVersionableUtil
    {

        /// <summary>
        /// Test unit for <see cref="VersionableUtil.IncrementVersion"/> 
        /// </summary>
        /// <param name="version">
        /// The version.
        /// </param>
        /// <param name="majorInc">
        /// If major version should be incremented.
        /// </param>
        /// <param name="expectedResult">
        /// The expected Result.
        /// </param>
        [TestCase("1", true, "2.0")]
        [TestCase("1.0", true, "2.0")]
        [TestCase("1.4", true, "2.0")]
        [TestCase("1.4.3", true, "2.0")]
        [TestCase("2", true, "3.0")]
        [TestCase("2.0", true, "3.0")]
        [TestCase("2.1", true, "3.0")]
        [TestCase("2", false, "2.1")]
        [TestCase("2.0", false, "2.1")]
        [TestCase("2.0.0", false, "2.1")]
        [TestCase("2.0.1", false, "2.1")]
        // new semantic versions
        [TestCase("3.0.2-tag", true, "4.0.0-tag")]
        [TestCase("3.0.2-tag", false, "3.1.0-tag")]
        public void TestIncrementVersion(string version, bool majorInc, string expectedResult)
        {
            Assert.AreEqual(expectedResult, VersionableUtil.IncrementVersion(version, majorInc));
        }

        /// <summary>
        /// Test unit for <see cref="VersionableUtil.IsHigherVersion"/> 
        /// </summary>
        /// <param name="versionA">
        /// The version A.
        /// </param>
        /// <param name="versionB">
        /// The version B.
        /// </param>
        /// <param name="expectedResult">
        /// The expected Result.
        /// </param>
        [TestCase("1", "2.0", false)]
        [TestCase("1.1", "2.0", false)]
        [TestCase("1.6", "2.0", false)]
        [TestCase("1.3.3", "2.0", false)]
        [TestCase("1", "2", false)]
        [TestCase("1.1", "2", false)]
        [TestCase("1.6", "2", false)]
        [TestCase("1.3.3", "2", false)]
        [TestCase("1", "2.2.3", false)]
        [TestCase("1.1", "2.2.3", false)]
        [TestCase("1.6", "2.9.9", false)]
        [TestCase("1.3.3", "2.9.3", false)]
        [TestCase("3", "2.0", true)]
        [TestCase("3.1", "2.0", true)]
        [TestCase("3.6", "2.0", true)]
        [TestCase("3.3.3", "2.0", true)]
        [TestCase("3", "2", true)]
        [TestCase("3.1", "2", true)]
        [TestCase("3.6", "2", true)]
        [TestCase("3.3.3", "2", true)]
        [TestCase("3", "2.2.3", true)]
        [TestCase("3.1", "2.2.3", true)]
        [TestCase("3.6", "2.9.9", true)]
        [TestCase("3.3.3", "2.9.3", true)]
        // new semantic versions
        [TestCase("3.0.2-tag", "2.4.0-tag2", true)]
        [TestCase("3.0.2-tag", "3.0.3-tag2", false)]
        [TestCase("3.0.2-tag", "3.0.1", true)]
        [TestCase("3.0.2-tag", "3.0.2", false)]
        [TestCase("3.0.2-tag", "3.2.0", false)]
        [TestCase("3.6", "1.0.0-beta+exp.sha.5114f85", true)]
        [TestCase("3.6", "3.6.1", false)]
        public void TestIsHigherVersion(string versionA, string versionB, bool expectedResult)
        {
            Assert.IsTrue(expectedResult == VersionableUtil.IsHigherVersion(versionA, versionB));
        }

        /// <summary>
        /// Test unit for <see cref="VersionableUtil.ValidVersion"/> 
        /// </summary>
        /// <param name="version">
        /// The version.
        /// </param>
        /// <param name="expectedResult">
        /// The expected Result.
        /// </param>
        [TestCase("1", true)]
        [TestCase("1.1", true)]
        [TestCase("1.6", true)]
        [TestCase("1.3.3", true)]
        [TestCase("1", true)]
        [TestCase("1.1", true)]
        [TestCase("1.6", true)]
        [TestCase("1.3.3", true)]
        [TestCase("1", true)]
        [TestCase("1.1", true)]
        [TestCase("1.6", true)]
        [TestCase("1.3.3", true)]
        [TestCase("3", true)]
        [TestCase("3.1", true)]
        [TestCase("3.6", true)]
        [TestCase("3.3.3", true)]
        [TestCase("3", true)]
        [TestCase("3.1", true)]
        [TestCase("3.6", true)]
        [TestCase("3.3.3", true)]
        [TestCase("3", true)]
        [TestCase("3.1", true)]
        [TestCase("3.6", true)]
        [TestCase("3.3.3", true)]
        [TestCase("1.o.1", false)]
        [TestCase("", false)]
        [TestCase("1.", false)]
        [TestCase("1.A", false)]
        [TestCase("0x02", false)]
        [TestCase("02", true)]
        [TestCase(".1", false)]
        [TestCase(".1.", false)]
        [TestCase("VERSION", false)]
        public void TestValidVersion(string version, bool expectedResult)
        {
            Assert.IsTrue(expectedResult == VersionableUtil.ValidVersion(version));
            // "VerifyWildcardVersion" should continue validate the same way all previous cases
            Assert.IsTrue(expectedResult == VersionableUtil.VerifyWildcardVersion(version));
        }

        [TestCase("2+.4.0", true)]
        [TestCase("2.4+.0", true)]
        [TestCase("2.4.0+", true)]
        [TestCase("2.+4.0", false)]
        [TestCase("2.4+.0+", false)]
        [TestCase("2.+", false)]
        public void TestWildcardVersion(string version, bool expectedResult)
        {
            Assert.IsTrue(expectedResult == VersionableUtil.VerifyWildcardVersion(version));
        }
    }
}