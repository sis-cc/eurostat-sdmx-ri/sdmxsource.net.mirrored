// -----------------------------------------------------------------------
// <copyright file="TestReportingTimePeriod.cs" company="EUROSTAT">
//   Date Created : 2017-09-27
//   Copyright (c) 2012, 2018 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtilTests.
// 
//     SdmxSourceUtilTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtilTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtilTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxSourceUtilTests
{
    using System;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Date;

    public class TestReportingTimePeriod
    {
        private readonly ReportingTimePeriod _reportingTimePeriod = new ReportingTimePeriod();

        [Test]
        public void TestAnnualy()
        {
            var reportingPeriod = "2016-A1";
            var reportingYearStartDate = "--11-15";

            var startDate = new DateTime(2016, 11, 15);
            var reportingTimePeriod = this._reportingTimePeriod;
            var timePeriod = reportingTimePeriod.ToGregorianPeriod(reportingPeriod, reportingYearStartDate);
            Assert.That(timePeriod.PeriodStart.UtcDateTime, Is.EqualTo(startDate));
            Assert.That(timePeriod.Frequency, Is.EqualTo(TimeFormat.GetFromEnum(TimeFormatEnumType.Year)));

            var period = reportingTimePeriod.ToReportingPeriod(new SdmxDateCore(timePeriod.PeriodStart, TimeFormat.Year), reportingYearStartDate);
            Assert.That(period, Is.EqualTo(reportingPeriod));
        }

        [Test]
        public void TestDaily()
        {
            var reportingPeriod = "2016-D005";
            var reportingYearStartDate = "--02-29";

            var startDate = new DateTime(2016, 3, 4);
            var reportingTimePeriod = this._reportingTimePeriod;
            var timePeriod = reportingTimePeriod.ToGregorianPeriod(reportingPeriod, reportingYearStartDate);
            Assert.That(timePeriod.PeriodStart.UtcDateTime, Is.EqualTo(startDate));
            Assert.That(timePeriod.Frequency, Is.EqualTo(TimeFormat.GetFromEnum(TimeFormatEnumType.Date)));

            var period = reportingTimePeriod.ToReportingPeriod(new SdmxDateCore(timePeriod.PeriodStart, TimeFormat.Date), reportingYearStartDate);
            Assert.That(period, Is.EqualTo(reportingPeriod));
        }

        [TestCase("2005", "--01-01", "2005-A1")]
        [TestCase("2005", "--12-01", "2004-A1")]
        [TestCase("2005-03", "--01-01", "2005-M03")]
        [TestCase("2005-03", "--04-01", "2004-M12")]
        [TestCase("2005-01", "--04-01", "2004-M10")]
        [TestCase("2005-02", "--04-01", "2004-M11")]
        [TestCase("2010-02", "--09-01", "2009-M06")]
        [TestCase("2005-03", "--04-01", "2004-M12")]
        [TestCase("2016-Q1", "--01-01", "2016-Q1")]
        [TestCase("2016-Q1", "--03-01", "2015-Q4")]
        [TestCase("2016-Q1", "--09-01", "2015-Q2")]
        [TestCase("2016-Q2", "--09-01", "2015-Q3")]
//        [Ignore("Roundtrip not always possible")]
        public void TestGreogorianToReportingRoundTrip(string gregorian, string reportingStartYearDay, string expectedReportingPeriod)
        {
            var reportingTimePeriod = this._reportingTimePeriod;
            var sdmxDate = new SdmxDateCore(gregorian);
            var reportingPeriod = reportingTimePeriod.ToReportingPeriod(sdmxDate, reportingStartYearDay);
            Assert.That(reportingPeriod, Is.Not.Null);
            Assert.That(reportingPeriod, Is.EqualTo(expectedReportingPeriod));

            var gregorianRounndTrip = reportingTimePeriod.ToGregorianPeriod(expectedReportingPeriod, reportingStartYearDay);
            if (DateUtil.IsGregorianCompatible(gregorianRounndTrip.PeriodStart, gregorianRounndTrip.Frequency))
            {
                var sdmxDateRoundTrip = new SdmxDateCore(gregorianRounndTrip.PeriodStart, gregorianRounndTrip.Frequency);
                Assert.That(sdmxDateRoundTrip.DateInSdmxFormat, Is.EqualTo(gregorian));
            }
            else
            {
                var timeInterval = new DateTimeOffsetDuration(gregorianRounndTrip.PeriodStart, TimeDuration.Parse(gregorianRounndTrip.Frequency.IsoDuration));

                var sdmxDateRoundTrip = new SdmxDateCore(DateUtil.FormatDate(timeInterval, TimeFormat.TimeRange));
                Assert.That(sdmxDateRoundTrip.Duration.ToString(), Is.EqualTo(gregorianRounndTrip.Frequency.IsoDuration));
            }
        }

        [Test]
        public void TestMonthly()
        {
            var reportingPeriod = "2001-M01";
            var reportingYearStartDate = "--07-01";

            var startDate = new DateTime(2001, 7, 1);
            var reportingTimePeriod = this._reportingTimePeriod;
            var timePeriod = reportingTimePeriod.ToGregorianPeriod(reportingPeriod, reportingYearStartDate);
            Assert.That(timePeriod.PeriodStart.UtcDateTime, Is.EqualTo(startDate));
            Assert.That(timePeriod.Frequency, Is.EqualTo(TimeFormat.GetFromEnum(TimeFormatEnumType.Month)));

            var period = reportingTimePeriod.ToReportingPeriod(new SdmxDateCore(timePeriod.PeriodStart, TimeFormat.Month), reportingYearStartDate);
            Assert.That(period, Is.EqualTo(reportingPeriod));
        }

        [Test]
        public void TestQuarterly()
        {
            var reportingPeriod = "2010-Q2";
            var reportingYearStartDate = "--07-01";

            var startDate = new DateTime(2010, 10, 01);
            var reportingTimePeriod = this._reportingTimePeriod;
            var timePeriod = reportingTimePeriod.ToGregorianPeriod(reportingPeriod, reportingYearStartDate);
            Assert.That(timePeriod.PeriodStart.UtcDateTime, Is.EqualTo(startDate));
            Assert.That(timePeriod.Frequency, Is.EqualTo(TimeFormat.GetFromEnum(TimeFormatEnumType.QuarterOfYear)));

            var period = reportingTimePeriod.ToReportingPeriod(new SdmxDateCore(timePeriod.PeriodStart, TimeFormat.QuarterOfYear), reportingYearStartDate);
            Assert.That(period, Is.EqualTo(reportingPeriod));
        }

        [Test]
        public void TestReportingStartDate()
        {
            var reportingPeriod = "2016-S2";
            var reportingYearStartDate = "--11-01-04:00";

            var startDate = new DateTime(2017, 5, 1, 4, 0, 0);
            var reportingTimePeriod = this._reportingTimePeriod;
            var timePeriod = reportingTimePeriod.ToGregorianPeriod(reportingPeriod, reportingYearStartDate);
            Assert.That(timePeriod.PeriodStart.UtcDateTime, Is.EqualTo(startDate));
            Assert.That(timePeriod.Frequency, Is.EqualTo(TimeFormat.GetFromEnum(TimeFormatEnumType.HalfOfYear)));

            var period = reportingTimePeriod.ToReportingPeriod(new SdmxDateCore(timePeriod.PeriodStart, TimeFormat.HalfOfYear), reportingYearStartDate);
            Assert.That(period, Is.EqualTo(reportingPeriod));
        }

        [Test]
        public void TestReportingStartDateZ()
        {
            var reportingPeriod = "2016-T2";
            var reportingYearStartDate = "--11-01Z";

            var startDate = new DateTime(2017, 3, 1);
            var reportingTimePeriod = this._reportingTimePeriod;
            var timePeriod = reportingTimePeriod.ToGregorianPeriod(reportingPeriod, reportingYearStartDate);
            Assert.That(timePeriod.PeriodStart.UtcDateTime, Is.EqualTo(startDate));
            Assert.That(timePeriod.Frequency, Is.EqualTo(TimeFormat.GetFromEnum(TimeFormatEnumType.ThirdOfYear)));

            var period = reportingTimePeriod.ToReportingPeriod(new SdmxDateCore(timePeriod.PeriodStart, TimeFormat.ThirdOfYear), reportingYearStartDate);
            Assert.That(period, Is.EqualTo(reportingPeriod));
        }

        [Test]
        public void TestWeekly()
        {
            var reportingPeriod = "2011-W36";
            var reportingYearStartDate = "--07-01";

            var startDate = new DateTime(2012, 03, 05);
            var reportingTimePeriod = this._reportingTimePeriod;
            var timePeriod = reportingTimePeriod.ToGregorianPeriod(reportingPeriod, reportingYearStartDate);
            var sdmxDateCore = new SdmxDateCore(timePeriod.PeriodStart, TimeFormat.Week);
            Assert.That(timePeriod.PeriodStart.UtcDateTime, Is.EqualTo(startDate));
            Assert.That(timePeriod.Frequency, Is.EqualTo(TimeFormat.GetFromEnum(TimeFormatEnumType.Week)));

            var period = reportingTimePeriod.ToReportingPeriod(new SdmxDateCore(timePeriod.PeriodStart, TimeFormat.Week), reportingYearStartDate);
            Assert.That(period, Is.EqualTo(reportingPeriod));
        }

        [TestCase("2001-Q1", null, false)]
        [TestCase("2001-Q1", "--07-11", true)]
        [TestCase("2001-M01", "--07-11", true)]
        [TestCase("2001-A1", null, false)]
        [TestCase("2001-T1", null, false)]
        [TestCase("2001-T1", "--07-11", true)]
        [TestCase("2001-W10", "--07-11", true)]
        [TestCase("2001-W1", null, false)]
        [TestCase("2001-01", "--07-11", false)]
        [TestCase("2001-S1", "--07-11", true)]
        [TestCase("2001-S1", null, false)]
        public void ValidateReportingPeriod(string reportingPeriod, string reportingYearStartDate, bool expectedValidity)
        {
            var isValid = this._reportingTimePeriod.CheckReportingPeriod(reportingPeriod, reportingYearStartDate);
            Assert.That(isValid, Is.EqualTo(expectedValidity));
        }

        [TestCase("2001-M1", "--07-11", false, typeof(SdmxSemmanticException))]
        public void ValidateReportingPeriodException(string reportingPeriod, string reportingYearStartDate, bool expectedValidity,Type type)
        {
            Assert.Throws(type, () => { this._reportingTimePeriod.CheckReportingPeriod(reportingPeriod, reportingYearStartDate); });
        }
        [TestCase("2010-Q1", "--04-01", "2010-04-01/P3M")]
        [TestCase("2009-Q3", "--07-01", "2010-01-01/P3M")]
        [TestCase("2010-S1", "--04-01", "2010-04-01/P6M")]
        [TestCase("2010-T1", "--04-01", "2010-04-01/P4M")]
        [TestCase("2011-W36", "--07-01", "2012-03-05/P7D")]
        [TestCase("2010-A1", "--09-01", "2010-09-01/P1Y")]
        [TestCase("2010-M02", "--09-01", "2010-10-01/P1M")]
        [TestCase("2010-M12", "--09-01", "2011-08-01/P1M")]
        [TestCase("2010-D010", "--09-01", "2010-09-10/P1D")]
        public void ShouldConvertToTimeRange(string reportingPeriond, string reportingYearStartDate, string expectedTimeRange)
        {
            var timeRange = this._reportingTimePeriod.ToTimeRange(reportingPeriond, reportingYearStartDate);
            Assert.That(timeRange, Is.EqualTo(expectedTimeRange));
        }
    }
}