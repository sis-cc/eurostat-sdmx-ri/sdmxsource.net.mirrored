
using System.Net;
using System.Net.Mime;
using Microsoft.AspNetCore.Mvc;

namespace GuideTests.SdmxWebService.Controllers;

public class ContentCallbackResult : ContentResult 
{
    private readonly Action<Stream> _action;
    private readonly string _contentType;
    private readonly string _filename;

    public ContentCallbackResult(Action<Stream> action, string contentType, string filename)
    {
        _action = action;
        _contentType = contentType;
        _filename = filename;
    }

    public override async Task ExecuteResultAsync(ActionContext context)
    {
        HttpResponse httpResponse = context.HttpContext.Response;
        httpResponse.ContentType = _contentType;
        httpResponse.StatusCode = (int)HttpStatusCode.OK;
        httpResponse.Headers.ContentDisposition = new ContentDisposition { FileName = _filename, Inline = true }.ToString();
        _action(httpResponse.Body);
        await httpResponse.Body.FlushAsync();
    }
}