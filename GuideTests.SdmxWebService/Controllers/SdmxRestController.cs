using Microsoft.AspNetCore.Mvc;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Factory;
using Org.Sdmxsource.Sdmx.Api.Manager.Output;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Rest;
using Org.Sdmxsource.Sdmx.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.Api.Model.Format;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model.Query;
using Org.Sdmxsource.Sdmx.Api.Util;
using Org.Sdmxsource.Sdmx.DataParser.Factory;
using Org.Sdmxsource.Sdmx.DataParser.Manager;
using Org.Sdmxsource.Sdmx.DataParser.Rest;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.Structureparser.Factory;
using Org.Sdmxsource.Sdmx.Structureparser.Manager;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Rest;
using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
using Org.Sdmxsource.Util.Io;

namespace GuideTests.SdmxWebService.Controllers;

[ApiController]
[Route("sdmx/rest")]
public class SdmxRestController : ControllerBase
{
    /// <summary>
        /// The map that contains the known HTTP Accept header values and the corresponding <see cref="IDataFormat"/>
        /// </summary>
        private readonly IDictionary<string, IDataFormat> _acceptToDataFormats;

        /// <summary>
        /// The map that contains the known HTTP Accept header values and the corresponding <see cref="IStructureFormat"/>
        /// </summary>
        private readonly IDictionary<string, IStructureFormat> _acceptToStructureFormats;
            
        // managers
        private readonly IRestDataQueryManager _dataQueryManager;
        private readonly IRestStructureQueryManager _restStructureQueryManager;

        // data formats
        private readonly SdmxDataFormatCore _genericV21Format = new SdmxDataFormatCore(DataType.GetFromEnum(DataEnumType.Generic21));
        private readonly SdmxDataFormatCore _structureSpecificFormat =  new SdmxDataFormatCore(DataType.GetFromEnum(DataEnumType.Compact21));

        /// <summary>
        /// Structure format for SDMX v2.1
        /// </summary>
        private readonly IStructureFormat _structureFormatv21 = new SdmxStructureFormat(StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument));
        
  
    private readonly Microsoft.Extensions.Hosting.IHostEnvironment _env;
    private readonly ILogger<SdmxRestController> _logger;

    public SdmxRestController(ILogger<SdmxRestController> logger, Microsoft.Extensions.Hosting.IHostEnvironment env)
    {
        _logger = logger;
        _env = env;
        // 1.a To support additional data formats pass additional IDataWriterFactory implementations to the DataWriterManager constructor  
        IDataWriterFactory dataWriterFactory = new DataWriterFactory();
        IDataWriterManager dataWriterManager = new DataWriterManager(dataWriterFactory/*, add your factories here) */);

        // 1.b To support additional data formats pass additional IStructureWriterFactory implementations to the DataWriterManager constructor  
        IStructureWriterManager structureWriterManager = new StructureWriterManager(new SdmxStructureWriterFactory() /*, add your factories here */);

        // a map between the HTTP Accept header value and the IDataFormat
        this._acceptToDataFormats = new Dictionary<string, IDataFormat>(StringComparer.Ordinal)
                                            {
                                                { "application/vnd.sdmx.genericdata+xml", this._genericV21Format },
                                                { "application/xml", this._genericV21Format },
                                                { "application/vnd.sdmx.structurespecificdata+xml", this._structureSpecificFormat }, 
                                                /* 2. add new data formats here */
                                            };

        // a map between the HTTP Accept header value and the IStructureFormat
        this._acceptToStructureFormats = new Dictionary<string, IStructureFormat>(StringComparer.Ordinal)
                                                 {
                                                     { "application/vnd.sdmx.structure+xml", this._structureFormatv21 },
                                                     { "application/xml", this._structureFormatv21 }
                                                    
                                                     /* 2. add new structure formats here */
                                                 };

        // load the structures
        ISdmxObjects objects;
        IReadableDataLocationFactory dataLocationFactory = new ReadableDataLocationFactory();
        IStructureParsingManager structureParsingManager = new StructureParsingManager();

        using (IReadableDataLocation structureLocation = dataLocationFactory.GetReadableDataLocation(Path.Combine(_env.ContentRootPath, "App_Data", "structures.xml")))
        {
            IStructureWorkspace structureWorkspace = structureParsingManager.ParseStructures(structureLocation);
            objects = structureWorkspace.GetStructureObjects(false);
        }

        ISdmxObjectRetrievalManager retrievalManager = new InMemoryRetrievalManager(objects);

        _dataQueryManager = new RestDataQueryManager(new SampleDataRetrieval(), dataWriterManager, retrievalManager);
        _restStructureQueryManager = new RestStructureQueryManager(structureWriterManager, retrievalManager);
    }

    [HttpGet("data/{flowRef}/{key=ALL}/{providerRef=ALL}/")]
    public IActionResult GetData(string flowRef, string key, string providerRef)
    {
         IRestDataQuery query = new RESTDataQueryCore(string.Format("/data/%s/%s/%s", flowRef, key, providerRef));

        // get the first known Accept Header value and IDataFormat from the request
        KeyValuePair<string, IDataFormat> dataFormat = GetDataFormat(Request);

        // for simplicity use the first known accept header
        return new ContentCallbackResult((s) => this._dataQueryManager.ExecuteQuery(query, dataFormat.Value, s), dataFormat.Key, "data.xml");
    }
    
    [HttpGet("{structure}/{agencyId=ALL}/{resourceId=ALL}/{version=LATEST}/")]
    public IActionResult GetStructure(string structure, string agencyId, string resourceId, string version)
    {
        IRestStructureQuery query = new RESTStructureQueryCore(string.Format("/%s/%s/%s/%s", structure, agencyId, resourceId, version));
        KeyValuePair<string, IStructureFormat> format = GetStructureFormat(Request);

        // for simplicity use the first known accept header
        return new ContentCallbackResult((s) => this._restStructureQueryManager.GetStructures(query, s, format.Value), format.Key, "structure.xml");
    }

    private KeyValuePair<string, IStructureFormat> GetStructureFormat(HttpRequest request)
    {
        string[] accept = request.GetTypedHeaders().Accept.Select(a => a.ToString()).ToArray();
        if (accept != null)
        {
            for (int i = 0; i < accept.Length; i++)
            {
                var acceptHeader = accept[i];
                IStructureFormat? format;
                if (this._acceptToStructureFormats.TryGetValue(acceptHeader, out format))
                {
                    return new KeyValuePair<string, IStructureFormat>(acceptHeader, format);
                }
            }
        }

        return new KeyValuePair<string, IStructureFormat>("application/vnd.sdmx.structure+xml", this._structureFormatv21);
    }

    private KeyValuePair<string, IDataFormat> GetDataFormat(HttpRequest request)
    {
        string[] accept = request.GetTypedHeaders().Accept.Select(a => a.ToString()).ToArray();
        if (accept != null)
        {
            for (int i = 0; i < accept.Length; i++)
            {
                var acceptHeader = accept[i];
                IDataFormat? format;
                if (this._acceptToDataFormats.TryGetValue(acceptHeader, out format))
                {
                    return new KeyValuePair<string, IDataFormat>(acceptHeader, format);
                }
            }
        }

        return new KeyValuePair<string, IDataFormat>("application/vnd.sdmx.genericdata+xml", this._genericV21Format);
    }
}
