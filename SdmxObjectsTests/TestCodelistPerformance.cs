﻿// -----------------------------------------------------------------------
// <copyright file="TestCodelistPerformance.cs" company="EUROSTAT">
//   Date Created : 2014-06-13
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjectsTests.
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxObjectsTests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Codelist;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    /// The test codelist performance.
    /// </summary>
    [TestFixture]
    public class TestCodelistPerformance
    {
        /// <summary>
        /// Tests the mutable to immutable.
        /// </summary>
        /// <param name="size">The size.</param>
        [TestCase(10000)]
        [TestCase(1000000, IgnoreReason = "Performance test")]
        public void TestMutableToImmutable(int size)
        {
            ICodelistMutableObject mutable = BuildMutable(size);
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            ICodelistObject immutableInstance = mutable.ImmutableInstance;
            stopwatch.Stop();
            Trace.WriteLine(stopwatch.Elapsed);
            Assert.NotNull(immutableInstance);
            Assert.LessOrEqual(stopwatch.ElapsedMilliseconds, size >> 5);
        }

        /// <summary>
        /// Tests the duplicate code.
        /// </summary>
        [Test]
        public void TestDuplicateCode()
        {
            ICodelistMutableObject mutable = BuildMutable(10);
            var codeMutableObject = mutable.Items[0];
            mutable.AddItem(codeMutableObject);
            TestDelegate testDelegate = () => { var codelist = new CodelistObjectCore(mutable); };
            Assert.Throws<SdmxSemmanticException>(testDelegate, "Error");
        }

        /// <summary>
        /// Tests the invalid parenting first.
        /// </summary>
        [Test]
        public void TestInvalidParent()
        {
            ICodelistMutableObject codelistMutableObject = BuildMutable(10);

            var dictionary = BuildDictionary(codelistMutableObject);

            // create a cycle at the first item with parent
            var firstCodeWithParent = codelistMutableObject.Items.First(o => o.ParentCode != null);
            firstCodeWithParent.ParentCode = "DOES_NOT_EXIST";

            TestDelegate testDelegate = () => { var codelist = new CodelistObjectCore(codelistMutableObject); };
            Assert.Throws<SdmxSemmanticException>(testDelegate, "Error");
        }

        /// <summary>
        /// Tests the invalid parenting first.
        /// </summary>
        [Test]
        public void TestInvalidParentingFirst()
        {
            ICodelistMutableObject codelistMutableObject = BuildMutable(10);

            var dictionary = BuildDictionary(codelistMutableObject);

            // create a cycle at the first item with parent
            var firstCodeWithParent = codelistMutableObject.Items.First(o => o.ParentCode != null);
            var parent = codelistMutableObject.Items[dictionary.GetOrDefault(firstCodeWithParent.ParentCode)];
            parent.ParentCode = firstCodeWithParent.Id;

            TestDelegate testDelegate = () => { var codelist = new CodelistObjectCore(codelistMutableObject); };
            Assert.Throws<SdmxSemmanticException>(testDelegate, "Error");
        }

        /// <summary>
        /// Tests the invalid parenting last.
        /// </summary>
        [Test]
        public void TestInvalidParentingLast()
        {
            ICodelistMutableObject codelistMutableObject = BuildMutable(10);

            var dictionary = BuildDictionary(codelistMutableObject);

            // create a cycle at the first item with parent
            var lastCodeWithParent = codelistMutableObject.Items.Last(o => o.ParentCode != null);
            var parent = codelistMutableObject.Items[dictionary.GetOrDefault(lastCodeWithParent.ParentCode)];
            parent.ParentCode = lastCodeWithParent.Id;

            TestDelegate testDelegate = () => { var codelist = new CodelistObjectCore(codelistMutableObject); };
            Assert.Throws<SdmxSemmanticException>(testDelegate, "Error");
        }

        /// <summary>
        /// Tests the invalid parenting first last.
        /// </summary>
        [Test]
        public void TestInvalidParentingFirstLast()
        {
            ICodelistMutableObject codelistMutableObject = BuildMutable(10);

            // create a cycle at the first item with parent
            var firstCodeWithParent = codelistMutableObject.Items.First(o => o.ParentCode != null);
            var lastCodeWithParent = codelistMutableObject.Items.Last(o => o.ParentCode != null);
            var middleCode = codelistMutableObject.Items[5];
            firstCodeWithParent.ParentCode = middleCode.Id;
            middleCode.ParentCode = lastCodeWithParent.Id;
            lastCodeWithParent.ParentCode = firstCodeWithParent.Id;

            TestDelegate testDelegate = () => { var codelist = new CodelistObjectCore(codelistMutableObject); };
            Assert.Throws<SdmxSemmanticException>(testDelegate, "Error");
        }

        /// <summary>
        /// Tests the invalid parenting first last.
        /// </summary>
        [Test]
        public void TestInvalidParentingLastFirst()
        {
            ICodelistMutableObject codelistMutableObject = BuildMutable(10);

            // create a cycle at the first item with parent
            var firstCodeWithParent = codelistMutableObject.Items.First(o => o.ParentCode != null);
            var lastCodeWithParent = codelistMutableObject.Items.Last(o => o.ParentCode != null);
            var middleCode = codelistMutableObject.Items[5];
            firstCodeWithParent.ParentCode = lastCodeWithParent.Id;
            middleCode.ParentCode = firstCodeWithParent.Id;
            lastCodeWithParent.ParentCode = middleCode.Id;

            TestDelegate testDelegate = () => { var codelist = new CodelistObjectCore(codelistMutableObject); };
            Assert.Throws<SdmxSemmanticException>(testDelegate, "Error");
        }

        /// <summary>
        /// Builds the dictionary.
        /// </summary>
        /// <param name="codelistMutableObject">The codelist mutable object.</param>
        /// <returns>The dictionary for code id vs position</returns>
        private static Dictionary<string, int> BuildDictionary(ICodelistMutableObject codelistMutableObject)
        {
            var dictionary = new Dictionary<string, int>(StringComparer.Ordinal);
            for (int index = 0; index < codelistMutableObject.Items.Count; index++)
            {
                var codeMutableObject = codelistMutableObject.Items[index];
                dictionary.Add(codeMutableObject.Id, index);
            }

            return dictionary;
        }

        /// <summary>
        /// Builds the mutable.
        /// </summary>
        /// <param name="size">The size.</param>
        /// <returns>The mutable codelist</returns>
        private static ICodelistMutableObject BuildMutable(int size)
        {
            var codelist = new CodelistMutableCore() { Id = "TEST", AgencyId = "TEST_AGENCY", Version = "1.0" };
            codelist.AddName("en", "Test name");
            string lastCode = null;
            for (int i = 0; i < size; i++)
            {
                string codeId = string.Format(CultureInfo.InvariantCulture, "ID{0}", i);
                var code = codelist.CreateItem(codeId, codeId);
                if (lastCode != null && (i % 2) == 0)
                {
                    code.ParentCode = lastCode;
                }

                if ((i % 6) == 0)
                {
                    lastCode = codeId;
                }
            }

            return codelist;
        }
    }
}