// -----------------------------------------------------------------------
// <copyright file="TestGetStub.cs" company="EUROSTAT">
//   Date Created : 2023-5-3
//   Copyright (c) 2023 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjectsTests.
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

namespace SdmxObjectsTests
{
    internal class TestGetStub
    {
        [Test]
        public void GetStubProvisionAgreement()
        {
            ProvisionAgreementMutableCore mutable = new ProvisionAgreementMutableCore();
            SetMaintainableStubProperties(mutable);
            mutable.DataproviderRef = new StructureReferenceImpl("TEST_AG", DataProviderScheme.FixedId, DataProviderScheme.FixedVersion, SdmxStructureEnumType.DataProviderScheme, "DP_TEST");
            mutable.StructureUsage = new StructureReferenceImpl("TEST_AG", "TEST_DF", "2.3.5", SdmxStructureEnumType.Dataflow);
            IProvisionAgreementObject immutable = mutable.ImmutableInstance;
            IProvisionAgreementObject stub = (IProvisionAgreementObject)immutable.GetStub(new Uri("http://www.example.com/path"), false);
            AssertCommonStub(mutable, stub);

            Assert.IsNull(stub.DataproviderRef);
            Assert.IsNull(stub.StructureUseage);
        }

        [Test]
        public void GetStubContentConstraint()
        {
            IContentConstraintMutableObject mutable = new ContentConstraintMutableCore();
            SetMaintainableStubProperties(mutable);
            mutable.IncludedCubeRegion = new CubeRegionMutableCore();
            var kv = new KeyValuesMutableImpl();
            kv.Id = "TEST_COMP";
            kv.AddValue("TEST_VAL");
            mutable.IncludedCubeRegion.AddKeyValue(kv);
            mutable.ConstraintAttachment = new ContentConstraintAttachmentMutableCore();
            mutable.ConstraintAttachment.AddStructureReference(new StructureReferenceImpl("TEST_AG", "TEST_DF", "2.3.5", SdmxStructureEnumType.Dataflow));

            IContentConstraintObject immutable = mutable.ImmutableInstance;
            IContentConstraintObject stub = (IContentConstraintObject)immutable.GetStub(new Uri("http://www.example.com/path"), false);
            AssertCommonStub(mutable, stub);

            Assert.IsNull(stub.IncludedCubeRegion);
            Assert.IsNull(stub.ExcludedCubeRegion);

            Assert.IsNull(stub.IncludedSeriesKeys);
            Assert.IsNull(stub.ExcludedSeriesKeys);

            Assert.IsNull(stub.IncludedMetadataKeys);
            Assert.IsNull(stub.ExcludedMetadataKeys);

            Assert.IsNull(stub.ReferencePeriod);
            Assert.IsNull(stub.ConstraintAttachment);

            Assert.AreEqual(mutable.IsDefiningActualDataPresent, stub.IsDefiningActualDataPresent);

        }


        private static void AssertCommonStub(IMaintainableMutableObject expected, IMaintainableObject stub)
        {
            Assert.NotNull(stub);
            Assert.IsTrue(stub.IsExternalReference.IsTrue);
            Assert.AreEqual(stub.Id, expected.Id);
            Assert.AreEqual(stub.AgencyId, expected.AgencyId);
            Assert.AreEqual(stub.Version, expected.Version);
        }

        private void SetMaintainableStubProperties(IMaintainableMutableObject mutableObject)
        {
            mutableObject.Id = "TEST";
            mutableObject.AgencyId = "TEST_AGENCY_ID";
            mutableObject.Version = "1.0.0";
            mutableObject.AddName("en", "Test name");
        }
    }
}
