using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.SdmxObjects.TestUtils;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Sdmx.Util.Extension;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using Org.Sdmxsource.Util;

namespace SdmxObjectsTests
{
    [TestFixture]
    public class VersionTests
    {
        [TestCase("1.0.0-beta+exp.sha.5114f85")]
        public void CheckSettingSemanticVersioning(string version)
        {
            IMaintainableObject[] maintainables = GetSdmxObjects().ToArray();

            foreach(var maintainable in maintainables)
            {
                if (maintainable.StructureType != SdmxStructureType.GetFromEnum(SdmxStructureEnumType.AgencyScheme)
                 && maintainable.StructureType != SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataConsumerScheme)
                 && maintainable.StructureType != SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataProviderScheme)
                 && maintainable.StructureType != SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd))
                {
                    
                        var mutable = maintainable.MutableInstance;
                        mutable.Version = version;
                        if (!maintainable.IsFinal.IsTrue)
                        { 
                            Assert.That(mutable.ImmutableInstance.Version, Is.EqualTo(version)); 
                        }
                    
                }
            }
        }

        [Test]
        [TestCaseSource(nameof(VersionAndFinalFlagArguments))]
        public void CreateObjectWithFinalFlag(
            string version, TertiaryBoolEnumType? configuredFinalFlag, TertiaryBoolEnumType expectedFinalFlag)
        {
            IDataStructureMutableObject dsd = SampleObjectsBuilder.BuildDataStructureMutableObject();
            dsd.Version = version;
            dsd.FinalStructure = configuredFinalFlag == null ? null : TertiaryBool.GetFromEnum(configuredFinalFlag.Value);

            IDataStructureObject immutable = dsd.ImmutableInstance;

            Assert.AreEqual(version, immutable.Version);
            Assert.AreEqual(expectedFinalFlag, immutable.IsFinal.EnumType);
        }

        [Test]
        [TestCase("2.4.0-draft", TertiaryBoolEnumType.True)]
        [TestCase("1.0.0-beta+exp.sha.5114f85", TertiaryBoolEnumType.True)]
        public void CreateObjectWithInvalidSetFinalFlag(string version, TertiaryBoolEnumType configuredFinalFlag)
        {
            IDataStructureMutableObject dsd = SampleObjectsBuilder.BuildDataStructureMutableObject();
            dsd.Version = version;
            dsd.FinalStructure = TertiaryBool.GetFromEnum(configuredFinalFlag);

            Assert.Throws<SdmxSemmanticException>(() => { var immutable = dsd.ImmutableInstance; });
        }

        [TestCase("1.0.0-beta+exp.sha.5114f85")]
        [TestCase("2.4.0")]
        public void CreateObjectWithSemanticVersioning(string version)
        {
            var mutableDsd = Utils.BuildTestDataStructureMutableBean(version,version.Contains("-"));
            var dsd = new DataStructureObjectCore(mutableDsd);

            Assert.That(dsd.Version, Is.EqualTo(version));
        }

        [TestCase("1.0.0-beta+exp.sha.5114f85")]
        [TestCase("2+.4.0")]
        public void CreateStructureRefereceWithSemanticVersion(string version)
        {
            var structureReference = new StructureReferenceImpl("test", "id", version, SdmxStructureEnumType.ActualConstraint);
            Assert.That(structureReference.Version, Is.EqualTo(version));
        }

        [TestCase("2+.4.0")]
        [TestCase("2.4+.0")]
        [TestCase("2.4.0+")]
        public void TestWildcardVersions(string version)
        {
            var structureReference = new StructureReferenceImpl("test", "id", version, SdmxStructureEnumType.ActualConstraint);
            IDataStructureMutableObject dsd = SampleObjectsBuilder.BuildDataStructureMutableObject();

            Assert.That(structureReference.Version, Is.EqualTo(version)); // is valid for structure references
            Assert.Throws<ArgumentException>(() => { dsd.Version = version; }); // is invalid for actual structures
        }

        //old implementation
        [TestCase("1.0.0","1.0.0",true)]
        [TestCase("1.0.0", "1.1.0", false)]
        //>=2.4.0 and <3.0.0 or >=2.4.0-draft and <3.0.0-draft
        [TestCase("2.4+.0", "2.6.0-draft",true)]
        [TestCase("2.4+.0", "3.1.0-draft", false)]
        //>=2.4.0 and <3.0.0
        [TestCase("2.4+.0","2.7.3",true)]
        [TestCase("2.4+.0", "3.7.3", false)]
        //>=2.4.0 and <2.5.0
        [TestCase("2.4.0+","2.4.1",true)]
        [TestCase("2.4.0+", "2.5.1", false)]
        //>=2.4.0
        [TestCase("2+.4.0","3.1.1",true)]
        [TestCase("2+.4.0", "1.1.1", false)]
        
        //test more complex semantic version
        [TestCase("1.0.0+","1.0.0-beta+exp.sha.5114f85",true)]
        public void TestMatchVersions(string wildcardVersion,string matchingVersion,bool isMatch)
        {
            Assert.AreEqual(VersionableUtil.IsMatch(wildcardVersion, matchingVersion), isMatch);
        }

        //old implementation
        [TestCase("1.0.0", "1.0.0", true)]
        [TestCase("1.0.0", "1.1.0", false)]
        //>=2.4.0 and <3.0.0 or >=2.4.0-draft and <3.0.0-draft
        [TestCase("2.4+.0", "2.6.0-draft", true)]
        [TestCase("2.4+.0", "3.1.0-draft", false)]
        //>=2.4.0 and <3.0.0
        [TestCase("2.4+.0", "2.7.3", true)]
        [TestCase("2.4+.0", "3.7.3", false)]
        //>=2.4.0 and <2.5.0
        [TestCase("2.4.0+", "2.4.1", true)]
        [TestCase("2.4.0+", "2.5.1", false)]
        //>=2.4.0
        [TestCase("2+.4.0", "3.1.1", true)]
        [TestCase("2+.4.0", "1.1.1", false)]

        //test more complex semantic version
        [TestCase("1.0.0+", "1.0.0-beta+exp.sha.5114f85", true)]
        public void TestStructureReferenceUsesMatchVersions(string wildcardVersion, string matchingVersion, bool isMatch)
        {
            IDataStructureMutableObject dsd = SampleObjectsBuilder.BuildDataStructureMutableObject();
            dsd.Version = matchingVersion;
            var dataflowReference = new StructureReferenceImpl("EXAMPLE", "TEST_ID", wildcardVersion, SdmxStructureEnumType.Dsd);

            Assert.AreEqual(dataflowReference.GetMatch(dsd.ImmutableInstance)!=null,isMatch);
        }

        [TestCaseSource(nameof(VerionsRequestTestSource))]
        public void TestVersionRequest(string version, DummyVersionRequest expectedRequest)
        {
            var testRequest = new VersionRequestCore(version);
            
            Assert.AreEqual(expectedRequest.SpecifiesVersion, testRequest.SpecifiesVersion);
            Assert.AreEqual(expectedRequest.VersionQueryType, testRequest.VersionQueryType);
            Assert.AreEqual(expectedRequest.Extension, testRequest.Extension);
            Assert.AreEqual(expectedRequest.Major, testRequest.Major);
            Assert.AreEqual(expectedRequest.Minor, testRequest.Minor);
            Assert.AreEqual(expectedRequest.Patch, testRequest.Patch);
            Assert.AreEqual(expectedRequest.WildCard?.WildcardType, testRequest.WildCard?.WildcardType);
            Assert.AreEqual(expectedRequest.WildCard?.WildcardPosition, testRequest.WildCard?.WildcardPosition);
        }

        /// <summary>
        /// A dummy class to hold control data for tests.
        /// </summary>
        public class DummyVersionRequest : IVersionRequest
        {
            public bool SpecifiesVersion { get; set; }

            public VersionQueryTypeEnum VersionQueryType { get; set; }

            public string Extension { get; set; }

            public int? Major { get; set; }

            public int? Minor { get; set; }

            public int? Patch { get; set; }

            public IVersionWildcard WildCard { get; set; }

            public int? VersionPart(VersionPosition position)
            {
                throw new NotImplementedException();
            }
        }

        // These are test cases that test the VersionRequestCore.
        // In the documentation it is written:
        // 1. MAJOR, MINOR or PATCH version parts in SDMX 3.0 artefact references CAN be wildcarded using "+" as extension.
        // 2. For references to non-dependent artefacts, MAJOR, MINOR or PATCH version parts in SDMX 3.0 artefact references
        // CAN alternatively be wildcarded using "*" as replacement: * means all available versions.
        // So we have different rules according to whether the the version request targets dependent or non-dependent artefacts.
        // Maybe we should include that distinction in the VersionRequestCore.
        private static object[] VerionsRequestTestSource()
        {
            return new object[]
            {
                new object[]{"2.4.0",
                    new DummyVersionRequest()
                    {
                        Major = 2, Minor = 4, Patch = 0,
                        SpecifiesVersion = true,
                        VersionQueryType = VersionQueryTypeEnum.All,
                        Extension = null,
                        WildCard = null
                    }
                },
                new object[]{"*", // this fails
                    new DummyVersionRequest()
                    {
                        Major = null, Minor = null, Patch = null,
                        SpecifiesVersion = false,
                        VersionQueryType = VersionQueryTypeEnum.All,
                        Extension = null,
                        WildCard = null
                    }
                },
                new object[]{"2.4.*-draft-ext",
                    new DummyVersionRequest()
                    {
                        Major = 2, Minor = 4, Patch = null,
                        SpecifiesVersion = true,
                        VersionQueryType = VersionQueryTypeEnum.All,
                        Extension = "draft-ext",
                        WildCard = new VersionWildcardCore(VersionQueryTypeEnum.All, VersionPosition.Patch)
                    }
                },
                new object[]{"+~", // this should not be accepted
                    new DummyVersionRequest()
                    {
                        Major = null, Minor = null, Patch = null,
                        SpecifiesVersion = false,
                        VersionQueryType = VersionQueryTypeEnum.Latest,
                        Extension = null,
                        WildCard = null
                    }
                },
                new object[]{"+.4.1-ext", // is that valid?
                    new DummyVersionRequest()
                    {
                        Major = null, Minor = 4, Patch = 1,
                        SpecifiesVersion = false,
                        VersionQueryType = VersionQueryTypeEnum.LatestStable,
                        Extension = "ext",
                        WildCard = new VersionWildcardCore(VersionQueryTypeEnum.LatestStable, VersionPosition.Major)
                    }
                },
                new object[]{"2.4+.1-ext",
                    new DummyVersionRequest()
                    {
                        Major = 2, Minor = 4, Patch = 1,
                        SpecifiesVersion = true,
                        VersionQueryType = VersionQueryTypeEnum.LatestStable,
                        Extension = "ext",
                        WildCard = new VersionWildcardCore(VersionQueryTypeEnum.LatestStable, VersionPosition.Minor)
                    }
                }
            };
        }

        private static object[] VersionAndFinalFlagArguments()
        {
            // 1st argument: test version
            // 2nd argument: set mutable IsFinal value
            // 3rd argument: expected immutable IsFinal value
            return new object[] 
            {
                // these examples fall under 3.0 schema 
                new object[] { "2.4.0"      , TertiaryBoolEnumType.True , TertiaryBoolEnumType.True },
                new object[] { "2.4.0"      , TertiaryBoolEnumType.Unset, TertiaryBoolEnumType.True },
                new object[] { "2.4.0"      , null                      , TertiaryBoolEnumType.True },
                new object[] { "2.4.0-draft", TertiaryBoolEnumType.False, TertiaryBoolEnumType.False },
                new object[] { "2.4.0-draft", TertiaryBoolEnumType.Unset, TertiaryBoolEnumType.False },
                new object[] { "2.4.0-draft", null                      , TertiaryBoolEnumType.False },
                // these examples fall under 2.x schema
                new object[] { "2.4.0"      , TertiaryBoolEnumType.False, TertiaryBoolEnumType.False },
                new object[] { "2.4"        , TertiaryBoolEnumType.Unset, TertiaryBoolEnumType.Unset },
                new object[] { "2.4"        , null                      , TertiaryBoolEnumType.Unset }
            };
        }

        private static IList<IMaintainableObject> GetSdmxObjects()
        {
            IStructureParsingManager parsingManager = new StructureParsingManager();
            DirectoryInfo testDir = new DirectoryInfo("tests/v21/Structure");
            if (!testDir.Exists)
            {
                Trace.WriteLine(string.Format("Test directory '{0}' not found ", testDir.FullName));
            }

            var files = testDir.GetFiles("*.xml");
            List<IMaintainableObject> maintainableObjects = new List<IMaintainableObject>();
            foreach (var file in files)
            {
                ISdmxObjects sdmxObjects;
                try
                {
                    sdmxObjects = file.GetSdmxObjects(parsingManager);
                }
                catch (Exception e)
                {
                    Trace.WriteLine(e.ToString());
                    continue;
                }

                maintainableObjects.AddRange(sdmxObjects.GetAllMaintainables());
            }

            return maintainableObjects;
        }
    }
}
