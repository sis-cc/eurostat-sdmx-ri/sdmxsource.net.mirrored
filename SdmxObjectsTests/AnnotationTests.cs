// -----------------------------------------------------------------------
// <copyright file="AnnotationTests.cs" company="EUROSTAT">
//   Date Created : 2022-03-18
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjectsTests.
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxObjectsTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.MutableSuperObjects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.TestUtils;

    [TestFixture]
    public class AnnotationTests
    {
        [Test]
        public void TestAnnotationSingleURL()
        {
            IDataStructureMutableObject dsd = BuildDataStructureWithAnnotation();
            var annotation = dsd.Annotations.Single();
            string uri = "https://some.example.ie/en";
            annotation.Uri = new Uri(uri);

            IAnnotation immutable = dsd.ImmutableInstance.Annotations.Single();

            Assert.AreEqual(uri, immutable.Uri.ToString());
            Assert.That(immutable.Urls, Has.Count.EqualTo(1));
            Assert.IsNull(immutable.Urls.Single().Locale);
            Assert.AreEqual(uri, immutable.Urls.Single().Uri.ToString());
            Assert.AreEqual(annotation.Value, immutable.Value);

            // test also from immutable to mutable
            IAnnotationMutableObject roundtripAnnotation = new AnnotationMutableCore(immutable);
            Assert.AreEqual(uri, roundtripAnnotation.Uri.ToString());
            Assert.AreEqual(1, roundtripAnnotation.Urls.Count);
            Assert.IsNull(roundtripAnnotation.Urls.Single().Locale);
            Assert.AreEqual(uri, roundtripAnnotation.Urls.Single().Uri);
            Assert.AreEqual(annotation.Value, roundtripAnnotation.Value);

            // TEST ALSO THE SUPER BEAN

            IAnnotationSuperObject annotationSuperObject = new AnnotationSuperObject(immutable);

            Assert.AreEqual(uri, annotationSuperObject.Url.ToString());
            Assert.AreEqual(1, annotationSuperObject.Urls.Count);
            Assert.IsNull(annotationSuperObject.Urls.First().Locale);
            Assert.AreEqual(uri, annotationSuperObject.Urls.First().Uri.ToString());
            Assert.AreEqual(annotation.Value, annotationSuperObject.Value);

            // test also from immutable to mutable
            IAnnotationMutableSuperObject annotationMutableSuperObject = new AnnotationMutableSuperObject(annotationSuperObject);
            Assert.AreEqual(uri, annotationMutableSuperObject.Url.ToString());
            Assert.AreEqual(1, annotationMutableSuperObject.Urls.Count);
            Assert.IsNull(annotationMutableSuperObject.Urls.First().Locale);
            Assert.AreEqual(uri, annotationMutableSuperObject.Urls.First().Uri);
            Assert.AreEqual(annotation.Value, annotationMutableSuperObject.Value);
        }

        [Test]
        public void TestAnnotationMultipleURLs()
        {
            IDataStructureMutableObject dsd = BuildDataStructureWithAnnotation();
            Uri defaultUri = new Uri("https://some.example.ie/en");
            var annotation = dsd.Annotations.Single();
            annotation.AddUrl("en", defaultUri);
            annotation.AddUrl("fr", new Uri("https://www.exemple.fr"));
            annotation.AddUrl(null, new Uri("https://nolocale.com"));

            IAnnotation immutable = dsd.ImmutableInstance.Annotations.Single();

            Assert.That(immutable.Urls, Has.Count.EqualTo(3));
            Assert.AreEqual(defaultUri, immutable.Uri);

            // test also from immutable to mutable
            IAnnotationMutableObject roundtripAnnotation = new AnnotationMutableCore(immutable);
            Assert.AreEqual(3, roundtripAnnotation.Urls.Count);
            Assert.AreEqual(defaultUri, roundtripAnnotation.Uri);

            // TEST ALSO THE SUPER BEAN

            AnnotationSuperObject annotationSuperObject = new AnnotationSuperObject(immutable);

            Assert.AreEqual(3, annotationSuperObject.Urls.Count);
            Assert.AreEqual(defaultUri, annotationSuperObject.Url.ToString());

            //test also from immutable to mutable
            AnnotationMutableSuperObject annotationMutableSuperObject = new AnnotationMutableSuperObject(annotationSuperObject);
            Assert.AreEqual(3, annotationMutableSuperObject.Urls.Count);
            Assert.AreEqual(defaultUri, annotationMutableSuperObject.Url);
        }

        [Test]
        public void TestNoUri()
        {
            IDataStructureMutableObject dsd = BuildDataStructureWithAnnotation();
            IAnnotation immutable = dsd.ImmutableInstance.Annotations.Single();
            Assert.That(immutable.Urls, Has.Count.EqualTo(0));
            Assert.IsNull(immutable.Uri);

            // test also from immutable to mutable
            IAnnotationMutableObject roundtripAnnotation = new AnnotationMutableCore(immutable);
            Assert.AreEqual(0, roundtripAnnotation.Urls.Count);
            Assert.IsNull(roundtripAnnotation.Uri);

            // TEST ALSO THE SUPER BEAN

            AnnotationSuperObject annotationSuperObject = new AnnotationSuperObject(immutable);
            Assert.AreEqual(0, annotationSuperObject.Urls.Count);
            Assert.IsNull(annotationSuperObject.Url);

            //test also from immutable to mutable
            AnnotationMutableSuperObject annotationMutableSuperObject = new AnnotationMutableSuperObject(annotationSuperObject);
            Assert.AreEqual(0, annotationMutableSuperObject.Urls.Count);
            Assert.IsNull(annotationMutableSuperObject.Url);
        }

        [Test]
        public void TestAnnotationSuperObjectSingleUrl()
        {
            IDataStructureMutableObject dsd = BuildDataStructureWithAnnotation();
            var annotation = dsd.Annotations.Single();
            string uri = "https://some.example.ie/en";
            annotation.Uri = new Uri(uri);

            IAnnotation immutable = dsd.ImmutableInstance.Annotations.Single();

            AnnotationSuperObject annotationSuperObject = new AnnotationSuperObject(immutable);

            Assert.AreEqual(uri, annotationSuperObject.Url.ToString());
            Assert.AreEqual(1, annotationSuperObject.Urls.Count);
            Assert.IsNull(annotationSuperObject.Urls.Single().Locale);
            Assert.AreEqual(uri, annotationSuperObject.Urls.Single().Uri.ToString());
            Assert.AreEqual(annotation.Value, annotationSuperObject.Value);

            //test also from immutable to mutable
            AnnotationMutableSuperObject annotationMutableSuperObject = new AnnotationMutableSuperObject(annotationSuperObject);
            Assert.AreEqual(uri, annotationMutableSuperObject.Url.ToString());
            Assert.AreEqual(1, annotationMutableSuperObject.Urls.Count);
            Assert.IsNull(annotationMutableSuperObject.Urls.Single().Locale);
            Assert.AreEqual(uri, annotationMutableSuperObject.Urls.Single().Uri);
            Assert.AreEqual(annotation.Value, annotationMutableSuperObject.Value);
        }

        [Test]
        [TestCaseSource(nameof(deepEqualsArguments))]
        public void TestDeepEqualsURLs(
            string locale1, string uri1, string value1,
            string locale2, string uri2, string value2,
            bool expectedResult)
        {
            IDataStructureMutableObject dsd1 = BuildDataStructureWithAnnotation();
            var annotation1 = dsd1.Annotations.Single();
            annotation1.AddUrl(new AnnotationUrlMutableCore(locale1, uri1));
            annotation1.Value = value1;
            IDataStructureObject immutable1 = dsd1.ImmutableInstance;

            IDataStructureMutableObject dsd2 = BuildDataStructureWithAnnotation();
            var annotation2 = dsd2.Annotations.Single();
            annotation2.AddUrl(new AnnotationUrlMutableCore(locale2, uri2));
            annotation2.Value = value2;
            IDataStructureObject immutable2 = dsd2.ImmutableInstance;

            Assert.AreEqual(expectedResult, immutable1.DeepEquals(immutable2, true));
        }

        private static readonly object[] deepEqualsArguments =
        {
            new object[]
                {
                    "en", "https://some.example.ie/en", "value1",
                    "en", "https://some.example.ie/en", "value1",
                    true
                },
            new object[]
                {
                    null, "https://some.example.ie/en", "value1",
                    null, "https://some.example.ie/en", "value1",
                    true
                },
            new object[]
                {
                    null, "https://some.example.ie/en", "value1",
                    "en", "https://some.example.ie/en", "value1",
                    false
                },
            new object[]
                {
                    "en", "https://some.example.ie/en", "value1",
                    "en", "https://some.example.ie/en", "value2",
                    false
                },
            new object[]
                {
                    "en", "https://some.example.ie/en", "value1",
                    "fr", "https://some.example.ie/en", "value1",
                    false
                },
            new object[]
                {
                    "en", "https://some.example.ie/en", "value1",
                    "en", "https://www.exemple.fr/",    "value1",
                    false
                },
        };

        private static IDataStructureMutableObject BuildDataStructureWithAnnotation()
        {
            IDataStructureMutableObject dsd = SampleObjectsBuilder.BuildDataStructureMutableObject();
            IAnnotationMutableObject annotation = new AnnotationMutableCore()
            {
                Id = "annotationId",
                Title = "annotationTitle",
                Type = "annotationType",
                Value = "annotationValue"
            };
            dsd.AddAnnotation(annotation);
            return dsd;
        }
    }
}
