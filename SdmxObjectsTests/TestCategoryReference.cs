﻿// -----------------------------------------------------------------------
// <copyright file="TestCategoryReference.cs" company="EUROSTAT">
//   Date Created : 2016-09-23
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjectsTests.
// 
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxObjectsTests
{
    using System;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// Ensure that category reference is generated correctly at object level
    /// </summary>
    [TestFixture]
    public class TestCategoryReference
    {
        /// <summary>
        /// Tests if category reference is created with full path.
        /// </summary>
        [Test]
        public void TestIfCategoryReferenceIsCreatedWithFullPath()
        {
            ICategorySchemeMutableObject categoryScheme = new CategorySchemeMutableCore();
            categoryScheme.Id = "TEST_CAT_SCH";
            categoryScheme.AgencyId = "TEST";
            categoryScheme.Version = "1.0";
            categoryScheme.AddName("en", "Test CategoryScheme");
            
            ICategoryMutableObject rootCategory = new CategoryMutableCore();
            rootCategory.Id = "ROOT_CAT";
            rootCategory.AddName("en", "Root category");
            categoryScheme.AddItem(rootCategory);

            ICategoryMutableObject secondLevelCategory1 = new CategoryMutableCore() { Id = "SecondLevel1" };
            secondLevelCategory1.AddName("en", "2nd level 1");
            rootCategory.AddItem(secondLevelCategory1);

            ICategoryMutableObject thirdLevelCategory1 = new CategoryMutableCore() { Id = "ThirdLevel1" };
            thirdLevelCategory1.AddName("en", "3rd level 1");
            secondLevelCategory1.AddItem(thirdLevelCategory1);

            var categorySchemeObject = categoryScheme.ImmutableInstance;
            var categoryObject = categorySchemeObject.GetCategory("ROOT_CAT", "SecondLevel1", "ThirdLevel1");
            Assert.That(categoryObject, Has.Property("AsReference").EqualTo(new StructureReferenceImpl(categorySchemeObject, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Category), new[] { "ROOT_CAT", "SecondLevel1", "ThirdLevel1" })));
        }

        /// <summary>
        /// Tests if category reference is created with full path.
        /// </summary>
        [Test]
        public void TestIfCategoryReferenceUrnIsCreatedWithFullPath()
        {
            ICategorySchemeMutableObject categoryScheme = new CategorySchemeMutableCore();
            categoryScheme.Id = "TEST_CAT_SCH";
            categoryScheme.AgencyId = "TEST";
            categoryScheme.Version = "1.0";
            categoryScheme.AddName("en", "Test CategoryScheme");

            ICategoryMutableObject rootCategory = new CategoryMutableCore();
            rootCategory.Id = "ROOT_CAT";
            rootCategory.AddName("en", "Root category");
            categoryScheme.AddItem(rootCategory);

            ICategoryMutableObject secondLevelCategory1 = new CategoryMutableCore() { Id = "SecondLevel1" };
            secondLevelCategory1.AddName("en", "2nd level 1");
            rootCategory.AddItem(secondLevelCategory1);

            ICategoryMutableObject thirdLevelCategory1 = new CategoryMutableCore() { Id = "ThirdLevel1" };
            thirdLevelCategory1.AddName("en", "3rd level 1");
            secondLevelCategory1.AddItem(thirdLevelCategory1);

            var categorySchemeObject = categoryScheme.ImmutableInstance;
            var categoryObject = categorySchemeObject.GetCategory("ROOT_CAT", "SecondLevel1", "ThirdLevel1");
            Assert.That(categoryObject, Has.Property("AsReference").EqualTo(new StructureReferenceImpl(categorySchemeObject, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Category), new[] { "ROOT_CAT", "SecondLevel1", "ThirdLevel1" })));
            Assert.That(categoryObject.AsReference.TargetUrn, Is.EqualTo(new Uri("urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=TEST:TEST_CAT_SCH(1.0).ROOT_CAT.SecondLevel1.ThirdLevel1")));
        }
    }
}