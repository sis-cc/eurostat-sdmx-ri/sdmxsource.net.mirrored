// -----------------------------------------------------------------------
// <copyright file="DsdMeasuresTests.cs" company="EUROSTAT">
//   Date Created : 2022-03-09
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjectsTests.
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxObjectsTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.TestUtils;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    [TestFixture]
    public class DsdMeasuresTests
    {
        [Test]
        public void AddMultipleMeasuresWithNoPrimaryMeasure()
        {
            IDataStructureMutableObject dsd = SampleObjectsBuilder.BuildDataStructureMutableObject();

            IMeasureMutableObject measure1 = new MeasureMutableCore();
            measure1.Id = "MEASURE1";
            measure1.Usage = UsageType.Mandatory;
            measure1.ConceptRef = new StructureReferenceImpl(SampleObjectsBuilder.TEST_AGENCY_ID, SampleObjectsBuilder.TEST_CONCEPT_SCHEME_ID, 
                SampleObjectsBuilder.TEST_VERSION, SdmxStructureEnumType.Concept, "MEASURE1");
            dsd.AddMeasure(measure1);

            IMeasureMutableObject measure2 = new MeasureMutableCore();
            measure2.Id = "MEASURE2";
            measure2.Usage = UsageType.Mandatory;
            measure2.ConceptRef = new StructureReferenceImpl(SampleObjectsBuilder.TEST_AGENCY_ID, "TEST_CONCEPT_SCHEME2", 
                SampleObjectsBuilder.TEST_VERSION, SdmxStructureEnumType.Concept, "MEASURE2");
            dsd.AddMeasure(measure2);

            Assert.AreEqual(2, dsd.Measures.Count);
            Assert.True(dsd.Measures.Any(m => m.Id.Equals(measure1.Id)));
            Assert.True(dsd.Measures.Any(m => m.Id.Equals(measure2.Id)));
            Assert.Null(dsd.MeasureList.PrimaryMeasure);

            IDataStructureObject immutable = dsd.ImmutableInstance;
            Assert.AreEqual(2, immutable.Measures.Count);
            Assert.True(immutable.Measures.Any(m => m.Id.Equals(measure1.Id)));
            Assert.True(immutable.Measures.Any(m => m.Id.Equals(measure2.Id)));
            // We have primary measure only and only if there is a single measure and has id OBS_VALUE
            Assert.That(immutable.PrimaryMeasure, Is.Null);

            //also test from immutable to mutable
            IDataStructureMutableObject roundTripDsd = new DataStructureMutableCore(immutable);
            Assert.AreEqual(2, roundTripDsd.Measures.Count);
            Assert.True(roundTripDsd.Measures.Any(m => m.Id.Equals(measure1.Id)));
            Assert.True(roundTripDsd.Measures.Any(m => m.Id.Equals(measure2.Id)));
            // We have primary measure only and only if there is a single measure and has id OBS_VALUE
            Assert.That(roundTripDsd.PrimaryMeasure, Is.Null);
        }

        [Test]
        public void AddOnlyPrimaryMeasureFromSetter()
        {
            IDataStructureMutableObject dsd = SampleObjectsBuilder.BuildDataStructureMutableObject();

            IStructureReference primaryConceptRef = new StructureReferenceImpl(SampleObjectsBuilder.TEST_AGENCY_ID, 
                SampleObjectsBuilder.TEST_CONCEPT_SCHEME_ID, SampleObjectsBuilder.TEST_VERSION, SdmxStructureEnumType.Concept, "OBS_VALUE");
            IPrimaryMeasureMutableObject primaryMeasureMutableBean = new PrimaryMeasureMutableCore();
            primaryMeasureMutableBean.ConceptRef = primaryConceptRef;
            dsd.PrimaryMeasure = primaryMeasureMutableBean;

            Assert.AreEqual(0, dsd.Measures.Count);

            IDataStructureObject immutable = dsd.ImmutableInstance;
            Assert.AreEqual(1, immutable.Measures.Count);
            Assert.AreEqual(primaryConceptRef.IdentifiableIds[0], immutable.Measures[0].Id);
            Assert.AreEqual(primaryConceptRef, immutable.PrimaryMeasure.ConceptRef);

            //also test from immutable to mutable
            IDataStructureMutableObject roundTripDsd = new DataStructureMutableCore(immutable);
            Assert.AreEqual(1, roundTripDsd.Measures.Count);
            Assert.NotNull(roundTripDsd.MeasureList.PrimaryMeasure);
        }

        [Test]
        public void AddOnlyPrimaryMeasureFromAddMethod()
        {
            IDataStructureMutableObject dsd = SampleObjectsBuilder.BuildDataStructureMutableObject();

            IStructureReference primaryConceptRef = new StructureReferenceImpl(SampleObjectsBuilder.TEST_AGENCY_ID, 
                SampleObjectsBuilder.TEST_CONCEPT_SCHEME_ID, SampleObjectsBuilder.TEST_VERSION, SdmxStructureEnumType.Concept, "OBS_VALUE");
            dsd.AddPrimaryMeasure(primaryConceptRef);

            Assert.AreEqual(0, dsd.Measures.Count);

            IDataStructureObject immutable = dsd.ImmutableInstance;
            Assert.AreEqual(1, immutable.Measures.Count);
            Assert.AreEqual(primaryConceptRef.IdentifiableIds[0], immutable.Measures[0].Id);
            Assert.AreEqual(primaryConceptRef, immutable.PrimaryMeasure.ConceptRef);

            //also test from immutable to mutable
            IDataStructureMutableObject roundTripDsd = new DataStructureMutableCore(immutable);
            Assert.AreEqual(1, roundTripDsd.Measures.Count);
            Assert.NotNull(roundTripDsd.MeasureList.PrimaryMeasure);
        }

        [Test]
        public void AttachAttributeToNonExistentMeasure()
        {
            IDataStructureMutableObject dsd = SampleObjectsBuilder.BuildDataStructureMutableObject();

            IMeasureMutableObject measure = new MeasureMutableCore();
            measure.Id = "MEASURE1";
            measure.Usage = UsageType.Mandatory;
            measure.ConceptRef = new StructureReferenceImpl(SampleObjectsBuilder.TEST_AGENCY_ID, 
                SampleObjectsBuilder.TEST_CONCEPT_SCHEME_ID, SampleObjectsBuilder.TEST_VERSION, SdmxStructureEnumType.Concept, "MEASURE1");
            dsd.AddMeasure(measure);

            IAttributeMutableObject attributeMutableBean = SampleObjectsBuilder.BuildAttributeMutableObject();
            attributeMutableBean.AttachmentLevel = AttributeAttachmentLevel.Observation;
            attributeMutableBean.MeasureRelationships.Add("MEASURE_XXX");
            dsd.AddAttribute(attributeMutableBean);
            
            Assert.Throws<SdmxSemmanticException>(() => { var immutable = dsd.ImmutableInstance; });
        }

        [Test]
        public void AttachAttributeToMeasure()
        {
            string measureId = "MEASURE_X";
            IDataStructureMutableObject dsd = SampleObjectsBuilder.BuildDataStructureMutableObject();

            IMeasureMutableObject measure = new MeasureMutableCore();
            measure.Id = measureId;
            measure.Usage = UsageType.Mandatory;
            measure.ConceptRef = new StructureReferenceImpl(SampleObjectsBuilder.TEST_AGENCY_ID, 
                SampleObjectsBuilder.TEST_CONCEPT_SCHEME_ID, SampleObjectsBuilder.TEST_VERSION, SdmxStructureEnumType.Concept, "MEASURE");
            dsd.AddMeasure(measure);

            IAttributeMutableObject attributeMutableBean = SampleObjectsBuilder.BuildAttributeMutableObject();
            attributeMutableBean.AttachmentLevel = AttributeAttachmentLevel.Observation;
            attributeMutableBean.MeasureRelationships.Add(measureId);
            dsd.AddAttribute(attributeMutableBean);

            IDataStructureObject immutable = dsd.ImmutableInstance;
            Assert.AreEqual(1, immutable.Measures.Count);
            Assert.AreEqual(1, immutable.GetAttribute(attributeMutableBean.Id).MeasureRelationships.Count);
            Assert.AreEqual(measure.Id, immutable.GetAttribute(attributeMutableBean.Id).MeasureRelationships.First());

            //also test from immutable to mutable
            IDataStructureMutableObject roundTripDsd = new DataStructureMutableCore(immutable);
            Assert.AreEqual(1, roundTripDsd.Measures.Count);
            Assert.AreEqual(1, roundTripDsd.Attributes.Count);
            Assert.True(roundTripDsd.Attributes[0].MeasureRelationships.Contains(measureId));
        }

        [Test]
        public void AttachAttributeToMultipleMeasure()
        {
            string measureId1 = "MEASURE_1";
            string measureId2 = "MEASURE_2";

            IDataStructureMutableObject dsd = SampleObjectsBuilder.BuildDataStructureMutableObject();

            IMeasureMutableObject measure1 = new MeasureMutableCore();
            measure1.Id = measureId1;
            measure1.Usage = UsageType.Mandatory;
            measure1.ConceptRef = new StructureReferenceImpl(SampleObjectsBuilder.TEST_AGENCY_ID, 
                SampleObjectsBuilder.TEST_CONCEPT_SCHEME_ID, SampleObjectsBuilder.TEST_VERSION, SdmxStructureEnumType.Concept, "MEASURE1");
            dsd.AddMeasure(measure1);

            IMeasureMutableObject measure2 = new MeasureMutableCore();
            measure2.Id = measureId2;
            measure2.Usage = UsageType.Mandatory;
            measure2.ConceptRef = new StructureReferenceImpl(SampleObjectsBuilder.TEST_AGENCY_ID, "TEST_CONCEPT_SCHEME2", 
                SampleObjectsBuilder.TEST_VERSION, SdmxStructureEnumType.Concept, "MEASURE2");
            dsd.AddMeasure(measure2);

            IAttributeMutableObject attributeMutableBean = SampleObjectsBuilder.BuildAttributeMutableObject();
            attributeMutableBean.AttachmentLevel = AttributeAttachmentLevel.Observation;
            attributeMutableBean.MeasureRelationships.Add(measureId1);
            attributeMutableBean.MeasureRelationships.Add(measureId2);

            dsd.AddAttribute(attributeMutableBean);

            IDataStructureObject immutable = dsd.ImmutableInstance;
            Assert.AreEqual(2, immutable.Measures.Count);
            CollectionAssert.AreEqual(new List<string>() { measureId1, measureId2 }, immutable.GetAttribute(attributeMutableBean.Id).MeasureRelationships);

            //also test from immutable to mutable
            IDataStructureMutableObject roundTripDsd = new DataStructureMutableCore(immutable);
            Assert.AreEqual(2, roundTripDsd.Measures.Count);
            CollectionAssert.AreEqual(new List<string>() { measureId1, measureId2 }, roundTripDsd.Attributes[0].MeasureRelationships);
        }

        [Test]
        public void AttachAttributeToMeasureWrongAttachmentLevel()
        {
            IDataStructureMutableObject dsd = SampleObjectsBuilder.BuildDataStructureMutableObject();

            IMeasureMutableObject measure = new MeasureMutableCore();
            measure.Id = "MEASURE1";
            measure.Usage = UsageType.Mandatory;
            measure.ConceptRef = new StructureReferenceImpl(SampleObjectsBuilder.TEST_AGENCY_ID, 
                SampleObjectsBuilder.TEST_CONCEPT_SCHEME_ID, SampleObjectsBuilder.TEST_VERSION, SdmxStructureEnumType.Concept, "MEASURE1");
            dsd.AddMeasure(measure);

            IAttributeMutableObject attributeMutableBean = SampleObjectsBuilder.BuildAttributeMutableObject();
            attributeMutableBean.MeasureRelationships.Add("MEASURE1");
            attributeMutableBean.AttachmentLevel = AttributeAttachmentLevel.Group;
            dsd.AddAttribute(attributeMutableBean);


            Assert.Throws<SdmxSemmanticException>(() => { var immutable = dsd.ImmutableInstance; });
        }

        /// <summary>
        /// test deep equals with 2 dsds that have two measures each. THe first measure is exactly the same in both dsds.
        /// The second measure of each dsd is parameterized by the unit test
        /// </summary>
        /// <param name="dsd1Measure2Role">The second measure for control dsd</param>
        /// <param name="dsd2Measure2Role">The second measure for test dsd</param>
        /// <param name="dsd1Measure2Usage">The usage type for the second measure for control dsd</param>
        /// <param name="dsd2Measure2Usage">The usage type for the second measure for test dsd</param>
        /// <param name="expectedResult"><c>true</c> if they are supposed to be equal, <c>false</c> otherwise.</param>
        [Test]
        [TestCaseSource("DeepEqualsArguments")]
        public void TestDeepEquals(string dsd1Measure2Role, string dsd2Measure2Role, UsageType dsd1Measure2Usage, UsageType dsd2Measure2Usage, bool expectedResult)
        {
            IDataStructureMutableObject dsd1 = SampleObjectsBuilder.BuildDataStructureMutableObject();

            IMeasureMutableObject dsd1Measure1 = new MeasureMutableCore();
            dsd1Measure1.Id = "MEASURE1";
            dsd1Measure1.Usage = UsageType.Mandatory;
            dsd1Measure1.ConceptRef = new StructureReferenceImpl(SampleObjectsBuilder.TEST_AGENCY_ID, 
                SampleObjectsBuilder.TEST_CONCEPT_SCHEME_ID, SampleObjectsBuilder.TEST_VERSION, SdmxStructureEnumType.Concept, "MEASURE1");
            dsd1.AddMeasure(dsd1Measure1);

            IMeasureMutableObject dsd1Measure2 = new MeasureMutableCore();
            dsd1Measure2.Id = "MEASURE2";
            dsd1Measure2.Usage = dsd1Measure2Usage;
            dsd1Measure2.ConceptRef = new StructureReferenceImpl(SampleObjectsBuilder.TEST_AGENCY_ID, "TEST_CONCEPT_SCHEME2", 
                SampleObjectsBuilder.TEST_VERSION, SdmxStructureEnumType.Concept, "MEASURE2");
            if (dsd1Measure2Role != null)
            {
                dsd1Measure2.ConceptRoles.Add(new StructureReferenceImpl(SampleObjectsBuilder.TEST_AGENCY_ID, 
                    SampleObjectsBuilder.TEST_CONCEPT_SCHEME_ID, SampleObjectsBuilder.TEST_VERSION, SdmxStructureEnumType.Concept, dsd1Measure2Role));
            }
            dsd1.AddMeasure(dsd1Measure2);

            IDataStructureObject immutable = dsd1.ImmutableInstance;

            IDataStructureMutableObject dsd2 = SampleObjectsBuilder.BuildDataStructureMutableObject();

            IMeasureMutableObject dsd2Measure1 = new MeasureMutableCore();
            dsd2Measure1.Id = "MEASURE1";
            dsd2Measure1.Usage = UsageType.Mandatory;
            dsd2Measure1.ConceptRef = new StructureReferenceImpl(SampleObjectsBuilder.TEST_AGENCY_ID, 
                SampleObjectsBuilder.TEST_CONCEPT_SCHEME_ID, SampleObjectsBuilder.TEST_VERSION, SdmxStructureEnumType.Concept, "MEASURE1");
            dsd2.AddMeasure(dsd2Measure1);

            IMeasureMutableObject dsd2Measure2 = new MeasureMutableCore();
            dsd2Measure2.Id = "MEASURE2";
            dsd2Measure2.Usage = dsd2Measure2Usage;
            dsd2Measure2.ConceptRef = new StructureReferenceImpl(SampleObjectsBuilder.TEST_AGENCY_ID, "TEST_CONCEPT_SCHEME2", 
                SampleObjectsBuilder.TEST_VERSION, SdmxStructureEnumType.Concept, "MEASURE2");
            if (dsd2Measure2Role != null)
            {
                dsd2Measure2.ConceptRoles.Add(new StructureReferenceImpl(SampleObjectsBuilder.TEST_AGENCY_ID, 
                    SampleObjectsBuilder.TEST_CONCEPT_SCHEME_ID, SampleObjectsBuilder.TEST_VERSION, SdmxStructureEnumType.Concept, dsd2Measure2Role));
            }
            dsd2.AddMeasure(dsd2Measure2);

            IDataStructureObject immutable2 = dsd2.ImmutableInstance;

            Assert.AreEqual(expectedResult, immutable.DeepEquals(immutable2, true));
        }

        private static IEnumerable<object> DeepEqualsArguments()
        {
            return new object[] {
                new object[] { "commonRole", "commonRole", UsageType.Mandatory, UsageType.Mandatory, true},
                new object[]  { "commonRole", "commonRole", UsageType.Optional, UsageType.Optional, true},
                new object[] { "commonRole", "commonRole", null, null, true},
                new object[] { null, null, null, null, true},

                //make sure that deep equals fails if either one of the measure roles or usage types are different

                new object[] { "roleX", "roleY", UsageType.Mandatory, UsageType.Mandatory, false},
                new object[] { "commonRole", "commonRole", UsageType.Mandatory, UsageType.Optional, false},
                new object[] { "commonRole", "commonRole", UsageType.Mandatory, null, false},
                new object[] { null, "roleY", UsageType.Mandatory, UsageType.Mandatory, false}
            };
        }
    }
}
