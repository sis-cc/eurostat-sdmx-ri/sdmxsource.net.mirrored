// -----------------------------------------------------------------------
// <copyright file="CategorySchemeMutableSuperObjectTests.cs" company="EUROSTAT">
//   Date Created : 2016-02-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjectsTests.
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;

using FluentAssertions;

using NUnit.Framework;

using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.CategoryScheme;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.CategoryScheme;

namespace SdmxObjectsTests
{
    [TestFixture]
    class CategorySchemeMutableSuperObjectTests
    {
        private ICategorySchemeObject categoryScheme;
        private CategorySchemeSuperObject _categorySchemeSuperObject;

        [SetUp]
        public virtual void SetUp()
        {
            categoryScheme = Utils.CategoryScheme;
            _categorySchemeSuperObject = new CategorySchemeSuperObject(categoryScheme);
        }

        /// <summary>
        /// Creates the sut.
        /// </summary>
        /// <returns></returns>
        public CategorySchemeMutableSuperObject CreateSUT()
        {
            
            return new CategorySchemeMutableSuperObject(_categorySchemeSuperObject);
        }

        public class When_constructing_a_CategorySchemeMutableSuperObject : CategorySchemeMutableSuperObjectTests
        {
            [Test]
            public void Should_be_able_to_construct_object()
            {
                CreateSUT();
            }

            [Test]
            public void Should_contain_the_categories_from_the_original_CategorySchemeSuperObject()
            {
                var sut = CreateSUT();
                sut.Categories.Should().OnlyContain(y=>_categorySchemeSuperObject.Categories.ToList().Exists(x=>x.Id == y.Id));
            }
        }
    }
}