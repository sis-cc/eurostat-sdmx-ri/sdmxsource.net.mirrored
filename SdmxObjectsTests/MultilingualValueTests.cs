using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Exception;

namespace SdmxObjectsTests
{
    [TestFixture]
    public class MultilingualValueTests
    {
        [Test]
        public void ShouldAllowMultilanguageOnAttributesAndMeasures()
        {
            var dsd = Utils.BuildTestDataStructureMutableBean("3.0", true);
            Utils.AddAttributeFor(dsd, "Attribute", true);
            Utils.AddMeasureFor(dsd, "Measure", true);

            var immutable = dsd.ImmutableInstance;
            Assert.True(immutable.GetAttribute("Attribute").Representation.TextFormat.Multilingual.IsTrue);
            Assert.True(immutable.Measures.First(x=>x.Id == "Measure").Representation.TextFormat.Multilingual.IsTrue);
        }

        [Test]
        public void ShouldThrowExceptionForMultilanguageTrueOnDimensions()
        {
            var dsd = Utils.BuildTestDataStructureMutableBean("3.0", true);
            Utils.AddDimensionFor(dsd, "Dimension", true);
            Assert.Throws<SdmxSemmanticException>(() => { var immutable = dsd.ImmutableInstance; });
        }

        [Test]
        public void ShouldAllowMultilanguageFalseOnDimensions()
        {
            var dsd = Utils.BuildTestDataStructureMutableBean("3.0", true);
            Utils.AddDimensionFor(dsd, "Dimension", false);
            var immutable = dsd.ImmutableInstance;
            Assert.False(immutable.GetDimension("Dimension").Representation.TextFormat.Multilingual.IsTrue);
        }

        [Test]
        public void DSDsShouldBeDeepEqual()
        {
            var dsd1 = Utils.BuildTestDataStructureMutableBean("3.0", true);
            Utils.AddAttributeFor(dsd1, "Attribute", true);
            Utils.AddMeasureFor(dsd1, "Measure", true);
            Utils.AddDimensionFor(dsd1, "Dimension", false);

            var dsd2 = Utils.BuildTestDataStructureMutableBean("3.0", true);
            Utils.AddAttributeFor(dsd2, "Attribute", true);
            Utils.AddMeasureFor(dsd2, "Measure", true);
            Utils.AddDimensionFor(dsd2, "Dimension", false);

            var immutable1 = dsd1.ImmutableInstance;
            var immutable2 = dsd2.ImmutableInstance;

            Assert.True(immutable1.DeepEquals(immutable2,true));
        }
    }
}
