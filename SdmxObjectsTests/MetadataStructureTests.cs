// -----------------------------------------------------------------------
// <copyright file="MetadataStructureTests.cs" company="EUROSTAT">
//   Date Created : 2022-03-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjectsTests.
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxObjectsTests
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.TestUtils;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    [TestFixture]
    public class MetadataStructureTests
    {
        [Test]
        public void TestMetadataStructure()
        {
            IDataStructureMutableObject dsd = SampleObjectsBuilder.BuildDataStructureMutableObject();
            dsd.MetadataStructure = new StructureReferenceImpl("EXAMPLE", "TEST_MSD", "1.0.0", SdmxStructureEnumType.Msd);

            IDataStructureObject immutable = dsd.ImmutableInstance;

            Assert.AreEqual("EXAMPLE", immutable.MetadataStructure.AgencyId);
            Assert.AreEqual("TEST_MSD", immutable.MetadataStructure.MaintainableId);
            Assert.AreEqual("1.0.0", immutable.MetadataStructure.Version);
            Assert.AreEqual(SdmxStructureEnumType.Msd, immutable.MetadataStructure.MaintainableStructureEnumType.EnumType);

            //also test from immutable to mutable
            IDataStructureMutableObject roundTripDsd = new DataStructureMutableCore(immutable);
            Assert.AreEqual("EXAMPLE", roundTripDsd.MetadataStructure.AgencyId);
            Assert.AreEqual("TEST_MSD", roundTripDsd.MetadataStructure.MaintainableId);
            Assert.AreEqual("1.0.0", roundTripDsd.MetadataStructure.Version);
            Assert.AreEqual(SdmxStructureEnumType.Msd, roundTripDsd.MetadataStructure.MaintainableStructureEnumType.EnumType);
        }

        [Test]
        [TestCaseSource(nameof(DeepEqualsArguments))]
        public void TestDeepEquals(IStructureReference msd1, IStructureReference msd2, bool expectedResult)
        {
            IDataStructureMutableObject dsd1 = SampleObjectsBuilder.BuildDataStructureMutableObject();
            dsd1.MetadataStructure = msd1;
            IDataStructureObject immutable = dsd1.ImmutableInstance;

            IDataStructureMutableObject dsd2 = SampleObjectsBuilder.BuildDataStructureMutableObject();
            dsd2.MetadataStructure = msd2;
            IDataStructureObject immutable2 = dsd2.ImmutableInstance;

            Assert.AreEqual(expectedResult, immutable.DeepEquals(immutable2, true));
        }

        private static IEnumerable<object> DeepEqualsArguments()
        {
            return new object[] {
                new object[] { new StructureReferenceImpl("EXAMPLE", "TEST_MSD", "1.0.0", SdmxStructureEnumType.Msd),
                            new StructureReferenceImpl("EXAMPLE", "TEST_MSD", "1.0.0", SdmxStructureEnumType.Msd),
                            true},
                new object[]  { new StructureReferenceImpl("EXAMPLE1", "TEST_MSD", "1.0.0", SdmxStructureEnumType.Msd),
                            new StructureReferenceImpl("EXAMPLE2", "TEST_MSD", "1.0.0", SdmxStructureEnumType.Msd),
                            false},
                new object[] { new StructureReferenceImpl("EXAMPLE", "TEST_MSD1", "1.0.0", SdmxStructureEnumType.Msd),
                            new StructureReferenceImpl("EXAMPLE", "TEST_MSD2", "1.0.0", SdmxStructureEnumType.Msd),
                            false},
                new object[] { new StructureReferenceImpl("EXAMPLE", "TEST_MSD", "1.0.0", SdmxStructureEnumType.Msd),
                            new StructureReferenceImpl("EXAMPLE", "TEST_MSD", "2.0.0", SdmxStructureEnumType.Msd),
                            false}
            };
        }
    }
}
