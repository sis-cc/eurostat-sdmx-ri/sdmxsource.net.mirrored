// -----------------------------------------------------------------------
// <copyright file="TestMutableImmutable.cs" company="EUROSTAT">
//   Date Created : 2017-04-12
//   Copyright (c) 2012, 2017 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjectsTests.
// 
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxObjectsTests
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;

    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util.ResourceBundle;

    [TestFixture]
    public class TestMutableImmutable
    {
        public TestMutableImmutable()
        {
            // show meaningful exception messages
            SdmxException.SetMessageResolver(new MessageDecoder());
        }

        [Test]
        public void TestMutableToImmutableRoundTrip()
        {
            IMaintainableObject[] _maintainable = GetSdmxObjects().ToArray();
            var artefact = _maintainable[0];
            var maintainableMutableObject = artefact.MutableInstance;
            var newImmutableInstance = maintainableMutableObject.ImmutableInstance;

            Assert.That(artefact, Is.EqualTo(newImmutableInstance));
            Assert.That(artefact.DeepEquals(newImmutableInstance, false));
        }

        [Test]
        public void AddAMeasure()
        {
            IDataStructureMutableObject dsd = BuildTestDataStructureMutableBean();

            IMeasureMutableObject measure1 = new MeasureMutableCore();
            measure1.Id = "MEASURE1";
            measure1.Usage = UsageType.Mandatory;
            measure1.ConceptRef = new StructureReferenceImpl("EXAMPLE", "TEST_CONCEPT_SCHEME", "1.0.0", SdmxStructureEnumType.Concept, "MEASURE1");
            dsd.AddMeasure(measure1);

            IDataStructureObject immutable = dsd.ImmutableInstance;
            Assert.IsNotEmpty(immutable.Measures);
            Assert.AreEqual(immutable.Measures.First().Id, measure1.Id);
            Assert.AreEqual(immutable.Measures.First().Usage, measure1.Usage);
        }

        [Test]
        public void AddMeasureMinMaxOccur()
        {
            IDataStructureMutableObject dsd = BuildTestDataStructureMutableBean();

            IMeasureMutableObject measure1 = new MeasureMutableCore();
            measure1.Id = "MEASURE1";
            measure1.Usage = UsageType.Mandatory;
            IRepresentationMutableObject representationMutableObject = new RepresentationMutableCore();
            representationMutableObject.MinOccurs = 2;
            IOccurenceObject maxOccurs2 = new FiniteOccurenceObjectCore(2);
            representationMutableObject.MaxOccurs = maxOccurs2;
            ITextFormatMutableObject textFormat = new TextFormatMutableCore();
            textFormat.Multilingual = TertiaryBool.ParseBoolean(true);
            textFormat.TextType = TextType.GetFromEnum(TextEnumType.String);
            representationMutableObject.TextFormat = textFormat;
            measure1.Representation = representationMutableObject;
            measure1.ConceptRef = new StructureReferenceImpl("EXAMPLE", "TEST_CONCEPT_SCHEME", "1.0.0", SdmxStructureEnumType.Concept, "MEASURE1");
            dsd.AddMeasure(measure1);

            IDataStructureObject immutable = dsd.ImmutableInstance;
            Assert.IsNotEmpty(immutable.Measures);
            Assert.AreEqual(immutable.Measures.First().Id, measure1.Id);
            Assert.AreEqual(immutable.Measures.First().RepresentationMinOccurs, 2);
            Assert.AreEqual(immutable.Measures.First().RepresentationMaxOccurs, maxOccurs2);
        }

        [Test]
        public void AddMetadataStructure()
        {
            IDataStructureMutableObject dsd = BuildTestDataStructureMutableBean();

            dsd.MetadataStructure = new StructureReferenceImpl("EXAMPLE", "TEST_MSD", "1.0.0", SdmxStructureEnumType.Msd);
            dsd.AttributeList = new AttributeListMutableCore();
            IMetadataAttributeUsageMutableObject attr = new MetadataAttributeUsageMutableCore();
            attr.MetadataAttributeReference = "CONTACT";
            attr.AttachmentLevel = AttributeAttachmentLevel.DimensionGroup;
            attr.DimensionReferences = new List<string>{ "FREQ" }.AsReadOnly();
            dsd.AttributeList.MetadataAttributes.Add(attr);

            IDataStructureObject immutable = dsd.ImmutableInstance;
            Assert.NotNull(immutable.MetadataStructure);
            Assert.IsNotEmpty(immutable.GetMetadataAttributes());
            Assert.IsTrue(immutable.GetMetadataAttributes().Count == 1);
            Assert.NotNull(immutable.GetMetadataAttribute(attr.MetadataAttributeReference));
            Assert.IsTrue(immutable.GetDimensionGroupMetadataAttributes().Count == 1);
        }

        [Test]
        public void AddMaxOccurs()
        {
            IDataStructureMutableObject dsd = BuildTestDataStructureMutableBean();

            dsd.MetadataStructure = new StructureReferenceImpl("EXAMPLE", "TEST_MSD", "1.0.0", SdmxStructureEnumType.Msd);
            dsd.AttributeList = new AttributeListMutableCore();
            IAttributeMutableObject unit = new AttributeMutableCore();
            unit.Id = "UNIT";
            unit.AttachmentLevel = AttributeAttachmentLevel.DataSet;
            unit.ConceptRef = new StructureReferenceImpl("EXAMPLE", "TEST_CONCEPT_SCHEME", "1.0.0", SdmxStructureEnumType.Concept, "UNIT");
            IRepresentationMutableObject representationMutableBean = new RepresentationMutableCore();
            representationMutableBean.MaxOccurs = UnboundedOccurenceObjectCore.Instance;
            ITextFormatMutableObject textFormat = new TextFormatMutableCore();
            textFormat.TextType = TextType.GetFromEnum(TextEnumType.Xhtml);
            ISentinelValueMutableObject value = new SentinelValueMutableCore();
            value.Value = "-100";
            value.Names = new List<ITextTypeWrapperMutableObject>(); 
            value.Names.Add(new TextTypeWrapperMutableCore("en", "Not applicable"));
            textFormat.SentinelValues.Add(value);
            representationMutableBean.TextFormat = textFormat;
            unit.Representation = representationMutableBean;
            dsd.AddAttribute(unit);

            IDataStructureObject immutable = dsd.ImmutableInstance;
            Assert.NotNull(immutable.GetAttribute(unit.Id));
            Assert.AreEqual(immutable.GetAttribute(unit.Id).RepresentationMaxOccurs, UnboundedOccurenceObjectCore.Instance);
        }

        [Test]
        public void TestSentinelValues()
        {
            IDataStructureMutableObject dsd = BuildTestDataStructureMutableBean();

            dsd.MetadataStructure = new StructureReferenceImpl("EXAMPLE", "TEST_MSD", "1.0.0", SdmxStructureEnumType.Msd);
            dsd.AttributeList = new AttributeListMutableCore();
            IAttributeMutableObject unit = new AttributeMutableCore();
            unit.Id = "UNIT";
            unit.AttachmentLevel = AttributeAttachmentLevel.DataSet;
            unit.ConceptRef = new StructureReferenceImpl("EXAMPLE", "TEST_CONCEPT_SCHEME", "1.0.0", SdmxStructureEnumType.Concept, "UNIT");
            IRepresentationMutableObject representationMutableBean = new RepresentationMutableCore();
            representationMutableBean.MaxOccurs = UnboundedOccurenceObjectCore.Instance;
            ITextFormatMutableObject textFormat = new TextFormatMutableCore();
            textFormat.TextType = TextType.GetFromEnum(TextEnumType.Xhtml);
            ISentinelValueMutableObject value = new SentinelValueMutableCore();
            value.Value = "-100";
            value.Names = new List<ITextTypeWrapperMutableObject>();
            value.Names.Add(new TextTypeWrapperMutableCore("en", "Not applicable"));
            textFormat.SentinelValues.Add(value);
            representationMutableBean.TextFormat = textFormat;
            unit.Representation = representationMutableBean;
            dsd.AddAttribute(unit);
            
            IDataStructureObject immutable = dsd.ImmutableInstance;
            Assert.AreEqual(immutable.GetAttribute(unit.Id).Representation.TextFormat.SentinelValues.First().Value, textFormat.SentinelValues.First().Value);
        }

        private static IList<IMaintainableObject> GetSdmxObjects()
        {
            IStructureParsingManager parsingManager = new StructureParsingManager();
            DirectoryInfo testDir = new DirectoryInfo("tests/v21/Structure");
            if (!testDir.Exists)
            {
                Trace.WriteLine(string.Format("Test directory '{0}' not found ", testDir.FullName));
            }

            var files = testDir.GetFiles("*.xml");
            List<IMaintainableObject> maintainableObjects = new List<IMaintainableObject>();
            foreach (var file in files)
            {
                ISdmxObjects sdmxObjects;
                try
                {
                    sdmxObjects = file.GetSdmxObjects(parsingManager);
                }
                catch (Exception e)
                {
                    Trace.WriteLine(e.ToString());
                    continue;
                }
                
                maintainableObjects.AddRange(sdmxObjects.GetAllMaintainables());
            }

            return maintainableObjects;
        }

        private IDataStructureMutableObject BuildTestDataStructureMutableBean()
        {
            IDataStructureMutableObject dsd = new DataStructureMutableCore();
            // Mandatory information
            // agency
            dsd.AgencyId = "EXAMPLE";
            // id
            dsd.Id = "TEST_ID";
            // a name
            dsd.AddName("en", "Mandatory name");
            // a dimension
            dsd.AddDimension(
                new StructureReferenceImpl("EXAMPLE", "TEST_CONCEPT_SCHEME", "1.0.0", SdmxStructureEnumType.Concept, "FREQ"),
                new StructureReferenceImpl("EXAMPLE", "CL_FREQ", "1.0.0", SdmxStructureEnumType.CodeList)
                );

            return dsd;
        }
    }
}