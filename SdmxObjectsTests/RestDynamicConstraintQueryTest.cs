// -----------------------------------------------------------------------
// <copyright file="RestDynamicConstraintQueryTest.cs" company="EUROSTAT">
//   Date Created : 2018-4-27
//   Copyright (c) 2018 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjectsTests.
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxObjectsTests
{
    using System.Collections;
    using System.Collections.Generic;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    [TestFixture]
    public class RestDynamicConstraintQueryTest
    {
        [TestCaseSource("TestCases")]
        public void ParsingRestDynamicConstraing(string restString, IStructureReference expectedFlowRef, IList<string> componentIds, string startPeriod, string endPeriod, IList<ISet<string>> key, SdmxStructureEnumType? specificStructure)
        {
            var restQuery = new RestAvailableConstraintQuery(restString, null);
            Assert.That(restQuery.FlowRef, Is.EqualTo(expectedFlowRef));
            
            Assert.That(restQuery.ComponentIds, Is.EquivalentTo(componentIds));
            if (startPeriod == null)
            {
                Assert.That(restQuery.StartPeriod, Is.Null);
            }
            else
            {
                Assert.That(restQuery.StartPeriod.DateInSdmxFormat, Is.EqualTo(startPeriod));
            }

            if (endPeriod == null)
            {
                Assert.That(restQuery.EndPeriod, Is.Null);
            }
            else
            {
                Assert.That(restQuery.EndPeriod.DateInSdmxFormat, Is.EqualTo(endPeriod));
            }

            var expectedStructureType = specificStructure.HasValue ? SdmxStructureType.GetFromEnum(specificStructure.Value) : null;
            Assert.That(restQuery.SpecificStructureReference, Is.EqualTo(expectedStructureType));

            Assert.That(restQuery.QueryList.Count, Is.EqualTo(key.Count));
            for (int i = 0; i < restQuery.QueryList.Count; i++)
            {
                var got = restQuery.QueryList[i];
                var expected = key[i];
                Assert.That(got, Is.EquivalentTo(expected));
            }
        }

        private static IEnumerable TestCases
        {
            get
            {
                yield return new TestCaseData(RestAvailableConstraintQuery.ResourceName + "/ESTAT,NA_MAIN,1.3", new StructureReferenceImpl("ESTAT", "NA_MAIN", "1.3", SdmxStructureEnumType.Dataflow) , new string[0], null, null, new List<ISet<string>>(), null );
                yield return new TestCaseData(RestAvailableConstraintQuery.ResourceName + "/ESTAT,NA_MAIN,1.3/ALL/ALL/FREQ", new StructureReferenceImpl("ESTAT", "NA_MAIN", "1.3", SdmxStructureEnumType.Dataflow), new string[] { "FREQ" }, null, null, new List<ISet<string>>(), null);
                yield return new TestCaseData(RestAvailableConstraintQuery.ResourceName + "/ESTAT,NA_MAIN,1.3/ALL/ALL/FREQ+REF_AREA", new StructureReferenceImpl("ESTAT", "NA_MAIN", "1.3", SdmxStructureEnumType.Dataflow), new string[] { "FREQ", "REF_AREA" }, null, null, new List<ISet<string>>(), null);
                yield return new TestCaseData(RestAvailableConstraintQuery.ResourceName + "/ESTAT,NA_MAIN,1.3/A+M..K/ALL/FREQ+REF_AREA", new StructureReferenceImpl("ESTAT", "NA_MAIN", "1.3", SdmxStructureEnumType.Dataflow), new string[] { "FREQ", "REF_AREA" }, null, null, new List<ISet<string>>() { new HashSet<string>() {"A", "M"}, new HashSet<string>(), new HashSet<string>() {"K"}}, null);
                yield return new TestCaseData(RestAvailableConstraintQuery.ResourceName + "/ESTAT,NA_MAIN,1.3/A+M..K/ALL/FREQ+REF_AREA?startPeriod=2001-01", new StructureReferenceImpl("ESTAT", "NA_MAIN", "1.3", SdmxStructureEnumType.Dataflow), new string[] { "FREQ", "REF_AREA" }, "2001-01", null, new List<ISet<string>>() { new HashSet<string>() { "A", "M" }, new HashSet<string>(), new HashSet<string>() { "K" } }, null);
                yield return new TestCaseData(RestAvailableConstraintQuery.ResourceName + "/ESTAT,NA_MAIN,1.3/A+M..K/ALL/FREQ+REF_AREA?startPeriod=2001-01&endPeriod=2011-03", new StructureReferenceImpl("ESTAT", "NA_MAIN", "1.3", SdmxStructureEnumType.Dataflow), new string[] { "FREQ", "REF_AREA" }, "2001-01", "2011-03", new List<ISet<string>>() { new HashSet<string>() { "A", "M" }, new HashSet<string>(), new HashSet<string>() { "K" } }, null);
                yield return new TestCaseData(RestAvailableConstraintQuery.ResourceName + "/ESTAT,NA_MAIN,1.3/A+M..K/ALL/FREQ+REF_AREA?startPeriod=2001-01&endPeriod=2011-03&references=codelist", new StructureReferenceImpl("ESTAT", "NA_MAIN", "1.3", SdmxStructureEnumType.Dataflow), new string[] { "FREQ", "REF_AREA" }, "2001-01", "2011-03", new List<ISet<string>>() { new HashSet<string>() { "A", "M" }, new HashSet<string>(), new HashSet<string>() { "K" } }, SdmxStructureEnumType.CodeList);
            }
        }

    }
}