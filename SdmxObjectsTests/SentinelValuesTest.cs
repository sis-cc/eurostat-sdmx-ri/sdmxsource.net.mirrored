// -----------------------------------------------------------------------
// <copyright file="SentinelValuesTest.cs" company="EUROSTAT">
//   Date Created : 2022-03-23
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjectsTests.
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxObjectsTests
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.TestUtils;

    [TestFixture]
    public class SentinelValuesTest
    {
        [Test]
        public void TestSentinelValues()
        {
            IDataStructureMutableObject dsd = SampleObjectsBuilder.BuildDataStructureMutableObject();
            dsd.AttributeList = new AttributeListMutableCore();
            IAttributeMutableObject unit = SampleObjectsBuilder.BuildAttributeMutableObject();

            IRepresentationMutableObject representationMutableObject = SampleObjectsBuilder.BuildMutableRepresentationObjectWithTextFormat();

            ISentinelValueMutableObject sentinelMutableObject = new SentinelValueMutableCore();
            sentinelMutableObject.Value = "-100";
            sentinelMutableObject.AddName("en", "Not applicable");

            ISentinelValueMutableObject sentinelMutableObject2 = new SentinelValueMutableCore();
            sentinelMutableObject2.Value = "test value";
            sentinelMutableObject2.AddName("en", "name en");
            sentinelMutableObject2.AddName("fr", "name fr");
            sentinelMutableObject2.AddDescription("it", "desc it");
            sentinelMutableObject2.AddDescription("el", "desc el");

            representationMutableObject.TextFormat.SentinelValues.Add(sentinelMutableObject);
            representationMutableObject.TextFormat.SentinelValues.Add(sentinelMutableObject2);

            unit.Representation = representationMutableObject;
            dsd.AddAttribute(unit);

            IDataStructureObject immutable = dsd.ImmutableInstance;
            IList<ISentinelValue> immutableSentinelValues = immutable.GetAttribute("UNIT").Representation.TextFormat.SentinelValues;
            Assert.AreEqual(2, immutableSentinelValues.Count);
            Assert.AreEqual("-100", immutableSentinelValues[0].Value);
            Assert.AreEqual("en", immutableSentinelValues[0].Names[0].Locale);
            Assert.AreEqual("Not applicable", immutableSentinelValues[0].Names[0].Value);
            Assert.AreEqual(0, immutableSentinelValues[0].Descriptions.Count);

            Assert.AreEqual("test value", immutableSentinelValues[1].Value);
            Assert.AreEqual("en", immutableSentinelValues[1].Names[0].Locale);
            Assert.AreEqual("name en", immutableSentinelValues[1].Names[0].Value);
            Assert.AreEqual("it", immutableSentinelValues[1].Descriptions[0].Locale);
            Assert.AreEqual("desc it", immutableSentinelValues[1].Descriptions[0].Value);
            Assert.AreEqual("fr", immutableSentinelValues[1].Names[1].Locale);
            Assert.AreEqual("name fr", immutableSentinelValues[1].Names[1].Value);
            Assert.AreEqual("el", immutableSentinelValues[1].Descriptions[1].Locale);
            Assert.AreEqual("desc el", immutableSentinelValues[1].Descriptions[1].Value);

            //also test from immutable to mutable
            IDataStructureMutableObject roundTripDsd = new DataStructureMutableCore(immutable);
            IList<ISentinelValueMutableObject> roundTripSentinelValues = roundTripDsd.GetAttribute("UNIT").Representation.TextFormat.SentinelValues;
            Assert.AreEqual(2, roundTripSentinelValues.Count);
            Assert.AreEqual("-100", roundTripSentinelValues[0].Value);
            Assert.AreEqual("en", roundTripSentinelValues[0].Names[0].Locale);
            Assert.AreEqual("Not applicable", roundTripSentinelValues[0].Names[0].Value);
            Assert.AreEqual(0, roundTripSentinelValues[0].Descriptions.Count);

            Assert.AreEqual("test value", roundTripSentinelValues[1].Value);
            Assert.AreEqual("en", roundTripSentinelValues[1].Names[0].Locale);
            Assert.AreEqual("name en", roundTripSentinelValues[1].Names[0].Value);
            Assert.AreEqual("it", roundTripSentinelValues[1].Descriptions[0].Locale);
            Assert.AreEqual("desc it", roundTripSentinelValues[1].Descriptions[0].Value);
            Assert.AreEqual("fr", roundTripSentinelValues[1].Names[1].Locale);
            Assert.AreEqual("name fr", roundTripSentinelValues[1].Names[1].Value);
            Assert.AreEqual("el", roundTripSentinelValues[1].Descriptions[1].Locale);
            Assert.AreEqual("desc el", roundTripSentinelValues[1].Descriptions[1].Value);
        }

        [Test]
        public void TestSentinelValuesNoValue()
        {
            IDataStructureMutableObject dsd = SampleObjectsBuilder.BuildDataStructureMutableObject();
            dsd.AttributeList = new AttributeListMutableCore();
            IAttributeMutableObject unit = SampleObjectsBuilder.BuildAttributeMutableObject();

            IRepresentationMutableObject representationMutableObject = SampleObjectsBuilder.BuildMutableRepresentationObjectWithTextFormat();

            ISentinelValueMutableObject sentinelMutableObject = new SentinelValueMutableCore();
            sentinelMutableObject.AddName("en", "test name");
            //we don't add a value so that it throws exception

            representationMutableObject.TextFormat.SentinelValues.Add(sentinelMutableObject);

            unit.Representation = representationMutableObject;
            dsd.AddAttribute(unit);

            Assert.Throws<SdmxException>(() => { var immutable = dsd.ImmutableInstance; });
        }

        [Test]
        public void TestSentinelValuesEmptyValue()
        {
            IDataStructureMutableObject dsd = SampleObjectsBuilder.BuildDataStructureMutableObject();
            dsd.AttributeList = new AttributeListMutableCore();
            IAttributeMutableObject unit = SampleObjectsBuilder.BuildAttributeMutableObject();

            IRepresentationMutableObject representationMutableObject = SampleObjectsBuilder.BuildMutableRepresentationObjectWithTextFormat();

            ISentinelValueMutableObject sentinelMutableObject = new SentinelValueMutableCore();
            sentinelMutableObject.Value = "";
            sentinelMutableObject.AddName("en", "test name");
            //we added empty value which is not allowed so that it throws exception

            representationMutableObject.TextFormat.SentinelValues.Add(sentinelMutableObject);

            unit.Representation = representationMutableObject;
            dsd.AddAttribute(unit);

            Assert.Throws<SdmxException>(() => { var immutable = dsd.ImmutableInstance; });
        }

        [Test]
        public void TestSentinelValuesNoName()
        {
            IDataStructureMutableObject dsd = SampleObjectsBuilder.BuildDataStructureMutableObject();
            dsd.AttributeList = new AttributeListMutableCore();
            IAttributeMutableObject unit = SampleObjectsBuilder.BuildAttributeMutableObject();

            IRepresentationMutableObject representationMutableObject = SampleObjectsBuilder.BuildMutableRepresentationObjectWithTextFormat();

            ISentinelValueMutableObject sentinelMutableObject = new SentinelValueMutableCore();
            sentinelMutableObject.Value = "-100";
            //we don't add a name so that it throws exception

            representationMutableObject.TextFormat.SentinelValues.Add(sentinelMutableObject);

            unit.Representation = representationMutableObject;
            dsd.AddAttribute(unit);

            Assert.Throws<SdmxException>(() => { var immutable = dsd.ImmutableInstance; });
        }


        [TestCaseSource(nameof(DeepEqualsArguments))]
        public void TestDeepEquals(ISentinelValueMutableObject sentinelMutableObject1, ISentinelValueMutableObject sentinelMutableObject2,
            bool includeFinalProperties, bool expectedResult)
        {
            IDataStructureMutableObject dsd = SampleObjectsBuilder.BuildDataStructureMutableObject();
            dsd.AttributeList = new AttributeListMutableCore();
            IAttributeMutableObject unit = SampleObjectsBuilder.BuildAttributeMutableObject();

            IRepresentationMutableObject representationMutableObject = SampleObjectsBuilder.BuildMutableRepresentationObjectWithTextFormat();

            representationMutableObject.TextFormat.SentinelValues.Add(sentinelMutableObject1);
            unit.Representation = representationMutableObject;
            dsd.AddAttribute(unit);

            IDataStructureObject immutable = dsd.ImmutableInstance;

            IDataStructureMutableObject dsd2 = SampleObjectsBuilder.BuildDataStructureMutableObject();
            dsd2.AttributeList = new AttributeListMutableCore();
            IAttributeMutableObject unit2 = SampleObjectsBuilder.BuildAttributeMutableObject();

            IRepresentationMutableObject representationMutableObject2 = SampleObjectsBuilder.BuildMutableRepresentationObjectWithTextFormat();

            representationMutableObject2.TextFormat.SentinelValues.Add(sentinelMutableObject2);
            unit2.Representation = representationMutableObject2;
            dsd2.AddAttribute(unit2);

            IDataStructureObject immutable2 = dsd2.ImmutableInstance;

            Assert.AreEqual(expectedResult, immutable.DeepEquals(immutable2, includeFinalProperties));
        }

        private static IEnumerable<object> DeepEqualsArguments()
        {
            return new object[]
            {
                //tests that include final properties when doing the comparison
                new object[] 
                {
                    CreateTestSentinelObject("commonValue", "en", "commonName", "it", "commondesc"),
                    CreateTestSentinelObject("commonValue", "en", "commonName", "it", "commondesc"),
                    true, true 
                },
                new object[] 
                {
                    CreateTestSentinelObject("val1", "en", "commonName", "it", "commondesc"),
                    CreateTestSentinelObject("val2", "en", "commonName", "it", "commondesc"),
                    true, false 
                },
                new object[] 
                {
                    CreateTestSentinelObject("commonValue", "en", "commonName", "it", "commondesc"),
                    CreateTestSentinelObject("commonValue", "fr", "commonName", "it", "commondesc"),
                    true, false
                },
                new object[] 
                {
                    CreateTestSentinelObject("commonValue", "en", "name 1", "it", "commondesc"),
                    CreateTestSentinelObject("commonValue", "en", "name 2", "it", "commondesc"),
                    true, false 
                },
                new object[] 
                {
                    CreateTestSentinelObject("commonValue", "en", "commonName", "en", "commondesc"),
                    CreateTestSentinelObject("commonValue", "en", "commonName", "fr", "commondesc"),
                    true, false 
                },
                new object[] 
                {
                    CreateTestSentinelObject("commonValue", "en", "commonName", "it", "desc 1"),
                    CreateTestSentinelObject("commonValue", "en", "commonName", "it", "desc 2"),
                    true, false 
                },

                //tests that DO NOT include final properties when doing the comparison
                new object[] 
                {
                    CreateTestSentinelObject("commonValue", "en", "commonName", "it", "commondesc"),
                    CreateTestSentinelObject("commonValue", "en", "commonName", "it", "commondesc"),
                    false, true 
                },
                new object[] 
                {
                    CreateTestSentinelObject("val1", "en", "commonName", "it", "commondesc"),
                    CreateTestSentinelObject("val2", "en", "commonName", "it", "commondesc"),
                    false, false 
                },
                new object[] 
                {
                    CreateTestSentinelObject("commonValue", "en", "commonName", "it", "commondesc"),
                    CreateTestSentinelObject("commonValue", "fr", "commonName", "it", "commondesc"),
                    false, true 
                },
                new object[] 
                {
                    CreateTestSentinelObject("commonValue", "en", "name 1", "it", "commondesc"),
                    CreateTestSentinelObject("commonValue", "en", "name 2", "it", "commondesc"),
                    false, true 
                },
                new object[] 
                {
                    CreateTestSentinelObject("commonValue", "en", "commonName", "en", "commondesc"),
                    CreateTestSentinelObject("commonValue", "en", "commonName", "fr", "commondesc"),
                    false, true 
                },
                new object[] 
                {
                    CreateTestSentinelObject("commonValue", "en", "commonName", "it", "desc 1"),
                    CreateTestSentinelObject("commonValue", "en", "commonName", "it", "desc 2"),
                    false, true 
                }
            };
        }

        private static ISentinelValueMutableObject CreateTestSentinelObject(String value, String nameLocale, String name, String descriptionLocale, String description)
        {

            ISentinelValueMutableObject sentinelMutableObject = new SentinelValueMutableCore();
            sentinelMutableObject.Value = value;
            sentinelMutableObject.AddName(nameLocale, name);
            sentinelMutableObject.AddDescription(descriptionLocale, description);

            return sentinelMutableObject;
        }
    }
}
