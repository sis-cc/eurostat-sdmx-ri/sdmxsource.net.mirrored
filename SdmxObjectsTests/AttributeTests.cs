// -----------------------------------------------------------------------
// <copyright file="AttributeTests.cs" company="EUROSTAT">
//   Date Created : 2022-02-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjectsTests.
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxObjectsTests
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.TestUtils;

    public class AttributeTests
    {
        [Test]
        public void TestAttributeNeitherUsageNorAssignmentStatusSet()
        {
            IDataStructureMutableObject dsd = SampleObjectsBuilder.BuildDataStructureMutableObject();
            dsd.AttributeList = new AttributeListMutableCore();
            IAttributeMutableObject attributeMutableObject = SampleObjectsBuilder.BuildAttributeMutableObject();
            dsd.AddAttribute(attributeMutableObject);
            IDataStructureObject immutable = dsd.ImmutableInstance;

            Assert.AreEqual(UsageType.Optional, immutable.GetAttribute(attributeMutableObject.Id).Usage);
        }

        [Test]
        [TestCaseSource(nameof(testAttributeUsageArguments))]
        public void TestAttributeUsage(UsageType usage, UsageType expectedUsage)
        {
            IDataStructureMutableObject dsd = SampleObjectsBuilder.BuildDataStructureMutableObject();
            dsd.AttributeList = new AttributeListMutableCore();
            IAttributeMutableObject attributeMutableObject = SampleObjectsBuilder.BuildAttributeMutableObject();
            attributeMutableObject.Usage = usage;
            dsd.AddAttribute(attributeMutableObject);
            IDataStructureObject immutable = dsd.ImmutableInstance;

            Assert.AreEqual(expectedUsage, immutable.GetAttribute(attributeMutableObject.Id).Usage);
        }

        [Test]
        [TestCaseSource(nameof(testAttributeAssignmentStatusArguments))]
        public void TestAttributeUsageViaAssignmentStatus(string assignmentStatus, UsageType expectedUsage)
        {
            IDataStructureMutableObject dsd = SampleObjectsBuilder.BuildDataStructureMutableObject();
            dsd.AttributeList = new AttributeListMutableCore();
            IAttributeMutableObject attributeMutableObject = SampleObjectsBuilder.BuildAttributeMutableObject();
            attributeMutableObject.AssignmentStatus = assignmentStatus;
            dsd.AddAttribute(attributeMutableObject);
            IDataStructureObject immutable = dsd.ImmutableInstance;

            Assert.AreEqual(expectedUsage, immutable.GetAttribute(attributeMutableObject.Id).Usage);
        }

        [Test]
        public void TestInvalidAssignmentStatus(
            [Values("MANDATORY", "mandatory", "CONDITIONAL", "conditional", "SLDKFJLSDFJ")]
            string assignmentStatus)
        {
            IDataStructureMutableObject dsd = SampleObjectsBuilder.BuildDataStructureMutableObject();
            dsd.AttributeList = new AttributeListMutableCore();
            IAttributeMutableObject attributeMutableObject = SampleObjectsBuilder.BuildAttributeMutableObject();
            Assert.Throws<SdmxSemmanticException>(() => attributeMutableObject.AssignmentStatus = assignmentStatus);
        }

        [Test]
        [TestCaseSource(nameof(deepEqualsArguments))]
        public void TestDeepEquals(UsageType usage1, UsageType usage2, bool expectedResult)
        {
            IDataStructureMutableObject dsd1 = SampleObjectsBuilder.BuildDataStructureMutableObject();
            dsd1.AttributeList = new AttributeListMutableCore();
            IAttributeMutableObject attributeMutableObject1 = SampleObjectsBuilder.BuildAttributeMutableObject();
            attributeMutableObject1.Usage = usage1;
            dsd1.AddAttribute(attributeMutableObject1);
            IDataStructureObject immutable = dsd1.ImmutableInstance;

            IDataStructureMutableObject dsd2 = SampleObjectsBuilder.BuildDataStructureMutableObject();
            dsd2.AttributeList = new AttributeListMutableCore();
            IAttributeMutableObject attributeMutableObject2 = SampleObjectsBuilder.BuildAttributeMutableObject();
            attributeMutableObject2.Usage = usage2;
            dsd2.AddAttribute(attributeMutableObject2);
            IDataStructureObject immutable2 = dsd2.ImmutableInstance;

            Assert.AreEqual(expectedResult, immutable.DeepEquals(immutable2, true));
        }

        //provided UsageType, expected UsageType
        private static readonly object[] testAttributeUsageArguments =
        {
             new object[] { UsageType.Mandatory, UsageType.Mandatory },
             new object[] { UsageType.Optional, UsageType.Optional },
             new object[] { null, UsageType.Optional }
        };

        //provided assignmentStatus, expected UsageType
        private static readonly object[] testAttributeAssignmentStatusArguments =
        {
            new object[] { null, UsageType.Optional },
            new object[] { "Mandatory", UsageType.Mandatory },
            new object[] { "Conditional", UsageType.Optional }
        };

        private static readonly object[] deepEqualsArguments =
        {
            new object[] { UsageType.Mandatory, UsageType.Mandatory, true },
            new object[] { UsageType.Optional, UsageType.Optional, true },
            new object[] { UsageType.Optional, UsageType.Mandatory, false },
            new object[] { UsageType.Mandatory, UsageType.Optional, false }
        };
    }
}
