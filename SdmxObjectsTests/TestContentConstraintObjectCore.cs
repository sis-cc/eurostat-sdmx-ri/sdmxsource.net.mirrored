// -----------------------------------------------------------------------
// <copyright file="TestContentConstraintObjectCore.cs" company="EUROSTAT">
//   Date Created : 2021-04-22
//   Copyright (c) 2012, 2021 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjectsTests.
// 
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry;

namespace SdmxObjectsTests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Util.Extension;

    [TestFixture]
    public class TestContentConstraintObjectCore
    {

        [Test]
        public void TestContentConstraintObjectCoreStartAndEndDateChanges()
        {
            var immutableConstraint = GetConstraints().FirstOrDefault() as ContentConstraintObjectCore;
            
            Assert.IsNotNull(immutableConstraint);

            var baseDateTime = DateTime.Now;

            // Same date should be OK for start and end
            immutableConstraint.StartDate = null;
            immutableConstraint.EndDate = null;
            Assert.DoesNotThrow(() => immutableConstraint.StartDate = new SdmxDateCore(baseDateTime, TimeFormatEnumType.DateTime));
            Assert.DoesNotThrow(() => immutableConstraint.EndDate = new SdmxDateCore(baseDateTime, TimeFormatEnumType.DateTime));

            // End date is later than start date
            immutableConstraint.StartDate = null;
            immutableConstraint.EndDate = null;
            Assert.DoesNotThrow(() => immutableConstraint.StartDate = new SdmxDateCore(baseDateTime.AddSeconds(-1), TimeFormatEnumType.DateTime));
            Assert.DoesNotThrow(() => immutableConstraint.EndDate = new SdmxDateCore(baseDateTime, TimeFormatEnumType.DateTime));

            // End date is earlier than start date
            immutableConstraint.StartDate = null;
            immutableConstraint.EndDate = null;
            Assert.DoesNotThrow(() => immutableConstraint.StartDate = new SdmxDateCore(baseDateTime, TimeFormatEnumType.DateTime));
            Assert.Throws<SdmxSemmanticException>(() => immutableConstraint.EndDate = new SdmxDateCore(baseDateTime.AddSeconds(-1), TimeFormatEnumType.DateTime));

            // Start date is later than end date
            immutableConstraint.StartDate = null;
            immutableConstraint.EndDate = null;
            Assert.DoesNotThrow(() => immutableConstraint.EndDate = new SdmxDateCore(baseDateTime, TimeFormatEnumType.DateTime));
            Assert.Throws<SdmxSemmanticException>(() => immutableConstraint.StartDate = new SdmxDateCore(baseDateTime.AddSeconds(1), TimeFormatEnumType.DateTime));
        }
        [Test]
        public void TestContentConstraintWithDuplicteKeyValues()
        {
            var immutableConstraint = GetConstraintsDuplicates().FirstOrDefault(x => string.Equals(x.Id, "CR_DSD_SNA@DF_SNA_TABLE4", StringComparison.Ordinal));
            
            Assert.IsNotNull(immutableConstraint);
            ICubeRegion cubeRegion = immutableConstraint.IncludedCubeRegion;
            var refSector = cubeRegion.KeyValues.Where(x => "REF_SECTOR".Equals(x.Id, StringComparison.Ordinal)).ToArray();
            Assert.IsNotNull(refSector);
            Assert.That(refSector.Length, Is.EqualTo(1),string.Join(", ", refSector.Select(x => x.Id)));
            Assert.That(refSector[0].Values.Count, Is.EqualTo(1));
            var cubeRegionCore = immutableConstraint.IncludedCubeRegion as CubeRegionCore;
            Assert.That(cubeRegionCore.Warnings.Count, Is.EqualTo(3));
            Assert.That(cubeRegionCore.Warnings[0], Is.EqualTo("Duplicate cube value PPP_B1GQ for component id MEASURE"));
            Assert.That(cubeRegionCore.Warnings[1], Is.EqualTo("Duplicate entry for component REF_SECTOR"));
            Assert.That(cubeRegionCore.Warnings[2], Is.EqualTo("Duplicate entry for component COUNTERPART_SECTOR"));
        }


        private static IList<IContentConstraintObject> GetConstraints()
        {
            IStructureParsingManager parsingManager = new StructureParsingManager();
            DirectoryInfo testDir = new DirectoryInfo("tests/v21/structure");
            if (!testDir.Exists)
            {
                Trace.WriteLine(string.Format("Test directory '{0}' not found ", testDir.FullName));
            }

            var files = testDir.GetFiles ("CNS_PR+UN+1.1-actual.xml");
            List<IContentConstraintObject> contentConstraintObjects = new List<IContentConstraintObject>();
            foreach (var file in files)
            {
                ISdmxObjects sdmxObjects;
                try
                {
                    sdmxObjects = file.GetSdmxObjects(parsingManager);
                }
                catch (Exception e)
                {
                    Trace.WriteLine(e.ToString());
                    continue;
                }

                contentConstraintObjects.AddRange(sdmxObjects.GetContentConstraintObjects("TEST"));
            }

            return contentConstraintObjects;
        }
        private static ISet<IContentConstraintObject> GetConstraintsDuplicates()
        {
            IStructureParsingManager parsingManager = new StructureParsingManager();
            DirectoryInfo testDir = new DirectoryInfo("tests/v21/structure");
            if (!testDir.Exists)
            {
                Trace.WriteLine(string.Format("Test directory '{0}' not found ", testDir.FullName));
            }
            return new FileInfo(Path.Combine(testDir.FullName, "NAD_TEST_Multiple.xml")).GetSdmxObjects(parsingManager).ContentConstraintObjects;
        }
    }
}