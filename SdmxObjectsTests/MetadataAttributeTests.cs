// -----------------------------------------------------------------------
// <copyright file="MetadataAttributeTests.cs" company="EUROSTAT">
//   Date Created : 2022-02-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjectsTests.
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxObjectsTests
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.TestUtils;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    [TestFixture]
    public class MetadataAttributeTests
    {
        [TestCaseSource(nameof(DeepEqualsArguments))]
        public void TestDeepEquals(
            string attributeId1, AttributeAttachmentLevel attachmentLevel1, string attachmentGroup1, List<string> dimensionReferences1,
            string attributeId2, AttributeAttachmentLevel attachmentLevel2, string attachmentGroup2, List<string> dimensionReferences2,
            bool expectedResult)
        {
            IMetadataAttributeUsageMutableObject attr1 = new MetadataAttributeUsageMutableCore()
            {
                MetadataAttributeReference = attributeId1,
                AttachmentLevel = attachmentLevel1,
                AttachmentGroup = attachmentGroup1,
                DimensionReferences = dimensionReferences1.AsReadOnly()
            };
            IDataStructureMutableObject dsd1 = CreateDataStructure(attr1);

            IMetadataAttributeUsageMutableObject attr2 = new MetadataAttributeUsageMutableCore()
            {
                MetadataAttributeReference = attributeId2,
                AttachmentLevel = attachmentLevel2,
                AttachmentGroup = attachmentGroup2,
                DimensionReferences = dimensionReferences2.AsReadOnly()
            };
            IDataStructureMutableObject dsd2 = CreateDataStructure(attr2);

            IDataStructureObject immutable1 = dsd1.ImmutableInstance;
            IDataStructureObject immutable2 = dsd2.ImmutableInstance;

            Assert.AreEqual(expectedResult, immutable1.DeepEquals(immutable2, true));
        }

        [Test]
        public void TestMutableImmutable()
        {
            IMetadataAttributeUsageMutableObject attr = new MetadataAttributeUsageMutableCore()
            {
                MetadataAttributeReference = "CONTACT",
                AttachmentLevel = AttributeAttachmentLevel.DimensionGroup,
                AttachmentGroup = "some group",
                DimensionReferences = new List<string> { "FREQ" }.AsReadOnly()
            };
            IDataStructureMutableObject dsd = CreateDataStructure(attr);
            IDataStructureObject immutable = dsd.ImmutableInstance;

            Assert.That(dsd.AttributeList.MetadataAttributes, Has.Count.EqualTo(1));
            var immutableAttribute = immutable.AttributeList.MetadataAttributes[0];
            Assert.AreEqual(attr.MetadataAttributeReference, immutableAttribute.MetadataAttributeReference);
            Assert.AreEqual(attr.AttachmentLevel, immutableAttribute.AttachmentLevel);
            Assert.AreEqual(attr.AttachmentGroup, immutableAttribute.AttachmentGroup);
            Assert.AreEqual(attr.DimensionReferences.Count, immutableAttribute.DimensionReferences.Count);
            Assert.AreEqual(attr.DimensionReferences[0], immutableAttribute.DimensionReferences[0]);
        }

        private IDataStructureMutableObject CreateDataStructure(IMetadataAttributeUsageMutableObject metadataAttribute)
        {
            IDataStructureMutableObject dsd = SampleObjectsBuilder.BuildDataStructureMutableObject();
            dsd.MetadataStructure = new StructureReferenceImpl("EXAMPLE", "TEST_MSD", "1.0.0", SdmxStructureEnumType.Msd);
            dsd.AttributeList = new AttributeListMutableCore();
            dsd.AttributeList.MetadataAttributes.Add(metadataAttribute);
            return dsd;
        }

        private static IEnumerable<object> DeepEqualsArguments()
        {
            return new object[] {
                new object[] 
                { 
                    "CONTACT", AttributeAttachmentLevel.DimensionGroup, "some group", new List<string> { "FREQ" },
                    "CONTACT", AttributeAttachmentLevel.DimensionGroup, "some group", new List<string> { "FREQ" },
                    true 
                },
                new object[]
                {
                    "CONTACT", AttributeAttachmentLevel.DimensionGroup, "some group", new List<string> { "FREQ" },
                    "CONTACT", AttributeAttachmentLevel.DimensionGroup, "some group", new List<string> { "VALUE" },
                    false
                },
                new object[]
                {
                    "CONTACT", AttributeAttachmentLevel.DimensionGroup, "some group", new List<string> { "FREQ", "UNIT" },
                    "CONTACT", AttributeAttachmentLevel.DimensionGroup, "some group", new List<string> { "FREQ" },
                    false
                },
                new object[]
                {
                    "CONTACT", AttributeAttachmentLevel.DimensionGroup, "some group", new List<string> { "FREQ" },
                    "CONTACT", AttributeAttachmentLevel.DimensionGroup, "some other group", new List<string> { "FREQ" },
                    false
                },
                new object[]
                {
                    "CONTACT", AttributeAttachmentLevel.DimensionGroup, "some group", new List<string> { "FREQ" },
                    "CONTACT", AttributeAttachmentLevel.Observation, "some group", new List<string> { "FREQ" },
                    false
                },
                new object[]
                {
                    "CONTACT", AttributeAttachmentLevel.DimensionGroup, "some group", new List<string> { "FREQ" },
                    "CONTACT_ME", AttributeAttachmentLevel.DimensionGroup, "some group", new List<string> { "FREQ" },
                    false
                }
            };
        }
    }
}
