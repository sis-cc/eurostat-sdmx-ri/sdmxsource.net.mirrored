// -----------------------------------------------------------------------
// <copyright file="RegistryInterfaceWriterV2.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureMutableParser.
//     SdmxStructureMutableParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureMutableParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureMutableParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.SdmxStructureMutableParser.Engine.V2
{
    using System;
    using System.IO;
    using System.Text;
    using System.Xml;

    using Estat.Sri.SdmxParseBase.Model;
    using Estat.Sri.SdmxStructureMutableParser.Model;
    using Estat.Sri.SdmxStructureMutableParser.Properties;
    using Estat.Sri.SdmxXmlConstants;

    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    ///     This class writes a SDMX Model IMutableObjects object to a stream
    /// </summary>
    public class RegistryInterfaceWriterV2 : RegistryInterfaceWriterBaseV2, IStructureWriterEngine
    {
        /// <summary>
        ///     The _close XML writer
        /// </summary>
        private readonly bool _closeXmlWriter;

        /// <summary>
        ///     The internal filed containing the SDMX Model structure object
        /// </summary>
        private IRegistryInfo _registryInterface;

        /// <summary>
        /// Initializes a new instance of the <see cref="RegistryInterfaceWriterV2" /> class
        /// </summary>
        /// <param name="writer">The output stream to actually perform the writing</param>
        /// <param name="encoding">The encoding.</param>
        /// <param name="prettify">if set to <c>true</c> [prettify].</param>
        /// <exception cref="System.ArgumentNullException">writer is null</exception>
        /// <exception cref="System.ArgumentException">writer</exception>
        /// <exception cref="ArgumentException">Cannot write to stream.</exception>
        public RegistryInterfaceWriterV2(Stream writer, Encoding encoding, bool prettify)
            : this(CreateXmlWriter(writer, encoding, prettify))
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            if (!writer.CanWrite)
            {
                throw new ArgumentException(Resources.ExcepteptionCannotWriteToStream, "writer");
            }

            this._closeXmlWriter = true;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RegistryInterfaceWriterV2" /> class.
        /// </summary>
        /// <param name="writer">
        ///     The writer.
        /// </param>
        public RegistryInterfaceWriterV2(XmlWriter writer)
            : this(writer, null)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RegistryInterfaceWriterV2" /> class.
        /// </summary>
        /// <param name="writer">
        ///     The writer.
        /// </param>
        /// <param name="namespaces">
        ///     The namespace.
        /// </param>
        public RegistryInterfaceWriterV2(XmlWriter writer, SdmxNamespaces namespaces)
            : base(writer, namespaces)
        {
            this.HeaderRetrievalManager = new HeaderRetrievalManager();
        }

        /// <summary>
        ///     This is the main method of the class and is used to write the <see cref="IRegistryInfo" />
        ///     to the <see cref="System.Xml.XmlTextWriter" /> given in the constructor
        /// </summary>
        /// <param name="registryInterface">
        ///     The <see cref="IRegistryInfo" /> object we want to write
        /// </param>
        public void WriteRegistryInterface(IRegistryInfo registryInterface)
        {
            if (registryInterface == null)
            {
                throw new ArgumentNullException("registryInterface");
            }

            this._registryInterface = registryInterface;

            this.WriteMessageTag(ElementNameTable.RegistryInterface);

            // print message header
            this.WriteMessageHeader(this._registryInterface.Header);
            if (ToolIndicator != null)
            {
                this.SdmxMLWriter.WriteComment(ToolIndicator.GetComment());
            }

            // write QueryStructureRequest
            if (this._registryInterface.QueryStructureRequest != null)
            {
                var writer = new QueryStructureRequestWriterV2(this.SdmxMLWriter, this.Namespaces);
                writer.ToolIndicator = ToolIndicator;
                writer.Write(registryInterface);
            }

            // write QueryStructureResponse
            if (this._registryInterface.QueryStructureResponse != null)
            {
                var writer = new QueryStructureResponseWriterV2(this.SdmxMLWriter, this.Namespaces);
                writer.ComponentRoleBuilder = this.ComponentRoleBuilder;
                writer.ToolIndicator = ToolIndicator;
                writer.Write(registryInterface);
            }

            // close document
            this.WriteEndElement();
            this.WriteEndDocument();
            if (this._closeXmlWriter)
            {
                this.SdmxMLWriter.Close();
            }
        }

        /// <summary>
        ///     Writes the <paramref name="maintainableObject" /> out to the output location in the format specified by the
        ///     implementation
        /// </summary>
        /// <param name="maintainableObject">
        ///     The maintainableObject.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="maintainableObject"/> is <see langword="null" />.</exception>
        public void WriteStructure(IMaintainableObject maintainableObject)
        {
            if (maintainableObject == null)
            {
                throw new ArgumentNullException("maintainableObject");
            }

            IMutableObjects mutableObjects = new MutableObjectsImpl();
            mutableObjects.AddIdentifiable(maintainableObject.MutableInstance);
            IRegistryInfo registry = new RegistryInfo();
            registry.QueryStructureResponse = new QueryStructureResponseInfo { Structure = mutableObjects, StatusMessage = new StatusMessageInfo { Status = Status.Success } };
            this.WriteRegistryInterface(registry);
        }

        /// <summary>
        ///     Writes the sdmxObjects to the output location in the format specified by the implementation
        /// </summary>
        /// <param name="sdmxObjects">
        ///     SDMX objects
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="sdmxObjects"/> is <see langword="null" />.</exception>
        public void WriteStructures(ISdmxObjects sdmxObjects)
        {
            if (sdmxObjects == null)
            {
                throw new ArgumentNullException("sdmxObjects");
            }

            IRegistryInfo registry = new RegistryInfo();
            registry.QueryStructureResponse = new QueryStructureResponseInfo
                                                  {
                                                      Structure = sdmxObjects.MutableObjects, 
                                                      StatusMessage =
                                                          new StatusMessageInfo
                                                              {
                                                                  Status =
                                                                      Status
                                                                      .Success
                                                              }
                                                  };
            this.WriteRegistryInterface(registry);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="managed"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected override void Dispose(bool managed)
        {
            if (this._closeXmlWriter)
            {
                this.SdmxMLWriter.Close();
            }
        }
    }
}