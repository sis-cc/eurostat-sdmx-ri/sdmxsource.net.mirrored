﻿// -----------------------------------------------------------------------
// <copyright file="SdmxStructureWriterV2Factory.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureMutableParser.
//     SdmxStructureMutableParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureMutableParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureMutableParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.SdmxStructureMutableParser.Factory
{
    using System;
    using System.IO;

    using Estat.Sri.SdmxStructureMutableParser.Engine.V2;
    using Estat.Sri.SdmxStructureMutableParser.Model;
    using Estat.Sri.SdmxStructureMutableParser.Properties;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects;

    /// <summary>
    ///     The <c>SDMX</c> Structure writer engine factory that uses <see cref="StructureWriterV2" /> and
    ///     <see cref="RegistryInterfaceWriterV2" /> for SDMX v2.0
    /// </summary>
    public class SdmxStructureWriterV2Factory : IStructureWriterFactory
    {
        /// <summary>
        /// The _component role builder
        /// </summary>
        private IBuilder<ComponentRole, IStructureReference> _componentRoleBuilder = new Sdmxv2ConceptRoleBuilder();

        /// <summary>
        /// Gets or sets the component role builder.
        /// </summary>
        /// <value>
        /// The component role builder.
        /// </value>
        public IBuilder<ComponentRole, IStructureReference> ComponentRoleBuilder
        {
            get
            {
                return this._componentRoleBuilder;
            }

            set
            {
                this._componentRoleBuilder = value;
            }
        }

        /// <summary>
        ///     Obtains a StructureWritingEngine engine for the given output format
        /// </summary>
        /// <param name="structureFormat">
        ///     An implementation of the StructureFormat to describe the output format for the structures (required)
        /// </param>
        /// <param name="streamWriter">
        ///     The output stream to write to (can be null if it is not required)
        /// </param>
        /// <returns>
        ///     Null if this factory is not capable of creating a data writer engine in the requested format
        /// </returns>
        /// <exception cref="ArgumentException">Cannot write to stream.</exception>
        /// <exception cref="ArgumentNullException"><paramref name="structureFormat"/> is <see langword="null" />.</exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "False warning. Method returns IDisposable")]
        public IStructureWriterEngine GetStructureWriterEngine(IStructureFormat structureFormat, Stream streamWriter)
        {
            if (structureFormat == null)
            {
                throw new ArgumentNullException("structureFormat");
            }

            if (streamWriter != null && !streamWriter.CanWrite)
            {
                throw new ArgumentException(Resources.ExcepteptionCannotWriteToStream, "streamWriter");
            }

            if (structureFormat.SdmxOutputFormat == null)
            {
                return null;
            }

            if (structureFormat.SdmxOutputFormat.OutputVersion.EnumType != SdmxSchemaEnumType.VersionTwo)
            {
                return null;
            }

            var outputFormat = structureFormat.SdmxOutputFormat;
            SdmxStructureV2Format sdmxStructureV2Format = structureFormat as SdmxStructureV2Format;
            if (sdmxStructureV2Format != null)
            {
                if (!outputFormat.IsQueryResponse && !outputFormat.IsRegistryDocument)
                {
                    return new StructureWriterV2(streamWriter, sdmxStructureV2Format.OutputEncoding, false) { ComponentRoleBuilder = this._componentRoleBuilder };
                }

                if (outputFormat.EnumType == StructureOutputFormatEnumType.SdmxV2RegistryQueryResponseDocument)
                {
                    return new RegistryInterfaceWriterV2(streamWriter, sdmxStructureV2Format.OutputEncoding, false) { ComponentRoleBuilder = this._componentRoleBuilder };
                }
            }

            SdmxXmlStructureV2Format sdmxXmlStructureV2Format = structureFormat as SdmxXmlStructureV2Format;
            if (sdmxXmlStructureV2Format != null)
            {
                if (!outputFormat.IsQueryResponse && !outputFormat.IsRegistryDocument)
                {
                    return new StructureWriterV2(sdmxXmlStructureV2Format.Writer) { ComponentRoleBuilder = this._componentRoleBuilder };
                }

                if (outputFormat.EnumType == StructureOutputFormatEnumType.SdmxV2RegistryQueryResponseDocument)
                {
                    return new RegistryInterfaceWriterV2(sdmxXmlStructureV2Format.Writer) { ComponentRoleBuilder = this._componentRoleBuilder };
                }
            }

            return null;
        }
    }
}