// -----------------------------------------------------------------------
// <copyright file="CommonParsingEngine.cs" company="EUROSTAT">
//   Date Created : 2022-01-18
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Parsing.SdmxMLV3
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    public abstract class CommonParsingEngine
    {
        public delegate bool XmlParsingFunction<T>(T obj, XmlReader reader);

        public delegate void XmlParsingConsumer<T>(T obj, XmlReader reader);

        public static IDictionary<string, XmlParsingConsumer<T>> BuildConsumerMap<T>()
        {
            return new Dictionary<string, XmlParsingConsumer<T>>();
        }

        /// <summary>
        /// Get the string value of an element.
        /// Is meant to be used instead of <see cref="XmlReader.ReadElementContentAsString()"/>
        /// </summary>
        /// <param name="reader">The xml reader.</param>
        /// <returns></returns>
        internal static string ParseValue(XmlReader reader)
        {
            string value = null;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Text)
                {
                    value = reader.Value;
                    break;
                }
            }
            return value;
        }

        protected IStructureReference ParseURN(XmlReader reader)
        {
            string value = ParseValue(reader);
            return new StructureReferenceImpl(value);
        }

        protected void ParseTextType(XmlReader reader, Action<string, string> consumer)
        {
            string lang = reader.GetAttribute("lang", SdmxConstants.Xmlns);
            if (!ObjectUtil.ValidString(lang))
            {
                lang = "en";
            }
            string value = ParseValue(reader);
            consumer.Invoke(lang, value);
        }

        protected void ParseNext<T>(XmlReader reader, T obj, Dictionary<string, XmlParsingConsumer<T>> startElementConsumers)
        {
            // it expects to be at StartElement
            string endElement = reader.LocalName;
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        XmlParsingConsumer<T> consumer = startElementConsumers[reader.LocalName];
                        if (consumer != null)
                        {
                            consumer.Invoke(obj, reader);
                        }
                        break;
                    case XmlNodeType.EndElement:
                        // go until end element
                        if (endElement.Equals(reader.LocalName))
                        {
                            return;
                        }
                        break;
                }
            }
        }

        protected void ParseNext<T>(XmlReader reader, T obj, XmlParsingFunction<T> startFunction, XmlParsingFunction<T> endFunction)
            where T : IMutableObject
        {
            // This is in case of an element without separate end tag (like <str:TextFormat textType="ObservationalTimePeriod" />)
            if (reader.IsEmptyElement)
            {
                return;
            }
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if (!startFunction.Invoke(obj, reader))
                        {
                            return;
                        }
                        break;
                    case XmlNodeType.EndElement:
                        if (!endFunction.Invoke(obj, reader))
                        {
                            return;
                        }
                        break;
                }
            }
        }

        protected void ParseNext<T>(XmlReader reader, T bean, XmlParsingFunction<T> startFunction)
            where T : IMutableObject
        {
            string endElement = reader.LocalName;
            ParseNext(reader, bean, startFunction, (bean1, reader1) => !reader1.LocalName.Equals(endElement));
        }

        protected void ParseNext<T>(XmlReader reader, T bean, XmlParsingFunction<T> startFunction, string endElement)
            where T : IMutableObject
        {
            ParseNext(reader, bean, startFunction, (bean1, reader1) => !reader1.LocalName.Equals(endElement));
        }

        protected T ParseAttributeValue<T>(XmlReader reader, string attributeName, Predicate<string> attributeValidator, Func<string, T> valueConverter)
        {
            string value = reader.GetAttribute(attributeName);
            if (value != null && attributeValidator.Invoke(value))
            {
                return valueConverter.Invoke(value);
            }

            return default;
        }

        protected IRepresentationMutableObject ParseRepresentation(XmlReader reader, string endTag)
        {
            IRepresentationMutableObject representation = new RepresentationMutableCore();
            string unboundedLiteral = UnboundedOccurenceObjectCore.Instance.ToString();

            IOccurenceObject maxOccurs = ParseAttributeValue(reader, "maxOccurs",
                    s => ObjectUtil.ValidString(s) && (s.Equals(unboundedLiteral) || ObjectUtil.ValidInt(s)),
                        s =>
                        {
                            if (s.Equals(unboundedLiteral))
                            {
                                return UnboundedOccurenceObjectCore.Instance;
                            }
                            else
                            {
                                return new FiniteOccurenceObjectCore(int.Parse(s));
                            }
                        });
            if (maxOccurs != null)
            {
                representation.MaxOccurs = maxOccurs;
            }

            int? minOccurs = ParseIntAttributeValue(reader, "minOccurs");
            if (minOccurs.HasValue)
            {
                representation.MinOccurs = minOccurs.Value;
            }

            ParseNext(reader, representation, this.ParseRepresentation, endTag);
            return representation;
        }

        private ITextFormatMutableObject ParseTextFormat(XmlReader reader, string endTag)
        {
            ITextFormatMutableObject obj = new TextFormatMutableCore();

            ParseDataTypeAttributeValue(reader, "textType").IfNotNull(v => obj.TextType = v);
            ParseTertiaryBooleanAttributeValue(reader, "isSequence").IfNotNull(v => obj.Sequence = v);
            ParseDecimalAttributeValue(reader, "interval").IfNotNull(v => obj.Interval = v);
            ParseDecimalAttributeValue(reader, "startValue").IfNotNull(v => obj.StartValue = v);
            ParseDecimalAttributeValue(reader, "endValue").IfNotNull(v => obj.EndValue = v);
            ParseStringAttributeValue(reader, "timeInterval").IfNotNull(v => obj.TimeInterval = v);
            ParseStandardTimePeriodTypeAttributeValue(reader, "startTime").IfNotNull(v => obj.StartTime = v);
            ParseStandardTimePeriodTypeAttributeValue(reader, "endTime").IfNotNull(v => obj.EndTime = v);
            ParsePositiveIntegerAttributeValue(reader, "minLength").IfNotNull(v => obj.MinLength = v);
            ParsePositiveIntegerAttributeValue(reader, "maxLength").IfNotNull(v => obj.MaxLength = v);
            ParseDecimalAttributeValue(reader, "minValue").IfNotNull(v => obj.MinValue = v);
            ParseDecimalAttributeValue(reader, "maxValue").IfNotNull(v => obj.MaxValue = v);
            ParsePositiveIntegerAttributeValue(reader, "decimals").IfNotNull(v => obj.Decimals = v);
            ParseStringAttributeValue(reader, "pattern").IfNotNull(v => obj.Pattern = v);
            ParseTertiaryBooleanAttributeValue(reader, "isMultiLingual").IfNotNull(v => obj.Multilingual = v);

            ParseNext(reader, obj, this.ParseSentinel, endTag);

            return obj;
        }

        protected string ParseStringAttributeValue(XmlReader reader, string attributeName)
        {
            return ParseAttributeValue(reader, attributeName,v =>  ObjectUtil.ValidString(v), s => s);
        }

        protected int? ParseIntAttributeValue(XmlReader reader, string attributeName)
        {
            return ParseAttributeValue(reader, attributeName, ObjectUtil.ValidInt, v => v.ParseToNullableStruct(int.Parse));
        }

        protected long? ParsePositiveIntegerAttributeValue(XmlReader reader, string attributeName)
        {
            return ParseAttributeValue(reader, attributeName, ObjectUtil.ValidInt, v => v.ParseToNullableStruct(long.Parse));
        }

        protected decimal? ParseDecimalAttributeValue(XmlReader reader, string attributeName)
        {
            return ParseAttributeValue(reader, attributeName, ObjectUtil.ValidDecimal, v => v.ParseToNullableStruct(decimal.Parse));
        }

        protected ISdmxDate ParseStandardTimePeriodTypeAttributeValue(XmlReader reader, string attributeName)
        {
            // validation should catch any errors
            return ParseAttributeValue(reader, attributeName, v => ObjectUtil.ValidString(v), v => new SdmxDateCore(v));
        }

        protected TertiaryBool ParseTertiaryBooleanAttributeValue(XmlReader reader, string attributeName)
        {
            return ParseAttributeValue(reader, attributeName, BooleanConverter.IsBoolean, b => TertiaryBool.ParseBoolean(XmlConvert.ToBoolean(b)));
        }

        protected bool? ParseBooleanAttributeValue(XmlReader reader, string attributeName)
        {
            return ParseAttributeValue(reader, attributeName, BooleanConverter.IsBoolean, XmlConvert.ToBoolean);
        }

        protected TextType ParseDataTypeAttributeValue(XmlReader reader, string attributeName)
        {
            return ParseAttributeValue(reader, attributeName, v => ObjectUtil.ValidString(v), TextTypeBuilderv3.Build);
        }

        protected CascadeSelection? ParseCascadeSelectionAttributeValue(XmlReader reader, string attributeName)
        {
            return ParseAttributeValue(reader, attributeName, v => ObjectUtil.ValidString(v), TextTypeBuilderv3.BuildCascadeSelection);
        }

        private bool ParseSentinel(ITextFormatMutableObject mutableObj, XmlReader reader)
        {
            if (reader.LocalName.Equals("SentinelValue"))
            {
                ISentinelValueMutableObject sentinel = new SentinelValueMutableCore();
                mutableObj.SentinelValues.Add(sentinel);
                ParseStringAttributeValue(reader, "value").IfNotNull(v => sentinel.Value = v);
                Dictionary<string, XmlParsingConsumer<ISentinelValueMutableObject>> consumerMap = new Dictionary<string, XmlParsingConsumer<ISentinelValueMutableObject>>();
                consumerMap.Put("Name", (obj, r) => ParseTextType(r, obj.AddName));
                consumerMap.Put("Description", (obj, r) => ParseTextType(r, obj.AddDescription));
                ParseNext(reader, sentinel, consumerMap);
            }

            return true;
        }

        private bool ParseRepresentation(IRepresentationMutableObject obj, XmlReader reader)
        {
            switch (reader.LocalName)
            {
                case "TextFormat":
                case "EnumerationFormat":
                    obj.TextFormat = ParseTextFormat(reader, reader.LocalName);
                    break;
                case "Enumeration":
                    obj.Representation = ParseURN(reader);
                    break;
            }
            return true;
        }
    }
}