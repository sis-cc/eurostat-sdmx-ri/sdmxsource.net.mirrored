// -----------------------------------------------------------------------
// <copyright file="ItemSchemeParsingEngine.cs" company="EUROSTAT">
//   Date Created : 2022-01-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Parsing.SdmxMLV3
{
    using System.Collections.Generic;
    using System.Xml;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Util.Extensions;

    public abstract class ItemSchemeParsingEngine<T, Y> : MaintainableParsingEngine<T>
        where T : IItemSchemeMutableObject<Y>
        where Y : IItemMutableObject
    {
        protected override IMaintainableParserConfig<T> GetMaintainableParserConfig()
        {
            return new MaintainableConfig(GetItemSchemeParserConfig(), this);
        }

        private class MaintainableConfig : IMaintainableParserConfig<T>
        {
            private readonly IItemSchemeParserConfig<T, Y> _config;
            private readonly ItemSchemeParsingEngine<T, Y> _engine;

            public MaintainableConfig(IItemSchemeParserConfig<T, Y> itemSchemeParserConfig, ItemSchemeParsingEngine<T, Y> engine)
            {
                this._config = itemSchemeParserConfig;
                this._engine = engine;
            }

            public string GetMaintainableTag()
            {
                return _config.GetSchemeTag();
            }

            public T BuildMaintainableArtefact()
            {
                return _config.BuildScheme();
            }

            public Dictionary<string, XmlParsingConsumer<T>> GetMaintainableTagMap()
            {
                Dictionary<string, XmlParsingConsumer<T>> schemeActions = new Dictionary<string, XmlParsingConsumer<T>>(_config.GetSchemeExtraTagMap());
                schemeActions.Add(_config.GetItemTag(), (b, r) => {
                    Y item = _config.BuildItem();
                    Dictionary<string, XmlParsingConsumer<Y>> itemActions = new Dictionary<string, XmlParsingConsumer<Y>>(_config.GetItemExtraTagMap());
                    this._engine.ParseNameableNext(r, item, itemActions);
                    b.AddItem(item);
                });

                return schemeActions;
            }
        }

        internal abstract IItemSchemeParserConfig<T, Y> GetItemSchemeParserConfig();

        protected override void ParseXmlAttribute(XmlReader reader, IMaintainableMutableObject current)
        {
            IItemSchemeMutableObject<Y> itemSchemeMutableObject = (IItemSchemeMutableObject<Y>)current;
            ParseBooleanAttributeValue(reader, "isPartial").IfNotNull(v => itemSchemeMutableObject.IsPartial = v.Value);
            base.ParseXmlAttribute(reader, current);
        }
    }
}