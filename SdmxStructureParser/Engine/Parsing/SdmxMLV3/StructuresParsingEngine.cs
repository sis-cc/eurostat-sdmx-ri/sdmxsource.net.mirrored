// -----------------------------------------------------------------------
// <copyright file="StructuresParsingEngine.cs" company="EUROSTAT">
//   Date Created : 2022-02-14
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Parsing.SdmxMLV3
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Xml;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Xml;

    public class StructuresParsingEngine : CommonParsingEngine
    {
        private readonly IMaintainableArtefactParsingEngine _dataStructureParsingEngine = new DataStructureParsingEngine();
        private readonly IMaintainableArtefactParsingEngine _dataflowParsingEngine = new DataflowParsingEngine();
        private readonly IMaintainableArtefactParsingEngine _codelistParsingEngine = new CodelistParsingEngine();
        private readonly IMaintainableArtefactParsingEngine _conceptSchemeParsingEngine = new ConceptSchemeParsingEngine();

        public ISdmxObjects Parse(XmlReader reader)
        {
            ISdmxObjects beans = new SdmxObjectsImpl();
            Dictionary<string, XmlParsingConsumer<ISdmxObjects>> consumerMap = BuildMap();
            ParseNext(reader, beans, consumerMap);
            return beans;
        }

        private Dictionary<string, XmlParsingConsumer<ISdmxObjects>> BuildMap()
        {
            Dictionary<string, XmlParsingConsumer<ISdmxObjects>> structureMap = new Dictionary<string, XmlParsingConsumer<ISdmxObjects>>();
            structureMap.Add("AgencySchemes", SkipNode);
            structureMap.Add("Categorisations", SkipNode);
            structureMap.Add("CategorySchemeMaps", SkipNode);
            structureMap.Add("CategorySchemes", SkipNode);
            structureMap.Add("ConceptSchemeMaps", SkipNode);
            structureMap.Add("CustomTypeSchemes", SkipNode);
            structureMap.Add("DataConstraints", SkipNode);
            structureMap.Add("DataConsumerSchemes", SkipNode);
            structureMap.Add("DataProviderSchemes", SkipNode);
            structureMap.Add("GeographicCodelists", SkipNode);
            structureMap.Add("GeoGridCodelists", SkipNode);
            structureMap.Add("Hierarchies", SkipNode);
            structureMap.Add("HierarchyAssociations", SkipNode);
            structureMap.Add("MetadataConstraints", SkipNode);
            structureMap.Add("Metadataflows", SkipNode);
            structureMap.Add("MetadataProviderSchemes", SkipNode);
            structureMap.Add("MetadataProvisionAgreements", SkipNode);
            structureMap.Add("MetadataStructures", SkipNode);
            structureMap.Add("NamePersonalisationSchemes", SkipNode);
            structureMap.Add("OrganisationSchemeMaps", SkipNode);
            structureMap.Add("OrganisationUnitSchemes", SkipNode);
            structureMap.Add("Processes", SkipNode);
            structureMap.Add("ProvisionAgreements", SkipNode);
            structureMap.Add("ReportingTaxonomies", SkipNode);
            structureMap.Add("ReportingTaxonomyMaps", SkipNode);
            structureMap.Add("RepresentationMaps", SkipNode);
            structureMap.Add("RulesetSchemes", SkipNode);
            structureMap.Add("StructureMaps", SkipNode);
            structureMap.Add("TransformationSchemes", SkipNode);
            structureMap.Add("UserDefinedOperatorSchemes", SkipNode);
            structureMap.Add("ValueLists", SkipNode);
            structureMap.Add("VtlMappingSchemes", SkipNode);
            structureMap.Add("Dataflows", (b, r) => Parse(b, r, _dataflowParsingEngine));
            structureMap.Add("Codelists", (b, r) => Parse(b, r, _codelistParsingEngine));
            structureMap.Add("ConceptSchemes", (b, r) => Parse(b, r, _conceptSchemeParsingEngine));
            structureMap.Add("DataStructures", (b, r) => Parse(b, r, _dataStructureParsingEngine));
            return structureMap;
        }

        private void Parse(ISdmxObjects sdmxObjects, XmlReader reader, IMaintainableArtefactParsingEngine parsingEngine)
        {
            IMutableObjects objects = parsingEngine.Parse(reader);
            sdmxObjects.Merge(objects.ImmutableObjects);
        }

        private void SkipNode(ISdmxObjects sdmxBeans, XmlReader reader)
        {
            StaxUtil.SkipToEndNode(reader, reader.LocalName);
        }
    }
}
