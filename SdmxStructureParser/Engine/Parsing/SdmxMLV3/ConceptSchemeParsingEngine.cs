// -----------------------------------------------------------------------
// <copyright file="ConceptSchemeParsingEngine.cs" company="EUROSTAT">
//   Date Created : 2022-01-18
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Parsing.SdmxMLV3
{
    using System.Collections.Generic;
    using System.Xml;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util.Extensions;

    public class ConceptSchemeParsingEngine : ItemSchemeParsingEngine<IConceptSchemeMutableObject, IConceptMutableObject>, IMaintainableArtefactParsingEngine
    {
        internal override IItemSchemeParserConfig<IConceptSchemeMutableObject, IConceptMutableObject> GetItemSchemeParserConfig()
        {
            return new ConceptSchemeParserConfig(ParseIsoConceptReference, ParseRepresentation);
        }

        private void ParseIsoConceptReference(IConceptMutableObject obj, XmlReader reader)
        {
            StructureReferenceImpl s = new StructureReferenceImpl();
            Dictionary<string, XmlParsingConsumer<StructureReferenceImpl>> consumerMap = new Dictionary<string, XmlParsingConsumer<StructureReferenceImpl>>();
            consumerMap.Put("ConceptAgency", (b, r) => b.AgencyId = ParseValue(r));
            consumerMap.Put("ConceptSchemeID", (b, r) => b.MaintainableId = ParseValue(r));
            consumerMap.Put("ConceptID", (b, r) => {
                IdentifiableRefObjetcImpl identifiableRefObj = new IdentifiableRefObjetcImpl(b, ParseValue(r),
                    SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Concept));
                b.ChildReference = identifiableRefObj;
            });
            s.TargetStructureType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Concept);
            obj.IsoConceptReference = s;
        }
    }
}
