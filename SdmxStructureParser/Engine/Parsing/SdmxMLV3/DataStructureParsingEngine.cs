// -----------------------------------------------------------------------
// <copyright file="DataStructureParsingEngine.cs" company="EUROSTAT">
//   Date Created : 2022-01-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Parsing.SdmxMLV3
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Xml;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Util.Extensions;

    public class DataStructureParsingEngine : MaintainableParsingEngine<IDataStructureMutableObject>
    {
        protected override IMaintainableParserConfig<IDataStructureMutableObject> GetMaintainableParserConfig()
        {
            return new DataStructureParserConfig(ParseDataStructureComponents2, ParseURN);
        }

        private Dictionary<string, XmlParsingConsumer<T>> ParseAttributeRelationship<T>()
            where T : IAttributeRelationshipMutableObject
        {
            Dictionary<string, XmlParsingConsumer<T>> consumerMap = new Dictionary<string, XmlParsingConsumer<T>>();

            consumerMap.Put("Dataflow", (b, r) => b.AttachmentLevel = AttributeAttachmentLevel.DataSet);
            consumerMap.Put("Dimension", (b, r) => {
                b.AttachmentLevel = AttributeAttachmentLevel.DimensionGroup;
                if (b.DimensionReferences == null)
                {
                    b.DimensionReferences = new List<string>();
                }
                // TODO support optional XML attribute
                b.DimensionReferences.Add(ParseValue(r));
            });
            consumerMap.Put("Group", (b, r) => {
                b.AttachmentLevel = AttributeAttachmentLevel.Group;
                b.AttachmentGroup = ParseValue(r);
            });
            consumerMap.Put("Observation", (b, r) => b.AttachmentLevel = AttributeAttachmentLevel.Observation);

            return consumerMap;
        }

        private Dictionary<string, XmlParsingConsumer<IMeasureListMutableObject>> ParseMeasureList()
        {
            Dictionary<string, XmlParsingConsumer<IMeasureListMutableObject>> consumerMap = 
                new Dictionary<string, XmlParsingConsumer<IMeasureListMutableObject>>();
            consumerMap.Put("Measure", (b, r) => b.AddMeasure(ParseMeasure(r)));
            return consumerMap;
        }

        private IDimensionMutableObject ParseDimension(XmlReader reader, string endTag)
        {
            IDimensionMutableObject component = new DimensionMutableCore();
            component.TimeDimension = endTag.Equals("TimeDimension");
            Dictionary<string, XmlParsingConsumer<IDimensionMutableObject>> consumerMap = 
                new Dictionary<string, XmlParsingConsumer<IDimensionMutableObject>>();
            ParseComponentCommon(reader, component, consumerMap);
            return component;
        }

        private IAttributeMutableObject ParseAttribute(XmlReader reader)
        {
            IAttributeMutableObject component = new AttributeMutableCore();
            ParseUsage(reader, component);
            Dictionary<string, XmlParsingConsumer<IAttributeMutableObject>> consumerMap = 
                new Dictionary<string, XmlParsingConsumer<IAttributeMutableObject>>();
            consumerMap.Put("AttributeRelationship", (b, r) => ParseNext(r, b, ParseAttributeRelationship<IAttributeMutableObject>()));
            consumerMap.Put("MeasureRelationship", (b, r) => ParseNext(r, b, ParseMeasureRelationShip()));
            ParseComponentCommon(reader, component, consumerMap);
            return component;
        }

        private Dictionary<string, XmlParsingConsumer<IAttributeMutableObject>> ParseMeasureRelationShip()
        {
            Dictionary<string, XmlParsingConsumer<IAttributeMutableObject>> consumerMap = 
                new Dictionary<string, XmlParsingConsumer<IAttributeMutableObject>>();
            consumerMap.Put("Measure", (b, r) => b.MeasureRelationships.Add(ParseValue(r)));
            return consumerMap;
        }

        private IMeasureMutableObject ParseMeasure(XmlReader reader)
        {
            IMeasureMutableObject component = new MeasureMutableCore();
            ParseUsage(reader, component);
            Dictionary<string, XmlParsingConsumer<IMeasureMutableObject>> consumerMap = 
                new Dictionary<string, XmlParsingConsumer<IMeasureMutableObject>>();
            ParseComponentCommon(reader, component, consumerMap);
            return component;
        }

        private void ParseUsage(XmlReader reader, IUsageMutableObject component)
        {
            ParseAttributeValue(reader, "usage", s => 
                !string.IsNullOrEmpty(s) && s.Equals("mandatory") || 
                s.Equals("optional"), s => s.Equals("mandatory") ? UsageType.Mandatory : UsageType.Optional)
                .IfNotNull(v => component.Usage = v);
        }

        private void ParseComponentCommon<T>(XmlReader reader, T component, Dictionary<string, XmlParsingConsumer<T>> consumerMap)
            where T : IComponentMutableObject
        {
            SetComponentConsumerMap(consumerMap);
            ParseIdentifiableNext(reader, component, consumerMap);
        }

        private IGroupMutableObject ParseGroup(XmlReader reader)
        {
            IGroupMutableObject groupMutableBean = new GroupMutableCore();
            ParseIdentifiableNext(reader, groupMutableBean, this.ParseStartGroup());
            return groupMutableBean;
        }

        private Dictionary<string, XmlParsingConsumer<IGroupMutableObject>> ParseStartGroup()
        {
            Dictionary<string, XmlParsingConsumer<IGroupMutableObject>> consumerMap = 
                new Dictionary<string, XmlParsingConsumer<IGroupMutableObject>>();
            consumerMap.Put("GroupDimension", (b, r) => {
                Dictionary<string, XmlParsingConsumer<IGroupMutableObject>> gm = 
                new Dictionary<string, XmlParsingConsumer<IGroupMutableObject>>();
                gm.Add("DimensionReference", (bean1, reader1) => bean1.DimensionRef.Add(ParseValue(reader1)));
                ParseNext(r, b, gm);
            });
            return consumerMap;
        }

        private void SetComponentConsumerMap<T>(Dictionary<string, XmlParsingConsumer<T>> consumerMap)
            where T : IComponentMutableObject
        {
            consumerMap.Put("ConceptIdentity", (b, r) => b.ConceptRef = ParseURN(r));
            consumerMap.Put("ConceptRole", (b, r) => b.AddConceptRole(ParseURN(r)));
            consumerMap.Put("LocalRepresentation", (b, r) => b.Representation = ParseRepresentation(r, r.LocalName));
        }

        private IMetadataAttributeUsageMutableObject ParseMetadataAttributeUsage(XmlReader reader)
        {
            IMetadataAttributeUsageMutableObject obj = new MetadataAttributeUsageMutableCore();
            Dictionary<string, XmlParsingConsumer<IMetadataAttributeUsageMutableObject>> consumerMap = 
                new Dictionary<string, XmlParsingConsumer<IMetadataAttributeUsageMutableObject>>();

            consumerMap.Put("MetadataAttributeReference", (b, r) => b.MetadataAttributeReference = ParseValue(r));
            consumerMap.Put("AttributeRelationship", (b, r) => ParseNext(r, b, ParseAttributeRelationship<IMetadataAttributeUsageMutableObject>()));
            ParseNext(reader, obj, consumerMap);

            return obj;
        }

        private void ParseDataStructureComponents2(IDataStructureMutableObject b, XmlReader r)
        {
            Dictionary<string, XmlParsingConsumer<IDataStructureMutableObject>> consumerMap = 
                new Dictionary<string, XmlParsingConsumer<IDataStructureMutableObject>>();

            consumerMap.Put("DimensionList", (bean, reader) => {
                bean.DimensionList = new DimensionListMutableCore();
                Dictionary<string, XmlParsingConsumer<IDimensionListMutableObject>> dlMap = 
                    new Dictionary<string, XmlParsingConsumer<IDimensionListMutableObject>>();
                dlMap.Add("Dimension", (b1, r1) => b1.AddDimension(ParseDimension(r1, r1.LocalName)));
                dlMap.Add("TimeDimension", (b1, r1) => b1.AddDimension(ParseDimension(r1, r1.LocalName)));
                ParseAnnotableNext(reader, bean.DimensionList, dlMap);
            });
            consumerMap.Put("Group", (bean, reader) => bean.AddGroup(ParseGroup(reader)));
            consumerMap.Put("AttributeList", (bean, reader) => {
                bean.AttributeList = new AttributeListMutableCore();
                Dictionary<string, XmlParsingConsumer<IAttributeListMutableObject>> dlMap = 
                    new Dictionary<string, XmlParsingConsumer<IAttributeListMutableObject>>();
                dlMap.Add("Attribute", (b1, r1) => b1.AddAttribute(ParseAttribute(r1)));
                dlMap.Add("MetadataAttributeUsage", (b1, r1) => b1.MetadataAttributes.Add(ParseMetadataAttributeUsage(r1)));
                ParseAnnotableNext(reader, bean.AttributeList, dlMap);
            });
            consumerMap.Put("MeasureList", (bean, reader) => {
                bean.MeasureList = new MeasureListMutableCore();
                ParseAnnotableNext(reader, bean.MeasureList, this.ParseMeasureList());
            });

            ParseNext(r, b, consumerMap);
        }
    }
}
