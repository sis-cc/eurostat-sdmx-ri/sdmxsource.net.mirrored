// -----------------------------------------------------------------------
// <copyright file="CodelistParserConfig.cs" company="EUROSTAT">
//   Date Created : 2022-01-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Parsing.SdmxMLV3
{
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Util.Extensions;
    using static Org.Sdmxsource.Sdmx.Structureparser.Engine.Parsing.SdmxMLV3.CommonParsingEngine;

    internal class CodelistParserConfig : IItemSchemeParserConfig<ICodelistMutableObject, ICodeMutableObject>
    {
        private readonly XmlParsingConsumer<ICodelistMutableObject> _consumer;

        public CodelistParserConfig(XmlParsingConsumer<ICodelistMutableObject> consumer)
        {
            _consumer = consumer;
        }

        public ICodelistMutableObject BuildScheme()
        {
            return new CodelistMutableCore();
        }

        public ICodeMutableObject BuildItem()
        {
            return new CodeMutableCore();
        }

        public string GetSchemeTag()
        {
            return "Codelist";
        }

        public string GetItemTag()
        {
            return "Code";
        }

        public Dictionary<string, XmlParsingConsumer<ICodelistMutableObject>> GetSchemeExtraTagMap()
        {
            Dictionary<string, XmlParsingConsumer<ICodelistMutableObject>> consumerMap = new Dictionary<string, XmlParsingConsumer<ICodelistMutableObject>>();
            consumerMap.Put("CodelistExtension", this._consumer);
            return consumerMap;
        }

        public Dictionary<string, XmlParsingConsumer<ICodeMutableObject>> GetItemExtraTagMap()
        {
            Dictionary<string, XmlParsingConsumer<ICodeMutableObject>> consumerMap = new Dictionary<string, XmlParsingConsumer<ICodeMutableObject>>();
            consumerMap.Put("Parent", (b, r) => b.ParentCode = ParseValue(r));
            return consumerMap;
        }
    }
}
