// -----------------------------------------------------------------------
// <copyright file="DataStructureParserConfig.cs" company="EUROSTAT">
//   Date Created : 2022-01-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Parsing.SdmxMLV3
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Util.Extensions;
    using static Org.Sdmxsource.Sdmx.Structureparser.Engine.Parsing.SdmxMLV3.CommonParsingEngine;

    internal class DataStructureParserConfig : IMaintainableParserConfig<IDataStructureMutableObject>
    {
        private readonly XmlParsingConsumer<IDataStructureMutableObject> _dataConsumer;
        private readonly Func<XmlReader, IStructureReference> _metadataConsumer;

        public DataStructureParserConfig(
            XmlParsingConsumer<IDataStructureMutableObject> dataConsumer,
            Func<XmlReader, IStructureReference> metadataConsumer)
        {
            _dataConsumer = dataConsumer;
            _metadataConsumer = metadataConsumer;
        }

        public string GetMaintainableTag()
        {
            return "DataStructure";
        }

        public IDataStructureMutableObject BuildMaintainableArtefact()
        {
            return new DataStructureMutableCore();
        }

        public Dictionary<string, XmlParsingConsumer<IDataStructureMutableObject>> GetMaintainableTagMap()
        {
            Dictionary<string, XmlParsingConsumer<IDataStructureMutableObject>> consumerMap = new Dictionary<string, XmlParsingConsumer<IDataStructureMutableObject>>();
            consumerMap.Put("DataStructureComponents", this._dataConsumer);
            consumerMap.Put("Metadata", (b, r) => b.MetadataStructure = this._metadataConsumer(r));

            return consumerMap;
        }
    }
}
