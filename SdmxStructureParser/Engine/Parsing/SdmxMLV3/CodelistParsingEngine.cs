// -----------------------------------------------------------------------
// <copyright file="CodelistParsingEngine.cs" company="EUROSTAT">
//   Date Created : 2022-01-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Parsing.SdmxMLV3
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Xml;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Codelist;
    using Org.Sdmxsource.Util.Extensions;

    public class CodelistParsingEngine : ItemSchemeParsingEngine<ICodelistMutableObject, ICodeMutableObject>
    {
        internal override IItemSchemeParserConfig<ICodelistMutableObject, ICodeMutableObject> GetItemSchemeParserConfig()
        {
            return new CodelistParserConfig(ParseCodelistExtension);
        }

        private void ParseCodelistExtension(ICodelistMutableObject obj, XmlReader reader)
        {
            ICodelistInheritanceRuleMutableObject codelistExtensionMutableObject = new CodelistInheritanceRuleMutableCore();
            obj.CodelistExtensions.Add(codelistExtensionMutableObject);
            ParseStringAttributeValue(reader, "prefix").IfNotNull(v => codelistExtensionMutableObject.Prefix = v);
            Dictionary<string, XmlParsingConsumer<ICodelistInheritanceRuleMutableObject>> consumerMap = 
                new Dictionary<string, XmlParsingConsumer<ICodelistInheritanceRuleMutableObject>>();
            consumerMap.Put("Codelist", (b, r) => b.CodelistRef = ParseURN(r));
            consumerMap.Put("InclusiveCodeSelection", (b, r) => ParseCodeSelection(b, r, true));
            consumerMap.Put("ExclusiveCodeSelection", (b, r) => ParseCodeSelection(b, r, false));
            ParseNext(reader, codelistExtensionMutableObject, consumerMap);
        }

        private void ParseCodeSelection(ICodelistInheritanceRuleMutableObject obj, XmlReader reader, bool isInclusive)
        {
            ICodeSelectionMutableObject selection = new CodeSelectionMutableCore();
            obj.CodeSelection = selection;
            selection.IsInclusive = isInclusive;
            Dictionary<string, XmlParsingConsumer<ICodeSelectionMutableObject>> consumerMap = 
                new Dictionary<string, XmlParsingConsumer<ICodeSelectionMutableObject>>();
            consumerMap.Put("MemberValue", (b, r) => {
                IMemberValueMutableObject ImutableObject = new MemberValueMutableCore();
                b.MemberValues.Add(ImutableObject);
                ParseCascadeSelectionAttributeValue(r, "cascadeValues").IfNotNull(v => ImutableObject.Cascade = v.Value);
                ImutableObject.Value = ParseValue(r);
            });
            ParseNext(reader, selection, consumerMap);
        }
    }
}
