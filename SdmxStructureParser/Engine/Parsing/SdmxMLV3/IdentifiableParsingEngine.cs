// -----------------------------------------------------------------------
// <copyright file="IdentifiableParsingEngine.cs" company="EUROSTAT">
//   Date Created : 2022-01-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Parsing.SdmxMLV3
{
    using System.Collections.Generic;
    using System.Xml;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    public abstract class IdentifiableParsingEngine : AnnotableParsingEngine
    {
        protected void ParseIdentifiableNext<T>(XmlReader reader, T obj, Dictionary<string, XmlParsingConsumer<T>> consumerMap)
            where T : IIdentifiableMutableObject
        {
            ParseXmlAttribute(reader, obj);

            consumerMap.Put("Link", (bean1, reader1)=> { }); // TODO parse link, SDMXCommon.xsd line 274

            ParseAnnotableNext(reader, obj, consumerMap);
        }

        private void ParseXmlAttribute(XmlReader reader, IIdentifiableMutableObject identifiableMutableObject)
        {
            if (ObjectUtil.ValidString(identifiableMutableObject.Id))
            {
                throw new SdmxSemmanticException("Id already set");
            }
            identifiableMutableObject.Id = reader.GetAttribute("id");
        }
    }
}