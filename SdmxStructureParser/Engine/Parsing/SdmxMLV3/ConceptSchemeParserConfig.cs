// -----------------------------------------------------------------------
// <copyright file="ConceptSchemeParserConfig.cs" company="EUROSTAT">
//   Date Created : 2022-01-18
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Parsing.SdmxMLV3
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Util.Extensions;
    using static Org.Sdmxsource.Sdmx.Structureparser.Engine.Parsing.SdmxMLV3.CommonParsingEngine;

    internal class ConceptSchemeParserConfig : IItemSchemeParserConfig<IConceptSchemeMutableObject, IConceptMutableObject>
    {
        private readonly XmlParsingConsumer<IConceptMutableObject> _consumer;
        private readonly Func<XmlReader, string, IRepresentationMutableObject> _representationParser;

        public ConceptSchemeParserConfig(
            XmlParsingConsumer<IConceptMutableObject> consumer,
            Func<XmlReader, string, IRepresentationMutableObject> representationParser)
        {
            _consumer = consumer;
            _representationParser = representationParser;
        }

        public IConceptSchemeMutableObject BuildScheme()
        {
            return new ConceptSchemeMutableCore();
        }

        public IConceptMutableObject BuildItem()
        {
            return new ConceptMutableCore();
        }

        public string GetSchemeTag()
        {
            return "ConceptScheme";
        }

        public string GetItemTag()
        {
            return "Concept";
        }

        public Dictionary<string, XmlParsingConsumer<IConceptSchemeMutableObject>> GetSchemeExtraTagMap()
        {
            return new Dictionary<string, XmlParsingConsumer<IConceptSchemeMutableObject>>();
        }

        public Dictionary<string, XmlParsingConsumer<IConceptMutableObject>> GetItemExtraTagMap()
        {
            Dictionary<string, XmlParsingConsumer<IConceptMutableObject>> consumerMap = new Dictionary<string, XmlParsingConsumer<IConceptMutableObject>>();
            consumerMap.Put("Parent", (b, r) => b.ParentConcept = ParseValue(r));
            consumerMap.Put("CoreRepresentation", (b, r) => b.CoreRepresentation = this._representationParser(r, r.LocalName));
            consumerMap.Put("ISOConceptReference", this._consumer);
            return consumerMap;
        }
    }
}
