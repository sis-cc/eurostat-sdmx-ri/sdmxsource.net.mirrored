// -----------------------------------------------------------------------
// <copyright file="AnnotableParsingEngine.cs" company="EUROSTAT">
//   Date Created : 2022-01-18
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Parsing.SdmxMLV3
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Xml;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    public abstract class AnnotableParsingEngine : CommonParsingEngine
    {
        public void ParseAnnotableNext<T>(XmlReader reader, T obj, Dictionary<string, XmlParsingConsumer<T>> consumerMap)
            where T : IAnnotableMutableObject
        {
            consumerMap.Put("Annotations", (l, r) => { });
            consumerMap.Put("Annotation", (l, r) =>
            {
                IAnnotationMutableObject current = new AnnotationMutableCore();
                l.AddAnnotation(current);
                current.Id = r.GetAttribute("id");
                ParseNext(r, current, ParseAnnotation);
            });

            base.ParseNext(reader, obj, consumerMap);
        }

        /// <summary>
        /// Parses an annotation URL
        /// </summary>
        /// <param name="reader">The xml reader</param>
        /// <param name="consumer">The action to take the annotation URL.</param>
        protected void ParseAnnotationURL(XmlReader reader, Action<IAnnotationUrlMutableObject> consumer)
        {
            string lang = reader.GetAttribute("lang", SdmxConstants.Xmlns);
            if (!ObjectUtil.ValidString(lang))
            {
                lang = null;
            }
            string uri = ParseValue(reader);
            consumer.Invoke(new AnnotationUrlMutableCore(lang, uri));
        }

        private bool ParseAnnotation(IAnnotationMutableObject current, XmlReader reader)
        {
            switch (reader.LocalName)
            {
                case "AnnotationTitle":
                    current.Title = ParseValue(reader);
                    break;
                case "AnnotationType":
                    current.Type = ParseValue(reader);
                    break;
                case "AnnotationValue":
                    current.Value = ParseValue(reader);
                    break;
                case "AnnotationURL":
                    ParseAnnotationURL(reader, current.AddUrl);
                    break;
                case "AnnotationText":
                    ParseTextType(reader, current.AddText);
                    break;
            }
            return true;
        }
    }
}
