// -----------------------------------------------------------------------
// <copyright file="IItemSchemeParserConfig.cs" company="EUROSTAT">
//   Date Created : 2022-01-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Parsing.SdmxMLV3
{
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using static Org.Sdmxsource.Sdmx.Structureparser.Engine.Parsing.SdmxMLV3.CommonParsingEngine;

    public interface IItemSchemeParserConfig<T, Y>
        where T : IItemSchemeMutableObject<Y>
        where Y : IItemMutableObject
    {
        T BuildScheme();
        
        Y BuildItem();

        string GetSchemeTag();

        string GetItemTag();

        /// <summary>
        /// Holds the actions for any extra direct children of T
        /// This shouldn't include the item, or any other common maintainable tag, like Name, Annotations
        /// </summary>
        /// <returns></returns>
        Dictionary<string, XmlParsingConsumer<T>> GetSchemeExtraTagMap();

        /// <summary>
        /// Holds any extra tag outside the common item tags like Name, Annotations etc
        /// </summary>
        /// <returns></returns>
        Dictionary<string, XmlParsingConsumer<Y>> GetItemExtraTagMap();
    }
}