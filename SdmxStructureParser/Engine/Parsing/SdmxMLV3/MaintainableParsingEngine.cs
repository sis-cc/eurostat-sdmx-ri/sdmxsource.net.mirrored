// -----------------------------------------------------------------------
// <copyright file="MaintanableParsingEngine.cs" company="EUROSTAT">
//   Date Created : 2022-01-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Parsing.SdmxMLV3
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Util.Date;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    public abstract class MaintainableParsingEngine<T> : NameableParsingEngine, IMaintainableArtefactParsingEngine
        where T : IMaintainableMutableObject
    {
        public IMutableObjects Parse(XmlReader reader)
        {
            IMaintainableParserConfig<T> config = GetMaintainableParserConfig();
            return ParseMaintainable(reader, config.GetMaintainableTag(), config.BuildMaintainableArtefact, config.GetMaintainableTagMap());
        }

        protected abstract IMaintainableParserConfig<T> GetMaintainableParserConfig();

        /// <summary>
        /// Start Parsing the maintainable artefact from the specified reader. It expects the reader to be at the element before
        /// each individual artefact.e.g. for Dataflow it expects the reader to be at<code>Dataflows</code> tag at START_ELEMENT
        /// </summary>
        /// <param name="reader">The XML reader that should point to the artefact type group start element</param>
        /// <param name="rootTag">The root tag of the Maintainable Artefact</param>
        /// <param name="artefactBuilder">The function to get the results from.</param>
        /// <param name="consumerMap">The Map that has the action/consumer for each <b>direct</b> child of <b>rootTag</b></param>
        /// <returns>The Mutable objects container usually filled with <code>T</code> type of artefacts</returns>
        protected IMutableObjects ParseMaintainable(XmlReader reader, string rootTag, Func<T> artefactBuilder, Dictionary<string, XmlParsingConsumer<T>> consumerMap)
        {
            IMutableObjects mutableObjects = new MutableObjectsImpl();
            ParseNext<IMutableObject>(reader, null, (b, r) => {
                if (r.LocalName.Equals(rootTag))
                {
                    T obj = artefactBuilder.Invoke();
                    mutableObjects.AddIdentifiable(obj);
                    ParseXmlAttribute(r, obj);
                    ParseNameableNext(r, obj, consumerMap);
                }
                return true;
            });
            return mutableObjects;
        }

        protected virtual void ParseXmlAttribute(XmlReader reader, IMaintainableMutableObject current)
        {
            if (reader.HasAttributes)
            {
                while (reader.MoveToNextAttribute())
                {
                    string name = reader.LocalName;
                    string value = reader.Value;
                    switch (name)
                    {
                        case "agencyID":
                            current.AgencyId = value;
                            break;
                        case "version":
                            current.Version = value;
                            break;
                        case "isExternalReference":
                            current.ExternalReference = TertiaryBool.ParseString(value);
                            break;
                        case "serviceURL":
                            current.ServiceURL = new Uri(value);
                            break;
                        case "structureURL":
                            current.StructureURL = new Uri(value);
                            break;
                        case "validFrom":
                            current.StartDate = DateUtil.FormatDate(value, true);
                            break;
                        case "validTo":
                            current.EndDate = DateUtil.FormatDate(value, false);
                            break;
                    }
                }
                reader.MoveToElement();
            }
        }
    }
}