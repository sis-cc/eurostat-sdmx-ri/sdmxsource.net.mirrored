// -----------------------------------------------------------------------
// <copyright file="DataflowParserConfig.cs" company="EUROSTAT">
//   Date Created : 2022-01-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Parsing.SdmxMLV3
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Util.Extensions;
    using static Org.Sdmxsource.Sdmx.Structureparser.Engine.Parsing.SdmxMLV3.CommonParsingEngine;

    internal class DataflowParserConfig : IMaintainableParserConfig<IDataflowMutableObject>
    {
        private readonly Func<XmlReader, IStructureReference> _consumer;

        public DataflowParserConfig(Func<XmlReader, IStructureReference> consumer)
        {
            _consumer = consumer;
        }

        public string GetMaintainableTag()
        {
            return "Dataflow";
        }

        public Dictionary<string, XmlParsingConsumer<IDataflowMutableObject>> GetMaintainableTagMap()
        {
            Dictionary<string, XmlParsingConsumer<IDataflowMutableObject>> consumerMap = new Dictionary<string, XmlParsingConsumer<IDataflowMutableObject>>();
            consumerMap.Put("Structure", (b, r) => b.DataStructureRef = this._consumer(r));

            return consumerMap;
        }

        public IDataflowMutableObject BuildMaintainableArtefact()
        {
            return new DataflowMutableCore();
        }
    }
}
