// -----------------------------------------------------------------------
// <copyright file="TextTypeBuilderv3.cs" company="EUROSTAT">
//   Date Created : 2022-01-18
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Parsing.SdmxMLV3
{
    using Org.Sdmxsource.Sdmx.Api.Constants;

    public class TextTypeBuilderv3
    {
        public static TextType Build(string dataType)
        {
            switch (dataType)
            {
                case "Alpha":
                    return TextType.GetFromEnum(TextEnumType.Alpha);
                case "AlphaNumeric":
                    return TextType.GetFromEnum(TextEnumType.Alphanumeric);
                case "string":
                    return TextType.GetFromEnum(TextEnumType.String);
                case "BigInteger":
                    return TextType.GetFromEnum(TextEnumType.BigInteger);
                case "Integer":
                    return TextType.GetFromEnum(TextEnumType.Integer);
                case "Long":
                    return TextType.GetFromEnum(TextEnumType.Long);
                case "Short":
                    return TextType.GetFromEnum(TextEnumType.Short);
                case "Decimal":
                    return TextType.GetFromEnum(TextEnumType.Decimal);
                case "Float":
                    return TextType.GetFromEnum(TextEnumType.Float);
                case "Double":
                    return TextType.GetFromEnum(TextEnumType.Double);
                case "Boolean":
                    return TextType.GetFromEnum(TextEnumType.Boolean);
                case "DateTime":
                    return TextType.GetFromEnum(TextEnumType.DateTime);
                case "Time":
                    return TextType.GetFromEnum(TextEnumType.Time);
                case "Month":
                    return TextType.GetFromEnum(TextEnumType.Month);
                case "Day":
                    return TextType.GetFromEnum(TextEnumType.Day);
                case "MonthDay":
                    return TextType.GetFromEnum(TextEnumType.MonthDay);
                case "Numeric":
                    return TextType.GetFromEnum(TextEnumType.Numeric);
                case "Duration":
                    return TextType.GetFromEnum(TextEnumType.Duration);
                case "URI":
                    return TextType.GetFromEnum(TextEnumType.Uri);
                case "GregorianTimePeriod":
                    return TextType.GetFromEnum(TextEnumType.TimePeriod);
                case "Count":
                    return TextType.GetFromEnum(TextEnumType.Count);
                case "InclusiveValueRange":
                    return TextType.GetFromEnum(TextEnumType.InclusiveValueRange);
                case "ExclusiveValueRange":
                    return TextType.GetFromEnum(TextEnumType.ExclusiveValueRange);
                case "Incremental":
                    return TextType.GetFromEnum(TextEnumType.Incremental);
                case "ObservationalTimePeriod":
                    return TextType.GetFromEnum(TextEnumType.ObservationalTimePeriod);
                case "BasicTimePeriod":
                    return TextType.GetFromEnum(TextEnumType.BasicTimePeriod);
                case "DataSetReference":
                    return TextType.GetFromEnum(TextEnumType.DataSetReference);
                case "GregorianDay":
                    return TextType.GetFromEnum(TextEnumType.GregorianDay);
                case "GregorianYear":
                    return TextType.GetFromEnum(TextEnumType.GregorianYear);
                case "GregorianYearMonth":
                    return TextType.GetFromEnum(TextEnumType.GregorianYearMonth);
                case "ReportingDay":
                    return TextType.GetFromEnum(TextEnumType.ReportingDay);
                case "ReportingMonth":
                    return TextType.GetFromEnum(TextEnumType.ReportingMonth);
                case "ReportingQuarter":
                    return TextType.GetFromEnum(TextEnumType.ReportingQuarter);
                case "ReportingSemester":
                    return TextType.GetFromEnum(TextEnumType.ReportingSemester);
                case "ReportingTimePeriod":
                    return TextType.GetFromEnum(TextEnumType.ReportingTimePeriod);
                case "ReportingTrimester":
                    return TextType.GetFromEnum(TextEnumType.ReportingTrimester);
                case "ReportingWeek":
                    return TextType.GetFromEnum(TextEnumType.ReportingWeek);
                case "ReportingYear":
                    return TextType.GetFromEnum(TextEnumType.ReportingYear);
                case "StandardTimePeriod":
                    return TextType.GetFromEnum(TextEnumType.StandardTimePeriod);
                case "TimeRange":
                    return TextType.GetFromEnum(TextEnumType.TimesRange);
                case "XHTML":
                    return TextType.GetFromEnum(TextEnumType.Xhtml);
            }

            return null;
        }

        public static CascadeSelection? BuildCascadeSelection(string cascadeValue)
        {
            if (cascadeValue == null)
            {
                return null;
            }

            switch (cascadeValue)
            {
                case "0":
                case "false":
                    return CascadeSelection.False;
                case "1":
                case "true":
                    return CascadeSelection.True;
                case "excluderoot":
                    return CascadeSelection.ExcludeRoot;
                default:
                    return null;
            }
        }
    }
}