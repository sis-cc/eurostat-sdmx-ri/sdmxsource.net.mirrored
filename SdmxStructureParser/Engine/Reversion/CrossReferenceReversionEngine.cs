﻿// -----------------------------------------------------------------------
// <copyright file="CrossReferenceReversionEngine.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Reversion
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Engine;

    /// <summary>
    /// CrossReferenceReversionEngine class
    /// </summary>
    /// <seealso cref="Org.Sdmxsource.Sdmx.Structureparser.Engine.ICrossReferenceReversionEngine" />
    public class CrossReferenceReversionEngine : ICrossReferenceReversionEngine
    {
        /// <summary>
        /// The categorisation cross reference updater engine
        /// </summary>
        private readonly ICategorisationCrossReferenceUpdaterEngine categorisationCrossReferenceUpdaterEngine =
            new CategorisationCrossReferenceUpdaterEngine();

        /// <summary>
        /// The concept scheme cross reference updater engine
        /// </summary>
        private readonly IConceptSchemeCrossReferenceUpdaterEngine conceptSchemeCrossReferenceUpdaterEngine =
            new ConceptSchemeCrossReferenceUpdaterEngine();

        /// <summary>
        /// The dataflow cross reference updater engine
        /// </summary>
        private readonly IDataflowCrossReferenceUpdaterEngine dataflowCrossReferenceUpdaterEngine =
            new DataflowCrossReferenceUpdaterEngine();

        /// <summary>
        /// The data structure cross reference updater engine
        /// </summary>
        private readonly IDataStructureCrossReferenceUpdaterEngine dataStructureCrossReferenceUpdaterEngine =
            new DataStructureCrossReferenceUpdaterEngine();

        /// <summary>
        /// The hierarchic codelist cross reference updater engine
        /// </summary>
        private readonly IHierarchicCodelistCrossReferenceUpdaterEngine hierarchicCodelistCrossReferenceUpdaterEngine =
            new HierarchicCodelistCrossReferenceUpdaterEngine();

        /// <summary>
        /// The metadataflow cross reference updater engine
        /// </summary>
        private readonly IMetadataflowCrossReferenceUpdaterEngine metadataflowCrossReferenceUpdaterEngine =
            new MetadataflowCrossReferenceUpdaterEngine();

        /// <summary>
        /// The metadata structure cross reference updater engine
        /// </summary>
        private readonly IMetadataStructureCrossReferenceUpdaterEngine metadataStructureCrossReferenceUpdaterEngine =
            new MetadataStructureCrossReferenceUpdaterEngine();

        /// <summary>
        /// The process cross reference updater
        /// </summary>
        private readonly IProcessCrossReferenceUpdater processCrossReferenceUpdater = new ProcessCrossReferenceUpdater();

        /// <summary>
        /// The provision cross reference updater engine
        /// </summary>
        private readonly IProvisionCrossReferenceUpdaterEngine provisionCrossReferenceUpdaterEngine =
            new ProvisionCrossReferenceUpdaterEngine();

        /// <summary>
        /// The reporting taxonomy bean cross reference updater engine
        /// </summary>
        private readonly IReportingTaxonomyBeanCrossReferenceUpdaterEngine
            reportingTaxonomyBeanCrossReferenceUpdaterEngine = new ReportingTaxonomyBeanCrossReferenceUpdaterEngine();

        /// <summary>
        /// The structure set cross reference updater engine
        /// </summary>
        private readonly IStructureSetCrossReferenceUpdaterEngine structureSetCrossReferenceUpdaterEngine =
            new StructureSetCrossReferenceUpdaterEngine();

        /// <summary>
        /// Resolves the references.
        /// </summary>
        /// <param name="structures">The structures</param>
        /// <param name="resolveAgencies">Flag indicating resolve agencies .</param>
        /// <param name="resolutionDepth">The resolution depth.</param>
        /// <param name="retrievalManager">The retrieval manager</param>
        /// <returns>
        /// The references
        /// </returns>
        public IDictionary<IIdentifiableObject, ISet<IIdentifiableObject>> ResolveReferences(
            ISdmxObjects structures, 
            bool resolveAgencies, 
            int resolutionDepth, 
            IIdentifiableRetrievalManager retrievalManager)
        {
            ICrossReferenceResolverEngine crossReferenceResolver = new CrossReferenceResolverEngineCore();
            return crossReferenceResolver.ResolveReferences(
                structures, 
                resolveAgencies, 
                resolutionDepth, 
                retrievalManager);
        }

        /// <summary>
        /// Updates the references of the given maintainable and reversions the maintainable structure
        /// </summary>
        /// <param name="maintianable">The maintainable.</param>
        /// <param name="updateReferences">The update references.</param>
        /// <param name="newVersionNumber">The new version number.</param>
        /// <returns>The <see cref="IMaintainableObject" />.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="maintianable" /> is <see langword="null" />.</exception>
        public IMaintainableObject UdpateReferences(
            IMaintainableObject maintianable, 
            IDictionary<IStructureReference, IStructureReference> updateReferences, 
            string newVersionNumber)
        {
            if (maintianable == null)
            {
                throw new ArgumentNullException("maintianable");
            }

            switch (maintianable.StructureType.EnumType)
            {
                case SdmxStructureEnumType.AttachmentConstraint:
                    break;
                case SdmxStructureEnumType.Categorisation:
                    return
                        this.categorisationCrossReferenceUpdaterEngine.UpdateReferences(
                            (ICategorisationObject)maintianable, 
                            updateReferences, 
                            newVersionNumber);
                case SdmxStructureEnumType.ConceptScheme:
                    return
                        this.conceptSchemeCrossReferenceUpdaterEngine.UpdateReferences(
                            (IConceptSchemeObject)maintianable, 
                            updateReferences, 
                            newVersionNumber);
                case SdmxStructureEnumType.ContentConstraint:
                    break;
                case SdmxStructureEnumType.Dataflow:
                    return this.dataflowCrossReferenceUpdaterEngine.UpdateReferences(
                        (IDataflowObject)maintianable, 
                        updateReferences, 
                        newVersionNumber);
                case SdmxStructureEnumType.Dsd:
                    return
                        this.dataStructureCrossReferenceUpdaterEngine.UpdateReferences(
                            (IDataStructureObject)maintianable, 
                            updateReferences, 
                            newVersionNumber);
                case SdmxStructureEnumType.HierarchicalCodelist:
                    return
                        this.hierarchicCodelistCrossReferenceUpdaterEngine.UpdateReferences(
                            (IHierarchicalCodelistObject)maintianable, 
                            updateReferences, 
                            newVersionNumber);
                case SdmxStructureEnumType.MetadataFlow:
                    return this.metadataflowCrossReferenceUpdaterEngine.UpdateReferences(
                        (IMetadataFlow)maintianable, 
                        updateReferences, 
                        newVersionNumber);
                case SdmxStructureEnumType.Msd:
                    return
                        this.metadataStructureCrossReferenceUpdaterEngine.UpdateReferences(
                            (IMetadataStructureDefinitionObject)maintianable, 
                            updateReferences, 
                            newVersionNumber);
                case SdmxStructureEnumType.Process:
                    return this.processCrossReferenceUpdater.UpdateReferences(
                        (IProcessObject)maintianable, 
                        updateReferences, 
                        newVersionNumber);
                case SdmxStructureEnumType.ProvisionAgreement:
                    return
                        this.provisionCrossReferenceUpdaterEngine.UpdateReferences(
                            (IProvisionAgreementObject)maintianable, 
                            updateReferences, 
                            newVersionNumber);
                case SdmxStructureEnumType.ReportingTaxonomy:
                    return
                        this.reportingTaxonomyBeanCrossReferenceUpdaterEngine.UpdateReferences(
                            (IReportingTaxonomyObject)maintianable, 
                            updateReferences, 
                            newVersionNumber);
                case SdmxStructureEnumType.StructureSet:
                    return
                        this.structureSetCrossReferenceUpdaterEngine.UpdateReferences(
                            (IStructureSetObject)maintianable, 
                            updateReferences, 
                            newVersionNumber);
            }

            return maintianable;
        }
    }
}