﻿// -----------------------------------------------------------------------
// <copyright file="ProcessCrossReferenceUpdater.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Reversion
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    ///     TODO: Update summary.
    /// </summary>
    public class ProcessCrossReferenceUpdater : IProcessCrossReferenceUpdater
    {
        /// <summary>
        ///     Update references.
        /// </summary>
        /// <param name="maintainable">
        ///     The maintainable.
        /// </param>
        /// <param name="updateReferences">
        ///     The update references.
        /// </param>
        /// <param name="newVersionNumber">
        ///     The new version number.
        /// </param>
        /// <returns>
        ///     The <see cref="IProcessObject" />.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="maintainable"/> is <see langword="null" />.</exception>
        public IProcessObject UpdateReferences(
            IProcessObject maintainable, 
            IDictionary<IStructureReference, IStructureReference> updateReferences, 
            string newVersionNumber)
        {
            if (maintainable == null)
            {
                throw new ArgumentNullException("maintainable");
            }

            IProcessMutableObject processMutableBean = maintainable.MutableInstance;
            processMutableBean.Version = newVersionNumber;
            this.UpdateProcessSteps(processMutableBean.ProcessSteps, updateReferences);
            return processMutableBean.ImmutableInstance;
        }

        /// <summary>
        ///     The update input output.
        /// </summary>
        /// <param name="inputOutput">
        ///     The input output.
        /// </param>
        /// <param name="updateReferences">
        ///     The update references.
        /// </param>
        private void UpdateInputOutput(
            IList<IInputOutputMutableObject> inputOutput, 
            IDictionary<IStructureReference, IStructureReference> updateReferences)
        {
            if (inputOutput != null)
            {
                foreach (IInputOutputMutableObject currentInputOutput in inputOutput)
                {
                    IStructureReference updateTo;
                    if (updateReferences.TryGetValue(currentInputOutput.StructureReference, out updateTo))
                    {
                        currentInputOutput.StructureReference = updateTo;
                    }
                }
            }
        }

        /// <summary>
        ///     The update process steps.
        /// </summary>
        /// <param name="processSteps">
        ///     The process steps.
        /// </param>
        /// <param name="updateReferences">
        ///     The update references.
        /// </param>
        private void UpdateProcessSteps(
            IList<IProcessStepMutableObject> processSteps, 
            IDictionary<IStructureReference, IStructureReference> updateReferences)
        {
            if (processSteps != null)
            {
                foreach (IProcessStepMutableObject currentProcessStep in processSteps)
                {
                    this.UpdateProcessSteps(currentProcessStep.ProcessSteps, updateReferences);
                    this.UpdateInputOutput(currentProcessStep.Input, updateReferences);
                    this.UpdateInputOutput(currentProcessStep.Output, updateReferences);
                }
            }
        }
    }
}