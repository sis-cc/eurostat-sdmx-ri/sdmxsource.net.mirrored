// -----------------------------------------------------------------------
// <copyright file="AbstractWritingEngine.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    ///     The abstract writing engine.
    /// </summary>
    public abstract class AbstractWritingEngine : IStructureWriterEngine
    {
        /// <summary>
        ///     The output stream.
        /// </summary>
        private readonly Stream _outputStream;

        /// <summary>
        ///     controls if output should be pretty (indented and no duplicate namespaces)
        /// </summary>
        private readonly bool _prettyfy;

        /// <summary>
        ///     the schema version
        /// </summary>
        private readonly SdmxSchemaEnumType _schemaVersion;

        /// <summary>
        ///     The xml writer.
        /// </summary>
        private readonly XmlWriter _writer;

        /// <summary>
        /// The _schema location writer
        /// </summary>
        private SchemaLocationWriter _schemaLocationWriter;

        /// <summary>
        /// The _encoding
        /// </summary>
        private Encoding _encoding;


        /// <summary>
        /// The namespace update
        /// </summary>
        private readonly UpdateNamespace _namespaceUpdate = new UpdateNamespace();

        /// <summary>
        ///     Initializes a new instance of the <see cref="AbstractWritingEngine" /> class.
        /// </summary>
        /// <param name="writer">
        ///     The writer.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="writer" /> is null
        /// </exception>
        protected AbstractWritingEngine(XmlWriter writer)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            this._writer = writer;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractWritingEngine" /> class.
        /// </summary>
        /// <param name="schemaVersion">The schema version.</param>
        /// <param name="outputStream">The output stream.</param>
        /// <param name="prettyfy">controls if output should be pretty (indented and no duplicate namespaces)</param>
        /// <exception cref="ArgumentNullException"><paramref name="outputStream" /> is null</exception>
        protected AbstractWritingEngine(SdmxSchemaEnumType schemaVersion, Stream outputStream, bool prettyfy)
        {
            if (outputStream == null)
            {
                throw new ArgumentNullException("outputStream");
            }

            this._outputStream = outputStream;
            this._schemaVersion = schemaVersion;
            this._prettyfy = prettyfy;
            this._encoding = new UTF8Encoding(false); // default to UTF-8 without BOM
        }

        /// <summary>
        /// Gets or sets the encoding.
        /// </summary>
        /// <value>
        /// The encoding.
        /// </value>
        public Encoding Encoding
        {
            get
            {
                return this._encoding ?? (this._encoding = new UTF8Encoding(false));
            }

            set
            {
                this._encoding = value;
            }
        }

        /// <summary>
        ///     Sets the schema location writer.
        /// </summary>
        /// <param name="schemaLocation">The schema location.</param>
        public void SetSchemaLocationWriter(SchemaLocationWriter schemaLocation)
        {
            this._schemaLocationWriter = schemaLocation;
        }

        /// <summary>
        ///     The write structure.
        /// </summary>
        /// <param name="bean">
        ///     The bean.
        /// </param>
        public virtual void WriteStructure(IMaintainableObject bean)
        {
            ISdmxObjects beans = new SdmxObjectsImpl();
            beans.AddIdentifiable(bean);
            this.WriteStructures(beans);
        }

        /// <summary>
        ///     Writes the beans to the output location in the format specified by the implementation
        /// </summary>
        /// <param name="beans">
        ///     The SDMX Objects to write
        /// </param>
        public virtual void WriteStructures(ISdmxObjects beans)
        {
            var docV21 = this.Build(beans);
            this._namespaceUpdate.For(docV21);
            if (this._writer != null)
            {
                Write(this._writer, docV21);
            }
            else
            {
                var settings = new XmlWriterSettings() { Encoding = this.Encoding };
                if (this._schemaLocationWriter != null)
                {
                    List<string> schemaUri = new List<string>();
                    switch (this._schemaVersion)
                    {
                        case SdmxSchemaEnumType.VersionOne:
                            schemaUri.Add(SdmxConstants.MessageNs10);
                            break;
                        case SdmxSchemaEnumType.VersionTwo:
                            schemaUri.Add(SdmxConstants.MessageNs20);
                            break;
                        case SdmxSchemaEnumType.VersionTwoPointOne:
                            schemaUri.Add(SdmxConstants.MessageNs21);
                            break;
                        default:
                            throw new SdmxNotImplementedException(
                                ExceptionCode.Unsupported, 
                                "Schema Version " + this._schemaVersion);
                    }

                    this._schemaLocationWriter.WriteSchemaLocation(docV21, schemaUri.ToArray());
                }

                if (this._prettyfy)
                {
                    settings.Indent = true;
                    settings.NamespaceHandling = NamespaceHandling.OmitDuplicates;
                }

                using (XmlWriter xmlWriter = XmlWriter.Create(this._outputStream, settings))
                {
                    Write(xmlWriter, docV21);
                }

                this._outputStream.Flush();
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     Build the XSD generated class objects from the specified <paramref name="beans" />
        /// </summary>
        /// <param name="beans">
        ///     The beans.
        /// </param>
        /// <returns>
        ///     the XSD generated class objects from the specified <paramref name="beans" />
        /// </returns>
        public abstract XNode Build(ISdmxObjects beans);

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="managed"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool managed)
        {
        }

        /// <summary>
        ///     Write the <paramref name="message" /> using <paramref name="xmlWriter" />
        /// </summary>
        /// <param name="xmlWriter">
        ///     The xml writer.
        /// </param>
        /// <param name="message">
        ///     The SDMX message
        /// </param>
        private void Write(XmlWriter xmlWriter, XNode message)
        {
            if (xmlWriter.WriteState == WriteState.Start)
            {
                if (message.Document != null)
                {
                    message.Document.Save(xmlWriter);
                }
                else
                {
                    new XDocument(message).Save(xmlWriter);
                }
            }
            else
            {
                //// this is needed for NSI WS & SOAP. We get a XmlWriter 
                //// where the document has already started with SOAP envelope
                var document = message as XDocument;
                if (document != null)
                {
                    foreach (var node in document.Nodes())
                    {
                        node.WriteTo(xmlWriter);
                    }
                }
                else
                {
                    message.WriteTo(xmlWriter);
                }
            }

            xmlWriter.Flush();
        }
    }
}