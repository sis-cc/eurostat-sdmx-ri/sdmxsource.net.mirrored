// -----------------------------------------------------------------------
// <copyright file="JsonV2StructureWriterEngineSdmxCoreAdapter.cs" company="EUROSTAT">
//   Date Created : 2023-05-18
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing.Json.V2
{
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using System.IO;
    using System.Collections.Generic;
    using System;
    using Io.Sdmx.Format.Json.Engine.Structure.Writer.Sdmx.V2;
    using Org.Sdmxsource.Sdmx.Api.Engine;

    /// <summary> 
    /// Implementation of Org.Sdmxsource.Sdmx.Api.Engine for json v2. 
    /// This class internally uses SdmxJsonStructureWriterEngineV2 that is the json v2 implementation of the io.sdmx.core.sdmx.api.engine.structure.StructureWriterEngine 
    /// </summary> 
    public class JsonV2StructureWriterEngineSdmxCoreAdapter : IStructureWriterEngine
    {
        private readonly Io.Sdmx.Core.Sdmx.Api.Engine.Structure.IStructureWriterEngine _sdmxCoreStructureWriterEngine;
        private readonly Stream _outputStream;

        private readonly string selectedLanguage;

        public JsonV2StructureWriterEngineSdmxCoreAdapter(Stream outputStream, SdmxStructureJsonFormat sdmxStructureJsonFormat)
        {
            if(outputStream== null)
            {
                throw new ArgumentNullException(nameof(outputStream));
            }
            _outputStream = outputStream;
            this._sdmxCoreStructureWriterEngine = SdmxJsonStructureWriterEngineV2.Instance;
            this.selectedLanguage = sdmxStructureJsonFormat.Translator.SelectedLanguage.TwoLetterISOLanguageName;
        }

        public void WriteStructures(ISdmxObjects objects)
        {
            ISet<string> selectedLocaleSet = new HashSet<string>();
            selectedLocaleSet.Add(selectedLanguage);
            objects.SetSelectedLocales(selectedLocaleSet);
            _sdmxCoreStructureWriterEngine.WriteStructures(objects, null, _outputStream);
        }

        public void WriteStructure(IMaintainableObject sdmxObject)
        {
            _sdmxCoreStructureWriterEngine.WriteStructure(sdmxObject, null, _outputStream);
        }

        public void Dispose()
        {

        }
    }
}