// -----------------------------------------------------------------------
// <copyright file="StructureWritingEngineV3.cs" company="EUROSTAT">
//   Date Created : 2022-01-07
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Xml;
    using Io.Sdmx.Format.Ml.Api.Engine;
    using Io.Sdmx.Format.Ml.Engine.Structure.Writer.V3;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing.SdmxMLV3;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// SDMX 3.0 Streaming structure writer
    /// </summary>
    public class StructureWritingEngineV3 : IStructureWriterEngineV3
    {
        private readonly XmlWriter _writer;
        private readonly WriterConfig _config;

        // TODO: remove after adding in ISdmxArtefactWriterEngine
        public const string STR_PREFIX = "str";
        public const string COM_PREFIX = "com";
        public const string MES_PREFIX = "mes";

        public StructureWritingEngineV3(WriterConfig config)
        {
            if (config == null)
            {
                throw new ArgumentNullException(nameof(config));
            }
            if (config.GetOutputStream() == null)
            {
                throw new ArgumentNullException("OutputStream");
            }
            this._config = config;
            try
            {
                this._writer = CreateXMLStreamWriter(config.GetOutputStream());
            }
            catch (XmlException e)
            {
                throw new SdmxException(e.Message, e);
            }
        }

        /// <inheritdoc/>
        public void WriteStructures(ISdmxObjects objects, string appVersion, string appId)
        {
            if (objects == null)
            {
                throw new ArgumentNullException(nameof(objects));
            }
            string rootComment = null;
            if (ObjectUtil.ValidString(appId, appVersion))
            {
                rootComment = appId + " v" + appVersion;
            }

            try
            {
                Write(objects, rootComment);
            }
            catch (XmlException e)
            {
                throw new SdmxException(e.Message, e);
            }
        }

        /// <inheritdoc/>
        public void WriteStructures(ISdmxObjects objects)
        {
            this.WriteStructures(objects, null, null);
        }

        /// <inheritdoc/>
        public void WriteStructure(IMaintainableObject obj, string appVersion, string appId)
        {
            ISdmxObjects beans = new SdmxObjectsImpl();
            beans.AddIdentifiable(obj);
            WriteStructures(beans, appVersion, appId);
        }

        /// <inheritdoc/>
        public void WriteStructure(IMaintainableObject obj)
        {
            this.WriteStructure(obj, null, null);
        }

        /// <inheritdoc/>
        public void Close()
        {
            if (_writer != null)
            {
                try
                {
                    _writer.Close();
                }
                catch (XmlException e)
                {
                    // At least log at debug level
                    // Ignore
                }
            }
        }

        private XmlWriter CreateXMLStreamWriter(Stream writerOut)
        {
            var xmlWriterSeetings = new XmlWriterSettings
            {
                Encoding = Encoding.UTF8
            };
            XmlWriter writer = XmlWriter.Create(writerOut, xmlWriterSeetings);
            return writer;
        }

        private void Write(ISdmxObjects sdmxObjects, string rootComment)
        {
            if (sdmxObjects == null)
            {
                throw new ArgumentNullException(nameof(sdmxObjects));
            }

            var writerEngineV3 = StaxStructureWriterEngineV3.GetInstance(true, null, null,
                new CodelistArtefactWriterEngine(_config),
                new ConceptSchemeArtefactWriterEngine(_config),
                new DataflowArtefactWriterEngine(_config),
                new DsdArtefactWriterEngine(_config));
            writerEngineV3.WriteStructures(sdmxObjects, null, _config.GetOutputStream());

            /* We pass our engines to StaxStructureWriterEngineV3 so there is no need for the below
            // start the Structure document
            _writer.WriteStartDocument();
            if (ObjectUtil.ValidString(rootComment))
            {
                _writer.WriteComment(rootComment);
            }
            _writer.WriteStartElement(MES_PREFIX, "Structure", SdmxConstants.MessageNs300);
            //_writer.WriteAttributeString("xmlns", MES_PREFIX, null, SdmxConstants.MessageNs300);
            _writer.WriteAttributeString("xmlns", STR_PREFIX, null, SdmxConstants.StructureNs300);
            _writer.WriteAttributeString("xmlns", COM_PREFIX, null, SdmxConstants.CommonNs300);

            IHeader header;
            if (sdmxObjects.Header == null)
            {
                header = new HeaderImpl("IREF000001", "UNKNOWN");
            }
            else
            {
                header = sdmxObjects.Header;
            }
            HeaderWriter.Write(header, _writer);
            _writer.WriteStartElement("Structures", SdmxConstants.MessageNs300);
            // Unlike 2.1 Elements can  be written in any order
            // SDMX 3.0.0 artefact groups
            // AgencySchemes
            // Categorisations
            // CategorySchemeMaps
            // CategorySchemes
            // ConceptSchemeMaps
            // CustomTypeSchemes
            // DataConstraints
            // DataConsumerSchemes
            // DataProviderSchemes
            // GeographicCodelists
            // GeoGridCodelists
            // Hierarchies
            // HierarchyAssociations
            // MetadataConstraints
            // Metadataflows
            // MetadataProviderSchemes
            // MetadataProvisionAgreements
            // MetadataStructures
            // NamePersonalisationSchemes
            // OrganisationSchemeMaps
            // OrganisationUnitSchemes
            // Processes
            // ProvisionAgreements
            // ReportingTaxonomies
            // ReportingTaxonomyMaps
            // RepresentationMaps
            // RulesetSchemes
            // StructureMaps
            // TransformationSchemes
            // UserDefinedOperatorSchemes
            // ValueLists
            // VtlMappingSchemes
            // Dataflows
            if (sdmxObjects.HasDataflows)
            {
                Write("Dataflows", sdmxObjects.Dataflows, new DataflowArtefactWriterEngine(_config));
            }
            // Codelists
            if (sdmxObjects.HasCodelists)
            {
                Write("Codelists", sdmxObjects.Codelists, new CodelistArtefactWriterEngine(_config));
            }
            // ConceptSchemes
            if (sdmxObjects.HasConceptSchemes)
            {
                Write("ConceptSchemes", sdmxObjects.ConceptSchemes, new ConceptSchemeArtefactWriterEngine(_config));
            }
            // DataStructures
            if (sdmxObjects.HasDataStructures)
            {
                Write("DataStructures", sdmxObjects.DataStructures, new DsdArtefactWriterEngine(_config));
            }

            _writer.WriteEndElement(); // END Structures
            _writer.WriteEndElement(); // END Structure
            _writer.WriteEndDocument();
            _writer.Flush();
            */
        }

        private void Write<T>(T obj, ISdmxArtefactWriterEngine<T> writerEngine)
            where T : IMaintainableObject
        {
            if (writerEngine == null)
            {
                throw new NullReferenceException(nameof(writerEngine));
            }
            writerEngine.Write(obj, _writer);
        }

        //private void Write<T>(string artefactTypePlural, ISet<T> maintainableObjects, ISdmxArtefactWriterEngine<T> writerEngine)
        //    where T : IMaintainableObject
        //{
        //    _writer.WriteStartElement(artefactTypePlural, SdmxConstants.StructureNs300);
        //    foreach (T obj in maintainableObjects)
        //    {
        //        this.Write(obj, writerEngine);
        //    }
        //    _writer.WriteEndElement(); // END DataStructures
        //}

        public void Dispose()
        {
            Close();
            this.Dispose();
        }
    }
}
