// -----------------------------------------------------------------------
// <copyright file="StructureWriterEngineJson.cs" company="EUROSTAT">
//   Date Created : 2016-07-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;

    using log4net;

    using Api.Model.Base;
    using Api.Model.Data;
    using Api.Model.Objects.Codelist;
    using Api.Model.Objects.ConceptScheme;
    using Api.Model.Objects.MetadataStructure;
    using Api.Model.Objects.Reference;
    using Api.Model.Objects.Registry;
    using Builder.XmlSerialization.V21;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
    using Org.Sdmxsource.Json;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.Util.Date;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Codelist;

    /// <summary>
    ///     The Json V1.0 structure writing engine for SDMX.
    /// </summary>
    public class StructureWriterEngineJsonV1 : IStructureWriterEngine
    {
        /// <summary>
        /// The logger
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// The jSON generator
        /// </summary>
        private readonly JsonGenerator _jsonGenerator;

        /// <summary>
        /// The Structure jSON Format
        /// </summary>
        private readonly SdmxStructureJsonFormat _sdmxStructureJsonFormat;

        private readonly DataTypeBuilder _dataTypeBuilder = new DataTypeBuilder();

        /// <summary>
        /// Initializes a new instance of the <see cref="StructureWriterEngineJson"/> class.
        /// </summary>
        /// <param name="outputStream">
        /// The output stream.
        /// </param>
        /// <param name="sdmxStructureJsonFormat">
        /// The SDMX structure JSON format.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// outputStream
        /// or
        /// translator
        /// or
        /// encoding
        /// </exception>
        public StructureWriterEngineJsonV1(Stream outputStream, SdmxStructureJsonFormat sdmxStructureJsonFormat)
        {
            if (outputStream == null)
            {
                throw new ArgumentNullException("outputStream");
            }

            if (sdmxStructureJsonFormat == null)
            {
                throw new ArgumentNullException("sdmxStructureJsonFormat");
            }

            if (sdmxStructureJsonFormat.SdmxOutputFormat.EnumType != StructureOutputFormatEnumType.JsonV10)
            {
                throw new ArgumentException("Output format must be Json V10");
            }

            _jsonGenerator = new JsonGenerator(new StreamWriter(outputStream, new UTF8Encoding(false)));

            _sdmxStructureJsonFormat = sdmxStructureJsonFormat;
        }

        /// <summary>
        /// Writes the @maintainableObject out to the output location in the format specified by the implementation
        /// </summary>
        /// <param name="maintainableObject">
        /// The maintainableObject.
        /// </param>
        public void WriteStructure(IMaintainableObject maintainableObject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Write into Json format the sdmxObjects
        /// </summary>
        /// <param name="sdmxObjects">
        /// SDMX objects
        /// </param>
        public void WriteStructures(ISdmxObjects sdmxObjects)
        {
            if (sdmxObjects == null)
            {
                throw new ArgumentNullException("sdmxObjects");
            }

            _jsonGenerator.WriteStartObject(); // The container for the message

            var header = WriteDocumentHeader(sdmxObjects);

            _log.Debug("{data}");
            _jsonGenerator.WriteObjectFieldStart("data");

            if (sdmxObjects.Dataflows.Count > 0)
            {
                _log.Debug("{dataflows}");
                _jsonGenerator.WriteArrayFieldStart("dataflows");
                foreach (var df in sdmxObjects.Dataflows)
                {
                    WriteDataflow(df);
                }

                _log.Debug("{/dataflows}");
                _jsonGenerator.WriteEndArray();
            }

            if (sdmxObjects.AgenciesSchemes.Count > 0)
            {
                _log.Debug("{agencySchemes}");
                _jsonGenerator.WriteArrayFieldStart("agencySchemes");
                foreach (var agencieScheme in sdmxObjects.AgenciesSchemes)
                {
                    WriteAgencyScheme(agencieScheme);
                }

                _log.Debug("{/agencySchemes}");
                _jsonGenerator.WriteEndArray();
            }

            if (sdmxObjects.CategorySchemes.Count > 0)
            {
                _log.Debug("{categorySchemes}");
                _jsonGenerator.WriteArrayFieldStart("categorySchemes");
                foreach (var categoryScheme in sdmxObjects.CategorySchemes)
                {
                    WriteCategoryScheme(categoryScheme);
                }

                _log.Debug("{/categorySchemes}");
                _jsonGenerator.WriteEndArray();
            }

            if (sdmxObjects.Categorisations.Count > 0)
            {
                _log.Debug("{categorisations}");
                _jsonGenerator.WriteArrayFieldStart("categorisations");
                foreach (var categorisation in sdmxObjects.Categorisations)
                {
                    WriteCategorisation(categorisation);
                }

                _log.Debug("{/categorisations}");
                _jsonGenerator.WriteEndArray();
            }

            if (sdmxObjects.ConceptSchemes.Count > 0)
            {
                _log.Debug("{conceptSchemes}");
                _jsonGenerator.WriteArrayFieldStart("conceptSchemes");
                foreach (var conceptScheme in sdmxObjects.ConceptSchemes)
                {
                    WriteConceptScheme(conceptScheme);
                }

                _log.Debug("{/conceptSchemes}");
                _jsonGenerator.WriteEndArray();
            }

            if (sdmxObjects.Codelists.Count > 0)
            {
                _log.Debug("{codelists}");
                _jsonGenerator.WriteArrayFieldStart("codelists");
                foreach (var codelist in sdmxObjects.Codelists)
                {
                    WriteCodeList(codelist);
                }

                _log.Debug("{/codelists}");
                _jsonGenerator.WriteEndArray();
            }

            if (sdmxObjects.DataStructures.Count > 0)
            {
                _log.Debug("{dataStructures}");
                _jsonGenerator.WriteArrayFieldStart("dataStructures");
                foreach (var dsd in sdmxObjects.DataStructures)
                {
                    WriteDataStructure(dsd);
                }

                _log.Debug("{/dataStructures}");
                _jsonGenerator.WriteEndArray();
            }

            if (sdmxObjects.ContentConstraintObjects.Count > 0)
            {
                _log.Debug("{contentConstraints}");
                _jsonGenerator.WriteArrayFieldStart("contentConstraints");

                foreach (var contentConstraint in sdmxObjects.ContentConstraintObjects)
                {
                    WriteContentConstraint(contentConstraint);
                }

                _log.Debug("{/contentConstraints}");
                _jsonGenerator.WriteEndArray();
            }

            if (sdmxObjects.HierarchicalCodelists.Count > 0)
            {
                _log.Debug("{hierarchicalCodelists}");
                _jsonGenerator.WriteArrayFieldStart("hierarchicalCodelists");

                foreach (var hcl in sdmxObjects.HierarchicalCodelists)
                {
                    WriteHierarchicalCodeList(hcl);
                }

                _log.Debug("{/hierarchicalCodelists}");
                _jsonGenerator.WriteEndArray();
            }

            if (sdmxObjects.StructureSets.Count > 0)
            {
                _log.Debug("{structureSets}");
                _jsonGenerator.WriteArrayFieldStart("structureSets");

                foreach (var structureSet in sdmxObjects.StructureSets)
                {
                    WriteStructureSet(structureSet);
                }

                _log.Debug("{/structureSets}");
                _jsonGenerator.WriteEndArray();
            }

            if (sdmxObjects.MetadataStructures.Count > 0)
            {
                _log.Debug("{metadataStructures}");
                _jsonGenerator.WriteArrayFieldStart("metadataStructures");

                foreach (var metadataStructure in sdmxObjects.MetadataStructures)
                {
                    WriteMetadataStructure(metadataStructure);
                }

                _log.Debug("{/metadataStructures}");
                _jsonGenerator.WriteEndArray();
            }

            if (sdmxObjects.Metadataflows.Count > 0)
            {
                _log.Debug("{metadataflows}");
                _jsonGenerator.WriteArrayFieldStart("metadataflows");

                foreach (var metadataflow in sdmxObjects.Metadataflows)
                {
                    WriteMetadataflow(metadataflow);
                }

                _log.Debug("{/metadataflows}");
                _jsonGenerator.WriteEndArray();
            }

            if (sdmxObjects.ProvisionAgreements.Count > 0)
            {
                _log.Debug("{provisionAgreements}");
                _jsonGenerator.WriteArrayFieldStart("provisionAgreements");

                foreach (var provisionAgreement in sdmxObjects.ProvisionAgreements)
                {
                    WriteProvisionAgreement(provisionAgreement);
                }

                _log.Debug("{/provisionAgreements}");
                _jsonGenerator.WriteEndArray();
            }

            _log.Debug("{/data}");

            _jsonGenerator.WriteEndObject();

            WriteMetaTag(sdmxObjects, header);

            _jsonGenerator.Flush();
            _jsonGenerator.Close();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="managed"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool managed)
        {
            if (managed && _jsonGenerator != null)
            {
                _jsonGenerator.Close();
            }
        }

        /// <summary>
        /// Write an AgencyScheme
        /// </summary>
        /// <param name="agencyScheme">
        /// The AgencyScheme to write.
        /// </param>
        private void WriteAgencyScheme(IAgencyScheme agencyScheme)
        {
            if (agencyScheme != null)
            {
                _jsonGenerator.WriteStartObject();

                WriteIMaintainableObject(agencyScheme);

                _jsonGenerator.WriteBooleanField("isPartial", agencyScheme.Partial);

                if (agencyScheme.Items != null)
                {
                    WriteItems(agencyScheme.Items, "agencies");
                }

                _jsonGenerator.WriteEndObject();

            }
        }

        /// <summary>
        /// Write an Annotation.
        /// </summary>
        /// <param name="annotation">
        /// The Annotation to write.
        /// </param>
        private void WriteAnnotation(IAnnotation annotation)
        {
            _jsonGenerator.WriteStartObject();

            if (annotation.Id != null)
            {
                _jsonGenerator.WriteStringField("id", annotation.Id);
            }

            if (annotation.Title != null)
            {
                _jsonGenerator.WriteStringField("title", annotation.Title);
            }

            if (annotation.Type != null)
            {
                _jsonGenerator.WriteStringField("type", annotation.Type);
            }

            if (annotation.Uri != null)
            {
                //Todo: "links": {
                //"$ref": "#/definitions/links",
                //"description": "Also used to specify the Annotation URL which points to an external resource which may contain or supplement the annotation (using 'self' as relationship). If a specific behavior is desired, an annotation type should be defined which specifies the use of this field more exactly. If appropriate, a collection of links to additional external resources."
                _jsonGenerator.WriteStringField("uri", annotation.Uri.ToString());
            }

            WriteITextTypeWrapper("text", "texts", annotation.Text);

            _jsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// Write a list of Annotations.
        /// </summary>
        /// <param name="annotations">
        /// The Annotations to write.
        /// </param>
        private void WriteAnnotations(IList<IAnnotation> annotations)
        {
            if (ObjectUtil.ValidCollection(annotations))
            {
                _jsonGenerator.WriteArrayFieldStart("annotations");

                foreach (var annotation in annotations)
                {
                    WriteAnnotation(annotation);
                }

                _jsonGenerator.WriteEndArray();
            }
        }

        /// <summary>
        /// Write a CategoryScheme
        /// </summary>
        /// <param name="categoryScheme">
        /// The CategoryScheme to Write.
        /// </param>
        private void WriteCategoryScheme(ICategorySchemeObject categoryScheme)
        {
            if (categoryScheme != null)
            {
                _jsonGenerator.WriteStartObject();

                WriteIMaintainableObject(categoryScheme);

                _jsonGenerator.WriteBooleanField("isPartial", categoryScheme.Partial);

                if (categoryScheme.Items != null)
                {
                    //Category represents a set of nested categories which describe a simple classification hierarchy.
                    WriteItems(categoryScheme.Items, "categories");
                }

                _jsonGenerator.WriteEndObject();
            }
        }

        /// <summary>
        /// Write a Categorisation
        /// </summary>
        /// <param name="categorisation"></param>
        private void WriteCategorisation(ICategorisationObject categorisation)
        {
            if (categorisation != null)
            {
                _jsonGenerator.WriteStartObject();

                WriteIMaintainableObject(categorisation);

                if (categorisation.StructureReference != null)
                {
                    _jsonGenerator.WriteStringField("source", categorisation.StructureReference.TargetUrn.ToString());
                }

                if (categorisation.CategoryReference != null)
                {
                    _jsonGenerator.WriteStringField("target", categorisation.CategoryReference.TargetUrn.ToString());
                }

                _jsonGenerator.WriteEndObject();
            }
        }

        /// <summary>
        /// Write a Contact.
        /// </summary>
        /// <param name="contact">
        /// The Contact to write.
        /// </param>
        private void WriteContact(IContact contact)
        {
            if (contact != null)
            {
                _jsonGenerator.WriteStartObject();
                if (contact.Id != null)
                {
                    _jsonGenerator.WriteStringField("id", contact.Id);
                }

                WriteITextTypeWrapper("name", "names", contact.Name);
                WriteITextTypeWrapper("department", "departments", contact.Departments);
                WriteITextTypeWrapper("role", "roles", contact.Role);
                WriteArray("email", contact.Email);
                WriteArray("fax", contact.Fax);
                WriteArray("telephone", contact.Telephone);
                WriteArray("uri", contact.Uri);
                _jsonGenerator.WriteEndObject();
            }
        }

        /// <summary>
        /// Write a dataflow
        /// </summary>
        /// <param name="dataflow">
        /// The object Dataflow
        private void WriteDataflow(IDataflowObject dataflow)
        {
            _jsonGenerator.WriteStartObject();

            WriteIMaintainableObject(dataflow);

            // Write DSD Ref
            if (dataflow.DataStructureRef != null)
                _jsonGenerator.WriteStringField("structure", dataflow.DataStructureRef.TargetUrn.ToString());

            _jsonGenerator.WriteEndObject();
        }


        /// <summary>
        /// Write a ConceptScheme
        /// </summary>
        /// <param name="conceptScheme"></param>
        private void WriteConceptScheme(IConceptSchemeObject conceptScheme)
        {
            _jsonGenerator.WriteStartObject();

            WriteIMaintainableObject(conceptScheme);

            _jsonGenerator.WriteBooleanField("isPartial", conceptScheme.Partial);

            if (conceptScheme.Items != null)
            {
                WriteItems(conceptScheme.Items, "concepts");
            }

            _jsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// Write a Representation
        /// </summary>
        /// <param name="name"></param>
        /// <param name="representation"></param>
        private void WriteRepresentation(string name, IRepresentation representation)
        {
            if (representation != null)
            {
                _jsonGenerator.WriteObjectFieldStart(name);

                if (representation.Representation != null && representation.Representation.HasTargetUrn())
                {
                    _jsonGenerator.WriteStringField("enumeration", representation.Representation.TargetUrn.ToString());
                }

                //todo: What about enumerationFormat?

                WriteTextFormat(representation.TextFormat);

                _jsonGenerator.WriteEndObject();
            }
        }

        /// <summary>
        /// Write a Concept
        /// </summary>
        /// <param name="concept"></param>
        private void WriteConcept(IConceptObject concept)
        {
            WriteRepresentation("coreRepresentation", concept.CoreRepresentation);

            if (concept.IsoConceptReference != null && concept.IsoConceptReference.HasMaintainableUrn()) //todo to check
            {
                //Provides a urn reference (containing conceptSchemeID, conceptAgency, conceptID) to an ISO 11179 concept.
                _jsonGenerator.WriteStringField("isoConceptReference", concept.IsoConceptReference.MaintainableUrn.ToString());
            }

            //Urn reference to a local concept. 
            //Parent captures the semantic relationships between concepts which occur within a single concept scheme. 
            //This identifies the concept of which the current concept is a qualification (in the ISO 11179 sense) or subclass.
            if (concept.ParentConcept != null) //todo: to check!!
            {
                _jsonGenerator.WriteStringField("parent", concept.ParentConcept); //Todo: does it really be the Urn reference to a local concept. 
            }
        }

        /// <summary>
        /// Write a CodeList
        /// </summary>
        /// <param name="codeList"></param>
        private void WriteCodeList(ICodelistObject codeList)
        {
            _jsonGenerator.WriteStartObject();

            WriteIMaintainableObject(codeList);

            _jsonGenerator.WriteBooleanField("isPartial", codeList.Partial);

            if (codeList.Items != null)
            {
                WriteItems(codeList.Items, "codes");
            }

            _jsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// Write a Hierarchical Codelist
        /// </summary>
        /// <param name="hcl"></param>
        private void WriteHierarchicalCodeList(IHierarchicalCodelistObject hcl)
        {
            _jsonGenerator.WriteStartObject();

            WriteIMaintainableObject(hcl);

            if (ObjectUtil.ValidCollection(hcl.Hierarchies))
            {
                _jsonGenerator.WriteArrayFieldStart("hierarchies");
                foreach (var hierarchy in hcl.Hierarchies)
                {
                    WriteHierarchy(hierarchy);
                }
                _jsonGenerator.WriteEndArray();
            }

            if (ObjectUtil.ValidCollection(hcl.CodelistRef))
            {
                _jsonGenerator.WriteObjectFieldStart("includedCodelists");

                foreach (var codelistRef in hcl.CodelistRef)
                {
                    _jsonGenerator.WriteStringField(codelistRef.Alias, codelistRef.CodelistReference.TargetUrn.ToString());
                }

                _jsonGenerator.WriteEndObject();
            }

            _jsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// Write a StructureSet
        /// </summary>
        /// <param name="structureSet"></param>
        private void WriteStructureSet(IStructureSetObject structureSet)
        {
            _jsonGenerator.WriteStartObject();

            WriteIMaintainableObject(structureSet);

            WriteMapList(structureSet.CategorySchemeMapList, "categorySchemeMaps", "categoryMaps");
            WriteMapList(structureSet.CodelistMapList, "codelistMaps", "codeMaps");
            WriteMapList(structureSet.ConceptSchemeMapList, "conceptSchemeMaps", "conceptMaps");
            WriteMapList(structureSet.OrganisationSchemeMapList, "organisationSchemeMaps", "organisationMaps");
            WriteMapList(structureSet.StructureMapList, "structureMaps", "componentMaps");

            _jsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// Write a MetadataStructure
        /// </summary>
        /// <param name="metadataStructure"></param>
        private void WriteMetadataStructure(IMetadataStructureDefinitionObject metadataStructure)
        {
            _jsonGenerator.WriteStartObject();

            WriteIMaintainableObject(metadataStructure);

            _jsonGenerator.WriteObjectFieldStart("metadataStructureComponents");

            WriteMetadataTargets(metadataStructure.MetadataTargets);
            WriteReportStructures(metadataStructure.ReportStructures);

            _jsonGenerator.WriteEndObject();

            _jsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// Write a Metadataflow
        /// </summary>
        /// <param name="metadataflow"></param>
        private void WriteMetadataflow(IMetadataFlow metadataflow)
        {
            _jsonGenerator.WriteStartObject();

            WriteIMaintainableObject(metadataflow);

            if (metadataflow.MetadataStructureRef != null)
            {
                _jsonGenerator.WriteStringField("structure", metadataflow.MetadataStructureRef.TargetUrn.ToString());
            }

            _jsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// Write a ProvisionAgreement
        /// </summary>
        /// <param name="provisionAgreement"></param>
        private void WriteProvisionAgreement(IProvisionAgreementObject provisionAgreement)
        {
            _jsonGenerator.WriteStartObject();

            WriteIMaintainableObject(provisionAgreement);

            if (provisionAgreement.DataproviderRef != null)
            {
                _jsonGenerator.WriteStringField("dataProvider", provisionAgreement.DataproviderRef.TargetUrn.ToString());
            }

            if (provisionAgreement.StructureUseage != null)
            {
                _jsonGenerator.WriteStringField("structureUsage", provisionAgreement.StructureUseage.TargetUrn.ToString());
            }

            _jsonGenerator.WriteEndObject();
        }

        private void WriteMetadataTargets(IList<IMetadataTarget> metadataTargetList)
        {
            if (!ObjectUtil.ValidCollection(metadataTargetList))
            {
                return;
            }

            _jsonGenerator.WriteArrayFieldStart("metadataTargets");

            foreach (var metadataTarget in metadataTargetList)
            {
                _jsonGenerator.WriteStartObject();

                WriteIdentifiableObject(metadataTarget);

                if (ObjectUtil.ValidCollection(metadataTarget.IdentifiableTarget))
                {
                    _jsonGenerator.WriteArrayFieldStart("identifiableObjectTargets");

                    foreach (var identifiableTarget in metadataTarget.IdentifiableTarget)
                    {
                        _jsonGenerator.WriteStartObject();

                        WriteComponent(identifiableTarget);
                        _jsonGenerator.WriteStringField("objectType", identifiableTarget.ReferencedStructureType.UrnClass);

                        _jsonGenerator.WriteEndObject();
                    }

                    _jsonGenerator.WriteEndArray();
                }

                _jsonGenerator.WriteEndObject();
            }

            _jsonGenerator.WriteEndArray();
        }

        private void WriteReportStructures(IList<IReportStructure> reportStructureList)
        {
            if (!ObjectUtil.ValidCollection(reportStructureList))
            {
                return;
            }

            _jsonGenerator.WriteArrayFieldStart("reportStructures");

            foreach (var reportStructure in reportStructureList)
            {
                _jsonGenerator.WriteStartObject();

                WriteIdentifiableObject(reportStructure);
                WriteMetadataAttributes(reportStructure.MetadataAttributes);
                WriteTargetMetadatas(reportStructure.TargetMetadatas);

                _jsonGenerator.WriteEndObject();
            }

            _jsonGenerator.WriteEndArray();
        }

        private void WriteTargetMetadatas(IList<string> reportStructureTargetMetadatas)
        {
            if (!ObjectUtil.ValidCollection(reportStructureTargetMetadatas))
            {
                return;
            }

            _jsonGenerator.WriteArrayFieldStart("metadataTargets");

            foreach (var targetMetadata in reportStructureTargetMetadatas)
            {
                _jsonGenerator.WriteString(targetMetadata);
            }

            _jsonGenerator.WriteEndArray();
        }

        private void WriteMetadataAttributes(IList<IMetadataAttributeObject> metadataAttributeList)
        {
            if (!ObjectUtil.ValidCollection(metadataAttributeList))
            {
                return;
            }

            _jsonGenerator.WriteArrayFieldStart("metadataAttributes");

            foreach (var metadataAttribute in metadataAttributeList)
            {
                _jsonGenerator.WriteStartObject();

                WriteComponent(metadataAttribute);

                if (metadataAttribute.MaxOccurs.HasValue)
                {
                    _jsonGenerator.WriteNumberField("maxOccurs", metadataAttribute.MaxOccurs.Value);
                }

                if (metadataAttribute.MinOccurs.HasValue)
                {
                    _jsonGenerator.WriteNumberField("minOccurs", metadataAttribute.MinOccurs.Value);
                }

                if (metadataAttribute.Presentational.IsSet())
                {
                    _jsonGenerator.WriteBooleanField("isPresentational", metadataAttribute.Presentational.IsTrue);
                }

                if (ObjectUtil.ValidCollection(metadataAttribute.MetadataAttributes))
                {
                    WriteMetadataAttributes(metadataAttribute.MetadataAttributes);
                }

                _jsonGenerator.WriteEndObject();
            }

            _jsonGenerator.WriteEndArray();
        }

        private void WriteMapList<T>(IList<T> mapObjectList, string mapListName, string itemListName) where T : ISchemeMapObject
        {
            if (!ObjectUtil.ValidCollection(mapObjectList))
            {
                return;
            }

            _jsonGenerator.WriteArrayFieldStart(mapListName);

            foreach (var mapObject in mapObjectList)
            {
                _jsonGenerator.WriteStartObject();

                WriteNameableObject(mapObject);
                
                _jsonGenerator.WriteStringField("source", mapObject.SourceRef.TargetUrn.AbsoluteUri);
                _jsonGenerator.WriteStringField("target", mapObject.TargetRef.TargetUrn.AbsoluteUri);

                if (mapObject is IItemSchemeMapObject itemSchemeMapObject && ObjectUtil.ValidCollection(itemSchemeMapObject.Items))
                {
                    _jsonGenerator.WriteArrayFieldStart(itemListName);

                    foreach (var item in itemSchemeMapObject.Items)
                    {
                        _jsonGenerator.WriteStartObject();

                        _jsonGenerator.WriteStringField("source", item.SourceId);
                        _jsonGenerator.WriteStringField("target", item.TargetId);

                        _jsonGenerator.WriteEndObject();
                    }

                    _jsonGenerator.WriteEndArray();
                } 
                else if (mapObject is ICategorySchemeMapObject categorySchemeMapObject && ObjectUtil.ValidCollection(categorySchemeMapObject.CategoryMaps))
                {
                    _jsonGenerator.WriteArrayFieldStart(itemListName);

                    foreach (var item in categorySchemeMapObject.CategoryMaps)
                    {
                        for (int i = 0; i < item.SourceId.Count; i++)
                        {
                            _jsonGenerator.WriteStartObject();

                            _jsonGenerator.WriteStringField("source", item.SourceId[i]);
                            _jsonGenerator.WriteStringField("target", item.TargetId[i]);

                            _jsonGenerator.WriteEndObject();
                        }
                    }

                    _jsonGenerator.WriteEndArray();
                }
                else if (mapObject is IStructureMapObject structureMapObject && ObjectUtil.ValidCollection(structureMapObject.Components))
                {
                    _jsonGenerator.WriteBooleanField("isExtension", structureMapObject.Extension);

                    _jsonGenerator.WriteArrayFieldStart(itemListName);

                    foreach (var item in structureMapObject.Components)
                    {
                        _jsonGenerator.WriteStartObject();

                        _jsonGenerator.WriteStringField("source", item.MapConceptRef);
                        _jsonGenerator.WriteStringField("target", item.MapTargetConceptRef);

                        _jsonGenerator.WriteEndObject();
                    }

                    _jsonGenerator.WriteEndArray();
                }

                _jsonGenerator.WriteEndObject();
            }

            _jsonGenerator.WriteEndArray();
        }

        private void WriteHierarchy(IHierarchy hierarchy)
        {
            _jsonGenerator.WriteStartObject();

            WriteIdentifiableObject(hierarchy);

            WriteNameableObject(hierarchy);

            _jsonGenerator.WriteBooleanField("leveled", hierarchy.HasFormalLevels());

            _jsonGenerator.WriteArrayFieldStart("hierarchicalCodes");
            foreach (var code in hierarchy.HierarchicalCodeObjects)
            {
                WriteHierarchicalCode(code);
            }
            _jsonGenerator.WriteEndArray();

            WriteLevel(hierarchy.Level);

            _jsonGenerator.WriteEndObject();
        }

        private void WriteLevel(ILevelObject level)
        {
            if (level != null)
            {
                _jsonGenerator.WriteObjectFieldStart("level");
                
                WriteNameableObject(level);
                WriteCodingTextFormat(level.CodingFormat);
                WriteLevel(level.ChildLevel);

                _jsonGenerator.WriteEndObject();
            }
        }

        private void WriteCodingTextFormat(ITextFormat textFormat)
        {
            if (textFormat != null)
            {
                _jsonGenerator.WriteObjectFieldStart("codingFormat");

                if (textFormat.Interval > -1)
                {
                    _jsonGenerator.WriteNumberField("interval", textFormat.Interval.Value);
                }

                _jsonGenerator.WriteBooleanField("isSequence", textFormat.Sequence.IsTrue);

                if (textFormat.MaxLength > -1)
                {
                    _jsonGenerator.WriteNumberField("maxLength", textFormat.MaxLength.Value);
                }

                if (textFormat.MaxValue.HasValue)
                {
                    _jsonGenerator.WriteNumberField("maxValue", textFormat.MaxValue.Value);
                }

                if (textFormat.MinLength > -1)
                {
                    _jsonGenerator.WriteNumberField("minLength", textFormat.MinLength.Value);
                }

                if (textFormat.MinValue.HasValue)
                {
                    _jsonGenerator.WriteNumberField("minValue", textFormat.MinValue.Value);
                }

                if (textFormat.Pattern != null)
                {
                    _jsonGenerator.WriteStringField("pattern", textFormat.Pattern);
                }

                if (textFormat.StartValue < textFormat.EndValue)
                {
                    _jsonGenerator.WriteNumberField("startValue", textFormat.StartValue.Value);
                    _jsonGenerator.WriteNumberField("endValue", textFormat.EndValue.Value);
                }

                if (textFormat.TextType != null)
                {
                    _jsonGenerator.WriteStringField("textType", _dataTypeBuilder.Build(textFormat.TextType));
                }

                _jsonGenerator.WriteEndObject();
            }
        }

        private void WriteHierarchicalCode(IHierarchicalCode code)
        {
            _jsonGenerator.WriteStartObject();

            WriteIdentifiableObject(code);

            if (code.ValidFrom!=null)
            {
                _jsonGenerator.WriteStringField("validFrom", DateUtil.FormatDate(code.ValidFrom.Date, TimeFormatEnumType.DateTime));
            }

            if (code.ValidTo != null)
            {
                _jsonGenerator.WriteStringField("validTo", DateUtil.FormatDate(code.ValidTo.Date, TimeFormatEnumType.DateTime));
            }

            // version

            if (code.CodeReference?.TargetUrn != null)
            {
                _jsonGenerator.WriteStringField("code", code.CodeReference.TargetUrn.ToString());
            }

            if (!string.IsNullOrEmpty(code.CodeId))
            {
                _jsonGenerator.WriteStringField("codeID", code.CodeId);
            }

            if (!string.IsNullOrEmpty(code.CodelistAliasRef))
            {
                _jsonGenerator.WriteStringField("codelistAliasRef", code.CodelistAliasRef);
            }

            if (ObjectUtil.ValidCollection(code.CodeRefs))
            {
                _jsonGenerator.WriteArrayFieldStart("hierarchicalCodes");
                foreach (var codeRef in code.CodeRefs)
                {
                    WriteHierarchicalCode(codeRef);
                }
                _jsonGenerator.WriteEndArray();
            }

            var level = code.GetLevel(false);

            if (level?.Urn != null)
            {
                _jsonGenerator.WriteStringField("level", level.Urn.ToString());
            }

            _jsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// Write a DSD
        /// </summary>
        /// <param name="dsd"></param>
        private void WriteDataStructure(IDataStructureObject dsd)
        {
            if (dsd != null)
            {
                _jsonGenerator.WriteStartObject();

                WriteIMaintainableObject(dsd);

                _jsonGenerator.WriteObjectFieldStart("dataStructureComponents");

                WriteDataStructureAttributeList(dsd.AttributeList);
                WriteDimensionList(dsd.DimensionList);
                WriteMeasureList(dsd.MeasureList);
                WriteDataStructureGroups(dsd.Groups);

                _jsonGenerator.WriteEndObject();

                _jsonGenerator.WriteEndObject();
            }
        }

        private void WriteDataStructureAttributeList(IAttributeList attributeList)
        {
            if (attributeList != null)
            {
                _jsonGenerator.WriteObjectFieldStart("attributeList");

                WriteIdentifiableObject(attributeList);
                WriteAttributes(attributeList.Attributes);

                _jsonGenerator.WriteEndObject();
            }

        }

        private void WriteAttributes(IList<IAttributeObject> attributes)
        {
            if (attributes != null && attributes.Any())
            {
                _jsonGenerator.WriteArrayFieldStart("attributes");

                foreach (var attribute in attributes)
                {
                    _jsonGenerator.WriteStartObject();

                    WriteComponent(attribute);

                    _jsonGenerator.WriteStringField("assignmentStatus", attribute.AssignmentStatus);

                    _jsonGenerator.WriteObjectFieldStart("attributeRelationship");

                    if (attribute.AttachmentGroup != null)
                    {
                        _jsonGenerator.WriteArrayFieldStart("attachmentGroups");
                        _jsonGenerator.WriteString(attribute.AttachmentGroup);
                        _jsonGenerator.WriteEndArray();
                    }

                    if (attribute.DimensionReferences != null && attribute.DimensionReferences.Any())
                    {
                        _jsonGenerator.WriteArrayFieldStart("dimensions");

                        foreach (string dimension in attribute.DimensionReferences)
                        {
                            _jsonGenerator.WriteString(dimension);
                        }

                        _jsonGenerator.WriteEndArray();
                    }

                    if (attribute.PrimaryMeasureReference != null)
                    {
                        _jsonGenerator.WriteStringField("primaryMeasure", attribute.PrimaryMeasureReference);
                    }

                    //todo: What about: attribute.AttachmentLevel ?

                    //Todo: group //Urn reference to a local GroupKey Descriptor. This is used as a convenience to referencing all of the dimension defined by the referenced group. The attribute will also be attached to this group.
                    //_jsonGenerator.WriteStringField("group", null);
                    //Todo:  none //This means that value of the attribute will not vary with any of the other data structure components. This will always be treated as a data set level attribute.
                    //_jsonGenerator.WriteObjectFieldStart("none");
                    //_jsonGenerator.WriteEndObject();

                    _jsonGenerator.WriteEndObject();

                    _jsonGenerator.WriteEndObject();
                }

                _jsonGenerator.WriteEndArray();
            }
        }


        /// <summary>
        /// Write a Data Structure Groups
        /// </summary>
        /// <param name="groups"></param>
        private void WriteDataStructureGroups(IList<IGroup> groups)
        {
            if (groups != null && groups.Any())
            {
                _jsonGenerator.WriteArrayFieldStart("groups");

                foreach (var group in groups)
                {
                    _jsonGenerator.WriteStartObject();
                    WriteIdentifiableObject(group);
                    if (group.DimensionRefs != null && group.DimensionRefs.Any())
                    {
                        _jsonGenerator.WriteArrayFieldStart("groupDimensions");

                        foreach (string groupDimension in group.DimensionRefs)
                        {
                            _jsonGenerator.WriteString(groupDimension);
                        }

                        _jsonGenerator.WriteEndArray();
                    }

                    if (group.AttachmentConstraintRef != null)
                    {
                        _jsonGenerator.WriteStringField("attachmentConstraint", group.AttachmentConstraintRef.TargetUrn.ToString());
                    }

                    _jsonGenerator.WriteEndObject();
                }

                _jsonGenerator.WriteEndArray();


            }
        }

        /// <summary>
        /// Write a DimensionList
        /// </summary>
        /// <param name="dimensionList"></param>
        private void WriteDimensionList(IDimensionList dimensionList)
        {
            if (dimensionList != null)
            {
                _jsonGenerator.WriteObjectFieldStart("dimensionList");
                WriteIdentifiableObject(dimensionList);

                WriteDimension(dimensionList.Dimensions, "dimensions", SdmxStructureEnumType.Dimension);
                WriteDimension(dimensionList.Dimensions, "measureDimensions", SdmxStructureEnumType.MeasureDimension);
                WriteDimension(dimensionList.Dimensions, "timeDimensions", SdmxStructureEnumType.TimeDimension);

                _jsonGenerator.WriteEndObject();
            }
        }

        /// <summary>
        /// Write a MeasureList
        /// </summary>
        /// <param name="measureList"></param>
        private void WriteMeasureList(IMeasureList measureList)
        {
            if (measureList != null)
            {
                _jsonGenerator.WriteObjectFieldStart("measureList");

                WriteIdentifiableObject(measureList);

                _jsonGenerator.WriteObjectFieldStart("primaryMeasure");

                WriteComponent(measureList.PrimaryMeasure);

                _jsonGenerator.WriteEndObject();

                _jsonGenerator.WriteEndObject();
            }
        }

        /// <summary>
        /// Write a Dimension according to its type
        /// </summary>
        /// <param name="dimensions"></param>
        /// <param name="typeName"></param>
        /// <param name="sdmxStructureEnumType"></param>
        private void WriteDimension(IList<IDimension> dimensions, string typeName, SdmxStructureEnumType sdmxStructureEnumType)
        {
            if (dimensions != null)
            {
                var typedDimensions = dimensions.Where(d => d.StructureType.EnumType == sdmxStructureEnumType);

                if (typedDimensions.Any())
                {
                    _jsonGenerator.WriteArrayFieldStart(typeName);

                    foreach (var dimension in typedDimensions)
                    {
                        _jsonGenerator.WriteStartObject();
                        WriteComponent(dimension);
                        _jsonGenerator.WriteEndObject();
                    }

                    _jsonGenerator.WriteEndArray();
                }
            }
        }

        private void WriteComponent(IComponent component)
        {
            if (component != null)
            {
                WriteIdentifiableObject(component);

                if (component is IDimension)
                {
                    var dimension = component as IDimension;

                    // Note that a Dimensions' position is 1 indexed, but we need it 0-indexed, so subtract 1
                    _jsonGenerator.WriteNumberField("position", dimension.Position - 1);
                    _jsonGenerator.WriteStringField("type", component.StructureType.UrnClass);

                    WriteConceptRoles(dimension.ConceptRole);
                }
                else if (component is IAttributeObject)
                {
                    var attribute = component as IAttributeObject;

                    WriteConceptRoles(attribute.ConceptRoles);
                }

                if (component.ConceptRef != null)
                {
                    _jsonGenerator.WriteStringField("conceptIdentity", component.ConceptRef.TargetUrn.ToString());
                }

                //todo: What about HasCodedRepresentation ??
                WriteRepresentation("localRepresentation", component.Representation);
                WriteAnnotations(component.Annotations);
            }
        }

        /// <summary>
        /// Write a Content constraint
        /// </summary>
        /// <param name="dsd"></param>
        private void WriteContentConstraint(IContentConstraintObject contentConstraint)
        {
            if (contentConstraint == null)
            {
                return;
            }

            _jsonGenerator.WriteStartObject();

            //Write top level information
            WriteIMaintainableObject(contentConstraint, contentConstraint.IsDefiningActualDataPresent);

            _jsonGenerator.WriteStringField("type", contentConstraint.IsDefiningActualDataPresent == true ? "Actual" : "Allowed");

            //Write constraintAttachment
            WriteConstraintAttachment(contentConstraint.ConstraintAttachment);

            //Write cube regions
            if ((contentConstraint.IncludedCubeRegion ?? contentConstraint.ExcludedCubeRegion) != null)
            {
                _jsonGenerator.WriteArrayFieldStart("cubeRegions");

                //TODO: Fix potential but, the CubRegion is always set the Include, event though the include flag in the SDMX ML is set to false 
                WriteCubeRegion(contentConstraint.IncludedCubeRegion, true);
                WriteCubeRegion(contentConstraint.ExcludedCubeRegion, false);

                _jsonGenerator.WriteEndArray();
            }

            //Write dataKeySets
            if ((contentConstraint.IncludedSeriesKeys ?? contentConstraint.ExcludedSeriesKeys) != null)
            {
                _jsonGenerator.WriteArrayFieldStart("dataKeySets");

                WriteDataKeySet(contentConstraint.IncludedSeriesKeys?.ConstrainedDataKeys, true);
                WriteDataKeySet(contentConstraint.ExcludedSeriesKeys?.ConstrainedDataKeys, false);

                _jsonGenerator.WriteEndArray();
            }

            //Write metadataKeySets
            if ((contentConstraint.IncludedMetadataKeys ?? contentConstraint.ExcludedMetadataKeys) != null)
            {
                _jsonGenerator.WriteArrayFieldStart("metadataKeySets");

                WriteMetadataKeySet(contentConstraint.IncludedMetadataKeys?.ConstrainedDataKeys, true, contentConstraint.MetadataTargetRegion);
                WriteMetadataKeySet(contentConstraint.ExcludedMetadataKeys?.ConstrainedDataKeys, false, contentConstraint.MetadataTargetRegion);

                _jsonGenerator.WriteEndArray();
            }

            //Write metadata target region
            WriteMetadataTargetRegion(contentConstraint.MetadataTargetRegion);

            _jsonGenerator.WriteEndObject();
        }

        private void WriteConstraintAttachment(IConstraintAttachment constraintAttachment)
        {
            if (constraintAttachment == null)
                return;

            _jsonGenerator.WriteObjectFieldStart("constraintAttachment");

            //Write all dataProvider attachments
            ICrossReference dataProvider = constraintAttachment.StructureReference.FirstOrDefault(x => x.TargetReference.EnumType == SdmxStructureEnumType.DataProvider);
            if (dataProvider != null)
            {
                _jsonGenerator.WriteStringField("dataProvider", dataProvider.TargetUrn.AbsoluteUri);
            }

            //Write all dataSets attachments
            //Write all metadataSet attachments
            var dataSetReference = constraintAttachment.DataOrMetadataSetReference;

            if (dataSetReference != null)
            {
                WriteDataSetReference(dataSetReference.IsDataSetReference ? "dataSets" : "metadataSets", dataSetReference);
            }

            //Write all dataStructure attachments
            IEnumerable<ICrossReference> dataStructures = constraintAttachment.StructureReference.Where(x => x.TargetReference.EnumType == SdmxStructureEnumType.Dsd);
            if (dataStructures.Any())
            {
                _jsonGenerator.WriteArrayFieldStart("dataStructures");

                foreach (var ds in dataStructures)
                {
                    _jsonGenerator.WriteString(ds.TargetUrn.AbsoluteUri);
                }

                _jsonGenerator.WriteEndArray();
            }

            //Write all dataflow attachments
            IEnumerable<ICrossReference> dataflows = constraintAttachment.StructureReference.Where(x => x.TargetReference.EnumType == SdmxStructureEnumType.Dataflow);

            if (dataflows.Any())
            {
                _jsonGenerator.WriteArrayFieldStart("dataflows");

                foreach (var df in dataflows)
                {
                    _jsonGenerator.WriteString(df.TargetUrn.AbsoluteUri);
                }

                _jsonGenerator.WriteEndArray();
            }

            //Write all metadataStructure attachments
            IEnumerable<ICrossReference> metadataStructure = constraintAttachment.StructureReference.Where(x => x.TargetReference.EnumType == SdmxStructureEnumType.MetadataDocument); //TODO: check if MetadataDocument is right here
            if (metadataStructure.Any())
            {
                _jsonGenerator.WriteArrayFieldStart("metadataStructures");

                foreach (var mds in metadataStructure)
                {
                    _jsonGenerator.WriteString(mds.TargetUrn.AbsoluteUri);
                }

                _jsonGenerator.WriteEndArray();
            }

            //Write all metadataflow attachments
            IEnumerable<ICrossReference> metadataFlows = constraintAttachment.StructureReference.Where(x => x.TargetReference.EnumType == SdmxStructureEnumType.MetadataFlow);
            if (metadataFlows.Any())
            {
                _jsonGenerator.WriteArrayFieldStart("metadataflows");

                foreach (var metadataFlow in metadataFlows)
                {
                    _jsonGenerator.WriteString(metadataFlow.TargetUrn.AbsoluteUri);
                }

                _jsonGenerator.WriteEndArray();
            }

            //Write all provisionAgreement attachments
            IEnumerable<ICrossReference> provisionAgreements = constraintAttachment.StructureReference.Where(x => x.TargetReference.EnumType == SdmxStructureEnumType.ProvisionAgreement);
            if (provisionAgreements.Any())
            {
                _jsonGenerator.WriteArrayFieldStart("metadataflows");

                foreach (var provisionAgreement in provisionAgreements)
                {
                    _jsonGenerator.WriteString(provisionAgreement.TargetUrn.AbsoluteUri);
                }

                _jsonGenerator.WriteEndArray();
            }

            //TODO: Write all queryableDataSource attachments

            //TODO: Write all simpleDataSource attachments

            _jsonGenerator.WriteEndObject();
        }

        private void WriteDataSetReference(string propertyName, IDataAndMetadataSetReference dataSetReference)
        {
            _jsonGenerator.WriteArrayFieldStart(propertyName);
            _jsonGenerator.WriteStartObject();

            _jsonGenerator.WriteStringField("dataProvider", ""); //TODO: Locate source of this element

            _jsonGenerator.WriteStringField("id", dataSetReference.SetId);

            _jsonGenerator.WriteEndObject();
            _jsonGenerator.WriteEndArray();
        }

        private void WriteCubeRegion(ICubeRegion cubeRegion, bool isIncluded)
        {
            if (cubeRegion != null)
            {
                _jsonGenerator.WriteStartObject();

                _jsonGenerator.WriteBooleanField("isIncluded", isIncluded);

                WriteValueSet("attributes", cubeRegion.AttributeValues);

                WriteValueSet("keyValues", cubeRegion.KeyValues);

                _jsonGenerator.WriteEndObject();
            }
        }

        private void WriteValueSet(string propertyName, IEnumerable<IKeyValues> keyValues)
        {
            if (keyValues != null && keyValues.Any())
            {
                _jsonGenerator.WriteArrayFieldStart(propertyName);

                foreach (var cubeRegionKey in keyValues)
                {
                    _jsonGenerator.WriteStartObject();
                    _jsonGenerator.WriteStringField("id", cubeRegionKey.Id);

                    WriteTimeRangeValue(cubeRegionKey.TimeRange);

                    WriteValues("values", cubeRegionKey.Values);

                    WriteValues("cascadeValues", cubeRegionKey.CascadeValues);

                    _jsonGenerator.WriteEndObject();
                }

                _jsonGenerator.WriteEndArray();
            }
        }

        private void WriteValues(string propertyName, IEnumerable<string> values)
        {
            var valuesArray = values == null ? null : values as string[] ?? values.ToArray();
            if (valuesArray == null || !valuesArray.Any())
            {
                return;
            }

            _jsonGenerator.WriteArrayFieldStart(propertyName);

            foreach (var value in valuesArray)
            {
                _jsonGenerator.WriteString(value);
            }

            _jsonGenerator.WriteEndArray();
        }

        private void WriteTimeRangeValue(ITimeRange timeRange)
        {
            if (timeRange == null)
            {
                return;
            }

            _jsonGenerator.WriteObjectFieldStart("timeRange");

            if (timeRange.StartDate != null)
            {
                WriteTimePeriodRange(timeRange.Range ? "startPeriod" : "afterPeriod", timeRange.StartDate, timeRange.StartInclusive);
            }

            if (timeRange.EndDate != null)
            {
                WriteTimePeriodRange(timeRange.Range ? "endPeriod" : "beforePeriod", timeRange.EndDate, timeRange.EndInclusive);
            }

            _jsonGenerator.WriteEndObject();

        }

        private void WriteTimePeriodRange(string propertyName, ISdmxDate date, bool isInclusive)
        {
            _jsonGenerator.WriteObjectFieldStart(propertyName);

            WriteDateProperty("period", date.Date, false);
            _jsonGenerator.WriteBooleanField("isInclusive", isInclusive);

            _jsonGenerator.WriteEndObject();
        }

        private void WriteDataKeySet(IList<IConstrainedDataKey> dataKeySet, bool isIncluded)
        {
            if (dataKeySet == null || !dataKeySet.Any())
                return;

            _jsonGenerator.WriteStartObject();
            _jsonGenerator.WriteBooleanField("isIncluded", isIncluded);

            _jsonGenerator.WriteArrayFieldStart("keys");
            foreach (var constrainedDataKey in dataKeySet)
            {
                WriteDataKey(constrainedDataKey);
            }
            _jsonGenerator.WriteEndArray();

            _jsonGenerator.WriteEndObject();
        }

        private void WriteDataKey(IConstrainedDataKey constrainedDataKey)
        {
            _jsonGenerator.WriteStartObject();
            _jsonGenerator.WriteArrayFieldStart("keyValues");

            foreach (var keyValue in constrainedDataKey.KeyValues)
            {
                WriteDataKeyValue(keyValue);
            }

            _jsonGenerator.WriteEndArray();
            _jsonGenerator.WriteEndObject();
        }

        private void WriteDataKeyValue(IKeyValue keyValue)
        {
            _jsonGenerator.WriteStartObject();

            _jsonGenerator.WriteStringField("id", keyValue.Concept);
            _jsonGenerator.WriteStringField("value", keyValue.Code);

            _jsonGenerator.WriteEndObject();
        }

        private void WriteMetadataKeySet(IList<IConstrainedDataKey> dataKeySet, bool isIncluded, IMetadataTargetRegion targetRegion)
        {
            if (dataKeySet == null || !dataKeySet.Any())
                return;

            _jsonGenerator.WriteStartObject();
            _jsonGenerator.WriteBooleanField("isIncluded", isIncluded);

            // --- start array -----

            _jsonGenerator.WriteArrayFieldStart("keys");

            foreach (var constrainedDataKey in dataKeySet)
            {
                _jsonGenerator.WriteStartObject();

                _jsonGenerator.WriteStringField("metadataTarget", targetRegion.MetadataTarget);
                _jsonGenerator.WriteStringField("report", targetRegion.Report);

                WriteMetadataKeyValues(constrainedDataKey.KeyValues);

                _jsonGenerator.WriteEndObject();
            }

            _jsonGenerator.WriteEndArray();

            // --- end array -----

            _jsonGenerator.WriteEndObject();
        }

        private void WriteMetadataKeyValues(IList<IKeyValue> keyValues)
        {
            if (keyValues == null || !keyValues.Any())
                return;

            // --- start array -----

            _jsonGenerator.WriteArrayFieldStart("keyValues");

            foreach (var keyValue in keyValues)
            {
                _jsonGenerator.WriteStartObject();

                _jsonGenerator.WriteStringField("id", keyValue.Concept);

                _jsonGenerator.WriteArrayFieldStart("dataKeys");
                //TODO: Locate source of this element
                _jsonGenerator.WriteEndArray();

                _jsonGenerator.WriteArrayFieldStart("dataSets");
                //TODO: Locate source of this element
                _jsonGenerator.WriteEndArray();

                _jsonGenerator.WriteStringField("object", ""); //TODO: Locate source of this element
                _jsonGenerator.WriteStringField("value", keyValue.Code);

                _jsonGenerator.WriteEndObject();
            }

            _jsonGenerator.WriteEndArray();

            // --- end array -----
        }

        private void WriteMetadataTargetRegion(IMetadataTargetRegion targetRegion)
        {
            if (targetRegion == null)
            {
                return;
            }

            _jsonGenerator.WriteArrayFieldStart("metadataTargetRegions");

            _jsonGenerator.WriteStartObject();

            _jsonGenerator.WriteBooleanField("include", targetRegion.IsInclude);

            _jsonGenerator.WriteStringField("metadataTarget", targetRegion.MetadataTarget);

            _jsonGenerator.WriteStringField("report", targetRegion.Report);

            WriteValueSet("attributes", targetRegion.Attributes);

            if (targetRegion.Key != null && targetRegion.Key.Any())
            {
                _jsonGenerator.WriteArrayFieldStart("keyValues");

                foreach (var metadataTargetRegionKey in targetRegion.Key)
                {
                    WriteMetadataTargetRegionKey(metadataTargetRegionKey);
                }

                _jsonGenerator.WriteEndArray();
            }

            _jsonGenerator.WriteEndObject();

            _jsonGenerator.WriteEndArray();

        }

        private void WriteMetadataTargetRegionKey(IMetadataTargetKeyValues metadataTargetRegionKey)
        {
            if (metadataTargetRegionKey == null)
            {
                return;
            }

            _jsonGenerator.WriteStartObject();

            _jsonGenerator.WriteStringField("id", metadataTargetRegionKey.Id);

            _jsonGenerator.WriteArrayFieldStart("dataKeys");
            //TODO:
            _jsonGenerator.WriteEndArray();

            _jsonGenerator.WriteArrayFieldStart("dataSets");
            //TODO:
            _jsonGenerator.WriteEndArray();

            if (metadataTargetRegionKey.ObjectReferences.Any())
            {
                WriteValues("objects", metadataTargetRegionKey.ObjectReferences.Select(or => or.TargetUrn?.AbsoluteUri));
            }

            WriteTimeRangeValue(metadataTargetRegionKey.TimeRange);

            WriteValues("values", metadataTargetRegionKey.Values);

            _jsonGenerator.WriteEndObject();

        }

        /// <summary>
        /// Write ConceptRoles
        /// </summary>
        /// <param name="conceptRoles"></param>
        private void WriteConceptRoles(IList<ICrossReference> conceptRoles)
        {
            if (conceptRoles != null && conceptRoles.Count > 0)
            {
                _jsonGenerator.WriteArrayFieldStart("conceptRoles");
                foreach (var conceptRole in conceptRoles)
                {
                    _jsonGenerator.WriteString(conceptRole.TargetUrn.ToString());
                }

                _jsonGenerator.WriteEndArray();
            }
        }

        /// <summary>
        /// Write a TextFormat
        /// </summary>
        /// <param name="textFormat"></param>
        private void WriteTextFormat(ITextFormat textFormat)
        {
            if (textFormat != null)
            {
                _jsonGenerator.WriteObjectFieldStart("textFormat");

                if (textFormat.TextType != null)
                {
                    _jsonGenerator.WriteStringField("textType", _dataTypeBuilder.Build(textFormat.TextType));
                }

                if (textFormat.Decimals > -1)
                {
                    _jsonGenerator.WriteNumberField("decimals", textFormat.Decimals.Value);
                }

                if (textFormat.StartValue < textFormat.EndValue)
                {
                    _jsonGenerator.WriteNumberField("startValue", textFormat.StartValue.Value);
                    _jsonGenerator.WriteNumberField("endValue", textFormat.EndValue.Value);
                }

                if (textFormat.Interval > -1)
                {
                    _jsonGenerator.WriteNumberField("interval", textFormat.Interval.Value);
                }

                _jsonGenerator.WriteBooleanField("isSequence", textFormat.Sequence.IsTrue);

                if (textFormat.MaxLength > -1)
                {
                    _jsonGenerator.WriteNumberField("maxLength", textFormat.MaxLength.Value);
                }

                if (textFormat.MinLength > -1)
                {
                    _jsonGenerator.WriteNumberField("minLength", textFormat.MinLength.Value);
                }

                if (textFormat.Pattern != null)
                {
                    _jsonGenerator.WriteStringField("pattern", textFormat.Pattern);
                }

                if (textFormat.MinValue.HasValue)
                {
                    _jsonGenerator.WriteNumberField("minValue", textFormat.MinValue.Value);
                }

                if (textFormat.MaxValue.HasValue)
                {
                    _jsonGenerator.WriteNumberField("maxValue", textFormat.MaxValue.Value);
                }

                if (textFormat.StartTime != null)
                {
                    WriteDateProperty("startTime", textFormat.StartTime.Date, false);
                }
                if (textFormat.EndTime != null)
                {
                    WriteDateProperty("endTime", textFormat.EndTime.Date, false);
                }

                if (textFormat.TimeInterval != null)
                {
                    _jsonGenerator.WriteStringField("timeInterval", textFormat.TimeInterval);
                }

                _jsonGenerator.WriteBooleanField("isMultiLingual", textFormat.Multilingual.IsTrue);

                _jsonGenerator.WriteEndObject();
            }
        }

        /// <summary>
        /// Write a DateTime into a given property.
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="dateTime"></param>
        /// <param name="convertToUtc"></param>
        private void WriteDateProperty(string propertyName, DateTime? dateTime, bool convertToUtc)
        {
            _jsonGenerator.WritePropertyName(propertyName);
            if (dateTime.HasValue)
            {
                _jsonGenerator.WriteString(convertToUtc ? DateUtil.FormatDateUTC(dateTime.Value) : DateUtil.FormatDate(dateTime.Value, TimeFormatEnumType.DateTime));
            }
            else
            {
                _jsonGenerator.WriteNull();
            }
        }

        /// <summary>
        /// Write the document header
        /// </summary>
        /// <param name="beans">
        /// The Header to write.
        /// </param>
        private StructureHeaderType WriteDocumentHeader(ISdmxObjects beans)
        {
            //IHeader header = beans.Header;

            var headerType = new Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.StructureHeaderType();
            Builder.XmlSerialization.Registry.Response.V21.V21Helper.SetHeader(headerType, beans);

            return headerType;
        }

        private void WriteMetaTag(ISdmxObjects beans, StructureHeaderType headerType)
        {
            _log.Debug("{meta}");

            _jsonGenerator.WriteObjectFieldStart("meta");
            _jsonGenerator.WriteStringField("schema", SdmxConstants.StructureJsonV10);

            _jsonGenerator.WriteArrayFieldStart("contentLanguages");

            foreach (var lang in _sdmxStructureJsonFormat.Translator.SelectedLanguages)
            {
                _jsonGenerator.WriteString(lang);
            }

            _jsonGenerator.WriteEndArray();

            _jsonGenerator.WriteStringField("id", headerType.ID);
            WriteDateProperty("prepared", DateTime.Parse(headerType.Prepared.ToString()), true);
            _jsonGenerator.WriteBooleanField("test", headerType.Test);

            WriteSender(new PartyCore(headerType.Sender));

            WriteReceivers(headerType.Receiver.Select(r => (IParty)new PartyCore(r)).ToList());

            _jsonGenerator.WriteEndObject();
            _log.Debug("{/meta}");
        }




        /// <summary>
        /// Write an Identifiable Object
        /// </summary>
        /// <param name="identifiableObject"></param>
        private void WriteIdentifiableObject(IIdentifiableObject identifiableObject)
        {
            _jsonGenerator.WriteStringField("id", identifiableObject.Id);

            if (identifiableObject.Uri != null)
            {
                _jsonGenerator.WriteStringField("uri", identifiableObject.Uri.ToString());
            }

            WriteLinks(identifiableObject);
        }

        /// <summary>
        /// Write a MaintainableObject as base for all Artefact.
        /// </summary>
        /// <param name="maintainableObject">
        /// The MaintainableObject to write.
        /// </param>
        /// <param name="writeValidityAsUTC"></param>
        private void WriteIMaintainableObject(IMaintainableObject maintainableObject, bool writeValidityAsUTC = false)
        {
            if (maintainableObject != null)
            {
                WriteIdentifiableObject(maintainableObject);

                if (maintainableObject.Version != null)
                {
                    _jsonGenerator.WriteStringField("version", maintainableObject.Version);
                }

                if (maintainableObject.AgencyId != null)
                {
                    _jsonGenerator.WriteStringField("agencyID", maintainableObject.AgencyId);
                }

                if (maintainableObject.IsExternalReference != null && maintainableObject.IsExternalReference.IsSet())
                {
                    _jsonGenerator.WriteBooleanField("isExternalReference", maintainableObject.IsExternalReference.IsTrue);
                }

                if (maintainableObject.IsFinal != null && maintainableObject.IsFinal.IsSet()) //NB: When the value is defined to false, then it is not set, this may be an issue while parsing.
                {
                    _jsonGenerator.WriteBooleanField("isFinal", maintainableObject.IsFinal.IsTrue);
                }

                if (maintainableObject.StartDate != null)
                {
                    _jsonGenerator.WriteStringField(
                        "validFrom",
                        writeValidityAsUTC ?
                        DateUtil.FormatDateUTC(maintainableObject.StartDate.Date) :
                        DateUtil.FormatDate(maintainableObject.StartDate.Date, TimeFormatEnumType.DateTime));
                }

                if (maintainableObject.EndDate != null)
                {
                    _jsonGenerator.WriteStringField(
                        "validTo",
                        writeValidityAsUTC ?
                        DateUtil.FormatDateUTC(maintainableObject.EndDate.Date) :
                        DateUtil.FormatDate(maintainableObject.EndDate.Date, TimeFormatEnumType.DateTime));
                }

                WriteNameableObject(maintainableObject);
                WriteAnnotations(maintainableObject.Annotations);
            }
        }

        /// <summary>
        /// Write links.
        /// </summary>
        /// <param name="identifiableObject">
        /// The IdentifiableObject to write.
        /// </param>
        private void WriteLinks(IIdentifiableObject identifiableObject)
        {
            var crossReferences = identifiableObject.CrossReferences
                .Where(reference => reference.TargetReference.EnumType == SdmxStructureEnumType.Dsd || reference.TargetReference.EnumType == SdmxStructureEnumType.Msd)
                .ToList();

            var maintainableObject = identifiableObject as IMaintainableObject;

            var shouldWriteSelfReference = HeaderScope.WriteUrn;
            var shouldWriteCrossReferences = HeaderScope.WriteUrn && crossReferences.Any();
            var shouldWriteExternalReference = maintainableObject?.IsExternalReference != null && maintainableObject.IsExternalReference.IsTrue;

            if (!shouldWriteSelfReference && !shouldWriteExternalReference && !shouldWriteCrossReferences)
            {
                return;
            }

            _jsonGenerator.WriteArrayFieldStart("links");

            if (shouldWriteSelfReference)
            {
                _jsonGenerator.WriteStartObject();
                _jsonGenerator.WriteStringField("rel", "self");
                _jsonGenerator.WriteStringField("urn", identifiableObject.Urn.ToString());
                _jsonGenerator.WriteEndObject();
            }

            if (shouldWriteExternalReference)
            {
                _jsonGenerator.WriteStartObject();
                _jsonGenerator.WriteStringField("href", maintainableObject.StructureUrl?.ToString());
                _jsonGenerator.WriteStringField("rel", "external");

                if (HeaderScope.WriteUrn)
                {
                    _jsonGenerator.WriteStringField("urn", maintainableObject.Urn.ToString());
                }

                _jsonGenerator.WriteEndObject();
            }

            if (shouldWriteCrossReferences)
            {
                foreach (var reference in crossReferences)
                {
                    _jsonGenerator.WriteStartObject();
                    _jsonGenerator.WriteStringField("rel", "structure");
                    _jsonGenerator.WriteStringField("urn", reference.TargetUrn.ToString());
                    _jsonGenerator.WriteEndObject();
                }
            }

            _jsonGenerator.WriteEndArray();
        }

        /// <summary>
        /// Write an array of strings
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="list"></param>
        private void WriteArray(string propertyName, IList<string> list)
        {
            if (list != null && propertyName != null && list.Count > 0)
            {
                _jsonGenerator.WriteArrayFieldStart(propertyName);

                foreach (var item in list)
                {
                    _jsonGenerator.WriteString(item);
                }

                _jsonGenerator.WriteEndArray();
            }
        }

        /// <summary>
        /// Write a list of TextTypeWrapper
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="arrayPropertyName"></param>
        /// <param name="textTypeWrapper"></param>
        /// <param name="provideAtLeastOneItem"></param>
        private void WriteITextTypeWrapper(string propertyName, string arrayPropertyName, IList<ITextTypeWrapper> textTypeWrapper, bool provideAtLeastOneItem = false)
        {
            if (textTypeWrapper != null && !string.IsNullOrWhiteSpace(propertyName) && !string.IsNullOrWhiteSpace(arrayPropertyName) && textTypeWrapper.Count > 0)
            {
                IList<ITextTypeWrapper> preferredTranslations = _sdmxStructureJsonFormat.Translator.GetSelectedTextInPreferredLanguages(textTypeWrapper, provideAtLeastOneItem);

                if (preferredTranslations == null || !preferredTranslations.Any())
                {
                    return;
                }

                _jsonGenerator.WriteStringField(propertyName, preferredTranslations.First().Value);

                _jsonGenerator.WriteObjectFieldStart(arrayPropertyName);

                if (preferredTranslations != null && preferredTranslations.Count > 0)
                {
                    foreach (var localizedText in preferredTranslations)
                    {
                        _jsonGenerator.WriteStringField(localizedText.Locale, localizedText.Value);
                    }
                }

                _jsonGenerator.WriteEndObject();
            }
        }

        /// <summary>
        /// Write a Nameable Object
        /// </summary>
        /// <param name="nameableObject"></param>
        private void WriteNameableObject(INameableObject nameableObject)
        {
            if (nameableObject != null)
            {
                // Nameable objects expected to have name property, so at least one translation is request even if there are no translation for preferred languages.
                WriteITextTypeWrapper("name", "names", nameableObject.Names, true);
                WriteITextTypeWrapper("description", "descriptions", nameableObject.Descriptions);
            }
        }

        /// <summary>
        /// Write a list of Items.
        /// </summary>
        /// <typeparam name="T">
        /// Type of the Item.
        /// </typeparam>
        /// <param name="items">
        /// The Item to write.
        /// </param>
        /// /// <param name="type">
        /// The Item type to write.
        /// </param>
        private void WriteItems<T>(IList<T> items, string type) where T : IItemObject
        {
            if (items.Count > 0)
            {
                _jsonGenerator.WriteArrayFieldStart(type);

                foreach (var item in items)
                {
                    _jsonGenerator.WriteStartObject();

                    if (item.Id != null)
                    {
                        _jsonGenerator.WriteStringField("id", item.Id);
                    }

                    WriteNameableObject(item);
                    WriteAnnotations(item.Annotations);
                    WriteLinks(item);

                    var organisation = item as IOrganisation;
                    if (organisation != null && organisation.Contacts.Count > 0)
                    {
                        _jsonGenerator.WriteArrayFieldStart("contact");
                        foreach (var contact in organisation.Contacts)
                        {
                            WriteContact(contact);
                        }
                        _jsonGenerator.WriteEndArray();
                    }

                    var concept = item as IConceptObject;
                    if (concept != null)
                    {
                        WriteConcept(concept);
                    }

                    var code = item as ICode;
                    if (code != null && code.ParentCode != null)
                    {
                        _jsonGenerator.WriteStringField("parent", code.ParentCode);
                    }

                    var hierarchy = item as IHierarchicalItemObject<T>;
                    //codes take too long to 
                    if (code == null)
                    {
                        if (hierarchy != null && hierarchy.Items != null && hierarchy.Items.Count > 0)
                        {
                            WriteItems(hierarchy.Items, type);
                        }
                    }
                    

                    _jsonGenerator.WriteEndObject();
                }

                _jsonGenerator.WriteEndArray();
            }
        }

        /// <summary>
        /// Write a Party.
        /// </summary>
        /// <param name="partyObject">
        /// The Party to write.
        /// </param>
        private void WriteParty(IParty partyObject)
        {
            if (partyObject != null)
            {
                if (!string.IsNullOrEmpty(partyObject.Id))
                {
                    _jsonGenerator.WriteStringField("id", partyObject.Id);
                }

                WriteITextTypeWrapper("name", "names", partyObject.Name);

                if (ObjectUtil.ValidCollection(partyObject.Contacts) && partyObject.Contacts.Count > 0)
                {
                    _jsonGenerator.WriteArrayFieldStart("contact");
                    foreach (var contact in partyObject.Contacts)
                    {
                        WriteContact(contact);
                    }
                    _jsonGenerator.WriteEndArray();
                }
            }
        }

        /// <summary>
        /// Write the Receivers.
        /// </summary>
        /// <param name="receivers">
        /// The Receivers
        /// </param>
        private void WriteReceivers(IList<IParty> receivers)
        {
            if (receivers == null || !ObjectUtil.ValidCollection(receivers))
            {
                // Do not write receiver since it's empty
                return;
            }

            _log.Debug("{receiver}");

            _jsonGenerator.WriteArrayFieldStart("receiver");

            foreach (var receiver in receivers)
            {
                _jsonGenerator.WriteStartObject();
                WriteParty(receiver);
                _jsonGenerator.WriteEndObject();
            }

            _log.Debug("{/receiver}");
            _jsonGenerator.WriteEndArray();
        }

        /// <summary>
        /// Write the Sender.
        /// </summary>
        /// <param name="sender">
        /// The Sender to write.
        /// </param>
        private void WriteSender(IParty sender)
        {
            _log.Debug("{sender}");
            _jsonGenerator.WriteObjectFieldStart("sender");

            WriteParty(sender);

            _log.Debug("{/sender}");
            _jsonGenerator.WriteEndObject();
        }
    }
}