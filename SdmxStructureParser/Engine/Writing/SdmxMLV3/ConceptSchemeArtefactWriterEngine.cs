// -----------------------------------------------------------------------
// <copyright file="ConceptSchemeArtefactWriterEngine.cs" company="EUROSTAT">
//   Date Created : 2022-01-04
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing.SdmxMLV3
{
    using System.Xml;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using static Org.Sdmxsource.Sdmx.Api.Constants.SdmxConstants;

    /// <summary>
    /// Writes a Concept Scheme in Sdmx xml v3.0 format.
    /// </summary>
    public class ConceptSchemeArtefactWriterEngine : StaxItemSchemeWriterEngineAdapter<IConceptObject, IConceptSchemeObject>
    {
        private readonly static string ROOT = "ConceptScheme";

        /// <summary>
        /// Initializes a new instance of the <see cref="ConceptSchemeArtefactWriterEngine"/> class.
        /// </summary>
        /// <param name="config">The writing configuration.</param>
        public ConceptSchemeArtefactWriterEngine(WriterConfig config)
            : base(config, ROOT)
        {
        }

        /// <summary>
        /// Writes the elements of an <see cref="IItemSchemeObject{IConceptObject}"/>.
        /// </summary>
        /// <param name="itemScheme">The item to write the elements for.</param>
        /// <param name="writer">The xml writer.</param>
        public override void WriteElements(IConceptSchemeObject itemScheme, XmlWriter writer)
        {
            if (!itemScheme.IsExternalReference.IsTrue)
            {
                // write codes
                WriteItems(writer, itemScheme);
            }
        }

        private void WriteItems(XmlWriter writer, IConceptSchemeObject conceptScheme)
        {
            foreach (IConceptObject item in conceptScheme.Items)
            {
                writer.WriteStartElement("Concept", StructureNs300);
                base.Write(item, item.ParentConcept, writer);
                if (item.CoreRepresentation != null)
                {
                    RepresentationWriter.Write("CoreRepresentation", item.CoreRepresentation, writer);
                }
                ICrossReference isoConceptReference = item.IsoConceptReference;
                if (isoConceptReference != null)
                {
                    writer.WriteStartElement("ISOConceptReference", StructureNs300);
                    writer.WriteStartElement("ConceptAgency", StructureNs300);
                    writer.WriteString(isoConceptReference.AgencyId);
                    writer.WriteEndElement();
                    writer.WriteStartElement("ConceptSchemeID", StructureNs300);
                    writer.WriteString(isoConceptReference.MaintainableId);
                    writer.WriteEndElement();
                    writer.WriteStartElement("ConceptID", StructureNs300);
                    writer.WriteString(isoConceptReference.FullId);
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                }

                writer.WriteEndElement(); // END Code
            }
        }
    }
}
