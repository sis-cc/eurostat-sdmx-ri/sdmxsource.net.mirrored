// -----------------------------------------------------------------------
// <copyright file="MaintainableWriter.cs" company="EUROSTAT">
//   Date Created : 2022-01-04
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing.SdmxMLV3
{
    using System;
    using System.Xml;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// Writes a <see cref="IMaintainableObject"/> in sdmx xml v3.0 format.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class MaintainableWriter<T> : NameableWriter, ISdmxArtefactWriterEngine<T>
        where T : IMaintainableObject
    {
        private readonly string _root;

        /// <summary>
        /// Initializes a new instance of the <see cref="MaintainableWriter{T}"/> class.
        /// </summary>
        /// <param name="config">the writing configuration.</param>
        /// <param name="root">The root element</param>
        protected MaintainableWriter(WriterConfig config, string root)
            : base(config)
        {
            this._root = root;
        }

        /// <summary>
        /// Writes a <see cref="IMaintainableObject"/>.
        /// </summary>
        /// <param name="sdmxObject">The <see cref="IMaintainableObject"/> object to be writen.</param>
        /// <param name="writer">The xml writer.</param>
        public virtual void Write(T sdmxObject, XmlWriter writer)
        {
            if (writer == null)
            {
                throw new ArgumentNullException(nameof(writer));
            }
            if (sdmxObject == null)
            {
                throw new ArgumentNullException(nameof(sdmxObject));
            }

            writer.WriteStartElement(_root, SdmxConstants.StructureNs300);

            WriteMaintainableAttributes(sdmxObject, writer);
            WriteAttributes(sdmxObject, writer);

            base.Write(sdmxObject, writer);

            WriteElements(sdmxObject, writer);

            writer.WriteEndElement();
        }

        /// <summary>
        /// Override this method for adding more attributes than the maintainable common attributes.
        /// </summary>
        /// <param name="maintainableObject"></param>
        /// <param name="writer"></param>
        protected virtual void WriteAttributes(T maintainableObject, XmlWriter writer)
        {

        }

        /// <summary>
        /// Writes the elements of a <see cref="IMaintainableObject"/>.
        /// </summary>
        /// <param name="maintainableObject">The <see cref="IMaintainableObject"/> to write the elements for.</param>
        /// <param name="writer">The xml writer.</param>
        public abstract void WriteElements(T maintainableObject, XmlWriter writer);

        private static void WriteMaintainableAttributes(IMaintainableObject maintainableObject, XmlWriter writer)
        {
            writer.WriteAttributeString("agencyID", maintainableObject.AgencyId);
            CommonWriter.WriteAttributeIfNotNull("version", FormatVersion(maintainableObject.Version, maintainableObject.IsFinal), writer);
            CommonWriter.WriteAttributeIfNotNull("validFrom", maintainableObject.StartDate, writer);
            CommonWriter.WriteAttributeIfNotNull("validTo", maintainableObject.EndDate, writer);
            CommonWriter.WriteAttributeAsStringIfNotNull("serviceURL", maintainableObject.ServiceUrl, writer);
            CommonWriter.WriteAttributeAsStringIfNotNull("structureURL", maintainableObject.StructureUrl, writer);
        }

        private static string FormatVersion(string version, TertiaryBool finalFlag)
        {
            bool finalFlagFalse = finalFlag.IsSet() && !finalFlag.IsTrue;
            if (VersionableUtil.IsFinal(version) && finalFlagFalse)
            {
                return VersionableUtil.FormatDraftVersion(version);
            }

            return version;
        }
    }
}
