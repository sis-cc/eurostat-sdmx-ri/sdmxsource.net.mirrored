// -----------------------------------------------------------------------
// <copyright file="StaxMaintainableWriterEngineAdapter.cs" company="EUROSTAT">
//   Date Created : 2023-06-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing.SdmxMLV3
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Io.Sdmx.Core.Sdmx.Api.Model.Structure;
    using Io.Sdmx.Format.Ml.Api.Engine;
    using Io.Sdmx.Utils.Core.Xml;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    public abstract class StaxMaintainableWriterEngineAdapter<T> : MaintainableWriter<T>, IStaxMaintainableWriterEngine<T>
        where T : IMaintainableObject
    {
        protected StaxMaintainableWriterEngineAdapter(WriterConfig config, string root) 
            : base(config, root)
        {
        }

        public void WriteMaintainable(T maint, StaxWriter writer)
        {
            try
            {
                Write(maint, writer.GetWriter());
            }
            catch(Exception e)
            {
                throw new SdmxInternalServerException(e.Message);
            }
        }

        public void WriteMaintainable(T maint, IExternalMaintainableLinks externalLinks, StaxWriter writer)
        {
            WriteMaintainable(maint, writer);
        }
    }
}
