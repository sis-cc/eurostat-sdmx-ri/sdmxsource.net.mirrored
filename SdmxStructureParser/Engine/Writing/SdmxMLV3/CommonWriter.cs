// -----------------------------------------------------------------------
// <copyright file="CommonWriter.cs" company="EUROSTAT">
//   Date Created : 2022-01-04
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing.SdmxMLV3
{
    using System.Xml;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

    public class CommonWriter
    {
        public static void WriteUrn(IIdentifiableObject identifiableObject, XmlWriter writer, WriterConfig config)
        {
            if (config == null || !config.IsWriteUrn()) 
            {
                return;
            }
            string urn;
            if (identifiableObject.StructureType == SdmxStructureType.GetFromEnum(SdmxStructureEnumType.PrimaryMeasure))
            {
                //for measure objects generated from primary measure objects we need to change the urn in 
                //order to be able to write the 3.0 xml
                var asReference = identifiableObject.AsReference;
                urn = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Measure).
                        GenerateUrn(asReference.AgencyId, asReference.MaintainableId, asReference.Version,new[] { asReference.FullId}).ToString();
            }
            else
            {
                urn = identifiableObject.Urn.ToString(); 
            }
            WriteAttributeIfNotNull("urn", urn, writer);
        }

        public static void WriteAttributeIfNotNull(string localName, string value, XmlWriter writer)
        {
            if (value != null) 
            {
                writer.WriteAttributeString(localName, value);
            }
        }
        public static void WriteAttributeIfNotNull(string localName, ISdmxDate value, XmlWriter writer)
        {
            if (value != null) 
            {
                writer.WriteAttributeString(localName, value.DateInSdmxFormat);
            }
        }
        public static void WriteAttributeAsStringIfNotNull(string localName, object value, XmlWriter writer)
        {
            if (value != null) 
            {
                writer.WriteAttributeString(localName, value.ToString());
            }
        }
        public static void WriteAttributeIfSet(string localName, TertiaryBool value, XmlWriter writer)
        {
            if (value != null && value != TertiaryBool.GetFromEnum(TertiaryBoolEnumType.Unset)) 
            {
                writer.WriteAttributeString(localName, value.IsTrue.ToString().ToLower());
            }
        }
    }
}
