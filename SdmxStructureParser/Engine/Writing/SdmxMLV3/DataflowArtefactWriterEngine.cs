// -----------------------------------------------------------------------
// <copyright file="DataflowArtefactWriterEngine.cs" company="EUROSTAT">
//   Date Created : 2022-01-04
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing.SdmxMLV3
{
    using System.Xml;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

    using static Org.Sdmxsource.Sdmx.Api.Constants.SdmxConstants;

    /// <summary>
    /// Writes a <see cref="IDataflowObject"/> in Sdmx xml v3.0 format.
    /// </summary>
    public class DataflowArtefactWriterEngine : StaxMaintainableWriterEngineAdapter<IDataflowObject>
    {
        private readonly static string ROOT = "Dataflow";

        /// <summary>
        /// Initializes a new instance of the <see cref="DataflowArtefactWriterEngine"/> class.
        /// </summary>
        /// <param name="config">The writing configurations.</param>
        public DataflowArtefactWriterEngine(WriterConfig config)
            : base(config, ROOT)
        {
        }

        /// <summary>
        /// Writes the elements of a <see cref="IDataflowObject"/>
        /// </summary>
        /// <param name="dataflow">The <see cref="IDataflowObject"/> to write the elements for.</param>
        /// <param name="writer">The xml writer.</param>
        public override void WriteElements(IDataflowObject dataflow, XmlWriter writer)
        {

            if (!dataflow.IsExternalReference.IsTrue)
            {
                writer.WriteStartElement("Structure", StructureNs300);
                writer.WriteString(dataflow.DataStructureRef.TargetUrn.ToString());
                writer.WriteEndElement();
            }
        }
    }
}
