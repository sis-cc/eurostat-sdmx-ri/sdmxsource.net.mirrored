// -----------------------------------------------------------------------
// <copyright file="IdentifiableWriter.cs" company="EUROSTAT">
//   Date Created : 2022-01-04
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing.SdmxMLV3
{
    using System.Xml;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    public abstract class IdentifiableWriter : AnnotableWriter
    {
        private readonly WriterConfig _config;

        protected IdentifiableWriter(WriterConfig config)
        {
            this._config = config;
        }

        public void Write(IIdentifiableObject identifiableObject, XmlWriter writer)
        {
            WriteIdentifiableAttribute(identifiableObject, writer, _config);
            base.Write(identifiableObject, writer);
        }

        private static void WriteIdentifiableAttribute(IIdentifiableObject identifiableObject, XmlWriter writer, WriterConfig config)
        {
            CommonWriter.WriteAttributeIfNotNull("id", identifiableObject.Id, writer);
            CommonWriter.WriteUrn(identifiableObject, writer, config);
            CommonWriter.WriteAttributeAsStringIfNotNull("uri", identifiableObject.Uri, writer);

        }
    }
}
