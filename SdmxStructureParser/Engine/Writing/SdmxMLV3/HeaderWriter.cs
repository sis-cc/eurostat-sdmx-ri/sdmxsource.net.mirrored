// -----------------------------------------------------------------------
// <copyright file="HeaderWriter.cs" company="EUROSTAT">
//   Date Created : 2022-01-04
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing.SdmxMLV3
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Date;
    using Org.Sdmxsource.Util;

    public class HeaderWriter
    {
        public static void Write(IHeader header, XmlWriter writer)
        {
            writer.WriteStartElement("Header", SdmxConstants.MessageNs300);
            //   <mes:ID>IREF262603</mes:ID>
            writer.WriteStartElement("ID", SdmxConstants.MessageNs300);
            writer.WriteString(header.Id);
            writer.WriteEndElement();
            //  <mes:Test>false</mes:Test>
            writer.WriteStartElement("Test", SdmxConstants.MessageNs300);
            writer.WriteString(header.Test.ToString().ToLower());
            writer.WriteEndElement();
            //  <mes:Prepared>2021-03-08T17:57:45Z</mes:Prepared>
            writer.WriteStartElement("Prepared", SdmxConstants.MessageNs300);
            if (header.Prepared.HasValue)
            {
                writer.WriteString(DateUtil.FormatDate(header.Prepared.Value));
            }
            else
            {
                // mandatory field default to now
                writer.WriteString(DateUtil.FormatDate(DateTimeOffset.Now));
            }

            writer.WriteEndElement();
            // <mes:Sender id="Unknown" />
            Write("Sender", header.Sender, writer);
            if (header.Receiver != null)
            {
                foreach (IParty party in header.Receiver)
                {
                    Write("Receiver", party, writer);
                }
            }
            TextTypeWriter.WriteText("Name", header.Name, writer);
            TextTypeWriter.WriteText("Source", header.Name, writer);
            writer.WriteEndElement();
        }

        private static void Write(string localName, IParty party, XmlWriter writer)
        {
            // <mes:Sender id="Unknown" />
            if (!ObjectUtil.ValidCollection(party.Name) && !ObjectUtil.ValidCollection(party.Contacts) && !ObjectUtil.ValidString(party.TimeZone))
            {
                writer.WriteStartElement(localName, SdmxConstants.MessageNs300);
                writer.WriteAttributeString("id", party.Id);
                writer.WriteEndElement();
                return;
            }

            writer.WriteStartElement(localName, SdmxConstants.MessageNs300);
            writer.WriteAttributeString("id", party.Id);

            TextTypeWriter.WriteText("Name", party.Name, writer);
            if (party.Contacts != null)
            {
                foreach (IContact contact in party.Contacts)
                {
                    writer.WriteStartElement("Contact", SdmxConstants.MessageNs300);
                    TextTypeWriter.WriteText("Name", contact.Name, writer);
                    TextTypeWriter.WriteText("Department", contact.Departments, writer);
                    TextTypeWriter.WriteText("Role", contact.Departments, writer);
                    WriteStrings("Telephone", contact.Telephone, writer);
                    WriteStrings("Fax", contact.Fax, writer);
                    WriteStrings("X400", contact.X400, writer);
                    WriteStrings("URI", contact.Uri, writer);
                    WriteStrings("Email", contact.Email, writer);
                    writer.WriteEndElement();
                }
            }

            if (ObjectUtil.ValidString(party.TimeZone))
            {
                writer.WriteStartElement("Timezone", SdmxConstants.MessageNs300);
                writer.WriteString(party.TimeZone);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        private static void WriteStrings(string localName, IList<string> values, XmlWriter writer)
        {
            if (!ObjectUtil.ValidCollection(values))
            {
                return;
            }

            foreach (string value in values.Where(s => ObjectUtil.ValidString(s)))
            {
                writer.WriteStartElement(localName, SdmxConstants.MessageNs300);
                writer.WriteString(value);
                writer.WriteEndElement();
            }
        }
    }
}
