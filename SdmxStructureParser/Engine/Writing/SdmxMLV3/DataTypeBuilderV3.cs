// -----------------------------------------------------------------------
// <copyright file="DataTypeBuilderV3.cs" company="EUROSTAT">
//   Date Created : 2022-01-04
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing.SdmxMLV3
{
    using Org.Sdmxsource.Sdmx.Api.Constants;

    public class DataTypeBuilderV3
    {
        // TODO taken from SDMX 2.1 need to cross check with
        public static string Build(TextType buildFrom)
        {
            switch (buildFrom.EnumType)
            {
                case TextEnumType.Alpha:
                    return "Alpha";
                case TextEnumType.Alphanumeric:
                    return "AlphaNumeric";
                case TextEnumType.String:
                    return "String";
                case TextEnumType.BigInteger:
                    return "BigInteger";
                case TextEnumType.Integer:
                    return "Integer";
                case TextEnumType.Long:
                    return "Long";
                case TextEnumType.Short:
                    return "Short";
                case TextEnumType.Decimal:
                    return "Decimal";
                case TextEnumType.Float:
                    return "Float";
                case TextEnumType.Double:
                    return "Double";
                case TextEnumType.Boolean:
                    return "Boolean";
                case TextEnumType.DateTime:
                    return "DateTime";
                case TextEnumType.Time:
                    return "Time";
                case TextEnumType.Year:
                    return "GregorianYear";
                case TextEnumType.Month:
                    return "Month";
                case TextEnumType.Day:
                    return "Day";
                case TextEnumType.MonthDay:
                    return "MonthDay";
                case TextEnumType.Numeric:
                    return "Numeric";
                case TextEnumType.YearMonth:
                    return "GregorianYearMonth";
                case TextEnumType.Duration:
                    return "Duration";
                case TextEnumType.Uri:
                    return "Uri";
                case TextEnumType.Timespan:
                    return "GregorianTimePeriod";
                case TextEnumType.Count:
                    return "Count";
                case TextEnumType.InclusiveValueRange:
                    return "InclusiveValueRange";
                case TextEnumType.ExclusiveValueRange:
                    return "ExclusiveValueRange";
                case TextEnumType.Incremental:
                    return "Incremental";
                case TextEnumType.ObservationalTimePeriod:
                    return "ObservationalTimePeriod";
                case TextEnumType.Date:
                    return "DateTime";
                case TextEnumType.BasicTimePeriod:
                    return "BasicTimePeriod";
                case TextEnumType.DataSetReference:
                    return "DataSetReference";
                case TextEnumType.GregorianDay:
                    return "GregorianDay";
                case TextEnumType.GregorianTimePeriod:
                    return "GregorianTimePeriod";
                case TextEnumType.GregorianYear:
                    return "GregorianYear";
                case TextEnumType.GregorianYearMonth:
                    return "GregorianYearMonth";
                case TextEnumType.ReportingDay:
                    return "ReportingDay";
                case TextEnumType.ReportingMonth:
                    return "ReportingMonth";
                case TextEnumType.ReportingQuarter:
                    return "ReportingQuarter";
                case TextEnumType.ReportingSemester:
                    return "ReportingSemester";
                case TextEnumType.ReportingTimePeriod:
                    return "ReportingTimePeriod";
                case TextEnumType.ReportingTrimester:
                    return "ReportingTrimester";
                case TextEnumType.ReportingWeek:
                    return "ReportingWeek";
                case TextEnumType.ReportingYear:
                    return "ReportingYear";
                case TextEnumType.StandardTimePeriod:
                    return "StandardTimePeriod";
                case TextEnumType.TimePeriod:
                    return "TimeRange";
                case TextEnumType.TimesRange:
                    return "TimeRange";
                case TextEnumType.Xhtml:
                    return "XHTML";
            }
            return null;
        }

        public static string BuildCascadeValue(CascadeSelection cascadeSelection)
        {
            switch (cascadeSelection)
            {
                case CascadeSelection.False:
                    return "false";
                case CascadeSelection.True:
                    return "true";
                case CascadeSelection.ExcludeRoot:
                    return "excluderoot";
            }

            return "false";
        }
    }
}
