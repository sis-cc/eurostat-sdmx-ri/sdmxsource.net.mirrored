// -----------------------------------------------------------------------
// <copyright file="RepresentationWriter.cs" company="EUROSTAT">
//   Date Created : 2022-01-04
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing.SdmxMLV3
{
    using System.ComponentModel;
    using System.Xml;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

    public class RepresentationWriter
    {
        public static void Write(string localName, IRepresentation representation, XmlWriter writer)
        {
            if (representation != null) {
                writer.WriteStartElement(localName, SdmxConstants.StructureNs300);
                SdmxStructureType parentStructuretype = representation.Parent.StructureType;
                switch (parentStructuretype.EnumType) 
                {
                    case SdmxStructureEnumType.Concept:
                    case SdmxStructureEnumType.DataAttribute:
                    case SdmxStructureEnumType.Measure:
                    //primary measure only for backward compatibility
                    case SdmxStructureEnumType.PrimaryMeasure:
                        if (representation.MinOccurs != null && representation.MinOccurs != 1) 
                        {
                            writer.WriteAttributeString("minOccurs", representation.MinOccurs.ToString());
                        }
                        if (representation.MaxOccurs != null && !representation.MaxOccurs.Equals(FiniteOccurenceObjectCore.DefaultValue)) 
                        {
                            writer.WriteAttributeString("maxOccurs", representation.MaxOccurs.ToString());
                        }
                        break;
                    case SdmxStructureEnumType.MetadataAttribute:
                    case SdmxStructureEnumType.TimeDimension:
                    case SdmxStructureEnumType.Dimension:
                    case SdmxStructureEnumType.MeasureDimension:
                        // we don't write
                        break;
                    default:
                        throw new InvalidEnumArgumentException($"{parentStructuretype.EnumType} structure type is not supported");
                }

                if (representation.Representation != null && parentStructuretype.EnumType != SdmxStructureEnumType.TimeDimension)
                {
                    writer.WriteStartElement("Enumeration", SdmxConstants.StructureNs300);
                    // Maybe in the future choose to write either REF or URN
                    writer.WriteString(representation.Representation.TargetUrn.ToString());
                    writer.WriteEndElement();
                    // EnumerationFormat
                    Write("EnumerationFormat", representation.TextFormat, writer, true);
                }
                else
                {
                    Write("TextFormat", representation.TextFormat, writer, false);
                }

                writer.WriteEndElement();
            }
        }

        public static void Write(string localName, ITextFormat textFormat, XmlWriter writer, bool isSimple)
        {
            if (textFormat == null) 
            {
                return;
            }
            writer.WriteStartElement(localName, SdmxConstants.StructureNs300);
            if (textFormat.TextType != null) 
            {
                string xmlTextType = DataTypeBuilderV3.Build(textFormat.TextType);
                writer.WriteAttributeString("textType", xmlTextType);
            }
            CommonWriter.WriteAttributeIfSet("isSequence", textFormat.Sequence, writer);
            CommonWriter.WriteAttributeAsStringIfNotNull("interval", textFormat.Interval, writer);
            CommonWriter.WriteAttributeAsStringIfNotNull("startValue", textFormat.StartValue, writer);
            CommonWriter.WriteAttributeAsStringIfNotNull("endValue", textFormat.EndValue, writer);
            CommonWriter.WriteAttributeIfNotNull("timeInterval", textFormat.TimeInterval, writer);
            CommonWriter.WriteAttributeIfNotNull("startTime", textFormat.StartTime, writer);
            CommonWriter.WriteAttributeIfNotNull("endTime", textFormat.EndTime, writer);
            CommonWriter.WriteAttributeAsStringIfNotNull("minLength", textFormat.MinLength, writer);
            CommonWriter.WriteAttributeAsStringIfNotNull("maxLength", textFormat.MaxLength, writer);
            CommonWriter.WriteAttributeAsStringIfNotNull("minValue", textFormat.MinValue, writer);
            CommonWriter.WriteAttributeAsStringIfNotNull("maxValue", textFormat.MaxValue, writer);
            CommonWriter.WriteAttributeIfNotNull("pattern", textFormat.Pattern, writer);
            if (!isSimple) 
            {
                CommonWriter.WriteAttributeAsStringIfNotNull("decimals", textFormat.Decimals, writer);
                CommonWriter.WriteAttributeIfSet("isMultiLingual", textFormat.Multilingual, writer);
                if (textFormat.SentinelValues != null)
                {
                    foreach (ISentinelValue sentinel in textFormat.SentinelValues)
                    {
                        writer.WriteStartElement("SentinelValue", SdmxConstants.StructureNs300);
                        writer.WriteAttributeString("value", sentinel.Value);
                        TextTypeWriter.WriteText("Name", sentinel.Names, writer);
                        if (sentinel.Descriptions != null)
                        {
                            TextTypeWriter.WriteText("Description", sentinel.Descriptions, writer);
                        }
                        writer.WriteEndElement();
                    }
                }
            }
            writer.WriteEndElement();
        }
    }
}
