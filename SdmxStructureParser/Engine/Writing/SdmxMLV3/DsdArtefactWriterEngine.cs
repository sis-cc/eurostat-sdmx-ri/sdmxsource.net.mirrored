// -----------------------------------------------------------------------
// <copyright file="DsdArtefactWriterEngine.cs" company="EUROSTAT">
//   Date Created : 2022-01-04
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing.SdmxMLV3
{
    using System.Collections.Generic;
    using System.Xml;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// Writes a <see cref="IDataStructureObject"/> in Sdmx xml v3.0 format.
    /// </summary>
    public class DsdArtefactWriterEngine : StaxMaintainableWriterEngineAdapter<IDataStructureObject>
    {
        private readonly static string ROOT = "DataStructure";
        private readonly ComponentWriter _componentWriter;

        /// <summary>
        /// Initializes a new instance of the <see cref="DsdArtefactWriterEngine"/> class.
        /// </summary>
        /// <param name="config"></param>
        public DsdArtefactWriterEngine(WriterConfig config)
            : base(config, ROOT)
        {
            this._componentWriter = new ComponentWriter(this);
        }

        /// <summary>
        /// Writes the elements of a <see cref="IDataStructureObject"/>.
        /// </summary>
        /// <param name="dsd">The DSD to write the elements for.</param>
        /// <param name="writer">The xml writer.</param>
        public override void WriteElements(IDataStructureObject dsd, XmlWriter writer)
        {
            if (!dsd.IsExternalReference.IsTrue)
            {
                writer.WriteStartElement("DataStructureComponents", SdmxConstants.StructureNs300);
                WriteDimensions(writer, dsd);
                // write group
                WriteGroups(writer, dsd);
                // write attribute list
                WriteAttributes(writer, dsd);
                // write measure list
                if (ObjectUtil.ValidCollection(dsd.Measures))
                {
                    writer.WriteStartElement("MeasureList", SdmxConstants.StructureNs300);
                    base.Write(dsd.MeasureList, writer);
                    foreach (IMeasure measure in dsd.Measures)
                    {
                        writer.WriteStartElement("Measure", SdmxConstants.StructureNs300);
                        WriteUsage(measure, writer);
                        _componentWriter.Write(measure, writer);
                        WriteConceptRole(measure.ConceptRoles, writer);
                        writer.WriteEndElement(); // end Measure
                    }

                    writer.WriteEndElement(); // end MeasureList
                }
                writer.WriteEndElement(); // end DataStructureComponents
            }
        }

        private void WriteAttributes(XmlWriter writer, IDataStructureObject dsd)
        {
            if (dsd.AttributeList != null && (ObjectUtil.ValidCollection(dsd.Attributes) || ObjectUtil.ValidCollection(dsd.GetMetadataAttributes())))
            {

                writer.WriteStartElement("AttributeList", SdmxConstants.StructureNs300);
                base.Write(dsd.AttributeList, writer);
                if (ObjectUtil.ValidCollection(dsd.Attributes))
                {
                    foreach (IAttributeObject attribute in dsd.Attributes)
                    {
                        Write(attribute, writer);
                    }
                }

                if (ObjectUtil.ValidCollection(dsd.GetMetadataAttributes()))
                {
                    foreach (IMetadataAttributeUsage metadataAttribute in dsd.GetMetadataAttributes())
                    {
                        Write(metadataAttribute, writer);
                    }
                }

                writer.WriteEndElement(); // end AttributeList
            }
        }

        private void WriteDimensions(XmlWriter writer, IDataStructureObject dsd)
        {
            if (dsd.DimensionList != null)
            {
                writer.WriteStartElement("DimensionList", SdmxConstants.StructureNs300);
                base.Write(dsd.DimensionList, writer);
                // measure dimension is a normal dimension in SDMX 3.0.0
                foreach (IDimension dimension in dsd.GetDimensions(SdmxStructureEnumType.Dimension, SdmxStructureEnumType.MeasureDimension))
                {
                    WriteDimension(dimension, writer);
                }
                if (dsd.TimeDimension != null)
                {
                    IDimension dimension = dsd.TimeDimension;
                    writer.WriteStartElement("TimeDimension", SdmxConstants.StructureNs300);
                    _componentWriter.Write(dimension, writer);
                    writer.WriteEndElement(); // end TimeDimension
                }
                writer.WriteEndElement(); // end DimensionList
            }
        }

        private void WriteGroups(XmlWriter writer, IDataStructureObject dsd)
        {
            if (ObjectUtil.ValidCollection(dsd.Groups))
            {
                foreach (IGroup group in dsd.Groups)
                {
                    writer.WriteStartElement("Group", SdmxConstants.StructureNs300);
                    base.Write(group, writer);
                    foreach (string dimensionRef in group.DimensionRefs)
                    {
                        writer.WriteStartElement("GroupDimension", SdmxConstants.StructureNs300);
                        writer.WriteStartElement("DimensionReference", SdmxConstants.StructureNs300);
                        writer.WriteString(dimensionRef);
                        writer.WriteEndElement(); // end DimensionRef
                        writer.WriteEndElement(); // end Group
                    }
                    writer.WriteEndElement(); // end Group
                }
            }
        }

        private void Write(IMetadataAttributeUsage metadataAttribute, XmlWriter writer)
        {
            writer.WriteStartElement("MetadataAttributeUsage", SdmxConstants.StructureNs300);
            AnnotableWriter.WriteAnnotations(metadataAttribute, writer);
            //
            writer.WriteStartElement("MetadataAttributeReference", SdmxConstants.StructureNs300);
            writer.WriteString(metadataAttribute.MetadataAttributeReference);
            writer.WriteEndElement(); // END MetadataAttributeReference

            // Attribute Relationship
            WriteAttributeRelationship(metadataAttribute, writer);
            writer.WriteEndElement(); // END MetadataAttributeUsage
        }

        private void Write(IAttributeObject attribute, XmlWriter writer)
        {
            writer.WriteStartElement("Attribute", SdmxConstants.StructureNs300);
            WriteUsage(attribute, writer);
            _componentWriter.Write(attribute, writer);
            WriteConceptRole(attribute.ConceptRoles, writer);
            // Attribute Relationship
            WriteAttributeRelationship(attribute, writer);
            // MeasureRelationship
            if (ObjectUtil.ValidCollection(attribute.MeasureRelationships))
            {
                writer.WriteStartElement("MeasureRelationship", SdmxConstants.StructureNs300);
                foreach (string measure in attribute.MeasureRelationships)
                {
                    writer.WriteStartElement("Measure", SdmxConstants.StructureNs300);
                    writer.WriteString(measure);
                    writer.WriteEndElement(); // END Measure
                }

                writer.WriteEndElement(); // END MeasureRelationship
            }
            writer.WriteEndElement(); // END Attribute
        }

        private void WriteUsage(IUsage attribute, XmlWriter writer)
        {
            if (attribute.Usage != null)
            {
                writer.WriteAttributeString("usage", attribute.Usage.ToString().ToLower());
            }
        }

        private void WriteAttributeRelationship(IAttributeRelationship attribute, XmlWriter writer)
        {
            writer.WriteStartElement("AttributeRelationship", SdmxConstants.StructureNs300);
            switch (attribute.AttachmentLevel)
            {
                case AttributeAttachmentLevel.DataSet:
                    writer.WriteStartElement("Dataflow", SdmxConstants.StructureNs300);
                    writer.WriteEndElement();
                    break;
                case AttributeAttachmentLevel.Group:
                    writer.WriteStartElement("Group", SdmxConstants.StructureNs300);
                    writer.WriteString(attribute.AttachmentGroup);
                    writer.WriteEndElement();
                    break;
                case AttributeAttachmentLevel.DimensionGroup:
                    // TODO 'optional' xml attribute information not stored in bean
                    foreach (string dimensionReference in attribute.DimensionReferences)
                    {
                        writer.WriteStartElement("Dimension", SdmxConstants.StructureNs300);
                        // FIXME
                        //writer.WriteAttribute("optional", dimensionReference.IsOptional());
                        writer.WriteString(dimensionReference);
                        writer.WriteEndElement();
                    }

                    break;
                case AttributeAttachmentLevel.Observation:
                    writer.WriteStartElement("Observation", SdmxConstants.StructureNs300);
                    writer.WriteEndElement();
                    break;
            }
            writer.WriteEndElement(); // END AttributeRelationship
        }

        private void WriteDimension(IDimension dimension, XmlWriter writer)
        {
            writer.WriteStartElement("Dimension", SdmxConstants.StructureNs300);
            writer.WriteAttributeString("position", dimension.Position.ToString());
            _componentWriter.Write(dimension, writer);
            WriteConceptRole(dimension.ConceptRole, writer);

            writer.WriteEndElement(); // END Dimension
        }

        private void WriteConceptRole(IList<ICrossReference> conceptRoles, XmlWriter writer)
        {
            if (conceptRoles == null)
            {
                return;
            }

            foreach (ICrossReference conceptRole in conceptRoles)
            {
                writer.WriteStartElement("ConceptRole", SdmxConstants.StructureNs300);
                writer.WriteString(conceptRole.TargetUrn.ToString());
                writer.WriteEndElement(); // END Dimension
            }
        }
    }
}
