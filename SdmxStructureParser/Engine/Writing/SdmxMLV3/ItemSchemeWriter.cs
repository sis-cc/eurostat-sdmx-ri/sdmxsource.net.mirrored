// -----------------------------------------------------------------------
// <copyright file="ItemSchemeWriter.cs" company="EUROSTAT">
//   Date Created : 2022-01-04
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing.SdmxMLV3
{
    using System.Xml;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// Writes a <see cref="IItemSchemeObject{T}"/> in sdmx xml v3.0 format. 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="S"></typeparam>
    public abstract class ItemSchemeWriter<T, S> : MaintainableWriter<S>
        where T : IItemObject
        where S : IItemSchemeObject<T>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ItemSchemeWriter{T,S}"/> class.
        /// </summary>
        /// <param name="config">The writing configuration</param>
        /// <param name="root">The root element.</param>
        protected ItemSchemeWriter(WriterConfig config, string root)
            : base(config, root)
        {
        }

        /// <summary>
        /// Writes the attributes of a <see cref="IItemSchemeObject{T}"/>.
        /// </summary>
        /// <param name="itemScheme">The <see cref="IItemSchemeObject{T}"/> to write the attributes for.</param>
        /// <param name="writer">The xml writer.</param>
        protected override void WriteAttributes(S itemScheme, XmlWriter writer)
        {
            if (itemScheme.Partial)
            {
                writer.WriteAttributeString("isPartial", "true");
            }
        }

        /// <summary>
        /// Writes a <see cref="IItemObject"/>.
        /// </summary>
        /// <param name="item">The <see cref="IItemObject"/> to write.</param>
        /// <param name="parentItem">The parent item.</param>
        /// <param name="writer">The xml writer.</param>
        protected void Write(IItemObject item, string parentItem, XmlWriter writer)
        {
            base.Write(item, writer);
            if (ObjectUtil.ValidString(parentItem))
            {
                writer.WriteStartElement("Parent", SdmxConstants.StructureNs300);
                writer.WriteString(parentItem);
                writer.WriteEndElement();
            }
        }
    }
}
