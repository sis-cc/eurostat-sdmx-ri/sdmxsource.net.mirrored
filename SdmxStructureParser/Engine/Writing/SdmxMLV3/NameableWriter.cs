// -----------------------------------------------------------------------
// <copyright file="NameableWriter.cs" company="EUROSTAT">
//   Date Created : 2022-01-04
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing.SdmxMLV3
{
    using System.Xml;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    public abstract class NameableWriter : IdentifiableWriter
    {
        protected NameableWriter(WriterConfig config)
            : base(config)
        {
        }

        public virtual void Write(INameableObject what, XmlWriter writer)
        {
            base.Write(what, writer);
            WriteText(what, writer);
        }

        private static void WriteText(INameableObject what, XmlWriter writer)
        {
            if (what.Names != null) 
            {
                TextTypeWriter.WriteText("Name", what.Names, writer);
            }
            if (what.Descriptions != null) 
            {
                TextTypeWriter.WriteText("Description", what.Descriptions, writer);
            }
        }
    }
}
