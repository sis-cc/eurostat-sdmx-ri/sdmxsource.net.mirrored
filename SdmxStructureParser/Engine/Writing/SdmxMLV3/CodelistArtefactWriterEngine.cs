// -----------------------------------------------------------------------
// <copyright file="CodelistArtefactWriterEngine.cs" company="EUROSTAT">
//   Date Created : 2022-01-04
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing.SdmxMLV3
{
    using System.Xml;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;

    using static Org.Sdmxsource.Sdmx.Api.Constants.SdmxConstants;

    /// <summary>
    /// Writes a codelist in sdmx xml v3.0 format.
    /// </summary>
    public class CodelistArtefactWriterEngine : StaxItemSchemeWriterEngineAdapter<ICode, ICodelistObject>
    {
        private readonly static string ROOT = "Codelist";

        /// <summary>
        /// Initializes a new instance of the <see cref="CodelistArtefactWriterEngine"/> class.
        /// </summary>
        /// <param name="config">The writing configuration.</param>
        public CodelistArtefactWriterEngine(WriterConfig config)
            : base(config, ROOT)
        {
        }

        /// <summary>
        /// Writes the elements of a codelist.
        /// </summary>
        /// <param name="codelist">The codelist tot write the elements for.</param>
        /// <param name="writer">The xml writer.</param>
        public override void WriteElements(ICodelistObject codelist, XmlWriter writer)
        {
            if (!codelist.IsExternalReference.IsTrue)
            {
                // write codes
                WriteCodes(writer, codelist);

                // write codelist extension
                WriteCodelistExtension(writer, codelist);
            }
        }

        private void WriteCodelistExtension(XmlWriter writer, ICodelistObject codelist)
        {
            if (codelist.CodelistExtensions != null)
            {
                foreach (ICodelistInheritanceRule codelistExtension in codelist.CodelistExtensions)
                {
                    writer.WriteStartElement("CodelistExtension", StructureNs300);
                    CommonWriter.WriteAttributeIfNotNull("prefix", codelistExtension.Prefix, writer);
                    writer.WriteStartElement("Codelist", StructureNs300);
                    writer.WriteString(codelistExtension.CodelistRef.TargetUrn.ToString());
                    writer.WriteEndElement(); // END Codelist
                    if (codelistExtension.CodeSelection != null)
                    {
                        string selectionName;
                        if (codelistExtension.CodeSelection.IsInclusive)
                        {
                            selectionName = "InclusiveCodeSelection";
                        }
                        else
                        {
                            selectionName = "ExclusiveCodeSelection";
                        }
                        writer.WriteStartElement(selectionName, StructureNs300);
                        foreach (IMemberValue memberValue in codelistExtension.CodeSelection.MemberValues)
                        {
                            writer.WriteStartElement("MemberValue", StructureNs300);
                            if (memberValue.Cascade != null)
                            {
                                writer.WriteAttributeString("cascadeValues", DataTypeBuilderV3.BuildCascadeValue(memberValue.Cascade));
                            }
                            writer.WriteString(memberValue.Value);
                            writer.WriteEndElement(); // END MemberValue
                        }
                        writer.WriteEndElement(); // END selectionName
                    }
                    writer.WriteEndElement(); // END CodelistExtension
                }
            }
        }

        private void WriteCodes(XmlWriter writer, ICodelistObject codelist)
        {
            foreach (ICode item in codelist.Items)
            {
                writer.WriteStartElement("Code", StructureNs300);
                base.Write(item, item.ParentCode, writer);
                writer.WriteEndElement(); // END Code
            }
        }
    }
}
