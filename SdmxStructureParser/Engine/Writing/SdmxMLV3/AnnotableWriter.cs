// -----------------------------------------------------------------------
// <copyright file="AnnotableWritter.cs" company="EUROSTAT">
//   Date Created : 2022-01-04
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing.SdmxMLV3
{
    using System.Xml;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    public abstract class AnnotableWriter
    {
        public void Write(IAnnotableObject annotable, XmlWriter writer)
        {
            WriteAnnotations(annotable, writer);
        }

        public static void WriteAnnotations(IAnnotableObject annotable, XmlWriter writer)
        {
            if (annotable != null && ObjectUtil.ValidCollection(annotable.Annotations))  
            {
                writer.WriteStartElement("Annotations", SdmxConstants.CommonNs300);
                foreach (var annotation in annotable.Annotations) 
                {
                    writer.WriteStartElement("Annotation", SdmxConstants.CommonNs300);
                    CommonWriter.WriteAttributeIfNotNull("id", annotation.Id, writer);
                    if (ObjectUtil.ValidString(annotation.Title)) {
                        writer.WriteStartElement("AnnotationTitle", SdmxConstants.CommonNs300);
                        writer.WriteString(annotation.Title);
                        writer.WriteEndElement(); // END AnnotationTitle
                    }
                    if (ObjectUtil.ValidString(annotation.Type)) {
                        writer.WriteStartElement("AnnotationType", SdmxConstants.CommonNs300);
                        writer.WriteString(annotation.Type);
                        writer.WriteEndElement(); // END AnnotationType
                    }
                    if (ObjectUtil.ValidCollection(annotation.Urls))
                    {
                        foreach (var annotationURL in annotation.Urls)
                        {
                            writer.WriteStartElement("AnnotationURL", SdmxConstants.CommonNs300);
                            if (ObjectUtil.ValidString(annotationURL.Locale))
                            {
                                writer.WriteAttributeString("xml", "lang", SdmxConstants.Xmlns, annotationURL.Locale);
                            }
                            writer.WriteString(annotationURL.Uri.ToString());
                            writer.WriteEndElement(); // END AnnotationURL
                        } 
                    }
                    if (ObjectUtil.ValidCollection(annotation.Text))
                    {
                        TextTypeWriter.WriteText("AnnotationText", annotation.Text, writer);
                    }
                    if (ObjectUtil.ValidString(annotation.Value))
                    {
                        writer.WriteStartElement("AnnotationValue", SdmxConstants.CommonNs300);
                        writer.WriteString(annotation.Value);
                        writer.WriteEndElement(); // END AnnotationType
                    }
                    writer.WriteEndElement(); // END Annotation
                }
                writer.WriteEndElement(); // END Annotations
            }
        }
    }
}
