﻿// -----------------------------------------------------------------------
// <copyright file="ErrorWriterManager.cs" company="EUROSTAT">
//   Date Created : 2013-06-06
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Manager
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Structureparser.Factory;

    /// <summary>
    /// Class Error Writer Manager.
    /// </summary>
    /// <seealso cref="Org.Sdmxsource.Sdmx.Api.Manager.Output.IErrorWriterManager" />
    public class ErrorWriterManager : IErrorWriterManager
    {
        /// <summary>
        ///     The error writer factory
        /// </summary>
        private readonly List<IErrorWriterFactory> _errorWriterFactory;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ErrorWriterManager" /> class.
        /// </summary>
        public ErrorWriterManager()
            : this(null)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ErrorWriterManager" /> class.
        /// </summary>
        /// <param name="errorWriterFactory">
        ///     The error  writer factory. If set to null the default factory will be used:
        ///     <see cref="SdmxStructureWriterFactory" />
        /// </param>
        public ErrorWriterManager(params IErrorWriterFactory[] errorWriterFactory)
        {
            this._errorWriterFactory = new List<IErrorWriterFactory>(errorWriterFactory ?? new[] { new SdmxErrorWriterFactory() });
        }

        /// <summary>
        /// Writes the exception to the output stream
        /// </summary>
        /// <param name="ex">The exception</param>
        /// <param name="outPutStream">The stream to write the error response to</param>
        /// <param name="outputFormat">The format to write the error message in</param>
        /// <returns>The HTTP status code based on the error type</returns>
        /// <exception cref="SdmxNotImplementedException">Could not write error out in format outputFormat</exception>
        public int WriteError(Exception ex, Stream outPutStream, IErrorFormat outputFormat)
        {
            return this.GetErrorWriterEngine(outputFormat).WriteError(ex, outPutStream);
        }

        /// <summary>
        /// Gets the error writer engine.
        /// </summary>
        /// <param name="outputFormat">The output format.</param>
        /// <returns>The <see cref="IErrorWriterEngine"/></returns>
        /// <exception cref="SdmxNotImplementedException">Could not write error out in format <paramref name="outputFormat"/></exception>
        private IErrorWriterEngine GetErrorWriterEngine(IErrorFormat outputFormat)
        {
            foreach (var factory in this._errorWriterFactory)
            {
                IErrorWriterEngine engine = factory.GetErrorWriterEngine(outputFormat);
                if (engine != null)
                {
                    return engine;
                }
            }
          
            throw new SdmxNotImplementedException("Could not write error out in format: " + outputFormat);
        }
    }
}