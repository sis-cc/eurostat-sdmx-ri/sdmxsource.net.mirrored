// -----------------------------------------------------------------------
// <copyright file="QueryParsingManager.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Xml;
    using System.Xml.Schema;

    using log4net;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V10.message;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.Query;
    using Org.Sdmxsource.Sdmx.Structureparser.Workspace;
    using Org.Sdmxsource.Sdmx.Util.Exception;
    using Org.Sdmxsource.Sdmx.Util.Sdmx;
    using Org.Sdmxsource.Util.Io;
    using Org.Sdmxsource.Util.Log;
    using Org.Sdmxsource.XmlHelper;

    using QueryMessageType = Org.Sdmx.Resources.SdmxMl.Schemas.V10.message.QueryMessageType;
    using RegistryInterface = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Message.RegistryInterface;

    /// <summary>
    ///     The query parsing manager
    /// </summary>
    /// <example>
    ///     A sample implementation in C# of <see cref="QueryParsingManager" />.
    ///     <code source="..\ReUsingExamples\StructureQuery\ReUsingQueryParsingManager.cs" lang="cs" />
    /// </example>
    public class QueryParsingManager : BaseParsingManager, IQueryParsingManager
    {
        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(QueryParsingManager));

        /// <summary>
        ///     The _query bean builder.
        /// </summary>
        private readonly IQueryBuilder _queryBuilder = new QueryBuilder();

        /// <summary>
        ///     Initializes a new instance of the <see cref="QueryParsingManager" /> class.
        /// </summary>
        public QueryParsingManager()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="QueryParsingManager" /> class.
        /// </summary>
        /// <param name="sdmxSchema">
        ///     The SDMX schema.
        /// </param>
        /// <param name="queryBuilder">
        ///     The query Builder. Overrides the default. Use <c>null</c> to use the default.
        /// </param>
        public QueryParsingManager(SdmxSchemaEnumType sdmxSchema, IQueryBuilder queryBuilder)
            : this(sdmxSchema)
        {
            if (queryBuilder != null)
            {
                this._queryBuilder = queryBuilder;
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="QueryParsingManager" /> class.
        /// </summary>
        /// <param name="sdmxSchema">
        ///     The SDMX schema.
        /// </param>
        public QueryParsingManager(SdmxSchemaEnumType sdmxSchema)
            : base(sdmxSchema)
        {
        }

        /// <summary>
        ///     Processes the SDMX at the given URI and returns a workspace containing the information on what was being queried.
        ///     <p />
        ///     The Query parsing manager processes queries that are in a RegistryInterface document, this includes queries for
        ///     Provisions, Registrations and Structures.  It also processes Queries that are in a QueryMessage document
        /// </summary>
        /// <param name="dataLocation">
        ///     The data location of the query
        /// </param>
        /// <returns>
        ///     The <see cref="IQueryWorkspace" /> from <paramref name="dataLocation" />.
        /// </returns>
        public virtual IQueryWorkspace ParseQueries(IReadableDataLocation dataLocation)
        {
            LoggingUtil.Debug(_log, "Parse Structure request, for xml at location: " + dataLocation);

            var schemaVersion = SdmxMessageUtil.GetSchemaVersion(dataLocation);
            _log.DebugFormat(CultureInfo.InvariantCulture, "Schema Version Determined to be : {0}", schemaVersion);
            XMLParser.ValidateXml(dataLocation, schemaVersion);
            _log.Debug("XML VALID");
            using (ISdmxXmlStream xmlStream = new SdmxXmlStream(dataLocation))
            {
                return this.ParseQueries(xmlStream);
            }
        }

        /// <summary>
        ///     Processes the SDMX at the given URI and returns a workspace containing the information on what was being queried.
        ///     <p />
        ///     The Query parsing manager processes queries that are in a RegistryInterface document, this includes queries for
        ///     Provisions, Registrations and Structures.  It also processes Queries that are in a QueryMessage document
        /// </summary>
        /// <param name="dataLocation">
        ///     The data location of the query
        /// </param>
        /// <returns>
        ///     The <see cref="IQueryWorkspace" /> from <paramref name="dataLocation" />.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="dataLocation"/> is <see langword="null" />.</exception>
        /// <exception cref="ParseException">An error occurred while parsing the query.</exception>
        public virtual IQueryWorkspace ParseQueries(ISdmxXmlStream dataLocation)
        {
            if (dataLocation == null)
            {
                throw new ArgumentNullException("dataLocation");
            }

            LoggingUtil.Debug(_log, "Parse Structure request, for xml at location: " + dataLocation);

            SdmxSchemaEnumType schemaVersion = dataLocation.SdmxVersion;

            MessageEnumType messageType = dataLocation.MessageType;
            using (XmlReader reader = dataLocation.Reader)
            {
                if (reader.Settings != null)
                {
                    LoggingUtil.Debug(_log, "XML Validation type " + reader.Settings.ValidationType);
                    LoggingUtil.Debug(_log, "XML Validation Flags: " + reader.Settings.ValidationFlags);
                }

                if (schemaVersion == SdmxSchemaEnumType.VersionOne || schemaVersion == SdmxSchemaEnumType.VersionTwo)
                {
                    switch (messageType)
                    {
                        case MessageEnumType.Query:
                            return this.ProcessQueryMessage(reader, schemaVersion);
                        case MessageEnumType.RegistryInterface:
                            {
                                RegistryMessageEnumType registryMessageType = dataLocation.RegistryType;
                                RegistryMessageType registryMessage =
                                    RegistryMessageType.GetFromEnum(registryMessageType);
                                if (registryMessage.IsQueryRequest())
                                {
                                    try
                                    {
                                        return this.ProcessRegistryQueryMessage(reader, schemaVersion, registryMessageType);
                                    }
                                    catch (XmlSchemaException th)
                                    {
                                        throw new ParseException(
                                            th, 
                                            DatasetActionEnumType.Information, 
                                            false, 
                                            registryMessage.ArtifactType);
                                    }
                                    catch (XmlException th)
                                    {
                                        throw new ParseException(
                                            th, 
                                            DatasetActionEnumType.Information, 
                                            false, 
                                            registryMessage.ArtifactType);
                                    }
                                    catch (SdmxException th)
                                    {
                                        throw new ParseException(
                                            th, 
                                            DatasetActionEnumType.Information, 
                                            false, 
                                            registryMessage.ArtifactType);
                                    }
                                }

                                throw new SdmxNotImplementedException(
                                    ExceptionCode.Unsupported, 
                                    "Expected query message - type found : " + registryMessageType);
                            }
                    }
                }
                else if (schemaVersion == SdmxSchemaEnumType.VersionTwoPointOne)
                {
                    if (messageType == MessageEnumType.RegistryInterface)
                    {
                        RegistryMessageEnumType registryMessageType = dataLocation.RegistryType;
                        return this.ProcessRegistryQueryMessage(reader, schemaVersion, registryMessageType);
                    }

                    QueryMessageEnumType queryMessageType = dataLocation.QueryMessageTypes.FirstOrDefault();

                    // Only one *Where element according to 2.1 Schema
                    if (messageType != MessageEnumType.Query)
                    {
                        // TODO should this be IllegalArgumentException?
                        throw new SdmxNotImplementedException(
                            ExceptionCode.Unsupported, 
                            "Not a structure query message:" + queryMessageType);
                    }

                    return this.ProcessQueryMessage(reader, queryMessageType);
                }
            }

            throw new SdmxNotImplementedException(ExceptionCode.Unsupported, messageType);
        }

        /// <summary>
        ///     processes all v2.1 structure Query messages and build the {@link ComplexStructureQuery}
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="queryMessageType">Type of the query message.</param>
        /// <returns>a parsed {@link ComplexStructureQuery} set in the {@link QueryWorkspace} </returns>
        /// <exception cref="System.ArgumentException">Not a structure query message: + queryMessageType</exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "It is OK. Big switch statement and a lot of SDMX v2.1 artefacts.")]
        private IQueryWorkspace ProcessQueryMessage(XmlReader reader, QueryMessageEnumType queryMessageType)
        {
            IComplexStructureQuery complexQuery;
            switch (queryMessageType)
            {
                case QueryMessageEnumType.StructuresWhere:
                    StructuresQuery structuresQuery = MessageFactory.Load<StructuresQuery, StructuresQueryType>(reader);
                    complexQuery = this._queryBuilder.Build(structuresQuery.Content);
                    break;
                case QueryMessageEnumType.DataflowWhere:
                    DataflowQuery dataflowQuery = MessageFactory.Load<DataflowQuery, DataflowQueryType>(reader);
                    complexQuery = this._queryBuilder.Build(dataflowQuery.Content);
                    break;
                case QueryMessageEnumType.MetadataflowWhere:
                    MetadataflowQuery metadataflowQuery =
                        MessageFactory.Load<MetadataflowQuery, MetadataflowQueryType>(reader);
                    complexQuery = this._queryBuilder.Build(metadataflowQuery.Content);
                    break;
                case QueryMessageEnumType.DsdWhere:
                    DataStructureQuery dataStructureQuery =
                        MessageFactory.Load<DataStructureQuery, DataStructureQueryType>(reader);
                    complexQuery = this._queryBuilder.Build(dataStructureQuery.Content);
                    break;
                case QueryMessageEnumType.MdsWhere:
                    MetadataStructureQuery metadataStructureQuery =
                        MessageFactory.Load<MetadataStructureQuery, MetadataStructureQueryType>(reader);
                    complexQuery = this._queryBuilder.Build(metadataStructureQuery.Content);
                    break;
                case QueryMessageEnumType.CategorySchemeWhere:
                    CategorySchemeQuery categorySchemeQuery =
                        MessageFactory.Load<CategorySchemeQuery, CategorySchemeQueryType>(reader);
                    complexQuery = this._queryBuilder.Build(categorySchemeQuery.Content);
                    break;
                case QueryMessageEnumType.ConceptSchemeWhere:
                    ConceptSchemeQuery conceptSchemeQuery =
                        MessageFactory.Load<ConceptSchemeQuery, ConceptSchemeQueryType>(reader);
                    complexQuery = this._queryBuilder.Build(conceptSchemeQuery.Content);
                    break;
                case QueryMessageEnumType.CodelistWhere:
                    CodelistQuery codelistQuery = MessageFactory.Load<CodelistQuery, CodelistQueryType>(reader);
                    complexQuery = this._queryBuilder.Build(codelistQuery.Content);
                    break;
                case QueryMessageEnumType.HclWhere:
                    HierarchicalCodelistQuery hierarchicalCodelistQuery =
                        MessageFactory.Load<HierarchicalCodelistQuery, HierarchicalCodelistQueryType>(reader);
                    complexQuery = this._queryBuilder.Build(hierarchicalCodelistQuery.Content);
                    break;
                case QueryMessageEnumType.OrganisationSchemeWhere:
                    OrganisationSchemeQuery organisationSchemeQuery =
                        MessageFactory.Load<OrganisationSchemeQuery, OrganisationSchemeQueryType>(reader);
                    complexQuery = this._queryBuilder.Build(organisationSchemeQuery.Content);
                    break;
                case QueryMessageEnumType.ReportingTaxonomyWhere:
                    ReportingTaxonomyQuery reportingTaxonomyQuery =
                        MessageFactory.Load<ReportingTaxonomyQuery, ReportingTaxonomyQueryType>(reader);
                    complexQuery = this._queryBuilder.Build(reportingTaxonomyQuery.Content);
                    break;
                case QueryMessageEnumType.StructureSetWhere:
                    StructureSetQuery structureSetQuery =
                        MessageFactory.Load<StructureSetQuery, StructureSetQueryType>(reader);
                    complexQuery = this._queryBuilder.Build(structureSetQuery.Content);
                    break;
                case QueryMessageEnumType.ProcessWhere:
                    ProcessQuery processQuery = MessageFactory.Load<ProcessQuery, ProcessQueryType>(reader);
                    complexQuery = this._queryBuilder.Build(processQuery.Content);
                    break;
                case QueryMessageEnumType.CategorisationWhere:
                    CategorisationQuery categorisationQuery =
                        MessageFactory.Load<CategorisationQuery, CategorisationQueryType>(reader);
                    complexQuery = this._queryBuilder.Build(categorisationQuery.Content);
                    break;
                case QueryMessageEnumType.ProvisionAgreementWhere:
                    ProvisionAgreementQuery provisionAgreementQuery =
                        MessageFactory.Load<ProvisionAgreementQuery, ProvisionAgreementQueryType>(reader);
                    complexQuery = this._queryBuilder.Build(provisionAgreementQuery.Content);
                    break;
                case QueryMessageEnumType.ConstraintWhere:
                    ConstraintQuery constraintQuery = MessageFactory.Load<ConstraintQuery, ConstraintQueryType>(reader);
                    complexQuery = this._queryBuilder.Build(constraintQuery.Content);
                    break;
                default:
                    throw new SdmxSemmanticException("Not a structure query message:" + queryMessageType);
            }

            return new QueryWorkspace(complexQuery);
        }

        /// <summary>
        ///     Processes a query message which is a QueryMessage Document
        /// </summary>
        /// <param name="reader">
        ///     the input stream reader
        /// </param>
        /// <param name="schemaVersion">
        ///     the SDMX schema version
        /// </param>
        /// <exception cref="SdmxNotImplementedException">
        ///     The value of <paramref name="schemaVersion" /> is not supported.
        /// </exception>
        /// <returns>
        ///     The <see cref="IQueryWorkspace" />.
        /// </returns>
        private IQueryWorkspace ProcessQueryMessage(XmlReader reader, SdmxSchemaEnumType schemaVersion)
        {
            IList<IStructureReference> structureReferences;

            switch (schemaVersion)
            {
                case SdmxSchemaEnumType.VersionOne:
                    QueryMessage docV1 =
                        Org.Sdmx.Resources.SdmxMl.Schemas.V10.MessageFactory.Load<QueryMessage, QueryMessageType>(
                            reader);
                    structureReferences = this._queryBuilder.Build(docV1.Content);
                    break;
                case SdmxSchemaEnumType.VersionTwo:
                    Org.Sdmx.Resources.SdmxMl.Schemas.V20.Message.QueryMessage docV2 =
                        Org.Sdmx.Resources.SdmxMl.Schemas.V20.Message.QueryMessage.Load(reader);
                    structureReferences = this._queryBuilder.Build(docV2.Content);
                    break;
                default:
                    throw new SdmxNotImplementedException(ExceptionCode.Unsupported, schemaVersion);
            }

            return new QueryWorkspace(null, null, structureReferences, false);
        }

        /// <summary>
        ///     Processes a query message which is a RegistryInterface Document
        /// </summary>
        /// <param name="reader">
        ///     - the stream containing the SDMX
        /// </param>
        /// <param name="schemaVersion">
        ///     - the schema version that the SDMX is in
        /// </param>
        /// <param name="registryMessageType">
        ///     - the type of query message, provision, registration or structure
        /// </param>
        /// <returns>
        ///     The <see cref="IQueryWorkspace" />.
        /// </returns>
        private IQueryWorkspace ProcessRegistryQueryMessage(
            XmlReader reader, 
            SdmxSchemaEnumType schemaVersion, 
            RegistryMessageEnumType registryMessageType)
        {
            switch (registryMessageType)
            {
                case RegistryMessageEnumType.QueryProvisionRequest:
                    return this.ProcessRegistryQueryMessageForProvision(reader, schemaVersion);
                case RegistryMessageEnumType.QueryRegistrationRequest:
                    return this.ProcessRegistryQueryMessageForRegistration(reader, schemaVersion);
                case RegistryMessageEnumType.QueryStructureRequest:
                    return this.ProcessRegistryQueryMessageForStructures(reader, schemaVersion);
                default:
                    throw new SdmxNotImplementedException(ExceptionCode.Unsupported, registryMessageType);
            }
        }

        /// <summary>
        ///     The process registry query message for provision.
        /// </summary>
        /// <param name="reader">
        ///     The mask 0.
        /// </param>
        /// <param name="schemaVersion">
        ///     The schema version.
        /// </param>
        /// <returns>
        ///     The <see cref="IQueryWorkspace" />.
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">
        ///     Unsupported value at <paramref name="schemaVersion" />
        /// </exception>
        private IQueryWorkspace ProcessRegistryQueryMessageForProvision(
            XmlReader reader, 
            SdmxSchemaEnumType schemaVersion)
        {
            switch (schemaVersion)
            {
                case SdmxSchemaEnumType.VersionTwo:
                    RegistryInterface doc = RegistryInterface.Load(reader);
                    IStructureReference provisionReferences = this._queryBuilder.Build(doc.QueryProvisioningRequest);
                    return new QueryWorkspace(provisionReferences, null, null, false);
                default:
                    throw new SdmxNotImplementedException(ExceptionCode.Unsupported, schemaVersion);
            }
        }

        /// <summary>
        ///     The process registry query message for registration.
        /// </summary>
        /// <param name="reader">
        ///     The mask 0.
        /// </param>
        /// <param name="schemaVersion">
        ///     The schema version.
        /// </param>
        /// <returns>
        ///     The <see cref="IQueryWorkspace" />.
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">
        ///     Unsupported value at <paramref name="schemaVersion" />
        /// </exception>
        private IQueryWorkspace ProcessRegistryQueryMessageForRegistration(
            XmlReader reader, 
            SdmxSchemaEnumType schemaVersion)
        {
            IStructureReference registrationReferences;
            switch (schemaVersion)
            {
                case SdmxSchemaEnumType.VersionTwo:
                    RegistryInterface doc = RegistryInterface.Load(reader);
                    registrationReferences = this._queryBuilder.Build(doc.QueryRegistrationRequest);
                    break;
                case SdmxSchemaEnumType.VersionTwoPointOne:
                    Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.RegistryInterface doc21 =
                        Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.RegistryInterface.Load(reader);
                    registrationReferences = this._queryBuilder.Build(doc21.Content.QueryRegistrationRequest);
                    break;
                default:
                    throw new SdmxNotImplementedException(ExceptionCode.Unsupported, schemaVersion);
            }

            return new QueryWorkspace(null, registrationReferences, null, false);
        }

        /// <summary>
        ///     The process registry query message for structures.
        /// </summary>
        /// <param name="reader">
        ///     The mask 0.
        /// </param>
        /// <param name="schemaVersion">
        ///     The schema version.
        /// </param>
        /// <returns>
        ///     The <see cref="IQueryWorkspace" />.
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">
        ///     Unsupported value at <paramref name="schemaVersion" />
        /// </exception>
        private IQueryWorkspace ProcessRegistryQueryMessageForStructures(
            XmlReader reader, 
            SdmxSchemaEnumType schemaVersion)
        {
            switch (schemaVersion)
            {
                case SdmxSchemaEnumType.VersionTwo:
                    RegistryInterface doc = RegistryInterface.Load(reader);
                    IList<IStructureReference> structureReferences = this._queryBuilder.Build(doc.QueryStructureRequest);
                    bool resolveRefernces = doc.QueryStructureRequest.resolveReferences;
                    return new QueryWorkspace(null, null, structureReferences, resolveRefernces);
                default:
                    throw new SdmxNotImplementedException(ExceptionCode.Unsupported, schemaVersion);
            }
        }
    }
}