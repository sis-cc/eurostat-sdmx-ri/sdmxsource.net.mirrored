﻿// -----------------------------------------------------------------------
// <copyright file="RestStructureQueryManager.cs" company="EUROSTAT">
//   Date Created : 2013-06-06
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Manager.Rest
{
    using System;
    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Rest;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;

    /// <summary>
    /// The REST structure query manager implementation.
    /// </summary>
    /// <seealso cref="Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Rest.IRestStructureQueryManager" />
    public class RestStructureQueryManager : IRestStructureQueryManager
    {
        /// <summary>
        ///     The structure writing manager.
        /// </summary>
        private readonly IStructureWriterManager _structureWritingManager; // = new StructureWritingManager();

        /// <summary>
        ///     The sdmx objects retrieval manager.
        /// </summary>
        private readonly ISdmxObjectRetrievalManager _beanRetrievalManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="RestStructureQueryManager" /> class.
        /// </summary>
        /// <param name="structureWritingManager">The structure writer manager.</param>
        /// <param name="beanRetrievalManager">The bean retrieval manager.</param>
        /// <exception cref="ArgumentNullException"><paramref name="structureWritingManager"/> is <see langword="null" />.</exception>
        public RestStructureQueryManager(
            IStructureWriterManager structureWritingManager, 
            ISdmxObjectRetrievalManager beanRetrievalManager)
        {
            if (structureWritingManager == null)
            {
                throw new ArgumentNullException("structureWritingManager");
            }

            if (beanRetrievalManager == null)
            {
                throw new ArgumentNullException("beanRetrievalManager");
            }

            this._structureWritingManager = structureWritingManager;
            this._beanRetrievalManager = beanRetrievalManager;
        }

        /// <summary>
        /// Gets the structures into an output stream
        /// </summary>
        /// <param name="query">The rest structures query</param>
        /// <param name="outputStream">The output stream</param>
        /// <param name="outputFormat">The output format</param>
        public void GetStructures(IRestStructureQuery query, Stream outputStream, IStructureFormat outputFormat)
        {
            ISdmxObjects beans = this._beanRetrievalManager.GetMaintainables(query);
            this._structureWritingManager.WriteStructures(beans, outputFormat, outputStream);
        }
    }
}