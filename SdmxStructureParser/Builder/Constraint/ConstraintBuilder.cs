// -----------------------------------------------------------------------
// <copyright file="ConstraintBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.Constraint
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The constraint builder implementation.
    /// </summary>
    public class ConstraintBuilder : IConstraintBuilder
    {
        /// <summary>
        ///     Build constraint.
        /// </summary>
        /// <param name="dataReaderEngine">
        ///     The data reader engine.
        /// </param>
        /// <param name="attachment">
        ///     The attachment.
        /// </param>
        /// <param name="dataSourceAttachment">
        ///     The data source attachment.
        /// </param>
        /// <param name="indexAttributes">
        ///     The index attributes.
        /// </param>
        /// <param name="indexDataset">
        ///     The index dataset.
        /// </param>
        /// <param name="indexReportingPeriod">
        ///     The index reporting period.
        /// </param>
        /// <param name="indexTimeSeries">
        ///     The index time series.
        /// </param>
        /// <param name="definingDataPresent">
        ///     The defining data present.
        /// </param>
        /// <param name="refParameters">
        ///     The ref parameters.
        /// </param>
        /// <returns>
        ///     The <see cref="IContentConstraintObject" />.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="dataReaderEngine"/> is <see langword="null" />.</exception>
        /// <exception cref="SdmxSemmanticException">Can not index time series for registered datasource, the data retrieved from the datasource does not contain a dataset</exception>
        public virtual IContentConstraintObject BuildConstraint(
            IDataReaderEngine dataReaderEngine, 
            IStructureReference attachment, 
            IDataSource dataSourceAttachment, 
            bool indexAttributes, 
            bool indexDataset, 
            bool indexReportingPeriod, 
            bool indexTimeSeries, 
            bool definingDataPresent, 
            IMaintainableRefObject refParameters)
        {
            if (dataReaderEngine == null)
            {
                throw new ArgumentNullException("dataReaderEngine");
            }

            if (refParameters == null)
            {
                throw new ArgumentNullException("refParameters");
            }

            dataReaderEngine.Reset();

            MoveNextDataset(dataReaderEngine);

            if (HasIndex(indexAttributes, indexDataset, indexReportingPeriod, indexTimeSeries))
            {
                return null;
            }

            IDatasetHeader header = dataReaderEngine.CurrentDatasetHeader;
            ValidateTimeIndex(indexTimeSeries, header);

            // Create mutable Maintainable
            var mutableBean = BuildContentConstraintMutableObject(attachment, refParameters);

            IConstraintDataKeySetMutableObject dataKeySet = null;

            IDictionary<string, ISet<string>> cubeRegionMap = new Dictionary<string, ISet<string>>();
            IDictionary<string, ISet<string>> attributeMap = new Dictionary<string, ISet<string>>();
            DateTime? reportFrom = null;
            DateTime? reportTo = null;
            ISet<string> processedDates = new HashSet<string>();

            while (dataReaderEngine.MoveNextKeyable())
            {
                IKeyable key = dataReaderEngine.CurrentKey;
                if (key.Series)
                {
                    // TODO Check if Cross Sectional and put out exception if it is
                    // 1. If indexing the time series store the time Series Key on the Constraint
                    if (indexTimeSeries)
                    {
                        if (dataKeySet == null)
                        {
                            dataKeySet = new ConstraintDataKeySetMutableCore();
                            mutableBean.IncludedSeriesKeys = dataKeySet;
                        }

                        IConstrainedDataKeyMutableObject dataKey = new ConstrainedDataKeyMutableCore();
                        dataKey.KeyValues.AddAll(key.Key);
                        dataKeySet.AddConstrainedDataKey(dataKey);
                    }

                    // 2. If indexing the dataset, store the individual code values
                    if (indexDataset)
                    {
                        StoreKeyValuesOnMap(key.Key, cubeRegionMap);
                    }

                    // 3. If indexing attributes, store the individual code values for each attribute
                    if (indexAttributes)
                    {
                        StoreKeyValuesOnMap(key.Attributes, attributeMap);
                    }
                }

                if (indexAttributes || indexReportingPeriod)
                {
                    while (dataReaderEngine.MoveNextObservation())
                    {
                        IObservation obs = dataReaderEngine.CurrentObservation;

                        // If indexing the dates, determine the data start and end dates from the obs dates
                        // To save time, do not process the same date twice
                        if (indexReportingPeriod)
                        {
                            if (!processedDates.Contains(obs.ObsTime))
                            {
                                DateTime obsDate = obs.ObsAsTimeDate.GetValueOrDefault();
                                if (reportFrom == null || (reportFrom.Value.Ticks / 10000) > (obsDate.Ticks / 10000))
                                {
                                    reportFrom = obsDate;
                                }

                                if (reportTo == null || (reportTo.Value.Ticks / 10000) < (obsDate.Ticks / 10000))
                                {
                                    reportTo = obsDate;
                                }

                                processedDates.Add(obs.ObsTime);
                            }
                        }

                        if (indexAttributes)
                        {
                            StoreKeyValuesOnMap(obs.Attributes, attributeMap);
                        }
                    }
                }
            }

            CreateIndex(indexAttributes, indexDataset, mutableBean, attributeMap, cubeRegionMap);

            if (indexReportingPeriod && reportFrom != null)
            {
                IReferencePeriodMutableObject refPeriodMutable = new ReferencePeriodMutableCore();
                refPeriodMutable.EndTime = reportTo.Value;
                refPeriodMutable.StartTime = reportFrom.Value;
                mutableBean.ReferencePeriod = refPeriodMutable;
            }

            return mutableBean.ImmutableInstance;
        }

        /// <summary>
        /// Builds the content constraint mutable object.
        /// </summary>
        /// <param name="attachment">The attachment.</param>
        /// <param name="refParameters">The reference parameters.</param>
        /// <returns>The <see cref="IContentConstraintMutableObject"/></returns>
        private static IContentConstraintMutableObject BuildContentConstraintMutableObject(IStructureReference attachment, IMaintainableRefObject refParameters)
        {
            IContentConstraintMutableObject mutableBean = new ContentConstraintMutableCore();
            mutableBean.AgencyId = refParameters.AgencyId;
            mutableBean.Id = refParameters.MaintainableId;
            mutableBean.Version = refParameters.Version;
            mutableBean.AddName("en", "Generated Constraint");
            mutableBean.AddDescription("en", "Constraint built from dataset");
            mutableBean.IsDefiningActualDataPresent = true;
            mutableBean.ConstraintAttachment = BuildAttachement(attachment);
            return mutableBean;
        }

        /// <summary>
        /// Validates the index of the time.
        /// </summary>
        /// <param name="indexTimeSeries">if set to <c>true</c> [index time series].</param>
        /// <param name="header">The header.</param>
        /// <exception cref="SdmxSemmanticException">Can not index time series for registered data source, the data retrieved from the data source is not time series</exception>
        private static void ValidateTimeIndex(bool indexTimeSeries, IDatasetHeader header)
        {
            if (indexTimeSeries && !header.Timeseries)
            {
                throw new SdmxSemmanticException("Can not index time series for registered datasource, the data retrieved from the datasource is not time series");
            }
        }

        /// <summary>
        /// Determines whether the specified index attributes has index.
        /// </summary>
        /// <param name="indexAttributes">if set to <c>true</c> [index attributes].</param>
        /// <param name="indexDataset">if set to <c>true</c> [index dataset].</param>
        /// <param name="indexReportingPeriod">if set to <c>true</c> [index reporting period].</param>
        /// <param name="indexTimeSeries">if set to <c>true</c> [index time series].</param>
        /// <returns><c>true</c> if the specified index attributes has index; otherwise, <c>false</c>.</returns>
        private static bool HasIndex(bool indexAttributes, bool indexDataset, bool indexReportingPeriod, bool indexTimeSeries)
        {
            if (!indexAttributes && !indexDataset && !indexReportingPeriod && !indexTimeSeries)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Moves the next dataset.
        /// </summary>
        /// <param name="dataReaderEngine">The data reader engine.</param>
        /// <exception cref="SdmxSemmanticException">Can not index time series for registered data source, the data retrieved from the data source does not contain a dataset</exception>
        private static void MoveNextDataset(IDataReaderEngine dataReaderEngine)
        {
            // TODO Should it check if there is more then one dataset in the datasource
            if (!dataReaderEngine.MoveNextDataset())
            {
                throw new SdmxSemmanticException("Can not index time series for registered datasource, the data retrieved from the datasource does not contain a dataset");
            }
        }

        /// <summary>
        /// Creates the index.
        /// </summary>
        /// <param name="indexAttributes">if set to <c>true</c> [index attributes].</param>
        /// <param name="indexDataset">if set to <c>true</c> [index dataset].</param>
        /// <param name="mutableBean">The mutable bean.</param>
        /// <param name="attributeMap">The attribute map.</param>
        /// <param name="cubeRegionMap">The cube region map.</param>
        private static void CreateIndex(bool indexAttributes, bool indexDataset, IContentConstraintMutableObject mutableBean, IDictionary<string, ISet<string>> attributeMap, IDictionary<string, ISet<string>> cubeRegionMap)
        {
            if (indexAttributes || indexDataset)
            {
                ICubeRegionMutableObject cubeRegionMutableBean = new CubeRegionMutableCore();
                mutableBean.IncludedCubeRegion = cubeRegionMutableBean;
                if (indexAttributes)
                {
                    CreateKeyValues(attributeMap, cubeRegionMutableBean.AttributeValues);
                }

                if (indexDataset)
                {
                    CreateKeyValues(cubeRegionMap, cubeRegionMutableBean.KeyValues);
                }
            }
        }

        /// <summary>
        ///     Build attachment.
        /// </summary>
        /// <param name="attachment">
        ///     The attachment.
        /// </param>
        /// <returns>
        ///     The <see cref="IConstraintAttachmentMutableObject" />.
        /// </returns>
        private static IConstraintAttachmentMutableObject BuildAttachement(IStructureReference attachment)
        {
            IConstraintAttachmentMutableObject mutable = new ContentConstraintAttachmentMutableCore();
            mutable.AddStructureReference(attachment);
            return mutable;
        }

        /// <summary>
        ///     The create key values.
        /// </summary>
        /// <param name="cubeRegionMap">
        ///     The cube region map.
        /// </param>
        /// <param name="populateMap">
        ///     The populate map.
        /// </param>
        private static void CreateKeyValues(
            IDictionary<string, ISet<string>> cubeRegionMap, 
            ICollection<IKeyValuesMutable> populateMap)
        {
            foreach (var currentConcept in cubeRegionMap)
            {
                IKeyValuesMutable kvs = new KeyValuesMutableImpl();
                kvs.Id = currentConcept.Key;
                kvs.KeyValues.AddAll(currentConcept.Value);
                populateMap.Add(kvs);
            }
        }

        /// <summary>
        ///     Store key values on map.
        /// </summary>
        /// <param name="keyValues">
        ///     The key value list.
        /// </param>
        /// <param name="cubeRegionMap">
        ///     The cube region map.
        /// </param>
        private static void StoreKeyValuesOnMap(
            IEnumerable<IKeyValue> keyValues, 
            IDictionary<string, ISet<string>> cubeRegionMap)
        {
            /* foreach */
            foreach (IKeyValue kv in keyValues)
            {
                ISet<string> valuesForConcept;
                if (!cubeRegionMap.TryGetValue(kv.Concept, out valuesForConcept))
                {
                    valuesForConcept = new HashSet<string>();
                    cubeRegionMap.Add(kv.Concept, valuesForConcept);
                }

                valuesForConcept.Add(kv.Code);
            }
        }
    }
}