// -----------------------------------------------------------------------
// <copyright file="SdmxObjectsJsonBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-07-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using Newtonsoft.Json;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;

namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.SdmxObjects
{
    using Api.Model.Mutable.CategoryScheme;
    using Api.Model.Mutable.Codelist;
    using Api.Model.Mutable.ConceptScheme;
    using Newtonsoft.Json.Linq;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Sdmx.SdmxObjects.Model.Mutable.ConceptScheme;
    using Sdmx.SdmxObjects.Model.Objects.Codelist;
    using Sdmx.SdmxObjects.Model.Objects.ConceptScheme;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    /// <summary>
    ///     This is the Parser for Json V1 Structure message.
    ///     For the time being it handles Dataflow, AgencyScheme, CategoryScheme, Categorisation, ConceptScheme, CodeList,DataStructure requests
    /// </summary>
    public class SdmxObjectsJsonV1Builder : AbstractSdmxObjectsBuilder, IBuilder<ISdmxObjects, JObject>
    {
        /// <summary>
        /// The _sdmx structure json format.
        /// </summary>
        private readonly SdmxStructureJsonFormat _sdmxStructureJsonFormat;


        /// <summary>
        /// Initializes a new instance of the <see cref="SdmxObjectsJsonV1Builder"/> class. 
        /// Main constructor based on a given SdmxStructureJsonFormat
        /// </summary>
        /// <param name="sdmxStructureJsonFormat">
        /// This parameter is used to get the default language and the Type of the Request
        /// </param>
        public SdmxObjectsJsonV1Builder(SdmxStructureJsonFormat sdmxStructureJsonFormat)
        {
            if (sdmxStructureJsonFormat == null)
            {
                throw new ArgumentNullException("sdmxStructureJsonFormat");
            }

            this._sdmxStructureJsonFormat = sdmxStructureJsonFormat;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SdmxObjectsJsonV1Builder"/> class.
        /// </summary>
        public SdmxObjectsJsonV1Builder()
        {
        }

        /// <summary>
        /// Build beans from Json Structure Document
        /// </summary>
        /// <param name="buildFrom">
        /// The structures Doc.
        /// </param>
        /// <returns>
        /// The <see cref="ISdmxObjects"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="buildFrom"/> is <see langword="null"/>.
        /// </exception>
        public ISdmxObjects Build(JObject buildFrom)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            JToken headerToken;
            if (buildFrom.TryGetValue("meta", StringComparison.OrdinalIgnoreCase, out headerToken))
            {
                return this.Build(buildFrom, this.ProcessHeader(headerToken), null);
            }
            else
            {
                return new SdmxObjectsImpl();
            }
        }

        /// <summary>
        /// Build SDMX objects from v2.1 structures
        /// </summary>
        /// <param name="structures">
        /// The structures
        /// </param>
        /// <param name="header">
        /// The header.
        /// </param>
        /// <param name="action">
        /// The action.
        /// </param>
        /// <returns>
        /// the container of all beans built
        /// </returns>
        private ISdmxObjects Build(JObject structures, IHeader header, DatasetAction action)
        {
            var beans = new SdmxObjectsImpl(header, action);
            JObject data = (JObject)structures["data"];

            if (data != null && data.Count > 0)
            {
                this.ProcessDataflows(data["dataflows"], beans);
                this.ProcessAgencySchemes(data["agencySchemes"], beans);
                this.ProcessCategorySchemes(data["categorySchemes"], beans);
                this.ProcessCategorisations(data["categorisations"], beans);
                this.ProcessConceptSchemes(data["conceptSchemes"], beans);
                this.ProcessCodeLists(data["codelists"], beans);
                this.ProcessDatastructures(data["dataStructures"], beans);
            }

            return beans;
        }

        private void ProcessDatastructures(JToken dataStructures, SdmxObjectsImpl beans)
        {
            if (dataStructures == null || beans == null)
            {
                return;
            }

            foreach (var item in dataStructures)
            {
                var dsd = JsonConvert.DeserializeObject<DataStructureDTO>(item.ToString());

                IDataStructureMutableObject dataStructureMutableCore = new DataStructureMutableCore();
                foreach (var name in dsd.Names)
                {
                    dataStructureMutableCore.AddName(name.Key, name.Value);
                }
                if (dsd.Description != null)
                {
                    foreach (var desc in dsd.Descriptions)
                    {
                        dataStructureMutableCore.AddName(desc.Key, desc.Value);
                    }
                }
                dataStructureMutableCore.Id = dsd.Id;
                dataStructureMutableCore.AgencyId = dsd.AgencyID;
                dataStructureMutableCore.Version = dsd.Version;
                if (dsd.ValidFrom > DateTime.MinValue)
                {
                    dataStructureMutableCore.StartDate = dsd.ValidFrom;
                }
                if (dsd.ValidTo > DateTime.MinValue)
                {
                    dataStructureMutableCore.EndDate = dsd.ValidTo;
                }

                dataStructureMutableCore.FinalStructure = TertiaryBool.ParseBoolean(dsd.IsFinal);

                AddAnnotations(dsd.Annotations, dataStructureMutableCore);
                if (dsd.DataStructureComponents != null)
                {
                    if (dsd.DataStructureComponents.AttributeList?.Attributes != null)
                    {
                        foreach (var itemAttr in dsd.DataStructureComponents.AttributeList.Attributes)
                        {
                            IAttributeMutableObject attr = new AttributeMutableCore();
                            attr.Id = itemAttr.Id;
                            attr.AssignmentStatus = itemAttr.AssignmentStatus;
                            attr.ConceptRef = SetConceptRef(itemAttr.ConceptIdentity);
                            if (itemAttr.ConceptRoles != null)
                            {
                                foreach (var itemRole in itemAttr.ConceptRoles)
                                {
                                    attr.AddConceptRole(SetConceptRole(itemRole));
                                }
                            }
                            attr.Representation = SetRappresentation(itemAttr.LocalRepresentation, true);
                            if (itemAttr.AttributeRelationship != null)
                            {
                                if (!string.IsNullOrWhiteSpace(itemAttr.AttributeRelationship.PrimaryMeasure))
                                {
                                    attr.PrimaryMeasureReference = itemAttr.AttributeRelationship.PrimaryMeasure;
                                    attr.AttachmentLevel = AttributeAttachmentLevel.Observation;
                                }
                                else if (itemAttr.AttributeRelationship.AttachmentGroups != null && itemAttr.AttributeRelationship.AttachmentGroups.Length > 0)
                                {
                                    attr.AttachmentGroup = itemAttr.AttributeRelationship.AttachmentGroups[0];
                                    attr.AttachmentLevel = AttributeAttachmentLevel.Group;
                                }
                                else if (itemAttr.AttributeRelationship.Dimensions != null)
                                {
                                    attr.AttachmentLevel = AttributeAttachmentLevel.DimensionGroup;
                                    foreach (var itemStr in itemAttr.AttributeRelationship.Dimensions)
                                    {
                                        attr.DimensionReferences.Add(itemStr);
                                    }
                                }
                                else
                                {
                                    attr.AttachmentLevel = AttributeAttachmentLevel.DataSet;
                                }
                            }
                            AddAnnotations(itemAttr.Annotations, attr);
                            dataStructureMutableCore.AddAttribute(attr);
                        }
                    }

                    if (dsd.DataStructureComponents.DimensionList != null)
                    {
                        var allDim = new Dictionary<int, IDimensionMutableObject>();
                        if (dsd.DataStructureComponents.DimensionList.Dimensions != null)
                        {
                            foreach (var itemDim in dsd.DataStructureComponents.DimensionList.Dimensions)
                            {
                                IDimensionMutableObject dim = new DimensionMutableCore();
                                dim.Id = itemDim.Id;
                                if (itemDim.ConceptRoles != null)
                                {
                                    foreach (var itemRole in itemDim.ConceptRoles)
                                    {
                                        dim.ConceptRole.Add(SetConceptRole(itemRole));
                                    }
                                }
                                dim.ConceptRef = SetConceptRef(itemDim.ConceptIdentity);
                                dim.Representation = SetRappresentation(itemDim.LocalRepresentation, true);
                                if (dim.Id.Equals("FREQ", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    dim.FrequencyDimension = true;
                                }
                                AddAnnotations(itemDim.Annotations, dim);
                                var i = itemDim.Position;
                                while (allDim.ContainsKey(i))
                                {
                                    i++;
                                }
                                allDim.Add(i, dim);
                            }
                        }

                        if (dsd.DataStructureComponents.DimensionList.MeasureDimensions != null)
                        {
                            foreach (var itemDim in dsd.DataStructureComponents.DimensionList.MeasureDimensions)
                            {
                                IDimensionMutableObject dim = new DimensionMutableCore();
                                dim.Id = itemDim.Id;
                                if (itemDim.ConceptRoles != null)
                                {
                                    foreach (var itemRole in itemDim.ConceptRoles)
                                    {
                                        dim.ConceptRole.Add(SetConceptRole(itemRole));
                                    }
                                }
                                dim.MeasureDimension = true;
                                dim.ConceptRef = SetConceptRef(itemDim.ConceptIdentity);
                                dim.Representation = SetRappresentation(itemDim.LocalRepresentation, false);
                                DataStructureDTO.Annotation[] annotations = itemDim.Annotations;
                                AddAnnotations(annotations, dim);
                                var i = itemDim.Position;
                                while (allDim.ContainsKey(i))
                                {
                                    i++;
                                }
                                allDim.Add(i, dim);
                            }
                        }
                        if (dsd.DataStructureComponents.DimensionList.TimeDimensions != null)
                        {
                            foreach (var itemDim in dsd.DataStructureComponents.DimensionList.TimeDimensions)
                            {
                                IDimensionMutableObject dim = new DimensionMutableCore();
                                dim.Id = itemDim.Id;
                                if (itemDim.ConceptRoles != null)
                                {
                                    foreach (var itemRole in itemDim.ConceptRoles)
                                    {
                                        dim.ConceptRole.Add(SetConceptRole(itemRole));
                                    }
                                }
                                dim.TimeDimension = true;
                                dim.ConceptRef = SetConceptRef(itemDim.ConceptIdentity);
                                dim.Representation = SetRappresentation(itemDim.LocalRepresentation, true);
                                AddAnnotations(itemDim.Annotations, dim);
                                var i = itemDim.Position;
                                while (allDim.ContainsKey(i))
                                {
                                    i++;
                                }
                                allDim.Add(i, dim);
                            }
                        }

                        var list = allDim.Keys.ToList();
                        list.Sort();
                        foreach (var key in list)
                        {
                            dataStructureMutableCore.AddDimension(allDim[key]);
                        }
                    }

                    if (dsd.DataStructureComponents.MeasureList != null && dsd.DataStructureComponents.MeasureList.PrimaryMeasure != null)
                    {
                        dataStructureMutableCore.PrimaryMeasure = new PrimaryMeasureMutableCore();
                        dataStructureMutableCore.PrimaryMeasure.ConceptRef = SetConceptRef(dsd.DataStructureComponents.MeasureList.PrimaryMeasure.ConceptIdentity);
                        dataStructureMutableCore.PrimaryMeasure.Representation = SetRappresentation(dsd.DataStructureComponents.MeasureList.PrimaryMeasure.LocalRepresentation, true);
                        AddAnnotations(dsd.DataStructureComponents.MeasureList.PrimaryMeasure.Annotations, dataStructureMutableCore.PrimaryMeasure);
                    }

                    if (dsd.DataStructureComponents.Groups != null)
                    {
                        foreach (var itemGrp in dsd.DataStructureComponents.Groups)
                        {
                            IGroupMutableObject group = new GroupMutableCore();
                            group.Id = itemGrp.Id;
                            foreach (var itemGrpDim in itemGrp.GroupDimensions)
                            {
                                group.DimensionRef.Add(itemGrpDim);
                            }
                            dataStructureMutableCore.AddGroup(group);
                        }
                    }

                }
                beans.AddDataStructure(dataStructureMutableCore.ImmutableInstance);
            }
        }

        private static void AddAnnotations(DataStructureDTO.Annotation[] annotations, IAnnotableMutableObject annotable)
        {
            if (annotations != null)
            {
                foreach (var itemAnn in annotations)
                {
                    var annItemCore = new AnnotationMutableCore();
                    if (!string.IsNullOrWhiteSpace(itemAnn.Id))
                    {
                        annItemCore.Id = itemAnn.Id;
                    }
                    if (!string.IsNullOrWhiteSpace(itemAnn.Title))
                    {
                        annItemCore.Title = itemAnn.Title;
                    }
                    if (!string.IsNullOrWhiteSpace(itemAnn.Type))
                    {
                        annItemCore.Type = itemAnn.Type;
                    }
                    if (itemAnn.Texts != null)
                    {
                        foreach (var itemTxt in itemAnn.Texts)
                        {
                            annItemCore.AddText(new TextTypeWrapperMutableCore { Locale = itemTxt.Key, Value = itemTxt.Value });
                        }
                    }

                    // HACK not clear conversion between XML AnnotationUri and Json annotation.links
                    if (itemAnn.Links != null)
                    {
                        var uri = itemAnn.Links.FirstOrDefault(x => !string.IsNullOrWhiteSpace(x.Urn)).Urn ?? itemAnn.Links.FirstOrDefault(x => !string.IsNullOrWhiteSpace(x.Url)).Url;
                        if (!string.IsNullOrWhiteSpace(uri))
                        {
                            annItemCore.Uri = new Uri(uri);
                        }
                    }

                    annotable.AddAnnotation(annItemCore);
                }
            }
        }

        private IStructureReference SetConceptRole(string urn)
        {
            // item role should be a URN
            return new StructureReferenceImpl(urn);
        }

        private StructureReferenceImpl SetConceptRef(string conceptIdentity)
        {
            if (string.IsNullOrWhiteSpace(conceptIdentity))
            {
                return null;
            }
            var conceptIdentitySplit = conceptIdentity.Split('=');
            var conceptAgencyId = "";
            var conceptId = "";
            var conceptVersion = "";
            string conceptType = null;
            if (conceptIdentitySplit.Length > 1)
            {
                conceptIdentitySplit[1] = conceptIdentitySplit[1].Replace('(', '+').Replace(')', '+').Replace(':', '+');
                var tmpSplit = conceptIdentitySplit[1].Split('+');
                conceptAgencyId = tmpSplit[0];
                conceptId = tmpSplit[1];
                conceptVersion = tmpSplit[2];
                if (tmpSplit.Length > 3)
                {
                    conceptType = tmpSplit[3].Replace(".", "");
                }
                return new StructureReferenceImpl
                (
                    conceptAgencyId,
                    conceptId,
                    conceptVersion,
                    SdmxStructureEnumType.Concept,
                    conceptType
                );
            }
            return null;
        }
        private RepresentationMutableCore SetRappresentation(DataStructureDTO.Localrepresentation representation, bool codeList)
        {
            if (representation != null && !string.IsNullOrWhiteSpace(representation.Enumeration))
            {
                var rapIdentitySplit = representation.Enumeration.Split('=');
                var rapAgencyId = "";
                var rapId = "";
                var rapVersion = "";
                if (rapIdentitySplit.Length > 1)
                {
                    rapIdentitySplit[1] = rapIdentitySplit[1].Replace('(', '+').Replace(')', '+').Replace(':', '+');
                    var tmpSplit = rapIdentitySplit[1].Split('+');
                    rapAgencyId = tmpSplit[0];
                    rapId = tmpSplit[1];
                    rapVersion = tmpSplit[2];
                    return new RepresentationMutableCore
                    {
                        Representation =
                        new StructureReferenceImpl
                        (
                            rapAgencyId,
                            rapId,
                            rapVersion,
                            codeList ? SdmxStructureEnumType.CodeList : SdmxStructureEnumType.ConceptScheme,
                            null
                        )
                    };
                }
            }
            return null;
        }


        /// <summary>
        /// Create Annotations based on the input Annotations
        /// </summary>
        /// <param name="data">
        /// JArray of Annotations
        /// </param>
        /// /// <param name="annotableMutableObject">
        /// 
        /// </param>
        /// <returns>
        /// The <see cref="IList{IAnnotationMutableObject}"/>.
        /// </returns>
        private void ProcessAnnotations(JToken data, IAnnotableMutableObject annotableMutableObject)
        {
            if (data["annotations"] != null && annotableMutableObject != null)
            {
                IAnnotationMutableObject mutable;
                JObject joAnnotation;

                foreach (JToken annotation in (JArray)data["annotations"])
                {
                    joAnnotation = (JObject)annotation;
                    mutable = new AnnotationMutableCore();

                    if (joAnnotation["id"] != null)
                    {
                        mutable.Id = joAnnotation.Value<string>("id");
                    }

                    if (joAnnotation["title"] != null)
                    {
                        mutable.Title = joAnnotation.Value<string>("title");
                    }

                    if (joAnnotation["type"] != null)
                    {
                        mutable.Type = joAnnotation.Value<string>("type");
                    }

                    if (joAnnotation["texts"] != null)
                    {
                        setLocalTexts((JObject)joAnnotation["texts"], mutable.Text);
                    }

                    if (joAnnotation["links"] != null)
                    {
                        //Todo: "links": {
                        //"$ref": "#/definitions/links",
                        //"description": "Also used to specify the Annotation URL which points to an external resource which may contain or supplement the annotation (using 'self' as relationship). If a specific behavior is desired, an annotation type should be defined which specifies the use of this field more exactly. If appropriate, a collection of links to additional external resources."
                        //mutable.Uri = new Uri(joAnnotation.Value<string>("url"));
                    }

                    annotableMutableObject.AddAnnotation(mutable);
                }
            }
        }

        /// <summary>
        /// To be used to set Local texts
        /// </summary>
        /// <param name="data"></param>
        /// <param name="textTypeWrapper"></param>
        private void setLocalTexts(JObject data, IList<ITextTypeWrapperMutableObject> textTypeWrapper)
        {
            if (data != null && textTypeWrapper != null)
            {
                foreach (var local in data)
                {
                    textTypeWrapper.Add(new TextTypeWrapperMutableCore(local.Key, local.Value.Value<string>()));
                }
            }
        }

        /// <summary>
        /// Creates AgencySchemes based on the input Agency Schemes.
        /// </summary>
        /// <param name="agencySchemes">
        /// - if null will not add anything to the beans container
        /// </param>
        /// <param name="beans">
        /// - to add AgencyScheme to beans
        /// </param>
        private void ProcessAgencySchemes(JToken agencySchemes, ISdmxObjects beans)
        {
            if (agencySchemes != null && beans != null)
            {
                var urns = new HashSet<Uri>();//Todo: See how to manage/fill this list

                AgencySchemeMutableCore asmc;
                Core core;

                foreach (var agencyScheme in agencySchemes)
                {
                    asmc = new AgencySchemeMutableCore();
                    core = this.ProcessMaintainableMutableObject((JObject)agencyScheme, asmc);

                    if (agencyScheme["isPartial"] != null)
                    {
                        asmc.IsPartial = agencyScheme.Value<bool>("isPartial");
                    }

                    ProcessItems<IAgencyMutableObject, AgencyMutableCore>((JArray)agencyScheme["agencies"], "agencies", asmc.Items);

                    try
                    {
                        this.AddIfNotDuplicateURN(beans, urns, new AgencySchemeCore(asmc));
                    }
                    catch (Exception th)
                    {
                        throw new MaintainableObjectException(th, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.AgencyScheme), core.agencyID, core.identifiable.Id, core.version);
                    }
                }
            }
        }

        /// <summary>
        /// Creates ConceptSchemes based on the input Concept Schemes.
        /// </summary>
        /// <param name="conceptSchemes"></param>
        /// <param name="beans"></param>
        private void ProcessConceptSchemes(JToken conceptSchemes, ISdmxObjects beans)
        {
            if (conceptSchemes != null && beans != null)
            {
                var urns = new HashSet<Uri>();//Todo: See how to manage/fill this list

                ConceptSchemeMutableCore csmc;
                Core core;

                foreach (var conceptScheme in conceptSchemes)
                {
                    csmc = new ConceptSchemeMutableCore();
                    core = this.ProcessMaintainableMutableObject((JObject)conceptScheme, csmc);

                    if (conceptScheme["isPartial"] != null)
                    {
                        csmc.IsPartial = conceptScheme.Value<bool>("isPartial");
                    }

                    ProcessItems<IConceptMutableObject, ConceptMutableCore>((JArray)conceptScheme["concepts"], "concepts", csmc.Items);

                    try
                    {
                        this.AddIfNotDuplicateURN(beans, urns, new ConceptSchemeObjectCore(csmc));
                    }
                    catch (Exception th)
                    {
                        throw new MaintainableObjectException(th, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConceptScheme), core.agencyID, core.identifiable.Id, core.version);
                    }
                }
            }
        }

        /// <summary>
        /// Creates categorisations based on the input categorisation schemes
        /// </summary>
        /// <param name="categorisations">
        /// - if null will not add anything to the beans container
        /// </param>
        /// <param name="beans">
        /// - to add categorisations to
        /// </param>
        private void ProcessCategorisations(JToken categorisations, ISdmxObjects beans)
        {
            var urns = new HashSet<Uri>();

            if (categorisations != null && beans != null)
            {
                CategorisationMutableCore cm;
                Core core;

                foreach (var categorisation in categorisations)
                {
                    cm = new CategorisationMutableCore();
                    core = this.ProcessMaintainableMutableObject((JObject)categorisation, cm);

                    if (categorisation["target"] != null)
                    {
                        cm.CategoryReference = new StructureReferenceImpl(categorisation.Value<string>("target"));
                    }

                    if (categorisation["source"] != null)
                    {
                        cm.StructureReference = new StructureReferenceImpl(categorisation.Value<string>("source"));
                    }

                    try
                    {
                        this.AddIfNotDuplicateURN(beans, urns, new CategorisationObjectCore(cm));
                    }
                    catch (Exception th)
                    {
                        throw new MaintainableObjectException(th, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Categorisation), core.agencyID, core.identifiable.Id, core.version);
                    }
                }
            }
        }

        /// <summary>
        /// Creates category schemes based on the input category schemes.
        /// </summary>
        /// <param name="categorySchemes">
        /// - if null will not add anything to the beans container
        /// </param>
        /// <param name="beans">
        /// - to add category schemes to
        /// </param>
        private void ProcessCategorySchemes(JToken categorySchemes, ISdmxObjects beans)
        {
            if (categorySchemes != null && beans != null)
            {
                var urns = new HashSet<Uri>();//Todo: See how to manage/fill this list

                CategorySchemeMutableCore csmc;
                Core core;

                foreach (var categoryScheme in categorySchemes)
                {
                    csmc = new CategorySchemeMutableCore();
                    core = this.ProcessMaintainableMutableObject((JObject)categoryScheme, csmc);

                    if (categoryScheme["isPartial"] != null)
                    {
                        csmc.IsPartial = categoryScheme.Value<bool>("isPartial");
                    }

                    ProcessItems<ICategoryMutableObject, CategoryMutableCore>((JArray)categoryScheme["categories"], "categories", csmc.Items);

                    try
                    {
                        this.AddIfNotDuplicateURN(beans, urns, new CategorySchemeObjectCore(csmc));
                    }
                    catch (Exception th)
                    {
                        throw new MaintainableObjectException(th, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CategoryScheme), core.agencyID, core.identifiable.Id, core.version);
                    }
                }
            }
        }

        /// <summary>
        /// Creates Codelists based on the input Code Lists.
        /// </summary>
        /// <param name="codeLists"></param>
        /// <param name="beans"></param>
        private void ProcessCodeLists(JToken codeLists, ISdmxObjects beans)
        {
            if (codeLists != null && beans != null)
            {
                var urns = new HashSet<Uri>();//Todo: See how to manage/fill this list

                CodelistMutableCore cl;
                Core core;

                foreach (var codeList in codeLists)
                {
                    cl = new CodelistMutableCore();
                    core = this.ProcessMaintainableMutableObject((JObject)codeList, cl);

                    if (codeList["isPartial"] != null)
                    {
                        cl.IsPartial = codeList.Value<bool>("isPartial");
                    }

                    ProcessItems<ICodeMutableObject, CodeMutableCore>((JArray)codeList["codes"], "codes", cl.Items);

                    try
                    {
                        this.AddIfNotDuplicateURN(beans, urns, new CodelistObjectCore(cl));
                    }
                    catch (Exception th)
                    {
                        throw new MaintainableObjectException(th, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList), core.agencyID, core.identifiable.Id, core.version);
                    }
                }
            }
        }

        /// <summary>
        /// Generic method to be used to create Items from specific Schemes
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="V"></typeparam>
        /// <param name="items"></param>
        /// <param name="type"></param>
        /// <param name="itemScheme"></param>
        private void ProcessItems<T, V>(JArray items, string type, IList<T> itemScheme) where T : class, IItemMutableObject where V : T, new()
        {
            if (items != null && itemScheme != null)
            {
                foreach (var item in items)
                {
                    T itemMutable = (new V()) as T;

                    if (itemMutable != null)
                    {
                        var identifiableObject = new IdentifiableObject((JObject)item, itemMutable.StructureType.EnumType);
                        itemMutable.Id = identifiableObject.Id;
                        itemMutable.Uri = identifiableObject.Uri;

                        ProcessNameableMutableObject((JObject)item, itemMutable);

                        var conceptMutableObject = itemMutable as IConceptMutableObject;

                        if (conceptMutableObject != null)
                        {
                            ProcessConceptSpecific((JObject)item, conceptMutableObject);
                        }

                        var codeMutableObject = itemMutable as ICodeMutableObject;

                        if (codeMutableObject != null && item["parent"] != null)
                        {
                            codeMutableObject.ParentCode = item.Value<string>("parent");
                        }

                        var hierarchicalItemMutable = itemMutable as IHierarchicalItemMutableObject<T>;

                        if (item[type] != null && hierarchicalItemMutable != null)
                        {
                            ProcessItems<T, V>((JArray)item[type], type, hierarchicalItemMutable.Items);
                        }

                        itemScheme.Add(itemMutable);
                    }
                }
            }
        }

        /// <summary>
        /// Build what is specific to a Concept
        /// </summary>
        /// <param name="conceptData"></param>
        /// <param name="conceptMutableObject"></param>
        private void ProcessConceptSpecific(JObject conceptData, IConceptMutableObject conceptMutableObject)
        {
            if (conceptData != null && conceptMutableObject != null)
            {
                var coreRepresentation = conceptData["coreRepresentation"];
                if (coreRepresentation != null)
                {
                    conceptMutableObject.CoreRepresentation = new RepresentationMutableCore();
                    if (coreRepresentation["enumeration"] != null)
                    {
                        conceptMutableObject.CoreRepresentation.Representation = new StructureReferenceImpl(coreRepresentation.Value<string>("enumeration"));
                    }

                    if (coreRepresentation["textFormat"] != null)
                    {
                        conceptMutableObject.CoreRepresentation.TextFormat = new TextFormatMutableCore();
                        ProcessTextFormat((JObject)coreRepresentation["textFormat"], conceptMutableObject.CoreRepresentation.TextFormat);
                    }
                }

                if (conceptData["isoConceptReference"] != null)
                {
                    conceptMutableObject.IsoConceptReference = new StructureReferenceImpl(conceptData.Value<string>("isoConceptReference"));
                }

                if (conceptData["parent"] != null)
                {
                    conceptMutableObject.ParentConcept = conceptData.Value<string>("parent");
                }
            }
        }

        /// <summary>
        /// Process Text Format
        /// </summary>
        /// <param name="textFormatData"></param>
        /// <param name="textFormatMutableObject"></param>
        private void ProcessTextFormat(JObject textFormatData, ITextFormatMutableObject textFormatMutableObject)
        {
            if (textFormatData != null && textFormatMutableObject != null)
            {
                if (textFormatData["textType"] != null)
                {
                    TextEnumType textType = TextEnumType.Null;
                    Enum.TryParse<TextEnumType>(textFormatData.Value<string>("textType"), true, out textType);
                    textFormatMutableObject.TextType = TextType.GetFromEnum(textType);
                }

                if (textFormatData["decimals"] != null)
                {
                    textFormatMutableObject.Decimals = textFormatData.Value<long?>("decimals");
                }

                if (textFormatData["startValue"] != null)
                {
                    textFormatMutableObject.StartValue = textFormatData.Value<decimal?>("startValue");
                }

                if (textFormatData["endValue"] != null)
                {
                    textFormatMutableObject.EndValue = textFormatData.Value<decimal?>("endValue");
                }

                if (textFormatData["interval"] != null)
                {
                    textFormatMutableObject.Interval = textFormatData.Value<decimal?>("interval");
                }

                if (textFormatData["isSequence"] != null)
                {
                    textFormatMutableObject.Sequence = TertiaryBool.ParseBoolean(textFormatData.Value<bool>("isSequence"));
                }

                if (textFormatData["maxLength"] != null)
                {
                    textFormatMutableObject.MaxLength = textFormatData.Value<long?>("maxLength");
                }

                if (textFormatData["minLength"] != null)
                {
                    textFormatMutableObject.MinLength = textFormatData.Value<long?>("minLength");
                }

                if (textFormatData["pattern"] != null)
                {
                    textFormatMutableObject.Pattern = textFormatData.Value<string>("pattern");
                }

                if (textFormatData["minValue"] != null)
                {
                    textFormatMutableObject.MinValue = textFormatData.Value<decimal?>("minValue");
                }

                if (textFormatData["maxValue"] != null)
                {
                    textFormatMutableObject.MaxValue = textFormatData.Value<decimal?>("maxValue");
                }

                if (textFormatData["startTime"] != null)
                {
                    textFormatMutableObject.StartTime = getSdmxDate(textFormatData.Value<DateTime?>("startTime"), TimeFormatEnumType.DateTime);
                }

                if (textFormatData["endTime"] != null)
                {
                    textFormatMutableObject.EndTime = getSdmxDate(textFormatData.Value<DateTime?>("endTime"), TimeFormatEnumType.DateTime);
                }

                if (textFormatData["timeInterval"] != null)
                {
                    textFormatMutableObject.TimeInterval = textFormatData.Value<string>("timeInterval");
                }

                if (textFormatData["isMultiLingual"] != null)
                {
                    textFormatMutableObject.Multilingual = TertiaryBool.ParseBoolean(textFormatData.Value<bool>("isMultiLingual"));
                }
            }
        }

        /// <summary>
        /// Provide Sdmx date from a Datetime and a given format.
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="timeFormatEnumType"></param>
        /// <returns></returns>
        private SdmxDateCore getSdmxDate(DateTime? dateTime, TimeFormatEnumType timeFormatEnumType)
        {
            return new SdmxDateCore(dateTime, timeFormatEnumType);
        }

        /// <summary>
        /// Creates dataflows based on the input dataflows.
        /// </summary>
        /// <param name="dataflows">
        /// - if null will not add anything to the beans container
        /// </param>
        /// <param name="beans">
        /// - to add dataflows to beans
        /// </param>
        private void ProcessDataflows(JToken dataflows, ISdmxObjects beans)
        {
            if (dataflows != null && beans != null)
            {
                DataflowMutableCore dmc;
                Core core;

                foreach (var dataflow in dataflows)
                {
                    if (dataflow["structure"] != null)//Todo: confirm that if no Structure is defined then we skip the DF
                    {
                        dmc = new DataflowMutableCore();
                        core = this.ProcessMaintainableMutableObject((JObject)dataflow, dmc);
                        dmc.DataStructureRef = new StructureReferenceImpl(dataflow.Value<string>("structure"));

                        try
                        {
                            this.AddIfNotDuplicateURN(beans, new HashSet<Uri>(), new DataflowObjectCore(dmc));
                        }
                        catch (Exception th)
                        {
                            throw new MaintainableObjectException(th, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow), core.agencyID, core.identifiable.Id, core.version);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Process the header.
        /// </summary>
        /// <param name="baseHeaderType">
        /// The base header type.
        /// </param>
        /// <returns>
        /// The <see cref="IHeader"/>.
        /// </returns>
        private IHeader ProcessHeader(JToken baseHeaderType)
        {
            string id = null;
            DateTime? prepared = null;
            DateTime? reportingBegin = null;
            DateTime? reportingEnd = null;
            IList<IParty> receiver = null;
            IParty sender = null;
            bool isTest = false;

            IDictionary<string, string> additionalAttributes = null;
            IList<IDatasetStructureReference> structures = null;
            IStructureReference dataProviderReference = null;
            DatasetAction datasetAction = null;
            string datasetId = null;
            DateTime? embargoDate = null;
            DateTime? extracted = null;
            IList<ITextTypeWrapper> name = null;
            IList<ITextTypeWrapper> source = null;

            if (baseHeaderType["id"] != null)
            {
                id = baseHeaderType.Value<string>("id");
            }

            if (baseHeaderType["prepared"] != null)
            {
                prepared = baseHeaderType.Value<DateTime>("prepared");
            }

            if (baseHeaderType["receiver"] != null)
            {
                JArray jaReceiver = (JArray)baseHeaderType["receiver"];

                if (jaReceiver.HasValues)
                {
                    List<ITextTypeWrapper> receiverName = new List<ITextTypeWrapper>();
                    receiver = new List<IParty>();

                    foreach (JObject joReceiver in jaReceiver)
                    {
                        if (joReceiver["name"] != null)
                        {
                            //receiverName.Add(new TextTypeWrapperImpl(this._locale, joReceiver.Value<string>("name"\), null));
                        }

                        receiver.Add(new PartyCore(receiverName, joReceiver.Value<string>("id"), new List<IContact>(), null));
                    }
                }
            }

            if (baseHeaderType["sender"] != null)
            {
                JObject joSender = (JObject)baseHeaderType["sender"];

                if (joSender.HasValues)
                {
                    List<ITextTypeWrapper> senderName = null;
                    if (joSender["name"] != null)
                    {
                        //senderName = new List<ITextTypeWrapper> { new TextTypeWrapperImpl(this._locale, joSender.Value<string>("name"), null) };
                    }

                    sender = new PartyCore(senderName, joSender.Value<string>("id"), null, null);
                }
            }

            if (baseHeaderType["reportingBegin"] != null)
            {
                reportingBegin = baseHeaderType.Value<DateTime>("reportingBegin");
            }

            if (baseHeaderType["reportingEnd"] != null)
            {
                reportingEnd = baseHeaderType.Value<DateTime>("reportingEnd");
            }

            if (baseHeaderType["test"] != null)
            {
                isTest = baseHeaderType.Value<bool>("test");
            }

            return new HeaderImpl(additionalAttributes, structures, dataProviderReference, datasetAction, id, datasetId, embargoDate, extracted, prepared, reportingBegin, reportingEnd, name, source, receiver, sender, isTest);
        }

        /// <summary>
        /// Processing base object properties to be used by IMaintainableMutableObject such as Dataflow, Category Scheme, Agency Scheme, etc.
        /// </summary>
        /// <param name="mutableCore">
        /// IMaintainableMutableObject to fill the properties
        /// </param>
        /// <param name="data">
        /// Json Object data
        /// </param>
        /// <returns>
        /// The <see cref="Core"/>.
        /// </returns>
        private Core ProcessMaintainableMutableObject(JObject data, IMaintainableMutableObject mutableCore)
        {
            Core core = new Core(data);
            mutableCore.AgencyId = core.agencyID;

            mutableCore.FinalStructure = TertiaryBool.ParseBoolean(core.isFinal);
            mutableCore.Id = core.identifiable.Id;
            mutableCore.StartDate = core.startDate;
            mutableCore.EndDate = core.endDate;
            mutableCore.Uri = core.identifiable.Uri;
            mutableCore.Version = core.version;

            ProcessNameableMutableObject(data, mutableCore);

            return core;
        }

        private void ProcessNameableMutableObject(JObject data, INameableMutableObject nameableObject)
        {
            if (data != null && nameableObject != null)
            {
                setLocalTexts((JObject)data["names"], nameableObject.Names);

                setLocalTexts((JObject)data["descriptions"], nameableObject.Descriptions);

                this.ProcessAnnotations(data, nameableObject);
            }
        }

        /// <summary>
        /// Internal class to get a core equivalent of an artefact.
        /// </summary>
        private class Core
        {
            /// <summary>
            /// The agency id.
            /// </summary>
            public string agencyID = null;

            /// <summary>
            /// The end date.
            /// </summary>
            public DateTime? endDate;

            /// <summary>
            /// The is final.
            /// </summary>
            public bool? isFinal = null;

            /// <summary>
            /// The nameable.
            /// </summary>
            public IdentifiableObject identifiable = null;

            /// <summary>
            /// The start date.
            /// </summary>
            public DateTime? startDate;

            /// <summary>
            /// The version.
            /// </summary>
            public string version = null;

            public bool? isExternalReference = null;

            /// <summary>
            /// Initializes a new instance of the <see cref="Core"/> class.
            /// </summary>
            /// <param name="jo">
            /// The jo.
            /// </param>
            public Core(JObject jo)
            {
                //Uri ServiceURL { get; set; }
                //Uri StructureURL { get; set; }
                //bool Stub { get; set; }

                if (jo["agencyID"] != null)
                {
                    this.agencyID = jo.Value<string>("agencyID");
                }//todo: Else throw exception because field is mandatory

                if (jo["isFinal"] != null)
                {
                    this.isFinal = jo.Value<bool>("isFinal");
                }

                if (jo["version"] != null)
                {
                    this.version = jo.Value<string>("version");
                }//todo: Else throw exception because field is mandatory

                if (jo["validFrom"] != null)
                {
                    this.startDate = jo.Value<DateTime>("validFrom");
                }

                if (jo["validTo"] != null)
                {
                    this.endDate = jo.Value<DateTime>("validTo");
                }

                if (jo["isExternalReference"] != null)
                {
                    this.isExternalReference = jo.Value<bool>("isExternalReference");
                }

                this.identifiable = new IdentifiableObject(jo, SdmxStructureEnumType.Any); //todo: Type
            }
        }

        /// <summary>
        /// Internal Base class to get the Identifiable Object properties
        /// </summary>
        private class IdentifiableObject : IIdentifiableMutableObject
        {
            private readonly JObject _jidentifiableObject;
            private readonly SdmxStructureType _structureType;

            /// <summary>
            /// Initializes a new instance of the <see cref="IdentifiableObject"/> class.
            /// </summary>
            /// <param name="jo"></param>
            /// <param name="structureType"></param>
            public IdentifiableObject(JObject jo, SdmxStructureEnumType structureType)
            {
                _jidentifiableObject = jo;
                //_structureType = structureType; //todo
            }

            public IList<IAnnotationMutableObject> Annotations
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            public string Id
            {
                get
                {
                    return _jidentifiableObject.Value<string>("id");
                }

                set
                {
                    throw new NotImplementedException();
                }
            }

            public SdmxStructureType StructureType
            {
                get
                {
                    return _structureType;
                }
            }

            public Uri Uri
            {
                get
                {
                    var jToken = _jidentifiableObject["uri"];
                    if (jToken == null)
                    {
                        return null;
                    }

                    var uriString = _jidentifiableObject.Value<string>("uri");

                    // uri might be returned as an empty string
                    return !string.IsNullOrWhiteSpace(uriString) ? new Uri(uriString) : null;

                }

                set
                {
                    throw new NotImplementedException();
                }
            }

            public Uri Urn
            {
                get
                {
                    return _jidentifiableObject["urn"] != null ? new Uri(_jidentifiableObject.Value<string>("urn")) : null;
                }
            }

            public IList<ILinkObject> Links { get; set; } = new List<ILinkObject>();

            public void AddAnnotation(IAnnotationMutableObject annotation)
            {
                throw new NotImplementedException();
            }

            public IAnnotationMutableObject AddAnnotation(string title, string type, string url)
            {
                throw new NotImplementedException();
            }

            public IIdentifiableMutableObject AddLink(ILinkObject link)
            {
                if (Links == null)
                {
                    Links = new List<ILinkObject>();
                }

                Links.Add(link);
                return this;
            }
        }

    }
}