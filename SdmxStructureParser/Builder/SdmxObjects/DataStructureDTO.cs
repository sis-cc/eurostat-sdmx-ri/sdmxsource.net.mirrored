// -----------------------------------------------------------------------
// <copyright file="DataStructureDTO.cs" company="EUROSTAT">
//   Date Created : --
//   Copyright (c) 2012, 2019 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.SdmxObjects
{
    internal class DataStructureDTO
    {
        public string Id { get; set; }
        public string Urn { get; set; }
        public Link[] Links { get; set; }
        public string Version { get; set; }
        public string AgencyID { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public string Name { get; set; }
        public Dictionary<string, string> Names { get; set; }
        public string Description { get; set; }
        public Dictionary<string, string> Descriptions { get; set; }
        public Datastructurecomponents DataStructureComponents { get; set; }
        public Annotation[] Annotations { get; set; }
        public bool IsFinal { get; set; }


        public class Datastructurecomponents
        {
            public Attributelist AttributeList { get; set; }
            public Dimensionlist DimensionList { get; set; }
            public Measurelist MeasureList { get; set; }
            public Group[] Groups { get; set; }
        }

        public class Attributelist
        {
            public string Id { get; set; }
            public string Urn { get; set; }
            public Link[] Links { get; set; }
            public Attribute[] Attributes { get; set; }
        }

        public class Link
        {
            public string Rel { get; set; }
            public string Urn { get; set; }
            public string Url { get; set; }
            public string Type { get; set; }
        }
        public class Annotation
        {
            public string Id { get; set; }
            public string Title { get; set; }
            public string Type { get; set; }
            public string Text { get; set; }
            public Dictionary<string, string> Texts { get; set; }
            public Link[] Links { get; set; }
        }
        public class Attribute
        {
            public string Id { get; set; }
            public string Urn { get; set; }
            public Link[] Links { get; set; }
            public string ConceptIdentity { get; set; }
            public Localrepresentation LocalRepresentation { get; set; }
            public string AssignmentStatus { get; set; }
            public Attributerelationship AttributeRelationship { get; set; }
            public Annotation[] Annotations { get; set; }
            public string[] ConceptRoles { get; set; }
        }

        public class Localrepresentation
        {
            public string Enumeration { get; set; }
            public Textformat TextFormat { get; set; }
        }

        public class Attributerelationship
        {
            public string[] AttachmentGroups { get; set; }
            public string[] Dimensions { get; set; }
            public string PrimaryMeasure { get; set; }
        }

        public class Dimensionlist
        {
            public string Id { get; set; }
            public string Urn { get; set; }
            public Link[] Links { get; set; }
            public Dimension[] Dimensions { get; set; }
            public Measuredimension[] MeasureDimensions { get; set; }
            public Timedimension[] TimeDimensions { get; set; }
        }

        public class Dimension
        {
            public string Id { get; set; }
            public string Urn { get; set; }
            public Link[] Links { get; set; }
            public int Position { get; set; }
            public string Type { get; set; }
            public string ConceptIdentity { get; set; }
            public Localrepresentation LocalRepresentation { get; set; }
            public Annotation[] Annotations { get; set; }
            public string[] ConceptRoles { get; set; }
        }

        public class Measuredimension
        {
            public string Id { get; set; }
            public string Urn { get; set; }
            public Link[] Links { get; set; }
            public int Position { get; set; }
            public string Type { get; set; }
            public string ConceptIdentity { get; set; }
            public Localrepresentation LocalRepresentation { get; set; }
            public Annotation[] Annotations { get; set; }
            public string[] ConceptRoles { get; set; }
        }

        public class Timedimension
        {
            public string Id { get; set; }
            public string Urn { get; set; }
            public Link[] Links { get; set; }
            public int Position { get; set; }
            public string Type { get; set; }
            public string ConceptIdentity { get; set; }
            public Localrepresentation LocalRepresentation { get; set; }
            public Annotation[] Annotations { get; set; }
            public string[] ConceptRoles { get; set; }
        }

        public class Textformat
        {
            public string TextType { get; set; }
            public bool IsSequence { get; set; }
            public bool IsMultiLingual { get; set; }
        }


        public class Measurelist
        {
            public string Id { get; set; }
            public string Urn { get; set; }
            public Link[] Links { get; set; }
            public Primarymeasure PrimaryMeasure { get; set; }
        }

        public class Primarymeasure
        {
            public string Id { get; set; }
            public string Urn { get; set; }
            public Link[] Links { get; set; }
            public string ConceptIdentity { get; set; }
            public Localrepresentation LocalRepresentation { get; set; }
            public Annotation[] Annotations { get; set; }
        }

        public class Group
        {
            public string Id { get; set; }
            public string Urn { get; set; }
            public Link[] Links { get; set; }
            public string[] GroupDimensions { get; set; }
        }

    }
}