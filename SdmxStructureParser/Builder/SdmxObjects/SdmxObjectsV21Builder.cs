// -----------------------------------------------------------------------
// <copyright file="SdmxObjectsV21Builder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.SdmxObjects
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Process;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Util;

    using DataStructureType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure.DataStructureType;
    using MetadataStructureType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure.MetadataStructureType;
    using StructureType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.StructureType;

    /// <summary>
    ///     The sdmx beans v 21 builder.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "It is OK. Big number of SDMX v2.1 artefacts")]
    public class SdmxObjectsV21Builder : AbstractSdmxObjectsBuilder, IBuilder<ISdmxObjects, RegistryInterface>, IBuilder<ISdmxObjects, Structure>
    {
        /// <summary>
        ///     Build beans from a v2.1 Registry Document
        /// </summary>
        /// <param name="rid">
        ///     The rid.
        /// </param>
        /// <returns>
        ///     The <see cref="ISdmxObjects" />.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="rid"/> is <see langword="null" />.</exception>
        public ISdmxObjects Build(RegistryInterface rid)
        {
            if (rid == null)
            {
                throw new ArgumentNullException("rid");
            }

            RegistryInterfaceType rit = rid.Content;
            if (rit.SubmitStructureRequest != null)
            {
                if (rit.SubmitStructureRequest.Structures != null)
                {
                    DatasetAction action = null;
                    if (!string.IsNullOrEmpty(rit.SubmitStructureRequest.action))
                    {
                        switch (rit.SubmitStructureRequest.action)
                        {
                            case ActionTypeConstants.Append:
                                action = DatasetAction.GetFromEnum(DatasetActionEnumType.Append);
                                break;
                            case ActionTypeConstants.Replace:
                                action = DatasetAction.GetFromEnum(DatasetActionEnumType.Replace);
                                break;
                            default:
                                action = DatasetAction.GetFromEnum(DatasetActionEnumType.Append);
                                break;
                        }
                    }

                    return this.Build(rit.SubmitStructureRequest.Structures.Content, ProcessHeader(rit.Header), action);
                }
            }

            return new SdmxObjectsImpl();
        }

        /// <summary>
        ///     Build beans from a v2.1 Structure Document
        /// </summary>
        /// <param name="structuresDoc">
        ///     The structures Doc.
        /// </param>
        /// <returns>
        ///     The <see cref="ISdmxObjects" />.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="structuresDoc"/> is <see langword="null" />.</exception>
        public ISdmxObjects Build(Structure structuresDoc)
        {
            if (structuresDoc == null)
            {
                throw new ArgumentNullException("structuresDoc");
            }

            StructureType structures = structuresDoc.Content;
            if (structures != null && structures.Structures != null)
            {
                return this.Build(structures.Structures, ProcessHeader(structures.Header), null);
            }

            return new SdmxObjectsImpl();
        }

        /// <summary>
        ///     Process the header.
        /// </summary>
        /// <param name="baseHeaderType">
        ///     The base header type.
        /// </param>
        /// <returns>
        ///     The <see cref="IHeader" />.
        /// </returns>
        private static IHeader ProcessHeader(BaseHeaderType baseHeaderType)
        {
            return new HeaderImpl(baseHeaderType);
        }

        /// <summary>
        /// Build SDMX objects from v2.1 structures
        /// </summary>
        /// <param name="structures">The structures</param>
        /// <param name="header">The header.</param>
        /// <param name="action">The action.</param>
        /// <returns>the container of all beans built</returns>
        private ISdmxObjects Build(StructuresType structures, IHeader header, DatasetAction action)
        {
            var beans = new SdmxObjectsImpl(header, action);
            this.ProcessOrganisationSchemes(structures.OrganisationSchemes, beans);
            this.ProcessDataflows(structures.Dataflows, beans);
            this.ProcessMetadataFlows(structures.Metadataflows, beans);
            this.ProcessCategorySchemes(structures.CategorySchemes, beans);
            this.ProcessCategorisations(structures.Categorisations, beans);
            this.ProcessCodelists(structures.Codelists, beans);
            this.ProcessHierarchicalCodelists(structures.HierarchicalCodelists, beans);
            this.ProcessConcepts(structures.Concepts, beans);
            this.ProcessMetadataStructures(structures.MetadataStructures, beans);
            this.ProcessDataStructures(structures.DataStructures, beans);
            this.ProcessStructureSets(structures.StructureSets, beans);
            this.ProcessReportingTaxonomies(structures.ReportingTaxonomies, beans);
            this.ProcessProcesses(structures.Processes, beans);
            this.ProcessConstraints(structures.Constraints, beans);
            this.ProcessProvisions(structures.ProvisionAgreements, beans);
            return beans;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////
        //////////            VERSION 2.1 METHODS FOR STRUCTURES          ///////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Creates categorisations based on the input categorisation schemes
        /// </summary>
        /// <param name="categorisations">
        ///     - if null will not add anything to the beans container
        /// </param>
        /// <param name="beans">
        ///     - to add categorisations to
        /// </param>
        private void ProcessCategorisations(CategorisationsType categorisations, ISdmxObjects beans)
        {
            var urns = new HashSet<Uri>();
            if (categorisations != null && ObjectUtil.ValidCollection(categorisations.Categorisation))
            {
                /* foreach */
                foreach (CategorisationType currentCategorisation in categorisations.Categorisation)
                {
                    ICategorisationObject categorisationObject = new CategorisationObjectCore(currentCategorisation);
                    this.AddIfNotDuplicateURN(beans, urns, categorisationObject);
                }
            }
        }

        /// <summary>
        ///     Creates category schemes based on the input category schemes
        /// </summary>
        /// <param name="catSchemes">
        ///     - if null will not add anything to the beans container
        /// </param>
        /// <param name="beans">
        ///     - to add category schemes to
        /// </param>
        private void ProcessCategorySchemes(CategorySchemesType catSchemes, ISdmxObjects beans)
        {
            var urns = new HashSet<Uri>();
            if (catSchemes != null && ObjectUtil.ValidCollection(catSchemes.CategoryScheme))
            {
                /* foreach */
                foreach (CategorySchemeType currentCatScheme in catSchemes.CategoryScheme)
                {
                    ICategorySchemeObject categorySchemeObject = new CategorySchemeObjectCore(currentCatScheme);
                    this.AddIfNotDuplicateURN(beans, urns, categorySchemeObject);
                }
            }
        }

        /// <summary>
        ///     Creates Codelist based on the input Codelist schemes
        /// </summary>
        /// <param name="codelists">
        ///     - if null will not add anything to the beans container
        /// </param>
        /// <param name="beans">
        ///     - to add codelist to
        /// </param>
        private void ProcessCodelists(CodelistsType codelists, ISdmxObjects beans)
        {
            var urns = new HashSet<Uri>();
            if (codelists != null && codelists.Codelist != null)
            {
                /* foreach */
                foreach (CodelistType currentType in codelists.Codelist)
                {
                    this.AddIfNotDuplicateURN(beans, urns, new CodelistObjectCore(currentType));
                }
            }
        }

        /// <summary>
        ///     Creates ConceptSchemes based on the input Concepts
        /// </summary>
        /// <param name="concepts">
        ///     - if null will not add anything to the beans container
        /// </param>
        /// <param name="beans">
        ///     - to add concepts to
        /// </param>
        private void ProcessConcepts(ConceptsType concepts, ISdmxObjects beans)
        {
            var urns = new HashSet<Uri>();
            if (concepts != null && concepts.ConceptScheme != null)
            {
                /* foreach */
                foreach (ConceptSchemeType currentType in concepts.ConceptScheme)
                {
                    this.AddIfNotDuplicateURN(beans, urns, new ConceptSchemeObjectCore(currentType));
                }
            }
        }

        /// <summary>
        ///     Creates Constraints based on the input Constraints
        /// </summary>
        /// <param name="constraints">
        ///     - if null will not add anything to the beans container
        /// </param>
        /// <param name="beans">
        ///     - to add concepts to
        /// </param>
        private void ProcessConstraints(ConstraintsType constraints, ISdmxObjects beans)
        {
            var urns = new HashSet<Uri>();
            if (constraints != null)
            {
                /* foreach */
                foreach (AttachmentConstraintType currentType in constraints.AttachmentConstraint)
                {
                        this.AddIfNotDuplicateURN(beans, urns, new AttachmentConstraintObjectCore(currentType));
                }

                /* foreach */
                foreach (ContentConstraintType currentType0 in constraints.ContentConstraint)
                {
                    this.AddIfNotDuplicateURN(beans, urns, new ContentConstraintObjectCore(currentType0));
                }
            }
        }

        /// <summary>
        ///     Creates dataflows and categorisations based on the input dataflows
        /// </summary>
        /// <param name="dataflowsType">
        ///     - if null will not add anything to the beans container
        /// </param>
        /// <param name="beans">
        ///     - to add dataflows to beans
        /// </param>
        private void ProcessDataflows(DataflowsType dataflowsType, ISdmxObjects beans)
        {
            var urns = new HashSet<Uri>();
            if (dataflowsType != null && ObjectUtil.ValidCollection(dataflowsType.Dataflow))
            {
                /* foreach */
                foreach (DataflowType currentType in dataflowsType.Dataflow)
                {
                    IDataflowObject currentDataflow = new DataflowObjectCore(currentType);
                    this.AddIfNotDuplicateURN(beans, urns, currentDataflow);
                }
            }
        }

        /// <summary>
        ///     Creates DataStructures based on the input DataStructures
        /// </summary>
        /// <param name="keyfamilies">
        ///     - if null will not add anything to the beans container
        /// </param>
        /// <param name="beans">
        ///     - to add concepts to
        /// </param>
        private void ProcessDataStructures(DataStructuresType keyfamilies, ISdmxObjects beans)
        {
            var urns = new HashSet<Uri>();
            if (keyfamilies != null && keyfamilies.DataStructure != null)
            {
                /* foreach */
                foreach (DataStructureType currentType in keyfamilies.DataStructure)
                {
                    this.AddIfNotDuplicateURN(beans, urns, new DataStructureObjectCore(currentType));
                }
            }
        }

        /// <summary>
        ///     Creates HierarchicalCodelists based on the input HierarchicalCodelist schemes
        /// </summary>
        /// <param name="hcodelists">
        ///     - if null will not add anything to the beans container
        /// </param>
        /// <param name="beans">
        ///     - to add hierarchical codelists to
        /// </param>
        private void ProcessHierarchicalCodelists(HierarchicalCodelistsType hcodelists, ISdmxObjects beans)
        {
            var urns = new HashSet<Uri>();
            if (hcodelists != null && hcodelists.HierarchicalCodelist != null)
            {
                /* foreach */
                foreach (HierarchicalCodelistType currentType in hcodelists.HierarchicalCodelist)
                {
                    this.AddIfNotDuplicateURN(beans, urns, new HierarchicalCodelistObjectCore(currentType));
                }
            }
        }

        /// <summary>
        ///     Creates metadataflows on the input metadataflows
        /// </summary>
        /// <param name="mdfType">
        ///     - if null will not add anything to the beans container
        /// </param>
        /// <param name="beans">
        ///     - to add metadataflow to beans
        /// </param>
        private void ProcessMetadataFlows(MetadataflowsType mdfType, ISdmxObjects beans)
        {
            var urns = new HashSet<Uri>();
            if (mdfType != null && ObjectUtil.ValidCollection(mdfType.Metadataflow))
            {
                /* foreach */
                foreach (MetadataflowType currentType in mdfType.Metadataflow)
                {
                    IMetadataFlow currentMetadataflow = new MetadataflowObjectCore(currentType);
                    this.AddIfNotDuplicateURN(beans, urns, currentMetadataflow);
                }
            }
        }

        /// <summary>
        ///     Creates MetadataStructures based on the input MetadataStructures
        /// </summary>
        /// <param name="metadataStructures">
        ///     - if null will not add anything to the beans container
        /// </param>
        /// <param name="beans">
        ///     - to add concepts to
        /// </param>
        private void ProcessMetadataStructures(MetadataStructuresType metadataStructures, ISdmxObjects beans)
        {
            var urns = new HashSet<Uri>();
            if (metadataStructures != null && metadataStructures.MetadataStructure != null)
            {
                /* foreach */
                foreach (MetadataStructureType currentType in metadataStructures.MetadataStructure)
                {
                    this.AddIfNotDuplicateURN(beans, urns, new MetadataStructureDefinitionObjectCore(currentType));
                }
            }
        }

        /// <summary>
        ///     Creates organisation schemes and agencies based on the input organisation schemes
        /// </summary>
        /// <param name="orgSchemesType">
        ///     - if null will not add anything to the beans container
        /// </param>
        /// <param name="beans">
        ///     - to add organisation schemes and agencies to
        /// </param>
        private void ProcessOrganisationSchemes(OrganisationSchemesType orgSchemesType, ISdmxObjects beans)
        {
            var urns = new HashSet<Uri>();
            if (orgSchemesType != null)
            {
                if (ObjectUtil.ValidCollection(orgSchemesType.AgencyScheme))
                {
                    /* foreach */
                    foreach (AgencySchemeType currentType in orgSchemesType.AgencyScheme)
                    {
                        this.AddIfNotDuplicateURN(beans, urns, new AgencySchemeCore(currentType));
                    }
                }

                if (ObjectUtil.ValidCollection(orgSchemesType.DataProviderScheme))
                {
                    /* foreach */
                    foreach (DataProviderSchemeType currentType0 in orgSchemesType.DataProviderScheme)
                    {
                        this.AddIfNotDuplicateURN(beans, urns, new DataProviderSchemeCore(currentType0));
                    }
                }

                if (ObjectUtil.ValidCollection(orgSchemesType.DataConsumerScheme))
                {
                    /* foreach */
                    foreach (DataConsumerSchemeType currentType2 in orgSchemesType.DataConsumerScheme)
                    {
                        this.AddIfNotDuplicateURN(beans, urns, new DataConsumerSchemeCore(currentType2));
                    }
                }

                if (ObjectUtil.ValidCollection(orgSchemesType.OrganisationUnitScheme))
                {
                    /* foreach */
                    foreach (OrganisationUnitSchemeType currentType4 in orgSchemesType.OrganisationUnitScheme)
                    {
                        this.AddIfNotDuplicateURN(beans, urns, new OrganisationUnitSchemeObjectCore(currentType4));
                    }
                }
            }
        }

        /// <summary>
        ///     Creates Processes based on the input Processes
        /// </summary>
        /// <param name="processes">
        ///     - if null will not add anything to the beans container
        /// </param>
        /// <param name="beans">
        ///     - to add concepts to
        /// </param>
        private void ProcessProcesses(ProcessesType processes, ISdmxObjects beans)
        {
            var urns = new HashSet<Uri>();
            if (processes != null && processes.Process != null)
            {
                /* foreach */
                foreach (ProcessType currentType in processes.Process)
                {
                    this.AddIfNotDuplicateURN(beans, urns, new ProcessObjectCore(currentType));
                }
            }
        }

        /// <summary>
        ///     Process the provisions.
        /// </summary>
        /// <param name="provisions">
        ///     The provisions.
        /// </param>
        /// <param name="beans">
        ///     The beans.
        /// </param>
        /// <exception cref="MaintainableObjectException">
        ///     Duplicate URN
        /// </exception>
        private void ProcessProvisions(ProvisionAgreementsType provisions, ISdmxObjects beans)
        {
            var urns = new HashSet<Uri>();
            if (provisions != null && ObjectUtil.ValidCollection(provisions.ProvisionAgreement))
            {
                /* foreach */
                foreach (ProvisionAgreementType currentType in provisions.ProvisionAgreement)
                {
                    this.AddIfNotDuplicateURN(beans, urns, new ProvisionAgreementObjectCore(currentType));
                }
            }
        }

        /// <summary>
        ///     Creates ReportingTaxonomies based on the input ReportingTaxonomies
        /// </summary>
        /// <param name="reportingTaxonomies">
        ///     - if null will not add anything to the beans container
        /// </param>
        /// <param name="beans">
        ///     - to add concepts to
        /// </param>
        private void ProcessReportingTaxonomies(ReportingTaxonomiesType reportingTaxonomies, ISdmxObjects beans)
        {
            var urns = new HashSet<Uri>();
            if (reportingTaxonomies != null && reportingTaxonomies.ReportingTaxonomy != null)
            {
                /* foreach */
                foreach (ReportingTaxonomyType currentType in reportingTaxonomies.ReportingTaxonomy)
                {
                    this.AddIfNotDuplicateURN(beans, urns, new ReportingTaxonomyObjectCore(currentType));
                }
            }
        }

        /// <summary>
        ///     Creates StructureSets based on the input StructureSets
        /// </summary>
        /// <param name="structureSets">
        ///     - if null will not add anything to the beans container
        /// </param>
        /// <param name="beans">
        ///     - to add concepts to
        /// </param>
        private void ProcessStructureSets(StructureSetsType structureSets, ISdmxObjects beans)
        {
            var urns = new HashSet<Uri>();
            if (structureSets != null && structureSets.StructureSet != null)
            {
                /* foreach */
                foreach (StructureSetType currentType in structureSets.StructureSet)
                {
                    this.AddIfNotDuplicateURN(beans, urns, new StructureSetObjectCore(currentType));
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////
        //////////            PROCESS 2.1  MESSAGES                        ///////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////
    }
}