// -----------------------------------------------------------------------
// <copyright file="SdmxObjectsV2RegDocBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.SdmxObjects
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Message;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Registry;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    using StructureType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Registry.StructureType;

    /// <summary>
    ///     The sdmx beans v 2 registry interface doc builder.
    /// </summary>
    public class SdmxObjectsV2RegDocBuilder : AbstractSdmxObjectsV2Builder, IBuilder<ISdmxObjects, RegistryInterface>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SdmxObjectsV2RegDocBuilder"/> class.
        /// </summary>
        /// <param name="conceptRoleBuilder">The concept role builder.</param>
        public SdmxObjectsV2RegDocBuilder(IBuilder<IStructureReference, ComponentRole> conceptRoleBuilder)
            : base(conceptRoleBuilder)
        {
        }

        /// <summary>
        ///     Builds an <see cref="ISdmxObjects" /> object from the specified <paramref name="registryInterface" />
        /// </summary>
        /// <param name="registryInterface">
        ///     An <see cref="RegistryInterface" /> to build the output object from
        /// </param>
        /// <returns>
        ///     an <see cref="ISdmxObjects" /> object from the specified <paramref name="registryInterface" />
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="registryInterface"/> is <see langword="null" />.</exception>
        public virtual ISdmxObjects Build(RegistryInterface registryInterface)
        {
            if (registryInterface == null)
            {
                throw new ArgumentNullException("registryInterface");
            }

            RegistryInterfaceType rit = registryInterface.Content;
            ISdmxObjects beans = new SdmxObjectsImpl(new HeaderImpl(rit.Header));
            if (rit.SubmitStructureRequest != null)
            {
                if (rit.SubmitStructureRequest.Structure != null)
                {
                    return this.Build(rit.SubmitStructureRequest.Structure, beans);
                }
            }

            if (rit.QueryStructureResponse != null)
            {
                return this.Build(rit.QueryStructureResponse, beans);
            }

            return beans;
        }

        /// <summary>
        ///     Builds an <see cref="ISdmxObjects" /> object from the specified <paramref name="structures" />
        /// </summary>
        /// <param name="structures">
        ///     An <see cref="StructureType" /> to build the output object from
        /// </param>
        /// <param name="beans">
        ///     The SDMX objects
        /// </param>
        /// <returns>
        ///     an <see cref="ISdmxObjects" /> object from the specified <paramref name="structures" />
        /// </returns>
        /// <exception cref="MaintainableObjectException">
        ///     Duplicate URN
        /// </exception>
        /// <exception cref="SdmxSemmanticException">Invalid Organisation Scheme.</exception>
        /// <exception cref="ArgumentNullException"><paramref name="structures"/> is <see langword="null" />.</exception>
        public ISdmxObjects Build(StructureType structures, ISdmxObjects beans)
        {
            if (structures == null)
            {
                throw new ArgumentNullException("structures");
            }

            var urns = new HashSet<Uri>();

            // CATEGORY SCHEMES
            this.ProcessCategorySchemes(structures.CategorySchemes, beans);

            // CODELISTS
            this.ProcessCodelists(structures.CodeLists, beans, urns);

            // CONCEPT SCHEMES
            this.ProcessConceptSchemes(structures.Concepts, beans, urns);

            // DATAFLOWS
            this.ProcessDataflows(structures.Dataflows, beans);

            // HIERARCHICAL CODELISTS
            this.ProcessHierarchicalCodelists(structures.HierarchicalCodelists, beans, urns);

            // KEY FAMILIES
            this.ProcessKeyFamilies(structures.KeyFamilies, beans, urns);

            // METADATA FLOWS
            this.ProcessMetadataFlows(structures.Metadataflows, beans);

            // METADATASTRUCTURE DEFINITIONS
            this.ProcessMetadataStructureDefinitions(structures.MetadataStructureDefinitions, beans, urns);

            // ORGANISATION SCHEMES
            this.ProcessOrganisationSchemes(structures.OrganisationSchemes, beans, urns);

            this.ProcessProcesses(structures.Processes, beans, urns);

            this.ProcessReportingTaxonomies(structures.ReportingTaxonomies, beans, urns);

            this.ProcessStructureSets(structures.StructureSets, beans, urns);

            return beans;
        }

        /// <summary>
        ///     Builds an <see cref="ISdmxObjects" /> object from the specified <paramref name="structures" />
        /// </summary>
        /// <param name="structures">
        ///     An <see cref="StructureType" /> to build the output object from
        /// </param>
        /// <param name="beans">
        ///     The SDMX objects
        /// </param>
        /// <returns>
        ///     an <see cref="ISdmxObjects" /> object from the specified <paramref name="structures" />
        /// </returns>
        /// <exception cref="SdmxSemmanticException">
        ///     - If anything goes wrong during the build process
        /// </exception>
        private ISdmxObjects Build(QueryStructureResponseType structures, ISdmxObjects beans)
        {
            var urns = new HashSet<Uri>();

            // CATEGORY SCHEMES
            this.ProcessCategorySchemes(structures.CategorySchemes, beans);

            // CODELISTS
            this.ProcessCodelists(structures.CodeLists, beans, urns);

            // CONCEPT SCHEMES
            this.ProcessConceptSchemes(structures.Concepts, beans, urns);

            // DATAFLOWS
            this.ProcessDataflows(structures.Dataflows, beans);

            // HIERARCHICAL CODELISTS
            this.ProcessHierarchicalCodelists(structures.HierarchicalCodelists, beans, urns);

            // KEY FAMILIES
            this.ProcessKeyFamilies(structures.KeyFamilies, beans, urns);

            // METADATA FLOWS
            this.ProcessMetadataFlows(structures.Metadataflows, beans);

            // METADATASTRUCTURE DEFINITIONS
            this.ProcessMetadataStructureDefinitions(structures.MetadataStructureDefinitions, beans, urns);

            // ORGANISATION SCHEMES
            this.ProcessOrganisationSchemes(structures.OrganisationSchemes, beans, urns);

            this.ProcessProcesses(structures.Processes, beans, urns);

            this.ProcessReportingTaxonomies(structures.ReportingTaxonomies, beans, urns);

            this.ProcessStructureSets(structures.StructureSets, beans, urns);

            return beans;
        }
    }
}