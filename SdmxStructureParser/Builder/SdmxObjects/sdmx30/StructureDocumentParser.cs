// -----------------------------------------------------------------------
// <copyright file="StructureDocumentParser.cs" company="EUROSTAT">
//   Date Created : 2022-02-14
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.SdmxObjects.sdmx30
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Xml;
    using Estat.Sri.SdmxXmlConstants;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Structureparser.Engine.Parsing.SdmxMLV3;
    using Org.Sdmxsource.Sdmx.Util.Date;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.XmlHelper;

    /// <summary>
    /// Class that process structure version 3.0 SDMX document.
    /// </summary>
    public class StructureDocumentParser : IDisposable
    {
        protected static readonly string XmlNs = "http://www.w3.org/XML/1998/namespace";

        [NonSerialized]
        protected XmlReader reader;

        [NonSerialized]
        private Stream _parserInputStream;

        private readonly IReadableDataLocation _dataLocation;

        private IHeader _headerObject;

        private ISdmxObjects _sdmxObjects;

        private readonly StructuresParsingEngine _structures = new StructuresParsingEngine();

        public StructureDocumentParser(IReadableDataLocation dataLocation)
        {
            this._dataLocation = dataLocation;
        }

        public ISdmxObjects Build()
        {
            Reset();
            if (ProcessStructures())
            {
                ISdmxObjects parse = this._structures.Parse(reader);
                this._sdmxObjects.Merge(parse);

            }
            Close();
            return this._sdmxObjects;
        }

        public void Reset()
        {
            try
            {
                _parserInputStream = _dataLocation.InputStream;
                reader = XMLParser.CreateSdmxMlReader(_parserInputStream, SdmxSchemaEnumType.VersionThree);
                this._headerObject = ProcessHeader();
            }
            catch (XmlException e)
            {
                Close();
                throw new SystemException("Failed to reset the Structure Document Parser.", e);
            }
        }

        /// <summary>
        /// Process Structure Header
        /// </summary>
        /// <returns></returns>
        private IHeader ProcessHeader()
        {
            string id = null;
            DateTime? prepared = null;
            var name = new List<ITextTypeWrapper>();
            var source = new List<ITextTypeWrapper>();
            var receiver = new List<IParty>();
            IParty sender = null;
            bool test = false;
            string text = null;

            string xmlLang = "en";

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    string nodeName = reader.LocalName;
                    xmlLang = reader.XmlLang;
                    ElementNameTable elementName;
                    if (Enum.TryParse(nodeName, out elementName))
                    {
                        switch (elementName)
                        {
                            case ElementNameTable.Sender:
                                sender = ProcessParty(elementName);
                                break;
                            case ElementNameTable.Receiver:
                                receiver.Add(ProcessParty(elementName));
                                break;
                        }
                    }
                }
                else if (reader.NodeType == XmlNodeType.Text)
                {
                    text = reader.Value;
                }
                else if (reader.NodeType == XmlNodeType.EndElement)
                {
                    string nodeName = reader.LocalName;
                    ElementNameTable elementName;
                    if (Enum.TryParse(nodeName, out elementName))
                    {
                        switch (elementName)
                        {
                            case ElementNameTable.ID:
                                id = text;
                                break;
                            case ElementNameTable.Test:
                                test = XmlConvert.ToBoolean(text);
                                break;
                            case ElementNameTable.Prepared:
                                prepared = XmlConvert.ToDateTime(text, XmlDateTimeSerializationMode.RoundtripKind);
                                break;
                            case ElementNameTable.Name:
                                AddItemToLang(name, text, xmlLang);
                                break;
                            case ElementNameTable.Source:
                                AddItemToLang(source, text, xmlLang);
                                break;
                            case ElementNameTable.Header:
                                return BuildHeader(id, prepared, name, source, receiver, sender, test);
                        }
                    }
                }
            }

            throw new ArgumentException("DataSet does not contain a Header");
        }

        /// <summary>
        /// Builds the header for structure metadata.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="prepared">The prepared.</param>
        /// <param name="name">The name.</param>
        /// <param name="source">The source.</param>
        /// <param name="receiver">The receiver.</param>
        /// <param name="sender">The sender.</param>
        /// <param name="test">if set to <c>true</c> [test].</param>
        /// <returns>The <see cref="IHeader"/>.</returns>
        private IHeader BuildHeader(
            string id,
            DateTime? prepared,
            IList<ITextTypeWrapper> name,
            IList<ITextTypeWrapper> source,
            IList<IParty> receiver,
            IParty sender,
            bool test)
        {
            return new HeaderImpl(
                null,
                null,
                null,
                null,
                id,
                null,
                null,
                null,
                prepared,
                null,
                null,
                name,
                source,
                receiver,
                sender,
                test);
        }

        private void AddItemToLang(ICollection<ITextTypeWrapper> texts, string value, string lang)
        {
            texts.Add(new TextTypeWrapperImpl(lang, value, null));
        }

        private IContact ProcessContact()
        {
            var name = new List<ITextTypeWrapper>();
            var role = new List<ITextTypeWrapper>();
            var departments = new List<ITextTypeWrapper>();
            var email = new List<string>();
            var fax = new List<string>();
            var telephone = new List<string>();
            var uri = new List<string>();
            var x400 = new List<string>();
            string text = null;
            string xmlLang = null;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    xmlLang = reader.XmlLang;
                }
                else if (reader.NodeType == XmlNodeType.Text)
                {
                    text = reader.Value;
                }
                else if (reader.NodeType == XmlNodeType.EndElement)
                {
                    string nodeName = reader.LocalName;
                    ElementNameTable elementName;
                    if (Enum.TryParse(nodeName, out elementName))
                    {
                        switch (elementName)
                        {
                            case ElementNameTable.Name:
                                AddItemToLang(name, text, xmlLang);
                                break;

                            case ElementNameTable.Role:
                                AddItemToLang(role, text, xmlLang);
                                break;
                            case ElementNameTable.Department:
                                AddItemToLang(departments, text, xmlLang);
                                break;
                            case ElementNameTable.Telephone:
                                telephone.Add(text);
                                break;
                            case ElementNameTable.Fax:
                                fax.Add(text);
                                break;
                            case ElementNameTable.X400:
                                x400.Add(text);
                                break;
                            case ElementNameTable.URI:
                                uri.Add(text);
                                break;
                            case ElementNameTable.Email:
                                email.Add(text);
                                break;
                            case ElementNameTable.Contact:
                                return new ContactCore(name, role, departments, email, fax, telephone, uri, x400);
                        }
                    }
                }
            }

            throw new SdmxSyntaxException("End element </Contact> not found.");
        }

        /// <summary>
        /// Process Party XML element
        /// </summary>
        /// <param name="partyNodeName">Xml Node</param>
        /// <returns>IParty</returns>
        private IParty ProcessParty(ElementNameTable partyNodeName)
        {
            bool isEmptyElement = reader.IsEmptyElement;
            string id = reader.GetAttribute(AttributeNameTable.id);
            string timeZone = null;
            var nameMap = new List<ITextTypeWrapper>();
            var contacts = new List<IContact>();
            string text = null;
            string xmlLang = null;
            if (!isEmptyElement)
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        xmlLang = reader.XmlLang;
                        ElementNameTable elementName;
                        if (Enum.TryParse(reader.LocalName, out elementName))
                        {
                            switch (elementName)
                            {
                                case ElementNameTable.Contact:
                                    contacts.Add(ProcessContact());
                                    break;
                            }
                        }
                    }
                    else if (reader.NodeType == XmlNodeType.Text)
                    {
                        text = reader.Value;
                    }
                    else if (reader.NodeType == XmlNodeType.EndElement)
                    {
                        if (partyNodeName.Is(reader.LocalName))
                        {
                            break;
                        }

                        ElementNameTable elementName;
                        if (Enum.TryParse(reader.LocalName, out elementName))
                        {
                            switch (elementName)
                            {
                                case ElementNameTable.Name:
                                    AddItemToLang(nameMap, text, xmlLang);
                                    break;

                                // NOTE .NET uses the correct "Timezone" name. In Java it uses the incorrect "TimeZone" which does not exist! PLEASE double check with SDMX v2.1 SDMXMessage.xsd before changing.
                                case ElementNameTable.Timezone:
                                    timeZone = text;
                                    break;
                            }
                        }
                    }
                }
            }

            return new PartyCore(nameMap, id, contacts, timeZone);
        }

        private void Close()
        {
            if (reader != null)
            {
                try
                {
                    reader.Close();
                }
                catch (XmlException e)
                {
                    throw new SystemException("Error trying to close reader : " + e);
                }
            }
            if (this._parserInputStream != null)
            {
                try
                {
                    this._parserInputStream.Close();
                }
                catch (Exception e)
                {
                    throw new SystemException("Error trying to close reader InputStream : " + e);
                }
            }
            // TODO this is controlled outside
            //		if (dataLocation != null) {
            //			dataLocation.Close();
            //			dataLocation = null;
            //		}
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool If the Xml Element Structures was found or not</returns>
        private bool ProcessStructures()
        {
            this._sdmxObjects = new SdmxObjectsImpl(this._headerObject);
            while (reader.Read())
            {
                var nodeType = reader.NodeType;
                if (nodeType == XmlNodeType.Element)
                {
                    string nodeName = reader.LocalName;
                    if (nodeName.Equals("Structures"))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void ProcessDataflows()
        {
            string dataFlowsNode = reader.LocalName;
            while (reader.Read())
            {
                // TODO: discard this method?
            }
        }

        public void Dispose()
        {
            Close();
        }
    }
}
