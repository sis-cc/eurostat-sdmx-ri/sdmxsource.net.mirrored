// -----------------------------------------------------------------------
// <copyright file="Soap20CommonStructureQueryBuilder.cs" company="EUROSTAT">
//   Date Created : 2022-05-24
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.Query.Common
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using log4net;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Builder.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Structureparser.Workspace;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
    /// Note this does not implement CommonStructureQueryBuilder because it does not build a CommonStructureQuery.
    /// Instead it builds a Soap20CommonStructureQuery used only by soap 2.0 requests.
    /// </summary>
    public class Soap20CommonStructureQueryBuilder
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(Soap20CommonStructureQueryBuilder));

        /// <summary>
        /// parsing manager for structure queries
        /// </summary>
        private readonly QueryParsingManager _queryParsingManager;

        public Soap20CommonStructureQueryBuilder()
        {

        }

        public ISoap20CommonStructureQuery BuildCommonStructureQuery(SoapStructureQueryParams queryParams)
        {
            ReadableDataLocationFactory readLocFactory = new ReadableDataLocationFactory();
            IReadableDataLocation requestLocation = readLocFactory.GetReadableDataLocation(queryParams.Request());

            IQueryWorkspace queryWorkspace = _queryParsingManager.ParseQueries(requestLocation);
            _log.Info("`-- Structure query parsed successfully!");

            //TODO returnStub should not be hardcoded false but calculated from params
            return BuildCommonStructureQuery(queryWorkspace.SimpleStructureQueries, queryWorkspace.ResolveReferences, false);
        }

        private ISoap20CommonStructureQuery BuildCommonStructureQuery(IList<IStructureReference> simpleStructureQueries, bool resolveReferences, bool returnStub)
        {
            return new Soap20CommonStructureQueryCore(simpleStructureQueries, resolveReferences, returnStub);
        }
    }
}
