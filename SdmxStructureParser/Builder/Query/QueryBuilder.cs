// -----------------------------------------------------------------------
// <copyright file="QueryBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.Query
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V10.message;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Registry;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;

    using QueryRegistrationRequestType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Registry.QueryRegistrationRequestType;

    /// <summary>
    ///     Builds query structure objects from SDMX query messages, this includes structure queries, registration queries and
    ///     provision queries
    /// </summary>
    public class QueryBuilder : IQueryBuilder
    {
        /// <summary>
        ///     The query bean builder v 1.
        /// </summary>
        private readonly QueryBuilderV1 _queryBuilderV1 = new QueryBuilderV1();

        /// <summary>
        ///     The query bean builder v 2.
        /// </summary>
        private readonly QueryBuilderV2 _queryBuilderV2 = new QueryBuilderV2();

        /// <summary>
        ///     The query bean builder v 21.
        /// </summary>
        private readonly QueryBuilderV21 _queryBuilderV21 = new QueryBuilderV21();

        /// <summary>
        ///     Initializes a new instance of the <see cref="QueryBuilder" /> class.
        /// </summary>
        public QueryBuilder()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="QueryBuilder" /> class.
        /// </summary>
        /// <param name="queryBuilderV1">
        ///     The query builder for SDMX v1.0. Use <c>null</c> to use the default.
        /// </param>
        /// <param name="queryBuilderV2">
        ///     The query builder for SDMX v2.0. Use <c>null</c> to use the default.
        /// </param>
        /// <param name="queryBuilderV21">
        ///     The query builder for SDMX v2.1. Use <c>null</c> to use the default.
        /// </param>
        public QueryBuilder(
            QueryBuilderV1 queryBuilderV1, 
            QueryBuilderV2 queryBuilderV2, 
            QueryBuilderV21 queryBuilderV21)
        {
            if (queryBuilderV1 != null)
            {
                this._queryBuilderV1 = queryBuilderV1;
            }

            if (queryBuilderV2 != null)
            {
                this._queryBuilderV2 = queryBuilderV2;
            }

            if (queryBuilderV21 != null)
            {
                this._queryBuilderV21 = queryBuilderV21;
            }
        }

        /// <summary>
        ///     Builds a list of structure references from a version 2.0 registry query structure request message
        /// </summary>
        /// <param name="queryStructureRequests">
        ///     The Query Structure Request (SDMX Registry Interface v2.0)
        /// </param>
        /// <returns>
        ///     list of structure references
        /// </returns>
        public virtual IList<IStructureReference> Build(QueryStructureRequestType queryStructureRequests)
        {
            return this._queryBuilderV2.Build(queryStructureRequests);
        }

        /// <summary>
        ///     Builds a list of provision references from a version 2.0 registry query registration request message
        ///     If only dataProviderRef is supplied then this is used and the flow type is assumed to be a dataflow
        /// </summary>
        /// <param name="queryRegistrationRequestType">
        ///     The query Registration Request Type.
        /// </param>
        /// <returns>
        ///     provision references
        /// </returns>
        public virtual IStructureReference Build(QueryRegistrationRequestType queryRegistrationRequestType)
        {
            return this._queryBuilderV2.Build(queryRegistrationRequestType);
        }

        /// <summary>
        ///     The build.
        /// </summary>
        /// <param name="queryRegistrationRequestType">
        ///     The query registration request type.
        /// </param>
        /// <returns>
        ///     The <see cref="IStructureReference" />.
        /// </returns>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public virtual IStructureReference Build(
            Org.Sdmx.Resources.SdmxMl.Schemas.V21.Registry.QueryRegistrationRequestType queryRegistrationRequestType)
        {
            return this._queryBuilderV21.Build(queryRegistrationRequestType);
        }

        /// <summary>
        ///     Builds a list of provision references from a version 2.0 registry query provision request message
        /// </summary>
        /// <param name="queryProvisionRequestType">
        ///     The query Provision Request Type.
        /// </param>
        /// <returns>
        ///     provision references
        /// </returns>
        /// <exception cref="ArgumentNullException">queryProvisionRequestType is <see langword="null" />.</exception>
        /// <exception cref="SdmxNotImplementedException">.At version 2.0 provisions can only be queries by Provision URN, Dataflow Ref, Data Provider Ref or Metadata Flow Ref</exception>
        public virtual IStructureReference Build(QueryProvisioningRequestType queryProvisionRequestType)
        {
            return this._queryBuilderV2.Build(queryProvisionRequestType);
        }

        /// <summary>
        ///     Builds a list of structure references from a version 1.0 query message
        /// </summary>
        /// <param name="queryMessage">
        ///     The query message SDMX v1.0
        /// </param>
        /// <returns>
        ///     list of structure references
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">
        ///     Not supported.
        /// </exception>
        public virtual IList<IStructureReference> Build(QueryMessageType queryMessage)
        {
            return this._queryBuilderV1.Build(queryMessage);
        }

        /// <summary>
        ///     Builds a list of structure references from a version 2.0 query message
        /// </summary>
        /// <param name="queryMessage">
        ///     The query message SDMX v2.0
        /// </param>
        /// <returns>
        ///     list of structure references
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">Unsupported structure.</exception>
        public virtual IList<IStructureReference> Build(
            Org.Sdmx.Resources.SdmxMl.Schemas.V20.Message.QueryMessageType queryMessage)
        {
            return this._queryBuilderV2.Build(queryMessage);
        }

        /// <summary>
        /// Builds the specified codelist query message.
        /// </summary>
        /// <param name="codelistQueryMessage">The codelist query message.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        public virtual IComplexStructureQuery Build(CodelistQueryType codelistQueryMessage)
        {
            return this._queryBuilderV21.Build(codelistQueryMessage);
        }

        /// <summary>
        /// Builds the specified dataflow query message.
        /// </summary>
        /// <param name="dataflowQueryMessage">The dataflow query message.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        public virtual IComplexStructureQuery Build(DataflowQueryType dataflowQueryMessage)
        {
            return this._queryBuilderV21.Build(dataflowQueryMessage);
        }

        /// <summary>
        /// Builds the specified metadataflow query message.
        /// </summary>
        /// <param name="metadataflowQueryMessage">The metadataflow query message.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        public virtual IComplexStructureQuery Build(MetadataflowQueryType metadataflowQueryMessage)
        {
            return this._queryBuilderV21.Build(metadataflowQueryMessage);
        }

        /// <summary>
        /// Builds the specified data structure query message.
        /// </summary>
        /// <param name="dataStructureQueryMessage">The data structure query message.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        public virtual IComplexStructureQuery Build(DataStructureQueryType dataStructureQueryMessage)
        {
            return this._queryBuilderV21.Build(dataStructureQueryMessage);
        }

        /// <summary>
        /// Builds the specified metadata structure query message.
        /// </summary>
        /// <param name="metadataStructureQueryMessage">The metadata structure query message.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        public virtual IComplexStructureQuery Build(MetadataStructureQueryType metadataStructureQueryMessage)
        {
            return this._queryBuilderV21.Build(metadataStructureQueryMessage);
        }

        /// <summary>
        /// Builds the specified category scheme query message.
        /// </summary>
        /// <param name="categorySchemeQueryMessage">The category scheme query message.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        public virtual IComplexStructureQuery Build(CategorySchemeQueryType categorySchemeQueryMessage)
        {
            return this._queryBuilderV21.Build(categorySchemeQueryMessage);
        }

        /// <summary>
        /// Builds the specified concept scheme query message.
        /// </summary>
        /// <param name="conceptSchemeQueryMessage">The concept scheme query message.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        public virtual IComplexStructureQuery Build(ConceptSchemeQueryType conceptSchemeQueryMessage)
        {
            return this._queryBuilderV21.Build(conceptSchemeQueryMessage);
        }

        /// <summary>
        /// Builds the specified hierarchical codelist query message.
        /// </summary>
        /// <param name="hierarchicalCodelistQueryMessage">The hierarchical codelist query message.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        public virtual IComplexStructureQuery Build(HierarchicalCodelistQueryType hierarchicalCodelistQueryMessage)
        {
            return this._queryBuilderV21.Build(hierarchicalCodelistQueryMessage);
        }

        /// <summary>
        /// Builds the specified organisation scheme query message.
        /// </summary>
        /// <param name="organisationSchemeQueryMessage">The organisation scheme query message.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        public virtual IComplexStructureQuery Build(OrganisationSchemeQueryType organisationSchemeQueryMessage)
        {
            return this._queryBuilderV21.Build(organisationSchemeQueryMessage);
        }

        /// <summary>
        /// Builds the specified reporting taxonomy query message.
        /// </summary>
        /// <param name="reportingTaxonomyQueryMessage">The reporting taxonomy query message.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        public virtual IComplexStructureQuery Build(ReportingTaxonomyQueryType reportingTaxonomyQueryMessage)
        {
            return this._queryBuilderV21.Build(reportingTaxonomyQueryMessage);
        }

        /// <summary>
        /// Builds the specified structure set query message.
        /// </summary>
        /// <param name="structureSetQueryMessage">The structure set query message.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        public virtual IComplexStructureQuery Build(StructureSetQueryType structureSetQueryMessage)
        {
            return this._queryBuilderV21.Build(structureSetQueryMessage);
        }

        /// <summary>
        /// Builds the specified process query message.
        /// </summary>
        /// <param name="processQueryMessage">The process query message.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        public virtual IComplexStructureQuery Build(ProcessQueryType processQueryMessage)
        {
            return this._queryBuilderV21.Build(processQueryMessage);
        }

        /// <summary>
        /// Builds the specified categorisation query message.
        /// </summary>
        /// <param name="categorisationQueryMessage">The categorisation query message.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        public virtual IComplexStructureQuery Build(CategorisationQueryType categorisationQueryMessage)
        {
            return this._queryBuilderV21.Build(categorisationQueryMessage);
        }

        /// <summary>
        /// Builds the specified provision agreement query message.
        /// </summary>
        /// <param name="provisionAgreementQueryMessage">The provision agreement query message.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        public virtual IComplexStructureQuery Build(ProvisionAgreementQueryType provisionAgreementQueryMessage)
        {
            return this._queryBuilderV21.Build(provisionAgreementQueryMessage);
        }

        /// <summary>
        /// Builds the specified constraint query message.
        /// </summary>
        /// <param name="constraintQueryMessage">The constraint query message.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        public virtual IComplexStructureQuery Build(ConstraintQueryType constraintQueryMessage)
        {
            return this._queryBuilderV21.Build(constraintQueryMessage);
        }

        /// <summary>
        /// Builds the specified structures query message.
        /// </summary>
        /// <param name="structuresQueryMessage">The structures query message.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        public virtual IComplexStructureQuery Build(StructuresQueryType structuresQueryMessage)
        {
            return this._queryBuilderV21.Build(structuresQueryMessage);
        }
    }
}