﻿// -----------------------------------------------------------------------
// <copyright file="GenericXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-06-07
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Structureparser.Factory;
    using Org.Sdmxsource.Sdmx.Util.Objects;

    using Xml.Schema.Linq;

    /// <summary>
    /// Extensions for building XML types
    /// </summary>
    public static class GenericXmlBuilder
    {
        /// <summary>
        /// Builds the specified SDMX objects.
        /// </summary>
        /// <typeparam name="TXmlTypeContainer">The type of the XML type container.</typeparam>
        /// <typeparam name="TXmlType">The type of the XML type.</typeparam>
        /// <typeparam name="TSdmxObject">The type of the SDMX object.</typeparam>
        /// <param name="sdmxObjects">The SDMX objects.</param>
        /// <param name="builderFactory">The builder factory.</param>
        /// <param name="categorisationObjects">The categorisation objects.</param>
        /// <param name="addMethod">The add method.</param>
        /// <returns>
        /// An instance of the type <typeparamref name="TXmlTypeContainer" />
        /// </returns>
        public static TXmlTypeContainer BuildCategorised<TXmlTypeContainer, TXmlType, TSdmxObject>(this ISet<TSdmxObject> sdmxObjects, IBuilderFactory builderFactory, ISet<ICategorisationObject> categorisationObjects, Action<TXmlTypeContainer, TXmlType> addMethod)
         where TXmlTypeContainer : XTypedElement, new()
         where TSdmxObject : IMaintainableObject
        {
            if (builderFactory == null)
            {
                throw new ArgumentNullException("builderFactory");
            }

            if (addMethod == null)
            {
                throw new ArgumentNullException("addMethod");
            }

            if (sdmxObjects != null && sdmxObjects.Count > 0)
            {
                var xmlTypeContainer = new TXmlTypeContainer();
                foreach (var sdmxObject in sdmxObjects)
                {
                    var xmlType = builderFactory.GetCategorisedBuilder<TXmlType, TSdmxObject>().Build(sdmxObject, GetCategorisations(sdmxObject, categorisationObjects));
                    addMethod(xmlTypeContainer, xmlType);
                }

                return xmlTypeContainer;
            }

            return null;
        }

        /// <summary>
        /// Builds the XML Type container the specified SDMX objects.
        /// </summary>
        /// <typeparam name="TXmlTypeContainer">The type of the XML type container.</typeparam>
        /// <typeparam name="TXmlType">The type of the XML type.</typeparam>
        /// <typeparam name="TSdmxObject">The type of the SDMX object.</typeparam>
        /// <param name="sdmxObjects">The SDMX objects.</param>
        /// <param name="builderFactory">The builder factory.</param>
        /// <param name="addMethod">The add method.</param>
        /// <returns>
        /// An instance of the type <typeparamref name="TXmlTypeContainer" />
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// builderFactory
        /// or
        /// addMethod
        /// </exception>
        public static TXmlTypeContainer Build<TXmlTypeContainer, TXmlType, TSdmxObject>(this ISet<TSdmxObject> sdmxObjects, IBuilderFactory builderFactory, Action<TXmlTypeContainer, TXmlType> addMethod) where TXmlTypeContainer : XTypedElement, new()
            where TSdmxObject : IMaintainableObject
        {
            return Build(sdmxObjects, null, builderFactory, addMethod);
        }

        /// <summary>
        /// Builds the XML Type container the specified SDMX objects.
        /// </summary>
        /// <typeparam name="TXmlTypeContainer">The type of the XML type container.</typeparam>
        /// <typeparam name="TXmlType">The type of the XML type.</typeparam>
        /// <typeparam name="TSdmxObject">The type of the SDMX object.</typeparam>
        /// <param name="sdmxObjects">The SDMX objects.</param>
        /// <param name="xmlTypeContainer">The XML type container.</param>
        /// <param name="builderFactory">The builder factory.</param>
        /// <param name="addMethod">The add method.</param>
        /// <returns>
        /// An instance of the type <typeparamref name="TXmlTypeContainer" />
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// builderFactory
        /// or
        /// addMethod
        /// </exception>
        public static TXmlTypeContainer Build<TXmlTypeContainer, TXmlType, TSdmxObject>(this ISet<TSdmxObject> sdmxObjects, TXmlTypeContainer xmlTypeContainer, IBuilderFactory builderFactory, Action<TXmlTypeContainer, TXmlType> addMethod) where TXmlTypeContainer : XTypedElement, new()
         where TSdmxObject : IMaintainableObject
        {
            if (builderFactory == null)
            {
                throw new ArgumentNullException("builderFactory");
            }

            if (addMethod == null)
            {
                throw new ArgumentNullException("addMethod");
            }

            if (sdmxObjects != null && sdmxObjects.Count > 0)
            {
                xmlTypeContainer = xmlTypeContainer ?? new TXmlTypeContainer();
                foreach (var sdmxObject in sdmxObjects)
                {
                    var xmlType = sdmxObject.Build<TXmlType, TSdmxObject>(builderFactory);
                    addMethod(xmlTypeContainer, xmlType);
                }

                return xmlTypeContainer;
            }

            return null;
        }

        /// <summary>
        /// Builds XML Type from the specified factory.
        /// </summary>
        /// <typeparam name="TXmlType">The type of the XML type.</typeparam>
        /// <typeparam name="TSdmxObject">The type of the SDMX object.</typeparam>
        /// <param name="sdmxObject">The SDMX object.</param>
        /// <param name="builderFactory">The builder factory.</param>
        /// <returns>An instance of the type <typeparamref name="TXmlType"/></returns>
        public static TXmlType Build<TXmlType, TSdmxObject>(this TSdmxObject sdmxObject, IBuilderFactory builderFactory)
        {
            if (builderFactory == null)
            {
                throw new ArgumentNullException("builderFactory");
            }

            return builderFactory.GetBuilder<TXmlType, TSdmxObject>().Build(sdmxObject);
        }

        /// <summary>
        /// Gets the categorisations.
        /// </summary>
        /// <param name="maintainable">
        /// The maintainable.
        /// </param>
        /// <param name="categorisations">
        /// The categorisations.
        /// </param>
        /// <returns>
        /// the set of categorisations
        /// </returns>
        private static ISet<ICategorisationObject> GetCategorisations(IMaintainableObject maintainable, ISet<ICategorisationObject> categorisations)
        {
            ISet<ICategorisationObject> returnSet = new HashSet<ICategorisationObject>();
            if (maintainable.IsExternalReference.IsTrue)
            {
                return returnSet;
            }

            if (maintainable.StructureType == SdmxStructureEnumType.CategoryScheme)
            {
                return categorisations;
            }

            foreach (ICategorisationObject cat in categorisations)
            {
                if (cat.IsExternalReference.IsTrue)
                {
                    continue;
                }

                if (cat.StructureReference.TargetReference.EnumType == maintainable.StructureType.EnumType)
                {
                    if (MaintainableUtil<IMaintainableObject>.Match(maintainable, cat.StructureReference))
                    {
                        returnSet.Add(cat);
                    }
                }
            }

            return returnSet;
        }
    }
}