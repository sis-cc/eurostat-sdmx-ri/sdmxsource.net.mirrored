// -----------------------------------------------------------------------
// <copyright file="ProcessStepSuperBeanBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-02-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.SuperObjects
{
    using System;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Process;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Process;

    /// <summary>
    ///     ProcessStepSuperBeanBuilder class
    /// </summary>
    public class ProcessStepSuperBeanBuilder
    {
        /// <summary>
        /// The _input output super object builder
        /// </summary>
        private readonly InputOutputSuperObjectBuilder _inputOutputSuperObjectBuilder;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ProcessStepSuperBeanBuilder" /> class.
        /// </summary>
        /// <param name="inputOutputSuperObjectBuilder">The input output super object builder.</param>
        public ProcessStepSuperBeanBuilder(InputOutputSuperObjectBuilder inputOutputSuperObjectBuilder)
        {
            this._inputOutputSuperObjectBuilder = inputOutputSuperObjectBuilder;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ProcessStepSuperBeanBuilder" /> class.
        /// </summary>
        public ProcessStepSuperBeanBuilder()
            : this(new InputOutputSuperObjectBuilder())
        {
        }

        /// <summary>
        ///     Builds the specified build from.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <returns>The super object</returns>
        /// <exception cref="ArgumentNullException"><paramref name="buildFrom"/> is <see langword="null" />.</exception>
        public IProcessStepSuperObject Build(
            IProcessStepObject buildFrom, 
            IIdentifiableRetrievalManager retrievalManager)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            var input =
                buildFrom.Input.Select(
                    inputOutputObject => this._inputOutputSuperObjectBuilder.Build(inputOutputObject, retrievalManager))
                    .ToList();

            var output =
                buildFrom.Output.Select(
                    inputOutputObject => this._inputOutputSuperObjectBuilder.Build(inputOutputObject, retrievalManager))
                    .ToList();

            var transitions = buildFrom.Transitions.ToList();

            var processSteps =
                buildFrom.ProcessSteps.Select(processStepObject => this.Build(processStepObject, retrievalManager)).ToList();

            return new ProcessStepSuperObject(
                buildFrom, 
                input, 
                output, 
                buildFrom.Computation, 
                transitions, 
                processSteps);
        }
    }
}