// -----------------------------------------------------------------------
// <copyright file="RegistrationSuperObjectBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-02-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.SuperObjects
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Registry;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    ///     Registration SuperObject Builder
    /// </summary>
    public class RegistrationSuperObjectBuilder : StructureBuilder<IRegistrationSuperObject, IRegistrationObject>
    {
        /// <summary>
        /// The _provision super object builder
        /// </summary>
        private readonly ProvisionSuperObjectBuilder _provisionSuperObjectBuilder;

        /// <summary>
        ///     Initializes a new instance of the <see cref="RegistrationSuperObjectBuilder" /> class.
        /// </summary>
        /// <param name="provisionSuperObjectBuilder">The provision super object builder.</param>
        public RegistrationSuperObjectBuilder(ProvisionSuperObjectBuilder provisionSuperObjectBuilder)
        {
            this._provisionSuperObjectBuilder = provisionSuperObjectBuilder;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RegistrationSuperObjectBuilder" /> class.
        /// </summary>
        public RegistrationSuperObjectBuilder()
            : this(new ProvisionSuperObjectBuilder())
        {
        }

        /// <summary>
        /// Builds the specified build from.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <param name="existingBeans">The existing beans.</param>
        /// <returns>The <see cref="IRegistrationSuperObject" /></returns>
        /// <exception cref="ArgumentNullException"><paramref name="buildFrom"/> is <see langword="null" />.</exception>
        public override IRegistrationSuperObject Build(
            IRegistrationObject buildFrom, 
            ISdmxObjectRetrievalManager retrievalManager, 
            ISuperObjects existingBeans)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            if (existingBeans == null)
            {
                existingBeans = new SuperObjects();
            }

            var superBeanRetrievalManager = new InMemorySdmxSuperObjectRetrievalManager(existingBeans);

            var provRef = buildFrom.ProvisionAgreementRef;

            var provisionSuperBean = superBeanRetrievalManager.GetProvisionAgreementSuperObject(provRef);

            if (provisionSuperBean == null)
            {
                IProvisionAgreementObject provision = null;
                if (retrievalManager != null)
                {
                    provision = retrievalManager.GetIdentifiableObject<IProvisionAgreementObject>(provRef);
                }

                if (provision == null)
                {
                    throw new CrossReferenceException(buildFrom.ProvisionAgreementRef);
                }

                    provisionSuperBean = this._provisionSuperObjectBuilder.Build(provision, retrievalManager, existingBeans);
                existingBeans.AddProvision(provisionSuperBean);
            }

            return new RegistrationSuperObject(buildFrom, provisionSuperBean);
        }
    }
}