// -----------------------------------------------------------------------
// <copyright file="PrimaryMeasureSuperObjectBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.SuperObjects
{
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    ///     Primary Measure Super Object Builder
    /// </summary>
    public class PrimaryMeasureSuperObjectBuilder :
        ComponentSuperObjectBuilder<IPrimaryMeasureSuperObject, IPrimaryMeasure>
    {
        /// <summary>
        /// Builds the specified build from.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <param name="existingBeans">The existing beans.</param>
        /// <returns>The <see cref="IPrimaryMeasureSuperObject"/></returns>
        /// <exception cref="CrossReferenceException">Could not find concept bean to build concept super bean from</exception>
        public override IPrimaryMeasureSuperObject Build(
            IPrimaryMeasure buildFrom, 
            ISdmxObjectRetrievalManager retrievalManager, 
            ISuperObjects existingBeans)
        {
            if (existingBeans == null)
            {
                existingBeans = new SuperObjects();
            }

            var concept = this.GetConcept(buildFrom, retrievalManager, existingBeans);
            var codelist = this.GetCodelist(buildFrom, retrievalManager, existingBeans);

            // TODO Hierarchical Codelist
            return new PrimaryMeasureSuperObject(buildFrom, codelist, concept);
        }
    }
}