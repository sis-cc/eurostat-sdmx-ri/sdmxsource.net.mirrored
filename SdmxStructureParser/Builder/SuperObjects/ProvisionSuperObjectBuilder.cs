// -----------------------------------------------------------------------
// <copyright file="ProvisionSuperObjectBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-02-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.SuperObjects
{
    using System;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Registry;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    ///     ProvisionSuperObjectBuilder class
    /// </summary>
    public class ProvisionSuperObjectBuilder :
        StructureBuilder<IProvisionAgreementSuperObject, IProvisionAgreementObject>
    {
        /// <summary>
        /// The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(ProvisionSuperObjectBuilder));

        /// <summary>
        /// The _dataflow super object builder
        /// </summary>
        private readonly DataflowSuperObjectBuilder _dataflowSuperObjectBuilder;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ProvisionSuperObjectBuilder" /> class.
        /// </summary>
        /// <param name="dataflowSuperObjectBuilder">The dataflow super object builder.</param>
        public ProvisionSuperObjectBuilder(DataflowSuperObjectBuilder dataflowSuperObjectBuilder)
        {
            this._dataflowSuperObjectBuilder = dataflowSuperObjectBuilder;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ProvisionSuperObjectBuilder" /> class.
        /// </summary>
        public ProvisionSuperObjectBuilder()
            : this(new DataflowSuperObjectBuilder())
        {
        }

        /// <summary>
        ///     Builds the specified build from.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <param name="existingBeans">The existing beans.</param>
        /// <returns>The super object</returns>
        /// <exception cref="CrossReferenceException">The exception </exception>
        /// <exception cref="ArgumentNullException"><paramref name="buildFrom"/> is <see langword="null" />.</exception>
        public override IProvisionAgreementSuperObject Build(
            IProvisionAgreementObject buildFrom, 
            ISdmxObjectRetrievalManager retrievalManager, 
            ISuperObjects existingBeans)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            if (retrievalManager == null)
            {
                throw new ArgumentNullException("retrievalManager");
            }

            if (existingBeans == null)
            {
                existingBeans = new SuperObjects();
            }

            var superBeanRetrievalManager = new InMemorySdmxSuperObjectRetrievalManager(existingBeans);

            var dataflowRef = buildFrom.StructureUseage;

            _log.Debug("Build provision, resolve dataflow ref: " + dataflowRef);

            var dataflowSuperBean = superBeanRetrievalManager.GetDataflowSuperObject(dataflowRef);

            if (dataflowSuperBean == null)
            {
                _log.Debug("Dataflow ref super bean not found, build new: " + dataflowRef);

                var dataflow = retrievalManager.GetIdentifiableObject<IDataflowObject>(dataflowRef);

                if (dataflow == null)
                {
                    throw new CrossReferenceException(buildFrom.StructureUseage);
                }

                dataflowSuperBean = this._dataflowSuperObjectBuilder.Build(dataflow, retrievalManager, existingBeans);
                existingBeans.AddDataflow(dataflowSuperBean);
            }

            var dataProvider = retrievalManager.GetIdentifiableObject<IDataProvider>(buildFrom.DataproviderRef);
            if (dataProvider == null)
            {
                throw new CrossReferenceException(buildFrom.DataproviderRef);
            }

            return new ProvisionAgreementSuperObject(buildFrom, dataflowSuperBean, dataProvider);
        }
    }
}