// -----------------------------------------------------------------------
// <copyright file="HierarchicalCodelistSuperObjectBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.SuperObjects
{
    using System.Collections.Generic;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Engine;

    /// <summary>
    ///     Hierarchical Codelist Super Object Builder
    /// </summary>
    public class HierarchicalCodelistSuperObjectBuilder
    {
        /// <summary>
        /// Builds the specified build from.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <returns>The <see cref="IHierarchicalCodelistSuperObject"/></returns>
        /// <exception cref="CrossReferenceException">- if any of the references could not be resolved</exception>
        public IHierarchicalCodelistSuperObject Build(
            IHierarchicalCodelistObject buildFrom, 
            IIdentifiableRetrievalManager retrievalManager)
        {
            var resolverEngine = new CrossReferenceResolverEngineCore();
            var identifiables = resolverEngine.ResolveReferences(buildFrom, false, 1, retrievalManager);

            var referencedCodelists = new List<ICodelistObject>();
            foreach (var identifiableObject in identifiables)
            {
                var maint = identifiableObject.MaintainableParent;

                if (!referencedCodelists.Contains(maint))
                {
                    var codelistObject = maint as ICodelistObject;
                    if (codelistObject != null)
                    {
                        referencedCodelists.Add(codelistObject);
                    }
                }
            }

            return new HierarchicalCodelistSuperObject(buildFrom, referencedCodelists);
        }
    }
}