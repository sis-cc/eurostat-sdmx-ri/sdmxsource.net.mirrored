// -----------------------------------------------------------------------
// <copyright file="DataflowXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V2
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The dataflow xml bean builder.
    /// </summary>
    public class DataflowXmlBuilder : AbstractBuilder, ICategorisedBuilder<DataflowType, IDataflowObject>
    {
        /// <summary>
        ///     Build <see cref="DataflowType" /> from <paramref name="buildFrom" />.
        /// </summary>
        /// <param name="buildFrom">
        ///     The build from.
        /// </param>
        /// <param name="categorisations">
        ///     The categorisations
        /// </param>
        /// <returns>
        ///     The <see cref="DataflowType" /> from <paramref name="buildFrom" /> .
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="buildFrom"/> is <see langword="null" />.</exception>
        public DataflowType Build(IDataflowObject buildFrom, ISet<ICategorisationObject> categorisations)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            var builtObj = new DataflowType();
            this.HandleMaintainableInformation(buildFrom, builtObj);

            BuildCategorisations(categorisations, builtObj);

            BuildDataStructureReference(buildFrom, builtObj);

            return builtObj;
        }

        /// <summary>
        /// Builds the data structure reference.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="builtObj">The built object.</param>
        private static void BuildDataStructureReference(IDataflowObject buildFrom, DataflowType builtObj)
        {
            if (buildFrom.DataStructureRef != null)
            {
                KeyFamilyRefType keyFamilyRefType = builtObj.KeyFamilyRef = new KeyFamilyRefType();
                ICrossReference dataStructureRef = buildFrom.DataStructureRef;
                IMaintainableRefObject refBean0 = dataStructureRef.MaintainableReference;
                string value3 = refBean0.AgencyId;
                if (!string.IsNullOrWhiteSpace(value3))
                {
                    keyFamilyRefType.KeyFamilyAgencyID = refBean0.AgencyId;
                }

                string value4 = refBean0.MaintainableId;
                if (!string.IsNullOrWhiteSpace(value4))
                {
                    keyFamilyRefType.KeyFamilyID = refBean0.MaintainableId;
                }

                string value5 = refBean0.Version;
                if (!string.IsNullOrWhiteSpace(value5))
                {
                    keyFamilyRefType.Version = refBean0.Version;
                }

                if (ObjectUtil.ValidString(dataStructureRef.TargetUrn))
                {
                    keyFamilyRefType.URN = dataStructureRef.TargetUrn;
                }
            }
        }

        /// <summary>
        /// Builds the categorisations.
        /// </summary>
        /// <param name="categorisations">The categorisations.</param>
        /// <param name="builtObj">The built object.</param>
        private static void BuildCategorisations(ISet<ICategorisationObject> categorisations, DataflowType builtObj)
        {
            if (ObjectUtil.ValidCollection(categorisations))
            {
                foreach (ICategorisationObject currentCategoryRef in categorisations)
                {
                    var categoryRefType = new CategoryRefType();
                    builtObj.CategoryRef.Add(categoryRefType);

                    ICrossReference refBean = currentCategoryRef.CategoryReference;
                    if (refBean != null)
                    {
                        IMaintainableRefObject maintainableReference = refBean.MaintainableReference;
                        string value3 = maintainableReference.AgencyId;
                        if (!string.IsNullOrWhiteSpace(value3))
                        {
                            categoryRefType.CategorySchemeAgencyID = maintainableReference.AgencyId;
                        }

                        string value4 = maintainableReference.MaintainableId;
                        if (!string.IsNullOrWhiteSpace(value4))
                        {
                            categoryRefType.CategorySchemeID = maintainableReference.MaintainableId;
                        }

                        string value5 = maintainableReference.Version;
                        if (!string.IsNullOrWhiteSpace(value5))
                        {
                            categoryRefType.CategorySchemeVersion = maintainableReference.Version;
                        }

                        CategoryIDType idType = null;
                        IIdentifiableRefObject childRef = refBean.ChildReference;
                        int i = 0;
                        while (childRef != null)
                        {
                            if (i == 0 || idType == null)
                            {
                                idType = categoryRefType.CategoryID = new CategoryIDType();
                            }
                            else
                            {
                                idType = idType.CategoryID = new CategoryIDType();
                            }

                            idType.ID = childRef.Id;
                            childRef = childRef.ChildReference;
                            i++;
                        }

                        if (ObjectUtil.ValidString(refBean.TargetUrn))
                        {
                            categoryRefType.URN = refBean.TargetUrn;
                        }
                    }
                }
            }
        }
    }
}