// -----------------------------------------------------------------------
// <copyright file="DataStructureXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V2
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Structureparser.Factory;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The data structure XML bean builder.
    /// </summary>
    public class DataStructureXmlBuilder : AbstractBuilder, IBuilder<KeyFamilyType, IDataStructureObject>
    {
        /// <summary>
        ///     The attribute xml bean builder.
        /// </summary>
        private readonly IBuilder<AttributeType, IAttributeObject> _attributeXmlBuilder;

        /// <summary>
        ///     The cross sectional xml bean builder.
        /// </summary>
        private readonly IBuilder<CrossSectionalMeasureType, ICrossSectionalMeasure> _crossSectionalXmlBuilder;

        /// <summary>
        ///     The dimension xml bean builder.
        /// </summary>
        private readonly IBuilder<DimensionType, IDimension> _dimensionXmlBuilder;

        /// <summary>
        ///     The group xml beans builder.
        /// </summary>
        private readonly IBuilder<GroupType, IGroup> _groupXmlsBuilder;

        /// <summary>
        ///     The primary measure xml bean builder.
        /// </summary>
        private readonly IBuilder<PrimaryMeasureType, IPrimaryMeasure> _primaryMeasureXmlBuilder;

        /// <summary>
        ///     The time dimension XML bean builder.
        /// </summary>
        private readonly IBuilder<TimeDimensionType, IDimension> _timeDimensionXmlBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataStructureXmlBuilder"/> class.
        /// </summary>
        /// <param name="builderFactory">The builder factory.</param>
        public DataStructureXmlBuilder(IBuilderFactory builderFactory)
        {
            if (builderFactory == null)
            {
                throw new ArgumentNullException("builderFactory");
            }

            this._attributeXmlBuilder = builderFactory.GetBuilder<AttributeType, IAttributeObject>();
            this._crossSectionalXmlBuilder = builderFactory.GetBuilder<CrossSectionalMeasureType, ICrossSectionalMeasure>();
            this._dimensionXmlBuilder = builderFactory.GetBuilder<DimensionType, IDimension>();
            this._groupXmlsBuilder = builderFactory.GetBuilder<GroupType, IGroup>();
            this._primaryMeasureXmlBuilder = builderFactory.GetBuilder<PrimaryMeasureType, IPrimaryMeasure>();
            this._timeDimensionXmlBuilder = builderFactory.GetBuilder<TimeDimensionType, IDimension>();
        }

        /// <summary>
        ///     Build <see cref="KeyFamilyType" /> from <paramref name="buildFrom" />.
        /// </summary>
        /// <param name="buildFrom">
        ///     The build from.
        /// </param>
        /// <returns>
        ///     The <see cref="KeyFamilyType" /> from <paramref name="buildFrom" /> .
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="buildFrom"/> is <see langword="null" />.</exception>
        public virtual KeyFamilyType Build(IDataStructureObject buildFrom)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            var builtObj = new KeyFamilyType();
            this.HandleMaintainableInformation(buildFrom, builtObj);

            ComponentsType componentsType = new ComponentsType();
            if (buildFrom.PrimaryMeasure != null)
            {
                builtObj.Components = componentsType;
            }

            List<IComponent> crossSectionalAttachDataSet = new List<IComponent>();
            List<IComponent> crossSectionalAttachGroup = new List<IComponent>();
            List<IComponent> crossSectionalAttachSection = new List<IComponent>();
            List<IComponent> crossSectionalAttachObservation = new List<IComponent>();

            var crossSectionalDataStructureObject = this.PopulateCrossSectionalInfo(buildFrom, crossSectionalAttachDataSet, crossSectionalAttachGroup, crossSectionalAttachSection, crossSectionalAttachObservation, componentsType);

            IList<IDimension> currentDims = buildFrom.GetDimensions(
                SdmxStructureEnumType.Dimension, 
                SdmxStructureEnumType.MeasureDimension);
            this.HandleDimensions(currentDims, crossSectionalDataStructureObject, crossSectionalAttachDataSet, crossSectionalAttachGroup, crossSectionalAttachSection, crossSectionalAttachObservation, componentsType);

            this.HandleTimeDimension(buildFrom, crossSectionalAttachDataSet, crossSectionalAttachGroup, crossSectionalAttachSection, crossSectionalAttachObservation, componentsType);

            IList<IGroup> currentGroups = buildFrom.Groups;
            this.HandleGroups(currentGroups, componentsType);

            if (buildFrom.PrimaryMeasure != null)
            {
                componentsType.PrimaryMeasure = this._primaryMeasureXmlBuilder.Build(buildFrom.PrimaryMeasure);
            }

            this.HandleAttributes(buildFrom, crossSectionalAttachDataSet, crossSectionalAttachGroup, crossSectionalAttachSection, crossSectionalAttachObservation, crossSectionalDataStructureObject, componentsType);

            return builtObj;
        }

        /// <summary>
        /// Handles the attributes.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="crossSectionalAttachDataSet">The cross sectional attach data set.</param>
        /// <param name="crossSectionalAttachGroup">The cross sectional attach group.</param>
        /// <param name="crossSectionalAttachSection">The cross sectional attach section.</param>
        /// <param name="crossSectionalAttachObservation">The cross sectional attach observation.</param>
        /// <param name="crossSectionalDataStructureObject">The cross sectional data structure object.</param>
        /// <param name="componentsType">Type of the components.</param>
        private void HandleAttributes(
            IDataStructureObject buildFrom, 
            ICollection<IComponent> crossSectionalAttachDataSet, 
            ICollection<IComponent> crossSectionalAttachGroup, 
            ICollection<IComponent> crossSectionalAttachSection, 
            ICollection<IComponent> crossSectionalAttachObservation, 
            ICrossSectionalDataStructureObject crossSectionalDataStructureObject, 
            ComponentsType componentsType)
        {
            IList<IAttributeObject> currentAttrs = buildFrom.Attributes;
            if (ObjectUtil.ValidCollection(currentAttrs))
            {
                /* foreach */
                foreach (IAttributeObject currentAttr in currentAttrs)
                {
                    AttributeType newAttribute = this._attributeXmlBuilder.Build(currentAttr);

                    if (currentAttr.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
                    {
                        // If the group of dimensions is also a group, do not create the attribute;
                        IList<string> dimensionReferences = currentAttr.DimensionReferences;
                        foreach (IGroup grp in buildFrom.Groups)
                        {
                            if (grp.DimensionRefs.ContainsAll(dimensionReferences) && dimensionReferences.ContainsAll(grp.DimensionRefs))
                            {
                                newAttribute.attachmentLevel = AttachmentLevelTypeConstants.Group;
                                newAttribute.AttachmentGroup.Add(grp.Id);
                                break;
                            }
                        }
                    }

                    if (crossSectionalAttachDataSet.Contains(currentAttr))
                    {
                        newAttribute.crossSectionalAttachDataSet = true;
                    }

                    if (crossSectionalAttachGroup.Contains(currentAttr))
                    {
                        newAttribute.crossSectionalAttachGroup = true;
                    }

                    if (crossSectionalAttachSection.Contains(currentAttr))
                    {
                        newAttribute.crossSectionalAttachSection = true;
                    }

                    if (crossSectionalAttachObservation.Contains(currentAttr))
                    {
                        newAttribute.crossSectionalAttachObservation = true;
                    }

                    if (crossSectionalDataStructureObject != null)
                    {
                        /* foreach */
                        foreach (ICrossSectionalMeasure crossSectionalMeasure in
                            crossSectionalDataStructureObject.GetAttachmentMeasures(currentAttr))
                        {
                            newAttribute.AttachmentMeasure.Add(crossSectionalMeasure.Id);
                        }
                    }

                    componentsType.Attribute.Add(newAttribute);
                }
            }
        }

        /// <summary>
        /// Handles the groups.
        /// </summary>
        /// <param name="currentGroups">The current groups.</param>
        /// <param name="componentsType">Type of the components.</param>
        private void HandleGroups(ICollection<IGroup> currentGroups, ComponentsType componentsType)
        {
            if (ObjectUtil.ValidCollection(currentGroups))
            {
                foreach (IGroup currentGroup in currentGroups)
                {
                    componentsType.Group.Add(this._groupXmlsBuilder.Build(currentGroup));
                }
            }
        }

        /// <summary>
        /// Handles the time dimension.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="crossSectionalAttachDataSet">The cross sectional attach data set.</param>
        /// <param name="crossSectionalAttachGroup">The cross sectional attach group.</param>
        /// <param name="crossSectionalAttachSection">The cross sectional attach section.</param>
        /// <param name="crossSectionalAttachObservation">The cross sectional attach observation.</param>
        /// <param name="componentsType">Type of the components.</param>
        private void HandleTimeDimension(
            IDataStructureObject buildFrom, 
            ICollection<IComponent> crossSectionalAttachDataSet, 
            ICollection<IComponent> crossSectionalAttachGroup, 
            ICollection<IComponent> crossSectionalAttachSection, 
            ICollection<IComponent> crossSectionalAttachObservation, 
            ComponentsType componentsType)
        {
            if (buildFrom.TimeDimension != null)
            {
                TimeDimensionType newDimension0 = this._timeDimensionXmlBuilder.Build(buildFrom.TimeDimension);
                if (crossSectionalAttachDataSet.Contains(buildFrom.TimeDimension))
                {
                    newDimension0.crossSectionalAttachDataSet = true;
                }

                if (crossSectionalAttachGroup.Contains(buildFrom.TimeDimension))
                {
                    newDimension0.crossSectionalAttachGroup = true;
                }

                if (crossSectionalAttachSection.Contains(buildFrom.TimeDimension))
                {
                    newDimension0.crossSectionalAttachSection = true;
                }

                if (crossSectionalAttachObservation.Contains(buildFrom.TimeDimension))
                {
                    newDimension0.crossSectionalAttachObservation = true;
                }

                componentsType.TimeDimension = newDimension0;
            }
        }

        /// <summary>
        /// Handles the dimensions.
        /// </summary>
        /// <param name="currentDims">The current dims.</param>
        /// <param name="crossSectionalDataStructureObject">The cross sectional data structure object.</param>
        /// <param name="crossSectionalAttachDataSet">The cross sectional attach data set.</param>
        /// <param name="crossSectionalAttachGroup">The cross sectional attach group.</param>
        /// <param name="crossSectionalAttachSection">The cross sectional attach section.</param>
        /// <param name="crossSectionalAttachObservation">The cross sectional attach observation.</param>
        /// <param name="componentsType">Type of the components.</param>
        private void HandleDimensions(
            ICollection<IDimension> currentDims, 
            ICrossSectionalDataStructureObject crossSectionalDataStructureObject, 
            ICollection<IComponent> crossSectionalAttachDataSet, 
            ICollection<IComponent> crossSectionalAttachGroup, 
            ICollection<IComponent> crossSectionalAttachSection, 
            ICollection<IComponent> crossSectionalAttachObservation, 
            ComponentsType componentsType)
        {
            if (ObjectUtil.ValidCollection(currentDims))
            {
                /* foreach */
                foreach (IDimension currentDim in currentDims)
                {
                    DimensionType newDimension = this._dimensionXmlBuilder.Build(currentDim);
                    if (crossSectionalDataStructureObject != null && currentDim.MeasureDimension)
                    {
                        ICrossReference xsRef = crossSectionalDataStructureObject.GetCodelistForMeasureDimension(currentDim.Id);
                        newDimension.codelist = xsRef.MaintainableReference.MaintainableId;
                        newDimension.codelistAgency = xsRef.MaintainableReference.AgencyId;
                        newDimension.codelistVersion = xsRef.MaintainableReference.Version;
                    }

                    if (crossSectionalAttachDataSet.Contains(currentDim))
                    {
                        newDimension.crossSectionalAttachDataSet = true;
                    }

                    if (crossSectionalAttachGroup.Contains(currentDim))
                    {
                        newDimension.crossSectionalAttachGroup = true;
                    }

                    if (crossSectionalAttachSection.Contains(currentDim))
                    {
                        newDimension.crossSectionalAttachSection = true;
                    }

                    if (crossSectionalAttachObservation.Contains(currentDim))
                    {
                        newDimension.crossSectionalAttachObservation = true;
                    }

                    componentsType.Dimension.Add(newDimension);
                }
            }
        }

        /// <summary>
        /// Populates the cross sectional information.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="crossSectionalAttachDataSet">The cross sectional attach data set.</param>
        /// <param name="crossSectionalAttachGroup">The cross sectional attach group.</param>
        /// <param name="crossSectionalAttachSection">The cross sectional attach section.</param>
        /// <param name="crossSectionalAttachObservation">The cross sectional attach observation.</param>
        /// <param name="componentsType">Type of the components.</param>
        /// <returns>The <see cref="ICrossSectionalDataStructureObject"/></returns>
        private ICrossSectionalDataStructureObject PopulateCrossSectionalInfo(
            IIdentifiableObject buildFrom, 
            List<IComponent> crossSectionalAttachDataSet, 
            List<IComponent> crossSectionalAttachGroup, 
            List<IComponent> crossSectionalAttachSection, 
            List<IComponent> crossSectionalAttachObservation, 
            ComponentsType componentsType)
        {
            var crossSectionalDataStructureObject = buildFrom as ICrossSectionalDataStructureObject;
            if (crossSectionalDataStructureObject != null)
            {
                crossSectionalAttachDataSet.AddRange(crossSectionalDataStructureObject.GetCrossSectionalAttachDataSet(false));
                crossSectionalAttachGroup.AddRange(crossSectionalDataStructureObject.GetCrossSectionalAttachGroup(false));
                crossSectionalAttachSection.AddRange(crossSectionalDataStructureObject.GetCrossSectionalAttachSection(false));
                crossSectionalAttachObservation.AddRange(crossSectionalDataStructureObject.GetCrossSectionalAttachObservation());

                foreach (ICrossSectionalMeasure currentMeasure in crossSectionalDataStructureObject.CrossSectionalMeasures)
                {
                    componentsType.CrossSectionalMeasure.Add(this._crossSectionalXmlBuilder.Build(currentMeasure));
                }
            }

            return crossSectionalDataStructureObject;
        }
    }
}