// -----------------------------------------------------------------------
// <copyright file="MetadataStructureDefinitionXmlsBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V2
{
    using System;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Sdmx.Structureparser.Factory;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    using TextType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common.TextType;

    /// <summary>
    ///     The metadata structure definition xml beans builder.
    /// </summary>
    public class MetadataStructureDefinitionXmlsBuilder : AbstractBuilder, 
                                                          IBuilder<MetadataStructureDefinitionType, IMetadataStructureDefinitionObject>
    {
        /// <summary>
        ///     The _report structure XML builder
        /// </summary>
        private readonly IBuilder<ReportStructureType, IReportStructure> _reportStructureXmlBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="MetadataStructureDefinitionXmlsBuilder"/> class.
        /// </summary>
        /// <param name="builderFactory">The builder factory.</param>
        /// <exception cref="System.ArgumentNullException">The <paramref name="builderFactory" /> is null.</exception>
        /// <remarks>Used by <see cref="XmlBuilderFactoryV2"/></remarks>
        public MetadataStructureDefinitionXmlsBuilder(IBuilderFactory builderFactory)
        {
            if (builderFactory == null)
            {
                throw new ArgumentNullException("builderFactory");
            }

            this._reportStructureXmlBuilder = builderFactory.GetBuilder<ReportStructureType, IReportStructure>();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataStructureDefinitionXmlsBuilder" /> class.
        /// </summary>
        /// <param name="reportStructureXmlBuilder">The report structure XML builder.</param>
        public MetadataStructureDefinitionXmlsBuilder(IBuilder<ReportStructureType, IReportStructure> reportStructureXmlBuilder)
        {
            this._reportStructureXmlBuilder = reportStructureXmlBuilder ?? new ReportStructureXmlBuilder();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataStructureDefinitionXmlsBuilder" /> class.
        /// </summary>
        public MetadataStructureDefinitionXmlsBuilder()
            : this((IBuilder<ReportStructureType, IReportStructure>)null)
        {
        }

        /// <summary>
        ///     Build <see cref="MetadataStructureDefinitionType" /> from <paramref name="buildFrom" />.
        ///     NOT IMPLEMENTED.
        /// </summary>
        /// <param name="buildFrom">
        ///     The build from.
        /// </param>
        /// <returns>
        ///     The <see cref="MetadataStructureDefinitionType" /> from <paramref name="buildFrom" /> .
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="buildFrom"/> is <see langword="null" />.</exception>
        public virtual MetadataStructureDefinitionType Build(IMetadataStructureDefinitionObject buildFrom)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            var builtObj = new MetadataStructureDefinitionType();
            this.HandleMaintainableInformation(buildFrom, builtObj);
            if (ObjectUtil.ValidCollection(buildFrom.MetadataTargets))
            {
                TargetIdentifiersType targetIdentifiers = new TargetIdentifiersType();
                builtObj.TargetIdentifiers = targetIdentifiers;

                // find the metadata target which has the superset of identifier components, it will act as FullTargetIdentifier
                int maxMetadataTargetSize = 0;
                int fullTargetPosition = 0;
                IMetadataTarget candidateFullTarget = null;
                var partialTargets = new PartialTargetIdentifierType[buildFrom.MetadataTargets.Count];

                for (int i = 0; i < buildFrom.MetadataTargets.Count; i++)
                {
                    var metadataTarget = buildFrom.MetadataTargets[i];
                    if (metadataTarget.KeyDescriptorValuesTarget != null || metadataTarget.ReportPeriodTarget != null
                        || metadataTarget.ConstraintContentTarget != null || metadataTarget.DataSetTarget != null)
                    {
                        throw new SdmxNotImplementedException(
                            ExceptionCode.Unsupported, 
                            "MSD contains Metadata Target content incompatible with SMDX v2.0 - please use SDMX v2.1");
                    }

                    if (metadataTarget.IdentifiableTarget.Count >= maxMetadataTargetSize)
                    {
                        maxMetadataTargetSize = metadataTarget.IdentifiableTarget.Count;
                        fullTargetPosition = i;
                        candidateFullTarget = metadataTarget;
                    }

                    partialTargets[i] = this.BuildPartialTargetIdentifier(metadataTarget);
                }

                targetIdentifiers.PartialTargetIdentifier.AddAll(partialTargets);
                targetIdentifiers.PartialTargetIdentifier.RemoveAt(fullTargetPosition);
                targetIdentifiers.FullTargetIdentifier = new FullTargetIdentifierType();
                this.PopulateFullTargetIdentifier(candidateFullTarget, targetIdentifiers.FullTargetIdentifier);
            }

            if (ObjectUtil.ValidCollection(buildFrom.ReportStructures))
            {
                foreach (var reportStructure in buildFrom.ReportStructures)
                {
                    builtObj.ReportStructure.Add(this._reportStructureXmlBuilder.Build(reportStructure));
                }
            }

            // needs to be last in .NET
            if (this.HasAnnotations(buildFrom))
            {
                builtObj.Annotations = this.GetAnnotationsType(buildFrom);
            }

            return builtObj;
        }

        /// <summary>
        /// Builds the identifier component.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <returns>The component type</returns>
        private IdentifierComponentType BuildIdentifierComponent(IIdentifiableTarget buildFrom)
        {
            var builtObj = new IdentifierComponentType();

            if (!string.IsNullOrWhiteSpace(buildFrom.Id))
            {
                builtObj.id = buildFrom.Id;
            }

            if (buildFrom.Uri != null)
            {
                builtObj.uri = buildFrom.Uri;
            }

            if (buildFrom.Urn != null)
            {
                builtObj.urn = buildFrom.Urn;
            }

            var textType = new TextType();
            this.SetDefaultText(textType);
            builtObj.Name.Add(textType);

            if (buildFrom.ReferencedStructureType != null)
            {
                var targetObjectClass = buildFrom.ReferencedStructureType == SdmxStructureEnumType.TimeDimension
                                            ? SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dimension)
                                            : buildFrom.ReferencedStructureType;

                builtObj.TargetObjectClass = XmlobjectsEnumUtil.GetSdmxObjectIdType(targetObjectClass);
            }

            if (buildFrom.HasCodedRepresentation())
            {
                var representationSchemeTarget = new RepresentationSchemeType();
                builtObj.RepresentationScheme = representationSchemeTarget;

                var reference = buildFrom.Representation.Representation;
                if (reference != null)
                {
                    if (reference.HasMaintainableId())
                    {
                        representationSchemeTarget.representationScheme = reference.MaintainableId;
                    }

                    if (reference.HasAgencyId())
                    {
                        representationSchemeTarget.representationSchemeAgency = reference.AgencyId;
                    }

                    representationSchemeTarget.representationSchemeType1 =
                        XmlobjectsEnumUtil.GetSdmxRepresentationSchemeType(reference.TargetReference);
                }
            }

            // needs to be last in .NET
            if (this.HasAnnotations(buildFrom))
            {
                builtObj.Annotations = this.GetAnnotationsType(buildFrom);
            }

            return builtObj;
        }

        /// <summary>
        ///     Builds the partial target identifier.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <returns>
        ///     The <see cref="PartialTargetIdentifierType" />
        /// </returns>
        private PartialTargetIdentifierType BuildPartialTargetIdentifier(IMetadataTarget buildFrom)
        {
            var builtObj = new PartialTargetIdentifierType();
            if (!string.IsNullOrWhiteSpace(buildFrom.Id))
            {
                builtObj.id = buildFrom.Id;
            }

            if (buildFrom.Uri != null)
            {
                builtObj.uri = buildFrom.Uri;
            }

            if (buildFrom.Urn != null)
            {
                builtObj.urn = buildFrom.Urn;
            }

            var textType = new TextType();
            this.SetDefaultText(textType);
            builtObj.Name.Add(textType);

            if (ObjectUtil.ValidCollection(buildFrom.IdentifiableTarget))
            {
                foreach (var identifiableTarget in buildFrom.IdentifiableTarget)
                {
                    builtObj.IdentifierComponentRef.Add(identifiableTarget.Id);
                }
            }

            // needs to be last in .NET
            if (this.HasAnnotations(buildFrom))
            {
                builtObj.Annotations = this.GetAnnotationsType(buildFrom);
            }

            return builtObj;
        }

        /// <summary>
        /// Populates the full target identifier.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="builtObj">The built object.</param>
        private void PopulateFullTargetIdentifier(IMetadataTarget buildFrom, FullTargetIdentifierType builtObj)
        {
            if (!string.IsNullOrWhiteSpace(buildFrom.Id))
            {
                builtObj.id = buildFrom.Id;
            }

            if (buildFrom.Uri != null)
            {
                builtObj.uri = buildFrom.Uri;
            }

            if (buildFrom.Urn != null)
            {
                builtObj.urn = buildFrom.Urn;
            }

            var textType = new TextType();
            this.SetDefaultText(textType);
            builtObj.Name.Add(textType);

            if (ObjectUtil.ValidCollection(buildFrom.IdentifiableTarget))
            {
                foreach (var identifiableTarget in buildFrom.IdentifiableTarget)
                {
                    builtObj.IdentifierComponent.Add(this.BuildIdentifierComponent(identifiableTarget));
                }
            }

            // needs to be last in .NET
            if (this.HasAnnotations(buildFrom))
            {
                builtObj.Annotations = this.GetAnnotationsType(buildFrom);
            }
        }
    }
}