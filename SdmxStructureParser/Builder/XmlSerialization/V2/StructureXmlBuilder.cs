// -----------------------------------------------------------------------
// <copyright file="StructureXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V2
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Message;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response.V2;
    using Org.Sdmxsource.Sdmx.Structureparser.Factory;

    using Xml.Schema.Linq;

    using StructureType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Message.StructureType;

    /// <summary>
    ///     The structure xml bean builder.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "It is OK. Big number of SDMX v2.0 artefacts")]
    public class StructureXmlBuilder : IBuilder<Structure, ISdmxObjects>
    {
        /// <summary>
        /// The _builder factory
        /// </summary>
        private readonly IBuilderFactory _builderFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="StructureXmlBuilder"/> class.
        /// </summary>
        /// <param name="builderFactory">
        /// The builder factory.
        /// </param>
        public StructureXmlBuilder(IBuilderFactory builderFactory)
        {
            if (builderFactory == null)
            {
                throw new ArgumentNullException("builderFactory");
            }

            this._builderFactory = builderFactory;
        }

        /// <summary>
        /// Build <see cref="Structure"/> from <paramref name="buildFrom"/>.
        /// </summary>
        /// <param name="buildFrom">
        /// The source SDMX Object.
        /// </param>
        /// <returns>
        /// The <see cref="Structure"/> from <paramref name="buildFrom"/> .
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="buildFrom"/> is <see langword="null"/>.
        /// </exception>
        public virtual Structure Build(ISdmxObjects buildFrom)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            var doc = new Structure();
            StructureType returnType = doc.Content;

            // HEADER
            HeaderType headerType;
            if (buildFrom.Header != null)
            {
                headerType = this._builderFactory.GetBuilder<HeaderType, IHeader>().Build(buildFrom.Header);
                returnType.Header = headerType;
            }
            else
            {
                headerType = new HeaderType();
                returnType.Header = headerType;

                V2Helper.SetHeader(headerType, buildFrom);
            }

            // TOP LEVEL STRUCTURES ELEMENT
            ISet<ICategorisationObject> categorisations = buildFrom.Categorisations;

            // GET CATEGORY SCHEMES
            var categorySchemeObjects = buildFrom.CategorySchemes;
            returnType.CategorySchemes = this.Build(categorySchemeObjects, categorisations);

            // GET CODELISTS
            var codelistObjects = buildFrom.Codelists;
            returnType.CodeLists = this.Build(codelistObjects);

            // CONCEPT SCHEMES
            ISet<IConceptSchemeObject> conceptSchemeObjects = buildFrom.ConceptSchemes;
            returnType.Concepts = this.Build(conceptSchemeObjects);

            // DATAFLOWS
            ISet<IDataflowObject> dataflowObjects = buildFrom.Dataflows;
            returnType.Dataflows = this.Build(dataflowObjects, categorisations);

            // HIERARCIC CODELIST
            ISet<IHierarchicalCodelistObject> hierarchicalCodelistObjects = buildFrom.HierarchicalCodelists;
            returnType.HierarchicalCodelists = this.Build(hierarchicalCodelistObjects);

            // KEY FAMILY
            ISet<IDataStructureObject> dataStructureObjects = buildFrom.DataStructures;
            returnType.KeyFamilies = this.Build(dataStructureObjects);

            // METADATA FLOW
            ISet<IMetadataFlow> metadataFlows = buildFrom.Metadataflows;
            returnType.Metadataflows = this.Build(metadataFlows, categorisations);

            // METADATA STRUCTURE
            ISet<IMetadataStructureDefinitionObject> metadataStructureDefinitionObjects = buildFrom.MetadataStructures;
            returnType.MetadataStructureDefinitions = this.Build(metadataStructureDefinitionObjects);

            // ORGAISATION SCHEME
            returnType.OrganisationSchemes = this.BuildOrganisationSchemes(buildFrom);

            // PROCESSES
            ISet<IProcessObject> processObjects = buildFrom.Processes;
            returnType.Processes = this.Build(processObjects);

            // STRUCTURE SETS
            ISet<IStructureSetObject> structureSetObjects = buildFrom.StructureSets;
            returnType.StructureSets = this.Build(structureSetObjects);

            // REPORTING TAXONOMIES
            ISet<IReportingTaxonomyObject> reportingTaxonomyObjects = buildFrom.ReportingTaxonomys;
            returnType.ReportingTaxonomies = this.Build(reportingTaxonomyObjects);

            ISet<IAttachmentConstraintObject> attachmentConstraintObjects = buildFrom.AttachmentConstraints;
            if (attachmentConstraintObjects.Count > 0)
            {
                throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "Attachment Constraint at SMDX v2.0 - please use SDMX v2.1");
            }

            ISet<IContentConstraintObject> contentConstraintObjects = buildFrom.ContentConstraintObjects;
            if (contentConstraintObjects.Count > 0)
            {
                throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "Content Constraint at SMDX v2.0 - please use SDMX v2.1");
            }

            return doc;
        }

        /// <summary>
        /// Build the XMLObject if Dataflow will return DataflowType
        /// </summary>
        /// <param name="bean">
        /// - the Bean to build the type for
        /// </param>
        /// <param name="categorisations">
        /// - optional (for v2 only if Maintainable is Dataflow, Metadataflow or Category)
        /// </param>
        /// <returns>
        /// The <see cref="XTypedElement"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="categorisations"/> is <see langword="null"/>.
        /// </exception>
        public XTypedElement Build(IMaintainableObject bean, ISet<ICategorisationObject> categorisations)
        {
            if (bean == null)
            {
                throw new ArgumentNullException("bean");
            }

            if (categorisations == null)
            {
                throw new ArgumentNullException("categorisations");
            }

            // FUNC 2.1 support IAgencyScheme, IDataConsumerScheme and IDataProviderScheme
            switch (bean.StructureType.EnumType)
            {
                case SdmxStructureEnumType.CategoryScheme:
                    return this.Build((ICategorySchemeObject)bean, categorisations);
                case SdmxStructureEnumType.CodeList:
                    return this.Build((ICodelistObject)bean);
                case SdmxStructureEnumType.ConceptScheme:
                    return this.Build((IConceptSchemeObject)bean);
                case SdmxStructureEnumType.Dataflow:
                    return this.Build((IDataflowObject)bean, categorisations);
                case SdmxStructureEnumType.HierarchicalCodelist:
                    return this.Build((IHierarchicalCodelistObject)bean);
                case SdmxStructureEnumType.Dsd:
                    return this.Build((IDataStructureObject)bean);
                case SdmxStructureEnumType.MetadataFlow:
                    return this.Build((IMetadataFlow)bean, categorisations);
                case SdmxStructureEnumType.Msd:
                    return this.Build((IMetadataStructureDefinitionObject)bean);
                case SdmxStructureEnumType.Process:
                    return this.Build((IProcessObject)bean);
                case SdmxStructureEnumType.ReportingTaxonomy:
                    return this.Build((IReportingTaxonomyObject)bean);
                case SdmxStructureEnumType.StructureSet:
                    return this.Build((IStructureSetObject)bean);
                default:
                    throw new SdmxNotImplementedException(ExceptionCode.Unsupported, bean.StructureType);
            }
        }

        /// <summary>
        /// The build.
        /// </summary>
        /// <param name="bean">
        /// The bean.
        /// </param>
        /// <param name="categorisations">
        /// The categorisations.
        /// </param>
        /// <returns>
        /// The <see cref="CategorySchemeType"/>.
        /// </returns>
        public CategorySchemeType Build(ICategorySchemeObject bean, ISet<ICategorisationObject> categorisations)
        {
            return this._builderFactory.GetCategorisedBuilder<CategorySchemeType, ICategorySchemeObject>().Build(bean, categorisations);
        }

        /// <summary>
        /// The build.
        /// </summary>
        /// <param name="bean">
        /// The bean.
        /// </param>
        /// <returns>
        /// The <see cref="CodeListType"/>.
        /// </returns>
        public CodeListType Build(ICodelistObject bean)
        {
            return this._builderFactory.GetBuilder<CodeListType, ICodelistObject>().Build(bean);
        }

        /// <summary>
        /// The build.
        /// </summary>
        /// <param name="bean">
        /// The bean.
        /// </param>
        /// <returns>
        /// The <see cref="ConceptSchemeType"/>.
        /// </returns>
        public ConceptSchemeType Build(IConceptSchemeObject bean)
        {
            return this._builderFactory.GetBuilder<ConceptSchemeType, IConceptSchemeObject>().Build(bean);
        }

        /// <summary>
        /// The build.
        /// </summary>
        /// <param name="bean">
        /// The bean.
        /// </param>
        /// <returns>
        /// The <see cref="ConceptType"/>.
        /// </returns>
        public ConceptType Build(IConceptObject bean)
        {
            return this._builderFactory.GetBuilder<ConceptType, IConceptObject>().Build(bean);
        }

        /// <summary>
        /// The build.
        /// </summary>
        /// <param name="bean">
        /// The bean.
        /// </param>
        /// <param name="categorisations">
        /// The categorisations.
        /// </param>
        /// <returns>
        /// The <see cref="DataflowType"/>.
        /// </returns>
        public DataflowType Build(IDataflowObject bean, ISet<ICategorisationObject> categorisations)
        {
            return this._builderFactory.GetCategorisedBuilder<DataflowType, IDataflowObject>().Build(bean, categorisations);
        }

        /// <summary>
        /// The build.
        /// </summary>
        /// <param name="bean">
        /// The bean.
        /// </param>
        /// <returns>
        /// The <see cref="HierarchicalCodelistType"/>.
        /// </returns>
        public HierarchicalCodelistType Build(IHierarchicalCodelistObject bean)
        {
            return this._builderFactory.GetBuilder<HierarchicalCodelistType, IHierarchicalCodelistObject>().Build(bean);
        }

        /// <summary>
        /// The build.
        /// </summary>
        /// <param name="bean">
        /// The bean.
        /// </param>
        /// <returns>
        /// The <see cref="KeyFamilyType"/>.
        /// </returns>
        public KeyFamilyType Build(IDataStructureObject bean)
        {
            return this._builderFactory.GetBuilder<KeyFamilyType, IDataStructureObject>().Build(bean);
        }

        /// <summary>
        /// The build.
        /// </summary>
        /// <param name="bean">
        /// The bean.
        /// </param>
        /// <param name="categorisations">
        /// The categorisations.
        /// </param>
        /// <returns>
        /// The <see cref="MetadataflowType"/>.
        /// </returns>
        public MetadataflowType Build(IMetadataFlow bean, ISet<ICategorisationObject> categorisations)
        {
            return this._builderFactory.GetCategorisedBuilder<MetadataflowType, IMetadataFlow>().Build(bean, categorisations);
        }

        /// <summary>
        /// The build.
        /// </summary>
        /// <param name="bean">
        /// The bean.
        /// </param>
        /// <returns>
        /// The <see cref="MetadataStructureDefinitionType"/>.
        /// </returns>
        public MetadataStructureDefinitionType Build(IMetadataStructureDefinitionObject bean)
        {
            return this._builderFactory.GetBuilder<MetadataStructureDefinitionType, IMetadataStructureDefinitionObject>().Build(bean);
        }

        /// <summary>
        /// The build.
        /// </summary>
        /// <param name="bean">
        /// The bean.
        /// </param>
        /// <returns>
        /// The <see cref="ProcessType"/>.
        /// </returns>
        public ProcessType Build(IProcessObject bean)
        {
            return this._builderFactory.GetBuilder<ProcessType, IProcessObject>().Build(bean);
        }

        /// <summary>
        /// The build.
        /// </summary>
        /// <param name="bean">
        /// The bean.
        /// </param>
        /// <returns>
        /// The <see cref="ReportingTaxonomyType"/>.
        /// </returns>
        public ReportingTaxonomyType Build(IReportingTaxonomyObject bean)
        {
            return this._builderFactory.GetBuilder<ReportingTaxonomyType, IReportingTaxonomyObject>().Build(bean);
        }

        /// <summary>
        /// The build.
        /// </summary>
        /// <param name="bean">
        /// The bean.
        /// </param>
        /// <returns>
        /// The <see cref="StructureSetType"/>.
        /// </returns>
        public StructureSetType Build(IStructureSetObject bean)
        {
            return this._builderFactory.GetBuilder<StructureSetType, IStructureSetObject>().Build(bean);
        }

        /// <summary>
        /// Builds the specified reporting taxonomy objects.
        /// </summary>
        /// <param name="reportingTaxonomyObjects">
        /// The reporting taxonomy objects.
        /// </param>
        /// <returns>
        /// The <see cref="ReportingTaxonomiesType"/>.
        /// </returns>
        internal ReportingTaxonomiesType Build(ISet<IReportingTaxonomyObject> reportingTaxonomyObjects)
        {
            return reportingTaxonomyObjects.Build<ReportingTaxonomiesType, ReportingTaxonomyType, IReportingTaxonomyObject>(this._builderFactory, (type, xmlType) => type.ReportingTaxonomy.Add(xmlType));
        }

        /// <summary>
        /// Builds the specified structure set objects.
        /// </summary>
        /// <param name="structureSetObjects">
        /// The structure set objects.
        /// </param>
        /// <returns>
        /// The <see cref="StructureSetsType"/>.
        /// </returns>
        internal StructureSetsType Build(ISet<IStructureSetObject> structureSetObjects)
        {
            return structureSetObjects.Build<StructureSetsType, StructureSetType, IStructureSetObject>(this._builderFactory, (type, xmlType) => type.StructureSet.Add(xmlType));
        }

        /// <summary>
        /// Builds the specified process objects.
        /// </summary>
        /// <param name="processObjects">
        /// The process objects.
        /// </param>
        /// <returns>
        /// The <see cref="ProcessesType"/>.
        /// </returns>
        internal ProcessesType Build(ISet<IProcessObject> processObjects)
        {
            return processObjects.Build<ProcessesType, ProcessType, IProcessObject>(this._builderFactory, (type, xmlType) => type.Process.Add(xmlType));
        }

        /// <summary>
        /// Builds the specified metadata structure definition objects.
        /// </summary>
        /// <param name="metadataStructureDefinitionObjects">
        /// The metadata structure definition objects.
        /// </param>
        /// <returns>
        /// The <see cref="MetadataStructureDefinitionsType"/>.
        /// </returns>
        internal MetadataStructureDefinitionsType Build(ISet<IMetadataStructureDefinitionObject> metadataStructureDefinitionObjects)
        {
            return metadataStructureDefinitionObjects.Build<MetadataStructureDefinitionsType, MetadataStructureDefinitionType, IMetadataStructureDefinitionObject>(
                this._builderFactory,
                (type, xmlType) => type.MetadataStructureDefinition.Add(xmlType));
        }

        /// <summary>
        /// Builds the specified metadata flows.
        /// </summary>
        /// <param name="metadataFlows">
        /// The metadata flows.
        /// </param>
        /// <param name="categorisations">
        /// The categorisations.
        /// </param>
        /// <returns>
        /// The <see cref="MetadataflowsType"/>.
        /// </returns>
        internal MetadataflowsType Build(ISet<IMetadataFlow> metadataFlows, ISet<ICategorisationObject> categorisations)
        {
            return metadataFlows.BuildCategorised<MetadataflowsType, MetadataflowType, IMetadataFlow>(this._builderFactory, categorisations, (type, xmlType) => type.Metadataflow.Add(xmlType));
        }

        /// <summary>
        /// Builds the specified data structure objects.
        /// </summary>
        /// <param name="dataStructureObjects">
        /// The data structure objects.
        /// </param>
        /// <returns>
        /// The <see cref="KeyFamiliesType"/>.
        /// </returns>
        internal KeyFamiliesType Build(ISet<IDataStructureObject> dataStructureObjects)
        {
            return dataStructureObjects.Build<KeyFamiliesType, KeyFamilyType, IDataStructureObject>(this._builderFactory, (type, xmlType) => type.KeyFamily.Add(xmlType));
        }

        /// <summary>
        /// Builds the specified hierarchical codelist objects.
        /// </summary>
        /// <param name="hierarchicalCodelistObjects">
        /// The hierarchical codelist objects.
        /// </param>
        /// <returns>
        /// The <see cref="HierarchicalCodelistsType"/>.
        /// </returns>
        internal HierarchicalCodelistsType Build(ISet<IHierarchicalCodelistObject> hierarchicalCodelistObjects)
        {
            return hierarchicalCodelistObjects.Build<HierarchicalCodelistsType, HierarchicalCodelistType, IHierarchicalCodelistObject>(this._builderFactory, (type, xmlType) => type.HierarchicalCodelist.Add(xmlType));
        }

        /// <summary>
        /// Builds the specified dataflow objects.
        /// </summary>
        /// <param name="dataflowObjects">
        /// The dataflow objects.
        /// </param>
        /// <param name="categorisations">
        /// The categorisations.
        /// </param>
        /// <returns>
        /// The <see cref="DataflowsType"/>.
        /// </returns>
        internal DataflowsType Build(ISet<IDataflowObject> dataflowObjects, ISet<ICategorisationObject> categorisations)
        {
            return dataflowObjects.BuildCategorised<DataflowsType, DataflowType, IDataflowObject>(this._builderFactory, categorisations, (type, xmlType) => type.Dataflow.Add(xmlType));
        }

        /// <summary>
        /// Builds the specified concept scheme objects.
        /// </summary>
        /// <param name="conceptSchemeObjects">
        /// The concept scheme objects.
        /// </param>
        /// <returns>
        /// The <see cref="ConceptsType"/>.
        /// </returns>
        internal ConceptsType Build(ISet<IConceptSchemeObject> conceptSchemeObjects)
        {
            return conceptSchemeObjects.Build<ConceptsType, ConceptSchemeType, IConceptSchemeObject>(this._builderFactory, (type, xmlType) => type.ConceptScheme.Add(xmlType));
        }

        /// <summary>
        /// Builds the specified codelist objects.
        /// </summary>
        /// <param name="codelistObjects">
        /// The codelist objects.
        /// </param>
        /// <returns>
        /// The <see cref="CodeListsType"/>.
        /// </returns>
        internal CodeListsType Build(ISet<ICodelistObject> codelistObjects)
        {
            return codelistObjects.Build<CodeListsType, CodeListType, ICodelistObject>(this._builderFactory, (type, xmlType) => type.CodeList.Add(xmlType));
        }

        /// <summary>
        /// Builds the specified category scheme objects.
        /// </summary>
        /// <param name="categorySchemeObjects">The category scheme objects.</param>
        /// <param name="categorisations">The categorisations.</param>
        /// <returns>
        /// The <see cref="CategorySchemesType" />.
        /// </returns>
        internal CategorySchemesType Build(ISet<ICategorySchemeObject> categorySchemeObjects, ISet<ICategorisationObject> categorisations)
        {
            return categorySchemeObjects.BuildCategorised<CategorySchemesType, CategorySchemeType, ICategorySchemeObject>(this._builderFactory, categorisations, (type, xmlType) => type.CategoryScheme.Add(xmlType));
        }

        /// <summary>
        /// Builds the organisation schemes.
        /// </summary>
        /// <param name="buildFrom">
        /// The build from.
        /// </param>
        /// <exception cref="SdmxNotImplementedException">
        /// Organisation unit not supported for SDMX v2.0
        /// </exception>
        /// <returns>
        /// The <see cref="OrganisationSchemesType"/>.
        /// </returns>
        internal OrganisationSchemesType BuildOrganisationSchemes(ISdmxObjects buildFrom)
        {
            ISet<IOrganisationUnitSchemeObject> organisationUnitSchemeObjects = buildFrom.OrganisationUnitSchemes;
            if (organisationUnitSchemeObjects.Count > 0)
            {
                throw new SdmxNotImplementedException(ExceptionCode.Unsupported, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.OrganisationUnitScheme).StructureType);
            }

            // AGENCY SCHEMES
            OrganisationSchemesType orgSchemesType = buildFrom.AgenciesSchemes.Build<OrganisationSchemesType, OrganisationSchemeType, IAgencyScheme>(this._builderFactory, this.AddXmlObjectToContainer);

            // DATA CONSUMER SCHEMES
            orgSchemesType = buildFrom.DataConsumerSchemes.Build<OrganisationSchemesType, OrganisationSchemeType, IDataConsumerScheme>(orgSchemesType, this._builderFactory, this.AddXmlObjectToContainer);

            // DATA PROVIDER SCHEMES
            orgSchemesType = buildFrom.DataProviderSchemes.Build<OrganisationSchemesType, OrganisationSchemeType, IDataProviderScheme>(orgSchemesType, this._builderFactory, this.AddXmlObjectToContainer);

            return orgSchemesType;
        }

        /// <summary>
        /// Adds the XML object to container.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        /// <param name="xmlObject">
        /// The XML object.
        /// </param>
        private void AddXmlObjectToContainer(OrganisationSchemesType container, OrganisationSchemeType xmlObject)
        {
            container.OrganisationScheme.Add(xmlObject);
        }
    }
}