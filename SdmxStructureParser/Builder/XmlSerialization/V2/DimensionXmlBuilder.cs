// -----------------------------------------------------------------------
// <copyright file="DimensionXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V2
{
    using System;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Structureparser.Factory;

    /// <summary>
    ///     The dimension xml bean builder.
    /// </summary>
    public class DimensionXmlBuilder : AbstractBuilder, IBuilder<DimensionType, IDimension>
    {
        /// <summary>
        /// The _component role builder
        /// </summary>
        private readonly IBuilder<ComponentRole, IStructureReference> _componentRoleBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="DimensionXmlBuilder"/> class.
        /// </summary>
        /// <param name="builderFactory">The builder factory.</param>
        public DimensionXmlBuilder(IBuilderFactory builderFactory)
        {
            if (builderFactory == null)
            {
                throw new ArgumentNullException("builderFactory");
            }

            this._componentRoleBuilder = builderFactory.GetBuilder<ComponentRole, IStructureReference>();
        }

        /// <summary>
        ///     Build <see cref="DimensionType" /> from <paramref name="buildFrom" />.
        /// </summary>
        /// <param name="buildFrom">
        ///     The build from.
        /// </param>
        /// <returns>
        ///     The <see cref="DimensionType" /> from <paramref name="buildFrom" /> .
        /// </returns>
        public virtual DimensionType Build(IDimension buildFrom)
        {
            var builtObj = new DimensionType();

            if (this.HasAnnotations(buildFrom))
            {
                builtObj.Annotations = this.GetAnnotationsType(buildFrom);
            }

            if (buildFrom.HasCodedRepresentation())
            {
                IMaintainableRefObject maintRef = buildFrom.Representation.Representation.MaintainableReference;
                if (!string.IsNullOrWhiteSpace(maintRef.MaintainableId))
                {
                    builtObj.codelist = maintRef.MaintainableId;
                }

                if (!string.IsNullOrWhiteSpace(maintRef.AgencyId))
                {
                    builtObj.codelistAgency = maintRef.AgencyId;
                }

                if (!string.IsNullOrWhiteSpace(maintRef.Version))
                {
                    builtObj.codelistVersion = maintRef.Version;
                }
            }

            if (buildFrom.ConceptRef != null)
            {
                IMaintainableRefObject maintainableRef = buildFrom.ConceptRef.MaintainableReference;
                if (!string.IsNullOrWhiteSpace(maintainableRef.AgencyId))
                {
                    builtObj.conceptSchemeAgency = maintainableRef.AgencyId;
                }

                if (!string.IsNullOrWhiteSpace(maintainableRef.MaintainableId))
                {
                    builtObj.conceptSchemeRef = maintainableRef.MaintainableId;
                }

                if (!string.IsNullOrWhiteSpace(buildFrom.ConceptRef.ChildReference.Id))
                {
                    builtObj.conceptRef = buildFrom.ConceptRef.ChildReference.Id;
                }

                if (!string.IsNullOrWhiteSpace(maintainableRef.Version))
                {
                    builtObj.conceptVersion = maintainableRef.Version;
                }
            }

            if (buildFrom.FrequencyDimension)
            {
                builtObj.isFrequencyDimension = buildFrom.FrequencyDimension;
            }

            if (buildFrom.MeasureDimension)
            {
                builtObj.isMeasureDimension = buildFrom.MeasureDimension;
            }

            this.BuildRoles(buildFrom, builtObj);

            if (buildFrom.Representation != null && buildFrom.Representation.TextFormat != null)
            {
                var textFormatType = new TextFormatType();
                this.PopulateTextFormatType(textFormatType, buildFrom.Representation.TextFormat);
                builtObj.TextFormat = textFormatType;
            }

            return builtObj;
        }

        /// <summary>
        /// Builds the dimension roles.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="builtObj">The built object.</param>
        private void BuildRoles(IDimension buildFrom, DimensionType builtObj)
        {
            if (this._componentRoleBuilder != null)
            {
                foreach (var reference in buildFrom.ConceptRole)
                {
                    var componentRole = this._componentRoleBuilder.Build(reference);
                    switch (componentRole)
                    {
                        case ComponentRole.Count:
                            builtObj.isCountDimension = true;
                            break;
                        case ComponentRole.NonObservationalTime:
                            builtObj.isNonObservationTimeDimension = true;
                            break;
                        case ComponentRole.Identity:
                            builtObj.isIdentityDimension = true;
                            break;
                        case ComponentRole.Entity:
                            builtObj.isEntityDimension = true;
                            break;
                    }
                }
            }
        }
    }
}