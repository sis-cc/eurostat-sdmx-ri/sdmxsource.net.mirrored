﻿// -----------------------------------------------------------------------
// <copyright file="XmlObjectBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-06-06
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing;

    using Xml.Schema.Linq;

    /// <summary>
    ///     XML object builder class
    /// </summary>
    public abstract class XmlObjectBuilder
    {
        /// <summary>
        ///     The schema location writer
        /// </summary>
        private readonly SchemaLocationWriter schemaLocationWriter = new SchemaLocationWriter();

        /// <summary>
        ///     Writes the schema location.
        /// </summary>
        /// <param name="doc">The document.</param>
        /// <param name="schemaVersion">The schema version.</param>
        /// <exception cref="SdmxNotImplementedException">Schema Version  + schemaVersion</exception>
        protected void WriteSchemaLocation(XTypedElement doc, SdmxSchemaEnumType schemaVersion)
        {
            if (doc == null)
            {
                throw new ArgumentNullException("doc");
            }

            if (this.schemaLocationWriter != null)
            {
                List<string> schemaUri = new List<string>();
                switch (schemaVersion)
                {
                    case SdmxSchemaEnumType.VersionOne:
                        schemaUri.Add(SdmxConstants.MessageNs10);
                        break;
                    case SdmxSchemaEnumType.VersionTwo:
                        schemaUri.Add(SdmxConstants.MessageNs20);
                        break;
                    case SdmxSchemaEnumType.VersionTwoPointOne:
                        schemaUri.Add(SdmxConstants.MessageNs21);
                        break;
                    default:
                        throw new SdmxNotImplementedException(
                            ExceptionCode.Unsupported, 
                            "Schema Version " + schemaVersion);
                }

                this.schemaLocationWriter.WriteSchemaLocation(doc.Untyped, schemaUri.ToArray());
            }
        }
    }
}