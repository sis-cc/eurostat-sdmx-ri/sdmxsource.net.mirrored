﻿// -----------------------------------------------------------------------
// <copyright file="XmlBuilderFactoryV2.cs" company="EUROSTAT">
//   Date Created : 2016-06-02
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Factory
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Message;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V2;
    using Org.Sdmxsource.Sdmx.Util.Objects;

    /// <summary>
    /// A factory for SDMX v2.0 SdmxSource Objects to <c>LINQ2XSD</c> objects
    /// </summary>
    /// <seealso cref="IBuilderFactory" />
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling",
        Justification = "It is OK. Need to register all builders. Cannot do it automatically currently because of duplicate entries.")]
    public class XmlBuilderFactoryV2 : IBuilderFactory
    {
        /// <summary>
        /// The _categorised map
        /// </summary>
        private readonly IDictionary<Tuple<Type, Type>, Func<IBuilderFactory, object>> _categorisedMap;

        /// <summary>
        /// The _map
        /// </summary>
        private readonly IDictionary<Tuple<Type, Type>, Func<IBuilderFactory, object>> _map;

        /// <summary>
        /// Initializes a new instance of the <see cref="XmlBuilderFactoryV2"/> class.
        /// </summary>
        public XmlBuilderFactoryV2()
        {
            // To use a proper IoC container or not. At one hand it may clean up the code and another hand it is a library and 
            // will add some dependency that will conflict with user dependencies like log4net does.
            // Also auto-registering has the problem of duplicate implementations of the same IBuilder. e.g. AgencySchemeXmlBuilder vs OrganisationSchemeXmlBuilder. 
            this._map = new Dictionary<Tuple<Type, Type>, Func<IBuilderFactory, object>>();
            this._categorisedMap = new Dictionary<Tuple<Type, Type>, Func<IBuilderFactory, object>>();
            this.RegisterDsd();
            this.RegisterOrganisation();
            this.RegisterConcepts();
            this.RegisterCodelists();
            this.RegisterHeader();
            this.RegisterMsd();
            this.RegisterProcessType();
            this.RegisterReportingTaxonomy();
            this.RegisterStructureSet();

            this.RegisterCategorised();
        }

        /// <summary>
        /// Gets an instance of a builder which builds a <typeparamref name="TTarget" /> object from a <typeparamref name="TSource" /> object
        /// </summary>
        /// <typeparam name="TTarget">The target type</typeparam>
        /// <typeparam name="TSource">The source type</typeparam>
        /// <returns>
        /// A builder which builds a <typeparamref name="TTarget" /> object from a <typeparamref name="TSource" /> object
        /// </returns>
        public IBuilder<TTarget, TSource> GetBuilder<TTarget, TSource>()
        {
            var sourceType = typeof(TSource);
            var targetType = typeof(TTarget);
            Func<IBuilderFactory, object> builder;
            if (this._map.TryGetValue(Tuple.Create(targetType, sourceType), out builder))
            {
                return (IBuilder<TTarget, TSource>)builder(this);
            }

            throw new NotImplementedException(string.Format(CultureInfo.InvariantCulture, "Could not find an implementation for IBuilder<{1}, {0}>", sourceType, targetType));
        }

        /// <summary>
        /// Gets an instance of a categorized builder which builds a <typeparamref name="TTarget" /> object from a <typeparamref name="TSource" /> object
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <returns>
        /// A builder which builds a <typeparamref name="TTarget" /> object from a <typeparamref name="TSource" /> object and a set of categorizations.
        /// </returns>
        public ICategorisedBuilder<TTarget, TSource> GetCategorisedBuilder<TTarget, TSource>()
        {
            var sourceType = typeof(TSource);
            var targetType = typeof(TTarget);
            Func<IBuilderFactory, object> builder;
            if (this._categorisedMap.TryGetValue(Tuple.Create(targetType, sourceType), out builder))
            {
                return (ICategorisedBuilder<TTarget, TSource>)builder(this);
            }

            throw new NotImplementedException(string.Format(CultureInfo.InvariantCulture, "Could not find an implementation for ICategorisedBuilder<{1}, {0}>", sourceType, targetType));
        }

        /// <summary>
        /// Registers the specified builder.
        /// </summary>
        /// <typeparam name="TTarget">
        /// The type of the target.
        /// </typeparam>
        /// <typeparam name="TSource">
        /// The type of the source.
        /// </typeparam>
        /// <param name="builder">
        /// The builder.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// The <paramref name="builder"/> is null.
        /// </exception>
        public void Register<TTarget, TSource>(Func<IBuilderFactory, object> builder)
        {
            if (builder == null)
            {
                throw new ArgumentNullException("builder");
            }

            var key = Tuple.Create(typeof(TTarget), typeof(TSource));
            this._map[key] = builder;
        }

        /// <summary>
        /// Registers the specified builder.
        /// </summary>
        /// <typeparam name="TTarget">
        /// The type of the target.
        /// </typeparam>
        /// <typeparam name="TSource">
        /// The type of the source.
        /// </typeparam>
        /// <param name="builder">
        /// The builder.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// The <paramref name="builder"/> is null.
        /// </exception>
        public void RegisterCategorised<TTarget, TSource>(Func<IBuilderFactory, object> builder)
        {
            if (builder == null)
            {
                throw new ArgumentNullException("builder");
            }

            var key = Tuple.Create(typeof(TTarget), typeof(TSource));
            this._categorisedMap[key] = builder;
        }

        /// <summary>
        /// Registers the categorised.
        /// </summary>
        private void RegisterCategorised()
        {
            this._categorisedMap.Add(Tuple.Create(typeof(DataflowType), typeof(IDataflowObject)), builderFactory => new DataflowXmlBuilder());
            this._categorisedMap.Add(Tuple.Create(typeof(MetadataflowType), typeof(IMetadataFlow)), builderFactory => new MetadataflowXmlBuilder());
            this._categorisedMap.Add(Tuple.Create(typeof(CategorySchemeType), typeof(ICategorySchemeObject)), builderFactory => new CategorySchemeXmlBuilder());
        }

        /// <summary>
        /// Registers the codelists.
        /// </summary>
        private void RegisterCodelists()
        {
            this._map.Add(Tuple.Create(typeof(CodeType), typeof(ICode)), builderFactory => new CodeXmlBuilder());
            this._map.Add(Tuple.Create(typeof(CodeListType), typeof(ICodelistObject)), builderFactory => new CodelistXmlBuilder(builderFactory));
            this._map.Add(Tuple.Create(typeof(HierarchicalCodelistType), typeof(IHierarchicalCodelistObject)), builderFactory => new HierarchicalCodelistXmlBuilder());
        }

        /// <summary>
        /// Registers the concepts.
        /// </summary>
        private void RegisterConcepts()
        {
            this._map.Add(Tuple.Create(typeof(ConceptType), typeof(IConceptObject)), builderFactory => new ConceptXmlBuilder());
            this._map.Add(Tuple.Create(typeof(ConceptSchemeType), typeof(IConceptSchemeObject)), builderFactory => new ConceptSchemeXmlBuilder(builderFactory));
        }

        /// <summary>
        /// Registers the DSD.
        /// </summary>
        private void RegisterDsd()
        {
            this._map.Add(Tuple.Create(typeof(AttributeType), typeof(IAttributeObject)), builderFactory => new AttributeXmlBuilder(builderFactory));
            this._map.Add(Tuple.Create(typeof(CrossSectionalMeasureType), typeof(ICrossSectionalMeasure)), builderFactory => new CrossSectionalMeasureXmlBuilder());
            this._map.Add(Tuple.Create(typeof(DimensionType), typeof(IDimension)), builderFactory => new DimensionXmlBuilder(builderFactory));
            this._map.Add(Tuple.Create(typeof(GroupType), typeof(IGroup)), builderFactory => new GroupXmlsBuilder());
            this._map.Add(Tuple.Create(typeof(PrimaryMeasureType), typeof(IPrimaryMeasure)), builderFactory => new PrimaryMeasureXmlBuilder());
            this._map.Add(Tuple.Create(typeof(TimeDimensionType), typeof(IDimension)), builderFactory => new TimeDimensionXmlBuilder());
            this._map.Add(Tuple.Create(typeof(KeyFamilyType), typeof(IDataStructureObject)), builderFactory => new DataStructureXmlBuilder(builderFactory));
            this._map.Add(Tuple.Create(typeof(ComponentRole), typeof(IStructureReference)), builderFactory => new Sdmxv2ConceptRoleBuilder());
        }

        /// <summary>
        /// Registers the header.
        /// </summary>
        private void RegisterHeader()
        {
            this._map.Add(Tuple.Create(typeof(HeaderType), typeof(IHeader)), builderFactory => new StructureHeaderXmlBuilder());
        }

        /// <summary>
        /// Registers the MSD.
        /// </summary>
        private void RegisterMsd()
        {
            this._map.Add(Tuple.Create(typeof(MetadataAttributeType), typeof(IMetadataAttributeObject)), builderFactory => new MetadataAttributeXmlBuilder());
            this._map.Add(Tuple.Create(typeof(ReportStructureType), typeof(IReportStructure)), builderFactory => new ReportStructureXmlBuilder(builderFactory));
            this._map.Add(Tuple.Create(typeof(MetadataStructureDefinitionType), typeof(IMetadataStructureDefinitionObject)), builderFactory => new MetadataStructureDefinitionXmlsBuilder(builderFactory));
        }

        /// <summary>
        /// Registers the organisation.
        /// </summary>
        private void RegisterOrganisation()
        {
            this._map.Add(Tuple.Create(typeof(OrganisationType), typeof(IOrganisation)), builderFactory => new OrganisationRoleXmlBuilder());
            this._map.Add(Tuple.Create(typeof(OrganisationSchemeType), typeof(IAgencyScheme)), builderFactory => new OrganisationSchemeXmlBuilder(builderFactory));
            this._map.Add(Tuple.Create(typeof(OrganisationSchemeType), typeof(IDataConsumerScheme)), builderFactory => new OrganisationSchemeXmlBuilder(builderFactory));
            this._map.Add(Tuple.Create(typeof(OrganisationSchemeType), typeof(IDataProviderScheme)), builderFactory => new OrganisationSchemeXmlBuilder(builderFactory));
            this._map.Add(Tuple.Create(typeof(OrganisationSchemeType), typeof(IOrganisationUnitSchemeObject)), builderFactory => new OrganisationSchemeXmlBuilder(builderFactory));
        }

        /// <summary>
        /// Registers the type of the process.
        /// </summary>
        private void RegisterProcessType()
        {
            this._map.Add(Tuple.Create(typeof(ProcessType), typeof(IProcessObject)), builderFactory => new ProcessXmlBuilder());
        }

        /// <summary>
        /// Registers the reporting taxonomy.
        /// </summary>
        private void RegisterReportingTaxonomy()
        {
            this._map.Add(Tuple.Create(typeof(ReportingTaxonomyType), typeof(IReportingTaxonomyObject)), builderFactory => new ReportingTaxonomyXmlBuilder());
        }

        /// <summary>
        /// Registers the structure set.
        /// </summary>
        private void RegisterStructureSet()
        {
            this._map.Add(Tuple.Create(typeof(StructureSetType), typeof(IStructureSetObject)), builderFactory => new StructureSetXmlBuilder());
        }
    }
}