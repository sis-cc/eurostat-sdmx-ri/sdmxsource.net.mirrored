﻿// -----------------------------------------------------------------------
// <copyright file="IBuilderFactory.cs" company="EUROSTAT">
//   Date Created : 2016-06-03
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Factory
{
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder;

    /// <summary>
    /// An interface for getting instances of builders
    /// </summary>
    public interface IBuilderFactory
    {
        /// <summary>
        /// Gets an instance of a builder which builds a <typeparamref name="TTarget"/> object from a <typeparamref name="TSource"/> object
        /// </summary>
        /// <typeparam name="TTarget">The target type</typeparam>
        /// <typeparam name="TSource">The source type</typeparam>
        /// <returns>A builder which builds a <typeparamref name="TTarget"/> object from a <typeparamref name="TSource"/> object</returns>
        IBuilder<TTarget, TSource> GetBuilder<TTarget, TSource>();

        /// <summary>
        /// Gets an instance of a categorized builder which builds a <typeparamref name="TTarget" /> object from a <typeparamref name="TSource" /> object
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <returns>A builder which builds a <typeparamref name="TTarget"/> object from a <typeparamref name="TSource"/> object and a set of categorizations.</returns>
        ICategorisedBuilder<TTarget, TSource> GetCategorisedBuilder<TTarget, TSource>();
    }
}