namespace Org.Sdmxsource.Translator
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    /// BestMatchLanguageTranslator class. When translating localised items, chooses the language(s) matching the list of preferred languages.
    /// </summary>
    public class BestMatchLanguageTranslator : ITranslator
    {
        /// <summary>
        /// Gets the list of preferred languages proveded to the translator
        /// </summary>
        public IReadOnlyList<CultureInfo> PreferredLanguages  { get; private set; }

        /// <summary>
        /// Gets the list of languages actually used by the translator when matching localisations
        /// </summary>
        public IReadOnlyList<string> UsedLanguages
        {
            get
            {
                if (_usedLanguages == null || _usedLanguages.Count == 0)
                {
                    return new List<string>(_calculatedUsedLanguages).AsReadOnly();
                }
                else
                {
                    return new List<string>(_usedLanguages).AsReadOnly();
                }
            }
        }

        private readonly ISet<string> _usedLanguages;

        private readonly ISet<string> _calculatedUsedLanguages;

        /// <summary>
        /// Initializes a new instance of the <see cref="PreferedLanguageTranslator"/> class.
        /// </summary>
        /// <param name="preferredLanguages">
        /// The preferred languages.
        /// </param>
        /// <param name="availableLanguages"></param>
        /// <param name="defaultLanguage">
        /// The default language.
        /// </param>
        public BestMatchLanguageTranslator(IList<CultureInfo> preferredLanguages, IList<CultureInfo> availableLanguages, CultureInfo defaultLanguage)
        {
            DefaultLanguage = defaultLanguage ?? throw new ArgumentNullException(nameof(defaultLanguage));

            if (preferredLanguages == null || !preferredLanguages.Any())
            {
                PreferredLanguages = new List<CultureInfo>() { defaultLanguage }.AsReadOnly();
            }
            else
            {
                PreferredLanguages = new List<CultureInfo>(preferredLanguages).AsReadOnly();
            }

            _calculatedUsedLanguages = CalculateUsedLanguagesFromAvailableLanguages(availableLanguages);

            _usedLanguages = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="PreferedLanguageTranslator"/> class.
        /// </summary>
        /// <param name="preferredLanguages">
        /// The preferred languages.
        /// </param>
        /// <param name="sdmxObjects"></param>
        /// <param name="defaultLanguage">
        /// The default language.
        /// </param>
        public BestMatchLanguageTranslator(IList<CultureInfo> preferredLanguages, ISdmxObjects sdmxObjects, CultureInfo defaultLanguage)
        {
            DefaultLanguage = defaultLanguage ?? throw new ArgumentNullException(nameof(defaultLanguage));

            if (preferredLanguages == null || !preferredLanguages.Any())
            {
                PreferredLanguages = new List<CultureInfo>() {defaultLanguage}.AsReadOnly();
            }
            else
            {
                PreferredLanguages = new List<CultureInfo>(preferredLanguages).AsReadOnly();
            }

            _calculatedUsedLanguages = CalculateUsedLanguagesFromSdmxObjects(sdmxObjects);

            _usedLanguages = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// Gets the default language.
        /// </summary>
        public CultureInfo DefaultLanguage { get; }

        /// <summary>
        /// Not implemented. At this translator selected language may vary elements by elements depending on the preferred languages and the available localisations.
        /// </summary>
        //TODO: public CultureInfo SelectedLanguage => throw new NotImplementedException();
        public CultureInfo SelectedLanguage => new CultureInfo(SelectedLanguages.FirstOrDefault() ?? DefaultLanguage?.Name ?? "en");

        /// <summary>
        /// Gets the selected/used languages
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> SelectedLanguages {
            get
            {
                if (UsedLanguages.Count > 0)
                {
                    return UsedLanguages;
                }

                if (_calculatedUsedLanguages.Any())
                {
                    return _calculatedUsedLanguages;
                }

                return PreferredLanguages.Select(l => l.Name);
            }
        }

        /// <summary>
        /// Get the text Type
        /// </summary>
        /// <param name="textTypeWrapperList"></param>
        /// <returns></returns>
        public ITextTypeWrapper GetSelectedText(IList<ITextTypeWrapper> textTypeWrapperList)
        {
            if (textTypeWrapperList == null || textTypeWrapperList.Count == 0)
            {
                return null;
            }

            ITextTypeWrapper selectedText = null;

            foreach (var languagePreference in PreferredLanguages)
            {
                foreach (var localizedText in textTypeWrapperList)
                {
                    var cultureMatch = LanguageTagComparer.CompareLanguageAndRegionTag(languagePreference, localizedText.Locale);

                    switch (cultureMatch)
                    {
                        case LanguageTagMatch.LanguageAndRegionMatch:

                            UpdateUsedLanguages(localizedText.Locale);

                            return localizedText;

                        case LanguageTagMatch.LanguageMatchOnly:
                            if (selectedText == null)
                            {
                                selectedText = localizedText;
                            }
                            break;
                    }
                }

                if (selectedText == null)
                {
                    continue;
                }

                UpdateUsedLanguages(selectedText.Locale);

                return selectedText;
            }

            return null;
        }

        private void UpdateUsedLanguages(IEnumerable<string> locales)
        {
            foreach (var locale in locales)
            {
                UpdateUsedLanguages(locale);
            }
        }

        private void UpdateUsedLanguages(string locale)
        {
            _usedLanguages?.Add(locale);
        }

        /// <summary>
        /// Get the Text in the seleted lang
        /// </summary>
        /// <param name="translationDictionary"></param>
        /// <returns></returns>
        public KeyValuePair<string, string> GetSelectedText(IDictionary<CultureInfo, string> translationDictionary)
        {
            if (translationDictionary == null || translationDictionary.Count == 0)
            {
                return default(KeyValuePair<string, string>);
            }

            var selectedText = default(KeyValuePair<CultureInfo, string>);

            foreach (var languagePreference in PreferredLanguages)
            {
                foreach (var localizedTextKvp in translationDictionary)
                {
                    var cultureMatch = LanguageTagComparer.CompareLanguageAndRegionTag(languagePreference, localizedTextKvp.Key);

                    switch (cultureMatch)
                    {
                        case LanguageTagMatch.LanguageAndRegionMatch:
                            UpdateUsedLanguages(localizedTextKvp.Key.Name);

                            return new KeyValuePair<string, string>(localizedTextKvp.Key.Name, localizedTextKvp.Value);

                        case LanguageTagMatch.LanguageMatchOnly:
                            if (selectedText.Equals(default(KeyValuePair<CultureInfo, string>)))
                            {
                                selectedText = localizedTextKvp;
                            }
                            break;
                    }
                }

                if (selectedText.Equals(default(KeyValuePair<CultureInfo, string>)))
                {
                    continue;
                }

                UpdateUsedLanguages(selectedText.Key.Name);

                return new KeyValuePair<string, string>(selectedText.Key.Name, selectedText.Value);
            }

            return default(KeyValuePair<string, string>);
        }

        /// <summary>
        /// Get the Text in the seleted lang
        /// </summary>
        /// <param name="textTypeWrapperList"></param>
        /// <param name="provideAtLeastOneItem"></param>
        /// <returns></returns>
        public IList<ITextTypeWrapper> GetSelectedTextInPreferredLanguages(IList<ITextTypeWrapper> textTypeWrapperList, bool provideAtLeastOneItem = false)
        {
            if (textTypeWrapperList == null || textTypeWrapperList.Count == 0)
            {
                return new List<ITextTypeWrapper>();
            }

            var selectedTexts = new HashSet<ITextTypeWrapper>();

            foreach (var languagePreference in PreferredLanguages)
            {
                foreach (var localizedText in textTypeWrapperList)
                {
                    var cultureMatch = LanguageTagComparer.CompareLanguageAndRegionTag(languagePreference, localizedText.Locale);

                    if (!cultureMatch.Equals(LanguageTagMatch.None))
                    {
                        selectedTexts.Add(localizedText);
                    }
                }
            }

            // If there are no matches found for the preferred languages and at least one item is requested (e.g. for names where name is mandatory for nameable objects) then
            // - first try to find a translation matching with the default language
            // - if still no match found then take the first item with any localization
            if (provideAtLeastOneItem && !selectedTexts.Any())
            {
                var defaultText =
                    textTypeWrapperList.FirstOrDefault(t => LanguageTagComparer.CompareLanguageAndRegionTag(DefaultLanguage, t.Locale).Equals(LanguageTagMatch.LanguageAndRegionMatch)) ??
                    textTypeWrapperList.FirstOrDefault(t => LanguageTagComparer.CompareLanguageAndRegionTag(DefaultLanguage, t.Locale).Equals(LanguageTagMatch.LanguageMatchOnly)) ??
                    textTypeWrapperList.First();

                selectedTexts.Add(defaultText);
            }

            UpdateUsedLanguages(selectedTexts.Select(t => t.Locale));

            return selectedTexts.ToList();
        }

        /// <summary>
        /// Get the Text in the seleted lang
        /// </summary>
        /// <param name="translationDictionary"></param>
        /// <param name="provideAtLeastOneItem"></param>
        /// <returns></returns>
        public IList<KeyValuePair<string, string>> GetSelectedTextInPreferredLanguages(IDictionary<CultureInfo, string> translationDictionary, bool provideAtLeastOneItem = false)
        {
            if (translationDictionary == null || translationDictionary.Count == 0)
            {
                return new List<KeyValuePair<string, string>>();
            }

            var selectedTexts = new HashSet<KeyValuePair<string, string>>();

            foreach (var languagePreference in PreferredLanguages)
            {
                foreach (var localisedTextKvp in translationDictionary)
                {
                    var cultureMatch = LanguageTagComparer.CompareLanguageAndRegionTag(languagePreference, localisedTextKvp.Key);

                    if (!cultureMatch.Equals(LanguageTagMatch.None))
                    {
                        selectedTexts.Add(new KeyValuePair<string, string>(localisedTextKvp.Key.Name, localisedTextKvp.Value));
                    }
                }
            }

            // If there are no matches found for the preferred languages and at least one item is requested (e.g. for names where name is mandatory for nameable objects) then
            // - first try to find a translation matching with the default language
            // - if still no match found then take the first item with any localisation
            if (provideAtLeastOneItem && !selectedTexts.Any())
            {
                var defaultText = translationDictionary.FirstOrDefault(t => LanguageTagComparer.CompareLanguageAndRegionTag(DefaultLanguage, t.Key).Equals(LanguageTagMatch.LanguageAndRegionMatch));

                if (defaultText.Equals(default(KeyValuePair<CultureInfo, string>)))
                {
                    defaultText = translationDictionary.FirstOrDefault(t => LanguageTagComparer.CompareLanguageAndRegionTag(DefaultLanguage, t.Key).Equals(LanguageTagMatch.LanguageMatchOnly));
                }

                if (defaultText.Equals(default(KeyValuePair<CultureInfo, string>)))
                {
                    defaultText = translationDictionary.First();
                }

                selectedTexts.Add(new KeyValuePair<string, string>(defaultText.Key.Name, defaultText.Value));
            }

            UpdateUsedLanguages(selectedTexts.Select(t => t.Key));

            return selectedTexts.ToList();
        }

        /// <summary>
        /// Gets the translation using the selected or default language.
        /// </summary>
        /// <param name="textTypeWrapperList">
        /// The textTypeWrapper list.
        /// </param>
        /// <returns>
        /// The translation.
        /// </returns>
        public string GetTranslation(IList<ITextTypeWrapper> textTypeWrapperList)
        {
            var selectedText = GetSelectedText(textTypeWrapperList);

            return selectedText == null ? string.Empty : selectedText.Value;
        }

        /// <summary>
        /// Gets the translation using the selected or default language.
        /// </summary>
        /// <param name="translationDictionary">
        /// The translation dictionary.
        /// </param>
        /// <returns>
        /// The translation.
        /// </returns>
        public string GetTranslation(IDictionary<CultureInfo, string> translationDictionary)
        {
            var selectedTextKvp = GetSelectedText(translationDictionary);

            if (selectedTextKvp.Equals(default(KeyValuePair<string,string>)))
            {
                return string.Empty;
            }

            return selectedTextKvp.Value;
        }

        private ISet<string> CalculateUsedLanguagesFromAvailableLanguages(IList<CultureInfo> availableLanguages)
        {
            var usedLanguages = CalculateUsedLanguagesFromAvailableLanguageTags(availableLanguages.Select(al => al.Name));

            usedLanguages.Add(DefaultLanguage.Name);

            return usedLanguages;
        }

        private ISet<string> CalculateUsedLanguagesFromSdmxObjects(ISdmxObjects sdmxObjects)
        {
            var usedLanguages = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);

            if (sdmxObjects != null)
            {
                var allMaintainables = sdmxObjects.GetAllMaintainables();

                foreach (var maintainable in allMaintainables)
                {
                    usedLanguages.UnionWith(CalculateUsedLanguagesFromNameable(maintainable));
                }

                foreach (var codelist in sdmxObjects.Codelists)
                {
                    foreach (var code in codelist.Items)
                    {
                        usedLanguages.UnionWith(CalculateUsedLanguagesFromNameable(code));
                    }
                }

                foreach (var conceptScheme in sdmxObjects.ConceptSchemes)
                {
                    foreach (var concept in conceptScheme.Items)
                    {
                        usedLanguages.UnionWith(CalculateUsedLanguagesFromNameable(concept));
                    }
                }

                foreach (var agencyScheme in sdmxObjects.AgenciesSchemes)
                {
                    foreach (var agency in agencyScheme.Items)
                    {
                        usedLanguages.UnionWith(CalculateUsedLanguagesFromNameable(agency));
                    }
                }
            }

            usedLanguages.Add(DefaultLanguage.Name);

            return usedLanguages;
        }

        private ISet<string> CalculateUsedLanguagesFromNameable(INameableObject nameableObject)
        {
            HashSet<string> availableLanguages = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);

            var preferredTranslationsOfNames = GetSelectedTextInPreferredLanguages(nameableObject.Names);
            availableLanguages.UnionWith(preferredTranslationsOfNames.Select(ptn => ptn.Locale));

            var preferredTranslationsOfDescriptions = GetSelectedTextInPreferredLanguages(nameableObject.Descriptions);
            availableLanguages.UnionWith(preferredTranslationsOfDescriptions.Select(ptn => ptn.Locale));

            foreach (var annotation in nameableObject.Annotations)
            {
                var preferredTranslationsOfAnnotations = GetSelectedTextInPreferredLanguages(annotation.Text);
                availableLanguages.UnionWith(preferredTranslationsOfAnnotations.Select(ptn => ptn.Locale));
            }

            return availableLanguages;
        }

        private ISet<string> CalculateUsedLanguagesFromAvailableLanguageTags(IEnumerable<string> availableLanguages)
        {
            var usedLanguages = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);

            var availableLanguagesArray = availableLanguages as string[] ?? availableLanguages.ToArray();

            foreach (var preferredLanguage in PreferredLanguages)
            {
                var preferredAvailableLanguages = availableLanguagesArray.Where(al => LanguageTagComparer.CompareLanguageAndRegionTag(preferredLanguage, al) != LanguageTagMatch.None);

                usedLanguages.UnionWith(preferredAvailableLanguages);
            }

            return usedLanguages;
        }

    }
}