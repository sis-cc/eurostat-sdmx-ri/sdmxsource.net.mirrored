using System;
using System.Globalization;

namespace Org.Sdmxsource.Translator
{
    public class LanguageTagComparer
    {
        public static LanguageTagMatch CompareLanguageAndRegionTag(CultureInfo cultureInfo1, CultureInfo cultureInfo2)
        {
            if (cultureInfo1 is null)
            {
                throw new ArgumentException("cultureInfo1");
            }

            if (cultureInfo2 is null)
            {
                throw new ArgumentException("cultureInfo2");
            }

            return CompareLanguageAndRegionTag(cultureInfo1.Name, cultureInfo2.Name);
        }

        public static LanguageTagMatch CompareLanguageAndRegionTag(CultureInfo cultureInfo, string languageTag)
        {
            if (cultureInfo is null)
            {
                throw new ArgumentException("cultureInfo");
            }

            if (string.IsNullOrWhiteSpace(languageTag))
            {
                throw new ArgumentException("languageTag");
            }

            return CompareLanguageAndRegionTag(cultureInfo.Name, languageTag);
        }

        public static LanguageTagMatch CompareLanguageAndRegionTag(string languageTag1, string languageTag2)
        {
            if (string.IsNullOrWhiteSpace(languageTag1))
            {
                throw new ArgumentException("languageTag1");
            }

            if (string.IsNullOrWhiteSpace(languageTag2))
            {
                throw new ArgumentException("languageTag2");
            }

            if (languageTag1.Equals("*") || languageTag2.Equals("*"))
            {
                return LanguageTagMatch.LanguageAndRegionMatch;
            }

            var language1SubTags = languageTag1.Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries);

            if (language1SubTags.Length > 2 || language1SubTags.Length == 0)
            {
                throw new ArgumentException($"Invalid languageTag '{languageTag1}', only 'Language[-Region]' format is supported.");
            }

            var language2SubTags = languageTag2.Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries);

            if (language2SubTags.Length > 2 || language1SubTags.Length == 0)
            {
                throw new ArgumentException($"Invalid languageTag of cultureInfo '{languageTag2}', only 'Language[-Region]' format is supported.");
            }

            var language1LanguageSubTag = language1SubTags[0];
            var language2LanguageSubTag = language2SubTags[0];

            if (language1LanguageSubTag.Equals(language2LanguageSubTag, StringComparison.InvariantCultureIgnoreCase))
            {
                if (language1SubTags.Length == 1 && language2SubTags.Length == 1)
                {
                    return LanguageTagMatch.LanguageAndRegionMatch;
                }

                if (language1SubTags.Length == 2 && language2SubTags.Length == 2)
                {
                    var language1RegionSubTag = language1SubTags[1];
                    var language2RegionSubTag = language2SubTags[1];

                    if (language1RegionSubTag.Equals(language2RegionSubTag, StringComparison.InvariantCultureIgnoreCase))
                    {
                        return LanguageTagMatch.LanguageAndRegionMatch;
                    }
                }

                return LanguageTagMatch.LanguageMatchOnly;
            }

            return LanguageTagMatch.None;
        }
    }
}
