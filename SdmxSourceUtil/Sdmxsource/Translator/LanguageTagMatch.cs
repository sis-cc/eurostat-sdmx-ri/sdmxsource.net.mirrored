namespace Org.Sdmxsource.Translator
{    
    /// <summary>
     ///     Contains a list of possible language tag comparision results
     /// </summary>
    public enum LanguageTagMatch
    {
        /// <summary>
        ///     Language tags do not match
        /// </summary>
        None = 0,

        /// <summary>
        ///     Only language subtags match
        /// </summary>
        LanguageMatchOnly = 1,

        /// <summary>
        ///     Both language and region subtags match
        /// </summary>
        LanguageAndRegionMatch = 2
    }
}
