// -----------------------------------------------------------------------
// <copyright file="PreferedLanguageTranslator.cs" company="EUROSTAT">
//   Date Created : 2016-07-01
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Translator
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    /// PreferedLanguageTranslator class.
    /// </summary>
    public class PreferedLanguageTranslator : ITranslator
    {
        /// <summary>
        /// The default language
        /// </summary>
        private readonly CultureInfo _defaultLanguage;

        /// <summary>
        /// Initializes a new instance of the <see cref="PreferedLanguageTranslator"/> class.
        /// </summary>
        /// <param name="preferredLanguages">
        /// The preferred languages.
        /// </param>
        /// <param name="availableLanguages">
        /// The available languages.
        /// </param>
        /// <param name="defaultLanguage">
        /// The default language.
        /// </param>
        public PreferedLanguageTranslator(IList<CultureInfo> preferredLanguages, IList<CultureInfo> availableLanguages, CultureInfo defaultLanguage)
        {
            if (defaultLanguage == null)
            {
                throw new ArgumentNullException("defaultLanguage");
            }

            this._defaultLanguage = defaultLanguage;

            this.PreferredLanguages = preferredLanguages != null ? new List<CultureInfo>(preferredLanguages).AsReadOnly() : null;

            this.SelectedLanguage = this.GetSelectedLanguage(preferredLanguages, availableLanguages) ?? defaultLanguage;
        }

        /// <summary>
        /// Gets the selected language.
        /// </summary>
        public CultureInfo SelectedLanguage { get; private set; }

        /// <summary>
        /// Gets the list of preferred languages proveded to the translator
        /// </summary>
        public IReadOnlyList<CultureInfo> PreferredLanguages { get; private set; }

        public IReadOnlyList<string> UsedLanguages
        {
            get
            {
                return new List<string>() { SelectedLanguage.Name, DefaultLanguage.Name }.AsReadOnly();
            }
        }

        /// <summary>
        /// Gets the default language.
        /// </summary>
        public CultureInfo DefaultLanguage
        {
            get
            {
                return _defaultLanguage;
            }
        }

        /// <summary>
        /// Gets the list of used languages.
        /// </summary>
        public IEnumerable<string> SelectedLanguages {
            get 
            {
                var selectedLanguages = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);

                selectedLanguages.Add(SelectedLanguage.Name);

                selectedLanguages.Add(DefaultLanguage.Name);

                return selectedLanguages;
            }
        }

        /// <summary>
        /// Get the text Type
        /// </summary>
        /// <param name="textTypeWrapperList"></param>
        /// <returns></returns>
        public ITextTypeWrapper GetSelectedText(IList<ITextTypeWrapper> textTypeWrapperList)
        {
            if (textTypeWrapperList == null)
            {
                return null;
            }

            return textTypeWrapperList.FirstOrDefault(x => x.Locale.Equals(this.SelectedLanguage.TwoLetterISOLanguageName, StringComparison.OrdinalIgnoreCase))
                ?? textTypeWrapperList.FirstOrDefault(x => x.Locale.Equals(this._defaultLanguage.TwoLetterISOLanguageName, StringComparison.OrdinalIgnoreCase));
        }

        /// <summary>
        /// Get the Text in the seleted lang
        /// </summary>
        /// <param name="translationDictionary"></param>
        /// <returns></returns>
        public KeyValuePair<string, string> GetSelectedText(IDictionary<CultureInfo, string> translationDictionary)
        {
            if (translationDictionary == null)
            {
                return default(KeyValuePair<string, string>);
            }

            var selectedText = translationDictionary.FirstOrDefault(x => x.Key.Equals(this.SelectedLanguage));

            //If label for selected language is missing, select default language
            if (selectedText.Equals(default(KeyValuePair<CultureInfo, string>)))
            {
                selectedText = translationDictionary.FirstOrDefault(x => x.Key.Equals(this._defaultLanguage));

                //If label for default language is missing, select default language
                if (selectedText.Equals(default(KeyValuePair<CultureInfo, string>)))
                {
                    return default(KeyValuePair<string, string>);
                }
            }

            return new KeyValuePair<string, string>(selectedText.Key.TwoLetterISOLanguageName, selectedText.Value);
        }

        public IList<KeyValuePair<string, string>> GetSelectedTextInPreferredLanguages(IDictionary<CultureInfo, string> translationDictionary, bool provideAtLeastOneItem = false)
        {
            var selectedText = GetSelectedText(translationDictionary);

            if (selectedText.Equals(default(KeyValuePair<string, string>)))
            {
                if (provideAtLeastOneItem && translationDictionary.Any())
                {
                    var translationItem = translationDictionary.First();

                    selectedText = new KeyValuePair<string, string>(translationItem.Key.Name, translationItem.Value);
                }
                else
                {
                    return new List<KeyValuePair<string, string>>();
                }
            }

            return new[] { selectedText };
        }

        public IList<ITextTypeWrapper> GetSelectedTextInPreferredLanguages(IList<ITextTypeWrapper> textTypeWrapperList, bool provideAtLeastOneItem = false)
        {
            var selectedText = GetSelectedText(textTypeWrapperList);

            if (selectedText == null)
            {
                if (provideAtLeastOneItem && textTypeWrapperList.Any())
                {
                    selectedText = textTypeWrapperList.First();
                }
                else
                {
                    return new List<ITextTypeWrapper>();
                }
            }

            return new[] { selectedText };
        }

        /// <summary>
        /// Gets the translation using the selected or default language.
        /// </summary>
        /// <param name="textTypeWrapperList">
        /// The textTypeWrapper list.
        /// </param>
        /// <returns>
        /// The translation.
        /// </returns>
        public string GetTranslation(IList<ITextTypeWrapper> textTypeWrapperList)
        {
            return textTypeWrapperList == null ? string.Empty : this.GetTranslation(textTypeWrapperList.ToDictionary(x => CultureInfo.CreateSpecificCulture(x.Locale), y => y.Value));
        }

        /// <summary>
        /// Gets the translation using the selected or default language.
        /// </summary>
        /// <param name="translationDictionary">
        /// The translation dictionary.
        /// </param>
        /// <returns>
        /// The translation.
        /// </returns>
        public string GetTranslation(IDictionary<CultureInfo, string> translationDictionary)
        {
            if (translationDictionary == null || !translationDictionary.Any())
            {
                return string.Empty;
            }

            var cultureInfo = translationDictionary.Keys.FirstOrDefault(x => x.TwoLetterISOLanguageName.Equals(this.SelectedLanguage.TwoLetterISOLanguageName, StringComparison.OrdinalIgnoreCase))
                              ?? translationDictionary.Keys.FirstOrDefault(x => x.TwoLetterISOLanguageName.Equals(this._defaultLanguage.TwoLetterISOLanguageName, StringComparison.OrdinalIgnoreCase));

            return cultureInfo != null ? translationDictionary[cultureInfo] : string.Empty;
        }

        /// <summary>
        /// Gets the selected language.
        /// </summary>
        /// <param name="preferredLanguages">
        /// The prefered languages.
        /// </param>
        /// <param name="availableLanguages">
        /// The available languages.
        /// </param>
        /// <returns>
        /// The selected language as a <see cref="CultureInfo"/>
        /// </returns>
        private CultureInfo GetSelectedLanguage(IList<CultureInfo> preferredLanguages, IList<CultureInfo> availableLanguages)
        {
            if (preferredLanguages == null || availableLanguages == null)
            {
                return null;
            }

            foreach (var language in preferredLanguages)
            {
                var selectedLanguage = availableLanguages.FirstOrDefault(x => x.TwoLetterISOLanguageName.Equals(language.TwoLetterISOLanguageName, StringComparison.OrdinalIgnoreCase));

                if (selectedLanguage != null)
                {
                    return selectedLanguage;
                }
            }

            return null;
        }
    }
}