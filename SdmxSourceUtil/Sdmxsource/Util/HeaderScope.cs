// -----------------------------------------------------------------------
// <copyright file="RangeScope.cs" company="EUROSTAT">
//   Date Created : 2018-03-29
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Mime;
using System.Text;
using System.Threading;
using Org.Sdmxsource.Sdmx.Api.Constants;

namespace Org.Sdmxsource.Util
{
    /// <inheritdoc />
    /// <summary>
    /// RangeScope
    /// </summary>
    /// <seealso cref="T:System.IDisposable" />
    public class HeaderScope : IDisposable
    {
        private static AsyncLocal<int> _numberOfStructureRecords = new AsyncLocal<int>();

        private static AsyncLocal<Tuple<long?, long?>> _range = new AsyncLocal<Tuple<long?, long?>>();

        private static AsyncLocal<bool> _isPit = new AsyncLocal<bool>();

        private static AsyncLocal<DateTime> _dataLastUpdate = new AsyncLocal<DateTime>();

        private static AsyncLocal<bool> _writeUrn = new AsyncLocal<bool>();

        private static AsyncLocal<bool> _returnDisseminationDbSql = new AsyncLocal<bool>();

        private static AsyncLocal<List<string>> _disseminationDbSqls = new AsyncLocal<List<string>>();

        private static AsyncLocal<CancellationToken> _requestAborted = new AsyncLocal<CancellationToken>();

        private static AsyncLocal<MetadataLevel> _metadataLevel = new AsyncLocal<MetadataLevel>() { Value = MetadataLevel.GetFromEnum(MetadataLevelEnumType.All) };

        /// <summary>
        /// Initializes a new instance of the <see cref="HeaderScope" /> class.
        /// </summary>
        /// <param name="headerCollection">The header collection.</param>
        /// <param name="writeUrn">if set to <c>true</c> [write urn].</param>S
        /// <param name="requestAborted">Cancellation token</param>
        public HeaderScope(WebHeaderCollection headerCollection, bool writeUrn, CancellationToken requestAborted)
        {
            var header = headerCollection[HttpRequestHeader.Range] ?? headerCollection["X-Range"];
            if (header != null)
            {
                _range.Value = ParseHeaderString(header);
            }

            var releaseHeader = headerCollection["X-Release"];
            _isPit.Value = !string.IsNullOrEmpty(releaseHeader) && releaseHeader.Equals("PIT", StringComparison.OrdinalIgnoreCase);

            var levelHeader = headerCollection["X-Level"];
            if (!string.IsNullOrEmpty(levelHeader))
            {
                _metadataLevel.Value = MetadataLevel.ParseString(levelHeader);
            }

            _writeUrn.Value = writeUrn;
            _requestAborted.Value = requestAborted;
        }

        /// <summary>
        /// Notifies when the connection
        /// </summary>
        public static CancellationToken RequestAborted => _requestAborted.Value;

        /// <summary>
        /// Gets or sets a value indicating whether [write urn].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [write urn]; otherwise, <c>false</c>.
        /// </value>
        public static bool WriteUrn
        {
            get => _writeUrn.Value;
            set => _writeUrn.Value = value;
        }

        /// <summary>
        /// Gets the number of structure records.
        /// </summary>
        /// <value>
        /// The number of structure records.
        /// </value>
        public static int NumberOfStructureRecords
        {
            get => _numberOfStructureRecords.Value;
            set => _numberOfStructureRecords.Value = value;
        }

        /// <summary>
        /// Gets the range.
        /// </summary>
        /// <value>
        /// The range.
        /// </value>
        public static Tuple<long?, long?> Range => _range.Value;

        /// <summary>Gets or sets a value indicating whether it is a point in time query.</summary>
        /// <value>
        ///   <c>true</c> if [point in time]; otherwise, <c>false</c>.
        /// </value>
        public static bool IsPit => _isPit.Value;

        /// <summary>
        /// Gets the metadata level.
        /// </summary>
        public static MetadataLevel MetadataLevel => _metadataLevel.Value;

        /// <summary>
        /// Gets or sets the data last update.
        /// </summary>
        /// <value>
        /// The data last update.
        /// </value>
        public static DateTime DataLastUpdate
        {
            get { return _dataLastUpdate.Value; }
            set { _dataLastUpdate.Value = value; }
        }

        /// <summary>
        /// Gets the unit.
        /// </summary>
        /// <value>
        /// The unit.
        /// </value>
        public static string Unit => "values";
        /// <summary>
        /// The sqls generated for the dissemination database
        /// </summary>
        public static List<string> DisseminationDbSqls { get => _disseminationDbSqls.Value; set => _disseminationDbSqls.Value = value; }
        /// <summary>
        /// Should return the sql generated for the dissemination database
        /// </summary>
        public static bool ReturnDisseminationDbSql { get => _returnDisseminationDbSql.Value; set => _returnDisseminationDbSql.Value = value; }

        /// <summary>
        /// Parses the header string.
        /// </summary>
        /// <param name="header">The header.</param>
        /// <returns></returns>
        public static Tuple<long?, long?> ParseHeaderString(string header)
        {
            var index = header.Replace(" ", string.Empty).IndexOf(Unit + "=", StringComparison.Ordinal);
            if (index == -1)
            {
                // values= not found
                return null;
            }

            var ranges = header.Replace(Unit + "=", "").Split(',');
            if (ranges.Length != 1)
            {
                // we don't support multiple ranges
                return null;
            }

            var range = ranges[0].Split('-');
            if (range.Length > 2 || range.Length < 1)
            {
                // invalid range specification
                return null;
            }

            long lowerLimit;
            if (!long.TryParse(range[0], NumberStyles.Integer, CultureInfo.InvariantCulture, out lowerLimit))
            {
                // ignore range
                return null;
            }

            long upperLimit;
            if (range.Length < 2 || !long.TryParse(range[1], NumberStyles.Integer, CultureInfo.InvariantCulture, out upperLimit))
            {
                // upper limit is optional
                return new Tuple<long?, long?>(lowerLimit, null);
            }

            if (lowerLimit > upperLimit)
            {
                return null;
            }

            return new Tuple<long?, long?>(lowerLimit, upperLimit);
        }

        /// <inheritdoc />
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _range.Value = null;
            }
        }
    }
}