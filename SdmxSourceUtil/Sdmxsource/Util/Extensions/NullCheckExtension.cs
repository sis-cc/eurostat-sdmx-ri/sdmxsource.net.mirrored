// -----------------------------------------------------------------------
// <copyright file="NullCheckExtension.cs" company="EUROSTAT">
//   Date Created : 2014-03-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Util.Extensions
{
    using System;

    using Org.Sdmxsource.Util.Attributes;

    /// <summary>
    /// Extension methods for checking null
    /// </summary>
    public static class NullCheckExtension
    {
        /// <summary>
        /// Pass through an object if it not null.
        /// </summary>
        /// <typeparam name="T">The type of object</typeparam>
        /// <param name="parameterToCheck">The parameter to check.</param>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <returns>The <paramref name="parameterToCheck"/></returns>
        /// <exception cref="ArgumentNullException"><paramref name="parameterToCheck"/> is <see langword="null" />.</exception>
        public static T PassNoNull<T>([ValidatedNotNull]this T parameterToCheck, string parameterName) where T : class
        {
            if (parameterToCheck == null)
            {
                throw new ArgumentNullException(parameterName);
            }

            return parameterToCheck;
        }

        /// <summary>
        /// Invokes <paramref name="action"/> if <paramref name="value"/> is not null.
        /// Else does nothing.
        /// </summary>
        /// <typeparam name="T">The value type could be class, type, or struct.</typeparam>
        /// <param name="value">The value to check and/or use.</param>
        /// <param name="action">The action to invoke.</param>
        public static void IfNotNull<T>([ValidatedNotNull]this T value, Action<T> action)
        {
            if (value != null)
            {
                action.Invoke(value);
            }
        }
        
        /// <summary>
        /// Parses a string to nullable struct type (i.e. int, long etc)
        /// </summary>
        /// <typeparam name="T">The struct type</typeparam>
        /// <param name="value">the string to parse</param>
        /// <param name="valueConverter">the function to parse with.</param>
        /// <returns>The struct value or <c>null</c>.</returns>
        public static T? ParseToNullableStruct<T>(this string value, Func<string, T> valueConverter)
            where T : struct
        {
            if (value == null)
            {
                return null;
            }
            T? result;
            try
            {
                result = valueConverter(value);
            }
            catch (FormatException)
            {
                return null;
            }
            return result;
        }
    }
}