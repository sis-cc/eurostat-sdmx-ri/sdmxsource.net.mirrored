// -----------------------------------------------------------------------
// <copyright file="VersionableUtil.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Util
{
    #region Using directives

    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Text;
    using System.Text.RegularExpressions;
    using Org.Sdmxsource.Sdmx.Api.Exception;

    #endregion

    /// <summary>
    ///     Utility class providing helper methods to determine if a String is a valid version1.
    /// </summary>
    public static class VersionableUtil
    {
        //semantic version taken from https://sdmx.org/wp-content/uploads/SDMX_3-0-0_SECTION_6_FINAL-1_0.pdf "ANNEX Semantic Versioning" section. Also is the same at https://semver.org/
        //note that json schema regex is different however https://github.com/sdmx-twg/sdmx-json/blob/master/structure-message/tools/schemas/2.0.0/sdmx-json-structure-schema.json#L1837
        private static readonly Regex _semanticVersionRegex = new Regex("^(0|[1-9]\\d*)\\.(0|[1-9]\\d*)\\.(0|[1-9]\\d*)(?:-((?:0|[1-9]\\d*|\\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\\.(?:0|[1-9]\\d*|\\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\\+([0-9a-zA-Z-]+(?:\\.[0-9a-zA-Z-]+)*))?$");

        /// <summary>
        ///     The version regex matching.
        /// </summary>
        private static readonly Regex _versionRegex = new Regex("^([0-9])+(\\.([0-9])+)*$", RegexOptions.Compiled);

        /// <summary>
        ///     The regex matching for version valid only in 2.x schemas
        /// </summary>
        private static readonly Regex _valid2xOnly = new Regex("^[0-9]\\.[0-9]$", RegexOptions.Compiled);

        /// <summary>
        ///     The version regex matching for wildcards.
        /// </summary>
        private static readonly Regex _wildcardVersionRegex = new Regex("(^(0|[1-9]\\d*)\\+\\.(0|[1-9]\\d*)\\.(0|[1-9]\\d*)$)|(^(0|[1-9]\\d*)\\.(0|[1-9]\\d*)\\+\\.(0|[1-9]\\d*)$)|(^(0|[1-9]\\d*)\\.(0|[1-9]\\d*)\\.(0|[1-9]\\d*)\\+$)", RegexOptions.Compiled);


        private static readonly string _draftVersionFormat = "{0}-draft";

        /// <summary>
        ///     Formats the supplied version1 String.
        ///     TODO: Review whether this method is actually required any more.
        /// </summary>
        /// <param name="version">
        ///     The version1 string to format.
        /// </param>
        /// <returns>
        ///     The formatted version1 of the input.
        /// </returns>
        public static string FormatVersion(string version)
        {
            if (version == null)
            {
                return null;
            }

            VerifyVersion(version);
            return version;
        }

        /// <summary>
        /// Formats the supplied version string.
        /// </summary>
        /// <param name="version">The version string to format.</param>
        /// <returns>The formatted version of the input.</returns>
        public static string FormatWildcardVersion(string version)
        {
            if (version == null)
            {
                return null;
            }

            if (!VerifyWildcardVersion(version))
            {
                throw new SdmxSyntaxException("Invalid version string provided");
            }
            return version;
        }

        /// <summary>
        ///     The increment version1.
        /// </summary>
        /// <param name="version">
        ///     The version1 1.
        /// </param>
        /// <param name="majorIncrement">
        ///     The major increment.
        /// </param>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="version"/> is <see langword="null" />.</exception>
        public static string IncrementVersion(string version, bool majorIncrement)
        {
            if (version == null)
            {
                throw new ArgumentNullException("version");
            }

            StringBuilder sb = new StringBuilder();

            string[] parts1 = version.Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
            string[] itemsV1 = parts1[0].Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
            int majorVersion = int.Parse(itemsV1[0], CultureInfo.InvariantCulture);
            if (majorIncrement)
            {
                sb.Append(++majorVersion + ".0");
            }
            else
            {
                int minorVersion = 0;
                if (itemsV1.Length > 1)
                {
                    minorVersion = int.Parse(itemsV1[1], CultureInfo.InvariantCulture);
                }

                minorVersion = minorVersion + 1;
                sb.Append(majorVersion + "." + minorVersion);
            }

            // if has tag, add patch version and tag
            if (parts1.Length > 1)
            {
                sb.Append(".0");
                sb.Append("-" + parts1[1]);
            }

            return sb.ToString();
        }

        public static bool IsMatch(string firstVersion, string secondVersion)
        {
            if(_wildcardVersionRegex.IsMatch(firstVersion))
            {
                return HandleWildcardVersion(firstVersion,secondVersion);
            }
            else if (_wildcardVersionRegex.IsMatch(secondVersion))
            {
                return HandleWildcardVersion(secondVersion, firstVersion);
            }
            else
            {
                return firstVersion.Equals(secondVersion);
            }
        }

        private static bool HandleWildcardVersion(string wildcardVersion, string versionToMatchString)
        {
            var lowerVersion = new Version(wildcardVersion.Replace("+",string.Empty));
            var upperVersion = new Version(9,9,9);
            var versionElements = wildcardVersion.Split('.');
            var majorVersion = versionElements[0];
            var minorVersion = versionElements[1];
            var patchVersion = string.Empty;
            if (versionElements.Length > 2)
            {
                patchVersion = versionElements[2];
            }
            if (patchVersion.Contains("+"))
            {
                patchVersion = "0";
                minorVersion = (int.Parse(minorVersion) + 1).ToString();
            }
            if (minorVersion.Contains("+"))
            {
                minorVersion = "0";
                majorVersion = (int.Parse(majorVersion) + 1).ToString();
            }
            if (!majorVersion.Contains("+"))
            {
                upperVersion = new Version(int.Parse(majorVersion), int.Parse(minorVersion), int.Parse(patchVersion));
            }
            else
            {
                upperVersion = new Version(9, 9, 9);
            }
            var versionToMatch = new Version(0, 0, 0);
            if (string.IsNullOrEmpty(versionToMatchString)) 
            {
                 versionToMatch = new Version(0, 0, 0);
            }
            else if (_semanticVersionRegex.IsMatch(versionToMatchString))
            {
                var matches = _semanticVersionRegex.Match(versionToMatchString);
                versionToMatch = new Version(int.Parse(matches.Groups[1].Value), int.Parse(matches.Groups[2].Value), int.Parse(matches.Groups[3].Value));
            }
            else
            {
                versionToMatch = new Version(versionToMatchString);
            }
            
            return lowerVersion.CompareTo(versionToMatch) != 1 &&
                        versionToMatch.CompareTo(upperVersion) != 1;
        }

        /// <summary>
        ///     Returns true if <paramref name="version1" /> has a higher version than <paramref name="version2" />
        /// </summary>
        /// <param name="version1">a String representing a valid SDMX version 1</param>
        /// <param name="version2">a String representing a valid SDMX version 2</param>
        /// <returns>
        ///     true if <paramref name="version1" /> has a higher version then <paramref name="version2" />
        /// </returns>
        public static bool IsHigherVersion(string version1, string version2)
        {
            VerifyVersion(version1);
            VerifyVersion(version2);

            string[] parts1 = version1.Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
            string[] parts2 = version2.Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries);

            string[] itemsV1 = parts1[0].Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
            string[] itemsV2 = parts2[0].Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
            int limit = (itemsV1.Length > itemsV2.Length) ? itemsV2.Length : itemsV1.Length;

            for (int i = 0; i < limit; i++)
            {
                decimal value1 = decimal.Parse(itemsV1[i], CultureInfo.InvariantCulture);
                decimal value2 = decimal.Parse(itemsV2[i], CultureInfo.InvariantCulture);
                int compare = value1.CompareTo(value2);
                if (compare != 0)
                {
                    return compare > 0;
                }
            }
            
            if (itemsV1.Length == itemsV2.Length)
            {
                return parts1.Length < parts2.Length;
            }

            return itemsV1.Length > itemsV2.Length;
        }

        /// <summary>
        ///     Returns if a version1 String is a valid version1 number for the registry.
        /// </summary>
        /// <param name="version">
        ///     the String to check validity of
        /// </param>
        /// <returns>
        ///     true if the version1 is valid for the registry
        /// </returns>
        public static bool ValidVersion(string version)
        {
            if (version == null || version.Length == 0)
            {
                return false;
            }
            try
            {
                VerifyVersion(version);
            }
            catch (ArgumentException)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        ///     Verifies a version1 is in the correct format.
        ///     The accepted format is a positive integer followed by zero or more sub-values, where a sub-value is
        ///     a period (.) followed by a positive integer. There may only be 10 characters in total.
        ///     Examples of legal values: 1 , 1.1 , 1.2.3.4.5.6.7 . 12341.122423221 .
        ///     <p />
        ///     An ArgumentException is thrown if the String argument cannot be represented as a number.
        /// </summary>
        /// <param name="version">
        ///     The string representation of the version1
        /// </param>
        /// <exception cref="System.ArgumentException">
        ///     If an illegal value was supplied
        /// </exception>
        private static void VerifyVersion(string version)
        {
            if (version == null)
            {
                throw new ArgumentException("Null version1 supplied.");
            }

            if (version.Equals("*"))
            {
                return;
            }

            if (_versionRegex.IsMatch(version))
            {
                return;
            }

            if (_semanticVersionRegex.IsMatch(version))
            {
                return;
            }

            throw new ArgumentException("Illegal in version1 supplied '" + version + "'");
        }

        /// <summary>
        /// Verifies a <b>reference</b> version is in the correct format.
	    /// The accepted format is a positive integer followed by zero or more sub-values, where a sub-value is
	    /// a period (.) followed by a positive integer.
        /// Examples of legal values: 1, 1.1, 1.2.3.4.5, 123.1224., 1.3+.0, 2.4.0-draft
        /// </summary>
        /// <param name="version">The string representation of the version.</param>
        /// <returns>True if the version is a valid for reference.</returns>
        public static bool VerifyWildcardVersion(string version)
        {
            if (version == null)
            {
                return false;
            }

            if (version.Equals("*"))
            {
                return true;
            }

            
            if (_wildcardVersionRegex.IsMatch(version))
            {
                return true;
            }


            if (_versionRegex.IsMatch(version))
            {
                return true;
            }

            if (_semanticVersionRegex.IsMatch(version))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Creates a valid format for a draft version.
        /// Is compatible to SDMX 3.0 schema
        /// </summary>
        /// <param name="version">The version to format.</param>
        /// <returns>The formated version.</returns>
        public static string FormatDraftVersion(string version)
        {
            var draftVersion = string.Format(_draftVersionFormat, version);
            //we need to check if draft version is valid because we can have version 1.1
            //and this method creates 1.1-draft which is not valid
            if (IsSdmxV30Valid(draftVersion))
            {
                return draftVersion;
            }
            return version;
        }

        /// <summary>
        /// Tests is the given version is valid against sdmx 3.0 schema.
        /// </summary>
        /// <param name="version">The version to validate.</param>
        /// <returns><c>true</c> if valid for sdmx 3.0, <c>false</c> otherwise.</returns>
        public static bool IsSdmxV30Valid(string version)
        {
            return VerifyWildcardVersion(version) && !_valid2xOnly.IsMatch(version);
        }

        /// <summary>
        /// Tests if the given version is final according to sdmx 3.0 schema.
        /// </summary>
        /// <param name="version">The version to test.</param>
        /// <returns><c>true</c> if found final, <c>false</c> otherwise.</returns>
        public static bool IsFinal(string version)
        {
            //Final flag is replaced by the -SOMETHING extension in version. When it is present it means that artefact is not final. When it is not present it means that the artefact is final.
            //For consistency this applies also to legacy SDMX 2.1 versioning systems.
            return !version.Contains("-");
        }
    }
}