// -----------------------------------------------------------------------
// <copyright file="Weekly.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Date
{
    using System;
    using System.Globalization;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    ///     Weekly frequency
    /// </summary>
    public class Weekly : BasePeriodicity, IPeriodicity
    {
        /// <summary>
        ///     The SDMX date format
        /// </summary>
        public const string SdmxDateTimeFormat = "{0:yyyy}-W{1:00}";

        /// <summary>
        ///     The SDMX date format with offset
        /// </summary>
        public const string SdmxDateTimeFormatWithOffset = "{0:yyyy}-W{1:00}{0:zzz}";

        /// <summary>
        ///     When the period digit starts
        /// </summary>
        private const byte DigitStartIndex = 1;

        /// <summary>
        ///     The number of months in a period
        /// </summary>
        private const byte NumberOfMonths = 0;

        /// <summary>
        ///     The number of months in a period
        /// </summary>
        private const short NumberOfPeriods = 53;

        /// <summary>
        /// The long year weeks.
        /// </summary>
        private const int LongYearWeeks = 53;

        /// <summary>
        /// The short year weeks.
        /// </summary>
        private const int ShortYearWeeks = 52;

        /// <summary>
        ///     The SDMX period only format
        /// </summary>
        private const string SdmxFormat = "\\W0";
        
        /// <summary>
        /// The format size without offset, used to determine if we have an offset or not.
        /// </summary>
        private static readonly int _formatSize = "yyyy-Www".Length;

        /// <summary>
        ///     GESMES period information
        /// </summary>
        private readonly GesmesPeriod _gesmesPeriod;

        /// <summary>
        /// The calendar
        /// </summary>
        private readonly GregorianCalendar _calendar = new GregorianCalendar();

        /// <summary>
        ///     Initializes a new instance of the <see cref="Weekly" /> class
        /// </summary>
        public Weekly()
            : base(TimeFormatEnumType.Week)
        {
            this._gesmesPeriod = new GesmesPeriod(this)
                                     {
                                         DateFormat = EdiTimeFormat.Week, 
                                         PeriodFormat = "0", 
                                         PeriodMax = NumberOfPeriods + 1
                                     };
        }

        /// <summary>
        ///     Gets when the digit starts inside a period time SDMX Time. (For SDMX TIme Period)
        ///     e.g. in Quarterly the digit x is second character "Qx" so starting from 0 it is 1
        /// </summary>
        public byte DigitStart
        {
            get
            {
                return DigitStartIndex;
            }
        }

        /// <summary>
        ///     Gets the format of the period for using it with int.ToString(string format,NumberFormatInfo)
        ///     <see cref="System.Int32" />
        ///     E.g. for monthly is "00" for quarterly is "\\Q0" (For SDMX TIme Period)
        /// </summary>
        public string Format
        {
            get
            {
                return SdmxFormat;
            }
        }

        /// <summary>
        ///     Gets the <see cref="GesmesPeriod" />
        /// </summary>
        public GesmesPeriod Gesmes
        {
            get
            {
                return _gesmesPeriod;
            }
        }

        /// <summary>
        ///     Gets the number of months in a period e.g. 1 for Monthly, 3 for quarterly (For SDMX TIme Period)
        /// </summary>
        public byte MonthsPerPeriod
        {
            get
            {
                return NumberOfMonths;
            }
        }

        /// <summary>
        ///     Gets the number of periods in a period e.g. 12 for monthly (For SDMX TIme Period)
        /// </summary>
        public short PeriodCount
        {
            get
            {
                return NumberOfPeriods;
            }
        }

        /// <summary>
        ///     Gets the period prefix if any
        /// </summary>
        public char PeriodPrefix
        {
            get
            {
                return FrequencyConstants.Weekly;
            }
        }

        /// <summary>
        ///     Add period to the specified <paramref name="dateTime" /> and returns the result.
        /// </summary>
        /// <param name="dateTime">
        ///     The date time.
        /// </param>
        /// <returns>
        ///     The result of adding period to the specified <paramref name="dateTime" />
        /// </returns>
        public DateTime AddPeriod(DateTime dateTime)
        {
            return Calendar.AddWeeks(dateTime, 1);
        }

        // https://www.epochconverter.com/weeks/2004
        /// <summary>
        ///     Convert an SDMX Time Period type to a System.DateTime object
        /// </summary>
        /// <param name="sdmxPeriod">
        ///     A string with the SDMX Time period
        /// </param>
        /// <param name="start">
        ///     If it is true it will expand to the start of the period else towards the end e.g. if it is true 2001-04 will become
        ///     2001-04-01 else it will become 2001-04-30
        /// </param>
        /// <returns>
        ///     A DateTime object
        /// </returns>
        public override DateTimeOffset ToDateTimeOffSet(string sdmxPeriod, bool start)
        {
            if (string.IsNullOrEmpty(sdmxPeriod))
            {
                return PeriodicityHelper.GetDefaultValue();
            }

            TimeSpan offset = TimeSpan.Zero; 
            if (sdmxPeriod.Length > _formatSize)
            {
                offset = PeriodicityHelper.GetTimeSpan(sdmxPeriod, _formatSize);
                sdmxPeriod = sdmxPeriod.Substring(0, _formatSize);
            }

            var yearPeriod = PeriodicityHelper.GetYearPeriod(sdmxPeriod);
            if (yearPeriod == null)
            {
                return PeriodicityHelper.GetDefaultValue();
            }

            var year = yearPeriod.Item1;

            int period = yearPeriod.Item2;

            var ret = new DateTimeOffset(year, 1, 4, 0, 0, 0, offset).AddDays((period - 1) * 7);
            int normalizeDayOfWeek = ((int)this._calendar.GetDayOfWeek(ret.DateTime) + 6) % 7;
            ret = ret.AddDays(-normalizeDayOfWeek);

            return start ? ret : ret.AddDays(6);
        }

        /// <inheritdoc />
        public bool IsValidPeriod(string sdmxPeriod)
        {
            if (sdmxPeriod == null)
            {
                throw new ArgumentNullException("sdmxPeriod");
            }

            // SDMX v2.0 seems to support W1 to W9, W10 to W52 but SDMX v2.1 W01 to W52
            if (sdmxPeriod.Length == 7)
            {
                // SDMX v2.0. TODO remove if normalization happens at the readers/writers
                return PeriodicityHelper.HasValidYearIndicator(sdmxPeriod, FrequencyConstants.Weekly, "123456789".ToCharArray());
            }

            if (sdmxPeriod.Length >= 8)
            {
                // SDMX v2.1 or SDMX v2.0 for week numbers > 10
                if (PeriodicityHelper.HasValidYearIndicator(sdmxPeriod, FrequencyConstants.Weekly, "012345".ToCharArray()))
                {
                    int year;
                    if (!int.TryParse(sdmxPeriod.Substring(0, 4), NumberStyles.None, CultureInfo.InvariantCulture, out year))
                    {
                        return false;
                    }

                    var dayValue = sdmxPeriod.Substring(6, 2);
                    int result;
                    if (!int.TryParse(dayValue, NumberStyles.None, CultureInfo.InvariantCulture, out result))
                    {
                        return false;
                    }

                    // get max ISO week of year
                    var maxWeeks = this.GetMaxWeeksForYear(year);

                    if (result > maxWeeks || result < 1)
                    {
                        return false;
                    }

                    if (sdmxPeriod.Length > 8)
                    {
                        // we have timezone ?
                        return PeriodicityHelper.GetTimeSpan(sdmxPeriod, 8) != TimeSpan.MinValue;
                    }

                    return true;
                }
            }

            return false;
        }


        /// <summary>
        ///     Convert the specified <paramref name="time" /> to SDMX Time Period type representation
        /// </summary>
        /// <param name="time">
        ///     The <see cref="DateTime" /> object to convert
        /// </param>
        /// <returns>
        ///     A string with the SDMX Time Period
        /// </returns>
        public override string ToString(DateTimeOffset time)
        {
            IFormatProvider fmt = CultureInfo.InvariantCulture;
            // TODO use ISOWeek but needs .NET Standard 2.1 or .NET 3.1 or later
            var firstMonday = SetDateToStartOfNextWeek(time);
            var normTime = SetDateToStartOfTheWeek(firstMonday);
            var weekOfYear = this._calendar.GetWeekOfYear(normTime.Date, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            int year = GetWeekYear(weekOfYear, normTime);
            if (time.Offset == TimeSpan.Zero)
            {
                return string.Format(
                    fmt, 
                    SdmxDateTimeFormat, 
                    new DateTime(year, 1, 1),
                    weekOfYear);
            }

            return string.Format(
                fmt, 
                    SdmxDateTimeFormatWithOffset, 
                    new DateTime(year, 1, 1),
                    weekOfYear);
        }

        private static int GetWeekYear(int weekOfYear, DateTimeOffset normTime)
        {
            if (weekOfYear >= 52 && normTime.Month == 1)
            {
                return normTime.Year - 1;
            }
            else if (weekOfYear == 1 && normTime.Month == 12)
            {
                return normTime.Year + 1;
            }
            else
            {
                return normTime.Year;
            }
        }

        /// <summary>
        /// Sets the date to start of the week.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <returns>The date to the start of the week</returns>
        public DateTimeOffset SetDateToStartOfTheWeek(DateTimeOffset startDate)
        {
            // copied from https://blogs.msdn.microsoft.com/shawnste/2006/01/24/iso-8601-week-of-year-format-in-microsoft-net/
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(startDate.DateTime);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                startDate = startDate.AddDays(3);
            }

            return startDate;
        }

        /// <summary>
        /// Sets the date to start of the week.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <returns>The date to the start of the week</returns>
        public DateTime SetDateToStartOfTheWeek(DateTime startDate)
        {
            // copied from https://blogs.msdn.microsoft.com/shawnste/2006/01/24/iso-8601-week-of-year-format-in-microsoft-net/
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = this._calendar.GetDayOfWeek(startDate);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                startDate = startDate.AddDays(3);
            }

            return startDate;
        }

        /// <summary>
        /// Sets the date to start of the the next week unless it is a monday.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <returns>The first monday following this week </returns>
        private DateTimeOffset SetDateToStartOfNextWeek(DateTimeOffset startDate)
        {
            DayOfWeek day = this._calendar.GetDayOfWeek(startDate.DateTime);
            if (day == DayOfWeek.Monday)
            {
                return startDate;
            }

            if (day == DayOfWeek.Sunday) 
            { 
                return startDate.AddDays(1); 
            }
            
            return startDate.AddDays(8 - (int)day);
        }

        /// <summary>
        /// Gets the number of max weeks for year.
        /// </summary>
        /// <param name="year">
        /// The year.
        /// </param>
        /// <returns>
        /// The <see cref="LongYearWeeks"/> or <see cref="ShortYearWeeks"/>
        /// </returns>
        private int GetMaxWeeksForYear(int year)
        {
            var firstDayOfYear = this._calendar.GetDayOfWeek(new DateTime(year, 1, 1));
            if (firstDayOfYear == DayOfWeek.Thursday)
            {
                return LongYearWeeks;
            }

            var lastDayOfYear = this._calendar.GetDayOfWeek(new DateTime(year, 12, 31));
            if (lastDayOfYear == DayOfWeek.Thursday)
            {
                return LongYearWeeks;
            }

            return ShortYearWeeks;
        }
    }
}