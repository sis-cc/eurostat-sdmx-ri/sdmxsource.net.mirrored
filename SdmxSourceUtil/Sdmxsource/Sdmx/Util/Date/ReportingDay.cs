// -----------------------------------------------------------------------
// <copyright file="ReportingDay.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Date
{
    using System;
    using System.Globalization;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    ///     Daily frequency
    /// </summary>
    public class ReportingDay : BasePeriodicity, IPeriodicity
    {
        /// <summary>
        ///     The SDMX date format
        /// </summary>
        public const string SdmxDateTimeFormat = "{0:yyyy}-D{1:000}";

        /// <summary>
        ///     The SDMX date format with offset
        /// </summary>
        public const string SdmxDateTimeFormatWithOffset = "{0:yyyy}-D{1:000}{0:zzz}";

        /// <summary>
        ///     When the period digit starts
        /// </summary>
        private const byte DigitStartIndex = 1;

        /// <summary>
        ///     The number of months in a period
        /// </summary>
        private const byte NumberOfMonths = 0;

        /// <summary>
        ///     The number of months in a period
        /// </summary>
        private const short NumberOfPeriods = 366;

        /// <summary>
        ///     The SDMX period only format
        /// </summary>
        private const string SdmxFormat = "\\D000";

        /// <summary>
        /// The _format size.
        /// </summary>
        private static readonly int _formatSize = "yyyy-Dddd".Length;

        /// <summary>
        ///     Gesmes period information
        /// </summary>
        private readonly GesmesPeriod _gesmesPeriod;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportingDay"/> class. 
        /// </summary>
        public ReportingDay()
            : base(TimeFormatEnumType.ReportingDay)
        {
            this._gesmesPeriod = new GesmesPeriod(this)
                                     {
                                         DateFormat = EdiTimeFormat.DailyFourDigYear, 
                                         RangeFormat = EdiTimeFormat.RangeDaily, 
                                         PeriodFormat = SdmxFormat, 
                                         PeriodMax = 31
                                     };
        }

        /// <summary>
        ///     Gets when the digit starts inside a period time SDMX Time. (For SDMX TIme Period)
        ///     e.g. in Quarterly the digit x is second character "Qx" so starting from 0 it is 1
        /// </summary>
        public byte DigitStart
        {
            get
            {
                return DigitStartIndex;
            }
        }

        /// <summary>
        ///     Gets the format of the period for using it with int.ToString(string format,NumberFormatInfo)
        ///     <see cref="System.Int32" />
        ///     E.g. for monthly is "00" for quarterly is "\\Q0" (For SDMX TIme Period)
        /// </summary>
        public string Format
        {
            get
            {
                return SdmxFormat;
            }
        }

        /// <summary>
        ///     Gets the number of months in a period e.g. 1 for Monthly, 3 for quarterly (For SDMX TIme Period)
        /// </summary>
        public byte MonthsPerPeriod
        {
            get
            {
                return NumberOfMonths;
            }
        }

        /// <summary>
        ///     Gets the <see cref="GesmesPeriod" />
        /// </summary>
        public GesmesPeriod Gesmes
        {
            get
            {
                return _gesmesPeriod;
            }
        }
        
        /// <summary>
        ///     Gets the number of periods in a period e.g. 12 for monthly (For SDMX TIme Period)
        /// </summary>
        public short PeriodCount
        {
            get
            {
                return NumberOfPeriods;
            }
        }

        /// <summary>
        ///     Gets the period prefix if any
        /// </summary>
        public char PeriodPrefix
        {
            get
            {
                return FrequencyConstants.ReportingDay;
            }
        }

        /// <summary>
        ///     Add period to the specified <paramref name="dateTime" /> and returns the result.
        /// </summary>
        /// <param name="dateTime">
        ///     The date time.
        /// </param>
        /// <returns>
        ///     The result of adding period to the specified <paramref name="dateTime" />
        /// </returns>
        public DateTime AddPeriod(DateTime dateTime)
        {
            return Calendar.AddDays(dateTime, 1);
        }

        /// <summary>
        ///     Convert an SDMX Time Period type to a System.DateTime object
        /// </summary>
        /// <param name="sdmxPeriod">
        ///     A string with the SDMX Time period
        /// </param>
        /// <param name="start">
        ///     If it is true it will expand to the start of the period else towards the end e.g. if it is true 2001-04 will become
        ///     2001-04-01 else it will become 2001-04-30
        /// </param>
        /// <returns>
        ///     A DateTime object
        /// </returns>
        public override DateTimeOffset ToDateTimeOffSet(string sdmxPeriod, bool start)
        {
            if (string.IsNullOrEmpty(sdmxPeriod))
            {
                return PeriodicityHelper.GetDefaultValue();
            }

            TimeSpan offset = TimeSpan.Zero; 
            if (sdmxPeriod.Length > _formatSize)
            {
                offset = PeriodicityHelper.GetTimeSpan(sdmxPeriod, _formatSize);

                sdmxPeriod = sdmxPeriod.Substring(0, _formatSize);
            }

            var yearPeriod = PeriodicityHelper.GetYearPeriod(sdmxPeriod);
            if (yearPeriod == null) 
            {
                return PeriodicityHelper.GetDefaultValue();
            }

            DateTimeOffset ret;
            if (start)
            {
                ret = new DateTimeOffset(yearPeriod.Item1, 1, 1, 0, 0, 0, offset).AddDays(yearPeriod.Item2 - 1);
            }
            else
            {
                ret = new DateTimeOffset(yearPeriod.Item1, 1, 1, 23, 59, 59, offset).AddDays(yearPeriod.Item2 - 1);
            }

            return ret;
        }

        /// <inheritdoc />
        public bool IsValidPeriod(string sdmxPeriod)
        {
            if (sdmxPeriod == null)
            {
                throw new ArgumentNullException("sdmxPeriod");
            }

            if (sdmxPeriod.Length >= 9)
            {
                if (PeriodicityHelper.HasValidYearIndicator(sdmxPeriod, FrequencyConstants.ReportingDay, "0123".ToCharArray()))
                {
                    int year;
                    if (!int.TryParse(sdmxPeriod.Substring(0, 4), NumberStyles.None, CultureInfo.InvariantCulture, out year))
                    {
                        return false;
                    }

                    // is always interpreted as a year in the Gregorian calendar.
                    int totalNumberOfDays = DateTime.IsLeapYear(year) ? 366 : 365;

                    var dayValue = sdmxPeriod.Substring(6, 3);
                    int result;
                    if (!int.TryParse(dayValue, NumberStyles.None, CultureInfo.InvariantCulture, out result))
                    {
                        return false;
                    }

                    if (result > totalNumberOfDays || result < 1)
                    {
                        return false;
                    }

                    if (sdmxPeriod.Length > 9)
                    {
                        // we have timezone ?
                        return PeriodicityHelper.GetTimeSpan(sdmxPeriod, 9) != TimeSpan.MinValue;
                    }

                    return true;
                }
            }

            return false;
        }

        /// <inheritdoc />
        public override string ToString(DateTimeOffset time)
        {
            IFormatProvider fmt = CultureInfo.InvariantCulture;

            // Try to maintain compatibility, do not add 'Z'
            if (time.Offset == TimeSpan.Zero)
            {
                return string.Format(fmt, SdmxDateTimeFormat, time, time.DayOfYear);
            }

            return string.Format(fmt, SdmxDateTimeFormatWithOffset, time, time.DayOfYear);
        }
    }
}