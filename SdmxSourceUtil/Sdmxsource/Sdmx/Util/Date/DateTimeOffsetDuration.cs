// -----------------------------------------------------------------------
// <copyright file="DateTimeOffsetDuration.cs" company="EUROSTAT">
//   Date Created : 2018-4-4
//   Copyright (c) 2018 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjectsTests.
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Util.Date
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Model.Base;

    /// <summary>
    /// Holds the date time 
    /// </summary>
    public class DateTimeOffsetDuration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DateTimeOffsetDuration"/> class.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="duration">The duration.</param>
        public DateTimeOffsetDuration(DateTimeOffset date, TimeDuration duration)
        {
            this.Date = date;
            this.Duration = duration;
        }

        /// <summary>
        /// Gets the date.
        /// </summary>
        /// <value>The date.</value>
        public DateTimeOffset Date { get; private set; }

        /// <summary>
        /// Gets the duration.
        /// </summary>
        /// <value>The duration.</value>
        public TimeDuration Duration { get; private set; }
    }
}
