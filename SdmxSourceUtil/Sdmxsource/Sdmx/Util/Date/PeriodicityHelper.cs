// -----------------------------------------------------------------------
// <copyright file="PeriodicityHelper.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Date
{
    using System;
    using System.Globalization;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;

    /// <summary>
    ///     Utils for periodicity
    /// </summary>
    internal static class PeriodicityHelper
    {
        /// <summary>
        ///     The format used in frequencies with prefix and contain two or more months in each period
        /// </summary>
        private const string PrefixFormatString = "{0:yyyy}-{1}{2}";

        /// <summary>
        ///     The format used in frequencies with prefix and contain two or more months in each period
        /// </summary>
        private const string PrefixFormatStringWithOffset = "{0:yyyy}-{1}{2}{0:zzz}";

        /// <summary>
        ///     Convert an SDMX Time Period type to a System.DateTime object
        /// </summary>
        /// <param name="sdmxPeriod">
        ///     A string with the SDMX Time period
        /// </param>
        /// <param name="months">
        ///     The number of months in a period
        /// </param>
        /// <param name="start">
        ///     If it is true it will expand to the start of the period else towards the end e.g. if it is true 2001-04 will become
        ///     2001-04-01 else it will become 2001-04-30
        /// </param>
        /// <param name="digitStart">
        ///     The first digit in the period
        /// </param>
        /// <param name="sizeWithoutOffset">The size of the format without an offset (TimeZone)</param>
        /// <returns>
        ///     A <see cref="DateTime" /> object
        /// </returns>
        public static DateTimeOffset ConvertToDateTimeOffSet(string sdmxPeriod, byte months, bool start, byte digitStart, int sizeWithoutOffset)
        {
            if (!string.IsNullOrEmpty(sdmxPeriod))
            {
                TimeSpan offset = TimeSpan.Zero;
                if (sdmxPeriod.Length > sizeWithoutOffset)
                {
                    offset = GetTimeSpan(sdmxPeriod, sizeWithoutOffset);

                    sdmxPeriod = sdmxPeriod.Substring(0, sizeWithoutOffset);
                }

                string[] dateFields = sdmxPeriod.Split(new[] { '-' }, 3);

                if (dateFields.Length == 2)
                {
                    short period = Convert.ToInt16(dateFields[1].Substring(digitStart), CultureInfo.InvariantCulture);
                    short year = Convert.ToInt16(dateFields[0].Substring(0, 4), CultureInfo.InvariantCulture);
                    int day = 1;
                    int endMonth = 0;
                    if (start)
                    {
                        checked
                        {
                            endMonth = months - 1;
                        }
                    }

                    int month = (period * months) - endMonth;
                    if (!start)
                    {
                        day = DateTime.DaysInMonth(year, month);
                    }

                    return new DateTimeOffset(year, month, day, 0, 0, 0, offset);
                }

                // TODO Shouldn't with throw an exception?
            }

            return GetDefaultValue();
        }

        /// <summary>
        /// Gets the timezone offset (ISO 8601 compatible)
        /// </summary>
        /// <param name="sdmxPeriod">The SDMX period.</param>
        /// <param name="formatSizeWithoutOffset">The format size without offset.</param>
        /// <returns>The offset.</returns>
        public static TimeSpan GetTimeSpan(string sdmxPeriod, int formatSizeWithoutOffset)
        {
            if (sdmxPeriod.Length == formatSizeWithoutOffset)
            {
                return TimeSpan.Zero;
            }

            TimeSpan offset = TimeSpan.MinValue;
            var inputOffset = sdmxPeriod.Substring(formatSizeWithoutOffset);
            DateTimeOffset withOffset;
            if (DateTimeOffset.TryParseExact(inputOffset, "zzz", CultureInfo.InvariantCulture, DateTimeStyles.None, out withOffset))
            {
                offset = withOffset.Offset;
            }

            return offset;
        }
 
        /// <summary>
        /// Convert the <paramref name="sdmxPeriod"/> to <see cref="DateTimeOffset"/>
        /// </summary>
        /// <param name="sdmxPeriod">
        /// The SDMX Period.
        /// </param>
        /// <param name="format">
        /// The format.
        /// </param>
        /// <param name="formatWithOffset">
        /// The format With Offset.
        /// </param>
        /// <returns>
        /// The <see cref="DateTimeOffset"/>.
        /// </returns>
       public static DateTimeOffset ConvertGregorianToDateTime(string sdmxPeriod, string format, string formatWithOffset)
        {
            if (!string.IsNullOrEmpty(sdmxPeriod))
            {
                DateTimeOffset ret;
                if (DateTimeOffset.TryParseExact(sdmxPeriod, new[] { format, formatWithOffset }, CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal, out ret))
                {
                    return ret;
                }
               
                // TODO Shouldn't with throw an exception?
                throw new SdmxSyntaxException(ExceptionCode.InvalidDateFormat, sdmxPeriod);
            }

            return GetDefaultValue();
        }

        /// <summary>
        ///     Convert the specified <paramref name="time" /> to SDMX Time Period type representation
        /// </summary>
        /// <param name="time">
        ///     The <see cref="DateTime" /> object to convert
        /// </param>
        /// <param name="months">
        ///     The number of months in a period
        /// </param>
        /// <param name="prefix">
        ///     The periodicity prefix
        /// </param>
        /// <returns>
        ///     A string with the SDMX Time Period
        /// </returns>
        public static string ConvertToString(DateTimeOffset time, int months, char prefix)
        {
            IFormatProvider fmt = CultureInfo.InvariantCulture;
            if (time.Day > 1)
            {
                time = time.AddMonths(1);
            }

            int period = ((time.Month - 1) / months) + 1;
            period += ((time.Month - 1) % months) > 0 ? 1 : 0;
            if (period > (12/months))
            {
                return string.Format(fmt, PrefixFormatString, time.AddYears(1), prefix, 1);
            }

            return string.Format(fmt, PrefixFormatString, time, prefix, period);
        }

        /// <summary>
        /// Get the (year, period) a tuple
        /// </summary>
        /// <param name="sdmxPeriod">The SDMX period</param>
        /// <param name="digitStart">The index of the period value in the period part</param>
        /// <exception cref="ArgumentNullException">The <paramref name="sdmxPeriod"/> is null</exception>
        /// <returns>The (year, period) tuple; otherwise null</returns>
        public static Tuple<int, int> GetYearPeriod(string sdmxPeriod, int digitStart = 6)
        {
            if (sdmxPeriod == null)
            {
                throw new ArgumentNullException("sdmxPeriod");
            }

            if (sdmxPeriod.Length + 1 < digitStart)
            {
                return null;
            }
                
            int period = Convert.ToInt32(sdmxPeriod.Substring(digitStart), CultureInfo.InvariantCulture);
            int year = Convert.ToInt32(sdmxPeriod.Substring(0, 4), CultureInfo.InvariantCulture);
            return Tuple.Create(year, period);
        }

        /// <summary>
        /// Gets the default value
        /// </summary>
        /// <returns>
        /// The <see cref="DateTimeOffset"/>.
        /// </returns>
        public static DateTimeOffset GetDefaultValue()
        {
            return DateTimeOffset.MinValue;
        }

        /// <summary>
        /// Gets a value indicating whether the <paramref name="sdmxPeriod"/> has a valid year followed by dash <c>'-'</c> and the <paramref name="periodIndicator"/> 
        /// and the character after the <paramref name="periodIndicator"/> is one of the <paramref name="digits"/>
        /// </summary>
        /// <param name="sdmxPeriod">
        /// The sdmx period.
        /// </param>
        /// <param name="periodIndicator">
        /// The period indicator.
        /// </param>
        /// <param name="digits">
        /// The digits.
        /// </param>
        /// <returns>
        /// <c>true</c> if the <paramref name="sdmxPeriod"/> has a valid year followed by dash <c>'-'</c> and the <paramref name="periodIndicator"/> 
        /// and the character after the <paramref name="periodIndicator"/> is one of the <paramref name="digits"/>; othewise false
        /// </returns>
        public static bool HasValidYearIndicator(string sdmxPeriod, char periodIndicator, params char[] digits)
        {
            return sdmxPeriod.Length >= 7 && HasYear(sdmxPeriod) && HasDash(sdmxPeriod) && sdmxPeriod[5] == periodIndicator && HasDigit(sdmxPeriod[6], digits);
        }

        /// <summary>
        /// Checks if if the specified <paramref name="dateString"/> starts with a year
        /// </summary>
        /// <param name="dateString">The SDMX period</param>
        /// <returns><c>true</c> if it has a year; <c>false</c> otherwise.</returns>
        private static bool HasYear(string dateString)
        {
            return IsNumber(dateString, 0, 4);
        }

        /// <summary>
        /// Gets a value indicating whether the specificed <paramref name="dateStr"/> has a <c>-</c> at position 4
        /// </summary>
        /// <param name="dateStr">The SDMX Period</param>
        /// <returns><c>true</c> if the  the specificed <paramref name="dateStr"/> has a <c>-</c> at position 4; otherwise <c>false</c></returns>
        private static bool HasDash(string dateStr)
        {
            return dateStr.Length > 4 && dateStr[4] == '-';
        }

        /// <summary>
        /// Gets a value indicating whether the <paramref name="c"/> is one of the <paramref name="digits"/>
        /// </summary>
        /// <param name="c">
        /// The character
        /// </param>
        /// <param name="digits">
        /// The digits
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool HasDigit(char c, params char[] digits)
        {
            for (int i = 0; i < digits.Length; i++)
            {
                if (digits[i] == c) 
                {
                    return true;
                }
            }

            return false;
        }
      
        /// <summary>
        /// Gets a value indicating whether the <paramref name="dateStr"/> is numbers from <paramref name="start"/> up to <paramref name="length"/>
        /// </summary>
        /// <param name="dateStr">
        /// The date Str.
        /// </param>
        /// <param name="start">
        /// The start.
        /// </param>
        /// <param name="length">
        /// The length.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool IsNumber(string dateStr, int start, int length)
        {
            for (int i = start; i < length; i++)
            {
                var c = dateStr[i];
                if (!char.IsNumber(c))
                {
                    return false;
                }
            }

            return true;
        }
    }
}