// -----------------------------------------------------------------------
// <copyright file="Triannual.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Date
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    ///     Tri-annual frequency
    /// T1 : 01/01 to 30/04
    /// T2 : 01/05 to 31/08
    /// T3 : 01/09 to 31/12
    /// </summary>
    public class TriAnnual : BasePeriodicity, IPeriodicity
    {
        /// <summary>
        ///     When the period digit starts
        /// </summary>
        private const byte DigitStartIndex = 1;

        /// <summary>
        ///     The number of months in a period
        /// </summary>
        private const byte NumberOfMonths = 4;

        /// <summary>
        ///     The number of months in a period
        /// </summary>
        private const short NumberOfPeriods = 3;

        /// <summary>
        ///     The SDMX period only format
        /// </summary>
        private const string SdmxFormat = "\\T0";

        /// <summary>
        /// The format size without offset, used to determine if we have an offset or not.
        /// </summary>
        private static readonly int _formatSize = "yyyy-Tt".Length;

        /// <summary>
        ///     The no gesmes.
        /// </summary>
        private readonly GesmesPeriod _noGesmes;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TriAnnual" /> class
        ///     Prevents a default instance of the <see cref="TriAnnual" /> class from being created.
        /// </summary>
        public TriAnnual()
            : base(TimeFormatEnumType.ThirdOfYear)
        {
            this._noGesmes = new GesmesPeriod(this);
        }

        /// <summary>
        ///     Gets when the digit starts inside a period time SDMX Time. (For SDMX TIme Period)
        ///     e.g. in Quarterly the digit x is second character "Qx" so starting from 0 it is 1
        /// </summary>
        public byte DigitStart
        {
            get
            {
                return DigitStartIndex;
            }
        }

        /// <summary>
        ///     Gets the format of the period for using it with int.ToString(string format,NumberFormatInfo)
        ///     <see cref="System.Int32" />
        ///     E.g. for monthly is "00" for quarterly is "\\Q0" (For SDMX TIme Period)
        /// </summary>
        public string Format
        {
            get
            {
                return SdmxFormat;
            }
        }

        /// <summary>
        ///     Gets the <see cref="GesmesPeriod" />
        /// </summary>
        public GesmesPeriod Gesmes
        {
            get
            {
                return _noGesmes;
            }
        }

        /// <summary>
        ///     Gets the number of months in a period e.g. 1 for Monthly, 3 for quarterly (For SDMX TIme Period)
        /// </summary>
        public byte MonthsPerPeriod
        {
            get
            {
                return NumberOfMonths;
            }
        }

        /// <summary>
        ///     Gets the number of periods in a period e.g. 12 for monthly (For SDMX TIme Period)
        /// </summary>
        public short PeriodCount
        {
            get
            {
                return NumberOfPeriods;
            }
        }

        /// <summary>
        ///     Gets the period prefix if any
        /// </summary>
        public char PeriodPrefix
        {
            get
            {
                return FrequencyConstants.TriAnnual;
            }
        }

        /// <summary>
        ///     Add period to the specified <paramref name="dateTime" /> and returns the result.
        /// </summary>
        /// <param name="dateTime">
        ///     The date time.
        /// </param>
        /// <returns>
        ///     The result of adding period to the specified <paramref name="dateTime" />
        /// </returns>
        public DateTime AddPeriod(DateTime dateTime)
        {
            return Calendar.AddMonths(dateTime, NumberOfMonths);
        }

        /// <inheritdoc />
        public override DateTimeOffset ToDateTimeOffSet(string sdmxPeriod, bool start)
        {
            return PeriodicityHelper.ConvertToDateTimeOffSet(sdmxPeriod, this.MonthsPerPeriod, start, this.DigitStart, _formatSize);
        }

        /// <inheritdoc />
        public bool IsValidPeriod(string sdmxPeriod)
        {
            if (sdmxPeriod == null)
            {
                throw new ArgumentNullException("sdmxPeriod");
            }

            if (sdmxPeriod.Length == 7)
            {
                return PeriodicityHelper.HasValidYearIndicator(sdmxPeriod, FrequencyConstants.TriAnnual, "123".ToCharArray());
            }

            return false;
        }

        /// <inheritdoc />
        public override string ToString(DateTimeOffset time)
        {
            return PeriodicityHelper.ConvertToString(time, this.MonthsPerPeriod, this.PeriodPrefix);
        }
    }
}