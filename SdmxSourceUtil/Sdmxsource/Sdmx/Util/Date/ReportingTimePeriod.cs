// -----------------------------------------------------------------------
// <copyright file="ReportingTimePeriod.cs" company="EUROSTAT">
//   Date Created : 2017-09-18
//   Copyright (c) 2012, 2017 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Date
{
    using System;
    using System.Globalization;
    using System.Xml;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;

    /// <summary>
    /// This class allows to convert between Gregorian Time periods and Reporting Time Periods
    /// </summary>
    public class ReportingTimePeriod
    {
        /// <summary>
        /// The _reporting year start date default value.
        /// </summary>
        private static readonly string _reportingYearStartDateDefaultValue = "--01-01";

        /// <summary>
        /// A reporting quarter represents a period of 3 months (P3M) from the start date of the reporting year. This is expressed as using the SDMX specific ReportingQuarterType.
        /// </summary>
        private readonly int P3M = 3;

        /// <summary>
        /// A reporting trimester represents a period of 4 months (P4M) from the start date of the reporting year. This is expressed as using the SDMX specific ReportingTrimesterType.
        /// </summary>
        private readonly int P4M = 4;

        /// <summary>
        /// A reporting semester represents a period of 6 months (P6M) from the start date of the reporting year. This is expressed as using the SDMX specific ReportingSemesterType.
        /// </summary>
        private readonly int P6M = 6;

        /// <summary>
        /// A reporting week represents a period of 7 days (P7D) from the start date of the reporting year. This is expressed as using the SDMX specific ReportingWeekType.
        /// </summary>
        private readonly int P7D = 7;

        /// <summary>
        /// The weekly
        /// </summary>
        private readonly Weekly _weekly = new Weekly();

        /// <summary>
        /// The calendar
        /// </summary>
        private readonly GregorianCalendar _calendar = new GregorianCalendar();

        /// <summary>
        /// Gets a value indicating whethere the specified <paramref name="reportingYearStartDateString"/> is null, empty or has the default value <c>--01-01</c>
        /// </summary>
        /// <param name="reportingYearStartDateString">
        /// The reporting year start date string.
        /// </param>
        /// <returns>
        /// <c>true</c> the specified <paramref name="reportingYearStartDateString"/> is null, empty or has the default value <c>--01-01</c>; otherwise <c>false</c>
        /// </returns>
        public static bool IsDefaultReportingYearStartDay(string reportingYearStartDateString)
        {
            return string.IsNullOrWhiteSpace(reportingYearStartDateString) || reportingYearStartDateString.Equals(_reportingYearStartDateDefaultValue);
        }

        /// <summary>
        /// Convert the <paramref name="reportingPeriod"/> to a SDMX v2.1 Time Interval (subset of ISO 8601 )
        /// </summary>
        /// <param name="reportingPeriod">
        /// The reporting Period.
        /// </param>
        /// <param name="reportingYearStartDateString">
        /// The reporting Year Start Date String.
        /// </param>
        /// <returns>
        /// A SDMX v2.1 Time Interval (subset of ISO 8601 ) 
        /// </returns>
        public string ToTimeRange(string reportingPeriod, string reportingYearStartDateString)
        {
            if (string.IsNullOrWhiteSpace(reportingYearStartDateString))
            {
                reportingYearStartDateString = _reportingYearStartDateDefaultValue;
            }
            
            var periodIndicator = DateUtil.GetTimeFormatOfDate(reportingPeriod);
            var periodStart = this.GetDateTimeOffset(reportingPeriod, reportingYearStartDateString, periodIndicator);
            if (string.IsNullOrEmpty(periodIndicator.IsoDuration))
            {
                throw new SdmxNotImplementedException(ExceptionCode.UnsupportedTransform, periodIndicator.EnumType);
            }

            var duration = TimeDuration.Parse(periodIndicator.IsoDuration);
            var dateTimeDuration = new DateTimeOffsetDuration(periodStart, duration);
            return DateUtil.FormatDate(dateTimeDuration, TimeFormat.TimeRange);
        }

        /// <summary>
        /// Parse two strings to a gregorian period
        /// </summary>
        /// <param name="reportingPeriod">The reporting period.</param>
        /// <param name="reportingYearStartDateString">The reporting year start date string.</param>
        /// <returns>
        /// The <see cref="SDMXGregorianPeriod" />.
        /// </returns>
        /// <exception cref="SdmxSemmanticException">Reporting period is not valid</exception>
        public SDMXGregorianPeriod ToGregorianPeriod(string reportingPeriod, string reportingYearStartDateString)
        {
            if (string.IsNullOrWhiteSpace(reportingYearStartDateString))
            {
                reportingYearStartDateString = _reportingYearStartDateDefaultValue;
            }

            var periodIndicator = DateUtil.GetTimeFormatOfDate(reportingPeriod);
            TimeFormat sdmxv20CompatibleFormat = GetSDMXv20CompatibleFormat(periodIndicator);
            if (!this.CheckReportingPeriod(reportingPeriod, reportingYearStartDateString))
            {
                return new SDMXGregorianPeriod(DateUtil.FormatDate(reportingPeriod, true), sdmxv20CompatibleFormat);
            }

            var periodStart = this.GetDateTimeOffset(reportingPeriod, reportingYearStartDateString, periodIndicator);

            return new SDMXGregorianPeriod(periodStart, sdmxv20CompatibleFormat);
        }

        /// <summary>
        /// Checks if reporting period strings are valid
        /// </summary>
        /// <param name="reportingPeriod">The reporting period.</param>
        /// <param name="reportingYearStartDateString">The reporting year start date string.</param>
        /// <returns>
        ///   <c>true</c> if <paramref name="reportingPeriod" /> is a reporting period (from SdmxSource point of view); otherwise false.
        /// </returns>
        public bool CheckReportingPeriod(string reportingPeriod, string reportingYearStartDateString)
        {
            if (IsDefaultReportingYearStartDay(reportingYearStartDateString))
            {
                return false;
            }

            var timeFormat = DateUtil.GetTimeFormatOfDate(reportingPeriod);
            return DateUtil.IsReportingPeriod(timeFormat);
        }

        /// <summary>
        /// Convert the gregorian time to a reporting period, using as offset the specified <paramref name="reportingYearStartDate"/>
        /// </summary>
        /// <param name="sdmxDate">The SDMX date.</param>
        /// <param name="reportingYearStartDate">The reporting year start date.</param>
        /// <returns>The reporting period</returns>
        public string ToReportingPeriod(ISdmxDate sdmxDate, string reportingYearStartDate)
        {
            if (IsDefaultReportingYearStartDay(reportingYearStartDate) && DateUtil.IsReportingPeriod(sdmxDate.TimeFormatOfDate))
            {
                return sdmxDate.DateInSdmxFormat;
            }

            var frequencyCode = sdmxDate.TimeFormatOfDate.FrequencyCode;
            var dateValue = sdmxDate.Date;
            int periodValue;
            var periodString = string.Empty;
            var periodStart = DateTime.MinValue;
            var utcDateTime = XmlConvert.ToDateTime(reportingYearStartDate, XmlDateTimeSerializationMode.Utc);
            var valueToAdd = dateValue.IsMonthAndDayAfter(utcDateTime) ? dateValue.Year - utcDateTime.Year : dateValue.Year - utcDateTime.Year - 1;
            var startDate = utcDateTime.AddYears(valueToAdd);

            // TODO use DateUtil/Periodicity for formating
            switch (sdmxDate.TimeFormatOfDate.EnumType)
            {
                case TimeFormatEnumType.Year:
                case TimeFormatEnumType.ReportingYear:
                    periodString = "1";
                    periodStart = startDate;
                    break;
                case TimeFormatEnumType.HalfOfYear:
                    periodValue = dateValue.MonthDifference(startDate) / this.P6M + 1;
                    periodStart = dateValue.AddMonths((-periodValue + 1) * this.P6M);
                    periodString = periodValue.ToString(CultureInfo.InvariantCulture);
                    break;
                case TimeFormatEnumType.ThirdOfYear:
                    periodValue = dateValue.MonthDifference(startDate) / this.P4M + 1;
                    periodStart = dateValue.AddMonths((-periodValue + 1) * this.P4M);
                    periodString = periodValue.ToString(CultureInfo.InvariantCulture);
                    break;
                case TimeFormatEnumType.QuarterOfYear:
                    periodValue = dateValue.MonthDifference(startDate) / this.P3M + 1;
                    periodStart = dateValue.AddMonths((-periodValue + 1) * this.P3M);
                    periodString = periodValue.ToString(CultureInfo.InvariantCulture);
                    break;
                case TimeFormatEnumType.Month:
                case TimeFormatEnumType.ReportingMonth:
                    periodValue = dateValue.MonthDifference(startDate) + 1;
                    periodStart = dateValue.AddMonths(-periodValue + 1);
                    periodString = periodValue.ToString().PadLeft(2, '0');
                    break;
                case TimeFormatEnumType.Week:

                    startDate = SetDateToStartOfTheWeek(startDate, utcDateTime.DayOfWeek);

                    periodValue = dateValue.Subtract(startDate).Days / this.P7D + 1;
                    periodStart = dateValue.AddDays((-periodValue + 1) * this.P7D);
                    periodString = periodValue.ToString().PadLeft(2, '0');
                    break;
                case TimeFormatEnumType.Date:
                case TimeFormatEnumType.ReportingDay:
                    periodValue = dateValue.Subtract(startDate).Days + 1;
                    periodStart = dateValue.AddDays(-periodValue + 1);
                    periodString = periodValue.ToString().PadLeft(3, '0');
                    break;
                default:
                    throw new SdmxSemmanticException("Cannot convert given SDMX period to Reporting period");
            }

            return periodStart.Year + "-" + frequencyCode + periodString;
        }

        /// <summary>
        /// Gets the SDM XV20 compatible format.
        /// </summary>
        /// <param name="timeFormat">The time format.</param>
        /// <returns>The <see cref="TimeFormat"/>.</returns>
        private static TimeFormat GetSDMXv20CompatibleFormat(TimeFormat timeFormat)
        {
            switch (timeFormat.EnumType)
            {
                case TimeFormatEnumType.ReportingYear:
                    return TimeFormat.Year;
               case TimeFormatEnumType.ReportingMonth:
                    return TimeFormat.Month;
               case TimeFormatEnumType.ReportingDay:
                    return TimeFormat.Date;
                default:
                    return timeFormat;
            }
        }

        /// <summary>
        /// Get time zone offset
        /// </summary>
        /// <param name="reportingPeriod">
        /// The reporting Period.
        /// </param>
        /// <param name="reportingYearStartDate">
        /// The reporting Year Start Date.
        /// </param>
        /// <param name="formatSizeWithoutOffset">
        /// The format Size Without Offset.
        /// </param>
        /// <returns>
        /// The <see cref="TimeSpan"/>.
        /// </returns>
        private static TimeSpan GetTimeZoneOffset(string reportingPeriod, DateTimeOffsetDuration reportingYearStartDate, int formatSizeWithoutOffset)
        {
            var timeZoneOffset = PeriodicityHelper.GetTimeSpan(reportingPeriod, formatSizeWithoutOffset);
            if (timeZoneOffset == TimeSpan.Zero)
            {
                timeZoneOffset = reportingYearStartDate.Date.Offset;
            }

            return timeZoneOffset;
        }

        /// <summary>
        /// Sets the date to start of the week.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="dayOfWeek">The day of week.</param>
        /// <returns>The date to the start of the week</returns>
        private static DateTime SetDateToStartOfTheWeek(DateTime startDate, DayOfWeek dayOfWeek)
        {
            switch (dayOfWeek)
            {
                case DayOfWeek.Friday:
                    startDate = startDate.AddDays(3);
                    break;
                case DayOfWeek.Saturday:
                    startDate = startDate.AddDays(2);
                    break;
                case DayOfWeek.Sunday:
                    startDate = startDate.AddDays(1);
                    break;
                case DayOfWeek.Tuesday:
                    startDate = startDate.AddDays(-1);
                    break;
                case DayOfWeek.Wednesday:
                    startDate = startDate.AddDays(-2);
                    break;
                case DayOfWeek.Thursday:
                    startDate = startDate.AddDays(-3);
                    break;
            }

            return startDate;
        }

        /// <summary>
        /// Extract the date time offset.
        /// </summary>
        /// <param name="reportingPeriod">
        /// The reporting period.
        /// </param>
        /// <param name="reportingYearStartDateString">
        /// The reporting year start date string.
        /// </param>
        /// <param name="periodIndicator">
        /// The period indicator.
        /// </param>
        /// <returns>
        /// The <see cref="DateTimeOffset"/>.
        /// </returns>
        /// <exception cref="SdmxSemmanticException">
        /// The <paramref name="periodIndicator"/> is not a SDMX v2.1 Reporting period format
        /// </exception>
        private DateTimeOffset GetDateTimeOffset(string reportingPeriod, string reportingYearStartDateString, TimeFormat periodIndicator)
        {
            var reportingTimePeriodValues = new ReportingTimePeriodValues(reportingPeriod, reportingYearStartDateString);

            var reportingYearStartDate = reportingTimePeriodValues.ReportingYearStartDate;
            var yearPeriod = PeriodicityHelper.GetYearPeriod(reportingPeriod);

            // TODO timezone offset support
            var reportingYearBase = new DateTime(yearPeriod.Item1, reportingYearStartDate.Duration.Months, reportingYearStartDate.Duration.Days, this._calendar);

            var periodValue = yearPeriod.Item2;
            if (periodIndicator == this._weekly.TimeFormat)
            {
                reportingYearBase = SetDateToStartOfTheWeek(reportingYearBase, reportingYearBase.DayOfWeek);
            }

            DateTimeOffset periodStart;
            var periodValueForStart = periodValue - 1;

            TimeSpan timeZoneOffset;

            // TODO reuse TimeFormat duration
            switch (periodIndicator.EnumType)
            {
                case TimeFormatEnumType.ReportingYear:
                    timeZoneOffset = GetTimeZoneOffset(reportingPeriod, reportingYearStartDate, 7);

                    periodStart = new DateTimeOffset(reportingYearBase.AddYears(periodValueForStart), timeZoneOffset);
                    break;
                case TimeFormatEnumType.HalfOfYear:
                    timeZoneOffset = GetTimeZoneOffset(reportingPeriod, reportingYearStartDate, 7);

                    periodStart = new DateTimeOffset(reportingYearBase.AddMonths(periodValueForStart * this.P6M), timeZoneOffset);
                    break;
                case TimeFormatEnumType.ThirdOfYear:
                    timeZoneOffset = GetTimeZoneOffset(reportingPeriod, reportingYearStartDate, 7);
                    periodStart = new DateTimeOffset(reportingYearBase.AddMonths(periodValueForStart * this.P4M), timeZoneOffset);
                    break;
                case TimeFormatEnumType.QuarterOfYear:
                    timeZoneOffset = GetTimeZoneOffset(reportingPeriod, reportingYearStartDate, 7);
                    periodStart = new DateTimeOffset(reportingYearBase.AddMonths(periodValueForStart * this.P3M), timeZoneOffset);
                    break;
                case TimeFormatEnumType.ReportingMonth:
                    timeZoneOffset = GetTimeZoneOffset(reportingPeriod, reportingYearStartDate, 8);
                    periodStart = new DateTimeOffset(reportingYearBase.AddMonths(periodValueForStart), timeZoneOffset);
                    break;
                case TimeFormatEnumType.Week:
                    timeZoneOffset = GetTimeZoneOffset(reportingPeriod, reportingYearStartDate, 8);
                    periodStart = new DateTimeOffset(reportingYearBase.AddDays(periodValueForStart * this.P7D), timeZoneOffset);
                    break;
                case TimeFormatEnumType.ReportingDay:
                    timeZoneOffset = GetTimeZoneOffset(reportingPeriod, reportingYearStartDate, 9);
                    periodStart = new DateTimeOffset(reportingYearBase.AddDays(periodValueForStart), timeZoneOffset);
                    break;
                default:
                    throw new SdmxSemmanticException(ExceptionCode.InvalidDateFormat, reportingPeriod + "/" + reportingYearStartDateString);
            }

            return periodStart;
        }
    }
}