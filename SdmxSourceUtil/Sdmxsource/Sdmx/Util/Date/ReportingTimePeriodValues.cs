// -----------------------------------------------------------------------
// <copyright file="ReportingTimePeriodValues.cs" company="EUROSTAT">
//   Date Created : 2017-09-21
//   Copyright (c) 2012, 2017 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Date
{
    using System;
    using System.Xml;

    using Api.Constants;

    using Org.Sdmxsource.Sdmx.Api.Model.Base;

    /// <summary>
    /// This model class represents a Reporting Time Period
    /// </summary>
    public class ReportingTimePeriodValues
    {
        /// <summary>
        /// The _month day without timezone offset size.
        /// </summary>
        private static readonly int _monthDayNoOffSetSize = "--MM-dd".Length;

        /// <summary>
        /// The reporting period
        /// </summary>
        private readonly string _reportingPeriod;
        
        /// <summary>
        /// The reporting year start date string
        /// </summary>
        private readonly string _reportingYearStartDateString;

        /// <summary>
        /// The frequency
        /// </summary>
        private TimeFormat _frequency;

        /// <summary>
        /// The reporting year start date
        /// </summary>
        private DateTimeOffsetDuration _reportingYearStartDate;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportingTimePeriodValues"/> class. 
        /// </summary>
        /// <param name="reportingPeriod">
        /// The reporting period.
        /// </param>
        /// <param name="reportingYearStartDateString">
        /// The reporting year start date string.
        /// </param>
        public ReportingTimePeriodValues(string reportingPeriod, string reportingYearStartDateString)
        {
            this._reportingPeriod = reportingPeriod;
            this._reportingYearStartDateString = reportingYearStartDateString;
        }

        /// <summary>
        /// Gets the period value.
        /// </summary>
        /// <value>
        /// The period value.
        /// </value>
        public int PeriodValue
        {
            get { return PeriodicityHelper.GetYearPeriod(this._reportingPeriod).Item2; }
        }

        /// <summary>
        /// Gets the reporting year start date.
        /// </summary>
        /// <value>
        /// The reporting year start date.
        /// </value>
        public DateTimeOffsetDuration ReportingYearStartDate
        {
            get
            {
                if (this._reportingYearStartDate == null)
                {
                    string utcOffset = string.Empty;
                    if (this._reportingYearStartDateString.Length == _monthDayNoOffSetSize)
                    {
                        utcOffset = "Z";
                    }

                    var dateTime = XmlConvert.ToDateTimeOffset(this._reportingYearStartDateString + utcOffset);
                    var timeDuration = new TimeDuration(0, dateTime.Month, TimeSpan.FromDays(dateTime.Day));
                    this._reportingYearStartDate = new DateTimeOffsetDuration(dateTime, timeDuration);
                }

                return this._reportingYearStartDate;
            }
        }

        /// <summary>
        /// Gets the frequency.
        /// </summary>
        /// <value>
        /// The frequency.
        /// </value>
        public TimeFormat Frequency
        {
            get
            {
                return this._frequency ?? (this._frequency = DateUtil.GetTimeFormatOfDate(this._reportingPeriod));
            }
        }
    }
}