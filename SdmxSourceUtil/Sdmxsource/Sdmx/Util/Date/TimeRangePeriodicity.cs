// -----------------------------------------------------------------------
// <copyright file="TimeRangePeriodicity.cs" company="EUROSTAT">
//   Date Created : 2018-4-3
//   Copyright (c) 2018 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjectsTests.
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Util.Date
{
    using System;
    using System.Globalization;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;

    /// <summary>
    /// Periodicity for time range
    /// </summary>
    public class TimeRangePeriodicity : IPeriodicity
    {
        /// <summary>
        /// The minimum size of the start date
        /// </summary>
        private static readonly int _minStartSize = "yyyy-MM-dd".Length;

        /// <summary>
        /// The _formats.
        /// </summary>
        private static readonly string[] _formats = { "o", "s", "u", Hourly.SdmxDateTimeFormat, Hourly.SdmxDateTimeFormatOffset, Hourly.SdmxDateTimeFormatZ, Daily.SdmxDateTimeFormat, Daily.SdmxDateTimeFormatWithOffset };

        /// <inheritdoc />
        public byte DigitStart
        {
            get
            {
                return 0;
            }
        }

        /// <inheritdoc />
        public string Format
        {
            get
            {
                return string.Empty;
            }
        }

        /// <inheritdoc />
        public GesmesPeriod Gesmes
        {
            get
            {
                return null;
            }
        }

        /// <inheritdoc />
        public byte MonthsPerPeriod { get; set; }

        /// <inheritdoc />
        public short PeriodCount { get; set; }

        /// <summary>
        ///     Gets the period prefix if any
        /// </summary>
        public char PeriodPrefix
        {
            get
            {
                return FrequencyConstants.Null;
            }
        }

        /// <inheritdoc />
        public TimeFormat TimeFormat
        {
            get
            {
                return TimeFormat.TimeRange;
            }
        }

        /// <inheritdoc />
        public DateTime AddPeriod(DateTime dateTime)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public bool IsValidPeriod(string sdmxPeriod)
        {
           return this.ToDateTimeOffsetDuration(sdmxPeriod) != null;
        }

        /// <inheritdoc />
        public DateTime ToDateTime(string sdmxPeriod, bool start)
        {
            return this.ToDateTimeOffSet(sdmxPeriod, start).DateTime;
        }

        /// <inheritdoc />
        public DateTimeOffset ToDateTimeOffSet(string sdmxPeriod, bool start)
        {
            var dateDuration = this.ToDateTimeOffsetDuration(sdmxPeriod);
            if (dateDuration != null)
            {
                return dateDuration.Date;
            }

            throw new SdmxSemmanticException(ExceptionCode.InvalidDateFormat, sdmxPeriod);
        }

        /// <inheritdoc />
        public string ToString(DateTime time)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public string ToString(DateTimeOffset time)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Converts the specified period to a Date Time with duration.
        /// </summary>
        /// <param name="sdmxPeriod">The SDMX period.</param>
        /// <returns>The <see cref="DateTimeOffsetDuration" />.</returns>
        public DateTimeOffsetDuration ToDateTimeOffsetDuration(string sdmxPeriod)
        {
            if (sdmxPeriod == null)
            {
                throw new ArgumentNullException("sdmxPeriod");
            }

            var durationSeparator = sdmxPeriod.IndexOf('/');
            if (durationSeparator >= _minStartSize)
            {
                // Time Range
                var durationStart = sdmxPeriod.IndexOf('P');
                if (durationStart == durationSeparator + 1)
                {
                    var isoStartDate = sdmxPeriod.Substring(0, durationSeparator);
                    DateTimeOffset ret;
                    if (!DateTimeOffset.TryParseExact(isoStartDate, _formats, CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal, out ret))
                    {
                        return null;
                    }

                    var isoDuration = sdmxPeriod.Substring(durationStart);

                    if (!TimeDuration.Check(isoDuration))
                    {
                        return null;
                    }

                    return new DateTimeOffsetDuration(ret, TimeDuration.Parse(isoDuration));
                }
            }

            return null;
        }

        /// <inheritdoc />
        public string ToString(DateTimeOffsetDuration dateDuration)
        {
            if (dateDuration == null)
            {
                throw new ArgumentNullException("dateDuration");
            }

            // There is no way to detect if there is time or not
            if (dateDuration.Date.Offset == TimeSpan.Zero)
            {
                if (dateDuration.Date.TimeOfDay == TimeSpan.Zero)
                {
                    return string.Format(CultureInfo.InvariantCulture, "{0:" + Daily.SdmxDateTimeFormat + "}/{1}", dateDuration.Date, dateDuration.Duration);
                }

                return string.Format(CultureInfo.InvariantCulture, "{0:" + Hourly.SdmxDateTimeFormat + "}/{1}", dateDuration.Date, dateDuration.Duration);
            }

            return string.Format(CultureInfo.InvariantCulture, "{0:" + Hourly.SdmxDateTimeFormatOffset + "}/{1}", dateDuration.Date, dateDuration.Duration);
        }
    }
}