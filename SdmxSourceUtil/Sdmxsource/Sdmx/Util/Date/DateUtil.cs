// -----------------------------------------------------------------------
// <copyright file="DateUtil.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Date
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The date util.
    /// </summary>
    public static class DateUtil
    {
        /// <summary>
        ///     Gets the date format.
        /// </summary>
        public static string DateFormat
        {
            get
            {
                return Daily.SdmxDateTimeFormat;
            }
        }

        /// <summary>
        ///     Gets a DateFormat object that can be used to format a date
        ///     string as a date object.
        ///     <p />
        ///     For a String to be successfully formatted as a Date using the
        ///     returned DateFormat object, it must conform to the ISO8601 Standard Date/Time format.
        /// </summary>
        /// <value> DateFormat object </value>
        public static string DateTimeFormat
        {
            get
            {
                // ISO8601 Standard Date/Time format
                return Hourly.SdmxDateTimeFormat;
            }
        }

        /// <summary>
        ///     Gets a DateFormat object that can be used to format a date
        ///     string as a UTC date object.
        ///     <p />
        ///     For a String to be successfully formatted as a Date using the
        ///     returned DateFormat object, it must conform to the ISO8601 Standard Date/Time format in UTC.
        /// </summary>
        /// <value> DateFormat object </value>
        public static string DateTimeFormatZ
        {
            get
            {
                // ISO8601 Standard Date/Time format
                return Hourly.SdmxDateTimeFormatZ;
            }
        }

        /// <summary>
        ///     Gets the date time string now.
        /// </summary>
        public static string DateTimeStringNow
        {
            get
            {
                return DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss.SSS", System.Globalization.CultureInfo.InvariantCulture);
            }
        }

        /// <summary>
        ///     Gets a DateFormat object that can be used to format a date
        ///     string as a date object. The hour only format: "yyyy-MM-dd'T'HH"
        ///     <p />
        ///     For a String to be successfully formatted as a Date using the
        ///     returned DateFormat object, it must conform to the ISO8601 Standard Date/Time format.
        /// </summary>
        /// <value> DateFormat object </value>
        public static string HourDateTimeFormat
        {
            get
            {
                // ISO8601 Standard Date/Time format
                return Hourly.SdmxHourDateTimeFormat;
            }
        }

        /// <summary>
        ///     Gets a DateFormat object that can be used to format a date
        ///     string as a date object. The hour:minute format: "yyyy-MM-dd'T'HH:mm"
        ///     <p />
        ///     For a String to be successfully formatted as a Date using the
        ///     returned DateFormat object, it must conform to the ISO8601 Standard Date/Time format.
        /// </summary>
        /// <value> DateFormat object </value>
        public static string HourMinuteDateTimeFormat
        {
            get
            {
                // ISO8601 Standard Date/Time format
                return Hourly.SdmxHourMinuteDateTimeFormat;
            }
        }

        /// <summary>
        ///     Gets the month format.
        /// </summary>
        public static string MonthFormat
        {
            get
            {
                return Monthly.SdmxDateTimeFormat;
            }
        }

        /// <summary>
        ///     Gets the year format.
        /// </summary>
        public static string YearFormat
        {
            get
            {
                return Annual.SdmxDateTimeFormat;
            }
        }

        /// <summary>
        ///     Creates a list of all the date values between the from and to dates.  The dates are iterated by the
        ///     time format.  The format of the dates is also dependant on the time format.
        /// </summary>
        /// <param name="dateFrom">
        ///     Date From
        /// </param>
        /// <param name="dateTo">
        ///     Date To
        /// </param>
        /// <param name="format">
        ///     Date Format
        /// </param>
        /// <returns>
        ///     The <see cref="IList{String}" />.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="format"/> is <see langword="null" />.</exception>
        public static IList<string> CreateTimeValues(DateTime dateFrom, DateTime dateTo, TimeFormat format)
        {
            if (format == null)
            {
                throw new ArgumentNullException("format");
            }

            IList<string> returnList = new List<string>();

            switch (format.EnumType)
            {
                case TimeFormatEnumType.DateTime:
                    throw new SdmxNotImplementedException(ExceptionCode.Unsupported, format);

                // FUNC Support iteration of date time    
                case TimeFormatEnumType.Date:
                case TimeFormatEnumType.HalfOfYear:

                case TimeFormatEnumType.Hour:

                case TimeFormatEnumType.Month:

                case TimeFormatEnumType.QuarterOfYear:

                case TimeFormatEnumType.ThirdOfYear:

                case TimeFormatEnumType.Week:

                case TimeFormatEnumType.Year:
                case TimeFormatEnumType.ReportingYear:
                case TimeFormatEnumType.ReportingMonth:
                case TimeFormatEnumType.ReportingDay:
                    IterateDateValues(dateFrom, dateTo, returnList, format.EnumType);
                    break;

                default:
                    throw new SdmxException("Unsupported time format : " + format);
            }

            return returnList;
        }

        /// <summary>
        ///     Formats a String as a date.
        ///     <p />
        ///     The expected string must be in either of the two following formats.
        ///     <ol>
        ///         <li>yyyy-MM-dd'T'HH:mm:ss.SSSz</li>
        ///         <li>yyyy-MM-dd</li>
        ///     </ol>
        /// </summary>
        /// <param name="dateObject">
        ///     Date object
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="dateObject" />
        ///     is null
        /// </exception>
        /// <returns>
        ///     The <see cref="DateTime" />
        /// </returns>
        [Obsolete("This method returns the end period but it is not clear. Use FormatDate(object,bool)")]
        public static DateTime FormatDate(object dateObject)
        {
            return FormatDate(dateObject, false);
        }

        /// <summary>
        ///     Formats a String as a date.
        ///     <p />
        ///     The expected string must be in either of the two following formats.
        ///     <ol>
        ///         <li>yyyy-MM-dd'T'HH:mm:ss.SSSz</li>
        ///         <li>yyyy-MM-dd</li>
        ///     </ol>
        /// </summary>
        /// <param name="dateObject">
        ///     Date object
        /// </param>
        /// <param name="startOfPeriod">
        ///     The start Of Period.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="dateObject" />
        ///     is null
        /// </exception>
        /// <returns>
        ///     The <see cref="DateTime" />.
        /// </returns>
        public static DateTime FormatDate(object dateObject, bool startOfPeriod)
        {
            if (dateObject == null)
            {
                throw new ArgumentNullException("dateObject");
            }

            if (dateObject is DateTime)
            {
                return (DateTime)dateObject;
            }

            if (dateObject is DateTimeOffset)
            {
                return ((DateTimeOffset)dateObject).UtcDateTime;
            }

            var dateString = dateObject as string;

            if (dateString == null)
            {
                throw new ArgumentException("Date type not recognised : " + dateObject.GetType().FullName);
            }

            if (dateString.Length == 0)
            {
                return DateTime.MinValue;
            }

            return FormatDate(dateString, startOfPeriod);
        }

        /// <summary>
        ///     Formats a String as a date.
        ///     <p />
        ///     The expected string must be in either of the two following formats.
        ///     <ol>
        ///         <li>yyyy-MM-dd'T'HH:mm:ss.SSSz</li>
        ///         <li>yyyy-MM-dd</li>
        ///     </ol>
        /// </summary>
        /// <param name="dateObject">
        ///     Date object
        /// </param>
        /// <param name="startOfPeriod">
        ///     The start Of Period.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="dateObject" />
        ///     is null
        /// </exception>
        /// <returns>
        ///     The <see cref="DateTime" />.
        /// </returns>
        public static DateTimeOffset FormatDateWithOffSet(object dateObject, bool startOfPeriod)
        {
            if (dateObject == null)
            {
                throw new ArgumentNullException("dateObject");
            }

            if (dateObject is DateTime)
            {
                return new DateTimeOffset((DateTime)dateObject);
            }

            if (dateObject is DateTimeOffset)
            {
                return (DateTimeOffset)dateObject;
            }

            var dateTimeOffsetDuration = dateObject as DateTimeOffsetDuration;
            if (dateTimeOffsetDuration != null)
            {
                return dateTimeOffsetDuration.Date;
            }

            var dateString = dateObject as string;

            if (dateString == null)
            {
                throw new ArgumentException("Date type not recognised : " + dateObject.GetType().FullName);
            }

            if (dateString.Length == 0)
            {
                return DateTime.MinValue;
            }

            return FormatDateOffSet(dateString, startOfPeriod);
        }

        /// <summary>
        ///     The format date.
        /// </summary>
        /// <param name="dt">
        ///     The dt.
        /// </param>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        public static string FormatDate(DateTime dt)
        {
            return dt.ToString(DateTimeFormat, CultureInfo.InvariantCulture);
        }

        /// <summary>
        ///     The format date.
        /// </summary>
        /// <param name="dt">
        ///     The dt.
        /// </param>
        /// <param name="convertToUTC">
        ///     Convert to UTC.
        /// </param>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        public static string FormatDateUTC(DateTime dt, bool convertToUTC = true)
        {
            return convertToUTC
                ? dt.ToUniversalTime().ToString(DateTimeFormatZ, CultureInfo.InvariantCulture) 
                : dt.ToString(DateTimeFormatZ, CultureInfo.InvariantCulture);
        }

        /// <summary>
        ///     The format date.
        /// </summary>
        /// <param name="dt">
        ///     The dt.
        /// </param>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        public static string FormatDate(DateTimeOffset dt)
        {
            return dt.ToString(Hourly.SdmxDateTimeFormatOffset, CultureInfo.InvariantCulture);
        }

        /// <summary>
        ///     Return an SDMX-ML formatted time period, based on the frequency supplied. The value
        ///     returned from this method will be determined based on the frequency and the date value
        ///     supplied as follows:
        ///     Annual: The year of the date
        ///     Biannual: January 1 - June 30 = 1st half, July 1 - December 31 = 2nd half
        ///     Triannual: January 1 - April 30 = 1st third, May 1 - August 30 = 2nd third, September 1 - December 31 = 3rd third
        ///     Quarterly: January 1 - March 31 = 1st quarter, April 1 - June 30 = 2nd quarter, July 1 - September 30 = 3rd
        ///     quarter, October 1 - December 31 = 4th quarter
        ///     Mothly: The month of the date
        ///     Weekly: The week of the year according to ISO 8601.
        ///     Daily: The date only
        ///     Hourly: The full date time representation.
        /// </summary>
        /// <param name="date">
        ///     the date to convert
        /// </param>
        /// <param name="format">
        ///     The format.
        /// </param>
        /// <returns>
        ///     The date in SDMX-ML <c>TimePeriodType</c> format.
        /// </returns>
        public static string FormatDate(DateTime date, TimeFormatEnumType format)
        {
            IPeriodicity periodicity = PeriodicityFactory.Create(format);
            return periodicity.ToString(date);
        }

        /// <summary>
        /// Return an SDMX-ML formatted time period, based on the time format supplied.
        /// </summary>
        /// <param name="dateOffsetDuration">Duration of the date offset.</param>
        /// <param name="timeFormat">The time format.</param>
        /// <returns>The date in SDMX-ML <c>TimePeriodType</c> format.</returns>
        public static string FormatDate(DateTimeOffsetDuration dateOffsetDuration, TimeFormat timeFormat)
        {
            IPeriodicity periodicity = PeriodicityFactory.Create(timeFormat);
            return periodicity.ToString(dateOffsetDuration);
        }

        /// <summary>
        ///   Return an SDMX-ML formatted time period, based on the duration supplied.
        /// </summary>
        /// <param name="dateOffsetDuration">Duration of the date offset.</param>
        /// <returns>
        ///     The date in SDMX-ML <c>TimePeriodType</c> format.
        /// </returns>
        public static string FormatDate(DateTimeOffsetDuration dateOffsetDuration)
        {
            if (dateOffsetDuration == null)
            {
                throw new ArgumentNullException("dateOffsetDuration");
            }

            var isoDuration = dateOffsetDuration.Duration.ToString();
            var timeFormat = TimeFormat.Values.FirstOrDefault(x => isoDuration.Equals(x.IsoDuration));
            if (timeFormat == null)
            {
                timeFormat = TimeFormat.TimeRange;
            }

            return FormatDate(dateOffsetDuration, timeFormat);
        }

        /// <summary>
        ///     Return an SDMX-ML formatted time period, based on the frequency supplied. The value
        ///     returned from this method will be determined based on the frequency and the date value
        ///     supplied as follows:
        ///     Annual: The year of the date
        ///     Biannual: January 1 - June 30 = 1st half, July 1 - December 31 = 2nd half
        ///     Triannual: January 1 - April 30 = 1st third, May 1 - August 30 = 2nd third, September 1 - December 31 = 3rd third
        ///     Quarterly: January 1 - March 31 = 1st quarter, April 1 - June 30 = 2nd quarter, July 1 - September 30 = 3rd
        ///     quarter, October 1 - December 31 = 4th quarter
        ///     Mothly: The month of the date
        ///     Weekly: The week of the year according to ISO 8601.
        ///     Daily: The date only
        ///     Hourly: The full date time representation.
        /// </summary>
        /// <param name="date">
        ///     the date to convert
        /// </param>
        /// <param name="format">
        ///     The format.
        /// </param>
        /// <returns>
        ///     The date in SDMX-ML <c>TimePeriodType</c> format.
        /// </returns>
        public static string FormatDate(DateTimeOffset date, TimeFormatEnumType format)
        {
            IPeriodicity periodicity = PeriodicityFactory.Create(format);
            return periodicity.ToString(date);
        }

        /// <summary>
        /// Formats a SDMX Period as <see cref="DateTimeOffsetDuration" />.
        /// </summary>
        /// <param name="sdmxPeriod">The SDMX period.</param>
        /// <returns>The <see cref="DateTimeOffsetDuration" />.</returns>
        public static DateTimeOffsetDuration FormatDateOffSetDuration(string sdmxPeriod)
        {
            TimeFormat timeFormat = GetTimeFormatOfDate(sdmxPeriod);
            return PeriodicityFactory.Create(timeFormat.EnumType).ToDateTimeOffsetDuration(sdmxPeriod);
        }

        /// <summary>
        /// Normalize a ISO 8601 time interval, <c>duration/end</c> to the SDMX support subset <c>start/duration</c>
        /// Note only the XML Schema Duration subset is supported
        /// </summary>
        /// <param name="endTime">
        /// The end time.
        /// </param>
        /// <param name="duration">
        /// The duration.
        /// </param>
        /// <returns>
        /// The <see cref="DateTimeOffsetDuration"/>.
        /// </returns>
        public static DateTimeOffsetDuration NormalizeTimeRange(DateTimeOffset endTime, TimeDuration duration)
        {
            var startDate = endTime
                .AddYears(-duration.Years)
                .AddMonths(-duration.Months)
                .AddDays(-duration.Days)
                .AddHours(-duration.Hours)
                .AddMinutes(-duration.Minutes)
                .AddSeconds(-duration.Seconds)
                .AddMilliseconds(-duration.Milliseconds);
            return new DateTimeOffsetDuration(startDate, duration);
        }

        /// <summary>
        /// Normalize a ISO 8601 time interval <c>start/end</c> to the SDMX support subset <c>start/duration</c>
        /// Note only the XML Schema Duration subset is supported
        /// </summary>
        /// <param name="startDate">
        /// The start Date.
        /// </param>
        /// <param name="endTime">
        /// The end Time.
        /// </param>
        /// <returns>
        /// The <see cref="DateTimeOffsetDuration"/>
        /// </returns>
        public static DateTimeOffsetDuration NormalizeTimeRange(DateTimeOffset startDate, DateTimeOffset endTime)
        {
            var diff = endTime - startDate;
           
            if (diff < TimeSpan.Zero)
            {
                throw new SdmxSemmanticException("End date is before start date");
            }

            if (diff.Days < 30)
            {
                return new DateTimeOffsetDuration(startDate, new TimeDuration(0, 0, diff));
            }

            int years = endTime.Year - startDate.Year;
            int months = 0;
            if (years > 0)
            {
                if (endTime.Month > startDate.Month)
                {
                    months = endTime.Month - startDate.Month;
                }
                else if (endTime.Month < startDate.Month)
                {
                    years--;
                    months = startDate.Month - endTime.Month;
                }
            }
            else if (years == 0)
            {
                months = endTime.Month - startDate.Month;
            }

            if (months > 0)
            {
                if (endTime.Day < startDate.Day)
                {
                    months--;
                }
            }

            var sameMonthDays = startDate.AddYears(years).AddMonths(months);
            var normDiff = endTime - sameMonthDays;

            return new DateTimeOffsetDuration(startDate, new TimeDuration(years, months, normDiff));
        }

        /// <summary>
        /// Gets a value indicating whethere the provided time format can correspond to a SDMX v2.1 Reporting period.
        /// </summary>
        /// <param name="timeFormat">The time format</param>
        /// <returns><c>true</c> if the provided time format can correspond to a SDMX v2.1 Reporting period; otherwise <c>false</c>.</returns>
        public static bool IsReportingPeriod(TimeFormat timeFormat)
        {
            if (timeFormat == null)
            {
                throw new ArgumentNullException("timeFormat");
            }

            switch (timeFormat.EnumType)
            {
                case TimeFormatEnumType.ReportingDay:
                case TimeFormatEnumType.ReportingMonth:
                case TimeFormatEnumType.ReportingYear:
                case TimeFormatEnumType.QuarterOfYear:
                case TimeFormatEnumType.ThirdOfYear:
                case TimeFormatEnumType.HalfOfYear:
                case TimeFormatEnumType.Week:
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Determines whether the specified <paramref name="dateTime"/> with format <paramref name="timeFormat"/> is gregorian compatible.
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <param name="timeFormat">The time format.</param>
        /// <returns><c>true</c> if [is gregorian compatible] [the specified date time]; otherwise, <c>false</c>.</returns>
        /// <exception cref="ArgumentNullException">timeFormat</exception>
        public static bool IsGregorianCompatible(DateTimeOffset dateTime, TimeFormat timeFormat)
        {
            if (timeFormat == null)
            {
                throw new ArgumentNullException("timeFormat");
            }

            IPeriodicity periodicity = PeriodicityFactory.Create(timeFormat);

            switch(timeFormat.EnumType)
            {
                case TimeFormatEnumType.ReportingYear:
                case TimeFormatEnumType.Year:
                    if (dateTime.Month != 1 || dateTime.Day != 1)
                    {
                        return false;
                    }
                    break;
                case TimeFormatEnumType.ReportingMonth:
                case TimeFormatEnumType.Month:
                    if (dateTime.Day != 1)
                    {
                        return false;
                    }
                    break;
                case TimeFormatEnumType.Week:
                    if (dateTime.DayOfWeek != DayOfWeek.Monday)
                    {
                        return false;
                    }

                    break;
                case TimeFormatEnumType.QuarterOfYear:
                    if (dateTime.Day != 1 || !dateTime.Month.IsOneOf(1, 4, 7, 10))
                    {
                        return false;
                    }

                    break;
                case TimeFormatEnumType.HalfOfYear:
                    if (dateTime.Day != 1 || !dateTime.Month.IsOneOf(1, 6))
                    {
                        return false;
                    }

                    break;
                case TimeFormatEnumType.ThirdOfYear:
                    if (dateTime.Day != 1 || !dateTime.Month.IsOneOf(1, 5, 9))
                    {
                        return false;
                    }

                    break;
            }

            return true;
        }

        /// <summary>
        /// Normalize a ISO 8601 time interval, <c>start/end</c> or <c>duration/end</c> to the SDMX support subset <c>start/duration</c>
        /// Note only the XML Schema Duration subset is supported
        /// </summary>
        /// <param name="timeRange">The ISO 8601 time range. It must be one of </param>
        /// <returns>The <see cref="DateTimeOffsetDuration"/></returns>
        public static DateTimeOffsetDuration NormalizeTimeRange(string timeRange)
        {
            if (string.IsNullOrWhiteSpace(timeRange))
            {
                throw new ArgumentException("This cannot be null or empty", "timeRange");
            }

            var index = timeRange.IndexOf('/');
            if (index == -1)
            {
                throw new SdmxSemmanticException("Provided time range doesn't appear to be valid. There is no '/'");
            }

            IPeriodicity timeRangePeriodicity = PeriodicityFactory.Create(TimeFormat.TimeRange);
            if (timeRangePeriodicity.IsValidPeriod(timeRange))
            {
                return timeRangePeriodicity.ToDateTimeOffsetDuration(timeRange);
            }
            
            // We check for either Pduration/endDate or startDate/endDate
            var dateTime = PeriodicityFactory.Create(TimeFormat.DateTime);

            var endDateVal = timeRange.Substring(index + 1);
            var endDate = dateTime.ToDateTimeOffSet(endDateVal, false);
            if (timeRange[0] == 'P')
            {
                var isoDuration = timeRange.Substring(0, index);
                var duration = TimeDuration.Parse(isoDuration);
                return NormalizeTimeRange(endDate, duration);
            }

            var startDateVal = timeRange.Substring(0, index);
            var startDate = dateTime.ToDateTimeOffSet(startDateVal, false);
            return NormalizeTimeRange(startDate, endDate);
        }

        /// <summary>
        ///     Gets the date formatter.
        /// </summary>
        /// <param name="pattern">The pattern.</param>
        /// <returns>The date format.</returns>
        /// <remarks>
        ///     Note the <see cref="DateFormat" /> is a very simple port from JDK DateFormat because no such class exists in
        ///     .NET framework. It only supports Parse and Format.
        /// </remarks>
        public static DateFormat GetDateFormatter(string pattern)
        {
            return new DateFormat(pattern);
        }

        /// <summary>
        /// Validates the date is in SDMX date/time format, and returns the Time Format for the date
        /// </summary>
        /// <param name="dateStr">
        /// Date string
        /// </param>
        /// <param name="version">
        /// The version.
        /// </param>
        /// <returns>
        /// The <see cref="TimeFormat"/>.
        /// </returns>
        /// <exception cref="SdmxSemmanticException">
        /// if the data format is invalid, with regards to the allowed SDMX date formats
        /// </exception>
        public static TimeFormat GetTimeFormatOfDate(string dateStr, SdmxSchema version)
        {
            var format = GetTimeFormatOfDate(dateStr);
            var isTwoZero = version.EnumType == SdmxSchemaEnumType.VersionTwo;
            switch (format.EnumType)
            {
                case TimeFormatEnumType.HalfOfYear:
                    if (isTwoZero)
                    {
                        if (dateStr[5] != 'B')
                        {
                            throw new SdmxSemmanticException(ExceptionCode.InvalidDateFormat, dateStr);
                        }
                    }
                    else
                    {
                        if (dateStr[5] != 'S')
                        {
                            throw new SdmxSemmanticException(ExceptionCode.InvalidDateFormat, dateStr);
                        }
                    }

                    break;
                case TimeFormatEnumType.ReportingMonth:
                case TimeFormatEnumType.ReportingYear:
                case TimeFormatEnumType.ReportingDay:
                case TimeFormatEnumType.TimeRange:

                    if (isTwoZero)
                    {
                        throw new SdmxSemmanticException(ExceptionCode.InvalidDateFormat, dateStr);
                    }

                    break;
                case TimeFormatEnumType.Week:
                    if (!isTwoZero && dateStr.Length == 7)
                    {
                        throw new SdmxSemmanticException(ExceptionCode.InvalidDateFormat, dateStr);
                    }

                    break;
            }

            return format;
        }

        /// <summary>
        ///     Validates the date is in SDMX date/time format, and returns the Time Format for the date
        /// </summary>
        /// <param name="dateStr">
        ///     Date string
        /// </param>
        /// <returns>
        ///     The <see cref="TimeFormat" />.
        /// </returns>
        /// <exception cref="SdmxSemmanticException">
        ///     if the data format is invalid, with regards to the allowed SDMX date formats
        /// </exception>
        public static TimeFormat GetTimeFormatOfDate(string dateStr)
        {
            if (dateStr == null)
            {
                throw new ArgumentException("Could not determine date format, date null");
            }

            if (dateStr.Length < 4)
            {
                throw new SdmxSemmanticException(ExceptionCode.InvalidDateFormat, dateStr);
            }

            foreach (var timeFormat in TimeFormat.Values)
            {
                if (PeriodicityFactory.Create(timeFormat).IsValidPeriod(dateStr))
                {
                    return timeFormat;
                }
            }

            throw new SdmxSemmanticException(ExceptionCode.InvalidDateFormat, dateStr);
        }

        /// <summary>
        ///     Moves to end of the specified period.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="timeFormat">The time format.</param>
        /// <returns>the date at the end</returns>
        public static DateTime MoveToEndofPeriod(DateTime date, TimeFormat timeFormat)
        {
            string dateAsString = FormatDate(date, timeFormat);
            return FormatDateEndPeriod(dateAsString);
        }

        /// <summary>
        ///     The week format.
        /// </summary>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        public static string WeekFormat()
        {
            return Weekly.SdmxDateTimeFormat;
        }

        /// <summary>
        ///   The number of Periods
        /// </summary>
        /// <param name="timeFormat"></param>
        /// <returns></returns>
        public static int GetNumberOfPeriods(TimeFormat timeFormat)
        {
            return PeriodicityFactory.Create(timeFormat).PeriodCount;
        }

        /// <summary>
        ///     The format date.
        /// </summary>
        /// <param name="valueRen">
        ///     The value ren.
        /// </param>
        /// <param name="startOfPeriod">
        ///     The start of period.
        /// </param>
        /// <returns>
        ///     The <see cref="DateTime" />.
        /// </returns>
        private static DateTime FormatDate(string valueRen, bool startOfPeriod)
        {
            TimeFormat timeFormat = GetTimeFormatOfDate(valueRen);
            return PeriodicityFactory.Create(timeFormat.EnumType).ToDateTime(valueRen, startOfPeriod);
        }

        /// <summary>
        ///     The format date.
        /// </summary>
        /// <param name="valueRen">
        ///     The value ren.
        /// </param>
        /// <param name="startOfPeriod">
        ///     The start of period.
        /// </param>
        /// <returns>
        ///     The <see cref="DateTime" />.
        /// </returns>
        private static DateTimeOffset FormatDateOffSet(string valueRen, bool startOfPeriod)
        {
            TimeFormat timeFormat = GetTimeFormatOfDate(valueRen);
            return PeriodicityFactory.Create(timeFormat.EnumType).ToDateTimeOffSet(valueRen, startOfPeriod);
        }

        /// <summary>
        ///     Formats the date end period.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>The end period</returns>
        private static DateTime FormatDateEndPeriod(string value)
        {
            TimeFormat timeFormat = GetTimeFormatOfDate(value);
            IPeriodicity periodicity = PeriodicityFactory.Create(timeFormat);
            return periodicity.ToDateTime(value, false);
        }

        /// <summary>
        ///     The iterate date values.
        /// </summary>
        /// <param name="dateFrom">
        ///     The date from.
        /// </param>
        /// <param name="dateTo">
        ///     The date to.
        /// </param>
        /// <param name="returnList">
        ///     The return list.
        /// </param>
        /// <param name="format">
        ///     The format.
        /// </param>
        private static void IterateDateValues(
            DateTime dateFrom, 
            DateTime dateTo, 
            ICollection<string> returnList, 
            TimeFormatEnumType format)
        {
            IPeriodicity periodicity = PeriodicityFactory.Create(format);

            DateTime pointer = dateFrom;
            while ((pointer.Ticks / 10000) <= (dateTo.Ticks / 10000))
            {
                returnList.Add(FormatDate(pointer, format));
                pointer = periodicity.AddPeriod(pointer);
            }
        }     
    }
}