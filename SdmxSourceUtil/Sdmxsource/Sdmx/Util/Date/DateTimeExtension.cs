// -----------------------------------------------------------------------
// <copyright file="DateTimeExtension.cs" company="EUROSTAT">
//   Date Created : 2017-11-02
//   Copyright (c) 2012, 2017 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Date
{
    using System;

    /// <summary>
    /// The date time extension.
    /// </summary>
    public static class DateTimeExtension
    {
        /// <summary>
        /// Determines whether the <paramref name="lValue"/> is after <paramref name="rValue"/>.
        /// </summary>
        /// <param name="lValue">The l value.</param>
        /// <param name="rValue">The r value.</param>
        /// <returns>
        ///   <c>true</c> if <paramref name="lValue"/> is after <paramref name="rValue"/>; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsMonthAndDayAfter(this DateTime lValue, DateTime rValue)
        {
            if (lValue.Month > rValue.Month)
            {
                return true;
            }

            if (lValue.Month == rValue.Month)
            {
                return lValue.Day >= rValue.Day;
            }

            return false;
        }

        /// <summary>
        /// The month difference.
        /// </summary>
        /// <param name="lValue">The l value.</param>
        /// <param name="rValue">The r value.</param>
        /// <returns>The number of months</returns>
        public static int MonthDifference(this DateTime lValue, DateTime rValue)
        {
            return (lValue.Month - rValue.Month) + 12 * (lValue.Year - rValue.Year);
        }
    }
}