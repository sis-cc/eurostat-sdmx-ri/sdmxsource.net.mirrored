// -----------------------------------------------------------------------
// <copyright file="BasePeriodicity.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Date
{
    using System;
    using System.Globalization;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;

    /// <summary>
    ///     The base periodicity class.
    /// </summary>
    public abstract class BasePeriodicity
    {
        /// <summary>
        ///     The default calendar instance
        /// </summary>
        private static readonly Calendar _calendar = CultureInfo.InvariantCulture.Calendar;

        /// <summary>
        ///     The time format
        /// </summary>
        private readonly TimeFormat _timeFormat;

        /// <summary>
        ///     Initializes a new instance of the <see cref="BasePeriodicity" /> class.
        /// </summary>
        /// <param name="timeFormat">
        ///     The time format.
        /// </param>
        protected BasePeriodicity(TimeFormatEnumType timeFormat)
        {
            this._timeFormat = TimeFormat.GetFromEnum(timeFormat);
        }

        /// <summary>
        ///     Gets the time format
        /// </summary>
        public TimeFormat TimeFormat
        {
            get
            {
                return this._timeFormat;
            }
        }

        /// <summary>
        ///     Gets the default calendar instance
        /// </summary>
        protected static Calendar Calendar
        {
            get
            {
                return _calendar;
            }
        }

        /// <summary>
        ///     Convert an SDMX Time Period type to a System.DateTime object
        /// </summary>
        /// <param name="sdmxPeriod">
        ///     A string with the SDMX Time period
        /// </param>
        /// <param name="start">
        ///     If it is true it will expand to the start of the period else towards the end e.g. if it is true 2001-04 will become
        ///     2001-04-01 else it will become 2001-04-30
        /// </param>
        /// <returns>
        ///     A DateTime object
        /// </returns>
        public abstract DateTimeOffset ToDateTimeOffSet(string sdmxPeriod, bool start);

        /// <summary>
        ///     Convert the specified <paramref name="time" /> to SDMX Time Period type representation
        /// </summary>
        /// <param name="time">
        ///     The <see cref="DateTime" /> object to convert
        /// </param>
        /// <returns>
        ///     A string with the SDMX Time Period
        /// </returns>
        public string ToString(DateTime time)
        {
            if (time.Kind == DateTimeKind.Local)
            {
                return this.ToString(new DateTimeOffset(time));
            }

            return this.ToString(new DateTimeOffset(time, TimeSpan.Zero));
        }

        /// <summary>
        ///     Convert the specified <paramref name="time" /> to SDMX Time Period type representation
        /// </summary>
        /// <param name="time">
        ///     The <see cref="DateTime" /> object to convert
        /// </param>
        /// <returns>
        ///     A string with the SDMX Time Period
        /// </returns>
        public abstract string ToString(DateTimeOffset time);

        /// <summary>
        /// Converts the specified period to a Date Time with duration.
        /// </summary>
        /// <param name="sdmxPeriod">The SDMX period.</param>
        /// <returns>The <see cref="DateTimeOffsetDuration" />.</returns>
        public DateTimeOffsetDuration ToDateTimeOffsetDuration(string sdmxPeriod)
        {
            if (string.IsNullOrEmpty(this._timeFormat.IsoDuration))
            {
                return null;
            }

            var duration = TimeDuration.Parse(this._timeFormat.IsoDuration);
            var date = this.ToDateTimeOffSet(sdmxPeriod, true);
            return new DateTimeOffsetDuration(date, duration);
        }
        
        /// <summary>
        ///     Convert an SDMX Time Period type to a System.DateTime object
        /// </summary>
        /// <param name="sdmxPeriod">
        ///     A string with the SDMX Time period
        /// </param>
        /// <param name="start">
        ///     If it is true it will expand to the start of the period else towards the end e.g. if it is true 2001-04 will become
        ///     2001-04-01 else it will become 2001-04-30
        /// </param>
        /// <returns>
        ///     A DateTime object
        /// </returns>
        public DateTime ToDateTime(string sdmxPeriod, bool start)
        {
            return this.ToDateTimeOffSet(sdmxPeriod, start).UtcDateTime;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <param name="dateDuration">Duration of the date.</param>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public string ToString(DateTimeOffsetDuration dateDuration)
        {
            if (dateDuration == null)
            {
                throw new ArgumentNullException("dateDuration");
            }

            if (!string.IsNullOrEmpty(this._timeFormat.IsoDuration))
            {
                if (dateDuration.Duration.ToString().Equals(this._timeFormat.IsoDuration))
                {
                    return this.ToString(dateDuration.Date);
                }
            }

            throw new SdmxNotImplementedException("Periodicity is not a Time Range, the duration must match the time formats duration");
        }

        /// <summary>
        /// Ensure the <paramref name="time"/> falls at the start of the (next) year
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        protected DateTimeOffset EnsureIncludesWholeYear(DateTimeOffset time)
        {
            if (time.DayOfYear > 1)
            {
                time = new DateTimeOffset(time.Year + 1, 1, 1, 0, 0, 0, 0, Calendar, time.Offset);
            }

            return time;
        }

        /// <summary>
        /// Enusre that <paramref name="time"/> begings at the (next <paramref name="month"/>) month
        /// </summary>
        /// <param name="time"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        protected static DateTimeOffset EnsureBeginingOfMonth(DateTimeOffset time, int month)
        {
            if (time.Day > 1)
            {
                time = time.AddMonths(month);
            }

            return time;
        }
    }
}