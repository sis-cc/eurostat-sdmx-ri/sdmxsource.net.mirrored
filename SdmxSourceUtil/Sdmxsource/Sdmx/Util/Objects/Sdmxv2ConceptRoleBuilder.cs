﻿// -----------------------------------------------------------------------
// <copyright file="Sdmxv2ConceptRoleBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-06-08
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Util.Objects
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// The sdmxv 2 concept role builder.
    /// </summary>
    public class Sdmxv2ConceptRoleBuilder : IBuilder<ComponentRole, IStructureReference>, IBuilder<IStructureReference, ComponentRole>
    {
        /// <summary>
        /// The agency id.
        /// </summary>
        private const string AgencyId = "ESTAT";

        /// <summary>
        /// The maintainable id.
        /// </summary>
        private const string MaintainableId = "COMPONENT_ROLES";

        /// <summary>
        /// The version.
        /// </summary>
        private const string Version = "1.0";

        /// <summary>
        /// The _map.
        /// </summary>
        private readonly IDictionary<ComponentRole, IStructureReference> _map;

        /// <summary>
        /// The _reverse map.
        /// </summary>
        private readonly IDictionary<IStructureReference, ComponentRole> _reverseMap;

        /// <summary>
        /// Initializes a new instance of the <see cref="Sdmxv2ConceptRoleBuilder"/> class.
        /// </summary>
        /// <param name="map">
        /// The map.
        /// </param>
        public Sdmxv2ConceptRoleBuilder(IDictionary<ComponentRole, IStructureReference> map)
        {
            if (map == null)
            {
                throw new ArgumentNullException("map");
            }

            this._map = map;
            this._reverseMap = map.ToDictionary(pair => pair.Value, pair => pair.Key);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Sdmxv2ConceptRoleBuilder"/> class.
        /// </summary>
        public Sdmxv2ConceptRoleBuilder()
            : this(BuildMap())
        {
        }

        /// <summary>
        /// Builds an object of type <see cref="ComponentRole"/>
        /// </summary>
        /// <param name="buildFrom">An Object to build the output object from</param>
        /// <returns>
        /// Object of type <see cref="ComponentRole"/>
        /// </returns>
        public ComponentRole Build(IStructureReference buildFrom)
        {
            ComponentRole structureReference;
            if (this._reverseMap.TryGetValue(buildFrom, out structureReference))
            {
                return structureReference;
            }

            return ComponentRole.None;
        }

        /// <summary>
        /// Builds an object of type <see cref="IStructureReference"/>
        /// </summary>
        /// <param name="buildFrom">An Object to build the output object from</param>
        /// <returns>
        /// Object of type <see cref="IStructureReference"/>
        /// </returns>
        public IStructureReference Build(ComponentRole buildFrom)
        {
            IStructureReference structureReference;
            if (this._map.TryGetValue(buildFrom, out structureReference))
            {
                return structureReference;
            }

            return null;
        }

        /// <summary>
        /// Builds the map.
        /// </summary>
        /// <returns>Build the map from <see cref="ComponentRole"/></returns>
        private static Dictionary<ComponentRole, IStructureReference> BuildMap()
        {
            Dictionary<ComponentRole, IStructureReference> dictionary = new Dictionary<ComponentRole, IStructureReference>();
            foreach (var roles in Enum.GetValues(typeof(ComponentRole)).Cast<ComponentRole>())
            {
                if (roles == ComponentRole.NonObservationalTime)
                {
                    dictionary.Add(roles, new StructureReferenceImpl(AgencyId, MaintainableId, Version, SdmxStructureEnumType.Concept, "NON_OBS_TIME"));
                }
                else if (roles != ComponentRole.None)
                {
                    dictionary.Add(roles, new StructureReferenceImpl(AgencyId, MaintainableId, Version, SdmxStructureEnumType.Concept, roles.ToString().ToUpperInvariant()));
                }
            }

            return dictionary;
        }
    }
}