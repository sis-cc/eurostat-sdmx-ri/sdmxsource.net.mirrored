﻿// -----------------------------------------------------------------------
// <copyright file="SuperObjects.cs" company="EUROSTAT">
//   Date Created : 2013-05-31
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Objects.Container
{
    #region Using directives

    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Registry;
    using Org.Sdmxsource.Util.Extensions;

    #endregion

    /// <summary>
    ///     SuperObjects class
    /// </summary>
    public class SuperObjects : ISuperObjects
    {
        /// <summary>
        ///     The _categorySchemes
        /// </summary>
        private readonly ISet<ICategorySchemeSuperObject> _categorySchemes = new HashSet<ICategorySchemeSuperObject>();

        /// <summary>
        ///     The _codelists
        /// </summary>
        private readonly ISet<ICodelistSuperObject> _codelists = new HashSet<ICodelistSuperObject>();

        /// <summary>
        ///     The _conceptSchemes
        /// </summary>
        private readonly ISet<IConceptSchemeSuperObject> _conceptSchemes = new HashSet<IConceptSchemeSuperObject>();

        /// <summary>
        ///     The _dataflows
        /// </summary>
        private readonly ISet<IDataflowSuperObject> _dataflows = new HashSet<IDataflowSuperObject>();

        /// <summary>
        ///     The _dataStructures
        /// </summary>
        private readonly ISet<IDataStructureSuperObject> _dataStructures = new HashSet<IDataStructureSuperObject>();

        /// <summary>
        ///     The _hcls
        /// </summary>
        private readonly ISet<IHierarchicalCodelistSuperObject> _hcls = new HashSet<IHierarchicalCodelistSuperObject>();

        /// <summary>
        ///     The _processes
        /// </summary>
        private readonly ISet<IProcessSuperObject> _processes = new HashSet<IProcessSuperObject>();

        /// <summary>
        ///     The _provisionAgreement
        /// </summary>
        private readonly ISet<IProvisionAgreementSuperObject> _provisionAgreement =
            new HashSet<IProvisionAgreementSuperObject>();

        /// <summary>
        ///     The _registrations
        /// </summary>
        private readonly ISet<IRegistrationSuperObject> _registrations = new HashSet<IRegistrationSuperObject>();

        /// <summary>
        ///     Initializes a new instance of the <see cref="SuperObjects" /> class.
        /// </summary>
        public SuperObjects()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SuperObjects" /> class.
        /// </summary>
        /// <param name="maintainables">
        ///     The maintainables.
        /// </param>
        public SuperObjects(IEnumerable<IMaintainableSuperObject> maintainables)
        {
            if (maintainables != null)
            {
                foreach (IMaintainableSuperObject currentMaintainable in maintainables)
                {
                    this._categorySchemes.AddIfOfType(currentMaintainable);
                    this._codelists.AddIfOfType(currentMaintainable);
                    this._conceptSchemes.AddIfOfType(currentMaintainable);
                    this._dataflows.AddIfOfType(currentMaintainable);
                    this._hcls.AddIfOfType(currentMaintainable);
                    this._dataStructures.AddIfOfType(currentMaintainable);
                    if (currentMaintainable.BuiltFrom.StructureType == SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ProvisionAgreement))
                    {
                        this._provisionAgreement.AddIfOfType(currentMaintainable);
                    }

                    this._processes.AddIfOfType(currentMaintainable);
                    this._registrations.AddIfOfType(currentMaintainable);
                }
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SuperObjects" /> class.
        /// </summary>
        /// <param name="superObjects">
        ///     The objects base
        /// </param>
        public SuperObjects(params ISuperObjects[] superObjects)
        {
            if (superObjects != null)
            {
                foreach (ISuperObjects currentObjectBase in superObjects)
                {
                    this._categorySchemes.UnionWith(currentObjectBase.CategorySchemes);
                    this._codelists.UnionWith(currentObjectBase.Codelists);
                    this._conceptSchemes.UnionWith(currentObjectBase.ConceptSchemes);
                    this._dataflows.UnionWith(currentObjectBase.Dataflows);
                    this._hcls.UnionWith(currentObjectBase.HierarchicalCodelists);
                    this._dataStructures.UnionWith(currentObjectBase.DataStructures);
                    this._provisionAgreement.UnionWith(currentObjectBase.Provisions);
                    this._processes.UnionWith(currentObjectBase.Processes);
                    this._registrations.UnionWith(currentObjectBase.Registartions);
                }
            }
        }

        /// <summary>
        ///     Gets a <b>copy</b> of the IMaintainableSuperObject Set.  Gets an empty set if no MaintainableBaseObjects are stored
        ///     in this container
        /// </summary>
        /// <value> </value>
        public ISet<IMaintainableSuperObject> AllMaintainables
        {
            get
            {
                ISet<IMaintainableSuperObject> returnSet = new HashSet<IMaintainableSuperObject>();

                returnSet.UnionWith(this._categorySchemes);
                returnSet.UnionWith(this._codelists);
                returnSet.UnionWith(this._conceptSchemes);
                returnSet.UnionWith(this._dataflows);
                returnSet.UnionWith(this._hcls);
                returnSet.UnionWith(this._dataStructures);
                returnSet.UnionWith(this._provisionAgreement);
                returnSet.UnionWith(this._processes);
                returnSet.UnionWith(this._registrations);

                return returnSet;
            }
        }

        /// <summary>
        ///     Gets a <b>copy</b> of the ICategorySchemeSuperObject Set.  Gets an empty set if no CategorySchemeBaseObjects are
        ///     stored in this container
        /// </summary>
        /// <value> </value>
        public ISet<ICategorySchemeSuperObject> CategorySchemes
        {
            get
            {
                return new HashSet<ICategorySchemeSuperObject>(this._categorySchemes);
            }
        }

        /// <summary>
        ///     Gets a <b>copy</b> of the ICodelistSuperObject Set.  Gets an empty set if no CodelistBaseObjects are stored in this
        ///     container
        /// </summary>
        /// <value> </value>
        public ISet<ICodelistSuperObject> Codelists
        {
            get
            {
                return new HashSet<ICodelistSuperObject>(this._codelists);
            }
        }

        /// <summary>
        ///     Gets a <b>copy</b> of the IConceptSchemeSuperObject Set.  Gets an empty set if no ConceptSchemeBaseObjects are
        ///     stored in this container
        /// </summary>
        /// <value> </value>
        public ISet<IConceptSchemeSuperObject> ConceptSchemes
        {
            get
            {
                return new HashSet<IConceptSchemeSuperObject>(this._conceptSchemes);
            }
        }

        /// <summary>
        ///     Gets a <b>copy</b> of the IDataflowSuperObject Set.  Gets an empty set if no DataflowBaseObjects are stored in this
        ///     container
        /// </summary>
        /// <value> </value>
        public ISet<IDataflowSuperObject> Dataflows
        {
            get
            {
                return new HashSet<IDataflowSuperObject>(this._dataflows);
            }
        }

        /// <summary>
        ///     Gets a <b>copy</b> of the KeyFamilyBaseObjects Set.  Gets an empty set if no KeyFamilyBaseObjects are stored in
        ///     this container
        /// </summary>
        /// <value> </value>
        public ISet<IDataStructureSuperObject> DataStructures
        {
            get
            {
                return new HashSet<IDataStructureSuperObject>(this._dataStructures);
            }
        }

        /// <summary>
        ///     Gets a <b>copy</b> of the IHierarchicalCodelistSuperObject Set.  Gets an empty set if no
        ///     HierarchicalCodelistBaseObjects are stored in this container
        /// </summary>
        /// <value> </value>
        public ISet<IHierarchicalCodelistSuperObject> HierarchicalCodelists
        {
            get
            {
                return new HashSet<IHierarchicalCodelistSuperObject>(this._hcls);
            }
        }

        /// <summary>
        ///     Gets a <b>copy</b> of the IProcessSuperObject Set.  Gets an empty set if no ProcessBaseObjects are stored in this
        ///     container
        /// </summary>
        /// <value> </value>
        public ISet<IProcessSuperObject> Processes
        {
            get
            {
                return new HashSet<IProcessSuperObject>(this._processes);
            }
        }

        /// <summary>
        ///     Gets a <b>copy</b> of the IProvisionAgreementSuperObject Set.  Gets an empty set if no
        ///     ProvisionAgreementBaseObjects are stored in this container
        /// </summary>
        /// <value> </value>
        public ISet<IProvisionAgreementSuperObject> Provisions
        {
            get
            {
                return new HashSet<IProvisionAgreementSuperObject>(this._provisionAgreement);
            }
        }

        /// <summary>
        ///     Gets a <b>copy</b> of the IRegistrationSuperObject Set.  Gets an empty set if no RegistrationBaseObjects are stored
        ///     in this container
        /// </summary>
        /// <value> </value>
        public ISet<IRegistrationSuperObject> Registartions
        {
            get
            {
                return new HashSet<IRegistrationSuperObject>(this._registrations);
            }
        }

        /// <summary>
        ///     Add a ICategorySchemeSuperObject to this container, overwrite if one already exists with the same URN
        /// </summary>
        /// <param name="categorySchemeObject">The object. </param>
        public void AddCategoryScheme(ICategorySchemeSuperObject categorySchemeObject)
        {
            if (categorySchemeObject != null)
            {
                this._categorySchemes.Remove(categorySchemeObject);
                this._categorySchemes.Add(categorySchemeObject);
            }
        }

        /// <summary>
        ///     Add a ICodelistSuperObject to this container, overwrite if one already exists with the same URN
        /// </summary>
        /// <param name="codelistObject">Codelist object. </param>
        public void AddCodelist(ICodelistSuperObject codelistObject)
        {
            if (codelistObject != null)
            {
                this._codelists.Remove(codelistObject);
                this._codelists.Add(codelistObject);
            }
        }

        /// <summary>
        ///     Add a IConceptSchemeSuperObject to this container, overwrite if one already exists with the same URN
        /// </summary>
        /// <param name="conceptSchemeObject"> ConceptScheme object </param>
        public void AddConceptScheme(IConceptSchemeSuperObject conceptSchemeObject)
        {
            if (conceptSchemeObject != null)
            {
                this._conceptSchemes.Remove(conceptSchemeObject);
                this._conceptSchemes.Add(conceptSchemeObject);
            }
        }

        /// <summary>
        ///     Add a IDataflowSuperObject to this container, overwrite if one already exists with the same URN
        /// </summary>
        /// <param name="dataflowObject">Dataflow Object </param>
        public void AddDataflow(IDataflowSuperObject dataflowObject)
        {
            if (dataflowObject != null)
            {
                this._dataflows.Remove(dataflowObject);
                this._dataflows.Add(dataflowObject);
            }
        }

        /// <summary>
        ///     Add a IDataStructureSuperObject to this container, overwrite if one already exists with the same URN
        /// </summary>
        /// <param name="dataStructureObject">Datastructure object. </param>
        public void AddDataStructure(IDataStructureSuperObject dataStructureObject)
        {
            if (dataStructureObject != null)
            {
                this._dataStructures.Remove(dataStructureObject);
                this._dataStructures.Add(dataStructureObject);
            }
        }

        /// <summary>
        ///     Add a IHierarchicalCodelistSuperObject to this container, overwrite if one already exists with the same URN
        /// </summary>
        /// <param name="hierarchicalCodelistObject"> HierarchicalCodelist Object </param>
        public void AddHierarchicalCodelist(IHierarchicalCodelistSuperObject hierarchicalCodelistObject)
        {
            if (hierarchicalCodelistObject != null)
            {
                this._hcls.Remove(hierarchicalCodelistObject);
                this._hcls.Add(hierarchicalCodelistObject);
            }
        }

        /// <summary>
        ///     The add maintainable.
        /// </summary>
        /// <param name="maintainableObject">
        ///     The categorySchemeObject.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="maintainableObject"/> is <see langword="null" />.</exception>
        public void AddMaintainable(IMaintainableSuperObject maintainableObject)
        {
            if (maintainableObject == null)
            {
                throw new ArgumentNullException("maintainableObject");
            }

            switch (maintainableObject.BuiltFrom.StructureType.EnumType)
            {
                case SdmxStructureEnumType.CategoryScheme:
                    this.AddCategoryScheme((ICategorySchemeSuperObject)maintainableObject);
                    break;

                case SdmxStructureEnumType.CodeList:
                    this.AddCodelist((ICodelistSuperObject)maintainableObject);
                    break;

                case SdmxStructureEnumType.ConceptScheme:
                    this.AddConceptScheme((IConceptSchemeSuperObject)maintainableObject);
                    break;

                case SdmxStructureEnumType.Dataflow:
                    this.AddDataflow((IDataflowSuperObject)maintainableObject);
                    break;

                case SdmxStructureEnumType.HierarchicalCodelist:
                    this.AddHierarchicalCodelist((IHierarchicalCodelistSuperObject)maintainableObject);
                    break;

                case SdmxStructureEnumType.Dsd:
                    this.AddDataStructure((IDataStructureSuperObject)maintainableObject);
                    break;

                case SdmxStructureEnumType.Process:
                    this.AddProcess((IProcessSuperObject)maintainableObject);
                    break;

                case SdmxStructureEnumType.ProvisionAgreement:
                    this.AddProvision((IProvisionAgreementSuperObject)maintainableObject);
                    break;

                case SdmxStructureEnumType.Registration:
                    this.AddRegistration((IRegistrationSuperObject)maintainableObject);
                    break;

                default:
                    throw new SdmxNotImplementedException(
                        "SuperBeansImpl.addMaintainable of type : "
                        + maintainableObject.BuiltFrom.StructureType.StructureType);
            }
        }

        /// <summary>
        ///     Add a IProcessSuperObject to this container, overwrite if one already exists with the same URN
        /// </summary>
        /// <param name="processObject"> Process Object </param>
        public void AddProcess(IProcessSuperObject processObject)
        {
            if (processObject != null)
            {
                this._processes.Remove(processObject);
                this._processes.Add(processObject);
            }
        }

        /// <summary>
        ///     Add a IProvisionAgreementSuperObject to this container, overwrite if one already exists with the same URN
        /// </summary>
        /// <param name="provisionAgreementObject"> ProvisionAgreement Object </param>
        public void AddProvision(IProvisionAgreementSuperObject provisionAgreementObject)
        {
            if (provisionAgreementObject != null)
            {
                this._provisionAgreement.Remove(provisionAgreementObject);
                this._provisionAgreement.Add(provisionAgreementObject);
            }
        }

        /// <summary>
        ///     Add a IRegistrationSuperObject to this container, overwrite if one already exists with the same URN
        /// </summary>
        /// <param name="registrationObject">Registration Object </param>
        public void AddRegistration(IRegistrationSuperObject registrationObject)
        {
            if (registrationObject != null)
            {
                this._registrations.Remove(registrationObject);
                this._registrations.Add(registrationObject);
            }
        }

        /// <summary>
        ///     Merges the super
        /// </summary>
        /// <param name="superObjects"> objects Base </param>
        /// <exception cref="ArgumentNullException"><paramref name="superObjects"/> is <see langword="null" />.</exception>
        public void Merge(ISuperObjects superObjects)
        {
            if (superObjects == null)
            {
                throw new ArgumentNullException("superObjects");
            }

            foreach (IMaintainableSuperObject currentObjectBase in superObjects.AllMaintainables)
            {
                this.AddMaintainable(currentObjectBase);
            }
        }

        /// <summary>
        ///     Remove the given ICategorySchemeSuperObject from this container, do nothing if it is not in this container
        /// </summary>
        /// <param name="categorySchemeObject">CategoryScheme Object </param>
        public void RemoveCategoryScheme(ICategorySchemeSuperObject categorySchemeObject)
        {
            this._categorySchemes.Remove(categorySchemeObject);
        }

        /// <summary>
        ///     Remove the given ICategorySchemeSuperObject from this container, do nothing if it is not in this container
        /// </summary>
        /// <param name="codelistObject">codelist Object </param>
        public void RemoveCodelist(ICodelistSuperObject codelistObject)
        {
            this._codelists.Remove(codelistObject);
        }

        /// <summary>
        ///     Remove the given ICategorySchemeSuperObject from this container, do nothing if it is not in this container
        /// </summary>
        /// <param name="conceptSchemeObject">ConceptScheme Object </param>
        public void RemoveConceptScheme(IConceptSchemeSuperObject conceptSchemeObject)
        {
            this._conceptSchemes.Remove(conceptSchemeObject);
        }

        /// <summary>
        ///     Remove the given IDataflowSuperObject from this container, do nothing if it is not in this container
        /// </summary>
        /// <param name="dataflowObject"> Dataflow Object </param>
        public void RemoveDataflow(IDataflowSuperObject dataflowObject)
        {
            this._dataflows.Remove(dataflowObject);
        }

        /// <summary>
        ///     Remove the given IDataStructureSuperObject from this container, do nothing if it is not in this container
        /// </summary>
        /// <param name="dataStructureObject">DataStructure Object </param>
        public void RemoveDataStructure(IDataStructureSuperObject dataStructureObject)
        {
            this._dataStructures.Remove(dataStructureObject);
        }

        /// <summary>
        ///     Remove the given IHierarchicalCodelistSuperObject from this container, do nothing if it is not in this container
        /// </summary>
        /// <param name="hierarchicalCodelistObject">HierarchicalCodelist Object </param>
        public void RemoveHierarchicalCodelist(IHierarchicalCodelistSuperObject hierarchicalCodelistObject)
        {
            this._hcls.Remove(hierarchicalCodelistObject);
        }

        /// <summary>
        ///     Remove the given IProcessSuperObject from this container, do nothing if it is not in this container
        /// </summary>
        /// <param name="processObject">Process Object </param>
        public void RemoveProcess(IProcessSuperObject processObject)
        {
            this._processes.Remove(processObject);
        }

        /// <summary>
        ///     Remove the given IProvisionAgreementSuperObject from this container, do nothing if it is not in this container
        /// </summary>
        /// <param name="provisionAgreementObject">ProvisionAgreement Object </param>
        public void RemoveProvision(IProvisionAgreementSuperObject provisionAgreementObject)
        {
            this._provisionAgreement.Remove(provisionAgreementObject);
        }

        /// <summary>
        ///     Remove the given IRegistrationSuperObject from this container, do nothing if it is not in this container
        /// </summary>
        /// <param name="registrationObject">Registration Object </param>
        public void RemoveRegistration(IRegistrationSuperObject registrationObject)
        {
            this._registrations.Remove(registrationObject);
        }
    }
}