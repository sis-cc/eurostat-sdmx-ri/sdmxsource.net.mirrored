// -----------------------------------------------------------------------
// <copyright file="MetadataContainer.cs" company="EUROSTAT">
//   Date Created : 2023-05-15
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Objects.Container
{

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using System.Collections.Generic;
    using System.Linq;
    using System;

    public abstract class MetadataContainer : IMetadataContainer
    {
        private string id = Guid.NewGuid().ToString();
        private DatasetAction action = DatasetAction.GetFromEnum(DatasetActionEnumType.Information);  //If Header is not available
        private IHeader header;

        private ISet<string> selectedLocales;

        public string Id => id;

        public DatasetAction Action => action;

        public void SetAction(DatasetAction action)
        {
            this.action = action;
        }

        public IHeader Header => header;

        public void SetHeader(IHeader header)
        {
            this.header = header;
        }

        public void SetSelectedLocales(ISet<string> selectedLocales)
        {

            this.selectedLocales = selectedLocales;
        }

        //NOTE SDMXSOURCE_SDMXCORE CHANGE
        public ISet<string> AllLocales
        {
            get
            {
                if (selectedLocales == null)
                {
                    selectedLocales = new HashSet<string>(AllMaintainables
                        .SelectMany(b => b.GetSupportedLanguages())
                        .Distinct());
                }
                return selectedLocales;
            }
        }

        public abstract ISet<IMaintainableObject> AllMaintainables { get; }
    }
}