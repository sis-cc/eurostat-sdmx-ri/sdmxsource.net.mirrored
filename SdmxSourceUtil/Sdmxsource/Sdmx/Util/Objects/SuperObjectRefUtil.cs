﻿// -----------------------------------------------------------------------
// <copyright file="SuperObjectRefUtil.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Objects
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     SuperObjectRefUtil CLASS
    /// </summary>
    /// <typeparam name="T">generic type</typeparam>
    public class SuperObjectRefUtil<T>
        where T : IMaintainableSuperObject
    {
        /// <summary>
        ///     Resolves the reference.
        /// </summary>
        /// <typeparam name="TV">The type of the v.</typeparam>
        /// <param name="maintainables">The maintainables.</param>
        /// <param name="bean">The bean.</param>
        /// <returns>The maintainable object</returns>
        /// <exception cref="ArgumentException">
        ///     Ref is null
        ///     or
        ///     Ref is mising AgencyId
        ///     or
        ///     Ref is mising Id
        /// </exception>
        /// <exception cref="System.ArgumentException">
        ///     Ref is null
        ///     or
        ///     Ref is mising AgencyId
        ///     or
        ///     Ref is mising Id
        /// </exception>
        public static IMaintainableSuperObject ResolveReference<TV>(
            IEnumerable<TV> maintainables, 
            IMaintainableRefObject bean) where TV : IMaintainableSuperObject
        {
            if (bean == null)
            {
                throw new ArgumentException("Ref is null");
            }

            if (!bean.HasAgencyId())
            {
                throw new ArgumentException("Ref is mising AgencyId");
            }

            if (!bean.HasMaintainableId())
            {
                throw new ArgumentException("Ref is mising Id");
            }

            IMaintainableSuperObject latestVersion = null;
            if (maintainables != null)
            {
                foreach (var current in maintainables)
                {
                    if (current.AgencyId.Equals(bean.AgencyId))
                    {
                        if (current.Id.Equals(bean.MaintainableId))
                        {
                            if (!bean.HasVersion())
                            {
                                if (latestVersion == null
                                    || VersionableUtil.IsHigherVersion(current.Version, latestVersion.Version))
                                {
                                    latestVersion = current;
                                }
                            }
                            else if (current.Version.Equals(bean.Version))
                            {
                                return current;
                            }
                        }
                    }
                }
            }

            return latestVersion;
        }

        /// <summary>
        ///     Resolves the references.
        /// </summary>
        /// <param name="maintainables">The maintainables.</param>
        /// <param name="bean">The bean.</param>
        /// <returns>The set of references</returns>
        public ISet<T> ResolveReferences(IEnumerable<T> maintainables, IMaintainableRefObject bean)
        {
            var returnSet = new HashSet<T>();

            if (bean == null)
            {
                bean = new MaintainableRefObjectImpl();
            }

            var hasAgencyFilter = ObjectUtil.ValidString(bean.AgencyId);
            var hasIdFilter = ObjectUtil.ValidString(bean.MaintainableId);
            var hasVersionFilter = ObjectUtil.ValidString(bean.Version);

            var agencyId = bean.AgencyId;
            var id = bean.MaintainableId;
            var version = bean.Version;

            if (maintainables != null)
            {
                foreach (var current in maintainables)
                {
                    if (hasAgencyFilter && !agencyId.Equals(current.AgencyId))
                    {
                        continue;
                    }

                    if (hasIdFilter && !id.Equals(current.Id))
                    {
                        continue;
                    }

                    if (hasVersionFilter && !version.Equals(current.Version))
                    {
                        continue;
                    }

                    returnSet.Add(current);
                }
            }

            return returnSet;
        }
    }
}