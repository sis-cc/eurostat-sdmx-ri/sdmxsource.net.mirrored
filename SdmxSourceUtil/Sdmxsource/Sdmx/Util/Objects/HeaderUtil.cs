// -----------------------------------------------------------------------
// <copyright file="HeaderUtil.cs" company="EUROSTAT">
//   Date Created : 2023-05-18
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Util.Objects
{
    using System;

    public class HeaderUtil
    {
        private static int i = 0;
        private static int j = 0;
        private static object lockObject = new object();

        /// <summary> 
        /// Generates an IREF for the Header Id which is compliant with the EDI Specification, i.e 
        /// IREF number should be 10 characters like IREFXXXXXX 
        /// </summary>
        /// <returns>
        /// </returns> 
        public static string GenerateIREF()
        {
            long timeNow = DateTimeOffset.Now.ToUnixTimeMilliseconds();
            string asString = timeNow.ToString();

            string last4;
            lock (lockObject)
            {
                // Example time: 1566901778083
                // Extract the period '0177' from the string, representing tens, hundreds, thousands, and hundred thousands of seconds
                last4 = asString.Substring(5, 4);

                // i and j combined will be a number between 00 and 99 inclusive
                j++;
                if (j > 9)
                {
                    j = 0;
                    i++;
                    if (i > 9)
                    {
                        i = 0;
                    }
                }
            }

            return $"IREF{last4}{i}{j}";
        }
    }
}