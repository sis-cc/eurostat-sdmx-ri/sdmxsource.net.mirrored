// -----------------------------------------------------------------------
// <copyright file="SdmxStructureRelation.cs" company="EUROSTAT">
//   Date Created : 2019-11-6
//   Copyright (c) 2019 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Util.Extensions;

namespace Org.Sdmxsource.Sdmx.Util.Sdmx
{
    /// <summary>
    /// Holds the paths of an structure type
    /// </summary>
    public class SdmxStructureRelation
    {
        private readonly List<IReadOnlyList<StructureAndRelation>> _parentsAndSiblings = new List<IReadOnlyList<StructureAndRelation>>();
        private readonly List<IReadOnlyList<StructureAndRelation>> _descendants = new List<IReadOnlyList<StructureAndRelation>>();

        public IReadOnlyList<IReadOnlyList<StructureAndRelation>> ParentsAndSiblings => _parentsAndSiblings;
        public IReadOnlyList<IReadOnlyList<StructureAndRelation>> Descendants => _descendants;

        public StructureReferenceDetail RequestedReference { get; }
        public SdmxStructureType Source { get; }

        public SdmxStructureRelation(SdmxStructureType source, StructureReferenceDetail requestedReference)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (requestedReference == null)
            {
                throw new ArgumentNullException(nameof(requestedReference));
            }

            RequestedReference = requestedReference;
            Source = source;
        }

        public SdmxStructureRelation(SdmxStructureType source, StructureReferenceDetailEnumType requestedReference) : this(source, StructureReferenceDetail.GetFromEnum(requestedReference))
        {
        }

        public void AddParentsAndSiblings(IReadOnlyList<StructureAndRelation> path)
        {
            if (RequestedReference.EnumType.IsOneOf(StructureReferenceDetailEnumType.Parents, StructureReferenceDetailEnumType.ParentsSiblings, StructureReferenceDetailEnumType.Specific, StructureReferenceDetailEnumType.All)) {
                _parentsAndSiblings.Add(path);
                return;
            }

            throw new InvalidOperationException("Add parents and siblings called but request was for " + RequestedReference.ToString());
        }

        public void AddDescendants(IReadOnlyList<StructureAndRelation> path)
        {
            if (RequestedReference.EnumType.IsOneOf(StructureReferenceDetailEnumType.Children, StructureReferenceDetailEnumType.Descendants, StructureReferenceDetailEnumType.Specific, StructureReferenceDetailEnumType.All)) {
                _descendants.Add(path);
                return;
            }
            throw new InvalidOperationException("Add descendants called but request was for " + RequestedReference.ToString());
        }
    }

    public class StructureAndRelation
    {
        public SdmxStructureType StructureType { get; set; }
        public StructureRelation Relation { get; set; }
    }

    public enum StructureRelation
    {
        Child,
        Parent
    }
}
