// -----------------------------------------------------------------------
// <copyright file="SdmxMaintainableReferenceTree.cs" company=SdmxStructureEnumType.EUROSTAT>
//   Date Created : 2019-10-22
//   Copyright (c) 2019 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Util.Extensions;

namespace Org.Sdmxsource.Sdmx.Util.Sdmx
{
    public static class SdmxMaintainableReferenceTree
    {
        private static readonly Dictionary<SdmxStructureEnumType, SdmxStructureEnumType[]> _parents;
        private static readonly Dictionary<SdmxStructureEnumType, List<SdmxStructureEnumType>> _children;

        static SdmxMaintainableReferenceTree()
        {
            _parents = new Dictionary<SdmxStructureEnumType, SdmxStructureEnumType[]>()
        {
{ SdmxStructureEnumType.AgencyScheme, new [] {SdmxStructureEnumType.Categorisation, SdmxStructureEnumType.Process, SdmxStructureEnumType.Msd, SdmxStructureEnumType.StructureSet}},
{ SdmxStructureEnumType.CategoryScheme, new [] {SdmxStructureEnumType.Categorisation, SdmxStructureEnumType.Process, SdmxStructureEnumType.StructureSet}},
{ SdmxStructureEnumType.CodeList, new [] {SdmxStructureEnumType.Categorisation, SdmxStructureEnumType.Process, SdmxStructureEnumType.HierarchicalCodelist, SdmxStructureEnumType.ConceptScheme, SdmxStructureEnumType.Dsd, SdmxStructureEnumType.Msd, SdmxStructureEnumType.StructureSet}},
{ SdmxStructureEnumType.ConceptScheme, new [] {SdmxStructureEnumType.Categorisation, SdmxStructureEnumType.Process, SdmxStructureEnumType.Dsd, SdmxStructureEnumType.Msd, SdmxStructureEnumType.StructureSet}},
{ SdmxStructureEnumType.AttachmentConstraint, new [] {SdmxStructureEnumType.Categorisation, SdmxStructureEnumType.Process}},
{ SdmxStructureEnumType.AllowedConstraint, new [] {SdmxStructureEnumType.Categorisation, SdmxStructureEnumType.Process}},
{ SdmxStructureEnumType.ContentConstraint, new [] {SdmxStructureEnumType.Categorisation, SdmxStructureEnumType.Process}},
{ SdmxStructureEnumType.ActualConstraint, new [] {SdmxStructureEnumType.Categorisation, SdmxStructureEnumType.Process}},
{ SdmxStructureEnumType.Constraint, new [] {SdmxStructureEnumType.Categorisation, SdmxStructureEnumType.Process}},
{ SdmxStructureEnumType.DataConsumerScheme, new [] {SdmxStructureEnumType.Categorisation, SdmxStructureEnumType.Process, SdmxStructureEnumType.Msd, SdmxStructureEnumType.StructureSet}},
{ SdmxStructureEnumType.Dataflow, new [] {SdmxStructureEnumType.Categorisation, SdmxStructureEnumType.Process, SdmxStructureEnumType.AllowedConstraint, SdmxStructureEnumType.ContentConstraint, SdmxStructureEnumType.ActualConstraint, SdmxStructureEnumType.Constraint, SdmxStructureEnumType.AttachmentConstraint, SdmxStructureEnumType.ProvisionAgreement, SdmxStructureEnumType.ReportingTaxonomy, SdmxStructureEnumType.StructureSet}},
{ SdmxStructureEnumType.DataProviderScheme, new [] {SdmxStructureEnumType.Categorisation, SdmxStructureEnumType.Process, SdmxStructureEnumType.AllowedConstraint, SdmxStructureEnumType.ContentConstraint, SdmxStructureEnumType.ActualConstraint, SdmxStructureEnumType.Constraint, SdmxStructureEnumType.AttachmentConstraint, SdmxStructureEnumType.ProvisionAgreement, SdmxStructureEnumType.Msd, SdmxStructureEnumType.StructureSet}},
{ SdmxStructureEnumType.Dsd, new [] {SdmxStructureEnumType.Categorisation, SdmxStructureEnumType.Process, SdmxStructureEnumType.AllowedConstraint, SdmxStructureEnumType.ContentConstraint, SdmxStructureEnumType.ActualConstraint, SdmxStructureEnumType.Constraint, SdmxStructureEnumType.AttachmentConstraint, SdmxStructureEnumType.Dataflow, SdmxStructureEnumType.StructureSet}},
{ SdmxStructureEnumType.HierarchicalCodelist, new [] {SdmxStructureEnumType.Categorisation, SdmxStructureEnumType.Process, SdmxStructureEnumType.StructureSet}},
{ SdmxStructureEnumType.MetadataFlow, new [] {SdmxStructureEnumType.Categorisation, SdmxStructureEnumType.Process, SdmxStructureEnumType.AllowedConstraint, SdmxStructureEnumType.ContentConstraint, SdmxStructureEnumType.ActualConstraint, SdmxStructureEnumType.Constraint, SdmxStructureEnumType.AttachmentConstraint, SdmxStructureEnumType.ProvisionAgreement, SdmxStructureEnumType.ReportingTaxonomy, SdmxStructureEnumType.StructureSet}},
{ SdmxStructureEnumType.Msd, new [] {SdmxStructureEnumType.Categorisation, SdmxStructureEnumType.Process, SdmxStructureEnumType.AllowedConstraint, SdmxStructureEnumType.ContentConstraint, SdmxStructureEnumType.ActualConstraint, SdmxStructureEnumType.Constraint, SdmxStructureEnumType.AttachmentConstraint, SdmxStructureEnumType.MetadataFlow, SdmxStructureEnumType.StructureSet}},
{ SdmxStructureEnumType.OrganisationUnitScheme, new [] {SdmxStructureEnumType.Categorisation, SdmxStructureEnumType.Process, SdmxStructureEnumType.AllowedConstraint, SdmxStructureEnumType.ContentConstraint, SdmxStructureEnumType.ActualConstraint, SdmxStructureEnumType.Constraint, SdmxStructureEnumType.AttachmentConstraint, SdmxStructureEnumType.Msd, SdmxStructureEnumType.StructureSet}},
{ SdmxStructureEnumType.ProvisionAgreement, new [] {SdmxStructureEnumType.Categorisation, SdmxStructureEnumType.Process, SdmxStructureEnumType.AllowedConstraint, SdmxStructureEnumType.ContentConstraint, SdmxStructureEnumType.ActualConstraint, SdmxStructureEnumType.Constraint, SdmxStructureEnumType.AttachmentConstraint}},
{ SdmxStructureEnumType.ReportingTaxonomy, new [] {SdmxStructureEnumType.Categorisation, SdmxStructureEnumType.Process, SdmxStructureEnumType.StructureSet}},
{ SdmxStructureEnumType.StructureSet, new [] {SdmxStructureEnumType.Categorisation, SdmxStructureEnumType.Process}} };

            _children = new Dictionary<SdmxStructureEnumType, List<SdmxStructureEnumType>>();
            foreach (var entry in _parents)
            {
                foreach (var value in entry.Value)
                {
                    List<SdmxStructureEnumType> parents;
                    if (!_children.TryGetValue(value, out parents))
                    {
                        parents = new List<SdmxStructureEnumType>();
                        _children.Add(value, parents);
                    }

                    parents.Add(entry.Key);
                }
            }
        }

        /// <summary>
        /// Get the structures types that can directly reference the specified <paramref name="structure"/>
        /// Example if Codelist is provided then DataStructure, metadata structure, concept scheme, etc anything that references a codelist 
        /// </summary>
        /// <param name="structure">The referenced structure</param>
        /// <returns>The list of parent references</returns>
        public static IList<SdmxStructureType> GetParentReferences(SdmxStructureType structure)
        {
            if (structure == null)
            {
                throw new ArgumentNullException(nameof(structure));
            }

            SdmxStructureType maintainableStructureType;
            if (!structure.IsMaintainable)
            {
                maintainableStructureType = structure.MaintainableStructureType;
            }
            else
            {
                maintainableStructureType = structure;
            }

            if (maintainableStructureType != null && _parents.TryGetValue(maintainableStructureType.EnumType, out SdmxStructureEnumType[] parents))
            {
                return parents.Select(x => SdmxStructureType.GetFromEnum(x)).ToArray();
            }

            return new SdmxStructureType[0];
        }

        /// <summary>
        /// Get the structures types that are directly referenced by the specified <paramref name="structure"/>
        /// Example if Dataflow is provided then it returns DataStructure 
        /// </summary>
        /// <param name="structure">The structure that references other structures</param>
        /// <returns>The list of children references</returns>
        public static IList<SdmxStructureType> GetChildReferences(SdmxStructureType structure)
        {
            if (structure == null)
            {
                throw new ArgumentNullException(nameof(structure));
            }

            SdmxStructureType maintainableStructureType;
            if (!structure.IsMaintainable)
            {
                maintainableStructureType = structure.MaintainableStructureType;
            }
            else
            {
                maintainableStructureType = structure;
            }

            if (maintainableStructureType != null && _children.TryGetValue(maintainableStructureType.EnumType, out List<SdmxStructureEnumType> children))
            {
                return children.Select(x => SdmxStructureType.GetFromEnum(x)).ToArray();
            }

            return new SdmxStructureType[0];
        }

        public static bool IsParent(SdmxStructureType source, SdmxStructureType target)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (target == null)
            {
                throw new ArgumentNullException(nameof(target));
            }

            if (_parents.TryGetValue(source.EnumType, out SdmxStructureEnumType[] parents) && parents.Contains(target.EnumType))
            {
                return true;
            }

            return false;
        }

        public static SdmxStructureRelation GetParentsAndSiblings(SdmxStructureType source)
        {
            var fullPaths = new SdmxStructureRelation(source, StructureReferenceDetailEnumType.ParentsSiblings);
            PopuplateParentsAndSiblings(source, fullPaths);

            return fullPaths;
        }
        public static SdmxStructureRelation GetAll(SdmxStructureType source)
        {
            var fullPaths = new SdmxStructureRelation(source, StructureReferenceDetailEnumType.All);
            PopuplateParentsAndSiblings(source, fullPaths);
            PopulateDescendants(source, fullPaths);
            return fullPaths;
        }
        private static void PopuplateParentsAndSiblings(SdmxStructureType source, SdmxStructureRelation fullPaths)
        {
            List<StructureAndRelation> item = new List<StructureAndRelation> { new StructureAndRelation
                {StructureType = source} };
            var queue = new Queue<List<StructureAndRelation>>();
            var isVisited = new HashSet<StructureAndRelation>();
            // parents and siblings
            foreach (var parent in GetParentReferences(source))
            {
                queue.Enqueue(new List<StructureAndRelation>(item) { new StructureAndRelation(){StructureType = parent,Relation = StructureRelation.Parent} });
            }

            while (queue.Count > 0)
            {
                var current = queue.Dequeue();
                var last = current.Last();
                fullPaths.AddParentsAndSiblings(current);
                if (isVisited.Add(last))
                {
                    if (current.Count == 2) // Only siblings , children are handled below 
                    {
                        var siblings = GetChildReferences(last.StructureType);
                        foreach (var child in siblings)
                        {
                            // categorisation can link a maintainable with a category but not 2 maintainables
                            if ((last.StructureType.EnumType != SdmxStructureEnumType.Categorisation
                                || (source.EnumType == SdmxStructureEnumType.CategoryScheme || child.EnumType == SdmxStructureEnumType.CategoryScheme))
                                && (!last.StructureType.EnumType.IsOneOf(SdmxStructureEnumType.ActualConstraint, SdmxStructureEnumType.AllowedConstraint, SdmxStructureEnumType.ContentConstraint, SdmxStructureEnumType.AttachmentConstraint) ||
                                child == source))
                            {
                                queue.Enqueue(new List<StructureAndRelation>(current) { new StructureAndRelation(){StructureType = child }});
                            }
                        }
                    }
                }
            }
        }

        public static SdmxStructureRelation GetDescendants(SdmxStructureType source)
        {
            var fullPaths = new SdmxStructureRelation(source, StructureReferenceDetailEnumType.Descendants);

            PopulateDescendants(source, fullPaths);

            return fullPaths;
        }

        private static void PopulateDescendants(SdmxStructureType source, SdmxStructureRelation fullPaths)
        {
            var queue = new Queue<List<StructureAndRelation>>();
            var isVisited = new HashSet<StructureAndRelation>();
            List<StructureAndRelation> item = new List<StructureAndRelation> { new StructureAndRelation(){StructureType = source } };
            queue.Enqueue(item);
            while (queue.Count > 0)
            {
                var current = queue.Dequeue();
                var last = current.Last();
                //if (isVisited.Add(last))
                //{
                    fullPaths.AddDescendants(current);
                    var children = GetChildReferences(last.StructureType);
                    foreach (var child in children)
                    {
                        queue.Enqueue(new List<StructureAndRelation>(current) { new StructureAndRelation() { StructureType = child} });
                    }
                //}
            }
        }

        public static SdmxStructureRelation GetSpecificPath(SdmxStructureType source, SdmxStructureType target)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (target == null)
            {
                throw new ArgumentNullException(nameof(target));
            }

            var fullPaths = new SdmxStructureRelation(source, StructureReferenceDetailEnumType.Specific);
            foreach(var path in GetParentsAndSiblings(source).ParentsAndSiblings)
            {
                if (path.Last().StructureType == target)
                {
                    fullPaths.AddParentsAndSiblings(path);
                }
            }

            foreach(var path in GetDescendants(source).Descendants)
            {
                if (path.Last().StructureType == target)
                {
                    fullPaths.AddDescendants(path);
                }
            }

            return fullPaths;
        }
    }
}
