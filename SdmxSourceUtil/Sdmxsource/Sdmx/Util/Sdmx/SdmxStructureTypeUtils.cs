// -----------------------------------------------------------------------
// <copyright file="SdmxStructureTypeUtils.cs" company="EUROSTAT">
//   Date Created : 2022-05-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Util.Sdmx
{
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;

    /// <summary>
    /// Utils class that extends the <see cref="SdmxStructureType"/>.
    /// </summary>
    public static class SdmxStructureTypeUtils
    {
        /// <summary>
        /// Gets the type of an ItemScheme and returns the type of it's Items.
        /// </summary>
        /// <param name="itemSchemeStructureType">The ItemScheme to get the type of items for.</param>
        /// <returns>The type of Items for the ItemScheme.</returns>
        /// <exception cref="SdmxException">If not Items type found.</exception>
        public static SdmxStructureType GetItemStructureType(SdmxStructureType itemSchemeStructureType)
        {
            foreach (SdmxStructureType structureType in SdmxStructureType.Values)
            {
                if (structureType.ParentStructureType == itemSchemeStructureType)
                {
                    return structureType;
                }
            }
            throw new SdmxException("could not find child structure type for structure type " + itemSchemeStructureType);
        }



        /// <summary>
        /// Utils to check the Item Scheme
        /// </summary>
        public static bool IsItemScheme(SdmxStructureEnumType structureType)
        {
            switch (structureType)
            {
                case SdmxStructureEnumType.CodeList:
                case SdmxStructureEnumType.Code:
                case SdmxStructureEnumType.CategoryScheme:
                case SdmxStructureEnumType.Category:
                case SdmxStructureEnumType.ConceptScheme:
                case SdmxStructureEnumType.Concept:
                case SdmxStructureEnumType.DataProvider:
                case SdmxStructureEnumType.DataProviderScheme:
                case SdmxStructureEnumType.DataConsumer:
                case SdmxStructureEnumType.DataConsumerScheme:
                case SdmxStructureEnumType.Agency:
                case SdmxStructureEnumType.AgencyScheme:
                case SdmxStructureEnumType.OrganisationUnit:
                case SdmxStructureEnumType.OrganisationUnitScheme:
                    return true;
                default:
                    return false;
            }
        }


    }
}
