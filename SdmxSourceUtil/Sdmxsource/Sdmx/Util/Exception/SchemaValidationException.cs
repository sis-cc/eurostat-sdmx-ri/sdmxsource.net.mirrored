// -----------------------------------------------------------------------
// <copyright file="SchemaValidationException.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Exception
{
    #region Using directives

    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Exception;

    #endregion

    /// <summary>
    ///     The schema validation exception.
    /// </summary>
    [Serializable]
    public class SchemaValidationException : SdmxSyntaxException
    {
        /// <summary>
        ///     The _validation errors
        /// </summary>
        private readonly IList<string> _validationErrors = new List<string>();

        /// <summary>
        /// Initializes a new instance of the <see cref="SchemaValidationException"/> class.
        /// </summary>
        public SchemaValidationException()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SchemaValidationException" /> class.
        ///     Creates Exception from an error Message
        /// </summary>
        /// <param name="errorMessage">
        ///     the error message
        /// </param>
        public SchemaValidationException(string errorMessage)
            : base(errorMessage)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SchemaValidationException" /> class.
        ///     If the <paramref name="innerException" /> is a SdmxException - then the
        ///     error code will be used, if it is not, then InternalServerError will be used
        /// </summary>
        /// <param name="errorMessage">
        ///     The error message that explains the reason for the exception.
        /// </param>
        /// <param name="innerException">
        ///     The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner
        ///     exception is specified.
        /// </param>
        public SchemaValidationException(string errorMessage, Exception innerException)
            : base(errorMessage, innerException)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SchemaValidationException" /> class.
        /// </summary>
        /// <param name="args">
        ///     The args.
        /// </param>
        public SchemaValidationException(Exception args)
            : base(args)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SchemaValidationException" /> class.
        /// </summary>
        /// <param name="errors">
        ///     The errors
        /// </param>
        public SchemaValidationException(IList<string> errors)
            : base(MergeErrors(errors))
        {
            foreach (string currentError in errors)
            {
                this._validationErrors.Add(currentError);
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SchemaValidationException" /> class with serialized data.
        /// </summary>
        /// <param name="info">
        ///     The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object
        ///     data about the exception being thrown.
        /// </param>
        /// <param name="context">
        ///     The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual
        ///     information about the source or destination.
        /// </param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="info" /> parameter is null. </exception>
        /// <exception cref="T:System.Runtime.Serialization.SerializationException">
        ///     The class name is null or
        ///     <see cref="P:System.Exception.HResult" /> is zero (0).
        /// </exception>
        protected SchemaValidationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        /// <summary>
        ///     Return the Validation error IList
        /// </summary>
        /// <returns>
        ///     The IList of string
        /// </returns>
        public IList<string> GetValidationErrors()
        {
            return new List<string>(this._validationErrors);
        }

        /// <summary>
        /// When overridden in a derived class, sets the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with
        /// information about the exception.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object
        /// data about the exception being thrown.</param>
        /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual
        /// information about the source or destination.</param>
        /// <PermissionSet>
        ///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Read="*AllFiles*" PathDiscovery="*AllFiles*" />
        ///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="SerializationFormatter" />
        /// </PermissionSet>
        /// <exception cref="ArgumentNullException"><paramref name="info"/> is <see langword="null" />.</exception>
        /// <exception cref="SerializationException">A value has already been associated. </exception>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }

            base.GetObjectData(info, context);
            info.AddValue("_validationErrors", this._validationErrors);
        }

        /// <summary>
        ///     Return a string based on error list parameters
        /// </summary>
        /// <param name="errors">
        ///     The list of errors
        /// </param>
        /// <returns>
        ///     The error string
        /// </returns>
        private static string MergeErrors(IList<string> errors)
        {
            var sb = new StringBuilder();

            foreach (string currentError in errors)
            {
                sb.Append(currentError);
            }

            return sb.ToString();
        }
    }
}