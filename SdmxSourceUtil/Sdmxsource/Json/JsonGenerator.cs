﻿// -----------------------------------------------------------------------
// <copyright file="JsonGenerator.cs" company="EUROSTAT">
//   Date Created : 2016-06-22
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Json
{
    using System.IO;

    using Newtonsoft.Json;

    /// <summary>
    /// The json generator class.
    /// </summary>
    public class JsonGenerator : JsonTextWriter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="JsonGenerator"/> class. 
        /// </summary>
        /// <param name="textWriter">
        /// The text writer.
        /// </param>
        public JsonGenerator(TextWriter textWriter)
            : base(textWriter)
        {
        }

        /// <summary>
        /// Writes an array field start.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        public void WriteArrayFieldStart(string name)
        {
            this.WritePropertyName(name);
            this.WriteStartArray();
        }

        /// <summary>
        /// Writes a boolean.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        public void WriteBoolean(bool value)
        {
            this.WriteValue(value);
        }

        /// <summary>
        /// Writes a boolean field.
        /// </summary>
        /// <param name="fieldName">
        /// The field name.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        public void WriteBooleanField(string fieldName, bool value)
        {
            this.WritePropertyName(fieldName);
            this.WriteValue(value);
        }

        /// <summary>
        /// Writes a null field.
        /// </summary>
        /// <param name="fieldName">
        /// The field name.
        /// </param>
        public void WriteNullField(string fieldName)
        {
            this.WritePropertyName(fieldName);
            this.WriteNull();
        }

        /// <summary>
        /// Writes a number.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        public void WriteNumber(decimal value)
        {
            this.WriteValue(value);
        }

        /// <summary>
        /// Writes a number.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        public void WriteNumber(double value)
        {
            this.WriteValue(value);
        }

        /// <summary>
        /// Writes a number.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        public void WriteNumber(float value)
        {
            this.WriteValue(value);
        }

        /// <summary>
        /// Writes a number.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        public void WriteNumber(int value)
        {
            this.WriteValue(value);
        }

        /// <summary>
        /// Writes a number.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        public void WriteNumber(long value)
        {
            this.WriteValue(value);
        }

        /// <summary>
        /// Writes a number field.
        /// </summary>
        /// <param name="fieldName">
        /// The field name.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        public void WriteNumberField(string fieldName, decimal value)
        {
            this.WritePropertyName(fieldName);
            this.WriteValue(value);
        }

        /// <summary>
        /// Writes a number field.
        /// </summary>
        /// <param name="fieldName">
        /// The field name.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        public void WriteNumberField(string fieldName, double value)
        {
            this.WritePropertyName(fieldName);
            this.WriteValue(value);
        }

        /// <summary>
        /// Writes a number field.
        /// </summary>
        /// <param name="fieldName">
        /// The field name.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        public void WriteNumberField(string fieldName, float value)
        {
            this.WritePropertyName(fieldName);
            this.WriteValue(value);
        }

        /// <summary>
        /// Writes a number field.
        /// </summary>
        /// <param name="fieldName">
        /// The field name.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        public void WriteNumberField(string fieldName, int value)
        {
            this.WritePropertyName(fieldName);
            this.WriteValue(value);
        }

        /// <summary>
        /// Writes a number field.
        /// </summary>
        /// <param name="fieldName">
        /// The field name.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        public void WriteNumberField(string fieldName, long value)
        {
            this.WritePropertyName(fieldName);
            this.WriteValue(value);
        }

        /// <summary>
        /// Writes an object field start.
        /// </summary>
        /// <param name="fieldName">
        /// The field name.
        /// </param>
        public void WriteObjectFieldStart(string fieldName)
        {
            this.WritePropertyName(fieldName);
            this.WriteStartObject();
        }

        /// <summary>
        /// Writes a string.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        public void WriteString(string value)
        {
            this.WriteValue(value);
        }

        /// <summary>
        /// Writes a string field.
        /// </summary>
        /// <param name="fieldName">
        /// The field name.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        public void WriteStringField(string fieldName, string value)
        {
            this.WritePropertyName(fieldName);
            this.WriteValue(value);
        }
    }
}