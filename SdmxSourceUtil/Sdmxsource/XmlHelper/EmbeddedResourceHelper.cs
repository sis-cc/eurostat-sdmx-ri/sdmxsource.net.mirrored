// -----------------------------------------------------------------------
// <copyright file="EmbeddedResourceHelper.cs" company="EUROSTAT">
//   Date Created : 2022-02-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.XmlHelper
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// Helper class to retrieve the XSD resource files.
    /// </summary>
    public static class EmbeddedResourceHelper
    {
        /// <summary>
        /// Gets the file stream of the selected XSD.
        /// </summary>
        /// <param name="xsdName">The name of the XSD file to get the stream for.</param>
        /// <param name="sdmxSchema">
        ///     The schema version 
        ///     (There could be more than one XSD files with the same name for different schema versions).
        /// </param>
        /// <returns>A <see cref="Stream"/> with the file contents.</returns>
        public static Stream GetXSDResourceStream(string xsdName, SdmxSchemaEnumType sdmxSchema)
        {
            var assembly = Assembly.GetExecutingAssembly();

            string ns = GetNamespace(sdmxSchema);
            string resourceName = $"{ns}.{xsdName}";

            return assembly.GetManifestResourceStream(resourceName);
        }

        /// <summary>
        /// Gets the file stream for all XSD files in a schema version.
        /// </summary>
        /// <param name="sdmxSchema">The schema version to get the XSD files for.</param>
        /// <returns>A collection of <see cref="Stream"/> with the files' contents.</returns>
        public static IList<Stream> GetAllXSDResourceStreams(SdmxSchemaEnumType sdmxSchema)
        {
            List<Stream> results = new List<Stream>();

            var assembly = Assembly.GetExecutingAssembly();
            string ns = GetNamespace(sdmxSchema);
            var resources = assembly.GetManifestResourceNames()
                .Where(n => n.StartsWith(ns));
            foreach (string xsdName in resources)
            {
                results.Add(assembly.GetManifestResourceStream(xsdName));
            }

            return results;
        }

        /// <summary>
        /// Get the XSD file names (without the namespace) for a given schema version.
        /// </summary>
        /// <param name="sdmxSchema">The schema version.</param>
        /// <returns>The collection of filenames.</returns>
        public static IEnumerable<string> GetXSDResourceFilenames(SdmxSchemaEnumType sdmxSchema)
        {
            var assembly = Assembly.GetExecutingAssembly();
            string ns = GetNamespace(sdmxSchema);
            return assembly.GetManifestResourceNames()
                .Where(n => n.StartsWith(ns))
                .Select(n => n.Replace($"{ns}.", string.Empty));
        }

        /// <summary>
        /// Get a dictionary of the schema names (key) and streams (value).
        /// </summary>
        /// <param name="sdmxSchema">The sdmx schema version.</param>
        /// <returns></returns>
        public static Dictionary<string, Stream> GetXSDResourcesDictionary(SdmxSchemaEnumType sdmxSchema)
        {
            var results = new Dictionary<string, Stream>();

            foreach (string fileName in GetXSDResourceFilenames(sdmxSchema))
            {
                results.Add(fileName, GetXSDResourceStream(fileName, sdmxSchema));
            }

            return results;
        }

        /// <summary>
        /// Retrieve the XDC file namespace according to schema version.
        /// </summary>
        /// <param name="sdmxSchema">The schema version to get the namespace for.</param>
        /// <returns>The namespace as <see cref="string"/>.</returns>
        private static string GetNamespace(SdmxSchemaEnumType sdmxSchema)
        {
            string ns;
            switch (sdmxSchema)
            {
                case SdmxSchemaEnumType.VersionThree:
                    ns = "Org.xsd._3_0_0";
                    break;
                case SdmxSchemaEnumType.VersionTwoPointOne:
                    ns = "Org.xsd._2_1";
                    break;
                case SdmxSchemaEnumType.VersionTwo:
                    ns = "Org.xsd._2_0";
                    break;
                case SdmxSchemaEnumType.VersionOne:
                    ns = "Org.csd._1_0";
                    break;
                default:
                    throw new NotImplementedException();
            }
            return ns;
        }
    }
}
