// -----------------------------------------------------------------------
// <copyright file="MetadatasetsParsingJsonManager.cs" company="EUROSTAT">
//   Date Created : 2020-02-24
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxDataParser.
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using log4net;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.Api.Factory;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Model.Format;
using Org.Sdmxsource.Sdmx.Api.Model.Metadata;
using Org.Sdmxsource.Sdmx.Api.Util;
using Org.Sdmxsource.Sdmx.DataParser.Factory;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model;

namespace Org.Sdmxsource.Sdmx.DataParser.Manager
{
    /// <summary>
    ///     The metadatasets parsing json manager.
    /// </summary>
    public class MetadatasetsParsingJsonManager : IMetadatasetsParsingManager
    {
        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(MetadatasetsParsingJsonManager));

        /// <summary>
        ///     The structure parser factory
        /// </summary>
        private readonly IList<IMetadataParsingFactory> _metadataWriterFactories;

        private IMetadataFormat _sdmxFormat { get; }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadatasetsParsingJsonManager" /> class.
        /// </summary>
        /// <param name="structureFormatType">
        /// The output format type.
        /// </param>
        public MetadatasetsParsingJsonManager(IList<IMetadataParsingFactory> metadataWriterFactories)
        {
            _metadataWriterFactories = metadataWriterFactories;
        }

        public IMetadata BuildMetadataset(IReadableDataLocation dataLocation, IMetadataFormat structureFormatType)
        {
            foreach (var item in _metadataWriterFactories)
            {
                if (item is SdmxJsonMetadataFormat)
                {
                    IMetadataParseEngine writeEngine = item.GetMetadataWriterEngine(structureFormatType);
                    IMetadata metadataset = writeEngine.Build(dataLocation);
                    return metadataset;
                }
            }

            return null;
        }
    }
}
