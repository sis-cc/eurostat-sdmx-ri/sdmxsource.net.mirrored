// -----------------------------------------------------------------------
// <copyright file="ReportingPeriodBehavior.cs" company="EUROSTAT">
//   Date Created : 2018-4-10
//   Copyright (c) 2018 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjectsTests.
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Org.Sdmxsource.Sdmx.DataParser.Model
{
    /// <summary>
    /// Enum ReportingPeriodBehavior
    /// </summary>
    public enum ReportingPeriodBehavior
    {
        /// <summary>
        /// No translation
        /// </summary>
        NoTranslation = 0,
        
        /// <summary>
        /// The translate only when reporting year start day is not default
        /// </summary>
        TranslateWithReportingYearStartDay,

        /// <summary>
        /// The translate to SDMX v2.0
        /// </summary>
        TranslateToSDMXvTwo
    }
}
