﻿// -----------------------------------------------------------------------
// <copyright file="DataTypeRepresentation.cs" company="EUROSTAT">
//   Date Created : 2016-05-31
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Model
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;

    /// <summary>
    /// The data type representation enum.
    /// </summary>
    public enum DataTypeRepresentationEnumType
    {
        /// <summary>
        /// Flat type.
        /// </summary>
        Flat = 0,

        /// <summary>
        /// Time series type.
        /// </summary>
        TimeSeries = 1,

        /// <summary>
        /// Cross sectional type.
        /// </summary>
        CrossSectional = 2
    }

    /// <summary>
    /// DataTypeRepresentation class.
    /// </summary>
    public class DataTypeRepresentation : BaseConstantType<DataTypeRepresentationEnumType>
    {
        /// <summary>
        /// The _instances.
        /// </summary>
        private static readonly Dictionary<DataTypeRepresentationEnumType, DataTypeRepresentation> _instances = new Dictionary<DataTypeRepresentationEnumType, DataTypeRepresentation>
                                                                                                                    {
                                                                                                                        {
                                                                                                                            DataTypeRepresentationEnumType.Flat,
                                                                                                                            new DataTypeRepresentation(
                                                                                                                            DataTypeRepresentationEnumType.Flat)
                                                                                                                        },
                                                                                                                        {
                                                                                                                            DataTypeRepresentationEnumType.TimeSeries,
                                                                                                                            new DataTypeRepresentation(
                                                                                                                            DataTypeRepresentationEnumType.TimeSeries)
                                                                                                                        },
                                                                                                                        {
                                                                                                                            DataTypeRepresentationEnumType.CrossSectional,
                                                                                                                            new DataTypeRepresentation(
                                                                                                                            DataTypeRepresentationEnumType.CrossSectional)
                                                                                                                        }
                                                                                                                    };

        /// <summary>
        /// The _node.
        /// </summary>
        private readonly DataTypeRepresentationEnumType _node;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataTypeRepresentation"/> class. 
        /// Initializes a new <see cref="DataTypeRepresentation"/> class.
        /// </summary>
        /// <param name="node">
        /// The data type.
        /// </param>
        private DataTypeRepresentation(DataTypeRepresentationEnumType node)
            : base(node)
        {
            this._node = node;
        }

        /// <summary>
        /// Gets the data type.
        /// </summary>
        /// <param name="dimensionAtObservation">
        /// The dimension at observation.
        /// </param>
        /// <returns>
        /// The <see cref="DataTypeRepresentation"/> instance.
        /// </returns>
        public static DataTypeRepresentation GetDataType(string dimensionAtObservation)
        {
            DataTypeRepresentationEnumType dataType;

            if (dimensionAtObservation == null)
            {
                throw new ArgumentNullException("dimensionAtObservation");
            }

            if (dimensionAtObservation.Equals(DimensionObject.TimeDimensionFixedId))
            {
                dataType = DataTypeRepresentationEnumType.TimeSeries;
            }
            else if (dimensionAtObservation.Equals(DatasetStructureReference.AllDimensions))
            {
                dataType = DataTypeRepresentationEnumType.Flat;
            }
            else
            {
                dataType = DataTypeRepresentationEnumType.CrossSectional;
            }

            return GetFromEnum(dataType);
        }

        /// <summary>
        /// Checks for equal DataTypeRepresentationEnumType.
        /// </summary>
        /// <param name="enumType">
        /// The DataTypeRepresentationEnumType.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Equals(DataTypeRepresentationEnumType enumType)
        {
            return this._node.Equals(enumType);
        }

        /// <summary>
        /// The get from enum.
        /// </summary>
        /// <param name="enumType">
        /// The enum type.
        /// </param>
        /// <returns>
        /// The <see cref="DataTypeRepresentation"/>.
        /// </returns>
        private static DataTypeRepresentation GetFromEnum(DataTypeRepresentationEnumType enumType)
        {
            DataTypeRepresentation output;

            if (_instances.TryGetValue(enumType, out output))
            {
                return output;
            }

            return null;
        }
    }
}