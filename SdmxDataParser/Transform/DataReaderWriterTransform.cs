﻿// -----------------------------------------------------------------------
// <copyright file="DataReaderWriterTransform.cs" company="EUROSTAT">
//   Date Created : 2016-09-07
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.DataParser.Transform
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.Util.Date;
    using Org.Sdmxsource.Util;

    using Xml.Schema.Linq;

    /// <summary>
    /// The default implementation of <see cref="IDataReaderWriterTransform"/>
    /// </summary>
    public class DataReaderWriterTransform : IDataReaderWriterTransform
    {
        /// <summary>
        /// Copies the dataset to writer.
        /// </summary>
        /// <param name="dataReaderEngine">The data reader engine</param>
        /// <param name="dataWriterEngine">The data writer engine</param>
        /// <param name="pivotDimension">The pivot dimension</param>
        /// <param name="includeObs">The include observation</param>
        /// <param name="maxObs">The max observation</param>
        /// <param name="dataFrom">The date from</param>
        /// <param name="dateTo">The date to</param>
        /// <param name="includeHeader">The include header</param>
        /// <param name="closeOnCompletion">The close on completion</param>
        public void CopyDatasetToWriter(
            IDataReaderEngine dataReaderEngine,
            IDataWriterEngine dataWriterEngine,
            string pivotDimension,
            bool includeObs,
            int maxObs,
            DateTime dataFrom,
            DateTime dateTo,
            bool includeHeader,
            bool closeOnCompletion)
        {
            this.CopyDatasetToWriter(null, dataReaderEngine, dataWriterEngine, pivotDimension, includeObs, maxObs, dataFrom, dateTo, includeHeader, closeOnCompletion, true);
        }

        /// <summary>
        /// Copies to writer.
        /// </summary>
        /// <param name="dataReaderEngine">
        /// The data reader engine.
        /// </param>
        /// <param name="dataWriterEngine">
        /// The data writer engine.
        /// </param>
        /// <param name="includeObs">
        /// if set to <c>true</c> include the OBS.
        /// </param>
        /// <param name="maxObs">
        /// The maximum OBS.
        /// </param>
        /// <param name="dateFrom">
        /// The date from.
        /// </param>
        /// <param name="dateTo">
        /// The date to.
        /// </param>
        /// <param name="copyHeader">
        /// if set to <c>true</c> copy the header.
        /// </param>
        /// <param name="closeWriter">
        /// if set to <c>true</c> close the writer.
        /// </param>
        public void CopyToWriter(IDataReaderEngine dataReaderEngine, IDataWriterEngine dataWriterEngine, bool includeObs, int maxObs, DateTime dateFrom, DateTime dateTo, bool copyHeader, bool closeWriter)
        {
            this.CopyToWriter(dataReaderEngine, dataWriterEngine, null, includeObs, -1, null, null, copyHeader, closeWriter);
        }

        /// <summary>
        /// Copies to writer.
        /// </summary>
        /// <param name="dataReaderEngine">
        /// The data reader engine.
        /// </param>
        /// <param name="dataWriterEngine">
        /// The data writer engine.
        /// </param>
        /// <param name="includeHeader">
        /// if set to <c>true</c> [include header].
        /// </param>
        /// <param name="closeOnCompletion">
        /// if set to <c>true</c> [close on completion].
        /// </param>
        public void CopyToWriter(IDataReaderEngine dataReaderEngine, IDataWriterEngine dataWriterEngine, bool includeHeader, bool closeOnCompletion)
        {
            this.CopyToWriter(dataReaderEngine, dataWriterEngine, null, true, -1, null, null, includeHeader, closeOnCompletion);
        }

        /// <summary>
        /// Copies to writer.
        /// </summary>
        /// <param name="dataReaderEngine">
        /// The data reader engine.
        /// </param>
        /// <param name="dataWriterEngine">
        /// The data writer engine.
        /// </param>
        /// <param name="pivotDimension">
        /// The pivot dimension.
        /// </param>
        /// <param name="closeOnCompletion">
        /// if set to <c>true</c> [close on completion].
        /// </param>
        public void CopyToWriter(IDataReaderEngine dataReaderEngine, IDataWriterEngine dataWriterEngine, string pivotDimension, bool closeOnCompletion)
        {
            this.CopyToWriter(dataReaderEngine, dataWriterEngine, pivotDimension, true, -1, null, null, true, closeOnCompletion);
        }

        /// <summary>
        /// Writes the keyable to the data writer engine, if includeObs is true and DateTime from and to filter out all observation values for a key, then the key will not be written. 
        /// series.
        /// <p/>
        /// Only writes the observations that fall between the two date parameters
        /// </summary>
        /// <param name="dataReaderEngine">
        /// The data reader engine
        /// </param>
        /// <param name="dataWriterEngine">
        /// The data writer engine
        /// </param>
        /// <param name="keyable">
        /// The keyable
        /// </param>
        /// <param name="maxObs">
        /// If 0 then do not output observations, if null or less then 0 then there is no limit, if greater then 0 then limit
        /// the max OBS to this number
        /// </param>
        /// <param name="dateFrom">
        /// The date from
        /// </param>
        /// <param name="dateTo">
        /// The date to
        /// </param>
        /// <exception cref="SdmxException">
        /// Error occurred whilst trying to read first observation in key
        /// or
        /// Error occurred whilst trying to determine if series key had another observation, last successfully processed observation:  +
        ///                             obs
        /// </exception>
        public void WriteKeyableToWriter(IDataReaderEngine dataReaderEngine, IDataWriterEngine dataWriterEngine, IKeyable keyable, int maxObs, DateTime dateFrom, DateTime dateTo)
        {
            bool writtenKey = false;
            bool filteredObs = false;
            string fromDateTimeFormatted = null;
            string toDateTimeFormatted = null;

            bool seriesKeysOnly = maxObs == 0;
            if (keyable == null)
            {
                throw new ArgumentNullException("keyable");
            }

            if (keyable.Series && !seriesKeysOnly)
            {
                // Filter out the observations from the series
                bool hasMax = maxObs > 0;
                List<IObservation> obsList = new List<IObservation>();
                IObservation obs = null;
                while (true)
                {
                    try
                    {
                        if (dataReaderEngine == null)
                        {
                            throw new ArgumentNullException("dataReaderEngine");
                        }

                        if (!dataReaderEngine.MoveNextObservation())
                        {
                            break;
                        }
                    }
                    catch (Exception th)
                    {
                        if (obs == null)
                        {
                            throw new SdmxException("Error occurred whilst trying to read first observation in key", th);
                        }

                        throw new SdmxException("Error occurred whilst trying to determine if series key had another observation, last successfully processed observation: " + obs, th);
                    }

                    obs = dataReaderEngine.CurrentObservation;
                    if (dateFrom != DateTime.MinValue)
                    {
                        if (fromDateTimeFormatted == null)
                        {
                            var format = obs.ObsTimeFormat;
                            fromDateTimeFormatted = DateUtil.FormatDate(dateFrom, format);
                        }

                        if (string.Compare(obs.ObsTime, fromDateTimeFormatted, StringComparison.Ordinal) < 0)
                        {
                            filteredObs = true;
                            continue;
                        }
                    }

                    if (dateTo != DateTime.MinValue)
                    {
                        if (toDateTimeFormatted == null)
                        {
                            var format = obs.ObsTimeFormat;
                            toDateTimeFormatted = DateUtil.FormatDate(dateTo, format);
                        }

                        if (string.Compare(obs.ObsTime, toDateTimeFormatted, StringComparison.Ordinal) > 0)
                        {
                            filteredObs = true;
                            continue;
                        }
                    }

                    if (!writtenKey)
                    {
                        this.WriteKeyableToWriter(dataWriterEngine, keyable);
                        writtenKey = true;
                    }

                    if (!hasMax)
                    {
                        this.WriteObsToWriter(dataWriterEngine, keyable, obs);
                    }
                    else
                    {
                        obsList.Add(obs);
                    }
                }

                // There was a limit on the number of obs, so now only write out the to 'x' results
                if (obsList.Count > 0)
                {
                    obsList.Sort();
                    obsList.Reverse();
                    int loopCount = obsList.Count > maxObs ? maxObs : obsList.Count;
                    for (int i = 0; i < loopCount; i++)
                    {
                        this.WriteObsToWriter(dataWriterEngine, keyable, obsList[i]);
                    }
                }

                if (!filteredObs && !writtenKey)
                {
                    // If no observations were filtered and the key was not written, write the key out
                    this.WriteKeyableToWriter(dataWriterEngine, keyable);
                }
            }
            else
            {
                // Either it is not a series, or observations are not to be included, either way the key needs to be written out
                this.WriteKeyableToWriter(dataWriterEngine, keyable);
            }
        }

        /// <summary>
        /// Writes the key able to the writer engine, if the key is a series then it will also write any observations under the
        /// series
        /// </summary>
        /// <param name="dataReaderEngine">
        /// The data reader engine
        /// </param>
        /// <param name="dataWriterEngine">
        /// The data writer engine
        /// </param>
        /// <param name="keyable">
        /// The key able.
        /// </param>
        /// <param name="maxObs">
        /// If 0 or null then do not output observations, if less then 0 then there is no limit, if greater
        /// then 0 then limit the max OBS to this number
        /// </param>
        public void WriteKeyableToWriter(IDataReaderEngine dataReaderEngine, IDataWriterEngine dataWriterEngine, IKeyable keyable, int maxObs)
        {
            this.WriteKeyableToWriter(dataReaderEngine, dataWriterEngine, keyable, maxObs, DateTime.MinValue, DateTime.MinValue);
        }

        /// <summary>
        /// Writes the observation to the writer engine
        /// </summary>
        /// <param name="dataWriterEngine">
        /// The data writer engine
        /// </param>
        /// <param name="keyable">
        /// The key able
        /// </param>
        /// <param name="obs">
        /// The observation
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// dataWriterEngine
        /// or
        /// keyable
        /// or
        /// obs
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        /// Dataset is cross sectional, missing cross section value for observation at time ' +
        ///                         keyable.ObsTime
        /// </exception>
        public void WriteObsToWriter(IDataWriterEngine dataWriterEngine, IKeyable keyable, IObservation obs)
        {
            if (dataWriterEngine == null)
            {
                throw new ArgumentNullException("dataWriterEngine");
            }

            if (keyable == null)
            {
                throw new ArgumentNullException("keyable");
            }

            if (obs == null)
            {
                throw new ArgumentNullException("obs");
            }

            if (obs.CrossSection)
            {
                var crossSection = obs.CrossSectionalValue;
                if (crossSection == null)
                {
                    throw new SdmxSemmanticException("Dataset is cross sectional, missing cross section value for observation at time '" + keyable.ObsTime);
                }

                dataWriterEngine.WriteObservation(crossSection.Concept, crossSection.Code, obs.ObservationValue, this.GetAnnotations(obs.Annotations));
            }
            else
            {
                dataWriterEngine.WriteObservation(DimensionObject.TimeDimensionFixedId, obs.ObsTime, obs.ObservationValue, this.GetAnnotations(obs.Annotations));
            }

            if (obs.Attributes != null)
            {
                foreach (IKeyValue kv in obs.Attributes)
                {
                    dataWriterEngine.WriteAttributeValue(kv.Concept, kv.Code);
                }
            }
        }

        /// <summary>
        /// Copies the dataset to writer.
        /// </summary>
        /// <param name="previousDatasetHeader">The previous dataset header.</param>
        /// <param name="dataReaderEngine">The data reader engine.</param>
        /// <param name="dataWriterEngine">The data writer engine.</param>
        /// <param name="pivotDimension">The pivot dimension.</param>
        /// <param name="includeObs">The include obs.</param>
        /// <param name="maxObs">The max obs.</param>
        /// <param name="dateFrom">The date from.</param>
        /// <param name="dateTo">The date to.</param>
        /// <param name="includeHeader">The include header.</param>
        /// <param name="closeOnCompletion">The close on completion.</param>
        /// <param name="forceStartNewDataset">The force start new dataset.</param>
        /// <returns>
        /// The <see cref="IDatasetHeader" />.
        /// </returns>
        /// <exception cref="SdmxException">Error while trying read first series key
        /// or
        /// Error while trying to read next series/group in the DataSet.  The last successfully processed key was: " + currentKey
        /// or
        /// Error occurred while processing " + currentKey</exception>
        private IDatasetHeader CopyDatasetToWriter(
            IDatasetHeader previousDatasetHeader,
            IDataReaderEngine dataReaderEngine,
            IDataWriterEngine dataWriterEngine,
            string pivotDimension,
            bool includeObs,
            int maxObs,
            DateTime? dateFrom,
            DateTime? dateTo,
            bool includeHeader,
            bool closeOnCompletion,
            bool forceStartNewDataset)
        {
            try
            {
                if (includeHeader && dataReaderEngine.Header != null)
                {
                    dataWriterEngine.WriteHeader(dataReaderEngine.Header);
                }

                var datasetHeader = dataReaderEngine.CurrentDatasetHeader;

                var fromIsTimeSeries = datasetHeader.Timeseries;

                var dimensionAtObs = datasetHeader.DataStructureReference.DimensionAtObservation;
                if (pivotDimension == null || (fromIsTimeSeries && pivotDimension.Equals(DimensionObject.TimeDimensionFixedId)) || dimensionAtObs.Equals(pivotDimension))
                {
                    if (forceStartNewDataset || (previousDatasetHeader == null || !this.IsHeaderEqual(datasetHeader, previousDatasetHeader)))
                    {
                        dataWriterEngine.StartDataset(dataReaderEngine.Dataflow, dataReaderEngine.DataStructure, datasetHeader);
                    }

                    foreach (var kv in dataReaderEngine.DatasetAttributes)
                    {
                        dataWriterEngine.WriteAttributeValue(kv.Concept, kv.Code);
                    }

                    IKeyable currentKey = null;
                    while (true)
                    {
                        try
                        {
                            if (!dataReaderEngine.MoveNextKeyable())
                            {
                                break;
                            }

                            currentKey = dataReaderEngine.CurrentKey;
                        }
                        catch (Exception th)
                        {
                            if (currentKey == null)
                            {
                                throw new SdmxException("Error while trying read first series key", th);
                            }

                            throw new SdmxException("Error while trying to read next series/group in the DataSet.  The last sucessfully processed key was: " + currentKey, th);
                        }

                        try
                        {
                            if (includeObs)
                            {
                                this.WriteKeyableToWriter(dataReaderEngine, dataWriterEngine, currentKey, maxObs, dateFrom ?? DateTime.MinValue, dateTo ?? DateTime.MinValue);
                            }
                            else
                            {
                                this.WriteKeyableToWriter(dataWriterEngine, currentKey);
                            }
                        }
                        catch (Exception th)
                        {
                            throw new SdmxException("Error occurred while processing " + currentKey, th);
                        }
                    }

                    return datasetHeader;
                }
                else
                {
                    // Modify the header to change the dimension at observation
                    string dsId = null;
                    IStructureReference structureRef;
                    Uri serviceUrl = null;
                    Uri structureUrl = null;

                    if (datasetHeader.DataStructureReference != null)
                    {
                        dsId = datasetHeader.DataStructureReference.Id;
                        structureRef = datasetHeader.DataStructureReference.StructureReference;
                        serviceUrl = datasetHeader.DataStructureReference.ServiceUrl;
                        structureUrl = datasetHeader.DataStructureReference.StructureUrl;
                    }
                    else
                    {
                        structureRef = dataReaderEngine.DataStructure.AsReference;
                    }

                    var dsStructureRef = new DatasetStructureReferenceCore(dsId, structureRef, serviceUrl, structureUrl, pivotDimension);
                    var modifiedDatasetHeader = datasetHeader.ModifyDataStructureReference(dsStructureRef);

                    if (previousDatasetHeader == null || !this.IsHeaderEqual(datasetHeader, previousDatasetHeader))
                    {
                        dataWriterEngine.StartDataset(dataReaderEngine.Dataflow, dataReaderEngine.DataStructure, modifiedDatasetHeader);
                    }

                    // The data reader is pivoting on a different dimension then the one we want to pivot on - start algorithm
                    // We are pivoting on a different dimension to the current pivot
                    this.Pivot(dataReaderEngine, dataWriterEngine, pivotDimension);
                    return modifiedDatasetHeader;
                }
            }
            finally
            {
                if (closeOnCompletion)
                {
                    dataWriterEngine.Close();
                }
            }
        }

        /// <summary>
        /// The copy to writer.
        /// </summary>
        /// <param name="dataReaderEngine">
        /// The data reader engine.
        /// </param>
        /// <param name="dataWriterEngine">
        /// The data writer engine.
        /// </param>
        /// <param name="pivotDimension">
        /// The pivot dimension.
        /// </param>
        /// <param name="includeObs">
        /// The include obs.
        /// </param>
        /// <param name="maxObs">
        /// The max obs.
        /// </param>
        /// <param name="dateFrom">
        /// The date from.
        /// </param>
        /// <param name="dateTo">
        /// The date to.
        /// </param>
        /// <param name="includeHeader">
        /// The include header.
        /// </param>
        /// <param name="closeOnCompletion">
        /// The close on completion.
        /// </param>
        private void CopyToWriter(
            IDataReaderEngine dataReaderEngine,
            IDataWriterEngine dataWriterEngine,
            string pivotDimension,
            bool includeObs,
            int maxObs,
            DateTime? dateFrom,
            DateTime? dateTo,
            bool includeHeader,
            bool closeOnCompletion)
        {
            dataReaderEngine.Reset();
            var header = dataReaderEngine.Header;
            if (includeHeader && header != null)
            {
                dataWriterEngine.WriteHeader(header);
            }

            try
            {
                IDatasetHeader dsHeader = null;
                List<IKeyValue> attributeValues = null;
                while (dataReaderEngine.MoveNextDataset())
                {
                    bool startNewDataset = false;
                    if (dataReaderEngine.DatasetPosition > 0)
                    {
                        // CHECK PREVIOUS ATTS Vs CURRENT ATTS
                        startNewDataset = !ObjectUtil.ContainsAll(attributeValues, dataReaderEngine.DatasetAttributes);
                    }

                    attributeValues = dataReaderEngine.DatasetAttributes.ToList();
                    dsHeader = this.CopyDatasetToWriter(dsHeader, dataReaderEngine, dataWriterEngine, pivotDimension, includeObs, maxObs, dateFrom, dateTo, false, false, startNewDataset);
                }
            }
            finally
            {
                if (closeOnCompletion)
                {
                    dataWriterEngine.Close();
                }
            }
        }

        /// <summary>
        /// The create new key.
        /// </summary>
        /// <param name="keyable">
        /// The keyable.
        /// </param>
        /// <param name="ignoreConcept">
        /// The ignore concept.
        /// </param>
        /// <param name="includeNewConcept">
        /// The include new concept.
        /// </param>
        /// <param name="newConceptValue">
        /// The new concept value.
        /// </param>
        /// <param name="movingToTimeSeries">
        /// The moving to time series.
        /// </param>
        /// <param name="seriesAttributeConcepts">
        /// The series attribute concepts.
        /// </param>
        /// <returns>
        /// The <see cref="IKeyable"/>.
        /// </returns>
        private IKeyable CreateNewKey(IKeyable keyable, string ignoreConcept, string includeNewConcept, string newConceptValue, bool movingToTimeSeries, List<string> seriesAttributeConcepts)
        {
            var newKeyList = keyable.Key;
            IKeyValue removeKv = keyable.Key.FirstOrDefault(kv => kv.Concept.Equals(ignoreConcept));
            var newAttList = keyable.Attributes.Where(currentAttr => seriesAttributeConcepts.Contains(currentAttr.Concept)).ToList();
            newKeyList.Remove(removeKv);
            newKeyList.Add(movingToTimeSeries ? new KeyValueImpl(keyable.ObsTime, DimensionObject.TimeDimensionFixedId) : new KeyValueImpl(newConceptValue, includeNewConcept));
            return new KeyableImpl(keyable.Dataflow, keyable.DataStructure, newKeyList, newAttList);
        }

        /// <summary>
        /// Gets the annotations.
        /// </summary>
        /// <param name="annotationList">The annotation list.</param>
        /// <returns>
        /// The array <see cref="IAnnotation" />.
        /// </returns>
        private IAnnotation[] GetAnnotations(IList<IAnnotation> annotationList)
        {
            if (annotationList.Count == 0)
            {
                return null;
            }

            var annotationArr = new IAnnotation[annotationList.Count];
            for (int i = 0; i < annotationList.Count; i++)
            {
                annotationArr[i] = annotationList[i];
            }

            return annotationArr;
        }

        /// <summary>
        /// The get key short code.
        /// </summary>
        /// <param name="currentKey">
        /// The current key.
        /// </param>
        /// <param name="promoteCode">
        /// The promote code.
        /// </param>
        /// <param name="ignoreConcept">
        /// The ignore concept.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetKeyShortCode(IKeyable currentKey, string promoteCode, string ignoreConcept)
        {
            var returnString = currentKey.Key.Where(kv => !kv.Concept.Equals(ignoreConcept)).Aggregate(string.Empty, (current, kv) => current + (kv.Code + ":"));
            returnString += promoteCode;
            return returnString;
        }

        ////private List<String> GetObsAttributes(string pivotDimension, IDataStructureObject keyFamily)
        ////{
        ////    return keyFamily.GetObservationAttributes(pivotDimension).Select(currentAttribute => currentAttribute.Id).ToList();
        ////}

        /// <summary>
        /// Gets the series attributes.
        /// </summary>
        /// <param name="pivotDimension">The pivot dimension.</param>
        /// <param name="keyFamily">The key family.</param>
        /// <returns>
        /// The <see cref="List{String}" />.
        /// </returns>
        private List<string> GetSeriesAttributes(string pivotDimension, IDataStructureObject keyFamily)
        {
            return keyFamily.GetSeriesAttributes(pivotDimension).Select(currentAttribute => currentAttribute.Id).ToList();
        }

        /// <summary>
        /// Determines whether [is header equal] [the specified one].
        /// </summary>
        /// <param name="one">
        /// The one.
        /// </param>
        /// <param name="two">
        /// The two.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool IsHeaderEqual(IDatasetHeader one, IDatasetHeader two)
        {
            if (one == null)
            {
                throw new ArgumentNullException("one");
            }

            if (two == null)
            {
                throw new ArgumentNullException("two");
            }

            if (!ObjectUtil.Equivalent(one.Action, two.Action))
            {
                return false;
            }

            if (!ObjectUtil.Equivalent(one.DataProviderReference, two.DataProviderReference))
            {
                return false;
            }

            if (!ObjectUtil.Equivalent(one.DatasetId, two.DatasetId))
            {
                return false;
            }

            if (!this.IsHeaderEqual(one.DataStructureReference, two.DataStructureReference))
            {
                return false;
            }

            if (!ObjectUtil.Equivalent(one.PublicationPeriod, two.PublicationPeriod))
            {
                return false;
            }

            if (!ObjectUtil.Equivalent(one.PublicationYear, two.PublicationYear))
            {
                return false;
            }

            if (!ObjectUtil.Equivalent(one.ReportingBeginDate, two.ReportingBeginDate))
            {
                return false;
            }

            if (!ObjectUtil.Equivalent(one.ReportingEndDate, two.ReportingEndDate))
            {
                return false;
            }

            if (!ObjectUtil.Equivalent(one.ValidFrom, two.ValidFrom))
            {
                return false;
            }

            if (!ObjectUtil.Equivalent(one.ValidTo, two.ValidTo))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Determines whether the specified <see cref="IDatasetStructureReference"/> are equal.
        /// </summary>
        /// <param name="one">The one.</param>
        /// <param name="two">The two.</param>
        /// <returns>
        /// True if they are equal; false otherwise
        /// </returns>
        private bool IsHeaderEqual(IDatasetStructureReference one, IDatasetStructureReference two)
        {
            if (one == null)
            {
                if (two != null)
                {
                    return false;
                }

                return true;
            }

            if (two == null)
            {
                return false;
            }

            if (one.Timeseries != two.Timeseries)
            {
                return false;
            }

            if (!ObjectUtil.Equivalent(one.StructureReference, two.StructureReference))
            {
                return false;
            }

            if (!ObjectUtil.Equivalent(one.ServiceUrl, two.ServiceUrl))
            {
                return false;
            }

            if (!ObjectUtil.Equivalent(one.StructureUrl, two.StructureUrl))
            {
                return false;
            }

            if (!ObjectUtil.Equivalent(one.DimensionAtObservation, two.DimensionAtObservation))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Pivots the specified data reader engine.
        /// </summary>
        /// <param name="dataReaderEngine">The data reader engine.</param>
        /// <param name="dataWriterEngine">The data writer engine.</param>
        /// <param name="pivotDimension">The pivot dimension.</param>
        private void Pivot(IDataReaderEngine dataReaderEngine, IDataWriterEngine dataWriterEngine, string pivotDimension)
        {
            var datasetHeader = dataReaderEngine.CurrentDatasetHeader;

            var currentCrossSectionConcept = datasetHeader.DataStructureReference.DimensionAtObservation;
            var pivotIsTime = pivotDimension.Equals(DimensionObject.TimeDimensionFixedId);
            var fromIsTimeSeries = datasetHeader.Timeseries;

            // Determine the series and observation attributes based on the new cross section, this is only possible if the data reader engine contains the keyFamily
            IDataStructureObject keyFamily = dataReaderEngine.DataStructure;
            List<string> seriesAttributeConcepts = null;

            if (keyFamily != null)
            {
                // TODO what if there is no Key Family???
                seriesAttributeConcepts = this.GetSeriesAttributes(pivotDimension, keyFamily);

                ////this.GetObsAttributes(pivotDimension, keyFamily);
            }

            var shortCodes = new HashSet<string>();

            var keyMap = new Dictionary<string, IKeyable>();
            var obsMap = new Dictionary<string, Set<IObservation>>();

            while (dataReaderEngine.MoveNextKeyable())
            {
                var key = dataReaderEngine.CurrentKey;
                if (!key.Series)
                {
                    // Copy the Group verbatim and continue loop. There is no more useful information that can be obtained from the group
                    this.WriteKeyableToWriter(dataReaderEngine, dataWriterEngine, key, 0);
                    continue;
                }

                var codeForPivot = pivotIsTime ? key.ObsTime : key.GetKeyValue(pivotDimension);

                while (dataReaderEngine.MoveNextObservation())
                {
                    var obs = dataReaderEngine.CurrentObservation;

                    var currentObsCode = fromIsTimeSeries ? obs.ObsTime : obs.CrossSectionalValue.Code;
                    var newKeyShortCode = this.GetKeyShortCode(key, currentObsCode, pivotDimension);
                    var observations = obsMap.ContainsKey(newKeyShortCode) ? obsMap[newKeyShortCode] : new Set<IObservation>();
                    if (!shortCodes.Contains(newKeyShortCode))
                    {
                        shortCodes.Add(newKeyShortCode);
                        keyMap.Add(newKeyShortCode, this.CreateNewKey(key, pivotDimension, currentCrossSectionConcept, currentObsCode, pivotIsTime, seriesAttributeConcepts));
                        observations = new Set<IObservation>();
                        obsMap.Add(newKeyShortCode, observations);
                    }

                    // TODO Obs Attributes
                    observations.Add(new ObservationImpl(key, codeForPivot, obs.ObservationValue, obs.Attributes));
                }
            }

            foreach (var currentKey in keyMap.Keys)
            {
                IKeyable key = keyMap.ContainsKey(currentKey) ? keyMap[currentKey] : null;
                this.WriteKeyableToWriter(dataWriterEngine, key);
                var observations = obsMap.ContainsKey(currentKey) ? obsMap[currentKey] : new Set<IObservation>();
                foreach (var obs in observations)
                {
                    this.WriteObsToWriter(dataWriterEngine, key, obs);
                }
            }
        }

        /// <summary>
        /// Writes the keyable to writer.
        /// </summary>
        /// <param name="dataWriterEngine">
        /// The data writer engine.
        /// </param>
        /// <param name="keyable">
        /// The keyable.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        /// Dataset is cross sectional, cross section is missing a time
        /// </exception>
        private void WriteKeyableToWriter(IDataWriterEngine dataWriterEngine, IKeyable keyable)
        {
            if (keyable.Series)
            {
                dataWriterEngine.StartSeries(this.GetAnnotations(keyable.Annotations));
                foreach (var kv in keyable.Key)
                {
                    dataWriterEngine.WriteSeriesKeyValue(kv.Concept, kv.Code);
                }

                if (!keyable.TimeSeries)
                {
                    if (keyable.ObsTime == null)
                    {
                        throw new SdmxSemmanticException("Dataset is cross sectional, cross section is missing a time");
                    }

                    dataWriterEngine.WriteSeriesKeyValue(DimensionObject.TimeDimensionFixedId, keyable.ObsTime);
                }
            }
            else
            {
                dataWriterEngine.StartGroup(keyable.GroupName);
                foreach (var kv in keyable.Key)
                {
                    dataWriterEngine.WriteGroupKeyValue(kv.Concept, kv.Code);
                }
            }

            if (keyable.Attributes != null)
            {
                foreach (var kv in keyable.Attributes)
                {
                    dataWriterEngine.WriteAttributeValue(kv.Concept, kv.Code);
                }
            }
        }
    }
}