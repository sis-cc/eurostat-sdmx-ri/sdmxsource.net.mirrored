// -----------------------------------------------------------------------
// <copyright file="SdmxJsonMetadatasetsParserFactory.cs" company="EUROSTAT">
//   Date Created : 2020-02-24
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxDataParser.
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Factory;
using Org.Sdmxsource.Sdmx.Api.Model.Format;
using Org.Sdmxsource.Sdmx.Api.Model.Metadata;
using Org.Sdmxsource.Sdmx.Api.Util;
using Org.Sdmxsource.Sdmx.DataParser.Engine;

namespace Org.Sdmxsource.Sdmx.DataParser.Factory
{
    public class SdmxJsonMetadatasetsParserFactory : IMetadataParsingFactory
    {

        public SdmxJsonMetadatasetsParserFactory()
        {
            
        }
        
        public IMetadataParseEngine GetMetadataWriterEngine(IMetadataFormat structureFormatType)
        {
            if (structureFormatType == null)
            {
                throw new ArgumentNullException("dataLocation");
            }

            if (structureFormatType.SdmxFormat.EnumType == SdmxSchemaEnumType.Json)
            {
                return new MetadataParserEngine();
            }

            return null;
        }
        
    }
}
