﻿namespace Org.Sdmxsource.Sdmx.DataParser.Engine
{
    using Api.Constants.InterfaceConstant;
    using Api.Engine;
    using Api.Model.Objects.Base;

    using SdmxObjects.Model.Objects.Base;

    /// <inheritdoc />
    /// <summary>
    /// Data writer that calculates the TIME_PERIOD using the defaut value for StartYear
    /// </summary>
    /// <seealso cref="T:Org.Sdmxsource.Sdmx.DataParser.Engine.ReportingPeriodDataWriterEngineBase" />
    public class GregorianPeriodNoStartYearDataWriterEngine : GregorianPeriodDataWriterEngine
    {
        /// <inheritdoc />
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Org.Sdmxsource.Sdmx.DataParser.Engine.GregorianPeriodNoStartYearDataWriterEngine" /> class.
        /// </summary>
        /// <param name="decoratedEngine">The decorated engine.</param>
        public GregorianPeriodNoStartYearDataWriterEngine(IDataWriterEngine decoratedEngine) : base(decoratedEngine)
        {
        }

        /// <summary>
        /// </summary>
        /// <param name="id"></param>
        /// <param name="valueRen"></param>
        /// <inheritdoc />
        public override void WriteAttributeValue(string id, string valueRen)
        {
            if (id != AttributeObject.Repyearstart)
            {
                this.DecoratedEngine.WriteAttributeValue(id, valueRen);
            }
        }
    }
}