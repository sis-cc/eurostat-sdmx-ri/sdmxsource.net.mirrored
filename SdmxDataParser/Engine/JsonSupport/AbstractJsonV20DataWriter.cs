// -----------------------------------------------------------------------
// <copyright file="AbstractJsonDataWriter.cs" company="EUROSTAT">
//   Date Created : 2016-08-17
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Engine.JsonSupport
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;

    using log4net;
    using Newtonsoft.Json.Linq;

    using Org.Sdmxsource.Json;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Date;
    using Org.Sdmxsource.Translator;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// Abstract JsonDataWriter class.
    /// </summary>
    public abstract class AbstractJsonV20DataWriter : DatasetInfoDataWriterEngine
    {
        private static readonly HashSet<TextEnumType> _nonStringTypes = new HashSet<TextEnumType>(new []
        {
            TextEnumType.BigInteger,
            TextEnumType.Count,
            TextEnumType.Decimal,
            TextEnumType.Double,
            TextEnumType.ExclusiveValueRange,
            TextEnumType.Float,
            TextEnumType.InclusiveValueRange,
            TextEnumType.Incremental,
            TextEnumType.Integer,
            TextEnumType.Long,
            TextEnumType.Short
        });

        private readonly bool _useHierarchicalCodelists;

        /// <summary>
        /// The _log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly Dictionary<string, Dictionary<string, Tuple<ICode, int>>> _codelistItemOrderMap;
        private readonly IList<IAnnotation> _annotations;
        private readonly IList<IFooterMessage> _errors;
        private readonly ISet<string> _missingCodelistCodes;
        private int _structureIndex = -1;
        private int _datasetIndex = -1;
        private List<int> _currentStructureDatasets;

        /// <summary>
        /// Flag indicating whether the dataset header was already written to JSON file
        /// </summary>
        protected bool jsonDatasetHeaderWritten;

        /// <summary>
        /// Flag indicating whether a dataset object was started in JSON result (and should be closed)
        /// </summary>
        protected bool jsonDatasetStarted;

        /// <summary>
        /// Flag indicating whether the JSON representation of the measure type (ie. text type) is string
        /// </summary>
        protected bool isPrimaryMeasureStringType;

        /// <summary>
        /// The group attribute array started
        /// </summary>
        private bool _groupAttributeArrayStarted;


        private bool _isGroupKeyAnnotations = false;

        /// <summary>
        /// Storage for group keys
        /// </summary>
        private List<GroupAttributeKey> _groupKeys;

        /// <summary>
        /// 
        /// </summary>
        protected Dictionary<string, int> _groupAttributes;
        private Dictionary<string, KeyValuePair<IMetadataAttributeObject, IConceptObject>> _metadataAttributes;
        private Dictionary<string, Dictionary<string, HclSupport.HclCode>> _hclDictionary;

        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractJsonDataWriter"/> class.
        /// </summary>
        /// <param name="jsonGenerator">
        /// The json generator.
        /// </param>
        /// <param name="superObjectRetrievalManager">
        /// The superObjectRetrievalManager.
        /// </param>
        /// <param name="objectRetrievalManager">
        /// The objectRetrievalManager.
        /// </param>
        /// <param name="translator">
        /// The translator.
        /// </param>
        /// <param name="useHierarchicalCodelists">Use Hierarchical codelists through dataflow/dsd annotation</param>
        protected AbstractJsonV20DataWriter(
            JsonGenerator jsonGenerator, 
            ISdmxSuperObjectRetrievalManager superObjectRetrievalManager, 
            ISdmxObjectRetrievalManager objectRetrievalManager, 
            ITranslator translator,
            bool useHierarchicalCodelists
        ) : base(superObjectRetrievalManager, objectRetrievalManager)
        {
            this._codelistItemOrderMap = new Dictionary<string, Dictionary<string, Tuple<ICode, int>>>();
            this._annotations = new List<IAnnotation>();
            this._errors = new List<IFooterMessage>();
            this._missingCodelistCodes = new HashSet<string>();
            this._groupKeys = new List<GroupAttributeKey>();
            this._groupAttributes = new Dictionary<string, int>();

            this.JsonGenerator = jsonGenerator;
            this.Translator = translator;
            this.StructureJson = new StringWriter();
            this.StructureJsonGenerator = new JsonGenerator(this.StructureJson);
            this._useHierarchicalCodelists = useHierarchicalCodelists;

            jsonDatasetHeaderWritten = false;
            jsonDatasetStarted = false;
        }

        /// <summary>
        /// Gets the translator.
        /// </summary>
        public ITranslator Translator { get;}

        /// <summary>
        /// Gets the current key.
        /// </summary>
        protected string CurrentKey { get; private set; }

        /// <summary>
        /// Main message json generator.
        /// </summary>
        protected JsonGenerator JsonGenerator { get;}

        private StringWriter StructureJson { get; }

        /// <summary>
        /// Structure json generator, injected into JsonGenerator when writer is closed
        /// </summary>
        protected JsonGenerator StructureJsonGenerator { get; }

        /// <summary>
        /// Gets or sets the previous key.
        /// </summary>
        protected string PrevKey { get; set; }

        /// <summary>
        /// Starts a dataset with the data conforming to the DSD.
        /// </summary>
        /// <param name="dataflow">
        /// The dataflow.
        /// </param>
        /// <param name="dsd">
        /// The data structure definition.
        /// </param>
        /// <param name="header">
        /// The header.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public override void StartDataset(IDataflowObject dataflow, IDataStructureObject dsd, IDatasetHeader header, params IAnnotation[] annotations)
        {
            if (!this.jsonDatasetHeaderWritten)
            {
                _log.Debug("[dataSets]");
                this.JsonGenerator.WriteObjectFieldStart("data");
                this.JsonGenerator.WriteArrayFieldStart("dataSets");

                this.jsonDatasetHeaderWritten = true;
            }

            base.StartDataset(dataflow, dsd, header, null);

            this.WriteJsonDataSet(header, MergeStructureAnnotations(dataflow?.Annotations, dsd.Annotations));
        }

        /// <summary>
        /// Inits dataset internal objects needed for writer
        /// </summary>
        /// <param name="dataflow"></param>
        /// <param name="dsd"></param>
        protected override void InitDataset(IDataflowObject dataflow, IDataStructureObject dsd)
        {
            if (this._isNewDataset)
            {
                base.InitDataset(dataflow, dsd);

                this.BindHierarchicalCodelists(dataflow, dsd);

                // TextFormat is null when representation is enumeration (codelist)
                var measureTextType = CurrentDsdSuperObject.PrimaryMeasure.TextFormat?.TextType.EnumType;

                isPrimaryMeasureStringType = measureTextType == null || _nonStringTypes.All(t => t != measureTextType);

                this._currentStructureDatasets = new List<int>();
                this._structureIndex++;
            }

            this._currentStructureDatasets.Add(++this._datasetIndex);
        }

        /// <summary>
        /// Closes current dataset
        /// </summary>
        protected override void CloseDataSet(bool force)
        {
            if (!this.jsonDatasetStarted)
            {
                return;
            }

            this.CloseSeries();
            this.CloseGroups();

            // Close previous Dataset JSON object
            this.JsonGenerator.WriteEnd();

            if (force)
            {
                this.WriteStructure();
            }

            this.PrevKey = null;
            this.CurrentKey = null;

            this.jsonDatasetStarted = false;
            this._groupAttributeArrayStarted = false;
            this._isGroupKeyAnnotations = false;
            this._groupKeys = new List<GroupAttributeKey>();
            this._groupAttributes = new Dictionary<string, int>();
        }

        private void WriteJsonDataSet(IDatasetHeader datasetHeader, IList<IAnnotation> annotations)
        {
            _log.Debug("{}");
            this.JsonGenerator.WriteStartObject();

            this.jsonDatasetStarted = true;
            this.JsonGenerator.WriteNumberField("structure", _structureIndex);
            this.JsonGenerator.WriteStringField("action", (datasetHeader?.Action ?? DatasetActionEnumType.Information).Action);

            if (datasetHeader != null)
            {
                if (datasetHeader.ReportingBeginDate != default(DateTime))
                {
                    this.JsonGenerator.WriteStringField("reportingBegin", DateUtil.FormatDate(datasetHeader.ReportingBeginDate));
                }

                if (datasetHeader.ReportingEndDate != default(DateTime))
                {
                    this.JsonGenerator.WriteStringField("reportingEnd", DateUtil.FormatDate(datasetHeader.ReportingEndDate));
                }

                if (datasetHeader.ValidFrom != default(DateTime))
                {
                    this.JsonGenerator.WriteStringField("validFrom", DateUtil.FormatDate(datasetHeader.ValidFrom));
                }

                if (datasetHeader.ValidTo != default(DateTime))
                {
                    this.JsonGenerator.WriteStringField("validTo", DateUtil.FormatDate(datasetHeader.ValidTo));
                }

                if (datasetHeader.PublicationYear > 0)
                {
                    this.JsonGenerator.WriteNumberField("publicationYear", datasetHeader.PublicationYear);
                }

                if (ObjectUtil.ValidString(datasetHeader.PublicationPeriod))
                {
                    this.JsonGenerator.WriteStringField("publicationPeriod", datasetHeader.PublicationPeriod);
                }

                if (datasetHeader.DataStructureReference != null)
                {
                    this.JsonGenerator.WriteArrayFieldStart("links");

                    this.JsonGenerator.WriteStartObject();

                    this.JsonGenerator.WriteStringField("urn", datasetHeader.DataStructureReference.StructureReference.TargetUrn.ToString());
                    this.JsonGenerator.WriteStringField("rel", datasetHeader.DataStructureReference.StructureReference.TargetReference.UrnClass);

                    this.JsonGenerator.WriteEndObject();

                    this.JsonGenerator.WriteEndArray();
                }
            }

            if (ObjectUtil.ValidCollection(annotations))
            {
                _log.Debug("[annotations]");
                this.JsonGenerator.WriteArrayFieldStart("annotations");
                this.WriteAnnotationReferences(annotations, this.JsonGenerator);
                _log.Debug("[/annotations]");
                this.JsonGenerator.WriteEndArray();
            }
        }

        public override void WriteGroupKeyValue(string dimensionId, string dimensionValue)
        {
            this.WriteSeriesKeyValue(dimensionId, dimensionValue);
        }

        protected override void StoreComponentValue(string componentId, string componentValue, bool isDimension)
        {
            if (isDimension && SpecialDimensionValue.IsSpecialDimensionValue(componentValue))
            {
                return;
            }

            base.StoreComponentValue(componentId, componentValue, isDimension);
        }

        /// <summary>
        /// Starts the series.
        /// </summary>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public override void StartGroup(string groupId, params IAnnotation[] annotations)
        {
            base.StartGroup(groupId, annotations);

            _flushKeyRequired = true;

            if (!this._groupAttributeArrayStarted)
            {
                this.JsonGenerator.WriteObjectFieldStart("dimensionGroupAttributes");

                this._groupAttributeArrayStarted = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void CloseGroups()
        {
            if (!this._groupAttributeArrayStarted)
            {
                return;
            }

            foreach (var key in _groupKeys)
            {
                this.JsonGenerator.WriteArrayFieldStart(key.GroupKey);

                var tempArr = new int?[this._isGroupKeyAnnotations ? _groupAttributes.Count : key.MaxIndex + 1];

                foreach (var a in key.Attributes)
                {
                    tempArr[a.Key] = a.Value;
                }

                foreach (var attrValue in tempArr)
                {
                    if (attrValue.HasValue)
                    {
                        this.JsonGenerator.WriteNumber(attrValue.Value);
                    }
                    else
                    {
                        this.JsonGenerator.WriteNull();
                    }
                }

                if (this._isGroupKeyAnnotations && key.Annotations != null)
                {
                    this.WriteAnnotationReferences(key.Annotations, this.JsonGenerator);
                }

                this.JsonGenerator.WriteEndArray();
            }

            // Close dimensionGroupAttributes array
            this.JsonGenerator.WriteEndObject();

            this._groupAttributeArrayStarted = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="annotations"></param>
        public override void StartSeries(params IAnnotation[] annotations)
        {
            base.StartSeries(annotations);
            this.CloseGroups();
        }

        /// <summary>
        /// Closes started series/observations array object.
        /// </summary>
        protected abstract void CloseSeries();

        /// <summary>
        /// Closes last observation written and observations object when series format 
        /// </summary>
        protected abstract void CloseObservations();

        private Tuple<ICode, int> FindCodelistItemByValue(ICodelistSuperObject codelist, string code)
        {
            if (codelist == null || string.IsNullOrEmpty(code))
                return null;

            if(!_codelistItemOrderMap.ContainsKey(codelist.Id))
            {
                var dict = codelist.BuiltFrom.Items
                    .Select((c, i) => new Tuple<ICode, int>(c, i))
                    .ToDictionary(x => x.Item1.Id, x => x);

                _codelistItemOrderMap.Add(codelist.Id, dict);
            }

            return _codelistItemOrderMap[codelist.Id].TryGetValue(code, out var result)
                ? result
                : null;
        }

        /// <summary>
        /// Writes dataset attributes.
        /// </summary>
        /// <param name="datasetAttributes">
        /// The dataset attributes.
        /// </param>
        protected override void WriteDatasetAttributes(IList<IKeyValue> datasetAttributes)
        {
            base.WriteDatasetAttributes(datasetAttributes);

            _log.Debug("[attributes]");
            this.JsonGenerator.WriteArrayFieldStart("attributes");

            foreach (var attr in this.CurrentDsdSuperObject.DatasetAttributes)
            {
                var currentKeyValue = datasetAttributes.FirstOrDefault(a => attr.Id == a.Concept);

                if (currentKeyValue == null || string.IsNullOrWhiteSpace(currentKeyValue.Code))
                {
                    this.JsonGenerator.WriteNull();
                }
                else
                {
                    var reportedIndex = this.GetReportedIndex(currentKeyValue.Concept, currentKeyValue.Code);
                    this.JsonGenerator.WriteNumber(reportedIndex);
                }
            }

            _log.Debug("[/attributes]");
            this.JsonGenerator.WriteEndArray();
        }

        /// <summary>
        /// Writes the dimensions.
        /// </summary>
        protected abstract void WriteDimensionsDataset();

        /// <summary>
        /// Returns string that will be used as raw value in output Json
        /// </summary>
        /// <param name="observationValue"></param>
        /// <returns></returns>
        protected string GetObservationValueRawJsonString(string observationValue)
        {
            if (observationValue == null)
            {
                return "null";
            }

            if (observationValue.Length == 0)
            {
                // Return null instead of "" when measure type is not a string
                return isPrimaryMeasureStringType ? "\"\"" : "null";
            }

            return isPrimaryMeasureStringType || observationValue.Equals(SdmxConstants.MissingDataValue, StringComparison.OrdinalIgnoreCase) ? $"\"{observationValue}\"" : observationValue;
        }

        /// <summary>
        /// Writes the observations.
        /// </summary>
        protected abstract void WriteDimensionsObservation();

        /// <summary>
        /// Writes the series.
        /// </summary>
        protected abstract void WriteDimensionsSeries();

        /// <summary>
        /// Writes the key.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        protected override void WriteKey(IKeyable key)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }

            

            if (!key.Series)
            {
                this._isGroupKeyAnnotations |= key.Annotations != null && key.Annotations.Any();

                var groupKeyArr = new string[this._dimensionOrder.Count];

                foreach (var kv in key.Key)
                {
                    groupKeyArr[this._dimensionOrder[kv.Concept]] = SpecialDimensionValue.IsSpecialDimensionValue(kv.Code)
                        ? kv.Code
                        : this.GetReportedIndex(kv.Concept, kv.Code).ToString();
                }

                var groupKey = string.Join(":", groupKeyArr);
                var maxIndex = -1;
                var attrArray = new List<KeyValuePair<int, int>>();

                foreach (var attribute in key.Attributes)
                {
                    if (!_groupAttributes.TryGetValue(attribute.Concept, out var index))
                    {
                        index = _groupAttributes.Count;
                        _groupAttributes.Add(attribute.Concept, index);
                    }

                    if (index > maxIndex)
                    {
                        maxIndex = index;
                    }

                    attrArray.Add(new KeyValuePair<int, int>(index, this.GetReportedIndex(attribute.Concept, attribute.Code)));
                }

                this._groupKeys.Add(new GroupAttributeKey()
                {
                    GroupKey = groupKey,
                    MaxIndex = maxIndex,
                    Attributes = attrArray,
                    Annotations = key.Annotations
                });

                return;
            }

            base.WriteKey(key);

            var sb = new StringBuilder();
            var delimiter = string.Empty;

            foreach (var kv in key.Key.OrderBy(x=>this._dimensionOrder[x.Concept]))
            {
                var currentIndex = SpecialDimensionValue.IsSpecialDimensionValue(kv.Code) 
                    ? kv.Code 
                    : this.GetReportedIndex(kv.Concept, kv.Code).ToString();

                sb.Append(delimiter + currentIndex);
                delimiter = ":";
            }

            if (this.CurrentKey != null)
            {
                this.PrevKey = this.CurrentKey;
            }

            this.CurrentKey = sb.ToString();
        }

        #region data/structures

        /// <summary>
        /// Writes data/structures node
        /// </summary>
        protected void WriteStructures()
        {
            _log.Debug("{structures}");
            
            this.JsonGenerator.WritePropertyName("structures");

            this.StructureJsonGenerator.WriteEndArray();
            this.StructureJsonGenerator.Flush();

            var json = this.StructureJson.ToString();

            this.JsonGenerator.WriteRawValue(json);

            _log.Debug("{/structures}");
        }

        /// <summary>
        /// Writes the structure.
        /// </summary>
        private void WriteStructure()
        {
            if (this._structureIndex == 0)
            {
                this.StructureJsonGenerator.WriteStartArray();
            }

            this.StructureJsonGenerator.WriteStartObject();

            var df = this.Dataflow ?? (IMaintainableObject) this.CurrentDsdSuperObject.BuiltFrom;

            WriteLocalizedText("name", "names", df.Names, this.StructureJsonGenerator, true);
            WriteLocalizedText("description", "descriptions", df.Descriptions, this.StructureJsonGenerator);

            this.WriteStructureDimensions();
            this.WriteStructureAttributes();
            this.WriteStructureAnnotations();

            this.StructureJsonGenerator.WriteArrayFieldStart("dataSets");
            foreach (var index in this._currentStructureDatasets)
            {
                this.StructureJsonGenerator.WriteNumber(index);
            }
            this.StructureJsonGenerator.WriteEndArray();

            this.StructureJsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// Writes the structure.
        /// </summary>
        protected void WriteStructureDimensions()
        {
            _log.Debug("{dimensions}");
            this.StructureJsonGenerator.WriteObjectFieldStart("dimensions");

            this.WriteDimensionsDataset();
            this.WriteDimensionsSeries();
            this.WriteDimensionsObservation();

            _log.Debug("{/dimensions}");
            this.StructureJsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// Writes the attributes.
        /// </summary>
        protected abstract void WriteStructureAttributes();

        /// <summary>
        /// Writes structure/attributes/dataSet node
        /// </summary>
        protected virtual void WriteStructureDatasetAttributes()
        {
            this.StructureJsonGenerator.WriteArrayFieldStart("dataSet");
            foreach (var attr in this.DataStructureDefinition.DatasetAttributes)
            {
                var superAttr = (IAttributeSuperObject) this.CurrentDsdSuperObject.GetComponentById(attr.Id);

                if (superAttr != null)
                {
                    this.WriteComponent(superAttr, -1);
                }
            }
            this.StructureJsonGenerator.WriteEndArray();
        }

        /// <summary>
        /// Writes structure/attributes/dimensionGroup node 
        /// </summary>
        protected virtual void WriteStructureDimensionGroupAttributes()
        {
            this.StructureJsonGenerator.WriteArrayFieldStart("dimensionGroup");
            foreach (var attribute in this._groupAttributes.OrderBy(kv => kv.Value))
            {
                var superAttr = this.CurrentDsdSuperObject.GetComponentById(attribute.Key) as IAttributeSuperObject;

                if (superAttr != null)
                {
                    this.WriteComponent(superAttr, -1);
                }
                // metadata attribute
                else
                {
                    this.WriteMetaAttribute(attribute.Key);
                }
            }
            this.StructureJsonGenerator.WriteEndArray();
        }

        /// <summary>
        /// Writes metadata attribute node
        /// </summary>
        /// <param name="metaAttributeId"></param>
        protected virtual void WriteMetaAttribute(string metaAttributeId)
        {
            if (!this._metadataAttributes.TryGetValue(metaAttributeId, out var metaAttribute))
            {
                throw new InvalidOperationException($"Unknown metadata attribute `{metaAttributeId}`");
            }

            this.StructureJsonGenerator.WriteStartObject();
            this.StructureJsonGenerator.WriteStringField("id", metaAttributeId);

            WriteLocalizedText("name", "names",  metaAttribute.Value.Names, this.StructureJsonGenerator, true);

            this.StructureJsonGenerator.WriteArrayFieldStart("values");
            
            foreach (var value in this.GetReportedValues(metaAttributeId))
            {
                this.StructureJsonGenerator.WriteStartObject();
                this.StructureJsonGenerator.WritePropertyName("value");

                var json = ParseMetaAsJson(value);

                if (json != null)
                {
                    this.StructureJsonGenerator.WriteRawValue(json.ToString());
                }
                else
                {
                    this.StructureJsonGenerator.WriteString(value);
                }
                
                this.StructureJsonGenerator.WriteEndObject();
            }

            this.StructureJsonGenerator.WriteEndArray();

            // close meta attribute
            this.StructureJsonGenerator.WriteEndObject();
        }

        private JToken ParseMetaAsJson(string content)
        {
            if (string.IsNullOrEmpty(content))
            {
                return null;
            }

            try
            {
                return JToken.Parse("{" + content + "}");
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Writes a component.
        /// </summary>
        /// <param name="component">
        /// The component.
        /// </param>
        /// <param name="position">
        /// The position.
        /// </param>
        protected void WriteComponent(IComponentSuperObject component, int position)
        {
            if (component == null)
            {
                throw new ArgumentNullException("component");
            }

            _log.Debug("{}");
            this.StructureJsonGenerator.WriteStartObject();
            this.StructureJsonGenerator.WriteStringField("id", component.Id);

            WriteLocalizedText("name", "names", component.Concept.Names, this.StructureJsonGenerator, true);

            if (position >= 0)
            {
                this.StructureJsonGenerator.WriteNumberField("keyPosition", position);
            }

            var isTime = component.Id.Equals(DimensionObject.TimeDimensionFixedId);

            this.StructureJsonGenerator.WriteArrayFieldStart("roles");
            this.StructureJsonGenerator.WriteString(component.Concept.Id);
            this.StructureJsonGenerator.WriteEndArray();

            var attribute = component as IAttributeSuperObject;

            if (attribute != null)
            {
                this.StructureJsonGenerator.WriteObjectFieldStart("relationship");


                if (attribute.AttachmentLevel == AttributeAttachmentLevel.DataSet)
                {
                    this.StructureJsonGenerator.WriteObjectFieldStart("dataflow");
                    this.StructureJsonGenerator.WriteEndObject();
                }
                else if (attribute.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
                {
                    this.StructureJsonGenerator.WriteArrayFieldStart("dimensions");

                    foreach (var dimension in attribute.DimensionReferences)
                    {
                        this.StructureJsonGenerator.WriteString(dimension);
                    }

                    this.StructureJsonGenerator.WriteEndArray();
                }
                else if (attribute.AttachmentLevel == AttributeAttachmentLevel.Group)
                {
                    var dsd = attribute.BuiltFrom.IdentifiableParent.IdentifiableParent as IDataStructureObject;

                    if (dsd == null)
                    {
                        throw new ArgumentException($"Missing DSD of attribute {attribute.BuiltFrom.GetFullIdPath(true)}");
                    }
                    var group = dsd.Groups.FirstOrDefault(g => g.Id.Equals(attribute.AttachmentGroup, StringComparison.InvariantCultureIgnoreCase));

                    if (group == null)
                    {
                        throw new ArgumentException($"Missing Group ({attribute.AttachmentGroup}) of attribute {attribute.BuiltFrom.GetFullIdPath(true)}");
                    }

                    this.StructureJsonGenerator.WriteArrayFieldStart("dimensions");

                    foreach (var dimension in group.DimensionRefs)
                    {
                        this.StructureJsonGenerator.WriteString(dimension);
                    }

                    this.StructureJsonGenerator.WriteEndArray();
                }
                else if (attribute.AttachmentLevel == AttributeAttachmentLevel.Observation)
                {
                    this.StructureJsonGenerator.WriteObjectFieldStart("observation");
                    this.StructureJsonGenerator.WriteEndObject();
                }

                this.StructureJsonGenerator.WriteEndObject();
            }

            _log.Debug("[values]");
            this.StructureJsonGenerator.WriteArrayFieldStart("values");
            var allCodes = new List<string>(this.GetReportedValues(component.Id));

            var codelist = isTime ? null : component.GetCodelist(true);
            var map = !isTime && _useHierarchicalCodelists
                ? this._hclDictionary.ContainsKey(component.Id) ? this._hclDictionary[component.Id] : HclSupport.RetrieveCodes(codelist)
                : null;

            if (map != null)
            {
                var hash = new HashSet<string>(allCodes);
                var notReportedCodes = new List<string>();

                foreach (var p in allCodes.SelectMany(currentCode => HclSupport.GetAllParents(map, currentCode)))
                {
                    if (hash.Add(p))
                    {
                        notReportedCodes.Add(p);
                    }
                }

                allCodes.AddRange(notReportedCodes);
            }

            foreach (var currentCode in allCodes)
            {
                _log.Debug("{}");
                this.StructureJsonGenerator.WriteStartObject();

                if (isTime)
                {
                    var start = DateUtil.FormatDate(DateUtil.FormatDate(currentCode, true), TimeFormatEnumType.DateTime);
                    var end = DateUtil.FormatDate(DateUtil.FormatDate(currentCode, false), TimeFormatEnumType.DateTime);
                    this.StructureJsonGenerator.WriteStringField("start", start);
                    this.StructureJsonGenerator.WriteStringField("end", end);
                    this.StructureJsonGenerator.WriteStringField("id", currentCode);
                    this.WriteLocalizedText("name", "names", currentCode, this.StructureJsonGenerator);
                }
                else
                {
                    var code = FindCodelistItemByValue(codelist, currentCode);

                    if (code != null)
                    {
                        this.StructureJsonGenerator.WriteStringField("id", code.Item1.Id);
                        this.StructureJsonGenerator.WriteNumberField("order", code.Item2);
                        WriteLocalizedText("name", "names", code.Item1.Names, this.StructureJsonGenerator, true);

                        if (!string.IsNullOrEmpty(code.Item1.ParentCode))
                        {
                            this.StructureJsonGenerator.WriteStringField("parent", code.Item1.ParentCode);
                        }

                        if (_useHierarchicalCodelists && map!=null)
                        {
                            var parents = map.TryGetValue(currentCode, out var hcl) 
                                ? hcl.Parents 
                                : null;

                            if (parents!=null && parents.Any())
                            {
                                this.StructureJsonGenerator.WriteArrayFieldStart("parents");
                                foreach (var p in parents)
                                {
                                    this.StructureJsonGenerator.WriteString(p);
                                }
                                this.StructureJsonGenerator.WriteEndArray();
                            }
                        }

                        if (ObjectUtil.ValidCollection(code.Item1.Annotations))
                        {
                            this.StructureJsonGenerator.WriteArrayFieldStart("annotations");
                            this.WriteAnnotationReferences(code.Item1.Annotations, this.StructureJsonGenerator);
                            this.StructureJsonGenerator.WriteEndArray();
                        }
                    }
                    else
                    {
                        if (codelist != null)
                        {
                            this.StructureJsonGenerator.WriteStringField("id", currentCode);
                            WriteLocalizedText("name", "names", currentCode, this.StructureJsonGenerator);
                        }
                        else
                        {
                            this.StructureJsonGenerator.WriteStringField("value", currentCode);
                        }

                        if (codelist != null && 
                            !this._missingCodelistCodes.Contains(codelist + currentCode) &&
                            SpecialDimensionValue.Values.All(x => !x.DimensionValue.Equals(currentCode, StringComparison.OrdinalIgnoreCase)))
                        {
                            this._missingCodelistCodes.Add(codelist + currentCode);
                            this._errors.Add(new FooterMessageCore("409", Severity.Error, new TextTypeWrapperImpl("en", $"Could not find code '{currentCode}' in codelist '{codelist}'", null)));
                        }
                    }
                }

                _log.Debug("{/}");
                this.StructureJsonGenerator.WriteEndObject();
            }

            _log.Debug("[/values]");
            this.StructureJsonGenerator.WriteEndArray();

            // Component annotations

            if (component.HasAnnotations() || component.Concept.HasAnnotations())
            {
                this.StructureJsonGenerator.WriteArrayFieldStart("annotations");

                if (component.HasAnnotations())
                    this.WriteAnnotationReferences(component.BuiltFrom.Annotations, this.StructureJsonGenerator);

                if (component.Concept.HasAnnotations())
                    this.WriteAnnotationReferences(component.Concept.BuiltFrom.Annotations, this.StructureJsonGenerator);

                this.StructureJsonGenerator.WriteEndArray();
            }

            _log.Debug("{/}");
            this.StructureJsonGenerator.WriteEndObject();
        }

        
        /// <summary>
        /// Writes annotation array at structure/annotations
        /// </summary>
        private void WriteStructureAnnotations()
        {
            this.StructureJsonGenerator.WriteArrayFieldStart("annotations");

            if (this._annotations != null)
            {
                foreach (var annotation in this._annotations)
                {
                    this.WriteAnnotation(annotation);
                }
            }

            this.StructureJsonGenerator.WriteEndArray();
        }

        /// <summary>
        /// Writes annotation object to structure/annotations, this object is referenced by index from other artifacts
        /// https://github.com/sdmx-twg/sdmx-json/blob/master/data-message/docs/1-sdmx-json-field-guide.md#annotation
        /// </summary>
        /// <param name="annotation">
        /// The annotation.
        /// </param>
        private void WriteAnnotation(IAnnotation annotation)
        {
            _log.Debug("{}");
            this.StructureJsonGenerator.WriteStartObject();

            if (annotation.Id != null)
            {
                this.StructureJsonGenerator.WriteStringField("id", annotation.Id);
            }

            if (annotation.Title != null)
            {
                this.StructureJsonGenerator.WriteStringField("title", annotation.Title);
            }

            if (annotation.Type != null)
            {
                this.StructureJsonGenerator.WriteStringField("type", annotation.Type);
            }

            if (annotation.Uri != null)
            {
                this.StructureJsonGenerator.WriteStringField("uri", annotation.Uri.ToString());
            }

            WriteLocalizedText("text", "texts", annotation.Text, this.StructureJsonGenerator);

            _log.Debug("{/}");
            this.StructureJsonGenerator.WriteEndObject();
        }

        #endregion

        /// <summary>
        /// Stores annotation object at the global storage (structure/annotations), and writes index from this storage as a reference
        /// </summary>
        /// <param name="annotations"></param>
        protected void WriteAnnotationReferences(IList<IAnnotation> annotations, JsonGenerator jsonGenerator)
        {
            foreach (var annotation in annotations)
            {
                if (!this._annotations.Contains(annotation))
                    this._annotations.Add(annotation);

                jsonGenerator.WriteNumber(this._annotations.IndexOf(annotation));
            }
        }

        private IList<IAnnotation> MergeStructureAnnotations(IList<IAnnotation> dataflowAnnotations, IList<IAnnotation> dsdAnnotations)
        {
            if (!ObjectUtil.ValidCollection(dataflowAnnotations))
                return dsdAnnotations;

            if (!ObjectUtil.ValidCollection(dsdAnnotations))
                return dataflowAnnotations;

            var typeHash = new HashSet<string>();
            var result = new List<IAnnotation>();

            foreach (var ann in dataflowAnnotations)
            {
                if (!string.IsNullOrEmpty(ann.Type))
                    typeHash.Add(ann.Type);

                result.Add(ann);
            }

            foreach (var ann in dsdAnnotations)
            {
                if (!string.IsNullOrEmpty(ann.Type) && typeHash.Contains(ann.Type))
                    continue;

                result.Add(ann);
            }

            return result;
        }

        /// <summary>
        /// Write a localized Text
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="arrayPropertyName"></param>
        /// <param name="textTypeWrapper"></param>
        /// <param name="provideAtLeastOneItem"></param>
        /// <param name="jsonGenerator"></param>
        public void WriteLocalizedText(string propertyName, string arrayPropertyName, IList<ITextTypeWrapper> textTypeWrapper, JsonGenerator jsonGenerator, bool provideAtLeastOneItem = false)
        {
            if (textTypeWrapper == null || string.IsNullOrWhiteSpace(propertyName) || string.IsNullOrWhiteSpace(arrayPropertyName) || (textTypeWrapper.Count == 0 && !provideAtLeastOneItem))
            {
                return;
            }

            var translations = Translator.GetSelectedTextInPreferredLanguages(textTypeWrapper, provideAtLeastOneItem);

            if (translations == null || !translations.Any())
            {
                return;
            }

            jsonGenerator.WriteStringField(propertyName, translations.First().Value);


            jsonGenerator.WriteObjectFieldStart(arrayPropertyName);

            if (translations != null)
            {
                foreach (var text in translations)
                {
                    jsonGenerator.WriteStringField(text.Locale, text.Value);
                }
            }

            jsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// Write a localized Text
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="arrayPropertyName"></param>
        /// <param name="texts"></param>
        /// <param name="provideAtLeastOneItem"></param>
        /// <param name="jsonGenerator"></param>
        public void WriteLocalizedText(string propertyName, string arrayPropertyName, IDictionary<CultureInfo, string> texts, JsonGenerator jsonGenerator, bool provideAtLeastOneItem = false)
        {
            if (texts == null || string.IsNullOrWhiteSpace(propertyName) || string.IsNullOrWhiteSpace(arrayPropertyName) || (texts.Count == 0 && !provideAtLeastOneItem))
            {
                return;
            }

            var translations = Translator.GetSelectedTextInPreferredLanguages(texts, provideAtLeastOneItem);

            if (translations == null || !translations.Any())
            {
                return;
            }

            jsonGenerator.WriteStringField(propertyName, translations.First().Value);

            jsonGenerator.WriteObjectFieldStart(arrayPropertyName);

            if (translations != null)
            {
                foreach (var translation in translations)
                {
                    jsonGenerator.WriteStringField(translation.Key, translation.Value);
                }
            }

            jsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// Write a Text with default language
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="arrayPropertyName"></param>
        /// <param name="text"></param>
        /// <param name="jsonGenerator"></param>
        public void WriteLocalizedText(string propertyName, string arrayPropertyName, string text, JsonGenerator jsonGenerator)
        {
            if (string.IsNullOrWhiteSpace(propertyName) || string.IsNullOrWhiteSpace(arrayPropertyName) || string.IsNullOrWhiteSpace(text))
            {
                return;
            }

            jsonGenerator.WriteStringField(propertyName, text);
            jsonGenerator.WriteObjectFieldStart(arrayPropertyName);

            if (!string.IsNullOrEmpty(text))
            {
                jsonGenerator.WriteStringField(Translator.DefaultLanguage.Name, text);
            }

            jsonGenerator.WriteEndObject();
        }

        private struct GroupAttributeKey
        {
            /// <summary>
            /// Group key
            /// </summary>
            public string GroupKey { get; set; }
            /// <summary>
            /// Max index of Attribute in this group
            /// </summary>
            public int MaxIndex { get; set; }
            /// <summary>
            /// Index of Attribute in structure / Index of attribute value 
            /// </summary>
            public List<KeyValuePair<int, int>> Attributes { get; set; }
            public IList<IAnnotation> Annotations { get; set; }
        }

        /// <summary>
        /// Writes the closing tags (only if needed) for the
        /// - observations object in the current series object
        /// - current series object
        /// - series object in the current dataSet object
        /// - current dataSet object
        /// - dataSets array
        /// along with the structure.
        /// </summary>
        /// <param name="footer">
        /// The footer.
        /// </param>
        public override void Close(params IFooterMessage[] footer)
        {
            base.Close(footer);

            try
            {
                this.CloseDataSet(true);

                this.JsonGenerator.WriteEnd(); // Closes dataSets array

                this.WriteStructures();

                // close JSON document
                this.JsonGenerator.WriteEndObject();

                this.WriteErrors();
            }
            catch (IOException e)
            {
                throw new ArgumentException("Illegal argument", e);
            }
            finally
            {
                this.JsonGenerator.Close();
            }
        }


        /// <summary>
        /// Sets Msd related to current DSD
        /// </summary>
        /// <param name="msd"></param>
        protected override void SetCurrentMsd(IMetadataStructureDefinitionObject msd)
        {
            base.SetCurrentMsd(msd);

            this._metadataAttributes = new Dictionary<string, KeyValuePair<IMetadataAttributeObject, IConceptObject>>();

            var conceptSchemes = new Dictionary<string, IConceptSchemeObject>();

            foreach (var metadataAttribute in msd.MetadataAttributes)
            {
                var metaAttributeId = metadataAttribute.GetFullIdPath(false);

                if (!conceptSchemes.TryGetValue(metadataAttribute.ConceptRef.MaintainableUrn.AbsoluteUri, out var conceptScheme))
                {
                    conceptScheme = (IConceptSchemeObject) this.ObjectRetrievalManager.GetMaintainableObject(metadataAttribute.ConceptRef);
                    conceptSchemes.Add(metadataAttribute.ConceptRef.MaintainableUrn.AbsoluteUri, conceptScheme);
                }

                var meta = new KeyValuePair<IMetadataAttributeObject, IConceptObject>(
                    metadataAttribute, 
                    conceptScheme.GetItemById(metadataAttribute.ConceptRef.FullId)
                );

                this.codesForComponentMap.Add(metaAttributeId, new List<string>());
                this._metadataAttributes.Add(metaAttributeId, meta);
            }
        }

        /// <summary>
        /// Writes errors array.
        /// </summary>
        private void WriteErrors()
        {
            _log.Debug("{errors}");

            this.JsonGenerator.WriteArrayFieldStart("errors");

            if (this._errors != null)
            {
                foreach (var errorMessage in this._errors)
                {
                    this.WriteError(errorMessage);
                }
            }

            this.JsonGenerator.WriteEndArray();
        }

        /// <summary>
        /// Write error.
        /// </summary>
        /// <param name="footerMessage">The error.</param>
        private void WriteError(IFooterMessage footerMessage)
        {
            _log.Debug("{error}");
            this.JsonGenerator.WriteStartObject();

            if (footerMessage.Code != null)
            {
                this.JsonGenerator.WriteStringField("code", footerMessage.Code);
            }

            WriteLocalizedText("title", "titles", footerMessage.FooterText, this.JsonGenerator);

            _log.Debug("{/error}");
            this.JsonGenerator.WriteEndObject();
        }

        #region Hierarchical Codelist support through dataflow/dsd annotations

        private void BindHierarchicalCodelists(IDataflowObject dataflow, IDataStructureObject dsd)
        {
            if (_useHierarchicalCodelists)
            {
                var lang = this.Translator.SelectedLanguage;

                this._hclDictionary = HclSupport.RetrieveHclCodes(
                    dataflow, 
                    dsd, 
                    this.ObjectRetrievalManager, 
                    lang?.TwoLetterISOLanguageName
                );
            }
        }

        #endregion
    }
}