using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

namespace Org.Sdmxsource.Sdmx.DataParser.Engine.JsonSupport
{
    /// <summary>
    /// 
    /// </summary>
    public static class HclSupport
    {
        private static string HCL_ANNOTATION_TYPE = "HIER_CONTEXT";
        private static Regex _metaHclRegex = new Regex(@"^([^:]+):([^:]+):([^(]+)\(([\d.]+)\)\.(.+)$", RegexOptions.Compiled);

        /// <summary>
        /// Reference like 'REF_AREA:TEST:HCL_TEST(1.0).CONT_EN'
        /// </summary>
        /// <param name="reference"></param>
        /// <returns></returns>
        public static HclAnnotationReference ParseHclPath(string reference)
        {
            if (string.IsNullOrEmpty(reference))
            {
                return null;
            }

            var match = _metaHclRegex.Match(reference);

            if (!match.Success)
            {
                return null;
            }

            return new HclAnnotationReference()
            {
                CodelistId = match.Groups[1].Value,
                HierarchicalCodelistRef = new StructureReferenceImpl(
                    match.Groups[2].Value, 
                    match.Groups[3].Value, 
                    match.Groups[4].Value, 
                    SdmxStructureEnumType.HierarchicalCodelist
                ),
                HierarchyId = match.Groups[5].Value
            };
        }

        public static Dictionary<string, HclCode> RetrieveCodes(ICodelistSuperObject codelist)
        {
            if (codelist == null)
            {
                return null;
            }

            var dict = new Dictionary<string, HclCode>();

            foreach (var code in codelist.BuiltFrom.Items)
            {
                var hcl = new HclCode() { Code = code.Id };
                
                if (!string.IsNullOrEmpty(code.ParentCode))
                {
                    hcl.Parents.Add(code.ParentCode);
                }
                
                dict[hcl.Code] = hcl;
            }

            return dict;
        }

        public static Dictionary<string, HclCode> RetrieveHclCodes(IHierarchicalCodelistObject hcl, string hierarchyId)
        {
            var hierarchy = hcl.Hierarchies.FirstOrDefault(x => x.Id.Equals(hierarchyId));

            if (hierarchy == null)
            {
                return null;
            }

            var dict = new Dictionary<string, HclCode>();
            
            TraverseHcl(dict, null, hierarchy.HierarchicalCodeObjects);

            return dict;
        }

        private static void TraverseHcl(Dictionary<string, HclCode> dict, IHierarchicalCode parent, IList<IHierarchicalCode> hclCodes)
        {
            if (hclCodes == null || !hclCodes.Any())
            {
                return;
            }

            foreach (var hclCode in hclCodes)
            {
                if (!dict.TryGetValue(hclCode.CodeId, out var hcl))
                {
                    dict[hclCode.CodeId] = hcl = new HclCode() { Code = hclCode.CodeId };
                }

                if (parent != null)
                {
                    hcl.Parents.Add(parent.CodeId);
                }

                TraverseHcl(dict, hclCode, hclCode.CodeRefs);
            }
        }

        public static (int height, string path) MaxHeight(Dictionary<string, HclCode> dict, string code, int lvl = 0)
        {
            var max = lvl;
            var hcl = dict[code];
            string path = null;

            foreach (var parent in hcl.Parents)
            {
                var (height, newPath) = MaxHeight(dict, parent, lvl + 1);

                if (height > max)
                {
                    max = height;
                    path = newPath;
                }
            }

            return (max, path == null ? code : code + "/" + path);
        }

        public static IEnumerable<string> GetAllParents(Dictionary<string, HclCode> dict, string code)
        {
            var map = new HashSet<string>();

            if (dict.TryGetValue(code, out var hcl))
            {
                foreach (var c in hcl.Parents)
                {
                    map.Add(c);
                    map.UnionWith(GetAllParents(dict, c));
                }
            }

            return map;
        }

        public static Dictionary<string, Dictionary<string, HclCode>> RetrieveHclCodes(IDataflowObject dataflow, IDataStructureObject dsd, ISdmxObjectRetrievalManager objectRetrieval, string language)
        {
            var dict = new Dictionary<string, Dictionary<string, HclCode>>();
            
            if (dsd != null)
            {
                RetrieveHclCodes(objectRetrieval, dict, language, dsd.Annotations.Where(x => x.Type.Equals(HCL_ANNOTATION_TYPE, StringComparison.OrdinalIgnoreCase)));
            }

            if (dataflow != null)
            {
                RetrieveHclCodes(objectRetrieval, dict, language, dataflow.Annotations.Where(x => x.Type.Equals(HCL_ANNOTATION_TYPE, StringComparison.OrdinalIgnoreCase)));
            }
            
            return dict;
        }

        public static void RetrieveHclCodes(
            ISdmxObjectRetrievalManager objectRetrieval, 
            Dictionary<string, Dictionary<string, HclCode>>  dict, 
            string language, 
            IEnumerable<IAnnotation> annotations)
        {
            foreach (var a in annotations)
            {
                var annotationValue = a.Text?.FirstOrDefault(x => x.Locale == language)?.Value ?? a.Title;

                if(string.IsNullOrEmpty(annotationValue))
                {
                    continue;
                }

                foreach (var reference in annotationValue.Split(','))
                {
                    var @ref = ParseHclPath(reference);

                    var hcl = @ref != null
                        ? objectRetrieval.GetMaintainableObject<IHierarchicalCodelistObject>(@ref.HierarchicalCodelistRef)
                        : null;

                    if (hcl != null)
                    {
                        var codeDictionary = RetrieveHclCodes(hcl, @ref.HierarchyId);

                        if (codeDictionary != null)
                        {
                            dict[@ref.CodelistId] = codeDictionary;
                        }
                    }
                }
            }
        }

        public class HclCode
        {
            public string Code { get; set; }
            public HashSet<string> Parents { get; set; } = new HashSet<string>();
        }

        public class HclAnnotationReference
        {
            public string CodelistId { get; set; }
            public IStructureReference HierarchicalCodelistRef { get; set; }
            public string HierarchyId { get; set; }
        }
    }
}
