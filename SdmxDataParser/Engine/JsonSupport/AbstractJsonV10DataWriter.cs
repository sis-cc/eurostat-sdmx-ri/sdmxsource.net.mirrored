// -----------------------------------------------------------------------
// <copyright file="AbstractJsonDataWriter.cs" company="EUROSTAT">
//   Date Created : 2016-08-17
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Engine.JsonSupport
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using System.Text;

    using log4net;

    using Org.Sdmxsource.Json;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Date;
    using Org.Sdmxsource.Translator;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// Abstract JsonDataWriter class.
    /// </summary>
    public abstract class AbstractJsonV10DataWriter : DatasetInfoDataWriterEngine
    {
        /// <summary>
        /// The _log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly Dictionary<string, Dictionary<string, Tuple<ICode, int>>> _codelistItemOrderMap;
        private readonly IList<IAnnotation> _annotations;
        private readonly IList<IFooterMessage> _errors;
        private readonly ISet<string> _missingCodelistCodes;

        /// <summary>
        /// Dictionary used for storing group attribute values provided in non-series keyables
        /// </summary>
        private readonly Dictionary<string, Dictionary<string, Tuple<string, IList<IKeyValue>>>> _groupAttributeValues;

        /// <summary>
        /// Flag indicating whether the dataset header was already written to JSON file
        /// </summary>
        protected bool jsonDatasetHeaderWritten;

        /// <summary>
        /// Flag indicating whether a dataset object was started in JSON result (and should be closed)
        /// </summary>
        protected bool jsonDatasetStarted;

        /// <summary>
        /// Flag indicating whether the JSON representation of the measure type (ie. text type) is string
        /// </summary>
        protected bool isPrimaryMeasureStringType;

        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractJsonDataWriter"/> class.
        /// </summary>
        /// <param name="jsonGenerator">
        /// The json generator.
        /// </param>
        /// <param name="superObjectRetrievalManager">
        /// The superObjectRetrievalManager.
        /// </param>
        /// <param name="translator">
        /// The translator.
        /// </param>
        protected AbstractJsonV10DataWriter(JsonGenerator jsonGenerator, ISdmxSuperObjectRetrievalManager superObjectRetrievalManager, ITranslator translator) : base(superObjectRetrievalManager)
        {
            this._codelistItemOrderMap = new Dictionary<string, Dictionary<string, Tuple<ICode, int>>>();
            this._annotations = new List<IAnnotation>();
            this._errors = new List<IFooterMessage>();
            this._missingCodelistCodes = new HashSet<string>();
            this.JsonGenerator = jsonGenerator;
            this.Translator = translator;
            
            _groupAttributeValues = new Dictionary<string, Dictionary<string, Tuple<string, IList<IKeyValue>>>>();

            jsonDatasetHeaderWritten = false;
            jsonDatasetStarted = false;
        }

        /// <summary>
        /// Gets the translator.
        /// </summary>
        public ITranslator Translator { get; private set; }

        /// <summary>
        /// Gets the current key.
        /// </summary>
        protected string CurrentKey { get; private set; }

        /// <summary>
        /// Gets the json generator.
        /// </summary>
        protected JsonGenerator JsonGenerator { get; private set; }

        /// <summary>
        /// Gets or sets the previous key.
        /// </summary>
        protected string PrevKey { get; set; }

        /// <summary>
        /// Starts a dataset with the data conforming to the DSD.
        /// </summary>
        /// <param name="dataflow">
        /// The dataflow.
        /// </param>
        /// <param name="dsd">
        /// The data structure definition.
        /// </param>
        /// <param name="header">
        /// The header.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public override void StartDataset(IDataflowObject dataflow, IDataStructureObject dsd, IDatasetHeader header, params IAnnotation[] annotations)
        {
            if (!this.jsonDatasetHeaderWritten)
            {
                this.JsonGenerator.WriteObjectFieldStart("data");

                if (!jsonDatasetHeaderWritten)
                {
                    _log.Debug("[dataSets]");
                    this.JsonGenerator.WriteArrayFieldStart("dataSets");
                }

                this.jsonDatasetHeaderWritten = true;
            }

            base.StartDataset(dataflow, dsd, header, null);

            // TextFormat is null when representation is enumeration (codelist)
            var measureTextType = CurrentDsdSuperObject.PrimaryMeasure.TextFormat?.TextType.EnumType;

            var nonStringTypes = new[]
            {
                TextEnumType.BigInteger,
                TextEnumType.Count,
                TextEnumType.Decimal,
                TextEnumType.Double,
                TextEnumType.ExclusiveValueRange,
                TextEnumType.Float,
                TextEnumType.InclusiveValueRange,
                TextEnumType.Incremental,
                TextEnumType.Integer,
                TextEnumType.Long,
                TextEnumType.Short
            };

            isPrimaryMeasureStringType = measureTextType == null || nonStringTypes.All(t => t != measureTextType);

            this.WriteJsonDataSet(header, MergeStructureAnnotations(dataflow?.Annotations, dsd.Annotations));
        }

        /// <summary>
        /// Closes started series/observations array object.
        /// </summary>
        protected abstract void CloseSeries();

        /// <summary>
        /// Closes last observation written and observations object when series format 
        /// </summary>
        protected abstract void CloseObservations();

        /// <summary>
        /// Writes the attributes.
        /// </summary>
        protected abstract void WriteAttributes();

        /// <summary>
        /// Writes a component.
        /// </summary>
        /// <param name="component">
        /// The component.
        /// </param>
        /// <param name="position">
        /// The position.
        /// </param>
        protected void WriteComponent(IComponentSuperObject component, int position)
        {
            if (component == null)
            {
                throw new ArgumentNullException("component");
            }

            _log.Debug("{}");
            this.JsonGenerator.WriteStartObject();
            this.JsonGenerator.WriteStringField("id", component.Id);

            WriteLocalizedText("name", "names", component.Concept.Names, true);

            if (position >= 0)
            {
                this.JsonGenerator.WriteNumberField("keyPosition", position);
            }

            var isTime = component.Id.Equals(DimensionObject.TimeDimensionFixedId);

            this.JsonGenerator.WriteArrayFieldStart("roles");
            this.JsonGenerator.WriteString(component.Concept.Id);
            this.JsonGenerator.WriteEndArray();

            var attribute = component as IAttributeSuperObject;

            if (attribute != null)
            {
                this.JsonGenerator.WriteObjectFieldStart("relationship");
                

                if (attribute.AttachmentLevel == AttributeAttachmentLevel.DataSet)
                {
                    this.JsonGenerator.WriteObjectFieldStart("none");
                    this.JsonGenerator.WriteEndObject();
                }
                else if (attribute.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
                {
                    this.JsonGenerator.WriteArrayFieldStart("dimensions");

                    foreach (var dimension in attribute.DimensionReferences)
                    {
                        this.JsonGenerator.WriteString(dimension);
                    }

                    this.JsonGenerator.WriteEndArray();
                }
                else if (attribute.AttachmentLevel == AttributeAttachmentLevel.Group)
                {                    
                    var dsd = attribute.BuiltFrom.IdentifiableParent.IdentifiableParent as IDataStructureObject;

                    if (dsd == null)
                    {
                        throw new ArgumentException($"Missing DSD of attribute {attribute.BuiltFrom.GetFullIdPath(true)}");
                    }
                    var group = dsd.Groups.FirstOrDefault(g => g.Id.Equals(attribute.AttachmentGroup, StringComparison.InvariantCultureIgnoreCase));

                    if (group == null)
                    {
                        throw new ArgumentException($"Missing Group ({attribute.AttachmentGroup}) of attribute {attribute.BuiltFrom.GetFullIdPath(true)}");
                    }

                    this.JsonGenerator.WriteArrayFieldStart("dimensions");

                    foreach (var dimension in group.DimensionRefs)
                    {
                        this.JsonGenerator.WriteString(dimension);
                    }

                    this.JsonGenerator.WriteEndArray();
                }
                else if (attribute.AttachmentLevel == AttributeAttachmentLevel.Observation)
                {
                    this.JsonGenerator.WriteStringField("primaryMeasure", attribute.PrimaryMeasureReference);
                }

                this.JsonGenerator.WriteEndObject();
            }

            _log.Debug("[values]");
            this.JsonGenerator.WriteArrayFieldStart("values");
            var allCodes = this.GetReportedValues(component.Id);

            foreach (var currentCode in allCodes)
            {
                _log.Debug("{}");
                this.JsonGenerator.WriteStartObject();
                var id = currentCode;

                if (isTime)
                {
                    var start = DateUtil.FormatDate(DateUtil.FormatDate(currentCode, true), TimeFormatEnumType.DateTime);
                    var end = DateUtil.FormatDate(DateUtil.FormatDate(currentCode, false), TimeFormatEnumType.DateTime);
                    this.JsonGenerator.WriteStringField("start", start);
                    this.JsonGenerator.WriteStringField("end", end);
                    this.JsonGenerator.WriteStringField("id", id);
                    this.WriteLocalizedText("name", "names", currentCode);
                }
                else
                {
                    var codelist = component.GetCodelist(true);
                    var code = FindCodelistItemByValue(codelist, currentCode);

                    if (code != null)
                    {
                        this.JsonGenerator.WriteStringField("id", code.Item1.Id);
                        this.JsonGenerator.WriteNumberField("order", code.Item2);
                        WriteLocalizedText("name", "names", code.Item1.Names, true);

                        if (!string.IsNullOrEmpty(code.Item1.ParentCode))
                        {
                            this.JsonGenerator.WriteStringField("parent", code.Item1.ParentCode);
                        }

                        if (ObjectUtil.ValidCollection(code.Item1.Annotations))
                        {
                            this.JsonGenerator.WriteArrayFieldStart("annotations");
                            this.WriteAnnotationReferences(code.Item1.Annotations);
                            this.JsonGenerator.WriteEndArray();
                        }
                    }
                    else
                    {
                        this.JsonGenerator.WriteStringField("id", currentCode);
                        WriteLocalizedText("name", "names", currentCode);

                        if (codelist != null &&
                            !this._missingCodelistCodes.Contains(codelist + currentCode) &&
                            SpecialDimensionValue.Values.All(x => !x.DimensionValue.Equals(currentCode, StringComparison.OrdinalIgnoreCase)))
                        {
                            this._missingCodelistCodes.Add(codelist + currentCode);
                            this._errors.Add(new FooterMessageCore("409", Severity.Error, new TextTypeWrapperImpl("en", $"Could not find code '{currentCode}' in codelist '{codelist}'", null)));
                        }
                    }
                }

                _log.Debug("{/}");
                this.JsonGenerator.WriteEndObject();
            }

            _log.Debug("[/values]");
            this.JsonGenerator.WriteEndArray();

            // Component annotations

            if (component.HasAnnotations() || component.Concept.HasAnnotations())
            {
                this.JsonGenerator.WriteArrayFieldStart("annotations");

                if(component.HasAnnotations())
                    this.WriteAnnotationReferences(component.BuiltFrom.Annotations);

                if (component.Concept.HasAnnotations())
                    this.WriteAnnotationReferences(component.Concept.BuiltFrom.Annotations);

                this.JsonGenerator.WriteEndArray();
            }

            _log.Debug("{/}");
            this.JsonGenerator.WriteEndObject();
        }

        private Tuple<ICode, int> FindCodelistItemByValue(ICodelistSuperObject codelist, string code)
        {
            if (codelist == null || string.IsNullOrEmpty(code))
                return null;

            if(!_codelistItemOrderMap.ContainsKey(codelist.Id))
            {
                var dict = codelist.BuiltFrom.Items
                    .Select((c, i) => new Tuple<ICode, int>(c, i))
                    .ToDictionary(x => x.Item1.Id, x => x);

                _codelistItemOrderMap.Add(codelist.Id, dict);
            }

            return _codelistItemOrderMap[codelist.Id].TryGetValue(code, out var result)
                ? result
                : null;
        }

        /// <summary>
        /// Writes dataset attributes.
        /// </summary>
        /// <param name="datasetAttributes">
        /// The dataset attributes.
        /// </param>
        protected override void WriteDatasetAttributes(IList<IKeyValue> datasetAttributes)
        {
            base.WriteDatasetAttributes(datasetAttributes);

            _log.Debug("[attributes]");
            this.JsonGenerator.WriteArrayFieldStart("attributes");

            foreach (var attr in this.CurrentDsdSuperObject.DatasetAttributes)
            {
                var currentKeyValue = datasetAttributes.FirstOrDefault(a => attr.Id == a.Concept);

                if (currentKeyValue == null || string.IsNullOrWhiteSpace(currentKeyValue.Code))
                {
                    this.JsonGenerator.WriteNull();
                }
                else
                {
                    var reportedIndex = this.GetReportedIndex(currentKeyValue.Concept, currentKeyValue.Code);
                    this.JsonGenerator.WriteNumber(reportedIndex);
                }
            }

            _log.Debug("[/attributes]");
            this.JsonGenerator.WriteEndArray();
        }

        /// <summary>
        /// Writes the dimensions.
        /// </summary>
        protected abstract void WriteDimensionsDataset();

        /// <summary>
        /// Returns string that will be used as raw value in output Json
        /// </summary>
        /// <param name="observationValue"></param>
        /// <returns></returns>
        protected string GetObservationValueRawJsonString(string observationValue)
        {
            if (observationValue == null)
            {
                return "null";
            }

            if (observationValue.Length == 0)
            {
                // Return null instead of "" when measure type is not a string
                return isPrimaryMeasureStringType ? "\"\"" : "null";
            }

            return isPrimaryMeasureStringType || observationValue.Equals(SdmxConstants.MissingDataValue, StringComparison.OrdinalIgnoreCase) ? $"\"{observationValue}\"" : observationValue;
        }

        /// <summary>
        /// Writes the observations.
        /// </summary>
        protected abstract void WriteDimensionsObservation();

        /// <summary>
        /// Writes the series.
        /// </summary>
        protected abstract void WriteDimensionsSeries();

        /// <summary>
        /// Writes the key.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        protected override void WriteKey(IKeyable key)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }

            if (!key.Series)
            {
                foreach (var groupAttr in key.Attributes)
                {
                    StoreGroupAttribute(groupAttr, key.Key);
                }

                return;
            }

            base.WriteKey(key);

            var sb = new StringBuilder();
            var delimiter = string.Empty;

            foreach (var kv in key.Key.OrderBy(x=>this._dimensionOrder[x.Concept]))
            {
                var currentIndex = this.GetReportedIndex(kv.Concept, kv.Code);
                sb.Append(delimiter + currentIndex);
                delimiter = ":";
            }

            if (this.CurrentKey != null)
            {
                this.PrevKey = this.CurrentKey;
            }

            this.CurrentKey = sb.ToString();
        }

        /// <summary>
        /// Stores a (group-attached) attribute value with its coordinate
        /// </summary>
        /// <param name="attribute">The IKeyValue instance representing the component id and the value of the attribute</param>
        /// <param name="key">Key of the attribute</param>
        protected void StoreGroupAttribute(IKeyValue attribute, IList<IKeyValue> key)
        {
            if (!_groupAttributeValues.TryGetValue(attribute.Concept, out var attributeValues))
            {
                attributeValues = new Dictionary<string, Tuple<string, IList<IKeyValue>>>();
                
                _groupAttributeValues.Add(attribute.Concept, attributeValues);
            }

            var attributeKeyStr = string.Join(":", key.Select(k => k.Code));

            attributeValues[attributeKeyStr] = new Tuple<string, IList<IKeyValue>>(attribute.Code, key);
        }

        /// <summary>
        /// Retrieves a (group-attached) attribute value belonging to a series key
        /// </summary>
        /// <param name="attributeConcept">The component id of the attribute</param>
        /// <param name="seriesKey">The series key</param>
        /// <param name="obsTime">The obs time</param>
        /// <returns></returns>
        protected string GetGroupAttributeValue(string attributeConcept, IList<IKeyValue> seriesKey, string obsTime, bool isFlat)
        {
            if (!_groupAttributeValues.TryGetValue(attributeConcept, out var attributeValuesDict))
            {
                return null;
            }

            foreach (var (attributeValue, attributeKey) in attributeValuesDict.Values)
            {
                var seriesKeyCount = 0;
                foreach (var sk in seriesKey)
                {
                    foreach (var ak in attributeKey)
                    {
                        if (ak.Concept.Equals(sk.Concept))
                        {
                            if (ak.Concept.Equals(DimensionObject.TimeDimensionFixedId))
                            {
                                if (ak.Code.Equals(obsTime))
                                {
                                    seriesKeyCount++;
                                    break;
                                }
                            }
                            else
                            {
                                if (ak.Code.Equals(sk.Code))
                                {
                                    seriesKeyCount++;
                                    break;
                                }
                            }
                        }
                    }
                }

                if (attributeKey.Count == seriesKeyCount)
                {
                    return attributeValue;
                }

                if (isFlat)
                {
                    var timePeriodKeyValue = attributeKey.FirstOrDefault(x => x.Concept.Equals(DimensionObject.TimeDimensionFixedId));

                    if (timePeriodKeyValue == null)
                    {
                        continue;
                    }

                    if (attributeKey.Count - 1 == seriesKeyCount && timePeriodKeyValue.Code.Equals(obsTime, StringComparison.OrdinalIgnoreCase))
                    {
                        return attributeValue;
                    }
                }

            }

            return null;
        }

        /// <summary>
        /// Writes the structure.
        /// </summary>
        protected void WriteStructure()
        {
            _log.Debug("{structure}");
            this.JsonGenerator.WriteObjectFieldStart("structure");

            var maint = this.Dataflow ?? (IMaintainableObject)this.CurrentDsdSuperObject.BuiltFrom;

            WriteLocalizedText("name", "names", maint.Names, true);
            WriteLocalizedText("description", "descriptions", maint.Descriptions);

            this.WriteStructureDimensions();
            this.WriteAttributes();
            this.WriteAnnotations();

            _log.Debug("{/structure}");
            this.JsonGenerator.WriteEndObject();

            this.JsonGenerator.WriteEndObject(); //NB: Assuming that WriteStructure is the last method call in Close method
        }

        /// <summary>
        /// Write a localized Text
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="arrayPropertyName"></param>
        /// <param name="textTypeWrapper"></param>
        /// <param name="provideAtLeastOneItem"></param>
        public void WriteLocalizedText(string propertyName, string arrayPropertyName, IList<ITextTypeWrapper> textTypeWrapper, bool provideAtLeastOneItem = false)
        {
            if (textTypeWrapper == null || string.IsNullOrWhiteSpace(propertyName) || string.IsNullOrWhiteSpace(arrayPropertyName) || (textTypeWrapper.Count == 0 && !provideAtLeastOneItem))
            {
                return;
            }

            var translations = Translator.GetSelectedTextInPreferredLanguages(textTypeWrapper, provideAtLeastOneItem);

            if (translations == null || !translations.Any())
            {
                return;
            }

            JsonGenerator.WriteStringField(propertyName, translations.First().Value);

            JsonGenerator.WriteObjectFieldStart(arrayPropertyName);

            if (translations != null)
            {
                foreach (var text in translations)
                {
                    JsonGenerator.WriteStringField(text.Locale, text.Value);
                }
            }

            JsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// Write a localized Text
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="arrayPropertyName"></param>
        /// <param name="texts"></param>
        /// <param name="provideAtLeastOneItem"></param>
        public void WriteLocalizedText(string propertyName, string arrayPropertyName, IDictionary<CultureInfo, string> texts, bool provideAtLeastOneItem = false)
        {
            if (texts == null || string.IsNullOrWhiteSpace(propertyName) || string.IsNullOrWhiteSpace(arrayPropertyName) || (texts.Count == 0 && !provideAtLeastOneItem))
            {
                return;
            }

            var translations = Translator.GetSelectedTextInPreferredLanguages(texts, provideAtLeastOneItem);

            if (translations == null || !translations.Any())
            {
                return;
            }

            JsonGenerator.WriteStringField(propertyName, translations.First().Value);

            JsonGenerator.WriteObjectFieldStart(arrayPropertyName);

            if (translations != null)
            {
                foreach (var translation in translations)
                {
                    JsonGenerator.WriteStringField(translation.Key, translation.Value);
                }
            }

            JsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// Write a Text with default language
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="arrayPropertyName"></param>
        /// <param name="text"></param>
        public void WriteLocalizedText(string propertyName, string arrayPropertyName, string text)
        {
            if (string.IsNullOrWhiteSpace(propertyName) || string.IsNullOrWhiteSpace(arrayPropertyName) || string.IsNullOrWhiteSpace(text))
            {
                return;
            }

            JsonGenerator.WriteStringField(propertyName, text);

            JsonGenerator.WriteObjectFieldStart(arrayPropertyName);

            if (!string.IsNullOrEmpty(text))
            {
                JsonGenerator.WriteStringField(Translator.DefaultLanguage.Name, text);
            }

            JsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// Writes the structure.
        /// </summary>
        protected void WriteStructureDimensions()
        {
            _log.Debug("{dimensions}");
            this.JsonGenerator.WriteObjectFieldStart("dimensions");

            this.WriteDimensionsDataset();
            this.WriteDimensionsSeries();
            this.WriteDimensionsObservation();

            _log.Debug("{/dimensions}");
            this.JsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// Stores annotation object at the global storage (structure/annotations), and writes index from this storage as a reference
        /// </summary>
        /// <param name="annotations"></param>
        protected void WriteAnnotationReferences(IList<IAnnotation> annotations)
        {
            foreach (var annotation in annotations)
            {
                if (!this._annotations.Contains(annotation))
                    this._annotations.Add(annotation);

                var idx = this._annotations.IndexOf(annotation);
                this.JsonGenerator.WriteNumber(idx);
            }
        }

        /// <summary>
        /// Writes annotation object to structure/annotations, this object is referenced by index from other artifacts
        /// https://github.com/sdmx-twg/sdmx-json/blob/master/data-message/docs/1-sdmx-json-field-guide.md#annotation
        /// </summary>
        /// <param name="annotation">
        /// The annotation.
        /// </param>
        private void WriteAnnotation(IAnnotation annotation)
        {
            _log.Debug("{}");
            this.JsonGenerator.WriteStartObject();

            if (annotation.Id != null)
            {
                this.JsonGenerator.WriteStringField("id", annotation.Id);
            }

            if (annotation.Title != null)
            {
                this.JsonGenerator.WriteStringField("title", annotation.Title);
            }

            if (annotation.Type != null)
            {
                this.JsonGenerator.WriteStringField("type", annotation.Type);
            }

            if (annotation.Uri != null)
            {
                this.JsonGenerator.WriteStringField("uri", annotation.Uri.ToString());
            }

            WriteLocalizedText("text", "texts", annotation.Text);

            _log.Debug("{/}");
            this.JsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// Writes annotation array at structure/annotations
        /// </summary>
        private void WriteAnnotations()
        {
            this.JsonGenerator.WriteArrayFieldStart("annotations");

            if (this._annotations != null)
            {
                foreach (var annotation in this._annotations)
                {
                    this.WriteAnnotation(annotation);
                }
            }

            this.JsonGenerator.WriteEndArray();
        }

        protected override void CloseDataSet(bool force)
        {
            if (!this.jsonDatasetStarted)
            {
                return;
            }

            this.CloseSeries();

            // Close previous Dataset JSON object
            this.JsonGenerator.WriteEnd();

            this.PrevKey = null;
            this.CurrentKey = null;

            this.jsonDatasetStarted = false;
        }

        private void WriteJsonDataSet(IDatasetHeader datasetHeader, IList<IAnnotation> annotations)
        {
            _log.Debug("{}");
            this.JsonGenerator.WriteStartObject();

            this.jsonDatasetStarted = true;

            this.JsonGenerator.WriteStringField("action", (datasetHeader?.Action ?? DatasetActionEnumType.Information).Action);

            if (datasetHeader != null)
            {
                if (datasetHeader.ReportingBeginDate != default(DateTime))
                {
                    this.JsonGenerator.WriteStringField("reportingBegin", DateUtil.FormatDate(datasetHeader.ReportingBeginDate));
                }

                if (datasetHeader.ReportingEndDate != default(DateTime))
                {
                    this.JsonGenerator.WriteStringField("reportingEnd", DateUtil.FormatDate(datasetHeader.ReportingEndDate));
                }

                if (datasetHeader.ValidFrom != default(DateTime))
                {
                    this.JsonGenerator.WriteStringField("validFrom", DateUtil.FormatDate(datasetHeader.ValidFrom));
                }

                if (datasetHeader.ValidTo != default(DateTime))
                {
                    this.JsonGenerator.WriteStringField("validTo", DateUtil.FormatDate(datasetHeader.ValidTo));
                }

                if (datasetHeader.PublicationYear > 0)
                {
                    this.JsonGenerator.WriteNumberField("publicationYear", datasetHeader.PublicationYear);
                }

                if (ObjectUtil.ValidString(datasetHeader.PublicationPeriod))
                {
                    this.JsonGenerator.WriteStringField("publicationPeriod", datasetHeader.PublicationPeriod);
                }

                if (datasetHeader.DataStructureReference != null)
                {
                    this.JsonGenerator.WriteArrayFieldStart("links");

                    this.JsonGenerator.WriteStartObject();

                    this.JsonGenerator.WriteStringField("urn", datasetHeader.DataStructureReference.StructureReference.TargetUrn.ToString());
                    this.JsonGenerator.WriteStringField("rel", datasetHeader.DataStructureReference.StructureReference.TargetReference.UrnClass);

                    this.JsonGenerator.WriteEndObject();

                    this.JsonGenerator.WriteEndArray();
                }
            }

            if (ObjectUtil.ValidCollection(annotations))
            {
                _log.Debug("[annotations]");
                this.JsonGenerator.WriteArrayFieldStart("annotations");
                this.WriteAnnotationReferences(annotations);
                _log.Debug("[/annotations]");
                this.JsonGenerator.WriteEndArray();
            }
        }

        private IList<IAnnotation> MergeStructureAnnotations(IList<IAnnotation> dataflowAnnotations, IList<IAnnotation> dsdAnnotations)
        {
            if (!ObjectUtil.ValidCollection(dataflowAnnotations))
                return dsdAnnotations;

            if (!ObjectUtil.ValidCollection(dsdAnnotations))
                return dataflowAnnotations;

            var typeHash = new HashSet<string>();
            var result = new List<IAnnotation>();

            foreach (var ann in dataflowAnnotations)
            {
                if (!string.IsNullOrEmpty(ann.Type))
                    typeHash.Add(ann.Type);

                result.Add(ann);
            }

            foreach (var ann in dsdAnnotations)
            {
                if(!string.IsNullOrEmpty(ann.Type) && typeHash.Contains(ann.Type))
                    continue;

                result.Add(ann);
            }

            return result;
        }

        /// <summary>
        /// Writes errors array.
        /// </summary>
        protected void WriteErrors()
        {
            _log.Debug("{errors}");

            this.JsonGenerator.WriteArrayFieldStart("errors");

            if (this._errors != null)
            {
                foreach (var errorMessage in this._errors)
                {
                    this.WriteError(errorMessage);
                }
            }

            this.JsonGenerator.WriteEndArray();
        }

        /// <summary>
        /// Write error.
        /// </summary>
        /// <param name="footerMessage">The error.</param>
        private void WriteError(IFooterMessage footerMessage)
        {
            _log.Debug("{error}");
            this.JsonGenerator.WriteStartObject();

            if (footerMessage.Code != null)
            {
                this.JsonGenerator.WriteStringField("code", footerMessage.Code);
            }

            WriteLocalizedText("title", "titles", footerMessage.FooterText);

            _log.Debug("{/error}");
            this.JsonGenerator.WriteEndObject();
        }
    }
}