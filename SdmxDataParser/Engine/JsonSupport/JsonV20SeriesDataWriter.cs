// -----------------------------------------------------------------------
// <copyright file="SeriesDataWriter.cs" company="EUROSTAT">
//   Date Created : 2016-05-31
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using Org.Sdmxsource.Sdmx.Api.Model.Header;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

namespace Org.Sdmxsource.Sdmx.DataParser.Engine.JsonSupport
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using Io.Sdmx.Utils.Json;
    using log4net;

    using Org.Sdmxsource.Json;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.DataParser.Model;
    using Org.Sdmxsource.Sdmx.Util.Date;
    using Org.Sdmxsource.Translator;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    /// SeriesDataWriter class.
    /// </summary>
    public class JsonV20SeriesDataWriter : AbstractJsonV20DataWriter
    {
        /// <summary>
        /// The _log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// The _series array started.
        /// </summary>
        private bool _seriesArrayStarted;

        /// <summary>
        /// Initializes a new instance of the <see cref="SeriesDataWriter"/> class.
        /// </summary>
        /// <param name="jsonGenerator">
        /// The jsonGenerator.
        /// </param>
        /// <param name="superObjectRetrievalManager">
        /// The superObjectRetrievalManager.
        /// </param>
        /// <param name="objectRetrievalManager">
        /// The objectRetrievalManager.
        /// </param>
        /// <param name="translator">
        /// The translator.
        /// </param>
        /// <param name="useHierarchicalCodelists">Use Hierarchical codelists through dataflow/dsd annotation</param>
        public JsonV20SeriesDataWriter(
            JsonGenerator jsonGenerator,
            ISdmxSuperObjectRetrievalManager superObjectRetrievalManager,
            ISdmxObjectRetrievalManager objectRetrievalManager,
            ITranslator translator,
            bool useHierarchicalCodelists
            ) 
            : base(
                jsonGenerator, 
                superObjectRetrievalManager, 
                objectRetrievalManager, 
                translator,
                useHierarchicalCodelists
              )
        {
        }

        /// <summary>
        /// Starts the series.
        /// </summary>
        /// <param name="annotations">The annotations.</param>
        public override void StartSeries(params IAnnotation[] annotations)
        {
            base.StartSeries(annotations);

            if (!this._seriesArrayStarted)
            {
                _log.Debug("{series}");
                this.JsonGenerator.WriteObjectFieldStart("series");

                this._seriesArrayStarted = true;
            }
        }

        /// <summary>
        /// Closes series array object if it has already been started
        /// </summary>
        protected override void CloseSeries()
        {
            if (!this._seriesArrayStarted)
            {
                return;
            }

            this.CloseObservations();

            // If series array has been started at the previous dataset then close it
             this.JsonGenerator.WriteEnd(); // Closes the last series key object written

            this.JsonGenerator.WriteEnd(); // Closes "series" object

            this._seriesArrayStarted = false;
        }



        /// <summary>
        /// Closes last observation written and observations object
        /// </summary>
        protected override void CloseObservations()
        {
            if (this.CurrentPosition != DataPosition.Observation && this.CurrentPosition != DataPosition.ObservationAttribute)
            {
                return;
            }

            this.JsonGenerator.WriteEnd(); // Closes observations object
        }

        /// <summary>
        /// Writes the attributes object.
        /// </summary>
        protected override void WriteStructureAttributes()
        {
            _log.Debug("{attributes}");
            this.StructureJsonGenerator.WriteObjectFieldStart("attributes");

            this.WriteStructureDatasetAttributes();
            this.WriteStructureDimensionGroupAttributes();

            this.StructureJsonGenerator.WriteArrayFieldStart("series");
            foreach (var attribute in this.CurrentDsdSuperObject.Attributes.Where(a => a.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup))
            {
                if (attribute.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup && attribute.DimensionReferences.Contains(this.DimensionAtObservation))
                {
                    continue;
                }

                this.WriteComponent(attribute, -1);
            }
            this.StructureJsonGenerator.WriteEndArray();

            _log.Debug("[observation]");
            this.StructureJsonGenerator.WriteArrayFieldStart("observation");
            foreach (var attr in this.DataStructureDefinition.GetObservationAttributes(this.DimensionAtObservation))
            {
                var superAttr = (IAttributeSuperObject)this.CurrentDsdSuperObject.GetComponentById(attr.Id);

                if (superAttr != null)
                {
                    this.WriteComponent(superAttr, -1);
                }
            }
            _log.Debug("[/observation]");
            this.StructureJsonGenerator.WriteEndArray();


            _log.Debug("{/attributes}");
            this.StructureJsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// Writes the dataset array into root -> structure -> dimensions.
        /// </summary>
        protected override void WriteDimensionsDataset()
        {
            this.StructureJsonGenerator.WriteArrayFieldStart("dataSet");
            this.StructureJsonGenerator.WriteEndArray();
        }

        /// <summary>
        /// Writes the series array into root -> structure -> dimensions.
        /// </summary>
        protected override void WriteDimensionsSeries()
        {
            _log.Debug("[series]");
            this.StructureJsonGenerator.WriteArrayFieldStart("series");

            var position = 0;
            IDimensionSuperObject timeDimension = null;

            foreach (var dimension in this.CurrentDsdSuperObject.Dimensions)
            {
                if (dimension.Id.Equals(this.DimensionAtObservation))
                {
                    continue;
                }

                if (dimension.TimeDimension)
                {
                    // Always write the time dimension at the end.
                    timeDimension = dimension;
                }
                else
                {
                    this.WriteComponent(dimension, position);
                    position++;
                }
            }

            if (timeDimension != null)
            {
                this.WriteComponent(timeDimension, position);
            }

            _log.Debug("[/series]");
            this.StructureJsonGenerator.WriteEndArray();
        }

        /// <summary>
        /// Writes the observation array into root -> structure -> dimensions.
        /// </summary>
        protected override void WriteDimensionsObservation()
        {
            _log.Debug("[observation]");
            this.StructureJsonGenerator.WriteArrayFieldStart("observation");

            var dimension = this.DataStructureDefinition.GetDimension(this.DimensionAtObservation);
            var superObject = this.CurrentDsdSuperObject.GetDimensionById(this.DimensionAtObservation);

            // Note that a Dimensions' position is 1 indexed, but we need it 0-indexed, so subtract 1
            this.WriteComponent(dimension, superObject, dimension.Position - 1);

            _log.Debug("[/observation]");
            this.StructureJsonGenerator.WriteEndArray();
        }

        /// <summary>
        /// Writes the observation.
        /// </summary>
        /// <param name="obs">
        /// The observation.
        /// </param>
        protected override void WriteObs(IObservation obs)
        {
            this.WriteJsonObs(this.CurrentKey, this.PrevKey, obs);
            this.PrevKey = this.CurrentKey;
        }

        /// <summary>
        /// Writes the component.
        /// </summary>
        /// <param name="component">The component.</param>
        /// <param name="superObject">The super object.</param>
        /// <param name="position">The position.</param>
        private void WriteComponent(IComponent component, IComponentSuperObject superObject, int position)
        {
            _log.Debug("{}");
            this.StructureJsonGenerator.WriteStartObject();
            this.StructureJsonGenerator.WriteStringField("id", component.Id);
            base.WriteLocalizedText("name","names", superObject.Concept.Names, this.StructureJsonGenerator, true);

            if (position >= 0)
            {
                this.StructureJsonGenerator.WriteNumberField("keyPosition", position);
            }

            var isTime = component.Id.Equals(DimensionObject.TimeDimensionFixedId);

            this.StructureJsonGenerator.WriteArrayFieldStart("roles");
            this.StructureJsonGenerator.WriteString(component.ConceptRef.FullId); // Todo: to check!
            this.StructureJsonGenerator.WriteEndArray();

            _log.Debug("[values]");
            this.StructureJsonGenerator.WriteArrayFieldStart("values");
            var allCodes = this.GetReportedValues(component.Id);

            foreach (var currentCode in allCodes)
            {
                _log.Debug("{}");
                this.StructureJsonGenerator.WriteStartObject();
                var id = currentCode;

                if (isTime)
                {
                    var start = DateUtil.FormatDate(DateUtil.FormatDate(currentCode, true), TimeFormatEnumType.DateTime);
                    var end = DateUtil.FormatDate(DateUtil.FormatDate(currentCode, false), TimeFormatEnumType.DateTime);
                    this.StructureJsonGenerator.WriteStringField("start", start);
                    this.StructureJsonGenerator.WriteStringField("end", end);
                    this.StructureJsonGenerator.WriteStringField("id", id);
                    this.WriteLocalizedText("name", "names", currentCode, this.StructureJsonGenerator);
                }
                else
                {
                    var codelist = superObject.GetCodelist(true);

                    if (codelist != null)
                    {
                        var code = codelist.GetCodeByValue(currentCode);

                        if (code != null)
                        {
                            id = code.Id;
                            this.StructureJsonGenerator.WriteStringField("id", id);
                            WriteLocalizedText("name", "names", code.Names, this.StructureJsonGenerator, true);
                        }
                        else
                        {
                            WriteLocalizedText("name", "names", currentCode, this.StructureJsonGenerator); //Add for default value the Code associated to default lang
                        }
                    }
                }

                _log.Debug("{/}");
                this.StructureJsonGenerator.WriteEndObject();
            }

            _log.Debug("[/values]");
            this.StructureJsonGenerator.WriteEndArray();
            _log.Debug("{/}");
            this.StructureJsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// Writes the json obs.
        /// </summary>
        /// <param name="currentKey">
        /// The current key.
        /// </param>
        /// <param name="prevKey">
        /// The previous key.
        /// </param>
        /// <param name="obs">
        /// The obs.
        /// </param>
        private void WriteJsonObs(string currentKey, string prevKey, IObservation obs)
        {
            var idx = this.GetReportedIndex(this.DimensionAtObservation, obs.ObsTime);

            if (currentKey.Equals(prevKey))
            {
                this.WriteJsonObs(obs, idx);
            }
            else
            {
                // Close the previous key if there was one open.
                if (prevKey != null)
                {
                    // End current object.
                    _log.Debug("{/}");
                    this.JsonGenerator.WriteEndObject();

                    // End Observations.
                    _log.Debug("/observations]");
                    this.JsonGenerator.WriteEndObject();
                }

                var seriesIndex = currentKey;

                // Start Series.
                _log.Debug("{series}");
                this.JsonGenerator.WriteObjectFieldStart(seriesIndex);

                _log.Debug("[attributes]");
                this.JsonGenerator.WriteArrayFieldStart("attributes");

                var seriesAttributes = this.CurrentDsdSuperObject.Attributes.Where(a => a.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup);
                var timeSeries = this.DimensionAtObservation.Equals(DimensionObject.TimeDimensionFixedId);

                foreach (var attribute in seriesAttributes)
                {
                    if (attribute.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup && attribute.DimensionReferences.Contains(this.DimensionAtObservation))
                    {
                        continue;
                    }

                    string value = null;

                    // If this is Time Series query against the Series Key.
                    if (timeSeries)
                    {
                        var seriesAttribute = obs.SeriesKey.GetAttribute(attribute.Id);
                        value = seriesAttribute?.Code;
                    }
                    else
                    {
                        var obsAttribute = obs.GetAttribute(attribute.Id);

                        if (obsAttribute != null)
                        {
                            value = obs.GetAttribute(attribute.Id).Code;
                        }
                    }

                    if (value == null)
                    {
                        this.JsonGenerator.WriteNull();
                    }
                    else
                    {
                        var reportedIndex = this.GetReportedIndex(attribute.Id, value);
                        this.JsonGenerator.WriteNumber(reportedIndex);
                    }
                }

                _log.Debug("[/attributes]");
                this.JsonGenerator.WriteEndArray();

                _log.Debug("[annotations]");
                this.JsonGenerator.WriteArrayFieldStart("annotations");
                this.WriteAnnotationReferences(obs.SeriesKey.Annotations, this.JsonGenerator);
                _log.Debug("[/annotations]");
                this.JsonGenerator.WriteEndArray();

                _log.Debug("{observations}");
                this.JsonGenerator.WriteObjectFieldStart("observations");

                this.WriteJsonObs(obs, idx);
            }
        }

        /// <summary>
        /// Writes the json obs.
        /// </summary>
        /// <param name="obs">The obs.</param>
        /// <param name="idx">The index.</param>
        private void WriteJsonObs(IObservation obs, int idx)
        {
            var seriesKey = idx.ToString(CultureInfo.InvariantCulture);
            _log.Debug("['variable series key']");
            this.JsonGenerator.WriteArrayFieldStart(seriesKey);

            // Write the observation value as the first array field.
            //this.JsonGenerator.WriteRawValue(this.GetObservationValueRawJsonString(obs.ObsTime));

            

            // Write the attribute values.
            IList<int?> obsAttrs = new List<int?>();

            var seriesAttributes = this.CurrentDsdSuperObject.Attributes.Where(a => a.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup);

            foreach (var attribute in seriesAttributes)
            {
                if (attribute.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup && !attribute.DimensionReferences.Contains(this.DimensionAtObservation))
                {
                    continue;
                }

                var kv = obs.GetAttribute(attribute.Id) ?? obs.SeriesKey.GetAttribute(attribute.Id);
                var attrValue = kv?.Code;

                if (string.IsNullOrEmpty(attrValue))
                {
                    obsAttrs.Add(null);
                }
                else
                {
                    var index = this.GetReportedIndex(attribute.Id, attrValue);
                    obsAttrs.Add(index);
                }
            }

            foreach (var attr in this.CurrentDsdSuperObject.Measures)
            {
                var kv = obs.GetAttribute(attr.Id) ?? obs.SeriesKey.GetAttribute(attr.Id);

                //Code is for simple values and we also need to check for multiple values
                var attrValue = kv?.Code;

                if (!string.IsNullOrEmpty(attrValue))
                {
                    this.JsonGenerator.WriteRawValue(attrValue);
                }
                else if (kv?.ComplexNodeValues != null && kv.ComplexNodeValues.Count > 0)
                {
                    //we have array values
                    JsonWriterUtil.WriteStringArray(JsonGenerator, null, kv.ComplexNodeValues.Select(x => x.Code).ToArray());
                }
                else
                {
                    this.JsonGenerator.WriteNull();
                }
            }

            foreach (var attr in this.CurrentDsdSuperObject.ObservationAttributes)
            {
                var kv = obs.GetAttribute(attr.Id) ?? obs.SeriesKey.GetAttribute(attr.Id);
                var attrValue = kv?.Code;

                if (!string.IsNullOrEmpty(attrValue))
                {
                    var index = this.GetReportedIndex(attr.Id, attrValue);
                    obsAttrs.Add(index);
                }
                else if (kv?.ComplexNodeValues != null && kv.ComplexNodeValues.Count > 0)
                {
                    //we have array values
                    var indexes = new List<int>();
                    foreach(var item in kv.ComplexNodeValues)
                    {
                        attrValue = item.Code;
                        var indexComplex = this.GetReportedIndex(attr.Id, attrValue);
                        indexes.Add(indexComplex);
                    }
                    
                    JsonWriterUtil.WriteNumberArray(JsonGenerator, null, indexes.ToArray());
                }
                else
                {
                    obsAttrs.Add(null);
                }
            }

            // Only write out the attributes, if they are not all null and there are no annotations following.
            if (ObjectUtil.ValidCollection(obsAttrs))
            {
                if (obsAttrs.Any(a => a.HasValue) || obs.Annotations.Count > 0)
                {
                    foreach (var attrIdx in obsAttrs)
                    {
                        if (!attrIdx.HasValue)
                        {
                            this.JsonGenerator.WriteNull();
                        }
                        else
                        {
                            this.JsonGenerator.WriteNumber(attrIdx.Value);
                        }
                    }
                }
            }

            this.WriteAnnotationReferences(obs.Annotations, this.JsonGenerator);

            _log.Debug("[/'variable series key']");
            this.JsonGenerator.WriteEndArray();
        }
    }
}