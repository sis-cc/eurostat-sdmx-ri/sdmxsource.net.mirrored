using Org.Sdmxsource.Sdmx.Api.Constants;

namespace Org.Sdmxsource.Sdmx.DataParser.Engine.Csv
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using Api.Constants.InterfaceConstant;
    using Api.Manager.Retrieval;
    using Api.Model.Objects.DataStructure;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.DataParser.Model;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;
    using SdmxObjects.Model.Data;
    using Translator;
    using Util.Objects;

    /// <summary>
    /// This class is responsible for writing SDMX-Csv v2.0 data messages.
    /// https://github.com/sdmx-twg/sdmx-csv/blob/master/data-message/docs/sdmx-csv-field-guide.md
    /// </summary>
    public class CsvV20DataWriterEngine : CsvDataWriterEngineBase
    {
        private IDataflowObject _currentDataflow;
        private DatasetAction _currentAction;
        private string _currentDataflowName;
        private string _lastDimension;

        private readonly ISdmxObjectRetrievalManager _objectRetrievalManager;
        private IMetadataStructureDefinitionObject _currentMsdObject;
        private readonly string _structureColumnId;
        private const string STRUCTURE_ID_COLUMN_ID = "STRUCTURE_ID";
        private const string STRUCTURE_NAME_COLUMN_ID = "STRUCTURE_NAME";
        private const string STRUCTURE_COLUMN_VALUE = "DATAFLOW";
        private const string VALUE_COLUMN_ID = "OBS_VALUE";
        private const string ACTION_COLUMN_ID = "ACTION";
        

        private readonly List<string> _requestedComponentIds;
        private List<string> _metadataAttributes;
        private readonly Dictionary<string, IConceptSchemeObject> _metaConceptSchemes;
        /// <summary>
        /// 
        /// </summary>
        public CsvV20DataWriterEngine(Stream outStream, ISdmxSuperObjectRetrievalManager superObjectRetrievalManager, SdmxCsvOptions options, ITranslator translator, ISdmxObjectRetrievalManager objectRetrievalManager)
        :base(outStream, superObjectRetrievalManager, options, translator)
        {
            this._structureColumnId = $"STRUCTURE{(options.MultipleValueDelimiter != null ? $"[{options.MultipleValueDelimiter}]" : "")}";
            this._requestedComponentIds = options.RequestedComponentIds ?? new List<string>();
            this._objectRetrievalManager = objectRetrievalManager;
            _metadataAttributes = new List<string>();
            this._metaConceptSchemes = new Dictionary<string, IConceptSchemeObject>();
        }

        /// <summary>
        /// Starts the dataset. Writes the header
        /// </summary>
        /// <param name="dataflow">
        /// The dataflow.
        /// </param>
        /// <param name="dataStructure">
        /// The data structure.
        /// </param>
        /// <param name="header">
        /// The header.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public override void StartDataset(IDataflowObject dataflow, IDataStructureObject dataStructure, IDatasetHeader header, params IAnnotation[] annotations)
        {
            this.CheckClosed();

            if (dataflow == null)
                throw new ArgumentNullException("dataflow");

            if (dataStructure == null)
                throw new ArgumentNullException("dataStructure");

            if (this.datasetStarted)
            {
                if(this._currentDataflow.Urn != dataflow.Urn)
                    throw new InvalidOperationException("Dataset has been already started");

                this.FlushRow();
                this.ResetDatasetBuffer();
                this.ResetSeriesKey();
                this.ResetGroupBuffer();
                this.ResetObsBuffer();
                this.currentPosition = DataPosition.Dataset;
                _currentAction = header?.Action ?? DatasetAction.GetFromEnum(DatasetActionEnumType.Information);

                // The same dataflow, different action (updated-after case)
                return;
            }

            _currentAction = header?.Action ?? DatasetAction.GetFromEnum(DatasetActionEnumType.Information);
            _currentDataflow = dataflow;

            if (header != null && header.DataStructureReference != null)
            {
                this.dimensionAtObservation = header.DataStructureReference.DimensionAtObservation;
            }

            if (!ObjectUtil.ValidString(this.dimensionAtObservation))
            {
                this.dimensionAtObservation = DimensionObject.TimeDimensionFixedId;
            }

            this.currentDsdSuperObject = this.superObjectRetrievalManager.GetDataStructureSuperObject(dataStructure.AsReference.MaintainableReference);

            this.SetCurrentMsd(dataStructure);

            this.WriteCsvHeader(dataflow, dataStructure);
            this.datasetStarted = true;
        }


        /// <summary>
        /// Writes the attribute value.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="valueRen">
        /// The value.
        /// </param>
        public override void WriteAttributeValue(string id, string valueRen)
        {
            WriteScalarValue(id, valueRen,true);
        }

        protected override void WriteScalarValue(string id, string valueRen, bool isAttribute)
        {
            this.CheckClosed();
            this.CheckDatasetStarted();

            if (currentPosition == DataPosition.Group)
            {
                var attributeKeyValue = new KeyValuePair<string, string>(id, valueRen);

                //Is MsdAttribute
                if (this._metadataAttributes.Any(c => c.Equals(id, StringComparison.CurrentCultureIgnoreCase)))
                {
                    _currentMsdAttributes.Add(attributeKeyValue);
                }
                else
                {
                    if (!this._groupValues.ContainsKey(_currentGroupKey))
                    {
                        this._groupValues.Add(_currentGroupKey, new List<KeyValuePair<string, string>>() { attributeKeyValue });
                    }
                    else
                    {
                        this._groupValues[_currentGroupKey].Add(attributeKeyValue);
                    }
                }

                return;
            }

            if (currentPosition == DataPosition.SeriesKey)
            {
                currentPosition = DataPosition.SeriesKeyAttribute;
            }

            if (!this.columnMap.TryGetValue(isAttribute ? this.GetAttrId(id) : this.GetMeasureId(id), out var column))
            {
                throw new InvalidOperationException("Invalid Attribute: " + id);
            }

            var arr = this.seriesBuffer;

            if (this.currentPosition == DataPosition.Observation)
            {
                arr = this.currentObsBuffer;
            }
            else if (this.currentPosition == DataPosition.Dataset)
            {
                arr = this.datasetBuffer;
            }

            arr[column.Index] = this.GetValue(valueRen, column);
        }
        public override void WriteComplexAttributeValue(IKeyValue keyValue)
        {
            WriteArrayValues(keyValue, true);
        }

        public void WriteArrayValues(IKeyValue keyValue,bool isAttribute)
        {
            this.CheckClosed();
            this.CheckDatasetStarted();

            //build the string value
            StringBuilder sb = new StringBuilder();
            if (keyValue.ComplexNodeValues != null && keyValue.ComplexNodeValues.Count > 0)
            {
                int countNodes = 0;
                foreach (ComplexNodeValue node in keyValue.ComplexNodeValues.Cast<ComplexNodeValue>())
                {
                    if (node.Code != null)
                    {
                        if (countNodes > 0)
                        {
                            //the value used in java is csvOutputConfig.getSubFieldSeparationChar() that defaults to ;
                            sb.Append(";");
                        }

                        //in java csvOutputConfig.isInternalSdmxCsv() || csvOutputConfig.getEscapeValues() == EscapeCsvValues.ESCAPE_ALL ? "\"" + node.getCode() + "\"" : node.getCode()

                        sb.Append(node.Code);
                        countNodes++;
                    }
                    else if (node.TextValues != null && node.TextValues.Count > 0)
                    {
                        foreach (var text in node.TextValues)
                        {
                            if (countNodes > 0)
                            {
                                sb.Append(";");
                            }

                            //in java
                            //if (csvOutputConfig.isInternalSdmxCsv() || csvOutputConfig.getEscapeValues() == EscapeCsvValues.ESCAPE_ALL)
                            //{
                            //    sb.append("\"").append(text.getLocale() != null ? text.getLocale() + ":" + text.getValue() : text.getValue()).append("\"");
                            //}
                            //else
                            sb.Append(text.Locale != null ? text.Locale + ":" + text.Value : text.Value);
                            countNodes++;
                        }
                    }
                }
            }

            if (!this.columnMap.TryGetValue(isAttribute ? this.GetAttrId(keyValue.Concept) : keyValue.Concept, out var column))
            {
                throw new InvalidOperationException("Invalid Attribute: " + keyValue.Code);
            }

            this.currentObsBuffer[column.Index] = sb.ToString();
        }
        public override void WriteComplexMeasureValue(IKeyValue keyValue)
        {
            WriteArrayValues(keyValue,false);
        }

        /// <summary>
        /// Start a group.
        /// </summary>
        /// <param name="groupId">
        /// The group id.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public override void StartGroup(string groupId, params IAnnotation[] annotations)
        {
            this.CheckClosed();
            this.CheckDatasetStarted();
            this.FlushRow();

            this.flushObsRequired = true;
            this.ResetSeriesKey();
            this.ResetGroupBuffer();
            _currentGroupKey = new Dictionary<string, string>();
            _currentMsdAttributes = new List<KeyValuePair<string, string>>();
            this.currentPosition = DataPosition.Group;
        }

        public override void WriteObservation(string obsConceptValue, IAnnotation[] annotations)
        {
            this.CheckClosed();
            this.CheckDatasetStarted();
            this.FlushRow();

            this.currentPosition = DataPosition.Observation;
            this.ResetObsBuffer();

            this.currentObsBuffer[0] = STRUCTURE_COLUMN_VALUE;
            this.currentObsBuffer[1] = UrnUtil.GetUrnPostfix(_currentDataflow.AgencyId, _currentDataflow.Id, _currentDataflow.Version);

            if (this._csvOptions.CsvLabels.Equals(CsvLabels.GetFromEnum(CsvLabelsEnumType.Name)))
            {
                // STRUCTURE NAME
                this.currentObsBuffer[2] = _currentDataflowName;
                this.currentObsBuffer[3] = _currentAction.ShortFormat;
            }
            else
            {
                this.currentObsBuffer[2] = _currentAction.ShortFormat;
            }

            //Time Dimension
            if (this._requestedComponentIds.Any(c => c.Equals(_lastDimension, StringComparison.CurrentCultureIgnoreCase)))
            {
                Column column;
                if (!this.columnMap.TryGetValue(this.GetDimId(_lastDimension), out column))
                    throw new InvalidOperationException("Invalid Dimension: " + _lastDimension);

                this.currentObsBuffer[column.Index] = NormalizeTimeFormat(column, obsConceptValue);
            }

            GenerateGroupBuffer(obsConceptValue);
            this.flushObsRequired = true;

        }
        /// <summary>
        /// Writes the observation.
        /// </summary>
        /// <param name="observationConceptId">
        /// The observation concept id.
        /// </param>
        /// <param name="obsConceptValue">
        /// The observation concept value.
        /// </param>
        /// <param name="obsValue">
        /// The observation value.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public override void WriteObservation(string observationConceptId, string obsConceptValue, string obsValue, params IAnnotation[] annotations)
        {
            throw new SdmxSyntaxException("SDMX 2.1 method called in SDMX 3.0.0 format");
        }


        protected override void GenerateGroupBuffer(string timeValue)
        {
            if (_currentGroupKey.ContainsKey(DimensionObject.TimeDimensionFixedId) && !string.IsNullOrEmpty(timeValue))
            {
                _currentGroupKey[DimensionObject.TimeDimensionFixedId] = timeValue;
            }

            var attributeValues = new List<KeyValuePair<string, string>>();

            //DSD attributes
            var keyValueAttributes = this._groupValues.Where(x => x.Key.Similar(_currentGroupKey))
                .SelectMany(x => x.Value);
            attributeValues.AddRange(keyValueAttributes);

            //MSD attributes
            attributeValues.AddRange(_currentMsdAttributes);

            this.groupBuffer[0] = STRUCTURE_COLUMN_VALUE;
            this.groupBuffer[1] = UrnUtil.GetUrnPostfix(_currentDataflow.AgencyId, _currentDataflow.Id, _currentDataflow.Version);
            this.groupBuffer[2] = _currentAction.ShortFormat;

            foreach (var groupKeyVal in _currentGroupKey)
            {
                if (!this.columnMap.TryGetValue(this.GetDimId(groupKeyVal.Key), out var column))
                {
                    throw new InvalidOperationException("Invalid Dimension: " + groupKeyVal.Key);
                }

                this.groupBuffer[column.Index] = this.GetValue(groupKeyVal.Value, column);

                if (this._csvOptions.CsvLabels.Equals(CsvLabels.GetFromEnum(CsvLabelsEnumType.Name)))
                {
                    if (!this.columnMap.TryGetValue(this.GetDimId(groupKeyVal.Key, true), out column))
                    {
                        throw new InvalidOperationException("Invalid Attribute: " + groupKeyVal.Key);
                    }

                    this.groupBuffer[column.Index] = this.GetValue(groupKeyVal.Value, column, true);
                }
            }

            foreach (var value in attributeValues)
            {
                if (!this.columnMap.TryGetValue(this.GetAttrId(value.Key), out var column))
                {
                    throw new InvalidOperationException("Invalid Attribute: " + value.Key);
                }

                this.groupBuffer[column.Index] = this.GetValue(value.Value, column);

                if (this._csvOptions.CsvLabels.Equals(CsvLabels.GetFromEnum(CsvLabelsEnumType.Name)))
                {
                    if (!this.columnMap.TryGetValue(this.GetAttrId(value.Key, true), out column))
                    {
                        throw new InvalidOperationException("Invalid Attribute: " + value.Key);
                    }

                    this.groupBuffer[column.Index] = this.GetValue(value.Value, column, true);
                }
            }
        }

        protected override void FlushRow()
        {
            if (!this.flushObsRequired)
                return;

            this.flushObsRequired = false;

            GenerateGroupBuffer(string.Empty);

            for (var i = 0; i < this.currentObsBuffer.Length; i++)
            {
                this.csvWriter.WriteField(this.currentObsBuffer[i] ?? this.seriesBuffer[i] ?? this.datasetBuffer[i] ?? this.groupBuffer[i]);
            }

            this.csvWriter.NextRecord();
        }
        #region Private methods

        /// <summary>
        /// Initialize the CSV header for SDMX-CSV v2.0
        /// </summary>
        /// <param name="dataflow"></param>
        /// <param name="dataStructure"></param>
        /// 
        protected override void WriteCsvHeader(IDataflowObject dataflow, IDataStructureObject dataStructure)
        {
            base.columnMap = new Dictionary<string, Column>();
            var index = 0;

            // STRUCTURE
            base.csvWriter.WriteField(this._structureColumnId);
            index++;

            // STRUCTURE ID
            this.csvWriter.WriteField(STRUCTURE_ID_COLUMN_ID);
            index++;

            if (this._csvOptions.CsvLabels.Equals(CsvLabels.GetFromEnum(CsvLabelsEnumType.Name)))
            {
                // STRUCTURE NAME
                this.csvWriter.WriteField(STRUCTURE_NAME_COLUMN_ID);
                index++;

                this._currentDataflowName = this._translator.GetTranslation(_currentDataflow.Names);
            }

            // ACTION
            this.csvWriter.WriteField(ACTION_COLUMN_ID);
            index++;

            // Requested Dimensions
            if (dataStructure?.DimensionList?.Dimensions != null)
            {
                foreach (var dim in dataStructure.DimensionList.Dimensions)
                {
                    if (this._requestedComponentIds.Any(r => r.Equals(dim.Id, StringComparison.CurrentCultureIgnoreCase)))
                    {
                        this.columnMap[this.GetDimId(dim.Id)] = new Column(dim.Id, index++, dim.TimeDimension, false);
                        this.csvWriter.WriteField(this.GetColumn(dim.Id, dim));

                        _lastDimension = dim.Id;
                        if (this._csvOptions.CsvLabels.Equals(CsvLabels.GetFromEnum(CsvLabelsEnumType.Name)))
                        {
                            this.columnMap[this.GetDimId(dim.Id, true)] = new Column(dim.Id, index++, dim.TimeDimension, false);
                            this.csvWriter.WriteField(this.GetColumn(dim.Id, dim, true));
                        }
                    }
                }
            }

            // Requested Measures
            // For sdmx version < 3.0 primary measure and measures are the same, please check the constructor
            // for MeasureListCore
            if (dataStructure?.MeasureList?.Measures != null)
            {
                foreach (var measure in dataStructure.MeasureList.Measures)
                {
                    if (this._requestedComponentIds.Any(r => r.Equals(measure.Id, StringComparison.CurrentCultureIgnoreCase)))
                    {
                        this.columnMap[this.GetMeasureId(measure.Id)] = new Column(measure.Id, index++, false, measure.Representation?.TextFormat?.TextType?.IsNumericTextType ?? false);
                        this.csvWriter.WriteField(this.GetColumn(measure.Id, measure));

                        if (this._csvOptions.CsvLabels.Equals(CsvLabels.GetFromEnum(CsvLabelsEnumType.Name)))
                        {
                            this.columnMap[this.GetMeasureId(measure.Id, true)] = new Column(measure.Id, index++, false, measure.Representation?.TextFormat?.TextType?.IsNumericTextType ?? false);
                            this.csvWriter.WriteField(this.GetColumn(measure.Id, measure, true));
                        }
                    }
                }
            }

            // Requested Attributes 
            var hasAttributes = false;

            if (dataStructure?.AttributeList?.Attributes != null)
            {
                foreach (var attr in dataStructure.AttributeList.Attributes)
                {
                    if (this._requestedComponentIds.Any(r => r.Equals(attr.Id, StringComparison.CurrentCultureIgnoreCase)))
                    {
                        this.columnMap[this.GetAttrId(attr.Id)] = new Column(attr.Id, index++, false, attr.Representation?.TextFormat?.TextType?.IsNumericTextType ?? false);
                        this.csvWriter.WriteField(this.GetColumn(attr.Id, attr));

                        if (this._csvOptions.CsvLabels.Equals(CsvLabels.GetFromEnum(CsvLabelsEnumType.Name)))
                        {
                            this.columnMap[this.GetAttrId(attr.Id, true)] = new Column(attr.Id, index++, false, attr.Representation?.TextFormat?.TextType?.IsNumericTextType ?? false);
                            this.csvWriter.WriteField(this.GetColumn(attr.Id, attr, true));
                        }

                        hasAttributes = true;
                    }
                }
            }

            // Requested metadata attributes 
            if (_currentMsdObject != null)
            {
                _metadataAttributes = new List<string>();
                foreach (var metadataAttribute in _currentMsdObject.MetadataAttributes)
                {
                    var hierarchicalId = metadataAttribute.GetFullIdPath(false);
                    _metadataAttributes.Add(hierarchicalId);
                    if (this._requestedComponentIds.Any(r => r.Equals(hierarchicalId, StringComparison.CurrentCultureIgnoreCase)))
                    {
                        // In case measure or attribute is written
                        if (this.columnMap.ContainsKey(VALUE_COLUMN_ID) || hasAttributes)
                        {
                            throw new SdmxNotImplementedException();
                        }

                        this.columnMap[this.GetAttrId(hierarchicalId)] = new Column(hierarchicalId, index++, false, metadataAttribute.Representation?.TextFormat?.TextType?.IsNumericTextType ?? false)
                        {
                            IsMeta = true
                        };

                        this.csvWriter.WriteField(this.GetColumn(hierarchicalId, metadataAttribute));

                        if (this._csvOptions.CsvLabels.Equals(CsvLabels.GetFromEnum(CsvLabelsEnumType.Name)))
                        { 
                            this.columnMap[this.GetAttrId(hierarchicalId, true)] = new Column(hierarchicalId, index++, false, metadataAttribute.Representation?.TextFormat?.TextType?.IsNumericTextType ?? false)
                            {
                                IsMeta = true
                            };
                            this.csvWriter.WriteField(this.GetColumn(hierarchicalId, metadataAttribute, true));
                        }
                    }
                }
            }

            this.datasetBuffer = new string[index];
            this.seriesBuffer = new string[index];
            this.currentObsBuffer = new string[index];
            this.groupBuffer = new string[index];
            this.csvWriter.NextRecord();
        }

        private void SetCurrentMsd(IDataStructureObject dsd)
        {
            if (this._objectRetrievalManager == null)
            {
                return;
            }

            if (!dsd.Annotations.Any())
            {
                return;
            }

            var msdAnnotation = dsd.Annotations.FirstOrDefault(x => x.Type.Equals("Metadata", StringComparison.OrdinalIgnoreCase));

            if (msdAnnotation == null || string.IsNullOrEmpty(msdAnnotation.Title))
            {
                return;
            }

            var msd = _objectRetrievalManager.GetMaintainableObject<IMetadataStructureDefinitionObject>(new StructureReferenceImpl(msdAnnotation.Title));

            this._currentMsdObject = msd ?? throw new SdmxNoResultsException("Can not read dataset, the data set references the MSD '" + msdAnnotation.Title + "' which could not be resolved");
        }

        private string GetColumn(string id, IComponent component, bool isName = false)
        {
            if (this._csvOptions.CsvLabels.Equals(CsvLabels.GetFromEnum(CsvLabelsEnumType.Id)) ||
                (this._csvOptions.CsvLabels.Equals(CsvLabels.GetFromEnum(CsvLabelsEnumType.Name)) && !isName))
            {
                return id;
            }

            if (component is IMetadataAttributeObject metaAttribute)
            {
                var conceptScheme = GetMetaAttributeConceptScheme(metaAttribute);
                var concept = conceptScheme.GetItemById(metaAttribute.ConceptRef.FullId);
                return !isName 
                    ? $"{component.Id}: {this._translator.GetTranslation(concept.Names)}" 
                    : this._translator.GetTranslation(concept.Names);
            }

            var superObj = this.currentDsdSuperObject.GetComponentById(component.Id);

            return !isName
                ? id + ": " + this._translator.GetTranslation(superObj.Concept.Names)
                : this._translator.GetTranslation(superObj.Concept.Names);
        }

        private IConceptSchemeObject GetMetaAttributeConceptScheme(IMetadataAttributeObject metadataAttribute)
        {
            var key = metadataAttribute.ConceptRef.MaintainableUrn.AbsoluteUri;

            if (!_metaConceptSchemes.TryGetValue(key, out var conceptScheme))
            {
                conceptScheme = _objectRetrievalManager.GetMaintainableObject<IConceptSchemeObject>(metadataAttribute.ConceptRef);
                _metaConceptSchemes.Add(key, conceptScheme);
            }

            return conceptScheme;
        }
        #endregion
    }
}
