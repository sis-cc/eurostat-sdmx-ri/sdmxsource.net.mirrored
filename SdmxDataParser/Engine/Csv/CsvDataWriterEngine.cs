namespace Org.Sdmxsource.Sdmx.DataParser.Engine.Csv
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Api.Constants.InterfaceConstant;
    using Api.Manager.Retrieval;
    using Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.DataParser.Model;
    using Org.Sdmxsource.Util;
    using SdmxObjects.Model.Data;
    using Translator;
    using Util.Objects;

    /// <summary>
    /// This class is responsible for writing SDMX-Csv data messages.
    /// https://github.com/sdmx-twg/sdmx-csv/blob/develop/data-message/docs/sdmx-csv-field-guide.md
    /// </summary>
    public class CsvDataWriterEngine : CsvDataWriterEngineBase
    {
        private const string DATAFLOW_COLUMN_ID = "DATAFLOW";
        private const string VALUE_COLUMN_ID = "OBS_VALUE";

        /// <summary>
        /// 
        /// </summary>
        public CsvDataWriterEngine(Stream outStream, ISdmxSuperObjectRetrievalManager superObjectRetrievalManager, SdmxCsvOptions options, ITranslator translator)
            : base(outStream, superObjectRetrievalManager, options, translator)
        {
        }

        /// <summary>
        /// Writes the attribute value.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="valueRen">
        /// The value.
        /// </param>
        public override void WriteAttributeValue(string id, string valueRen)
        {
            this.CheckClosed();
            this.CheckDatasetStarted();

            if (currentPosition == DataPosition.Group)
            {
                var attributeKeyValue = new KeyValuePair<string, string>(id, valueRen);
                if (!this._groupValues.ContainsKey(_currentGroupKey))
                {
                    this._groupValues.Add(_currentGroupKey, new List<KeyValuePair<string, string>>(){
                        attributeKeyValue });
                }
                else
                {
                    this._groupValues[_currentGroupKey].Add(attributeKeyValue);
                }
                return;
            }
            if (currentPosition == DataPosition.SeriesKey)
            {
                currentPosition = DataPosition.SeriesKeyAttribute;
            }

            Column column;

            if (!this.columnMap.TryGetValue(this.GetAttrId(id), out column))
                throw new InvalidOperationException("Invalid Attribute: " + id);

            var arr = this.seriesBuffer;

            if (this.currentPosition == DataPosition.Observation)
            {
                arr = this.currentObsBuffer;
            }
            else if (this.currentPosition == DataPosition.Dataset)
            {
                arr = this.datasetBuffer;
            }

            arr[column.Index] = this.GetValue(valueRen, column);
        }

        /// <summary>
        /// Start a group.
        /// </summary>
        /// <param name="groupId">
        /// The group id.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public override void StartGroup(string groupId, params IAnnotation[] annotations)
        {
            this.CheckClosed();
            this.CheckDatasetStarted();
            _currentGroupKey = new Dictionary<string, string>();
            this.currentPosition = DataPosition.Group;
        }

        /// <summary>
        /// Starts the dataset. Writes the header
        /// </summary>
        /// <param name="dataflow">
        /// The dataflow.
        /// </param>
        /// <param name="dataStructure">
        /// The data structure.
        /// </param>
        /// <param name="header">
        /// The header.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public override void StartDataset(IDataflowObject dataflow, IDataStructureObject dataStructure, IDatasetHeader header, params IAnnotation[] annotations)
        {
            this.CheckClosed();

            if (dataflow == null)
                throw new ArgumentNullException("dataflow");

            if (dataStructure == null)
                throw new ArgumentNullException("dataStructure");

            if (this.datasetStarted)
                throw new InvalidOperationException("Dataset has been already started");

            if (header != null && header.DataStructureReference != null)
            {
                this.dimensionAtObservation = header.DataStructureReference.DimensionAtObservation;
            }

            if (!ObjectUtil.ValidString(this.dimensionAtObservation))
            {
                this.dimensionAtObservation = DimensionObject.TimeDimensionFixedId;
            }

            this.currentDsdSuperObject = this.superObjectRetrievalManager.GetDataStructureSuperObject(dataStructure.AsReference.MaintainableReference);

            this.WriteCsvHeader(dataflow, dataStructure);
            this.datasetStarted = true;
        }

        /// <summary>
        /// Writes the observation.
        /// </summary>
        /// <param name="observationConceptId">
        /// The observation concept id.
        /// </param>
        /// <param name="obsConceptValue">
        /// The observation concept value.
        /// </param>
        /// <param name="obsValue">
        /// The observation value.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public override void WriteObservation(string observationConceptId, string obsConceptValue, string obsValue, params IAnnotation[] annotations)
        {
            this.CheckClosed();
            this.CheckDatasetStarted();
            this.FlushRow();

            if (!this.columnMap.TryGetValue(this.GetDimId(observationConceptId), out var column))
            {
                throw new InvalidOperationException("Invalid Dimension: " + observationConceptId);
            }

            this.currentPosition = DataPosition.Observation;
            this.currentObsBuffer = new string[this.currentObsBuffer.Length];
            this.currentObsBuffer[column.Index] = NormalizeTimeFormat(column, obsConceptValue);

            var measureColumn = this.columnMap[VALUE_COLUMN_ID];
            obsValue = GetCultureSpecificValue(obsValue, measureColumn);

            this.currentObsBuffer[measureColumn.Index] = obsValue;
            GenerateGroupBuffer(obsConceptValue);
            this.flushObsRequired = true;
        }

        protected override void GenerateGroupBuffer(string timeValue)
        {
            if (_currentGroupKey.ContainsKey(DimensionObject.TimeDimensionFixedId))
            {
                _currentGroupKey[DimensionObject.TimeDimensionFixedId] = timeValue;
            }

            var relevantKeys = this._groupValues.Keys.Where(x => x.Similar(_currentGroupKey)).ToList();
            var attributeValues = new List<KeyValuePair<string, string>>();
            foreach (var key in relevantKeys)
            {
                attributeValues.AddRange(_groupValues[key]);
            }


            if (attributeValues != null)
            {
                foreach (var value in attributeValues)
                {
                    Column column;
                    if (!this.columnMap.TryGetValue(this.GetAttrId(value.Key), out column))
                        throw new InvalidOperationException("Invalid Attribute: " + value.Key);
                    this.groupBuffer[column.Index] = this.GetValue(value.Value, column);
                }
            }
        }

        protected override void FlushRow()
        {
            if (!this.flushObsRequired)
                return;

            this.flushObsRequired = false;

            for (var i = 0; i < this.currentObsBuffer.Length; i++)
            {
                this.csvWriter.WriteField(this.currentObsBuffer[i] ?? this.seriesBuffer[i] ?? this.datasetBuffer[i] ?? this.groupBuffer[i]);
            }

            this.csvWriter.NextRecord();
        }

        #region Private methods

        /// <summary>
        /// Initialize the CSV header for SDMX-CSV v1.0
        /// </summary>
        /// <param name="dataflow"></param>
        /// <param name="dataStructure"></param>
        protected override void WriteCsvHeader(IDataflowObject dataflow, IDataStructureObject dataStructure)
        {
            this.columnMap = new Dictionary<string, Column>();
            var index = 0;

            // DATAFLOW unless it is old test client format
            if (!_csvOptions.IsOldTestClientFormat)
            {
                this.csvWriter.WriteField(DATAFLOW_COLUMN_ID);
                index++;
            }

            // Dimensions
            foreach (var dim in dataStructure.DimensionList.Dimensions)
            {
                this.columnMap[this.GetDimId(dim.Id)] = new Column(dim.Id, index++, dim.TimeDimension, false);
                this.csvWriter.WriteField(this.GetColumn(dim));
            }

            // VALUE
            this.columnMap[VALUE_COLUMN_ID] = new Column(VALUE_COLUMN_ID, index++, false, dataStructure.PrimaryMeasure.Representation?.TextFormat?.TextType?.IsNumericTextType ?? false);
            this.csvWriter.WriteField(VALUE_COLUMN_ID);

            // Attributes
            foreach (var attr in dataStructure.Attributes)
            {
                this.columnMap[this.GetAttrId(attr.Id)] = new Column(attr.Id, index++, false, attr.Representation?.TextFormat?.TextType?.IsNumericTextType ?? false);
                this.csvWriter.WriteField(this.GetColumn(attr));
            }

            this.datasetBuffer = new string[index];
            this.seriesBuffer = new string[index];
            this.currentObsBuffer = new string[index];
            this.groupBuffer = new string[index];
            this.datasetBuffer[0] = UrnUtil.GetUrnPostfix(dataflow.AgencyId, dataflow.Id, dataflow.Version);

            this.csvWriter.NextRecord();
        }

        private string GetColumn(IComponent component)
        {
            if (this._csvOptions.CsvLabels.Equals(CsvLabels.GetFromEnum(CsvLabelsEnumType.Id)))
            {
                return component.Id;
            }

            var superObj = this.currentDsdSuperObject.GetComponentById(component.Id);

            return component.Id + ": " + this._translator.GetTranslation(superObj.Concept.Names);
        }
        #endregion
    }
}
