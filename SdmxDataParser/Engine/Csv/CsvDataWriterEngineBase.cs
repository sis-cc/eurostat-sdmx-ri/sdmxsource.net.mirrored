namespace Org.Sdmxsource.Sdmx.DataParser.Engine.Csv
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using Api.Constants;
    using Api.Engine;
    using Api.Manager.Retrieval;
    using Api.Model;
    using Api.Model.Header;
    using Api.Model.Objects.Base;
    using Api.Model.Objects.DataStructure;
    using Api.Model.SuperObjects.Codelist;
    using Api.Model.SuperObjects.DataStructure;
    using CsvHelper;
    using CsvHelper.Configuration;
    using Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using SdmxObjects.Model.Data;
    using Translator;
    using Util.Date;

    /// <summary>
    /// This class is responsible for writing SDMX-Csv data messages.
    /// https://github.com/sdmx-twg/sdmx-csv/blob/develop/data-message/docs/sdmx-csv-field-guide.md
    /// https://github.com/sdmx-twg/sdmx-csv/blob/master/data-message/docs/sdmx-csv-field-guide.md
    /// </summary>
    public abstract class CsvDataWriterEngineBase : IDataWriterEngine
    {
        protected readonly ISdmxSuperObjectRetrievalManager superObjectRetrievalManager;
        protected IDataStructureSuperObject currentDsdSuperObject;
        protected SdmxCsvOptions _csvOptions;
        protected ITranslator _translator;
        protected DataPosition currentPosition;
        protected Dictionary<string, Column> columnMap;
        protected string dimensionAtObservation;

        protected bool datasetStarted = false;
        protected bool flushObsRequired = false;
        private bool _isClosed;

        protected string[] datasetBuffer;
        protected string[] seriesBuffer;
        protected string[] currentObsBuffer;
        protected readonly CsvWriter csvWriter;
        protected readonly Dictionary<Dictionary<string, string>, List<KeyValuePair<string, string>>> _groupValues = new Dictionary<Dictionary<string, string>, List<KeyValuePair<string, string>>>();
        protected Dictionary<string, string> _currentGroupKey =
            new Dictionary<string, string>();
        protected string[] groupBuffer;

        protected List<KeyValuePair<string, string>> _currentMsdAttributes =
            new List<KeyValuePair<string, string>>();
        /// <summary>
        /// 
        /// </summary>
        public CsvDataWriterEngineBase(Stream outStream, ISdmxSuperObjectRetrievalManager superObjectRetrievalManager, SdmxCsvOptions options, ITranslator translator)
        {
            this.currentPosition = DataPosition.Dataset;
            this.superObjectRetrievalManager = superObjectRetrievalManager;
            this._csvOptions = options;
            this._translator = translator;

            var conf = new CsvConfiguration(options.UseCultureSpecificColumnAndDecimalSeparators ? translator.SelectedLanguage : translator.DefaultLanguage)
            {
                Encoding = options.Encoding,
            };
            if(options.IsOldTestClientFormat)
            {
                conf.Delimiter = ";";
            }
            this.csvWriter = new CsvWriter(new StreamWriter(outStream, options.Encoding), conf); 
            
        }

        /// <summary>
        /// Writes the closing tags along with the structure.
        /// </summary>
        /// <param name="footer">
        /// The footer.
        /// </param>
        public void Close(params IFooterMessage[] footer)
        {
            this.Dispose();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (this._isClosed)
                return;

            this.FlushRow();

            if (this.csvWriter != null)
                this.csvWriter.Dispose();

            this._isClosed = true;
        }

        /// <summary>
        /// This method has no effect
        /// </summary>
        /// <param name="header">
        /// The header.
        /// </param>
        public void WriteHeader(IHeader header)
        { }

        /// <summary>
        /// Starts the dataset. Writes the header
        /// </summary>
        /// <param name="dataflow">
        /// The dataflow.
        /// </param>
        /// <param name="dataStructure">
        /// The data structure.
        /// </param>
        /// <param name="header">
        /// The header.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public abstract void StartDataset(IDataflowObject dataflow, IDataStructureObject dataStructure, IDatasetHeader header, params IAnnotation[] annotations);
        

        /// <summary>
        /// Starts the series.
        /// </summary>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public void StartSeries(params IAnnotation[] annotations)
        {
            this.CheckClosed();
            this.CheckDatasetStarted();
            this.FlushRow();
            this.ResetSeriesKey();
            this.ResetGroupBuffer();
            _currentGroupKey = new Dictionary<string, string>();
            _currentMsdAttributes = new List<KeyValuePair<string, string>>();

            this.currentPosition = DataPosition.SeriesKey;
        }

        /// <summary>
        /// Writes the series key value.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="valueRen">
        /// The value.
        /// </param>
        public void WriteSeriesKeyValue(string id, string valueRen)
        {
            this.CheckClosed();
            this.CheckDatasetStarted();
            this.FlushRow();

            if(this.currentPosition >= DataPosition.Observation)
                this.ResetSeriesKey();

            this.currentPosition = DataPosition.SeriesKey;

            Column column;

            if (!this.columnMap.TryGetValue(this.GetDimId(id), out column))
                throw new InvalidOperationException("Invalid Dimension: " + id);

            this.seriesBuffer[column.Index] = this.GetValue(valueRen, column);
            _currentGroupKey.Add(id, valueRen);
        }

        /// <summary>
        /// Writes the attribute value.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="valueRen">
        /// The value.
        /// </param>
        public abstract void WriteAttributeValue(string id, string valueRen);

        /// <summary>
        /// Start a group.
        /// </summary>
        /// <param name="groupId">
        /// The group id.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public abstract void StartGroup(string groupId, params IAnnotation[] annotations);

        /// <summary>
        /// Writes the group key value.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="valueRen">
        /// The value.
        /// </param>
        public void WriteGroupKeyValue(string id, string valueRen)
        {
            this.CheckClosed();
            this.CheckDatasetStarted();
            this._currentGroupKey.Add(id, valueRen);
        }
        
        /// <summary>
        /// Writes the observation.
        /// </summary>
        /// <param name="obsConceptValue">
        /// The observation concept value.
        /// </param>
        /// <param name="obsValue">
        /// The observation value.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public void WriteObservation(string obsConceptValue, string obsValue, params IAnnotation[] annotations)
        {
            if (this.dimensionAtObservation.Equals(DimensionAtObservation.GetFromEnum(DimensionAtObservationEnumType.All).Value))
            {
                throw new ArgumentException(
                    "Cannot write observation, as no observation concept id was given, and this is writing a flat dataset. "
                    + "Please use the method: writeObservation(String obsConceptId, String obsIdValue, String obsValue, AnnotationBean... annotations)");
            }

            this.WriteObservation(this.dimensionAtObservation, obsConceptValue, obsValue, annotations);
        }

        /// <summary>
        /// Writes the observation.
        /// </summary>
        /// <param name="observationConceptId">
        /// The observation concept id.
        /// </param>
        /// <param name="obsConceptValue">
        /// The observation concept value.
        /// </param>
        /// <param name="obsValue">
        /// The observation value.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public abstract void WriteObservation(string observationConceptId, string obsConceptValue, string obsValue, params IAnnotation[] annotations);
        
        /// <summary>
        /// Writes the observation.
        /// </summary>
        /// <param name="obsTime">
        /// The observation time.
        /// </param>
        /// <param name="obsValue">
        /// The observation value.
        /// </param>
        /// <param name="sdmxSwTimeFormat">
        /// The SDMX time format.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public void WriteObservation(DateTime obsTime, string obsValue, TimeFormat sdmxSwTimeFormat, params IAnnotation[] annotations)
        {
            this.WriteObservation(DateUtil.FormatDate(obsTime, sdmxSwTimeFormat), obsValue, annotations);
        }

        #region protected methods

        /// <summary>
        /// Initialize the CSV header for SDMX-CSV
        /// </summary>
        /// <param name="dataflow"></param>
        /// <param name="dataStructure"></param>
        protected abstract void WriteCsvHeader(IDataflowObject dataflow, IDataStructureObject dataStructure);

        protected string GetAttrId(string id, bool isName = false)
        {
            return !isName
                ? $"ATTR::{id}"
                : $"ATTR::NAME::{id}";
        }

        protected string GetDimId(string id, bool isName = false)
        {
            return !isName 
                ? $"DIM::{id}" 
                : $"DIM::NAME::{id}";
        }

        protected string GetMeasureId(string id, bool isName = false)
        {
            return !isName
                ? $"MEASURE::{id}"
                : $"MEASURE::NAME::{id}";
        }

        protected abstract void GenerateGroupBuffer(string timeValue);

        /// <summary>
        /// The check closed.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Data Writer has already been closed and cannot have any more information written to it!</exception>
        protected void CheckClosed()
        {
            if (this._isClosed)
            {
                throw new InvalidOperationException("Data Writer has already been closed and cannot have any more information written to it!");
            }
        }

        protected void CheckDatasetStarted()
        {
            if (!this.datasetStarted)
            {
                throw new InvalidOperationException("Dataset should be started first");
            }
        }

        protected abstract void FlushRow();

        protected string NormalizeTimeFormat(Column column, string dateStr)
        {
            return column == null || !column.IsTimeDimension || string.IsNullOrEmpty(dateStr) || !_csvOptions.UseNormalizedTime
                ? dateStr
                : DateUtil.FormatDate(DateUtil.FormatDateWithOffSet(dateStr, false), TimeFormatEnumType.Date);
        }

        #endregion

        #region Private methods

        protected string GetValue(string value, Column column, bool isName = false)
        {
            value = GetCultureSpecificValue(value, column);
            
            if (column.IsMeta)
            {
                return isName ? string.Empty : value;
            }

            if (this._csvOptions.CsvLabels.Equals(CsvLabels.GetFromEnum(CsvLabelsEnumType.Id)) ||
                (this._csvOptions.CsvLabels.Equals(CsvLabels.GetFromEnum(CsvLabelsEnumType.Name)) && !isName))
            {
                return value;
            }

            return column.GetCodeLabel(this.currentDsdSuperObject, this._translator, value, isName);
        }

        protected string GetCultureSpecificValue(string value, Column column)
        {
            if (this._csvOptions.UseCultureSpecificColumnAndDecimalSeparators &&
                column.IsNumericTextType &&
                !this._translator.DefaultLanguage.Equals(this._translator.SelectedLanguage) &&
                decimal.TryParse(value, NumberStyles.Number, this._translator.DefaultLanguage, out var decimalValue))
            {
                return decimalValue.ToString(_translator.SelectedLanguage);
            }

            return value;
        }

        protected void ResetDatasetBuffer()
        {
            this.datasetBuffer = new string[this.datasetBuffer.Length];
        }

        protected void ResetSeriesKey()
        {
            this.seriesBuffer = new string[this.seriesBuffer.Length];
        }

        protected void ResetObsBuffer()
        {
            this.currentObsBuffer = new string[this.currentObsBuffer.Length];
        }

        protected void ResetGroupBuffer()
        {
            this.groupBuffer = new string[this.groupBuffer.Length];
        }

        public virtual void WriteComplexAttributeValue(IKeyValue keyValue)
        {
            throw new NotImplementedException("This can only be used by SDMX 3.0.0");
        }


        public virtual void WriteComplexMeasureValue(IKeyValue keyValue)
        {
            throw new NotImplementedException("This can only be used by SDMX 3.0.0");
        }
       

        public void WriteMeasureValue(string id, string value)
        {
            WriteScalarValue(id, value,false);
        }

        protected virtual void WriteScalarValue(string id, string value, bool v)
        {
            throw new NotImplementedException();
        }

        public virtual void WriteObservation(string obsConceptValue, params IAnnotation[] annotations)
        {
            this.WriteObservation(obsConceptValue, null, annotations);
        }

        #endregion

        protected class Column
        {
            private bool _codelistInited;
            private ICodelistSuperObject _codelist;
            private string _id;
            public int Index { get; }
            public bool IsTimeDimension { get; }
            public bool IsMeta { get; set; }
            public bool IsNumericTextType { get; set; }

            public Column(string id, int index, bool isTimeDimension, bool isNumericTextType)
            {
                this._id = id;
                this.Index = index;
                this.IsTimeDimension = isTimeDimension;
                this.IsNumericTextType = isNumericTextType;
            }

            public string GetCodeLabel(IDataStructureSuperObject currentDsdSuperObject, ITranslator translator, string value, bool isName)
            {
                if (!this._codelistInited)
                {
                    var superObj = currentDsdSuperObject.GetComponentById(this._id);
                    this._codelist = superObj.GetCodelist(true);
                    this._codelistInited = true;
                }

                if (this._codelist == null)
                {
                    return isName ? string.Empty : value;
                }

                var code = this._codelist.GetCodeByValue(value);

                if (code == null)
                {
                    return value;
                }

                return !isName 
                    ? $"{value}: {translator.GetTranslation(code.Names)}"
                    : translator.GetTranslation(code.Names);
            }
        }
    }
}
