using System.Collections.Generic;

namespace Org.Sdmxsource.Sdmx.DataParser.Engine.Csv
{ 
    internal static class GroupValuesDictionaryExtension 
    {
        public static bool Similar(this Dictionary<string, string> x, Dictionary<string, string> y)
        {
            foreach (var valuePair in y)
            {
                if (x.ContainsKey(valuePair.Key))
                {
                    if (x[valuePair.Key] != valuePair.Value)
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
    
}
