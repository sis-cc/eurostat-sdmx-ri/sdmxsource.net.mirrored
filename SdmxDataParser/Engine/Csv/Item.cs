namespace Org.Sdmxsource.Sdmx.DataParser.Engine.Csv
{
    internal class Item
    {
        public Item(int index, bool sanitize)
        {
            Index = index;
            Sanitize = sanitize;
        }

        public int Index { get; set; }
        public bool Sanitize { get; set; }
    }
}
