namespace Org.Sdmxsource.Sdmx.DataParser.Engine.Csv
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.DataParser.Engine.Reader;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    using CsvHelper;
    using CsvHelper.Configuration;

    /// <summary>
    /// This class is responsible for writing SDMX-CSV 2.0 data messages.
    /// </summary>
    public sealed class CsvDataReaderEngineV2 : AbstractDataReaderEngine
    {
        private CsvReader _csvReader;

        private Dictionary<string, int> _csvHeaders;
        private int _valueIndex = -1;
        private int _timeIndex = -1;
        private bool _readNextObservation = false;
        private List<KeyValuePair<string, Item>> _dimensions;
        private List<KeyValuePair<string, Item>> _obsAttributes;
        private List<IKeyValue> _datasetAttributes;
        private List<KeyValuePair<string, Item>> _metadataAttributes;

        private string[] _buffer;
        private string _seriesKey;
        private static readonly Regex urnRegex = new Regex(@"(.+):(.+)\(((\d+\.){1,2}\d+)\)", RegexOptions.Compiled);
        private const string IdLabelSeparator = ": ";
        private bool _containsIdsAndLabels;
        private bool _containsActionColumn;

        /// <summary>
        ///     Initializes a new instance of the <see cref="CsvDataReaderEngine" /> class.
        ///     Initializes a new instance of the <see cref="AbstractSdmxDataReaderEngine" /> class.
        /// </summary>
        /// <param name="dataLocation">
        ///     The data Location.
        /// </param>
        /// <param name="objectRetrieval">
        ///     The SDMX Object Retrieval. giving the ability to retrieve DSDs for the datasets this
        ///     reader engine is reading.  This can be null if there is only one relevant DSD - in which case the
        ///     <paramref name="defaultDsd" /> should be provided.
        /// </param>
        /// <param name="defaultDataflow">
        ///     The default Dataflow. (Optional)
        /// </param>
        /// <param name="defaultDsd">
        ///     The default DSD. The default DSD to use if the <paramref name="objectRetrieval" /> is null, or
        ///     if the bean retrieval does not return the DSD for the given dataset.
        /// </param>
        /// <param name="defaultMsd">
        ///     The default metadata structure definition object (optional)
        /// </param>
        /// <exception cref="System.ArgumentException">
        ///     AbstractDataReaderEngine expects either a ISdmxObjectRetrievalManager or a
        ///     IDataStructureObject to be able to interpret the structures
        /// </exception>
        public CsvDataReaderEngineV2(IReadableDataLocation dataLocation, ISdmxObjectRetrievalManager objectRetrieval, IDataflowObject defaultDataflow, IDataStructureObject defaultDsd, IMetadataStructureDefinitionObject defaultMsd = null) : 
            base(dataLocation, objectRetrieval, defaultDataflow, defaultDsd, defaultMsd)
        {
            Reset();
        }

        /// <summary>
        ///     Gets the attributes available for the current dataset
        /// </summary>
        /// <value> a copy of the list, returns an empty list if there are no dataset attributes </value>
        public override IList<IKeyValue> DatasetAttributes
        {
            get { return _datasetAttributes; }
        }

        /// <summary>
        ///     Creates a copy of this data reader engine, the copy is another iterator over the same source data
        /// </summary>
        /// <returns>
        ///     The <see cref="IDataReaderEngine" /> .
        /// </returns>
        public override IDataReaderEngine CreateCopy()
        {
            return new CsvDataReaderEngineV2(DataLocation, ObjectRetrieval, DefaultDataflow, DefaultDsd, DefaultMsd);
        }

        /// <summary>
        ///     Gets the current action if defined on the row.
        /// </summary>
        /// <value> </value>
        public override DatasetAction CurrentAction => _containsActionColumn ? DatasetAction.GetAction(_buffer[2]) : CurrentDatasetHeader?.Action;

        /// <summary>
        ///     The lazy load key.
        /// </summary>
        /// <returns>
        ///     The <see cref="IKeyable" />.
        /// </returns>
        protected override IKeyable LazyLoadKey()
        {
            var key = BuildKey(_dimensions);

            return CreateKeyable(key, null, TimeFormat.GetFromEnum(TimeFormatEnumType.Null));
        }

        /// <summary>
        ///     The lazy load observation.
        /// </summary>
        /// <returns>
        ///     The <see cref="IObservation" />.
        /// </returns>
        protected override IObservation LazyLoadObservation()
        {
            var attributes = BuildKey(_obsAttributes);

            if (_metadataAttributes != null && _metadataAttributes.Any())
            {
                attributes.AddRange(BuildKey(_metadataAttributes));
            }

            var obsValue = _valueIndex != -1 ? _buffer[_valueIndex] : null;
            var obsTime = _timeIndex != -1 ? _buffer[_timeIndex] : null;

            return new ObservationImpl(
                CurrentKeyValue, 
                obsTime, 
                obsValue, 
                attributes,
                crossSectionValue:null
            );
        }

        /// <summary>
        ///     Moves the next dataset internal.
        /// </summary>
        /// <returns>
        ///     True if there is another <c>Dataset</c>; otherwise false.
        /// </returns>
        protected override bool MoveNextDatasetInternal()
        {
            if (DatasetPosition > -1)
            {
                return false;
            }

            // read header row
            if (!Read())
            {
                return false;
            }

            if (_csvReader.Context.Parser.Record != null)
            {
                if (_csvReader.Context.Parser.Record.Any(x => x.Contains(IdLabelSeparator)))
                {
                    _containsIdsAndLabels = true;

                }

                _containsActionColumn = _csvReader.Context.Parser.Record[2].Equals("ACTION", StringComparison.OrdinalIgnoreCase);
                _csvHeaders = ReadHeaders(_csvReader.Context.Parser.Record, _containsActionColumn);
            }

            // read 1-st data row to obtain dataflow ID
            if (!Read())
            {
                return false;
            }

            DatasetHeader = GetHeader(_csvReader[1], _containsActionColumn ? _csvReader[2] : null);

            return true;
        }

        /// <summary>
        ///     Moves the next key-able (internal).
        /// </summary>
        /// <returns>
        ///     True if there is another <c>Key-able</c>; otherwise false.
        /// </returns>
        protected override bool MoveNextKeyableInternal()
        {
            do
            {
                _buffer = _csvReader.Context.Parser.Record;

                if (IsNewSeriesKey())
                {
                    _readNextObservation = false;
                    _seriesKey = GetKey();
                    return true;
                }
            }
            while (Read());

            return false;
        }

        /// <summary>
        ///     Moves the next observation internal.
        /// </summary>
        /// <returns>
        ///     True if there is another <c>observation</c>; otherwise false.
        /// </returns>
        protected override bool MoveNextObservationInternal()
        {
            if (_readNextObservation && !Read())
            {
                return false;
            }

            _buffer = _csvReader.Context.Parser.Record;

            var datasetAttributes = new List<KeyValuePair<string, Item>>();

            foreach (var attr in DataStructure.Attributes.Where(x => x.AttachmentLevel == AttributeAttachmentLevel.DataSet && _csvHeaders.ContainsKey(x.Id)))
            {
                datasetAttributes.Add(new KeyValuePair<string, Item>(attr.Id, new Item(_csvHeaders[attr.Id], attr.Representation?.TextFormat == null)));
            }

            _datasetAttributes = BuildKey(datasetAttributes);

            return _readNextObservation = !IsNewSeriesKey();
        }


        /// <summary>
        ///     Sets the current DSD.
        /// </summary>
        /// <param name="currentDsd">
        ///     The current DSD.
        /// </param>
        /// <exception cref="SdmxNotImplementedException">
        ///     Time series without time dimension
        /// </exception>
        protected override void SetCurrentDsd(IDataStructureObject currentDsd)
        {
            base.SetCurrentDsd(currentDsd);

            _buffer = _csvReader.Context.Parser.Record;
            _dimensions = new List<KeyValuePair<string, Item>>();
            _obsAttributes = new List<KeyValuePair<string, Item>>();

            var datasetAttributes = new List<KeyValuePair<string, Item>>();

            if (_valueIndex != -1 && currentDsd.DimensionList.Dimensions.Any(x => !_csvHeaders.ContainsKey(x.Id)))
            {
                throw new SdmxSemmanticException(
                    new SdmxSemmanticException(string.Join(",", currentDsd.DimensionList.Dimensions.Select(x => x.Id))),
                    ExceptionCode.SdmxCsvMissingDimensions);
            }

            foreach (var dim in currentDsd.DimensionList.Dimensions.Where(x => _csvHeaders.ContainsKey(x.Id)))
            {
                if (dim.TimeDimension)
                {
                    _timeIndex = _csvHeaders[dim.Id];
                }
                else
                {
                    _dimensions.Add(new KeyValuePair<string, Item>(dim.Id, new Item(_csvHeaders[dim.Id], true)));
                }
            }

            foreach (var attr in currentDsd.Attributes.Where(x => _csvHeaders.ContainsKey(x.Id)))
            {
                // TODO: The group information is not available, so for now all of the dimensions need to be present for a group attribute
                if ((attr.AttachmentLevel == AttributeAttachmentLevel.Observation || attr.AttachmentLevel == AttributeAttachmentLevel.Group) && 
                        currentDsd.DimensionList.Dimensions.Any(x => !_csvHeaders.ContainsKey(x.Id)) ||
                    attr.DimensionReferences.Any(x => !_csvHeaders.ContainsKey(x)))
                {
                    throw new SdmxSemmanticException(
                        new SdmxSemmanticException(string.Join(",", currentDsd.DimensionList.Dimensions.Select(x => x.Id))),
                        ExceptionCode.SdmxCsvMissingDimensions);
                }

                var attrList = attr.AttachmentLevel == AttributeAttachmentLevel.DataSet ? datasetAttributes : _obsAttributes;

                attrList.Add(new KeyValuePair<string, Item>(attr.Id, new Item(_csvHeaders[attr.Id], attr.Representation?.TextFormat == null)));
            }

            _datasetAttributes = BuildKey(datasetAttributes);
        }

        /// <summary>
        ///     Sets the current MSD.
        /// </summary>
        /// <param name="currentMsd">
        ///     The current MSD.
        /// </param>
        protected override void SetCurrentMsd(IMetadataStructureDefinitionObject currentMsd)
        {
            base.SetCurrentMsd(currentMsd);

            _buffer = _csvReader.Context.Parser.Record;
            _metadataAttributes = new List<KeyValuePair<string, Item>>();

            foreach (var reportStructure in currentMsd.ReportStructures)
            {
                foreach (var metadataAttribute in reportStructure.MetadataAttributes)
                {
                    SetMetadataAttributes(metadataAttribute, metadataAttribute.Id);
                }
            }
        }

        private void SetMetadataAttributes(IMetadataAttributeObject metadataAttributeObject, string id)
        {
            if (_csvHeaders.TryGetValue(id, out var index))
            {
                _metadataAttributes.Add(new KeyValuePair<string, Item>(id, new Item(index, metadataAttributeObject.Representation?.TextFormat == null)));
            }

            if (!metadataAttributeObject.Presentational.IsTrue)
            {
                return;
            }

            foreach (var metadataAttribute in metadataAttributeObject.MetadataAttributes)
            {
                SetMetadataAttributes(metadataAttribute, metadataAttribute.IdentifiableParent.Id + "." + metadataAttribute.Id);
            }
        }

        /// <summary>
        ///     Moves the read position back to the start of the Data Set (<see cref="IDataReaderEngine.KeyablePosition" /> moved
        ///     back to -1)
        /// </summary>
        public override void Reset()
        {
            base.Reset();
            CloseStreams();

            var buffer = new char[32];

            using (var sr = new StreamReader(DataLocation.InputStream))
            {
                sr.Read(buffer, 0, 32);
            }

            DataLocation.InputStream.Position = 0;

            var marker = new string(buffer);

            if (!marker.StartsWith("STRUCTURE", StringComparison.OrdinalIgnoreCase))
            {
                throw new InvalidOperationException("The STRUCTURE column is missing.");
            }

            var delimiter = marker[9].Equals('[') ? marker[12] : marker[9];
            var markerSegments = marker.Split(delimiter);

            if (!markerSegments[1].Equals("STRUCTURE_ID", StringComparison.OrdinalIgnoreCase))
            {
                throw new InvalidOperationException("The STRUCTURE_ID column is missing.");
            }

            try
            {
                _csvReader = new CsvReader(
                    new StreamReader(DataLocation.InputStream), 
                    new CsvConfiguration(CultureInfo.InvariantCulture)
                    {
                        Delimiter = delimiter.ToString(), 
                        HasHeaderRecord = false
                    });
            }
            catch (ConfigurationException ex)
            {
                throw new SdmxSemmanticException(ExceptionCode.SdmxCsvInvalidDelimiter, ex);
            }
        }

        private void CloseStreams()
        {
            if (_csvReader != null)
            {
                _csvReader.Dispose();
            }
        }

        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="managed">
        ///     <c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only
        ///     unmanaged resources.
        /// </param>
        protected override void Dispose(bool managed)
        {
            if (managed)
            {
                CloseStreams();

                if (DataLocation != null)
                {
                    DataLocation.Close();
                    DataLocation = null;
                }
            }

            base.Dispose(managed);
        }

        private Dictionary<string, int> ReadHeaders(string[] input, bool containsActionColumn)
        {
            var dict = new Dictionary<string, int>();

            for (var i = containsActionColumn ? 3 : 2; i < input.Length; i++)
            {
                if (input[i].Equals("OBS_VALUE"))
                {
                    _valueIndex = i;
                    continue;
                }

                var header = SanitizeValue(input[i]);

                if (header.Contains('['))
                {
                    // E.g.: CONTACT[].CONTACT_NAME
                    header = header.Replace("[]", string.Empty);

                    // E.g.: STRING_MULTILANG_TYPE[de;en;fr]
                    if (header.Contains('['))
                    {
                        header = header.Remove(header.IndexOf('['));
                    }
                }

                dict.Add(header, i);
            }

            return dict;
        }

        private string SanitizeValue(string str)
        {
            return string.IsNullOrEmpty(str) || !_containsIdsAndLabels
                ? str
                : str.Split(new[] {IdLabelSeparator}, StringSplitOptions.RemoveEmptyEntries)[0].Trim();
        }

        private IDatasetHeader GetHeader(string urn, string action)
        {
            urn = SanitizeValue(urn);
            var match = urnRegex.Match(urn);

            if(!match.Success)
            {
                throw new InvalidOperationException(string.Format("Dataflow id is in a wrong format: [{0}], it should be 'AgencyId:DataflowId(Version)'", urn));
            }

            var dataflowRef = new StructureReferenceImpl(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow))
            {
                MaintainableId = match.Groups[2].Value,
                AgencyId = match.Groups[1].Value,
                Version = match.Groups[3].Value
            };

            var datasetAction = string.IsNullOrEmpty(action) ? DatasetAction.GetFromEnum(DatasetActionEnumType.Information) : DatasetAction.GetAction(action);

            return new DatasetHeaderCore(null, datasetAction, new DatasetStructureReferenceCore(dataflowRef));
        }

        private bool IsNewSeriesKey()
        {
            return _seriesKey != GetKey();
        }

        private string GetKey()
        {
            return string.Join("_", _dimensions.Select(kvp => _buffer[kvp.Value.Index]));
        }

        private bool Read()
        {
            return HasNext = _csvReader.Read();
        }

        private List<IKeyValue> BuildKey(List<KeyValuePair<string, Item>> map)
        {
            return new List<IKeyValue>(
                map.Select(kvp => new KeyValueImpl(
                    kvp.Value.Sanitize ? SanitizeValue(_buffer[kvp.Value.Index]) : _buffer[kvp.Value.Index],
                    kvp.Key)));
        }
    }
}
