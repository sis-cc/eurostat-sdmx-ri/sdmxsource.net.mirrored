// -----------------------------------------------------------------------
// <copyright file="MetadataWriterEngine.cs" company="EUROSTAT">
//   Date Created : 2020-02-24
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxDataParser.
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
using Org.Sdmx.Resources.SdmxMl.Schemas.V21.MetaData.Generic;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Format;
using Org.Sdmxsource.Sdmx.Api.Model.Header;
using Org.Sdmxsource.Sdmx.Api.Model.Metadata;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Util;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Metadata;

namespace Org.Sdmxsource.Sdmx.DataParser.Engine
{
    public class MetadataParserEngine : IMetadataParseEngine
    {
        public IMetadata Build(IReadableDataLocation dataLocation)
        {
            try
            {
                using (StreamReader sr = new StreamReader(dataLocation.InputStream))
                {
                    using (JsonTextReader reader = new JsonTextReader(sr))
                    {
                        return commonBuild((JObject)JToken.ReadFrom(reader));
                    }
                }
            }
            catch (JsonException e)
            {
                throw new SdmxException("Error while attempting to process SDMX-Json Metadatasets file", e);
            }
            catch (IOException e)
            {
                throw new SdmxException("Error while attempting to process SDMX-Json Metadatasets file", e);
            }
        }

        private IMetadata commonBuild(JObject jObject)
        {
            GenericMetadata metadata = new GenericMetadata();

            metadata.Content.Header = buildHead(jObject);

            metadata.Content.DataSet = buildData(jObject);

            return new MetadataObjectCore(metadata);
        }

        private GenericMetadataHeaderType buildHead(JObject jObject)
        {
            string id = null;
            DateTime? prepared = null;
            IList<IParty> receiver = null;
            IParty sender = null;


            var header = new GenericMetadataHeaderType();

            JToken baseHeaderType;
            jObject.TryGetValue("meta", StringComparison.OrdinalIgnoreCase, out baseHeaderType);
            if (baseHeaderType["id"] != null)
            {
                header.ID = baseHeaderType.Value<string>("id");
            }

            if (baseHeaderType["prepared"] != null)
            {
                //header.Prepared = baseHeaderType.Value<DateTime>("prepared");
            }

            if (baseHeaderType["receiver"] != null)
            {
                JArray jaReceiver = (JArray)baseHeaderType["receiver"];

                if (jaReceiver.HasValues)
                {
                    List<ITextTypeWrapper> receiverName = new List<ITextTypeWrapper>();
                    receiver = new List<IParty>();

                    foreach (JObject joReceiver in jaReceiver)
                    {
                        receiver.Add(new PartyCore(receiverName, joReceiver.Value<string>("id"), new List<IContact>(), null));
                    }
                    //TODO how to convert
                    //header.Receiver = receiver;
                }
            }

            if (baseHeaderType["sender"] != null)
            {
                JObject joSender = (JObject)baseHeaderType["sender"];

                if (joSender.HasValues)
                {
                    List<ITextTypeWrapper> senderName = null;
                    sender = new PartyCore(senderName, joSender.Value<string>("id"), null, null);
                    //TODO how to convert
                    //header.Sender = sender;
                }
            }

            JToken dataToken;
            jObject.TryGetValue("data", StringComparison.OrdinalIgnoreCase, out dataToken);

            if (dataToken["metadataSets"] != null)
            {
                foreach (var item in (JArray)dataToken["metadataSets"])
                {
                    var structureType = new GenericMetadataStructureType();
                    var structure = new MetadataStructureReferenceType();
                    var refType = new MetadataStructureRefType();

                    var structureRef = item.Value<string>("structureRef");
                    var substr = structureRef.Split('=')[1];
                    refType.agencyID = substr.Split(':')[0];
                    refType.id = substr.Split(':')[1].Split('(')[0];
                    refType.version = substr.Split('(')[1].Split(')')[0];
                    structure.SetTypedRef(refType);
                    structureType.Structure = structure;
                    structureType.structureID = $"urn:sdmx:org.sdmx.infomodel.metadatastructure.MetadataStructure={refType.agencyID}:{refType.id}({refType.version})";
                    header.Structure.Add(structureType);
                }
            }

            return header;
        }

        private List<MetadataSetType> buildData(JObject jObject)
        {
            var metadataSetTypes = new List<MetadataSetType>();

            JToken dataToken;
            jObject.TryGetValue("data", StringComparison.OrdinalIgnoreCase, out dataToken);

            if (dataToken["metadataSets"] != null)
            {
                foreach (var itemMetadata in (JArray)dataToken["metadataSets"])
                {
                    var newDataSet = new MetadataSetType();

                    newDataSet.setID = itemMetadata.Value<string>("id");

                    newDataSet.publicationPeriod = itemMetadata.Value<string>("publicationPeriod");
                    newDataSet.publicationYear = itemMetadata.Value<DateTime?>("publicationYear");
                    if (itemMetadata["reportingBegin"] != null)
                    {
                        newDataSet.reportingBeginDate = itemMetadata.Value<string>("reportingBegin");
                    }
                    if (itemMetadata["reportingEnd"] != null)
                    {
                        newDataSet.reportingEndDate = itemMetadata.Value<string>("reportingEnd");
                    }
                    newDataSet.validFromDate = itemMetadata.Value<DateTime?>("validFrom");
                    newDataSet.validToDate = itemMetadata.Value<DateTime?>("validTo");

                    if (itemMetadata["names"] != null)
                    {
                        foreach (var local in (JObject)itemMetadata["names"])
                        {
                            newDataSet.Name.Add(new Name() { lang = local.Key, TypedValue = local.Value.Value<string>() });
                        }
                    }
                    newDataSet.Annotations = createAnnotation(itemMetadata);
                    
                    newDataSet.structureRef = itemMetadata.Value<string>("structureRef");

                    foreach (var itemReport in (JArray)itemMetadata["reports"])
                    {
                        var newReport = new ReportType();
                        newReport.id = itemReport.Value<string>("id");
                        newReport.Annotations = createAnnotation(itemReport);

                        //START Target
                        var objTarget = (JObject)itemReport["target"];
                        var mainTarget = new TargetType();
                        mainTarget.id = objTarget.Value<string>("id");
                        newReport.Target = mainTarget;

                        var listReferenceValueType = new List<ReferenceValueType>();
                        foreach (var itemReferenceValues in (JArray)objTarget["referenceValues"])
                        {
                            var itemReferenceValueType = new ReferenceValueType();
                            itemReferenceValueType.id = itemReferenceValues.Value<string>("id");

                            if (itemReferenceValues.Value<string>("constraintContent") != null)
                            {
                                var itemObjectReferenceType = new AttachmentConstraintReferenceType();
                                itemObjectReferenceType.URN = new List<Uri>();
                                itemObjectReferenceType.URN.Add(new Uri(itemReferenceValues.Value<string>("constraintContent")));
                                itemReferenceValueType.ConstraintContentReference = itemObjectReferenceType;
                            }
                            if (itemReferenceValues["dataKey"] != null)
                            {
                                var dataKeys = (JObject)itemReferenceValues["dataKey"];
                                var itemKey = new DataKeyType();
                                itemKey.include = dataKeys.Value<bool>("include");
                                itemKey.KeyValue = new List<ComponentValueSetType>();
                                foreach (var itemKeyValue in itemReferenceValues["keyValues"])
                                {
                                    var itemValue = new MetadataAttributeValueSetType();
                                    itemValue.id = itemKeyValue.Value<string>("id");
                                    itemValue.include = itemKeyValue.Value<bool>("include");
                                    //TODO how to set value string?
                                    itemKey.KeyValue.Add(itemValue);
                                }
                                itemReferenceValueType.DataKey = itemKey;
                            }
                            if (itemReferenceValues["dataSet"] != null)
                            {
                                var dataSet = (JObject)itemReferenceValues["dataSet"];
                                itemReferenceValueType.DataSetReference = new SetReferenceType();
                                var dataProviderRef = new DataProviderReferenceType();
                                dataProviderRef.URN = new List<Uri>();
                                dataProviderRef.URN.Add(new Uri(dataSet.Value<string>("dataProvider")));
                                itemReferenceValueType.DataSetReference.DataProvider = dataProviderRef;
                                itemReferenceValueType.DataSetReference.ID = dataSet.Value<string>("id");
                            }
                            if (itemReferenceValues.Value<string>("object") != null)
                            {
                                var itemObjectReferenceType = new ObjectReferenceType();
                                itemObjectReferenceType.URN = new List<Uri>();
                                itemObjectReferenceType.URN.Add(new Uri(itemReferenceValues.Value<string>("object")));
                                itemReferenceValueType.ObjectReference = itemObjectReferenceType;
                            }
                            if (itemReferenceValues.Value<string>("reportPeriod") != null)
                            {
                                itemReferenceValueType.ReportPeriod = itemReferenceValues.Value<string>("reportPeriod");
                            }
                            listReferenceValueType.Add(itemReferenceValueType);
                        }
                        mainTarget.ReferenceValue = listReferenceValueType;
                        //END Target

                        //START AttributeSet
                        var objAttributeSet = (JObject)itemReport["attributeSet"];
                        var itemAttributeSetType = new AttributeSetType();
                        foreach (var itemReportedAttributes in (JArray)objAttributeSet["reportedAttributes"])
                        {
                            var itemReportedAttributeType = new ReportedAttributeType();
                            itemReportedAttributeType.id = itemReportedAttributes.Value<string>("id");
                            itemReportedAttributeType.value = itemReportedAttributes.Value<string>("value");
                            if (itemReportedAttributes["texts"] != null)
                            {
                                itemReportedAttributeType.Text = new List<Text>();
                                foreach (var local in (JObject)itemReportedAttributes["texts"])
                                {
                                    itemReportedAttributeType.Text.Add(new Text() { lang = local.Key, TypedValue = local.Value.Value<string>() });
                                }
                            }
                            if (itemReportedAttributes["texts"] != null)
                            {
                                itemReportedAttributeType.StructuredText = new List<StructuredText>();
                                foreach (var local in (JObject)itemReportedAttributes["texts"])
                                {
                                    var itemAdd = new StructuredText() { lang = local.Key };
                                    //itemAdd.Untyped TODO how to popolate?
                                    itemReportedAttributeType.StructuredText.Add(itemAdd);
                                }
                            }
                            itemReportedAttributeType.Annotations = null;
                            itemReportedAttributeType.Annotations = createAnnotation(itemReportedAttributes);
                            itemAttributeSetType.ReportedAttribute.Add(itemReportedAttributeType);
                        }
                        newReport.AttributeSet = itemAttributeSetType;
                        //END AttributeSet

                        if (itemReport["dataProvider"] != null)
                        {
                            var newDataProvider = new DataProviderReferenceType();
                            newDataProvider.URN = new List<Uri>();
                            newDataProvider.URN.Add(new Uri(itemReport.Value<string>("dataProvider")));
                            newDataSet.DataProvider = newDataProvider;
                        }

                        newDataSet.Report.Add(newReport);
                    }
                    metadataSetTypes.Add(newDataSet);
                }
            }
            return metadataSetTypes;
        }

        private Annotations createAnnotation(JToken itemMetadata)
        {
            if (itemMetadata != null && itemMetadata["annotations"] != null)
            {
                var annotations = new Annotations();
                foreach (var itemAnnotation in (JArray)itemMetadata["annotations"])
                {
                    var addItem = new AnnotationType();
                    if (itemAnnotation["id"] != null)
                    {
                        addItem.id = itemAnnotation.Value<string>("id");
                    }
                    if (itemAnnotation["texts"] != null)
                    {
                        addItem.AnnotationText = new List<TextType>();
                        foreach (var local in (JObject)itemAnnotation["texts"])
                        {
                            addItem.AnnotationText.Add(new TextType() { lang = local.Key, TypedValue = local.Value.Value<string>() });
                        }
                    }
                    if (itemAnnotation["type"] != null)
                    {
                        addItem.AnnotationType1 = itemAnnotation.Value<string>("type");
                    }
                    annotations.Annotation.Add(addItem);
                }
                return annotations;
            }
            return null;
        }

    }
}
