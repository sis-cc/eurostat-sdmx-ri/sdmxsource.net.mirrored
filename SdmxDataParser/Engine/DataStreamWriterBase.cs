// -----------------------------------------------------------------------
// <copyright file="DataStreamWriterBase.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxDataParser.
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Xml;

    using Estat.Sri.SdmxParseBase.Engine;
    using Estat.Sri.SdmxParseBase.Model;
    using Estat.Sri.SdmxXmlConstants;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects;
    using Org.Sdmxsource.Util.Attributes;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     This is a base class for all the Writers classes that writes XML files.
    /// </summary>
    public abstract class DataStreamWriterBase : Writer
    {
        /// <summary>
        ///     Contains a mapping from component id - to concept id
        /// </summary>
        private IDictionary<string, string> _componentIdMapping = new Dictionary<string, string>(StringComparer.Ordinal);

        /// <summary>
        ///     The dataset namespace
        /// </summary>
        private NamespacePrefixPair _dataSetNS;

        /// <summary>
        ///     Default observation value to use in case it is null or empty
        /// </summary>
        private string _defaultObs = null;

        /// <summary>
        ///     The dimension at observation
        /// </summary>
        private string _dimensionAtObservation;

        /// <summary>
        ///     The comment to be added to the generated file
        /// </summary>
        private string _generatedFileComment;

        /// <summary>
        ///     The message header
        /// </summary>
        private IHeader _header;

        /// <summary>
        ///     This field holds if this message is a delete message
        /// </summary>
        private bool _isDeleteMessage;

        /// <summary>
        ///     The KeyFamily used by the dataset
        /// </summary>
        protected IDataStructureObject _keyFamilyBean;

        /// <summary>
        ///     The optional internal field used to store the namespace Uri
        /// </summary>
        private string _namespace;

        /// <summary>
        ///     The optional internal field used to store the namespace prefix
        /// </summary>
        private string _namespacePrefix;

        /// <summary>
        ///     The namespace count
        /// </summary>
        private int _prefixCount = 1;

        /// <summary>
        ///     A value indicating whether the
        ///     <see cref="StartDataset" />
        ///     has been called
        /// </summary>
        private bool _startedDataSet;

        /// <summary>
        ///     A value indicating whether the header has been written
        /// </summary>
        private bool _startedHeader;

        protected IDatasetHeader _datasetHeader;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataStreamWriterBase" /> class.
        /// </summary>
        /// <param name="writer">
        ///     The writer.
        /// </param>
        /// <param name="schema">
        ///     The schema.
        /// </param>
        protected DataStreamWriterBase(XmlWriter writer, SdmxSchema schema)
            : this(writer, CreateDataNamespaces(schema), schema)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataStreamWriterBase" /> class.
        /// </summary>
        /// <param name="writer">
        ///     The writer.
        /// </param>
        /// <param name="namespaces">
        ///     The namespaces.
        /// </param>
        /// <param name="schema">
        ///     The schema.
        /// </param>
        protected DataStreamWriterBase([ValidatedNotNull]XmlWriter writer, SdmxNamespaces namespaces, [ValidatedNotNull]SdmxSchema schema)
            : base(writer, namespaces, schema)
        {
            Validate(namespaces);
        }

        /// <summary>
        ///     Gets or sets the comment to add to the generated file
        /// </summary>
        public string GeneratedFileComment
        {
            get
            {
                return this._generatedFileComment;
            }

            set
            {
                this._generatedFileComment = value;
            }
        }

        /// <summary>
        ///     Gets or sets the namespace uri used by the generated sdmx file
        /// </summary>
        public string Namespace
        {
            get
            {
                return this._namespace;
            }

            set
            {
                this._namespace = value;
            }
        }

        /// <summary>
        ///     Gets or sets the namespace prefix used by the generated sdmx file
        /// </summary>
        public string NamespacePrefix
        {
            get
            {
                return this._namespacePrefix;
            }

            set
            {
                this._namespacePrefix = value;
            }
        }

        /// <summary>
        ///     Gets the data format namespace Suffix e.g. compact
        /// </summary>
        protected abstract BaseDataFormatEnumType DataFormatType { get; }

        /// <summary>
        ///     Gets the default observation value to use in case it is null or empty
        /// </summary>
        protected string DefaultObs
        {
            get
            {
                return this._defaultObs;
            }
        }

        /// <summary>
        ///     Gets the dimension at observation.
        /// </summary>
        protected virtual string DimensionAtObservation
        {
            get
            {
                return this._dimensionAtObservation;
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether to enable schema location.
        /// </summary>
        /// <value><c>true</c> if schema location is enabled; otherwise, <c>false</c>.</value>
        protected bool EnabledSchemaLocation { get; set; }

        /// <summary>
        ///     Gets a value indicating whether this is a delete message
        /// </summary>
        protected bool IsDeleteMessage
        {
            get
            {
                return this._isDeleteMessage;
            }
        }

        /// <summary>
        ///     Gets or sets the SDMX KeyFamily
        /// </summary>
        protected IDataStructureObject KeyFamily
        {
            get
            {
                return this._keyFamilyBean;
            }

            set
            {
                this._keyFamilyBean = value;
            }
        }

        /// <summary>
        ///     Gets the Message element tag
        /// </summary>
        protected abstract string MessageElement { get; }

        /// <summary>
        ///     Gets or sets a value indicating whether the
        ///     <see cref="DataWriterEngineBase.StartDataset" />
        ///     has been called
        /// </summary>
        protected bool StartedDataSet
        {
            get
            {
                return this._startedDataSet;
            }

            set
            {
                this._startedDataSet = value;
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the message tag <see cref="MessageElement" /> has been written
        /// </summary>
        /// <value><c>true</c> if the message has been written; otherwise, <c>false</c>.</value>
        protected bool StartedMessage { get; set; }

        /// <summary>
        ///     Starts a dataset with the data conforming to the DSD
        /// </summary>
        /// <param name="dataflow">
        ///     dataflow (optional) The dataflow can be provided to give extra information about the dataset
        /// </param>
        /// <param name="dataStructure">
        ///     The <see cref="IDataStructureObject" /> for which the dataset will be created
        /// </param>
        /// <param name="header">
        ///     The <see cref="IDatasetHeader" /> of the dataset
        /// </param>
        /// <param name="annotations">
        ///     The annotations.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        ///     if the <paramref name="dataflow" /> is null
        /// </exception>
        public virtual void StartDataset(
            IDataflowObject dataflow, 
            IDataStructureObject dataStructure, 
            IDatasetHeader header, 
            params IAnnotation[] annotations)
        {
            if (dataStructure == null)
            {
                throw new ArgumentNullException("dataStructure");
            }

            this._datasetHeader = header;

            this._componentIdMapping = new Dictionary<string, string>(StringComparer.Ordinal);
            foreach (IComponent currentComponent in dataStructure.Components)
            {
                this._componentIdMapping[currentComponent.Id] = this.GetComponentId(currentComponent);
            }

            this._keyFamilyBean = dataStructure;

            if (!this._startedHeader)
            {
                this._dimensionAtObservation = this.GetDimensionAtObservation();

                var headerWriter = new HeaderWriter(this.SdmxMLWriter, this.Namespaces, this.TargetSchema);

                var artefactForHeader = GetHeaderStructure(dataflow, dataStructure, _datasetHeader);
                this.WriteMessageTag(artefactForHeader);

                if (IsHigherThanTwoPoinZero && artefactForHeader.StructureType.EnumType == SdmxStructureEnumType.Dataflow)
                {
                    headerWriter.WriteHeader(this._header, this._dimensionAtObservation, _datasetHeader?.DataStructureReference, dataflow);
                }
                else
                {
                    headerWriter.WriteHeader(this._header, this._dimensionAtObservation, _datasetHeader?.DataStructureReference, dataStructure);
                }
                this.ParseAction(this._header);
                if (this.IsDeleteMessage)
                {
                    this._defaultObs = null;
                }

                this._startedHeader = true;
            }

            this.WriteFormatDataSet(header,dataflow);
            this._startedDataSet = true;
        }

        /// <summary>
        ///     Write the specified <paramref name="header" />
        /// </summary>
        /// <param name="header">
        ///     The SDMX header.
        /// </param>
        public virtual void WriteHeader(IHeader header)
        {
            this._header = header;
        }

        /// <summary>
        ///     Build the Header Artefact specific URN and set <see cref="_namespace" />
        /// </summary>
        /// <param name="namespacePrefix">
        ///     The namespace prefix
        /// </param>
        /// <param name="artefactForHeader">
        ///     The artefact specified in the header
        /// </param>
        /// <returns>
        ///     The <see cref="NamespacePrefixPair" />.
        /// </returns>
        protected virtual NamespacePrefixPair BuildArtefactSpecificUrn(string namespacePrefix, IMaintainableObject artefactForHeader)
        {
            //// TODO if namespaces.UseEstatUrn is set we use the ESTAT Registry way.. 
            if (this.DimensionAtObservation == null)
            {
                this.Namespaces.UseEstatUrn = true;
            }

            if (this.TargetSchema.EnumType == SdmxSchemaEnumType.VersionTwo && this.Namespaces.UseEstatUrn)
            {
                this._namespace = string.Format(
                    CultureInfo.InvariantCulture, 
                    "{0}{1}:{2}:{3}:{4}",
                    artefactForHeader.StructureType.EnumType == SdmxStructureEnumType.Dsd
                    ? "urn:estat:sdmx.infomodel.keyfamily.KeyFamily=" : "urn:estat:sdmx.infomodel.datastructure.Dataflow=", 
                    artefactForHeader.AgencyId,
                    artefactForHeader.Id,
                    artefactForHeader.Version, 
                    GetDataFormat(this.DataFormatType));
            }
            else if(IsHigherThanTwoPoinZero)
            {
                // TODO explict measure possibly in a different implementation ?
                this._namespace = string.Format(
                    CultureInfo.InvariantCulture, 
                    "{0}:ObsLevelDim:{1}",
                    artefactForHeader.Urn, 
                    this.DimensionAtObservation);
                if (this.TargetSchema.EnumType == SdmxSchemaEnumType.VersionTwoPointOne
                    && this._datasetHeader!= null 
                    && this._datasetHeader.DataStructureReference.IsExplicitMeasures)
                {
                    this._namespace += ":explicit";
                }
            }
            else
            {
                this.Namespace = artefactForHeader.Urn + ":compact";
            }

            return new NamespacePrefixPair(this._namespace, this._namespacePrefix);
        }

        /// <summary>
        ///     Checks if Dataset has started
        /// </summary>
        /// <param name="message">
        ///     The message.
        /// </param>
        /// <exception cref="InvalidOperationException">
        ///     <see cref="StartDataset" />
        ///     has not been called
        /// </exception>
        protected void CheckDataSet(string message)
        {
            if (!this._startedDataSet)
            {
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        ///     Close the root *Data tag
        /// </summary>
        protected void CloseMessageTag()
        {
            if (this.StartedMessage)
            {
                this.WriteEndElement();
                this.WriteEndDocument();
            }
        }

        /// <summary>
        ///     Conditionally end DataSet element
        /// </summary>
        protected virtual void EndDataSet()
        {
            if (this._startedDataSet)
            {
                this.WriteEndElement();
                this._startedDataSet = false;
            }
        }

        /// <summary>
        ///     Returns the id that should be output for a component Id.  For a 1.0 and 2.0 data message this is the Id of the
        ///     Concept.
        /// </summary>
        /// <param name="componentId">
        ///     The component id.
        /// </param>
        /// <returns>
        ///     The id that should be output for a component Id.
        /// </returns>
        protected string GetComponentId(string componentId)
        {
            if (this.IsHigherThanTwoPoinZero)
            {
                return componentId;
            }

            string value;
            if (this._componentIdMapping.TryGetValue(componentId, out value))
            {
                return value;
            }

            return componentId;
        }

        /// <summary>
        ///     Gets the component identifier.
        /// </summary>
        /// <param name="component">The component.</param>
        /// <returns>The component identifier.</returns>
        protected string GetComponentId(IComponent component)
        {
            if (component == null)
            {
                return null;
            }

            if (this.IsHigherThanTwoPoinZero)
            {
                return component.Id;
            }

            return ConceptRefUtil.GetConceptId(component.ConceptRef);
        }

        /// <summary>
        ///     Gets the dimension at observation.
        /// </summary>
        /// <param name="header">
        ///     The dataset header.
        /// </param>
        /// <returns>
        ///     The dimension at observation
        /// </returns>
        protected virtual string GetDimensionAtObservation()
        {
            string dimensionAtObservation = DimensionObject.TimeDimensionFixedId;
            if (this._datasetHeader != null && IsHigherThanTwoPoinZero
                && (this._datasetHeader.DataStructureReference != null
                    && !string.IsNullOrWhiteSpace(this._datasetHeader.DataStructureReference.DimensionAtObservation)))
            {
                dimensionAtObservation = this._datasetHeader.DataStructureReference.DimensionAtObservation;
            }

            return dimensionAtObservation;
        }

        /// <summary>
        ///     Gets schema location for the Structure Specific datasets; otherwise returns an empty string
        /// </summary>
        /// <param name="sdmxSchema">
        ///     The sdmx schema.
        /// </param>
        /// <returns>
        ///     The schema location for the Structure Specific datasets; otherwise returns an empty string
        /// </returns>
        protected virtual string GetSchemaLocation(SdmxSchemaEnumType sdmxSchema)
        {
            return string.Empty;
        }

        /// <summary>
        ///     Parse the Header.DataSetAction and update the <see cref="IsDeleteMessage" />
        /// </summary>
        /// <param name="header">
        ///     The SDMX Header
        /// </param>
        protected void ParseAction(IHeader header)
        {
            this._isDeleteMessage = header != null && header.Action != null
                                    && header.Action.EnumType == DatasetActionEnumType.Delete;
        }

        /// <summary>
        ///     Writes the dataset attributes.
        /// </summary>
        /// <param name="header">
        ///     The dataset header.
        /// </param>

        protected void WriteDataSetHeader(IDatasetHeader header)
        {
            WriteDataSetHeader(header, null);
        }

        /// <summary>
        ///     Writes the dataset attributes.
        /// </summary>
        /// <param name="header">
        ///     The dataset header.
        /// </param>
        /// <param name="dataflow">The dataflow (optional)</param>
        protected void WriteDataSetHeader(IDatasetHeader header, IDataflowObject dataflow)
        {
            // populate dataset header
            if (header != null)
            {
                this.TryWriteAttribute(AttributeNameTable.publicationPeriod, header.PublicationPeriod);

                // PublicationYear seems to default to -1.
                if (header.PublicationYear > 0)
                {
                    this.TryWriteAttribute(AttributeNameTable.publicationYear, header.PublicationYear);
                }

                if (header.ValidFrom > DateTime.MinValue)
                {
                    this.TryWriteAttribute(AttributeNameTable.validFromDate, header.ValidFrom.ToString("s", DateTimeFormatInfo.InvariantInfo));
                }

                if (header.ValidTo > DateTime.MinValue)
                {
                    this.TryWriteAttribute(AttributeNameTable.validToDate, header.ValidTo.ToString("s", DateTimeFormatInfo.InvariantInfo));
                }

                this.TryWriteAttribute(AttributeNameTable.action, header.Action.Action);

                //// TODO MessageGroup support
            }

            if (this.TargetSchema.EnumType == SdmxSchemaEnumType.VersionTwoPointOne)
            {
                string structureRef = GetRef(GetHeaderStructure(dataflow, this.KeyFamily, header));
                switch (this.DataFormatType)
                {
                    case BaseDataFormatEnumType.Compact:
                        this.WriteAttributeString(
                            this.Namespaces.StructureSpecific21, 
                            AttributeNameTable.dataScope, 
                            "DataStructure");
                        this.WriteAttributeString(
                            this.Namespaces.Xsi, 
                            AttributeNameTable.type, 
                            this.Namespaces.DataSetStructureSpecific.Prefix + ":DataSetType");
                        this.WriteAttributeString(
                            this.Namespaces.StructureSpecific21, 
                            AttributeNameTable.structureRef, 
                            structureRef);
                        break;
                    case BaseDataFormatEnumType.Generic:
                        this.WriteAttributeString(AttributeNameTable.structureRef, structureRef);
                        break;
                }
            }
            else if (this.TargetSchema.EnumType == SdmxSchemaEnumType.VersionThree)
            {
                string structureRef = GetRef(GetHeaderStructure(dataflow, this.KeyFamily, header));
                this.WriteAttributeString(
                    this.Namespaces.StructureSpecific30,
                    AttributeNameTable.dataScope,
                    "DataStructure");
                this.WriteAttributeString(
                    this.Namespaces.Xsi,
                    AttributeNameTable.type,
                    this.Namespaces.DataSetStructureSpecific.Prefix + ":DataSetType");
                this.WriteAttributeString(
                    this.Namespaces.StructureSpecific30,
                    AttributeNameTable.structureRef,
                    structureRef);

            }
        }

        private IMaintainableObject GetHeaderStructure(IDataflowObject dataflow, IDataStructureObject dataStructure, IDatasetHeader datasetHeader)
        {
            if (dataflow != null && datasetHeader != null)
            {
                if (datasetHeader.StructureUsage == null || datasetHeader.StructureUsage == SdmxStructureEnumType.Dataflow)
                {
                    return dataflow;
                }
            }

            return dataStructure;
        }

        /// <summary>
        ///     Writes the DataSet element
        /// </summary>
        /// <param name="header">
        ///     The dataset header
        /// </param>
        /// <param name="dataflow"></param>
        protected abstract void WriteFormatDataSet(IDatasetHeader header, IDataflowObject dataflow);

        /// <summary>
        ///     This method is used to write the root xml tag and *Data tag
        ///     with their corresponding attributes
        /// </summary>
        protected void WriteMessageTag(IMaintainableObject artefactForHeader)
        {
            this.WriteMessageTag(this.MessageElement, artefactForHeader);
        }

        /// <summary>
        ///     This method is used to write the root xml tag and *Data tag
        ///     with their corresponding attributes
        /// </summary>
        /// <param name="element">
        ///     The first element tag name
        /// </param>
        /// <param name="artefactForHeader">
        ///     The artefact specified in the header
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="element"/> is <see langword="null" />.</exception>
        protected void WriteMessageTag(string element, IMaintainableObject artefactForHeader)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }

            // <?xml version="1.0" encoding="UTF-8"?>
            TryWriteStartDocument();

            // Generated File comment
            if (!string.IsNullOrEmpty(this._generatedFileComment))
            {
                this.SdmxMLWriter.WriteComment(this._generatedFileComment);
            }

            // <CompactData
            this.WriteStartElement(this.Namespaces.Message, element);
            this.StartedMessage = true;

            // xmlns="http://www.SDMX.org/resources/SDMXML/schemas/v2_0/message"
            if (this.TargetSchema.EnumType == SdmxSchemaEnumType.VersionTwoPointOne)
            {
                if (this.DataFormatType == BaseDataFormatEnumType.Compact)
                {
                    this.WriteNamespaceDecl(this.Namespaces.StructureSpecific21);
                }

                this.WriteNamespaceDecl(this.Namespaces.Footer);
            }

            if (this.IsThree)
            {
                if (this.DataFormatType == BaseDataFormatEnumType.Compact)
                {
                    this.WriteNamespaceDecl(this.Namespaces.StructureSpecific30);
                }

                this.WriteNamespaceDecl(this.Namespaces.Footer);
            }

            if (string.IsNullOrEmpty(this._namespacePrefix))
            {
                this._namespacePrefix = string.Format(
                    CultureInfo.InvariantCulture, 
                    "{0}{1}", 
                    PrefixConstants.DataSetStructureSpecific, 
                    this._prefixCount);
                this._prefixCount++;
            }

            switch (this.DataFormatType)
            {
                case BaseDataFormatEnumType.Generic:
                    this._dataSetNS = this.Namespaces.Generic;
                    break;
                case BaseDataFormatEnumType.Compact:
                case BaseDataFormatEnumType.CrossSectional:
                    this._dataSetNS = this._namespace == null
                                          ? this.BuildArtefactSpecificUrn(this._namespacePrefix, artefactForHeader)
                                          : new NamespacePrefixPair(this._namespace, this._namespacePrefix);
                    this.Namespaces.DataSetStructureSpecific = this._dataSetNS;
                    break;
            }

            // We are trying to emulate Java behavior here.
            this.WriteNamespaceDecl(this._dataSetNS);

            this.WriteNamespaceDecl(this.Namespaces.Message);
            this.WriteNamespaceDecl(this.Namespaces.Common);

            // xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            this.WriteNamespaceDecl(this.Namespaces.Xsi);
            this.WriteNamespaceDecl(this.Namespaces.Xml);

            if (this.EnabledSchemaLocation)
            {
                // xsi:schemaLocation="http://www.SDMX.org/resources/SDMXML/schemas/v2_0/message SDMXMessage.xsd">
                string schemaLocation = this.Namespaces.SchemaLocation ?? string.Empty;
                string structureSpecific = this.GetSchemaLocation(this.TargetSchema.EnumType);
                if (!string.IsNullOrWhiteSpace(structureSpecific))
                {
                    schemaLocation = string.Format(
                        CultureInfo.InvariantCulture, 
                        "{0} {1}", 
                        schemaLocation, 
                        structureSpecific);
                }

                if (!string.IsNullOrWhiteSpace(schemaLocation))
                {
                    this.WriteAttributeString(this.Namespaces.Xsi, XmlConstants.SchemaLocation, schemaLocation);
                }
            }
        }

        /// <summary>
        ///     The create namespaces.
        /// </summary>
        /// <param name="sdmxSchema">
        ///     The SDMX version
        /// </param>
        /// <returns>
        ///     The <see cref="SdmxNamespaces" />.
        /// </returns>
        private static SdmxNamespaces CreateDataNamespaces(SdmxSchemaEnumType sdmxSchema)
        {
            var namespaces = new SdmxNamespaces
                                 {
                                     Xsi =
                                         new NamespacePrefixPair(
                                         XmlConstants.XmlSchemaNS, 
                                         XmlConstants.XmlSchemaPrefix), 
                                     Xml =
                                         new NamespacePrefixPair(
                                         XmlConstants.XmlNamespace, 
                                         XmlConstants.XmlPrefix)
                                 };
            switch (sdmxSchema)
            {
                case SdmxSchemaEnumType.VersionOne:
                    namespaces.Common = new NamespacePrefixPair(SdmxConstants.CommonNs10, PrefixConstants.Common);
                    namespaces.Message = new NamespacePrefixPair(SdmxConstants.MessageNs10, string.Empty);
                    namespaces.Generic = new NamespacePrefixPair(SdmxConstants.GenericNs10, PrefixConstants.Generic);
                    namespaces.SchemaLocation = string.Format(
                        CultureInfo.InvariantCulture, 
                        "{0} SDMXMessage.xsd", 
                        SdmxConstants.MessageNs10);
                    break;
                case SdmxSchemaEnumType.VersionTwo:
                    namespaces.Common = new NamespacePrefixPair(SdmxConstants.CommonNs20, PrefixConstants.Common);
                    namespaces.Message = new NamespacePrefixPair(SdmxConstants.MessageNs20, string.Empty);
                    namespaces.Generic = new NamespacePrefixPair(SdmxConstants.GenericNs20, PrefixConstants.Generic);
                    namespaces.SchemaLocation = string.Format(
                        CultureInfo.InvariantCulture, 
                        "{0} SDMXMessage.xsd", 
                        SdmxConstants.MessageNs20);
                    break;
                case SdmxSchemaEnumType.VersionTwoPointOne:
                    namespaces.Common = new NamespacePrefixPair(SdmxConstants.CommonNs21, PrefixConstants.Common);
                    namespaces.Message = new NamespacePrefixPair(SdmxConstants.MessageNs21, PrefixConstants.Message);
                    namespaces.Generic = new NamespacePrefixPair(SdmxConstants.GenericNs21, PrefixConstants.Generic);
                    namespaces.Footer = new NamespacePrefixPair(SdmxConstants.FooterNs21, PrefixConstants.Footer);
                    namespaces.StructureSpecific21 = new NamespacePrefixPair(
                        SdmxConstants.StructureSpecificNs21, 
                        PrefixConstants.StructureSpecific21);
                    namespaces.SchemaLocation = string.Format(
                        CultureInfo.InvariantCulture, 
                        "{0} SDMXMessage.xsd", 
                        SdmxConstants.MessageNs21);
                    break;
                case SdmxSchemaEnumType.VersionThree:
                    namespaces.Common = new NamespacePrefixPair(SdmxConstants.CommonNs300, PrefixConstants.Common);
                    namespaces.Message = new NamespacePrefixPair(SdmxConstants.MessageNs300, PrefixConstants.Message);
                    namespaces.Generic = new NamespacePrefixPair(SdmxConstants.GenericNs300, PrefixConstants.Generic);
                    namespaces.Footer = new NamespacePrefixPair(SdmxConstants.MessageFooterNs300, PrefixConstants.Footer);
                    namespaces.StructureSpecific30 = new NamespacePrefixPair(
                        SdmxConstants.StructureSpecificDataNs300,
                        PrefixConstants.StructureSpecific21);
                    namespaces.SchemaLocation = string.Format(
                        CultureInfo.InvariantCulture,
                        "{0} SDMXMessage.xsd",
                        SdmxConstants.MessageNs300);
                    break;
            }

            return namespaces;
        }

        /// <summary>
        ///     Convert <see cref="BaseDataFormatEnumType" /> field to lower
        /// </summary>
        /// <param name="type">
        ///     The  <see cref="BaseDataFormatEnumType" /> field
        /// </param>
        /// <returns>
        ///     The  <see cref="BaseDataFormatEnumType" /> field as string
        /// </returns>
        private static string GetDataFormat(BaseDataFormatEnumType type)
        {
            switch (type)
            {
                case BaseDataFormatEnumType.Compact:
                    return "compact";
                case BaseDataFormatEnumType.CrossSectional:
                    return "cross";
            }

            return null;
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <param name="namespaces">
        ///     The namespaces.
        /// </param>
        /// <exception cref="ArgumentException">
        ///     <paramref name="namespaces" /> is not configured correctly
        /// </exception>
        private static void Validate(SdmxNamespaces namespaces)
        {
            if (namespaces == null)
            {
                throw new ArgumentNullException("namespaces");
            }

            if (namespaces.Message == null || namespaces.Common == null || namespaces.Xsi == null
                || (namespaces.DataSetStructureSpecific == null && namespaces.Generic == null))
            {
                throw new ArgumentException(
                    "One or more of the required namespaces are not set. Please set Message, Common, Xsi and either DataSetStructureSpecific or Generic namespaces. Else use another ctor to use the defaults.");
            }
        }

        /// <summary>
        /// Writes the dataflow values.
        /// </summary>
        /// <param name="dataflow">The dataflow.</param>
        protected void WriteDataflowValues(IDataflowObject dataflow)
        {
            this.TryWriteAttribute(AttributeNameTable.dataflowID, dataflow.Id);
            this.TryWriteAttribute(AttributeNameTable.dataflowAgencyID, dataflow.AgencyId);
        }
    }
}