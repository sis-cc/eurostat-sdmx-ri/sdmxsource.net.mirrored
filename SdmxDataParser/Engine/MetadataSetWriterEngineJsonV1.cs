// -----------------------------------------------------------------------
// <copyright file="MetadataSetWriterEngineJsonV1.cs" company="EUROSTAT">
//   Date Created : 2020-02-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxDataParser.
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using log4net;
using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
using Org.Sdmxsource.Json;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.Api.Model.Metadata;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
using Org.Sdmxsource.Sdmx.Util.Date;

namespace Org.Sdmxsource.Sdmx.DataParser.Engine
{
    public class MetadataSetWriterEngineJsonV1 : IMetadataWriterEngine
    {
        /// <summary>
        /// The logger
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// The jSON generator
        /// </summary>
        private readonly JsonGenerator _jsonGenerator;

        /// <summary>
        /// The Structure jSON Format
        /// </summary>
        private readonly SdmxJsonMetadataFormat _sdmxStructureMetadataJsonFormat;

        /// <summary>
        /// Initializes a new instance of the <see cref="MetadataSetWriterEngineJsonV1"/> class.
        /// </summary>
        /// <param name="outputStream">
        /// The output stream.
        /// </param>
        /// <param name="sdmxStructureMetadataJsonFormat">
        /// The SDMX structure JSON format.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// outputStream
        /// or
        /// translator
        /// or
        /// encoding
        /// </exception>
        public MetadataSetWriterEngineJsonV1(Stream outputStream, SdmxJsonMetadataFormat sdmxStructureMetadataJsonFormat)
        {
            if (outputStream == null)
            {
                throw new ArgumentNullException("outputStream");
            }

            if (sdmxStructureMetadataJsonFormat == null)
            {
                throw new ArgumentNullException("sdmxStructureJsonFormat");
            }

            if (sdmxStructureMetadataJsonFormat.SdmxFormat.EnumType != SdmxSchemaEnumType.Json)
            {
                throw new ArgumentException("Output format must be Json V10");
            }

            _jsonGenerator = new JsonGenerator(new StreamWriter(outputStream, new UTF8Encoding(false)));

            _sdmxStructureMetadataJsonFormat = sdmxStructureMetadataJsonFormat;
        }

        public void WriteMetadataSet(Stream outputStream, params IMetadataSet[] metadataSet)
        {
            //remove Stream from constructor and use this interface?
            writeMetadataSet(metadataSet);
        }

        public void WriteMetadataSet(params IMetadataSet[] metadataSet)
        {
            writeMetadataSet(metadataSet);
        }

        private void writeMetadataSet(params IMetadataSet[] metadataSet)
        {
            if (metadataSet == null)
            {
                throw new ArgumentNullException("metadataSet");
            }

            _jsonGenerator.WriteStartObject();
            _jsonGenerator.WritePropertyName("meta");
            _jsonGenerator.WriteStartObject();
            writeMetaData();
            _jsonGenerator.WriteEndObject();


            _jsonGenerator.WritePropertyName("data");
            _jsonGenerator.WriteStartObject();
            _jsonGenerator.WritePropertyName("metadataSets");
            _jsonGenerator.WriteStartArray();
            foreach (IMetadataSet mdst in metadataSet)
            {
                writeMetadataSet(mdst);
            }
            _jsonGenerator.WriteEndArray();
            _jsonGenerator.WriteEndObject();


            //_jsonGenerator.WritePropertyName("errors");


            _jsonGenerator.WriteEndObject();

            _jsonGenerator.Flush();
            _jsonGenerator.Close();
        }

        private void writeMetaData()
        {
            _jsonGenerator.WritePropertyName("schema");
            _jsonGenerator.WriteValue("https://raw.githubusercontent.com/sdmx-twg/sdmx-json/develop/metadata-message/tools/schemas/1.0/sdmx-json-metadata-schema.json");
            _jsonGenerator.WritePropertyName("id");
            _jsonGenerator.WriteValue(DateTimeOffset.Now.ToUnixTimeMilliseconds());
            _jsonGenerator.WritePropertyName("prepared");
            _jsonGenerator.WriteValue(DateTime.Now.ToString());
            _jsonGenerator.WritePropertyName("sender");
            _jsonGenerator.WriteStartObject();
            _jsonGenerator.WriteEndObject();
            _jsonGenerator.WritePropertyName("receivers");
            _jsonGenerator.WriteStartArray();
            _jsonGenerator.WriteEndArray();
            _jsonGenerator.WritePropertyName("links");
            _jsonGenerator.WriteStartArray();
            _jsonGenerator.WriteEndArray();
        }

        private void writeMetadataSet(IMetadataSet mdst)
        {
            _jsonGenerator.WriteStartObject();
            writeMetadataSetData(mdst);
            _jsonGenerator.WriteEndObject();
        }

        private void writeMetadataSetData(IMetadataSet mdst)
        {
            //Can't find action in mdst
            _jsonGenerator.WritePropertyName("action");
            _jsonGenerator.WriteValue(mdst.Action);
            if (mdst.PublicationPeriod != null)
            {
                _jsonGenerator.WritePropertyName("publicationPeriod");
                _jsonGenerator.WriteValue(mdst.PublicationPeriod);
            }
            if (mdst.PublicationYear != null && mdst.PublicationYear.Date != default(DateTime))
            {
                _jsonGenerator.WritePropertyName("publicationYear");
                _jsonGenerator.WriteValue(mdst.PublicationYear.Date.Year);
            }
            if (mdst.ReportingBeginDate != null && mdst.ReportingBeginDate.Date != default(DateTime))
            {
                _jsonGenerator.WritePropertyName("reportingBegin");
                _jsonGenerator.WriteValue(mdst.ReportingBeginDate.DateInSdmxFormat);
            }
            if (mdst.ReportingEndDate != null && mdst.ReportingEndDate.Date != default(DateTime))
            {
                _jsonGenerator.WritePropertyName("reportingEnd");
                _jsonGenerator.WriteValue(mdst.ReportingEndDate.DateInSdmxFormat);
            }
            if (!string.IsNullOrWhiteSpace(mdst.SetId))
            {
                _jsonGenerator.WritePropertyName("id");
                _jsonGenerator.WriteValue(mdst.SetId.Trim());
            }
            if (mdst.MsdReference != null)
            {
                _jsonGenerator.WritePropertyName("structureRef");
                _jsonGenerator.WriteValue(mdst.MsdReference.TargetUrn);
            }
            if (mdst.ValidFromDate != null && mdst.ValidFromDate.Date != default(DateTime))
            {
                _jsonGenerator.WritePropertyName("validFrom");
                _jsonGenerator.WriteValue(mdst.ValidFromDate.DateInSdmxFormat);
            }
            if (mdst.ValidToDate != null && mdst.ValidToDate.Date != default(DateTime))
            {
                _jsonGenerator.WritePropertyName("validTo");
                _jsonGenerator.WriteValue(mdst.ValidToDate.DateInSdmxFormat);
            }
            CreateAnnotations(mdst.Annotations);
            //Can't find links in mdst
            //_jsonGenerator.WriteArrayFieldStart("links");
            //_jsonGenerator.WriteStartObject();
            //_jsonGenerator.WriteEndObject();
            //_jsonGenerator.WriteEndArray();
            if (mdst.Names != null)
            {
                _jsonGenerator.WritePropertyName("names");
                _jsonGenerator.WriteStartObject();
                foreach (var item in mdst.Names)
                {
                    _jsonGenerator.WritePropertyName(item.Locale);
                    _jsonGenerator.WriteValue(item.Value);
                }
                _jsonGenerator.WriteEndObject();
            }
            string stringDataProvider = null;
            if (mdst.DataProviderReference != null)
            {
                _jsonGenerator.WritePropertyName("dataProvider");
                stringDataProvider = mdst.DataProviderReference.FullId;
                _jsonGenerator.WriteValue(stringDataProvider);
            }
            
            if (mdst.Reports != null)
            {
                _jsonGenerator.WritePropertyName("reports");
                _jsonGenerator.WriteStartArray();
                foreach (var report in mdst.Reports)
                {
                    _jsonGenerator.WriteStartObject();

                    _jsonGenerator.WritePropertyName("id");
                    _jsonGenerator.WriteValue(report.Id);

                    CreateAnnotations(report.Annotations);

                    if (report.Target != null)
                    {
                        _jsonGenerator.WritePropertyName("target");
                        _jsonGenerator.WriteStartObject();
                        _jsonGenerator.WritePropertyName("id");
                        _jsonGenerator.WriteValue(report.Target.Id);
                        if (report.Target.ReferenceValues != null)
                        {
                            _jsonGenerator.WritePropertyName("referenceValues");
                            _jsonGenerator.WriteStartArray();
                            foreach (var refValue in report.Target.ReferenceValues)
                            {
                                _jsonGenerator.WriteStartObject();
                                _jsonGenerator.WritePropertyName("id");
                                _jsonGenerator.WriteValue(refValue.Id);
                                if (refValue.IsContentConstriantReference && refValue.ContentConstraintReference != null)
                                {
                                    _jsonGenerator.WritePropertyName("constraintContent");
                                    _jsonGenerator.WriteValue(refValue.ContentConstraintReference.FullId);
                                }
                                if (refValue.DatakeyReference && refValue.DataKeys != null)
                                {
                                    _jsonGenerator.WritePropertyName("dataKey");
                                    _jsonGenerator.WriteStartObject();
                                    _jsonGenerator.WritePropertyName("include");
                                    _jsonGenerator.WriteValue("true");
                                    _jsonGenerator.WriteArrayFieldStart("keyValues");
                                    foreach (var item in refValue.DataKeys)
                                    {
                                        _jsonGenerator.WriteStartObject();
                                        _jsonGenerator.WritePropertyName("id");
                                        _jsonGenerator.WriteValue(item.Id);
                                        _jsonGenerator.WritePropertyName("include");
                                        _jsonGenerator.WriteValue(item.Included);
                                        _jsonGenerator.WritePropertyName("value");
                                        _jsonGenerator.WriteValue(item.KeyValue);
                                        _jsonGenerator.WriteEndArray();
                                    }
                                    _jsonGenerator.WriteEndArray();
                                    _jsonGenerator.WriteEndObject();
                                }
                                if (refValue.DatasetReference && refValue.DatasetId != null)
                                {
                                    if (!string.IsNullOrWhiteSpace(stringDataProvider))
                                    {
                                        _jsonGenerator.WritePropertyName("dataProvider");
                                        _jsonGenerator.WriteValue(stringDataProvider);
                                    }
                                    _jsonGenerator.WritePropertyName("id");
                                    _jsonGenerator.WriteValue(refValue.DatasetId);
                                }
                                if (refValue.IsIdentifiableReference && refValue.IdentifiableReference != null)
                                {
                                    _jsonGenerator.WritePropertyName("object");
                                    _jsonGenerator.WriteValue(refValue.IdentifiableReference.TargetUrn);
                                }
                                if (refValue.ReportPeriod != null)
                                {
                                    _jsonGenerator.WritePropertyName("reportPeriod");
                                    _jsonGenerator.WriteValue(refValue.ReportPeriod.DateInSdmxFormat);
                                }
                                _jsonGenerator.WriteEndObject();
                            }
                        }
                        _jsonGenerator.WriteEndArray();
                        _jsonGenerator.WriteEndObject();
                    }
                    if (report.ReportedAttributes != null)
                    {
                        _jsonGenerator.WritePropertyName("attributeSet");
                        _jsonGenerator.WriteStartObject();
                        _jsonGenerator.WriteArrayFieldStart("reportedAttributes");
                        foreach (var attributeSet in report.ReportedAttributes)
                        {
                            _jsonGenerator.WriteStartObject();
                            _jsonGenerator.WritePropertyName("id");
                            _jsonGenerator.WriteValue(attributeSet.Id);
                            
                            CreateAnnotations(attributeSet.Annotations);

                            if (attributeSet.HasSimpleValue())
                            {
                                _jsonGenerator.WritePropertyName("value");
                                _jsonGenerator.WriteValue(attributeSet.SimpleValue);
                            }
                            if (attributeSet.MetadataText != null && attributeSet.Presentational)
                            {
                                _jsonGenerator.WritePropertyName("structuredTexts");
                                _jsonGenerator.WriteStartObject();
                                foreach (var item in attributeSet.MetadataText)
                                {
                                    _jsonGenerator.WritePropertyName(item.Locale);
                                    _jsonGenerator.WriteValue(item.Value);
                                }
                                _jsonGenerator.WriteEndObject();
                            }
                            _jsonGenerator.WriteEndObject();
                        }
                        _jsonGenerator.WriteEndArray();
                        _jsonGenerator.WriteEndObject();
                    }
                    _jsonGenerator.WriteEndObject();
                }
                _jsonGenerator.WriteEndArray();
            }

        }

        private void CreateAnnotations(IList<IAnnotation> annotations)
        {
            _jsonGenerator.WriteArrayFieldStart("annotations");
            
            foreach (var itemAnnotation in annotations)
            {
                _jsonGenerator.WriteStartObject();
                _jsonGenerator.WritePropertyName("id");
                _jsonGenerator.WriteValue(itemAnnotation.Id);
                _jsonGenerator.WritePropertyName("type");
                _jsonGenerator.WriteValue(itemAnnotation.Type);

                _jsonGenerator.WritePropertyName("texts");
                _jsonGenerator.WriteStartObject();
                foreach (var item in itemAnnotation.Text)
                {
                    _jsonGenerator.WritePropertyName(item.Locale);
                    _jsonGenerator.WriteValue(item.Value);
                }
                _jsonGenerator.WriteEndObject();

                _jsonGenerator.WriteEndObject();
            }
            
            _jsonGenerator.WriteEndArray();
        }
        
    }
}
