﻿namespace Org.Sdmxsource.Sdmx.DataParser.Engine
{
    using Api.Constants.InterfaceConstant;
    using Api.Engine;

    /// <inheritdoc />
    public class NoGregorianPeriodDataWriterEngine : ReportingPeriodDataWriterEngineBase
    {
        /// <inheritdoc />
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Org.Sdmxsource.Sdmx.DataParser.Engine.NoGregorianPeriodDataWriterEngine" /> class.
        /// </summary>
        /// <param name="decoratedEngine">The decorated engine.</param>
        public NoGregorianPeriodDataWriterEngine(IDataWriterEngine decoratedEngine) : base(decoratedEngine)
        {
        }

        /// <summary>
        /// </summary>
        /// <param name="id"></param>
        /// <param name="valueRen"></param>
        /// <inheritdoc />
        public override void WriteAttributeValue(string id, string valueRen)
        {
            if (id != AttributeObject.Repyearstart)
            {
                this.DecoratedEngine.WriteAttributeValue(id, valueRen);
            }
        }
    }
}