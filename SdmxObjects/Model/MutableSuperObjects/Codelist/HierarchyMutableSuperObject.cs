// -----------------------------------------------------------------------
// <copyright file="HierarchyMutableSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.Codelist
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.MutableSuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.Base;

    /// <summary>
    ///     HierarchyMutableSuperObject class
    /// </summary>
    public class HierarchyMutableSuperObject : NameableMutableSuperObject, IHierarchyMutableSuperObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="HierarchyMutableSuperObject" /> class.
        /// </summary>
        public HierarchyMutableSuperObject()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="HierarchyMutableSuperObject" /> class.
        /// </summary>
        /// <param name="hsuperBean">The h super bean.</param>
        public HierarchyMutableSuperObject(IHierarchySuperObject<IHierarchicalCodelistSuperObject> hsuperBean)
            : base(hsuperBean)
        {
            this.CodeRefs = new List<ICodeRefMutableSuperObject>();
            foreach (var hierarchicalCodeSuperObject in hsuperBean.Codes)
            {
                this.CodeRefs.Add(new CodeRefMutableSuperObject(hierarchicalCodeSuperObject));
            }

            if (hsuperBean.Level != null)
            {
                this.ChildLevel = new LevelMutableCore(hsuperBean.Level);
            }

            this.FormalLevels = hsuperBean.HasFormalLevels();
        }

        /// <summary>
        ///     Gets or sets the child level.
        /// </summary>
        public ILevelMutableObject ChildLevel { get; set; }

        /// <summary>
        ///     Gets the code refs.
        /// </summary>
        public IList<ICodeRefMutableSuperObject> CodeRefs { get; private set; }

        /// <summary>
        ///     Gets or sets a value indicating whether the formal levels are set.
        /// </summary>
        public bool FormalLevels { get; set; }

        /// <summary>
        ///     The has formal levels.
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public bool HasFormalLevels()
        {
            return this.FormalLevels;
        }
    }
}