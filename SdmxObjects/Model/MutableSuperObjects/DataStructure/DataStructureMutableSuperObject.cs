// -----------------------------------------------------------------------
// <copyright file="DataStructureMutableSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-16
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.DataStructure
{
    using System.Collections.Generic;
    using System.Linq;
    using Org.Sdmxsource.Sdmx.Api.Model.MutableSuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.Base;

    /// <summary>
    ///     DataStructureMutableSuperObject class
    /// </summary>
    public class DataStructureMutableSuperObject : MaintainableMutableSuperObject, IDataStructureMutableSuperObject
    {
        private readonly List<IMeasureMutableSuperObject> _measures = new List<IMeasureMutableSuperObject>();

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataStructureMutableSuperObject" /> class.
        /// </summary>
        /// <param name="keyFamily">The key family.</param>
        public DataStructureMutableSuperObject(IDataStructureSuperObject keyFamily)
            : base(keyFamily)
        {
            if (keyFamily.Dimensions != null)
            {
                this.Dimensions = new List<IDimensionMutableSuperObject>();
                foreach (var dimensionSuperObject in keyFamily.Dimensions)
                {
                    this.Dimensions.Add(new DimensionMutableSuperObject(dimensionSuperObject));
                }
            }

            if (keyFamily.Attributes != null)
            {
                this.Attributes = new List<IAttributeMutableSuperObject>();
                foreach (var attributeSuperObject in keyFamily.Attributes)
                {
                    this.Attributes.Add(new AttributeMutableSuperObject(attributeSuperObject));
                }
            }

            if (keyFamily.Groups != null)
            {
                this.Groups = new List<IGroupMutableSuperObject>();
                foreach (var group in keyFamily.Groups)
                {
                    this.Groups.Add(new GroupMutableSuperObject(group));
                }
            }

            if (keyFamily.Measures != null)
            {
                this._measures.AddRange(keyFamily.Measures.Select(m => new MeasureMutableSuperObject(m)));
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataStructureMutableSuperObject" /> class.
        /// </summary>
        public DataStructureMutableSuperObject()
        {
        }

        /// <summary>
        ///     Gets the attributes.
        /// </summary>
        public IList<IAttributeMutableSuperObject> Attributes { get; private set; }

        /// <summary>
        ///     Gets the dimensions.
        /// </summary>
        public IList<IDimensionMutableSuperObject> Dimensions { get; private set; }

        /// <summary>
        ///     Gets the groups.
        /// </summary>
        public IList<IGroupMutableSuperObject> Groups { get; private set; }

        /// <summary>
        ///     Gets or sets the primary measure.
        /// </summary>
        public IMeasureMutableSuperObject PrimaryMeasure { get; set; }

        /// <summary>
        /// Gets the SDMX measures
        /// Introduced in SDMX v3.0.0 schema
        /// </summary>
        public IList<IMeasureMutableSuperObject> Measures { get; set; }
    }
}