// -----------------------------------------------------------------------
// <copyright file="AttributeMutableSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-16
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.DataStructure
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.MutableSuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.Base;

    /// <summary>
    ///     AttributeMutableSuperObject class
    /// </summary>
    public class AttributeMutableSuperObject : ComponentMutableSuperObject, IAttributeMutableSuperObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="AttributeMutableSuperObject" /> class.
        /// </summary>
        /// <param name="attributeBean">The attribute bean.</param>
        public AttributeMutableSuperObject(IAttributeSuperObject attributeBean)
            : base(attributeBean)
        {
            this.AttachmentMeasure = new List<string>();

            // TODO Implement
            this.AssignmentStatus = attributeBean.AssignmentStatus;
            this.AttachmantLevel = attributeBean.AttachmentLevel;
            this.IsMandatory = attributeBean.Mandatory;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AttributeMutableSuperObject" /> class.
        /// </summary>
        public AttributeMutableSuperObject()
        {
            this.AttachmentMeasure = new List<string>();
        }

        /// <summary>
        ///     Gets or sets the assignment status.
        /// </summary>
        /// <value>
        ///     The assignment status.
        /// </value>
        public string AssignmentStatus { get; set; }

        /// <summary>
        ///     Gets or sets the attachmant level.
        /// </summary>
        /// <value>
        ///     The attachmant level.
        /// </value>
        public AttributeAttachmentLevel AttachmantLevel { get; set; }

        /// <summary>
        ///     Gets or sets the attachment group.
        /// </summary>
        /// <value>
        ///     The attachment group.
        /// </value>
        public string AttachmentGroup { get; set; }

        /// <summary>
        ///     Gets the attachment measure.
        /// </summary>
        /// <value>
        ///     The attachment measure.
        /// </value>
        public IList<string> AttachmentMeasure { get; private set; }

        /// <summary>
        ///     Gets or sets a value indicating whether [count attribute].
        /// </summary>
        /// <value>
        ///     <c>true</c> if [count attribute]; otherwise, <c>false</c>.
        /// </value>
        public bool CountAttribute { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether [entity attribute].
        /// </summary>
        /// <value>
        ///     <c>true</c> if [entity attribute]; otherwise, <c>false</c>.
        /// </value>
        public bool EntityAttribute { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether [frequency attribute].
        /// </summary>
        /// <value>
        ///     <c>true</c> if [frequency attribute]; otherwise, <c>false</c>.
        /// </value>
        public bool FrequencyAttribute { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether [identity attribute].
        /// </summary>
        /// <value>
        ///     <c>true</c> if [identity attribute]; otherwise, <c>false</c>.
        /// </value>
        public bool IdentityAttribute { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether this instance is mandatory.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is mandatory; otherwise, <c>false</c>.
        /// </value>
        public bool IsMandatory { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether this <see cref="AttributeMutableSuperObject" /> is mandatory.
        /// </summary>
        /// <value>
        ///     <c>true</c> if mandatory; otherwise, <c>false</c>.
        /// </value>
        public bool Mandatory { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether [non observation time attribute].
        /// </summary>
        /// <value>
        ///     <c>true</c> if [non observation time attribute]; otherwise, <c>false</c>.
        /// </value>
        public bool NonObservationTimeAttribute { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether [time format].
        /// </summary>
        /// <value>
        ///     <c>true</c> if [time format]; otherwise, <c>false</c>.
        /// </value>
        public bool TimeFormat { get; set; }
    }
}