// -----------------------------------------------------------------------
// <copyright file="DimensionMutableSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-24
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.DataStructure
{
    using Org.Sdmxsource.Sdmx.Api.Model.MutableSuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.Base;

    /// <summary>
    ///     DimensionMutableSuperObject class
    /// </summary>
    public class DimensionMutableSuperObject : ComponentMutableSuperObject, IDimensionMutableSuperObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="DimensionMutableSuperObject" /> class.
        /// </summary>
        /// <param name="dimension">The dimension.</param>
        public DimensionMutableSuperObject(IDimensionSuperObject dimension)
            : base(dimension)
        {
            this.FrequencyDimension = dimension.FrequencyDimension;
            this.MeasureDimension = dimension.MeasureDimension;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DimensionMutableSuperObject" /> class.
        /// </summary>
        public DimensionMutableSuperObject()
        {
        }

        /// <summary>
        ///     Gets or sets a value indicating whether [count dimension].
        /// </summary>
        /// <value>
        ///     <c>true</c> if [count dimension]; otherwise, <c>false</c>.
        /// </value>
        public bool CountDimension { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether [entity dimension].
        /// </summary>
        /// <value>
        ///     <c>true</c> if [entity dimension]; otherwise, <c>false</c>.
        /// </value>
        public bool EntityDimension { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether [frequency dimension].
        /// </summary>
        /// <value>
        ///     <c>true</c> if [frequency dimension]; otherwise, <c>false</c>.
        /// </value>
        public bool FrequencyDimension { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether [identity dimension].
        /// </summary>
        /// <value>
        ///     <c>true</c> if [identity dimension]; otherwise, <c>false</c>.
        /// </value>
        public bool IdentityDimension { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether [measure dimension].
        /// </summary>
        /// <value>
        ///     <c>true</c> if [measure dimension]; otherwise, <c>false</c>.
        /// </value>
        public bool MeasureDimension { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether [non observation time dimension].
        /// </summary>
        /// <value>
        ///     <c>true</c> if [non observation time dimension]; otherwise, <c>false</c>.
        /// </value>
        public bool NonObservationTimeDimension { get; set; }
    }
}