// -----------------------------------------------------------------------
// <copyright file="ConceptSchemeMutableSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.ConceptScheme
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.MutableSuperObjects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.Base;

    /// <summary>
    ///     ConceptSchemeMutableSuperObject class
    /// </summary>
    public class ConceptSchemeMutableSuperObject : MaintainableMutableSuperObject, IConceptSchemeMutableSuperObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ConceptSchemeMutableSuperObject" /> class.
        /// </summary>
        /// <param name="conceptSchemeType">Type of the concept scheme.</param>
        public ConceptSchemeMutableSuperObject(IConceptSchemeSuperObject conceptSchemeType)
            : base(conceptSchemeType)
        {
            if (conceptSchemeType.Concepts != null)
            {
                this.Concepts = new HashSet<IConceptMutableSuperObject>();
                foreach (var conceptMutableSuperObject in conceptSchemeType.Concepts)
                {
                    this.Concepts.Add(new ConceptMutableSuperObject(conceptMutableSuperObject));
                }
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConceptSchemeMutableSuperObject" /> class.
        /// </summary>
        public ConceptSchemeMutableSuperObject()
        {
        }

        /// <summary>
        ///     Gets the concepts.
        /// </summary>
        public ISet<IConceptMutableSuperObject> Concepts { get; private set; }
    }
}