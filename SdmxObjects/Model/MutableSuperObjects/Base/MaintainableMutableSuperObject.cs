// -----------------------------------------------------------------------
// <copyright file="MaintainableMutableSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.Base
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Model.MutableSuperObjects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;
    using Org.Sdmxsource.Util.Attributes;

    /// <summary>
    ///     MaintainableMutableSuperObject class
    /// </summary>
    public abstract class MaintainableMutableSuperObject : NameableMutableSuperObject, IMaintainableMutableSuperObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="MaintainableMutableSuperObject" /> class.
        /// </summary>
        /// <param name="maintainable">The maintainable.</param>
        /// <exception cref="ArgumentNullException"><paramref name="maintainable"/> is <see langword="null" />.</exception>
        protected MaintainableMutableSuperObject(IMaintainableSuperObject maintainable)
            : base(maintainable)
        {
            if (maintainable == null)
            {
                throw new ArgumentNullException("maintainable");
            }

            this.AgencyId = maintainable.AgencyId;
            this.Version = maintainable.Version;
            this.IsFinal = maintainable.Final;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MaintainableMutableSuperObject" /> class.
        /// </summary>
        /// <param name="maintainable">The maintainable.</param>
        protected MaintainableMutableSuperObject([ValidatedNotNull]IMaintainableObject maintainable)
            : base(maintainable)
        {
            this.AgencyId = maintainable.AgencyId;
            this.Version = maintainable.Version;
            this.IsFinal = maintainable.IsFinal.IsTrue;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MaintainableMutableSuperObject" /> class.
        /// </summary>
        protected MaintainableMutableSuperObject()
        {
        }

        /// <summary>
        ///     Gets or sets the agency id of the agency maintaining this object
        /// </summary>
        public string AgencyId { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether this maintainable is final
        /// </summary>
        public bool IsFinal { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether this instance is stub.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is stub; otherwise, <c>false</c>.
        /// </value>
        public bool IsStub { get; set; }

        /// <summary>
        ///     Gets or sets the version of this maintainable object
        /// </summary>
        public string Version { get; set; }
    }
}