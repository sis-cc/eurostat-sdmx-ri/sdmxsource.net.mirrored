// -----------------------------------------------------------------------
// <copyright file="IdentifiableMutableObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.Base
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.MutableSuperObjects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;
    using Org.Sdmxsource.Util.Attributes;

    /// <summary>
    ///     The IdentifiableMutableObject class
    /// </summary>
    public class IdentifiableMutableObject : AnnotableMutableSuperObject, IIdentifiableMutableSuperObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="IdentifiableMutableObject" /> class.
        /// </summary>
        /// <param name="identifiable">The identifiable.</param>
        /// <exception cref="ArgumentNullException"><paramref name="identifiable"/> is <see langword="null" />.</exception>
        public IdentifiableMutableObject(IIdentifiableSuperObject identifiable)
            : base(identifiable)
        {
            if (identifiable == null)
            {
                throw new ArgumentNullException("identifiable");
            }

            this.Id = identifiable.Id;
            this.Urn = identifiable.Urn;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="IdentifiableMutableObject" /> class.
        /// </summary>
        /// <param name="identifiable">The identifiable.</param>
        /// <exception cref="ArgumentNullException"><paramref name="identifiable"/> is <see langword="null" />.</exception>
        public IdentifiableMutableObject([ValidatedNotNull]IIdentifiableObject identifiable)
            : base(identifiable)
        {
            if (identifiable == null)
            {
                throw new ArgumentNullException("identifiable");
            }

            this.Id = identifiable.Id;
            this.Urn = identifiable.Urn;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="IdentifiableMutableObject" /> class.
        /// </summary>
        public IdentifiableMutableObject()
        {
        }

        /// <summary>
        ///     Gets or sets the Id of the Identifiable Object
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        ///     Gets or sets the URN of the Identifiable Object
        /// </summary>
        public Uri Urn { get; set; }

        /// <summary>
        ///     Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///     <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(obj, this))
            {
                return true;
            }

            var o = obj as IdentifiableMutableObject;
            if (o != null)
            {
                var that = o;
                if (this.Urn == null || that.Urn == null)
                {
                    return false;
                }

                return this.Urn.Equals(that.Urn);
            }

            return false;
        }

        /// <summary>
        ///     Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        ///     A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        public override int GetHashCode()
        {
            if (this.Urn == null)
            {
                return base.GetHashCode();
            }

            return this.Urn.GetHashCode();
        }
    }
}