// -----------------------------------------------------------------------
// <copyright file="ComponentMutableSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.Base
{
    using Org.Sdmxsource.Sdmx.Api.Model.MutableSuperObjects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.MutableSuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.MutableSuperObjects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.ConceptScheme;

    /// <summary>
    ///     The ComponentMutableSuperObject class
    /// </summary>
    public abstract class ComponentMutableSuperObject : IdentifiableMutableObject, IComponentMutableSuperObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ComponentMutableSuperObject" /> class.
        /// </summary>
        /// <param name="componentBean">The component bean.</param>
        protected ComponentMutableSuperObject(IComponentSuperObject componentBean)
            : base(componentBean)
        {
            if (componentBean.GetCodelist(false) != null)
            {
                this.CodelistObject = new CodelistMutableSuperObject(componentBean.GetCodelist(false));
            }

            if (componentBean.Concept != null)
            {
                this.ConceptSuperObject = new ConceptMutableSuperObject(componentBean.Concept);
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComponentMutableSuperObject" /> class.
        /// </summary>
        protected ComponentMutableSuperObject()
        {
        }

        /// <summary>
        ///     Gets or sets the codelist object.
        /// </summary>
        public ICodelistMutableSuperObject CodelistObject { get; set; }

        /// <summary>
        ///     Gets or sets the concept object base.
        /// </summary>
        public IConceptMutableSuperObject ConceptSuperObject { get; set; }
    }
}