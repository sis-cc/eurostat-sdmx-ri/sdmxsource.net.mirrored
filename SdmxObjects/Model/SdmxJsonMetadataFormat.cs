// -----------------------------------------------------------------------
// <copyright file="SdmxStructureSpecificMetadataFormat.cs" company="EUROSTAT">
//   Date Created : 2016-07-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Format;

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model
{
    public class SdmxJsonMetadataFormat : IMetadataFormat
    {
        private readonly XmlWriter _xmlWriter;

        /**
	     * Default constructor
	     */
        public SdmxJsonMetadataFormat()
        {
            SdmxFormat = SdmxSchema.GetFromEnum(SdmxSchemaEnumType.Json);
        }

        /**
         * Constructor to use when an XmlWriter is already available e.g. a SOAP message
         * @param xmlWriter
         */
        public SdmxJsonMetadataFormat(XmlWriter xmlWriter)
        {
            _xmlWriter = xmlWriter;
        }

        public SdmxSchema SdmxFormat { get; private set; }


        /**
         * Gets the XML Stream Writer, in case it is provided.
         * This can be used with SOAP
         * @return
         */
        public XmlWriter getXmlWriter()
        {
            return _xmlWriter;
        }
    }
}
