﻿// -----------------------------------------------------------------------
// <copyright file="MetadataReportSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-15
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Metadata
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Metadata;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Metadata;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    ///     MetadataReportSuperObject class
    /// </summary>
    public class MetadataReportSuperObject : SuperObject, IMetadataReportSuperObject
    {
        /// <summary>
        ///     The built from
        /// </summary>
        private readonly IMetadataReport _builtFrom;

        /// <summary>
        ///     The reported attributes
        /// </summary>
        private readonly IList<IReportedAttributeSuperObject> _reportedAttributes =
            new List<IReportedAttributeSuperObject>();

        /// <summary>
        ///     The target
        /// </summary>
        private readonly ITargetSuperObject _target;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataReportSuperObject" /> class.
        /// </summary>
        /// <param name="rs">The rs.</param>
        /// <param name="builtFrom">The built from.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <exception cref="ArgumentNullException"><paramref name="rs"/> is <see langword="null" />.</exception>
        public MetadataReportSuperObject(
            IReportStructure rs, 
            IMetadataReport builtFrom, 
            IIdentifiableRetrievalManager retrievalManager)
            : base(builtFrom)
        {
            if (rs == null)
            {
                throw new ArgumentNullException("rs");
            }

            this._builtFrom = builtFrom;
            this._target = new TargetSuperObject(builtFrom.Target, retrievalManager);
            foreach (IReportedAttributeObject currentRa in builtFrom.ReportedAttributes)
            {
                IMetadataAttributeObject mabean = this.GetMetadataAttributeForReportedAttribute(
                    currentRa, 
                    rs.MetadataAttributes);
                this._reportedAttributes.Add(new ReportedAttributeSuperObject(mabean, currentRa, retrievalManager));
            }
        }

        /// <summary>
        ///     Gets the built from.
        /// </summary>
        /// <value>
        ///     The built from.
        /// </value>
        public new IMetadataReport BuiltFrom
        {
            get
            {
                return this._builtFrom;
            }
        }

        /// <summary>
        ///     Gets the composite objects.
        /// </summary>
        /// <value>
        ///     The composite objects.
        /// </value>
        public new ISet<IMaintainableObject> CompositeObjects
        {
            get
            {
                ISet<IMaintainableObject> returnSet = base.CompositeObjects;
                if (this._target != null)
                {
                    foreach (IMaintainableObject co in this._target.CompositeObjects)
                    {
                        returnSet.Add(co);
                    }
                }

                if (this._reportedAttributes != null)
                {
                    foreach (IReportedAttributeSuperObject reportedAttributeSuperBean in this._reportedAttributes)
                    {
                        foreach (IMaintainableObject co in reportedAttributeSuperBean.CompositeObjects)
                        {
                            returnSet.Add(co);
                        }
                    }
                }

                return returnSet;
            }
        }

        /// <summary>
        ///     Gets the id.
        /// </summary>
        public string Id
        {
            get
            {
                return this._builtFrom.Id;
            }
        }

        /// <summary>
        ///     Gets the reported attributes.
        /// </summary>
        public IList<IReportedAttributeSuperObject> ReportedAttributes
        {
            get
            {
                return new List<IReportedAttributeSuperObject>(this._reportedAttributes);
            }
        }

        /// <summary>
        ///     Gets the target.
        /// </summary>
        public ITargetSuperObject Target
        {
            get
            {
                return this._target;
            }
        }

        /// <summary>
        ///     Gets the metadata attribute for reported attribute.
        /// </summary>
        /// <param name="reportedAttribute">The reported attribute.</param>
        /// <param name="attributeBeans">The m attribute beans.</param>
        /// <returns>The metadata attribute object</returns>
        /// <exception cref="SdmxReferenceException">SDMX reference exception</exception>
        private IMetadataAttributeObject GetMetadataAttributeForReportedAttribute(
            IReportedAttributeObject reportedAttribute, 
            IList<IMetadataAttributeObject> attributeBeans)
        {
            foreach (IMetadataAttributeObject currentMAttribute in attributeBeans)
            {
                if (currentMAttribute.Id.Equals(reportedAttribute.Id))
                {
                    return currentMAttribute;
                }
            }

            IMaintainableObject maint = reportedAttribute.GetParent<IMaintainableObject>(false);

            IList<string> ids = new List<string>();
            ids.Add(reportedAttribute.Id);
            IStructureReference sref = new StructureReferenceImpl(
                maint, 
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataAttribute), 
                ids);
            throw new SdmxReferenceException(maint, sref);
        }
    }
}