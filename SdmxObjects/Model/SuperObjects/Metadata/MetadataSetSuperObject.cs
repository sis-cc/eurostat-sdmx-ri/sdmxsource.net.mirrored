﻿// -----------------------------------------------------------------------
// <copyright file="MetadataSetSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-15
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Metadata
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Metadata;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Metadata;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base;

    /// <summary>
    ///     MetadataSetSuperObject class
    /// </summary>
    public class MetadataSetSuperObject : SuperObject, IMetadataSetSuperObject
    {
        /// <summary>
        ///     The built from
        /// </summary>
        private readonly IMetadataSet _builtFrom;

        /// <summary>
        ///     The data provider
        /// </summary>
        private readonly IDataProvider _dataProvider;

        /// <summary>
        ///     The metadata structure
        /// </summary>
        private readonly IMetadataStructureDefinitionObject _metadataStructure;

        /// <summary>
        ///     The reports
        /// </summary>
        private readonly List<IMetadataReportSuperObject> _reports = new List<IMetadataReportSuperObject>();

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataSetSuperObject" /> class.
        /// </summary>
        /// <param name="builtFrom">The built from.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <exception cref="ArgumentNullException"><paramref name="retrievalManager"/> is <see langword="null" />.</exception>
        public MetadataSetSuperObject(IMetadataSet builtFrom, IIdentifiableRetrievalManager retrievalManager)
            : base(builtFrom)
        {
            if (retrievalManager == null)
            {
                throw new ArgumentNullException("retrievalManager");
            }

            this._builtFrom = builtFrom;
            this._metadataStructure =
                retrievalManager.GetIdentifiableObject<IMetadataStructureDefinitionObject>(builtFrom.MsdReference);
            if (builtFrom.DataProviderReference != null)
            {
                this._dataProvider = retrievalManager.GetIdentifiableObject<IDataProvider>(builtFrom.DataProviderReference);
            }

            if (builtFrom.Reports != null)
            {
                foreach (IMetadataReport currentMr in builtFrom.Reports)
                {
                    IReportStructure rs = this.GetReportStructure(currentMr, this._metadataStructure.ReportStructures);
                    this._reports.Add(new MetadataReportSuperObject(rs, currentMr, retrievalManager));
                }
            }
        }

        /// <summary>
        ///     Gets the built from.
        /// </summary>
        /// <value>
        ///     The built from.
        /// </value>
        public new IMetadataSet BuiltFrom
        {
            get
            {
                return this._builtFrom;
            }
        }

        /// <summary>
        ///     Gets the composite objects.
        /// </summary>
        /// <value>
        ///     The composite objects.
        /// </value>
        public new ISet<IMaintainableObject> CompositeObjects
        {
            get
            {
                ISet<IMaintainableObject> returnSet = base.CompositeObjects;

                returnSet.Add(this._metadataStructure);

                if (this._dataProvider != null)
                {
                    returnSet.Add(this._dataProvider.MaintainableParent);
                }

                if (this._reports != null)
                {
                    foreach (IMetadataReportSuperObject report in this._reports)
                    {
                        foreach (IMaintainableObject co in report.CompositeObjects)
                        {
                            returnSet.Add(co);
                        }
                    }
                }

                return returnSet;
            }
        }

        /// <summary>
        ///     Gets DataProvider
        /// </summary>
        public IDataProvider DataProvider
        {
            get
            {
                return this._dataProvider;
            }
        }

        /// <summary>
        ///     Gets  the metadata structure that defines the structure of this metadata set.
        ///     Mandatory
        /// </summary>
        public IMetadataStructureDefinitionObject MetadataStructure
        {
            get
            {
                return this._metadataStructure;
            }
        }

        /// <summary>
        ///     Gets the publication period
        /// </summary>
        public object PublicationPeriod
        {
            get
            {
                return this._builtFrom.PublicationPeriod;
            }
        }

        /// <summary>
        ///     Gets the four digit ISo 8601 year of publication
        /// </summary>
        public ISdmxDate PublicationYear
        {
            get
            {
                return this._builtFrom.PublicationYear;
            }
        }

        /// <summary>
        ///     Gets  the inclusive start time of the data reported in the metadata set
        /// </summary>
        public ISdmxDate ReportingBeginDate
        {
            get
            {
                return this._builtFrom.ReportingBeginDate;
            }
        }

        /// <summary>
        ///     Gets the inclusive end time of the data reported in the metadata set
        /// </summary>
        public ISdmxDate ReportingEndDate
        {
            get
            {
                return this._builtFrom.ReportingEndDate;
            }
        }

        /// <summary>
        ///     Gets the reports.
        /// </summary>
        public IList<IMetadataReportSuperObject> Reports
        {
            get
            {
                return new List<IMetadataReportSuperObject>(this._reports);
            }
        }

        /// <summary>
        ///     Gets the inclusive start time of the validity of the info in the metadata set
        /// </summary>
        public ISdmxDate ValidFromDate
        {
            get
            {
                return this._builtFrom.ValidFromDate;
            }
        }

        /// <summary>
        ///     Gets the inclusive end time of the validity of the info in the metadata set
        /// </summary>
        public ISdmxDate ValidToDate
        {
            get
            {
                return this._builtFrom.ValidToDate;
            }
        }

        /// <summary>
        ///     Gets the report structure.
        /// </summary>
        /// <param name="currentMr">The current report.</param>
        /// <param name="reportStructures">The report structures.</param>
        /// <returns>The report structure</returns>
        /// <exception cref="SdmxSemmanticException">Can not find reference to report structure with id  + currentMR.id</exception>
        private IReportStructure GetReportStructure(IMetadataReport currentMr, IList<IReportStructure> reportStructures)
        {
            foreach (IReportStructure currentRs in reportStructures)
            {
                if (currentRs.Id.Equals(currentMr.Id))
                {
                    return currentRs;
                }
            }

            throw new SdmxSemmanticException("Can not find reference to report structure with id " + currentMr.Id);
        }
    }
}