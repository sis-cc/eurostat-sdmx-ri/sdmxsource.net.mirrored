// -----------------------------------------------------------------------
// <copyright file="HierarchicalCodelistSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Codelist
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     HierarchicalCodelistSuperObject class
    /// </summary>
    public class HierarchicalCodelistSuperObject : MaintainableSuperObject, IHierarchicalCodelistSuperObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="HierarchicalCodelistSuperObject" /> class.
        /// </summary>
        /// <param name="hcl">The HCL.</param>
        /// <param name="referencedCodelists">The referenced codelists.</param>
        public HierarchicalCodelistSuperObject(
            IHierarchicalCodelistObject hcl, 
            IList<ICodelistObject> referencedCodelists)
            : base(hcl)
        {
            this.Hierarchies = new HashSet<IHierarchySuperObject<IHierarchicalCodelistSuperObject>>();
            foreach (var hierarchy in hcl.Hierarchies)
            {
                this.Hierarchies.Add(new HierarchySuperObject(hierarchy, this, referencedCodelists));
            }
        }

        /// <summary>
        ///     Gets the composite objects that were used to build this object base.
        /// </summary>
        /// <value>
        ///     The composite objects.
        /// </value>
        public override ISet<IMaintainableObject> CompositeObjects
        {
            get
            {
                var returnSet = base.CompositeObjects;
                foreach (var hierarchySuperObject in this.Hierarchies)
                {
                    returnSet.AddAll(hierarchySuperObject.CompositeObjects);
                }

                return returnSet;
            }
        }

        /// <summary>
        ///     Gets the set of hierarchies that this hierarchical codelist
        ///     has reference to
        /// </summary>
        public ISet<IHierarchySuperObject<IHierarchicalCodelistSuperObject>> Hierarchies { get; private set; }
    }
}