// -----------------------------------------------------------------------
// <copyright file="CategorySchemeSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.CategoryScheme
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base;

    /// <summary>
    ///     CategorySchemeSuperObject class
    /// </summary>
    public class CategorySchemeSuperObject : MaintainableSuperObject, ICategorySchemeSuperObject
    {
        /// <summary>
        ///     The built from
        /// </summary>
        private readonly ICategorySchemeObject _builtFrom;

        /// <summary>
        ///     Initializes a new instance of the <see cref="CategorySchemeSuperObject" /> class.
        /// </summary>
        /// <param name="categoryScheme">The category scheme.</param>
        public CategorySchemeSuperObject(ICategorySchemeObject categoryScheme)
            : base(categoryScheme)
        {
            this._builtFrom = categoryScheme;
            this.Categories = new List<ICategorySuperObject>();
            if (categoryScheme.Items != null)
            {
                foreach (var categoryObject in categoryScheme.Items)
                {
                    var category = new CategorySuperObject(this, categoryObject, null);
                    this.Categories.Add(category);
                }
            }
        }

        /// <summary>
        ///     Gets the built from.
        /// </summary>
        /// <value>
        ///     The built from.
        /// </value>
        public new ICategorySchemeObject BuiltFrom
        {
            get
            {
                return this._builtFrom;
            }
        }

        /// <summary>
        ///     Gets a list of all the categories contained within this scheme
        /// </summary>
        /// <value>
        ///     list of categories
        /// </value>
        public IList<ICategorySuperObject> Categories { get; private set; }

        /// <summary>
        ///     Gets the composite objects that were used to build this object base.
        /// </summary>
        /// <value>
        ///     The composite objects.
        /// </value>
        public override ISet<IMaintainableObject> CompositeObjects
        {
            get
            {
                ISet<IMaintainableObject> returnSet = base.CompositeObjects;
                returnSet.Add(this._builtFrom);
                return returnSet;
            }
        }
    }
}