// -----------------------------------------------------------------------
// <copyright file="CategorisationSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.CategoryScheme
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base;

    /// <summary>
    ///     CategorisationSuperObject class
    /// </summary>
    public class CategorisationSuperObject : MaintainableSuperObject, ICategorisationSuperObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="CategorisationSuperObject" /> class.
        /// </summary>
        /// <param name="categorisation">The categorisation.</param>
        /// <param name="retMan">The ret man.</param>
        /// <exception cref="ArgumentNullException"><paramref name="retMan"/> is <see langword="null" />.</exception>
        public CategorisationSuperObject(ICategorisationObject categorisation, IIdentifiableRetrievalManager retMan)
            : base(categorisation)
        {
            if (retMan == null)
            {
                throw new ArgumentNullException("retMan");
            }

            this.BuiltFrom = categorisation;
            this.Category = retMan.GetIdentifiableObject<ICategoryObject>(categorisation.CategoryReference);
            this.Structure = retMan.GetIdentifiableObject(categorisation.StructureReference);

            if (this.Structure == null)
            {
                throw new CrossReferenceException(categorisation.StructureReference);
            }

            if (this.Category == null)
            {
                throw new CrossReferenceException(categorisation.CategoryReference);
            }
        }

        /// <summary>
        ///     Gets the built from.
        /// </summary>
        /// <value>
        ///     The built from.
        /// </value>
        public new ICategorisationObject BuiltFrom { get; private set; }

        /// <summary>
        ///     Gets the category that is categorising the structure - this can not be null
        /// </summary>
        public ICategoryObject Category { get; private set; }

        /// <summary>
        ///     Gets the composite objects that were used to build this object base.
        /// </summary>
        /// <value>
        ///     The composite objects.
        /// </value>
        public new ISet<IMaintainableObject> CompositeObjects
        {
            get
            {
                var returnSet = base.CompositeObjects;
                returnSet.Add(this.BuiltFrom);
                returnSet.Add(this.Structure.MaintainableParent);
                returnSet.Add(this.Category.MaintainableParent);
                return returnSet;
            }
        }

        /// <summary>
        ///     Gets a the structure that this is categorising - this can not be null
        /// </summary>
        public IIdentifiableObject Structure { get; private set; }
    }
}