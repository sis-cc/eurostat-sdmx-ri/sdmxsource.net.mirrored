// -----------------------------------------------------------------------
// <copyright file="NameableSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     NameableSuperObject class
    /// </summary>
    public abstract class NameableSuperObject : IdentifiableSuperObject, INameableSuperObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="NameableSuperObject" /> class.
        /// </summary>
        /// <param name="identifiable">The identifiable.</param>
        protected NameableSuperObject(INameableObject identifiable)
            : base(identifiable)
        {
            if (identifiable == null)
            {
                throw new ArgumentNullException("identifiable");
            }

            this.Names = LocaleUtil.BuildLocalMap(identifiable.Names);
            this.Descriptions = LocaleUtil.BuildLocalMap(identifiable.Descriptions);
        }

        /// <summary>
        ///     Gets the Description of the Identifiable object for a the default Locale
        /// </summary>
        public string Description
        {
            get
            {
                if (this.Descriptions == null || this.Descriptions.Count == 0)
                {
                    return null;
                }

                var returnStr = LocaleUtil.GetStringByDefaultLocale(this.Descriptions);
                if (returnStr == null && this.Descriptions.Count > 0)
                {
                    return this.Descriptions.Values.ElementAt(0);
                }

                return returnStr;
            }
        }

        /// <summary>
        ///     Gets the descriptions.
        /// </summary>
        public IDictionary<CultureInfo, string> Descriptions { get; private set; }

        /// <summary>
        ///     Gets the Name of the Identifiable object for a the default Locale
        /// </summary>
        public string Name
        {
            get
            {
                if (this.Names == null || this.Names.Count == 0)
                {
                    return null;
                }

                var returnStr = LocaleUtil.GetStringByDefaultLocale(this.Names);
                if (returnStr == null)
                {
                    return this.Names.Values.ElementAt(0);
                }

                return returnStr;
            }
        }

        /// <summary>
        ///     Gets the names.
        /// </summary>
        public IDictionary<CultureInfo, string> Names { get; private set; }

        /// <summary>
        ///     Gets the description of the Identifiable Object for the given Locale
        /// </summary>
        /// <param name="loc">The locale</param>
        /// <returns>
        ///     The <see cref="string" /> .
        /// </returns>
        public string GetDescription(CultureInfo loc)
        {
            string returnStr;
            if (!this.Descriptions.TryGetValue(loc, out returnStr) && this.Descriptions.Count > 0)
            {
                return this.Descriptions.Values.ElementAt(0);
            }

            return returnStr;
        }

        /// <summary>
        ///     Gets the Name of the Identifiable object for a given Locale
        /// </summary>
        /// <param name="loc">The locale</param>
        /// <returns>
        ///     The <see cref="string" /> .
        /// </returns>
        public string GetName(CultureInfo loc)
        {
            return this.Names.GetOrDefault(loc);
        }
    }
}