// -----------------------------------------------------------------------
// <copyright file="AnnotationSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects;

    /// <summary>
    ///     AnnotationSuperObject class
    /// </summary>
    public class AnnotationSuperObject : IAnnotationSuperObject
    {
        /// <summary>
        ///     The url.
        /// </summary>
        private Uri _uri;

        /// <summary>
        ///     The annotation urls
        /// </summary>
        private readonly IList<IAnnotationUrlSuperObject> _urls = new List<IAnnotationUrlSuperObject>();

        /// <summary>
        ///     Initializes a new instance of the <see cref="AnnotationSuperObject" /> class.
        /// </summary>
        /// <param name="annotation">The annotation.</param>
        /// <exception cref="ArgumentNullException"><paramref name="annotation"/> is <see langword="null" />.</exception>
        public AnnotationSuperObject(IAnnotation annotation)
        {
            if (annotation == null)
            {
                throw new ArgumentNullException("annotation");
            }

            this.Title = annotation.Title;
            this._uri = annotation.Uri;
            this.Type = annotation.Type;
            this.Texts = LocaleUtil.BuildLocalMap(annotation.Text);
            this.Value = annotation.Value;

            if (annotation.Urls != null)
            {
                foreach(var url in annotation.Urls)
                {
                    this._urls.Add(new AnnotationUrlSuperObject(url));
                }
            }
        }

        /// <summary>
        ///     Gets the text of the annotation in the default locale
        /// </summary>
        public string Text
        {
            get
            {
                return LocaleUtil.GetStringByDefaultLocale(this.Texts);
            }
        }

        /// <summary>
        ///     Gets the text of the annotation in the default locale
        /// </summary>
        public IDictionary<CultureInfo, string> Texts { get; private set; }

        /// <summary>
        ///     Gets the title of the annotation
        /// </summary>
        public string Title { get; private set; }

        /// <summary>
        ///     Gets the type of the annotation
        /// </summary>
        public string Type { get; private set; }

        /// <summary>
        ///     Gets the Uri of the annotation
        /// </summary>
        public Uri Url 
        {
            get
            {
                if (this._uri == null)
                {
                    return GetDefaultUri();
                }
                return this._uri;
            }
        }

        /// <summary>
        ///     Gets the text of the annotation in the given locale
        /// </summary>
        /// <param name="locale">The locale</param>
        /// <returns>
        ///     The <see cref="string" /> .
        /// </returns>
        public string GetText(CultureInfo locale)
        {
            return this.Title;
        }

        /// <inheritdoc/>
        public IList<IAnnotationUrlSuperObject> Urls 
        { 
            get
            {
                if (!_urls.Any() && _uri != null)
                {
                    return new List<IAnnotationUrlSuperObject>() { new AnnotationUrlSuperObject(_uri, null) }.AsReadOnly();
                }
                return _urls;
            }
        }

        /// <inheritdoc/>
        public string Value { get; private set; }

        /// <summary>
        /// Gets the default <see cref="Uri"/> from the list of <see cref="_urls"/>.
        /// </summary>
        /// <returns></returns>
        private Uri GetDefaultUri()
        {
            IEnumerable<string> defaultLocales = LocaleUtil.DefaultLocales.Select(l => l.ThreeLetterISOLanguageName);
            return  this._urls.FirstOrDefault(x => defaultLocales.Contains(x.Locale))?.Uri
                ?? this.Urls.FirstOrDefault()?.Uri;
        }
    }
}