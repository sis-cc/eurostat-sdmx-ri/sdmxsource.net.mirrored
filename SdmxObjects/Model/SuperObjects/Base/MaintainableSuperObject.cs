// -----------------------------------------------------------------------
// <copyright file="MaintainableSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;

    /// <summary>
    ///     MaintainableSuperObject class
    /// </summary>
    public abstract class MaintainableSuperObject : NameableSuperObject, IMaintainableSuperObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="MaintainableSuperObject" /> class.
        /// </summary>
        /// <param name="maintainable">The maintainable.</param>
        /// <exception cref="ArgumentNullException"><paramref name="maintainable"/> is <see langword="null" />.</exception>
        protected MaintainableSuperObject(IMaintainableObject maintainable)
            : base(maintainable)
        {
            if (maintainable == null)
            {
                throw new ArgumentNullException("maintainable");
            }

            // TODO AgencyId should be full agency
            this.BuiltFrom = maintainable;
            this.AgencyId = maintainable.AgencyId;
            this.Version = maintainable.Version;
            this.Final = maintainable.IsFinal != null && maintainable.IsFinal.IsTrue;
        }

        /// <summary>
        ///     Gets the agency id of the agency maintaining this object
        /// </summary>
        public string AgencyId { get; private set; }

        /// <summary>
        ///     Gets the built from.
        /// </summary>
        /// <value>
        ///     The built from.
        /// </value>
        public new IMaintainableObject BuiltFrom { get; private set; }

        /// <summary>
        ///     Gets a value indicating whether the maintainable is final
        /// </summary>
        public bool Final { get; private set; }

        /// <summary>
        ///     Gets the version of this maintainable object
        /// </summary>
        public string Version { get; private set; }
    }
}