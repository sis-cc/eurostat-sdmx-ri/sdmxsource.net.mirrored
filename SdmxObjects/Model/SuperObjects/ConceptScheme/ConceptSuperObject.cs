﻿// -----------------------------------------------------------------------
// <copyright file="ConceptSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-15
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.ConceptScheme
{
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base;

    /// <summary>
    ///     ConceptSuperObject class
    /// </summary>
    public class ConceptSuperObject : NameableSuperObject, IConceptSuperObject
    {
        /// <summary>
        ///     The built from
        /// </summary>
        private readonly IConceptObject builtFrom;

        /// <summary>
        ///     The representation
        /// </summary>
        private readonly ICodelistSuperObject _representation;

        /// <summary>
        ///     The text format
        /// </summary>
        private readonly ITextFormat _textFormat;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConceptSuperObject" /> class.
        /// </summary>
        /// <param name="concept">The concept.</param>
        /// <param name="representation">The representation.</param>
        public ConceptSuperObject(IConceptObject concept, ICodelistSuperObject representation)
            : base(concept)
        {
            this._representation = representation;
            if (concept.CoreRepresentation != null)
            {
                this._textFormat = concept.CoreRepresentation.TextFormat;
            }

            this.builtFrom = concept;
        }

        /// <summary>
        ///     Gets the built from.
        /// </summary>
        /// <value>
        ///     The built from.
        /// </value>
        public new IConceptObject BuiltFrom
        {
            get
            {
                return this.builtFrom;
            }
        }

        /// <summary>
        ///     Gets the representation of this concept - this will return null if <see cref="HasRepresentation" /> is false
        /// </summary>
        public ICodelistSuperObject CoreRepresentation
        {
            get
            {
                return this._representation;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether this concept has core representation
        /// </summary>
        public bool HasRepresentation
        {
            get
            {
                return this._representation != null;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether the concept is a stand alone concept
        /// </summary>
        public bool StandAloneConcept
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        ///     Gets the text format.
        /// </summary>
        public ITextFormat TextFormat
        {
            get
            {
                return this._textFormat;
            }
        }
    }
}