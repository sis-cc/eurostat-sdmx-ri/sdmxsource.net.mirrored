// -----------------------------------------------------------------------
// <copyright file="RegistrationSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-16
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Registry
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     RegistrationSuperObject class
    /// </summary>
    public class RegistrationSuperObject : MaintainableSuperObject, IRegistrationSuperObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="RegistrationSuperObject" /> class.
        /// </summary>
        /// <param name="reigstrationbean">The registration object.</param>
        /// <param name="provBean">The prov bean.</param>
        public RegistrationSuperObject(IRegistrationObject reigstrationbean, IProvisionAgreementSuperObject provBean)
            : base(reigstrationbean)
        {
            this.ProvisionAgreement = provBean;
            this.BuiltFrom = reigstrationbean;
        }

        /// <summary>
        ///     Gets the built from.
        /// </summary>
        /// <value>
        ///     The built from.
        /// </value>
        public new IRegistrationObject BuiltFrom { get; private set; }

        /// <summary>
        ///     Gets the composite objects that were used to build this object base.
        /// </summary>
        /// <value>
        ///     The composite objects.
        /// </value>
        public override ISet<IMaintainableObject> CompositeObjects
        {
            get
            {
                var returnSet = base.CompositeObjects;
                returnSet.Add(this.BuiltFrom);
                returnSet.AddAll(this.ProvisionAgreement.CompositeObjects);
                return returnSet;
            }
        }

        /// <summary>
        ///     Gets the data source.
        /// </summary>
        public IDataSource DataSource
        {
            get
            {
                return this.BuiltFrom.DataSource;
            }
        }

        /// <summary>
        ///     Gets the index attributes.
        /// </summary>
        public TertiaryBool IndexAttribtues
        {
            get
            {
                return this.BuiltFrom.IndexAttribtues;
            }
        }

        /// <summary>
        ///     Gets the index dataset.
        /// </summary>
        public TertiaryBool IndexDataset
        {
            get
            {
                return this.BuiltFrom.IndexDataset;
            }
        }

        /// <summary>
        ///     Gets the index reporting period.
        /// </summary>
        public TertiaryBool IndexReportingPeriod
        {
            get
            {
                return this.BuiltFrom.IndexReportingPeriod;
            }
        }

        /// <summary>
        ///     Gets the index time series.
        /// </summary>
        public TertiaryBool IndexTimeSeries
        {
            get
            {
                return this.BuiltFrom.IndexTimeseries;
            }
        }

        /// <summary>
        ///     Gets the last updated.
        /// </summary>
        public ISdmxDate LastUpdated
        {
            get
            {
                return this.BuiltFrom.LastUpdated;
            }
        }

        /// <summary>
        ///     Gets the provision agreement that this registration is referencing
        /// </summary>
        public IProvisionAgreementSuperObject ProvisionAgreement { get; private set; }

        /// <summary>
        ///     Gets the valid from.
        /// </summary>
        public ISdmxDate ValidFrom
        {
            get
            {
                return this.BuiltFrom.ValidFrom;
            }
        }

        /// <summary>
        ///     Gets the valid to.
        /// </summary>
        public ISdmxDate ValidTo
        {
            get
            {
                return this.BuiltFrom.ValidTo;
            }
        }
    }
}