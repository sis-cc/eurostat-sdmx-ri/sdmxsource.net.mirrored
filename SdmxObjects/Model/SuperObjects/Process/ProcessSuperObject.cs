// -----------------------------------------------------------------------
// <copyright file="ProcessSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Process
{
    using System.Collections.Generic;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Process;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base;

    /// <summary>
    ///     ProcessSuperObject class
    /// </summary>
    public class ProcessSuperObject : MaintainableSuperObject, IProcessSuperObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ProcessSuperObject" /> class.
        /// </summary>
        /// <param name="process">The process.</param>
        /// <param name="processSteps">The process steps.</param>
        public ProcessSuperObject(IProcessObject process, IList<IProcessStepSuperObject> processSteps)
            : base(process)
        {
            this.Process = process;
            this.ProcessSteps = processSteps;
        }

        /// <summary>
        ///     Gets the composite objects that were used to build this object base.
        /// </summary>
        /// <value>
        ///     The composite objects.
        /// </value>
        public override ISet<IMaintainableObject> CompositeObjects
        {
            get
            {
                ISet<IMaintainableObject> returnSet = base.CompositeObjects;
                returnSet.Add(this.Process);
                this.AddToSet(returnSet, this.ProcessSteps.ToList());
                return returnSet;
            }
        }

        /// <summary>
        ///     Gets the process.
        /// </summary>
        public IProcessObject Process { get; private set; }

        /// <summary>
        ///     Gets the process steps.
        /// </summary>
        public IList<IProcessStepSuperObject> ProcessSteps { get; private set; }
    }
}