﻿// -----------------------------------------------------------------------
// <copyright file="GroupSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-15
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.DataStructure
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     GroupSuperObject class
    /// </summary>
    public class GroupSuperObject : IdentifiableSuperObject, IGroupSuperObject
    {
        /// <summary>
        ///     The attachment constraint reference
        /// </summary>
        private readonly ICrossReference _attachmentConstraintRef;

        /// <summary>
        ///     The dimension
        /// </summary>
        private readonly IList<IDimensionSuperObject> _dimension = new List<IDimensionSuperObject>();

        /// <summary>
        ///     The dimension map
        /// </summary>
        private readonly IDictionary<string, IDimensionSuperObject> _dimensionMap =
            new Dictionary<string, IDimensionSuperObject>();

        /// <summary>
        ///     The identifier
        /// </summary>
        private readonly string _id;

        /// <summary>
        ///     Initializes a new instance of the <see cref="GroupSuperObject" /> class.
        /// </summary>
        /// <param name="group">The group.</param>
        /// <param name="keyFamily">The key family.</param>
        /// <exception cref="ArgumentNullException"><paramref name="keyFamily"/> is <see langword="null" />.</exception>
        /// <exception cref="SdmxSemmanticException">Super Bean missing the required SDMXBean that it is built from</exception>
        public GroupSuperObject(IGroup group, IDataStructureSuperObject keyFamily)
            : base(group)
        {
            if (keyFamily == null)
            {
                throw new ArgumentNullException("keyFamily");
            }

            this._attachmentConstraintRef = group.AttachmentConstraintRef;
            foreach (string conceptId in group.DimensionRefs)
            {
                IDimensionSuperObject dim = keyFamily.GetDimensionById(conceptId);
                this._dimension.Add(dim);
                this._dimensionMap.Add(conceptId, dim);
            }

            this._id = group.Id;
        }

        /// <summary>
        ///     Gets the attachment constraint ref.
        /// </summary>
        public ICrossReference AttachmentConstraintRef
        {
            get
            {
                return this._attachmentConstraintRef;
            }
        }

        /// <summary>
        ///     Gets the dimension.
        /// </summary>
        public IList<IDimensionSuperObject> Dimensions
        {
            get
            {
                return new List<IDimensionSuperObject>(this._dimension);
            }
        }

        /// <summary>
        ///     Gets the identifier.
        /// </summary>
        /// <value>
        ///     The identifier.
        /// </value>
        public new string Id
        {
            get
            {
                return this._id;
            }
        }

        /// <summary>
        ///     The get dimension by id.
        /// </summary>
        /// <param name="conceptId">The concept id.</param>
        /// <returns>
        ///     The <see cref="IDimensionSuperObject" /> .
        /// </returns>
        public IDimensionSuperObject GetDimensionById(string conceptId)
        {
            return this._dimensionMap.GetOrDefault(conceptId);
        }
    }
}