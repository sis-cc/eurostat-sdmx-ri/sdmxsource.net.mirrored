// -----------------------------------------------------------------------
// <copyright file="MetadataObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Metadata
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.MetaData.Generic;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Metadata;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The metadata object core.
    /// </summary>
    [Serializable]
    public class MetadataObjectCore : SdmxObjectCore, IMetadata
    {
        /// <summary>
        ///     The iheader.
        /// </summary>
        private readonly IHeader _header;

        /// <summary>
        ///     The metadata sets.
        /// </summary>
        private readonly IList<IMetadataSet> _metadataSets;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataObjectCore" /> class.
        /// </summary>
        /// <param name="metadata">
        ///     The metadata.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="metadata"/> is <see langword="null" />.</exception>
        public MetadataObjectCore(GenericMetadata metadata)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataDocument), null)
        {
            if (metadata == null)
            {
                throw new ArgumentNullException("metadata");
            }

            this._metadataSets = new List<IMetadataSet>();

            this._header = new HeaderImpl(metadata.Content.Header);

            foreach (MetadataSetType metadataset in metadata.Content.DataSet)
            {
                this._metadataSets.Add(new MetadataSetObjectCore(this, metadataset));
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataObjectCore" /> class.
        /// </summary>
        /// <param name="metadataSets">
        ///     The metadata.
        /// </param>
        public MetadataObjectCore(ICollection<IMetadataSet> metadataSets)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataDocument), null)
        {
            if (metadataSets != null)
            {
                this._metadataSets.AddAll(metadataSets);
            }
        }

        /// <summary>
        ///     Gets the header.
        /// </summary>
        public virtual IHeader Header
        {
            get
            {
                return this._header;
            }
        }

        /// <summary>
        ///     Gets the metadata set.
        /// </summary>
        public virtual IList<IMetadataSet> MetadataSet
        {
            get
            {
                return this._metadataSets;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">The sdmxObject.</param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                IMetadata that = (IMetadata)sdmxObject;
                if (!this.Equivalent(this._metadataSets, that.MetadataSet, includeFinalProperties))
                {
                    return false;
                }

                return this.DeepEqualsInternal(that);
            }

            return false;
        }

        /// <summary>
        ///     The get composites internal.
        /// </summary>
        /// <returns>The set of SDMX objects</returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = new HashSet<ISdmxObject>();
            this.AddToCompositeSet(this._metadataSets, composites);
            return composites;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
    }
}