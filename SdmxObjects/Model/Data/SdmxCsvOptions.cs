namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data
{
    using System.Collections.Generic;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// Class for defining Sdmx CSV options
    /// </summary>
    public class SdmxCsvOptions
    {
        /// <summary>
        /// Use labels in headers, in dimension & attribute columns
        /// </summary>
        public CsvLabels CsvLabels { get; set; } = CsvLabels.GetFromEnum(CsvLabelsEnumType.Id);

        /// <summary>
        /// Use normalized time
        /// </summary>
        public bool UseNormalizedTime { get; set; }

        /// <summary>
        /// Csv encoding
        /// </summary>
        public Encoding Encoding { get; private set; }

        /// <summary>
        /// Delimiter within a CSV column for multiple value fields.
        /// </summary>
        public char? MultipleValueDelimiter { get; set; }

        /// <summary>
        /// The SDMX ids of the components (attributes and measures) that should be included as columns in the csv.
        /// </summary>
        public List<string> RequestedComponentIds { get; private set; }

        /// <summary>
        /// Use culture specific column and decimal separators.
        /// </summary>
        public bool UseCultureSpecificColumnAndDecimalSeparators { get; }

        /// <summary>
        /// Is the old test client format
        /// </summary>
        public bool IsOldTestClientFormat { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SdmxCsvOptions" /> class.
        /// </summary>
        /// <param name="encoding">Csv encoding.</param>
        /// <param name="requestedComponentIds">The SDMX ids of the components (attributes and measures) that should be included as columns in the csv.</param>
        /// <param name="useCultureSpecificColumnAndDecimalSeparators">Use culture specific column and decimal separators.</param>
        ///  <param name="isOldTestClientFormat">Is the old test client format</param>
        public SdmxCsvOptions(Encoding encoding = null, List<string> requestedComponentIds = null, bool useCultureSpecificColumnAndDecimalSeparators = false, bool isOldTestClientFormat = false)
        {
            this.Encoding = encoding ?? Encoding.UTF8;
            this.RequestedComponentIds = requestedComponentIds ?? new List<string>();
            this.UseCultureSpecificColumnAndDecimalSeparators = useCultureSpecificColumnAndDecimalSeparators;
            IsOldTestClientFormat = isOldTestClientFormat;
        }

    }
}
