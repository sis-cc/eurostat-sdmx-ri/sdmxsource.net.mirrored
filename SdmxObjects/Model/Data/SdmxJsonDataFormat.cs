// -----------------------------------------------------------------------
// <copyright file="SdmxDataFormatCore.cs" company="EUROSTAT">
//   Date Created : 2013-03-14
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Translator;

    /// <summary>
    /// The SDMX json data format.
    /// </summary>
    public class SdmxJsonDataFormat : SdmxDataFormatCore
    {
        /// <summary>
        /// The translator
        /// </summary>
        private readonly ITranslator _translator;

        /// <summary>
        /// Initializes a new instance of the <see cref="SdmxJsonDataFormat"/> class. 
        /// Initializes a new instance of the <see cref="SdmxDataFormatCore"/> class.
        /// </summary>
        /// <param name="sdmxSuperObjectRetrievalManager">
        /// The sdmxSuperObjectRetrievalManager.
        /// </param>
        /// <param name="objectRetrievalManager">
        /// The sdmxSuperObjectRetrievalManager.
        /// </param>
        /// <param name="dataType">
        /// The data type.
        /// </param>
        /// <param name="encoding">
        /// The encoding.
        /// </param>
        /// <param name="translator">
        /// The translator.
        /// </param>
        public SdmxJsonDataFormat(ISdmxSuperObjectRetrievalManager sdmxSuperObjectRetrievalManager,  DataType dataType,  ITranslator translator, ISdmxObjectRetrievalManager objectRetrievalManager=null) : base(dataType)
        {
            this.SdmxSuperObjectRetrievalManager = sdmxSuperObjectRetrievalManager  ?? throw new ArgumentNullException(nameof(sdmxSuperObjectRetrievalManager));;
            this._translator = translator ?? throw new ArgumentNullException(nameof(objectRetrievalManager));
            this.SdmxObjectRetrievalManager = objectRetrievalManager;
        }

        public ISdmxSuperObjectRetrievalManager SdmxSuperObjectRetrievalManager { get; }

        /// <summary>
        /// Gets the SdmxSuperObjectRetrievalManager.
        /// </summary>
        public ISdmxObjectRetrievalManager SdmxObjectRetrievalManager { get; }

        /// <summary>
        /// Gets the translator.
        /// </summary>
        public ITranslator Translator
        {
            get
            {
                return this._translator;
            }
        }
    }
}