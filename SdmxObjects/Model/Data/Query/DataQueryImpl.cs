// -----------------------------------------------------------------------
// <copyright file="DataQueryImpl.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The data query impl.
    /// </summary>
    public class DataQueryImpl : BaseDataQuery, IDataQuery
    {
        /// <summary>
        ///     The _data providers.
        /// </summary>
        private readonly ISet<IDataProvider> _dataProviders = new HashSet<IDataProvider>();

        /// <summary>
        ///     The _data query selection groups.
        /// </summary>
        private readonly IList<IDataQuerySelectionGroup> _dataQuerySelectionGroups =
            new List<IDataQuerySelectionGroup>();

        /// <summary>
        ///     The _last updated.
        /// </summary>
        private readonly ISdmxDate _lastUpdated;

        /// <summary>
        /// The rest data query.
        /// </summary>
        private readonly IRestDataQuery _dataQuery;

        /// <summary>
        /// The retrieval manager.
        /// </summary>
        private readonly ISdmxObjectRetrievalManager _retrievalManager;

        /// <summary>
        /// The apply contentConstraints flag.
        /// </summary>
        private readonly bool _applyContentConstraints;

        /// <summary>
        /// The apply wildcard flag.
        /// </summary>
        private readonly bool _applyWildcardValues;

        /// <summary>
        /// A flag to use in <see cref="SetStructureReferences"/>
        /// </summary>
        private bool _structureAlreadySet;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataQueryImpl" /> class.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <param name="applyContentConstraints">The apply contentConstraints flag.</param>
        /// <param name="applyWildcardValues">The apply wildcard flag.</param>
        /// <exception cref="SdmxNoResultsException">
        ///     Data Flow could not be found for query :  + dataQuery.FlowRef
        ///     or
        ///     DSD could not be found for query :  + this.Dataflow.DataStructureRef
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        ///     Not enough key values in query, expecting
        ///     + this.DataStructure.GetDimensions(
        ///     SdmxStructureEnumType.Dimension,
        ///     SdmxStructureEnumType.MeasureDimension).Count +  got  + dataQuery.QueryList.Count
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="retrievalManager"/> is <see langword="null" />.</exception>
        public DataQueryImpl(IRestDataQuery dataQuery, ISdmxObjectRetrievalManager retrievalManager, bool applyContentConstraints = false, bool? applyWildcardValues = null)
        {
            if (dataQuery == null)
            {
                throw new ArgumentNullException("dataQuery");
            }

            if (retrievalManager == null)
            {
                throw new ArgumentNullException("retrievalManager");
            }

            this._dataQuery = dataQuery;
            this._retrievalManager = retrievalManager;
            this._applyContentConstraints = applyContentConstraints;
            this._applyWildcardValues = applyWildcardValues ?? applyContentConstraints;

            this._lastUpdated = dataQuery.UpdatedAfter;
            SetDataQueryDetail(dataQuery.QueryDetail);

            if (dataQuery is IRestDataQueryV2 dataQueryV2)
            {
                this.Attributes = dataQueryV2.Attributes ?? Attributes.GetFromEnum(AttributesEnumType.Dsd);
                this.Measures = dataQueryV2.Measures ?? Measures.GetFromEnum(MeasuresEnumType.All);
            }

            this.FirstNObservations = dataQuery.FirstNObservations;
            this.LastNObservations = dataQuery.LastNObsertations;
            this.IncludeHistory = dataQuery.IncludeHistory;
            this.AsOfDate = dataQuery.AsOfDate;

            if (ObjectUtil.ValidString(dataQuery.DimensionAtObservation))
            {
                this.DimensionAtObservation = dataQuery.DimensionAtObservation;
            }

            SetStructureReferences();
            this._structureAlreadySet = true;
        }

        /// <summary>Gets or sets a value indicating whether [include history].</summary>
        /// <value>
        ///   <c>true</c> if [include history]; otherwise, <c>false</c>.</value>
        public bool IncludeHistory { get; set; }

        /// <summary>
        /// Gets asOf date (returns data as it was stored at the defined point in time), or null if undefined
        /// </summary>
        public ISdmxDate AsOfDate { get; set; }

        /// <summary>
        /// Gets or sets, whether the metadata availability information is needed.
        /// </summary>
        public bool IsMetadataAvailabilityNeeded { get; set; }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataQueryImpl" /> class.
        /// </summary>
        /// <param name="dataStructure">
        ///     The data structure.
        /// </param>
        /// <param name="lastUpdated">
        ///     The last updated.
        /// </param>
        /// <param name="dataQueryDetail">
        ///     The data query detail.
        /// </param>
        /// <param name="firstNObs">
        ///     The first n obs.
        /// </param>
        /// <param name="lastNObs">
        ///     The last n obs.
        /// </param>
        /// <param name="dataProviders">
        ///     The data providers.
        /// </param>
        /// <param name="dataflow">
        ///     The dataflow.
        /// </param>
        /// <param name="dimensionAtObservation">
        ///     The dimension at observation.
        /// </param>
        /// <param name="selections">
        ///     The selections.
        /// </param>
        /// <param name="dateFrom">
        ///     The date from.
        /// </param>
        /// <param name="dateTo">
        ///     The date to.
        /// </param>
        public DataQueryImpl(
            IDataStructureObject dataStructure, 
            ISdmxDate lastUpdated, 
            DataQueryDetail dataQueryDetail, 
            int? firstNObs, 
            int? lastNObs, 
            ISet<IDataProvider> dataProviders, 
            IDataflowObject dataflow, 
            string dimensionAtObservation, 
            ISet<IDataQuerySelection> selections, 
            DateTime? dateFrom, 
            DateTime? dateTo)
        {
            this.DataStructure = dataStructure;
            if (dataProviders != null)
            {
                this._dataProviders = new HashSet<IDataProvider>(dataProviders);
            }

            if (ObjectUtil.ValidCollection(selections) || dateFrom != null || dateTo != null)
            {
                ISdmxDate sdmxDateFrom = null;
                if (dateFrom != null)
                {
                    sdmxDateFrom = new SdmxDateCore(dateFrom, TimeFormatEnumType.Date);
                }

                ISdmxDate sdmxDateTo = null;
                if (dateTo != null)
                {
                    sdmxDateTo = new SdmxDateCore(dateTo, TimeFormatEnumType.Date);
                }

                this._dataQuerySelectionGroups.Add(
                    new DataQuerySelectionGroupImpl(selections, sdmxDateFrom, sdmxDateTo));
            }

            this.Dataflow = dataflow;
            this._lastUpdated = lastUpdated;
            this.SetDataQueryDetail(dataQueryDetail);
            this.DimensionAtObservation = dimensionAtObservation;
            this.FirstNObservations = firstNObs;
            this.LastNObservations = lastNObs;
            this.ValidateQuery();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataQueryImpl" /> class.
        /// </summary>
        /// <param name="dataStructure">The data structure.</param>
        /// <param name="lastUpdated">The last updated.</param>
        /// <param name="dataQueryDetail">The data query detail.</param>
        /// <param name="maxObs">The maximum obs.</param>
        /// <param name="orderAsc">if set to <c>true</c> [order asc].</param>
        /// <param name="dataProviders">The data providers.</param>
        /// <param name="dataflow">The dataflow.</param>
        /// <param name="dimensionAtObservation">The dimension at observation.</param>
        /// <param name="selections">The selections.</param>
        /// <param name="dateFrom">The date from.</param>
        /// <param name="dateTo">The date to.</param>
        public DataQueryImpl(
            IDataStructureObject dataStructure, 
            ISdmxDate lastUpdated, 
            DataQueryDetail dataQueryDetail, 
            int maxObs, 
            bool orderAsc, 
            ISet<IDataProvider> dataProviders, 
            IDataflowObject dataflow, 
            string dimensionAtObservation, 
            ISet<IDataQuerySelection> selections, 
            DateTime? dateFrom, 
            DateTime? dateTo)
        {
            this.DataStructure = dataStructure;
            this._lastUpdated = lastUpdated;
            this.SetDataQueryDetail(dataQueryDetail);

            if (orderAsc)
            {
                this.FirstNObservations = maxObs;
            }
            else
            {
                this.LastNObservations = maxObs;
            }

            if (dataProviders != null)
            {
                this._dataProviders = new HashSet<IDataProvider>(dataProviders);
            }

            this.Dataflow = dataflow;
            this.DimensionAtObservation = dimensionAtObservation;

            if (ObjectUtil.ValidCollection(selections) || dateFrom != null || dateTo != null)
            {
                ISdmxDate sdmxDateFrom = null;
                if (dateFrom != null)
                {
                    sdmxDateFrom = new SdmxDateCore(dateFrom, TimeFormatEnumType.Date);
                }

                ISdmxDate sdmxDateTo = null;
                if (dateTo != null)
                {
                    sdmxDateTo = new SdmxDateCore(dateTo, TimeFormatEnumType.Date);
                }

                // TODO: move to fluent interface
                this._dataQuerySelectionGroups.Add(
                    new DataQuerySelectionGroupImpl(selections, sdmxDateFrom, sdmxDateTo));
            }

            this.ValidateQuery();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataQueryImpl" /> class.
        /// </summary>
        /// <param name="dataStructure">The data structure.</param>
        /// <param name="lastUpdated">The last updated.</param>
        /// <param name="dataQueryDetail">The data query detail.</param>
        /// <param name="firstNObs">The first asynchronous obs.</param>
        /// <param name="lastNObs">The last asynchronous obs.</param>
        /// <param name="dataProviders">The data providers.</param>
        /// <param name="dataflow">The dataflow.</param>
        /// <param name="dimensionAtObservation">The dimension at observation.</param>
        /// <param name="selectionGroup">The selection group.</param>
        public DataQueryImpl(
            IDataStructureObject dataStructure, 
            ISdmxDate lastUpdated, 
            DataQueryDetail dataQueryDetail, 
            int? firstNObs, 
            int? lastNObs, 
            IEnumerable<IDataProvider> dataProviders, 
            IDataflowObject dataflow, 
            string dimensionAtObservation, 
            IEnumerable<IDataQuerySelectionGroup> selectionGroup)
        {
            this.DataStructure = dataStructure;
            this._lastUpdated = lastUpdated;
            this.SetDataQueryDetail(dataQueryDetail);
            this.FirstNObservations = firstNObs;
            this.LastNObservations = lastNObs;
            if (dataProviders != null)
            {
                this._dataProviders = new HashSet<IDataProvider>(dataProviders);
            }

            this.Dataflow = dataflow;
            this.DimensionAtObservation = dimensionAtObservation;

            if (selectionGroup != null)
            {
                foreach (IDataQuerySelectionGroup dqsg in selectionGroup)
                {
                    if (dqsg != null)
                    {
                        this._dataQuerySelectionGroups.Add(dqsg);
                    }
                }
            }

            this.ValidateQuery();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataQueryImpl" /> class.
        /// </summary>
        /// <param name="dataStructure">
        ///     The data structure.
        /// </param>
        /// <param name="lastUpdated">
        ///     The last updated.
        /// </param>
        /// <param name="dataQueryDetail">
        ///     The data query detail.
        /// </param>
        /// <param name="maxObs">
        ///     The max obs.
        /// </param>
        /// <param name="orderAsc">
        ///     If the order is ascending.
        /// </param>
        /// <param name="dataProviders">
        ///     The data providers.
        /// </param>
        /// <param name="dataflow">
        ///     The dataflow.
        /// </param>
        /// <param name="dimensionAtObservation">
        ///     The dimension at observation.
        /// </param>
        /// <param name="selectionGroup">
        ///     The selection group.
        /// </param>
        public DataQueryImpl(
            IDataStructureObject dataStructure, 
            ISdmxDate lastUpdated, 
            DataQueryDetail dataQueryDetail, 
            int maxObs, 
            bool orderAsc, 
            ISet<IDataProvider> dataProviders, 
            IDataflowObject dataflow, 
            string dimensionAtObservation, 
            ICollection<IDataQuerySelectionGroup> selectionGroup)
        {
            this.DataStructure = dataStructure;
            this._lastUpdated = lastUpdated;
            this.SetDataQueryDetail(dataQueryDetail);

            if (orderAsc)
            {
                this.FirstNObservations = maxObs;
            }
            else
            {
                this.LastNObservations = maxObs;
            }

            if (dataProviders != null)
            {
                this._dataProviders = new HashSet<IDataProvider>(dataProviders);
            }

            this.Dataflow = dataflow;
            this.DimensionAtObservation = dimensionAtObservation;

            if (selectionGroup != null)
            {
                foreach (IDataQuerySelectionGroup dqsg in selectionGroup)
                {
                    if (dqsg != null)
                    {
                        this._dataQuerySelectionGroups.Add(dqsg);
                    }
                }
            }

            this.ValidateQuery();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataQueryImpl" /> class.
        /// </summary>
        /// <param name="dataStructure">The data structure.</param>
        /// <param name="dataflow">The dataflow.</param>
        /// <param name="dataQueryDetail">The data query detail.</param>
        private DataQueryImpl(
            IDataStructureObject dataStructure, 
            IDataflowObject dataflow, 
            DataQueryDetail dataQueryDetail)
        {
            this.DataStructure = dataStructure;
            this.SetDataQueryDetail(dataQueryDetail);

            this.Dataflow = dataflow;
            this.ValidateQuery();
        }

        /// <summary>
        ///     Gets the data provider.
        /// </summary>
        public virtual ISet<IDataProvider> DataProvider
        {
            get
            {
                return new HashSet<IDataProvider>(this._dataProviders);
            }
        }
        /// <summary>
        ///     Gets the last updated date.
        /// </summary>
        public virtual ISdmxDate LastUpdatedDate
        {
            get
            {
                return this._lastUpdated;
            }
        }

        /// <summary>
        ///     Gets the selection groups.
        /// </summary>
        public virtual IList<IDataQuerySelectionGroup> SelectionGroups
        {
            get
            {
                return new List<IDataQuerySelectionGroup>(this._dataQuerySelectionGroups);
            }
        }

        /// <summary>
        /// Builds the query info based on a different <see cref="IRestDataQuery.FlowRef"/>.
        /// </summary>
        public void SetStructureReferences()
        {
            // a constructor is using this method for the initial dataQuery.FlowRef,
            // so avoid rebuilding during iterations.
            if (this._structureAlreadySet)
            {
                this._structureAlreadySet = false;
                return;
            }

            this.RetrieveDataflow(this._dataQuery, this._retrievalManager);

            this.RetrieveDsd(this._retrievalManager);

            this.RetrieveMsd(this._retrievalManager);

            RetrieveDataProviders(this._dataQuery, this._retrievalManager);

            this.BuildSelections(this._dataQuery);

            if (this._applyContentConstraints)
            {
                this.ApplyContentConstraints(this._dataQuery, this._retrievalManager);
                
            }

            if (this._applyWildcardValues)
            {
                this.ApplyWildCardValues();
            }

            this.ValidateQuery();
        }

        /// <summary>
        ///     Builds the empty query.
        /// </summary>
        /// <param name="dataStructure">The data structure.</param>
        /// <param name="dataflow">The dataflow.</param>
        /// <param name="dataQueryDetail">The data query detail.</param>
        /// <returns>The data query.</returns>
        public static IDataQuery BuildEmptyQuery(
            IDataStructureObject dataStructure, 
            IDataflowObject dataflow, 
            DataQueryDetail dataQueryDetail)
        {
            return new DataQueryImpl(dataStructure, dataflow, dataQueryDetail);
        }

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>
        ///     The has selections.
        /// </summary>
        /// <returns> The <see cref="bool" /> . </returns>
        public virtual bool HasSelections()
        {
            return this._dataQuerySelectionGroups.Count > 0;
        }

        /// <summary>
        ///     The to string.
        /// </summary>
        /// <returns> The <see cref="string" /> . </returns>
        public override string ToString()
        {
            var sb = new StringBuilder();

            string newLine = Environment.NewLine;
            if (this.DataStructure != null)
            {
                sb.Append(newLine).Append("Data Structure : ").Append(this.DataStructure.Urn);
            }

            if (this.Dataflow != null)
            {
                sb.Append(newLine).Append("Dataflow : ").Append(this.Dataflow.Urn);
            }

            if (this._dataProviders != null)
            {
                foreach (IDataProvider dataProvider in this._dataProviders)
                {
                    sb.Append(newLine).Append("Data Provider  : ").Append(dataProvider.Urn);
                }
            }

            // ADD SELECTION INFORMATION
            if (this.HasSelections())
            {
                string concat = string.Empty;

                foreach (IDataQuerySelectionGroup selectionGroup in this._dataQuerySelectionGroups)
                {
                    sb.Append(concat).Append("(").Append(selectionGroup).Append(")");
                    concat = "OR";
                }
            }

            return sb.ToString();
        }

        /// <summary>
        ///     Gets a set of strings which represent the component ids that are being queried on
        /// </summary>
        /// <returns>
        ///     The set of components
        /// </returns>
        protected override ISet<string> GetQueryComponentIds()
        {
            ISet<string> returnSet = new HashSet<string>();
            foreach (IDataQuerySelectionGroup dqsg in this.SelectionGroups)
            {
                foreach (IDataQuerySelection dqs in dqsg.Selections)
                {
                    returnSet.Add(dqs.ComponentId);
                }
            }

            return returnSet;
        }

        /// <summary>
        /// Retrieves the data providers.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        private static void RetrieveDataProviders(IRestDataQuery dataQuery, ISdmxObjectRetrievalManager retrievalManager)
        {
            ISet<IDataProvider> dataProviders = new HashSet<IDataProvider>();
            if (dataQuery.ProviderRef != null)
            {
                ISet<IDataProviderScheme> dataProviderSchemes = retrievalManager.GetMaintainableObjects<IDataProviderScheme>(dataQuery.ProviderRef.MaintainableReference);
                foreach (IDataProviderScheme currentDpScheme in dataProviderSchemes)
                {
                    foreach (IDataProvider dataProvider in currentDpScheme.Items)
                    {
                        if (dataProvider.Id.Equals(dataQuery.ProviderRef.ChildReference.Id))
                        {
                            dataProviders.Add(dataProvider);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Builds the selections.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <exception cref="SdmxSemmanticException">Not enough key values in query, expecting the number of Dimensions got dataQuery.QueryList.Count</exception>
        private void BuildSelections(IRestDataQuery dataQuery)
        {
            ISet<IDataQuerySelection> selections = new HashSet<IDataQuerySelection>();
            if (dataQuery.QueryList.Count > 0)
            {
                int i = 0;
                foreach (IDimension dimension in
                    this.DataStructure.GetDimensions(SdmxStructureEnumType.Dimension, SdmxStructureEnumType.MeasureDimension).OrderBy(dimension => dimension.Position))
                {
                    if (dataQuery.QueryList.Count <= i)
                    {
                        HandleQueryDimensionNotFound(dataQuery.QueryList.Count);
                        continue;
                    }

                    ISet<string> queriesForDimension = dataQuery.QueryList[i];
                    if (queriesForDimension != null && queriesForDimension.Count > 0)
                    {
                        IDataQuerySelection selectionsForDimension = new DataQueryDimensionSelectionImpl(dimension.Id, new HashSet<string>(queriesForDimension));
                        selections.Add(selectionsForDimension);
                    }

                    i++;
                }
            }

            if (ObjectUtil.ValidCollection(selections) || dataQuery.StartPeriod != null || dataQuery.EndPeriod != null)
            {
                this._dataQuerySelectionGroups.Add(new DataQuerySelectionGroupImpl(selections, dataQuery.StartPeriod, dataQuery.EndPeriod));
            }
        }

        /// <summary>
        /// Throws an exception if number of data query dimensions is less to actual number of dimensions.
        /// </summary>
        /// <param name="dimsFound">the number of dimensions provided in the data query.</param>
        /// <exception cref="SdmxSemmanticException">Not enough key values in query.</exception>
        protected virtual void HandleQueryDimensionNotFound(int dimsFound)
        {
            throw new SdmxSemmanticException(
                "Not enough key values in query, expecting " + this.DataStructure.GetDimensions(SdmxStructureEnumType.Dimension, SdmxStructureEnumType.MeasureDimension).Count + " got "
                + dimsFound);
        }

        /// <summary>
        /// Retrieves the DSD.
        /// </summary>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <exception cref="SdmxNoResultsException">DSD could not be found for query :  + this.Dataflow.DataStructureRef</exception>
        private void RetrieveDsd(ISdmxObjectRetrievalManager retrievalManager)
        {
            this.DataStructure = retrievalManager.GetMaintainableObject<IDataStructureObject>(this.Dataflow.DataStructureRef.MaintainableReference);
            if (this.DataStructure == null)
            {
                throw new SdmxNoResultsException("DSD could not be found for query : " + this.Dataflow.DataStructureRef);
            }
        }

        /// <summary>
        /// Retrieves the MSD.
        /// </summary>
        /// <param name="retrievalManager">The retrieval manager.</param>
        private void RetrieveMsd(ISdmxObjectRetrievalManager retrievalManager)
        {
            if (this.DataStructure == null || !this.DataStructure.Annotations.Any())
            {
                return;
            }

            var msdAnnotation = this.DataStructure.Annotations.FirstOrDefault(x => x.Type.Equals("Metadata", StringComparison.OrdinalIgnoreCase));

            if (msdAnnotation == null || string.IsNullOrEmpty(msdAnnotation.Title))
            {
                return;
            }

            MetadataStructure = retrievalManager.GetMaintainableObject<IMetadataStructureDefinitionObject>(new StructureReferenceImpl(msdAnnotation.Title));

            if (MetadataStructure == null)
            {
                //throw new SdmxNoResultsException("MSD could not be found for query : " + this.Dataflow.DataStructureRef);
            }
        }

        /// <summary>
        /// Retrieves the dataflow.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <exception cref="SdmxNoResultsException">Data Flow could not be found for query :  + dataQuery.FlowRef</exception>
        private void RetrieveDataflow(IRestDataQuery dataQuery, ISdmxObjectRetrievalManager retrievalManager)
        {
            if (dataQuery is IRestDataQueryV2 dataQueryV2)
            {
                IStructureReference queryReferenceBean = dataQueryV2.FlowRef.ChangeStarsToNull();
                try
                {
                    this.Dataflow = retrievalManager.GetMaintainableObject<IDataflowObject>(queryReferenceBean, dataQueryV2.QueryMetadata);
                }
                catch (SdmxException e)
                {
                    if (e.Message.StartsWith("Did not expect more then 1 structure from query"))
                    {
                        throw new SdmxNotImplementedException(
                            "Keyword '*' cannot be used to retrieve multiple dataflows for the same agency or for different versions. " +
                            "Feature not yet implemented. Please try to provide the exact values.");
                    }
                    
                    throw;
                }
                // the above could be the case for '*' in either agencyId, resourceId or version
                // TODO (konstantinos): try implement by overriding this method in DataQueryV2Impl & using an IMaintainableObjectIterator
            }
            else
            {
                this.Dataflow = retrievalManager.GetMaintainableObject<IDataflowObject>(dataQuery.FlowRef.MaintainableReference);
            }
            
            if (this.Dataflow == null)
            {
                throw new SdmxNoResultsException("Data Flow could not be found for query : " + dataQuery.FlowRef);
            }
        }


        private void ApplyWildCardValues()
        {
            if (!_dataQuerySelectionGroups.Any())
            {
                return;
            }

            var dataQuerySelectionGroup = _dataQuerySelectionGroups.First();

            foreach (var sel in dataQuerySelectionGroup.SelectionsSet)
            {
                sel.Values.AddAll(SpecialDimensionValue.Values.Select(x => x.DimensionValue));
            }
        }


        /// <summary>
        /// Applies the allowed contentConstraints
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        private void ApplyContentConstraints(IRestDataQuery dataQuery, ISdmxObjectRetrievalManager retrievalManager)
        {
            var allowedContentConstraintObjects = GetAllowedContentConstraints(dataQuery, retrievalManager);

            if (allowedContentConstraintObjects == null)
            {
                return;
            }

            if (!_dataQuerySelectionGroups.Any())
            {
                _dataQuerySelectionGroups.Add(new DataQuerySelectionGroupImpl(new HashSet<IDataQuerySelection>(), null, null));
            }

            var dataQuerySelectionGroup = _dataQuerySelectionGroups.First();

            foreach (var allowedContentConstraint in allowedContentConstraintObjects)
            {
                if (allowedContentConstraint.IncludedCubeRegion == null ||
                    !allowedContentConstraint.IncludedCubeRegion.KeyValues.Any() && !allowedContentConstraint.IncludedCubeRegion.AttributeValues.Any())
                {
                    continue;
                }

                foreach (var constraintCubeRegion in allowedContentConstraint.IncludedCubeRegion.KeyValues)
                {
                    if (constraintCubeRegion.TimeRange != null)
                    {
                        if (constraintCubeRegion.TimeRange.StartDate != null 
                            && (dataQuerySelectionGroup.DateFrom == null || dataQuerySelectionGroup.DateFrom.Date < constraintCubeRegion.TimeRange.StartDate.Date))
                        {
                            dataQuerySelectionGroup.DateFrom = constraintCubeRegion.TimeRange.StartDate;
                        }

                        if (constraintCubeRegion.TimeRange.EndDate != null
                            && (dataQuerySelectionGroup.DateTo == null || dataQuerySelectionGroup.DateTo.PeriodEndDate > constraintCubeRegion.TimeRange.EndDate.Date))
                        {
                            dataQuerySelectionGroup.DateTo = constraintCubeRegion.TimeRange.EndDate;
                        }

                        continue;
                    }

                    if (constraintCubeRegion.Values == null || !constraintCubeRegion.Values.Any())
                    {
                        continue;
                    }

                    var dataQuerySelection = dataQuerySelectionGroup.SelectionsSet.FirstOrDefault(x => x.ComponentId.Equals(constraintCubeRegion.Id, StringComparison.OrdinalIgnoreCase));

                    // No user filter, add filter from the constraint.
                    if (dataQuerySelection == null)
                    {
                        dataQuerySelectionGroup.SelectionsSet.Add(new DataQueryDimensionSelectionImpl(constraintCubeRegion.Id, new HashSet<string>(constraintCubeRegion.Values)));
                    }
                    else
                    {
                        if (!dataQuerySelection.Values.Any())
                        {
                            dataQuerySelection.Values.AddAll(constraintCubeRegion.Values);
                        }
                        else
                        {
                            // Get the intersection of the constraint and the user filter.
                            var filtersIntersect = constraintCubeRegion.Values.Intersect(dataQuerySelection.Values).ToList();

                            if (!filtersIntersect.Any())
                            {
                                throw new SdmxException(SdmxErrorCodeEnumType.NoResultsFound.ToString(), SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.NoResultsFound));
                            }

                            dataQuerySelection.Values.Clear();
                            dataQuerySelection.Values.AddAll(filtersIntersect);
                        }
                    }
                }

                // Add all attribute constraints.
                foreach (var constraintAttribute in allowedContentConstraint.IncludedCubeRegion.AttributeValues)
                {
                    if (constraintAttribute.Values == null || !constraintAttribute.Values.Any())
                    {
                        continue;
                    }

                    var constraintValues = new HashSet<string>(constraintAttribute.Values);

                    if (!this.DataStructure.Attributes.FirstOrDefault(a => a.Id.Equals(constraintAttribute.Id, StringComparison.InvariantCultureIgnoreCase))?.Mandatory ?? false)
                    {
                        //Add empty string value to let the allowed constraint include observations with optional attribute value not provided
                        constraintValues.Add("");
                    }

                    dataQuerySelectionGroup.SelectionsSet.Add(new DataQueryDimensionSelectionImpl(constraintAttribute.Id, constraintValues));
                }
            }
            
        }

        /// <summary>
        /// Gets the allowed contentConstraints.
        /// </summary>
        /// <param name="dataQuery">The dataQuery.</param>
        /// <param name="manager">The retrieval manager.</param>
        /// <returns></returns>
        private static IEnumerable<IContentConstraintObject> GetAllowedContentConstraints(IRestDataQuery dataQuery, ISdmxObjectRetrievalManager manager)
        {
            try
            {
                var sdmxObjects = manager.GetMaintainables(
                    new RESTStructureQueryCore(
                        StructureQueryDetail.GetFromEnum(StructureQueryDetailEnumType.Full),
                        StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.Parents),
                        SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ContentConstraint),
                        dataQuery.FlowRef,
                        true));

                if (sdmxObjects?.ContentConstraintObjects != null && sdmxObjects.ContentConstraintObjects.Any(x => !x.IsDefiningActualDataPresent))
                {
                    return sdmxObjects.ContentConstraintObjects.Where(x => !x.IsDefiningActualDataPresent);
                }

                return null;
            }
            catch (SdmxNoResultsException)
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the max dateTime.
        /// </summary>
        /// <param name="dt1">First dateTime.</param>
        /// <param name="dt2">Second dateTime.</param>
        /// <returns></returns>
        private static DateTime? Max(DateTime? dt1, DateTime? dt2) => dt2 == null || dt1 > dt2 ? dt1 : dt2;

        /// <summary>
        /// Gets the min dateTime.
        /// </summary>
        /// <param name="dt1">First dateTime.</param>
        /// <param name="dt2">Second dateTime.</param>
        private static DateTime? Min(DateTime? dt1, DateTime? dt2) => dt2 == null || dt1 < dt2 ? dt1 : dt2;
    }
}