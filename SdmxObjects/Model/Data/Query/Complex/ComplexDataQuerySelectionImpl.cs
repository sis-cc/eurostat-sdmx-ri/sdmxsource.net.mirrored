﻿// -----------------------------------------------------------------------
// <copyright file="ComplexDataQuerySelectionImpl.cs" company="EUROSTAT">
//   Date Created : 2013-06-04
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query.Complex
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The complex data query selection class
    /// </summary>
    /// <seealso cref="Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex.IComplexDataQuerySelection" />
    public class ComplexDataQuerySelectionImpl : IComplexDataQuerySelection
    {
        /// <summary>
        ///     The _component identifier
        /// </summary>
        private readonly string _componentId;

        /// <summary>
        ///     The _values
        /// </summary>
        private readonly ISet<IComplexComponentValue> _values = new HashSet<IComplexComponentValue>();

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComplexDataQuerySelectionImpl" /> class.
        /// </summary>
        /// <param name="componentId">The component identifier.</param>
        /// <param name="value">The value.</param>
        /// <exception cref="SdmxSemmanticException">The query selection missing concept value exception.</exception>
        public ComplexDataQuerySelectionImpl(string componentId, IList<IComplexComponentValue> value)
        {
            if (!ObjectUtil.ValidString(componentId))
            {
                throw new SdmxSemmanticException(ExceptionCode.QuerySelectionMissingConcept);
            }

            this._componentId = componentId;

            if (value == null || value.Count == 0)
            {
                throw new SdmxSemmanticException(ExceptionCode.QuerySelectionMissingConceptValue);
            }

            // check 
            foreach (IComplexComponentValue currentValue in value)
            {
                this._values.Add(currentValue);
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComplexDataQuerySelectionImpl" /> class.
        /// </summary>
        /// <param name="componentId">The component identifier.</param>
        /// <param name="values">The values.</param>
        /// <exception cref="SdmxSemmanticException">The query selection missing concept value exception.</exception>
        public ComplexDataQuerySelectionImpl(string componentId, ISet<IComplexComponentValue> values)
        {
            if (!ObjectUtil.ValidString(componentId))
            {
                throw new SdmxSemmanticException(ExceptionCode.QuerySelectionMissingConcept);
            }

            this._componentId = componentId;

            if (!ObjectUtil.ValidCollection(values))
            {
                throw new SdmxSemmanticException(ExceptionCode.QuerySelectionMissingConceptValue);
            }

            this._values = new HashSet<IComplexComponentValue>(values);
        }

        /// <summary>
        ///     Gets the component Id that the code selection(s) are for.  The component Id is either a reference to a
        ///     dimension
        ///     or an attribute
        /// </summary>
        public string ComponentId
        {
            get
            {
                return this._componentId;
            }
        }

        /// <summary>
        ///     Gets the component value of the code that has been selected for the component.
        ///     <p />
        ///     If more then one value exists then calling this method will result in a exception.
        ///     Check that hasMultipleValues() returns false before calling this method
        /// </summary>
        /// <exception cref="InvalidOperationException">More than one value exists for this selection</exception>
        public IComplexComponentValue Value
        {
            get
            {
                return this._values.SingleOrDefault();
            }
        }

        /// <summary>
        ///     Gets all the code values that have been selected for this component
        /// </summary>
        public ISet<IComplexComponentValue> Values
        {
            get
            {
                return this._values;
            }
        }

        /// <summary>
        ///     Adds the value.
        /// </summary>
        /// <param name="value">The value.</param>
        public void AddValue(IComplexComponentValue value)
        {
            this._values.Add(value);
        }

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>
        ///     Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///     <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            var selection = obj as IComplexDataQuerySelection;
            return selection != null && this.ComponentId.Equals(selection.ComponentId)
                   && this.Values.SetEquals(selection.Values);
        }

        /// <summary>
        ///     Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        ///     A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

        /// <summary>
        ///     Returns true if more then one value exists for this component
        /// </summary>
        /// <returns>
        ///     The boolean
        /// </returns>
        public bool HasMultipleValues()
        {
            return this._values.Count > 1;
        }

        /// <summary>
        ///     Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        ///     A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(string.Empty);
            sb.Append(this._componentId);
            sb.Append(string.Empty);
            sb.Append(" : ");
            string concat = string.Empty;
            foreach (IComplexComponentValue currentValue in this._values)
            {
                sb.Append(concat);
                sb.Append(currentValue.Value);
                concat = ",";
            }

            return sb.ToString();
        }
    }
}