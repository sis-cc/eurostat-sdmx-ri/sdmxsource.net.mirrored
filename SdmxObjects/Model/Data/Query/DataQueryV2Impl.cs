// -----------------------------------------------------------------------
// <copyright file="DataQueryV2Impl.cs" company="EUROSTAT">
//   Date Created : 2021-09-09
//   Copyright (c) 2012, 2021 by the European Union.
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
// 
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query
{
    using System;
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    /// The data query implementation for SDMX REST 2.0
    /// </summary>
    public class DataQueryV2Impl : DataQueryImpl, IDataQueryV2
    {

        /// <summary>
        /// Initializes a new instance of the class <see cref="DataQueryV2Impl"/>.
        /// See base constructor
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <param name="applyContentConstraints">The apply contentConstraints flag.</param>
        /// <param name="applyWildcardValues">The apply wildcard flag.</param>
        /// <exception cref="SdmxNoResultsException">See base constructor.</exception>
        /// <exception cref="SdmxSemmanticException">See base constructor.</exception>
        /// <exception cref="ArgumentNullException">See base constructor.</exception>
        public DataQueryV2Impl(IRestDataQueryV2 dataQuery, ISdmxObjectRetrievalManager retrievalManager, bool applyContentConstraints = false, bool? applyWildcardValues = null)
            : base(dataQuery, retrievalManager, applyContentConstraints, applyWildcardValues)
        {
            this.DataQuery = dataQuery;
        }

        /// <summary>
        /// The data query
        /// </summary>
        public IRestDataQueryV2 DataQuery { get; }

        /// <inheritdoc />
        public IList<ComponentFilter> ComponentFilters => this.DataQuery.ComponentFilters;

        /// <inheritdoc />
        public IDataQueryIterator StartIterator()
        {
            return new DataQueryIteratorCore(SetStructureReferences, this.DataQuery);
        }

        /// <summary>
        /// Any dimension value omitted at the end of the Key is assumed as equivalent to a wildcard
        /// </summary>
        /// <param name="dimsFound"></param>
        protected override void HandleQueryDimensionNotFound(int dimsFound)
        {
        }
    }
}
