﻿// -----------------------------------------------------------------------
// <copyright file="SdmxDataFormatCore.cs" company="EUROSTAT">
//   Date Created : 2013-03-14
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;

    /// <summary>
    ///     The SDMX data format core
    /// </summary>
    public class SdmxDataFormatCore : IDataFormat
    {
        /// <summary>
        ///     The _data type
        /// </summary>
        private readonly DataType _dataType;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxDataFormatCore" /> class.
        /// </summary>
        /// <param name="dataType">Type of the data.</param>
        /// <exception cref="ArgumentException">Data Type can not be null for SdmxDataFormat</exception>
        public SdmxDataFormatCore(DataType dataType)
        {
            if (dataType == null)
            {
                throw new ArgumentException("Data Type can not be null for SdmxDataFormat");
            }

            this._dataType = dataType;
        }

        /// <summary>
        ///     Gets a string representation of the format, that can be used for auditing and debugging purposes.
        ///     <p />
        ///     This is expected to return a not null response.
        /// </summary>
        public virtual string FormatAsString
        {
            get
            {
                return this.ToString();
            }
        }

        /// <summary>
        ///     Gets the sdmx data format that this interface is describing.
        ///     If this is not describing an SDMX message then this will return null and the implementation class will be expected
        ///     to expose additional methods to understand the data
        /// </summary>
        public DataType SdmxDataFormat
        {
            get
            {
                return this._dataType;
            }
        }

        /// <summary>
        ///     Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        ///     A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return this._dataType.ToString();
        }
    }
}