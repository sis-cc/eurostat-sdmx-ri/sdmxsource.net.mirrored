// -----------------------------------------------------------------------
// <copyright file="CompactDataReaderEngine.cs" company="EUROSTAT">
//   Date Created : 2014-06-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxDataParser.
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data
{
    public class ComplexNodeValue: IComplexNodeValue
    {
        public ComplexNodeValue(string code)
        {
            if (code is null)
            {
                throw new ArgumentNullException(nameof(code));
            }

            Type = code.GetType();
            Code = code;
            TextValues = new List<ITextTypeWrapper>();

        }

        // TODO switch List to IList so we can use arrays and other types
        public ComplexNodeValue(Type type,string code = null, List<ITextTypeWrapper> textTypeWrappers = null)
        {
            Type = type;
            Code = code;
            TextValues = textTypeWrappers ?? new List<ITextTypeWrapper>();
        }

        // TODO why do we need this 
        public Type Type { get; set; }

        public string Code { get; set; }

        public List<ITextTypeWrapper> TextValues { get; set; }
    }
}