// -----------------------------------------------------------------------
// <copyright file="KeyValueImpl.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Util.Object;

    /// <summary>
    ///     The key value impl.
    /// </summary>
    [Serializable]
    public class KeyValueImpl : IKeyValue
    {
        /// <summary>
        ///     The _code.
        /// </summary>
        private readonly string _code;

        /// <summary>
        ///     The _concept.
        /// </summary>
        private readonly string _concept;

        private readonly List<IComplexNodeValue> _complexNodeValues;
        private static readonly Dictionary<string, WeakReference<KeyValueImpl>> cache = new Dictionary<string, WeakReference<KeyValueImpl>>();

        /// <summary>
        ///   Initializes a new instance of the <see cref="KeyValueImpl" /> class.
        ///   Caching Refernce Type of KeyValueImpl
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static IKeyValue GetInstance(string id, params string[] value)
        {
            if (value != null && value.Length > 1)
            {
                return new KeyValueImpl(id, value);
            }
            if (value == null || value.Length == 0)
            {
                return new KeyValueImpl(id, value);
            }
            string key = id + ":" + value[0];

            lock (cache)
            {
                if (cache.TryGetValue(key, out WeakReference<KeyValueImpl> weakKv) 
                    && weakKv.TryGetTarget(out KeyValueImpl returnKv))
                {
                    return returnKv;
                }
              
                var newKv = new KeyValueImpl(id, value);
                weakKv = new WeakReference<KeyValueImpl>(newKv);
                cache[key] = weakKv;
                return newKv;
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="KeyValueImpl" /> class.
        /// </summary>
        public KeyValueImpl()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="KeyValueImpl" /> class.
        /// </summary>
        /// <param name="code0">
        ///     The code 0.
        /// </param>
        /// <param name="concept1">
        ///     The concept 1.
        /// </param>
        public KeyValueImpl(string code0, string concept1)
        {
            _code = code0;
            _concept = concept1;
        }

        /// <summary>
        ///      Initializes a new instance of the <see cref="KeyValueImpl" /> class.
        /// </summary>
        /// <param name="concept"></param>
        /// <param name="complexNodeValues"></param>
        public KeyValueImpl(string concept, List<IComplexNodeValue> complexNodeValues)
        {
            _complexNodeValues = complexNodeValues;
            _concept = concept;
        }

        /// <summary>
        ///      Initializes a new instance of the <see cref="KeyValueImpl" /> class.
        /// </summary>
        /// <param name="concept"></param>
        /// <param name="complexNodeValues"></param>
        public KeyValueImpl(string concept, string[] complexNodeValues)
        {
            _concept = StringUtil.ManualIntern(concept);           
            
            if(complexNodeValues != null && complexNodeValues.Length > 0 ) 
            {
                if(complexNodeValues.Length > 1) 
                {                   
                    foreach (string value in complexNodeValues)
                    {                       
                        _complexNodeValues.Add(new ComplexNodeValue(value));
                    }
                }
                else
                {
                    _code = StringUtil.ManualIntern(complexNodeValues.First());
                }
            }       
        }

        /// <summary>
        ///     Gets the code.
        /// </summary>
        public virtual string Code
        {
            get
            {
                return this._code;
            }
        }

        /// <summary>
        ///     Gets the concept.
        /// </summary>
        public virtual string Concept
        {
            get
            {
                return this._concept;
            }
        }

        /// <summary>
        /// Get complex node values
        /// </summary>
        public List<IComplexNodeValue> ComplexNodeValues 
        { 
            get 
            {
                return _complexNodeValues;
            } 
        }

        public static IKeyValue BuildComplex(string componentId, params string[] complexValues)
        {
            return new KeyValueImpl(componentId, complexValues);
        }

        /// <summary>
        ///     The compare to.
        /// </summary>
        /// <param name="other">
        ///     Key value to compare to
        /// </param>
        /// <returns>
        ///     The <see cref="int" /> .
        /// </returns>
        public virtual int CompareTo(IKeyValue other)
        {
            if (other == null)
            {
                return 1;
            }

            if (this._concept.Equals(other.Concept))
            {
                return string.CompareOrdinal(this._code, other.Code);
            }

            return string.CompareOrdinal(this._concept, other.Concept);
        }

        /// <summary>
        ///     The equals.
        /// </summary>
        /// <param name="obj">
        ///     The obj.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool Equals(object obj)
        {
            var that = obj as IKeyValue;
            if (that != null)
            {              
                return this.Concept.Equals(that.Concept) && this.Code.Equals(that.Code);
            }

            return base.Equals(obj);
        }

        /// <summary>
        ///     The get hash code.
        /// </summary>
        /// <returns> The <see cref="int" /> . </returns>
        public override int GetHashCode()
        {
            return (this._concept + this._code).GetHashCode();
        }

        /// <summary>
        ///     The to string.
        /// </summary>
        /// <returns> The <see cref="string" /> . </returns>
        public override string ToString()
        {
            return _concept + ":" + _code;
        }
    }
}