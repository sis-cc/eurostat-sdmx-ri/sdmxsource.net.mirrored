// -----------------------------------------------------------------------
// <copyright file="ConstrainedKeyValue.cs" company="EUROSTAT">
//   Date Created : 2023-11-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data
{
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;

    public class ConstrainedKeyValue : IConstrainedKeyValue
    {
        private static readonly Dictionary<string, ConstrainedKeyValue> cache = new Dictionary<string, ConstrainedKeyValue>();
        private int hashCode = -1;
        private static readonly long serialVersionUID = 2L;
        private bool removePrefix;
        private IKeyValue kv;

        public string Code => kv.Code;

        public string Concept => kv.Concept;

        public List<IComplexNodeValue> ComplexNodeValues => kv.ComplexNodeValues;

        private ConstrainedKeyValue(string concept, bool removePrefix, params string[] code)
        {
            kv = KeyValueImpl.GetInstance(concept, code);
            this.removePrefix = removePrefix;
        }

        public static ConstrainedKeyValue GetInstance(string id, bool removePrefix, params string[] value)
        {
            if (value != null && value.Length > 1)
            {
                return new ConstrainedKeyValue(id, removePrefix, value);
            }
            if (value == null || value.Length == 0)
            {
                return new ConstrainedKeyValue(id, removePrefix, value);
            }
            string key = id + ":" + value[0] + ":" + removePrefix;
            lock (cache)
            {
                if (!cache.TryGetValue(key, out ConstrainedKeyValue returnKv))
                {
                    returnKv = new ConstrainedKeyValue(id, removePrefix, value);
                    cache[key] = returnKv;
                }
                return returnKv;
            }
        }

        public bool IsRemovePrefix()
        {
            return removePrefix;
        }

        public int CompareTo(IKeyValue that)
        {
            int rt = kv.CompareTo(that);
            if (rt == 0)
            {
                bool removePrefix = false;
                if (that is ConstrainedKeyValue ckv)
                {
                    removePrefix = ckv.IsRemovePrefix();
                }
                if (IsRemovePrefix() && removePrefix)
                {
                    return 1;
                }
                if (!IsRemovePrefix() && removePrefix)
                {
                    return -1;
                }
            }
            return rt;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            if (obj is ConstrainedKeyValue that)
            {
                if (!kv.Equals(that.kv))
                {
                    return false;
                }
                return removePrefix == that.IsRemovePrefix();
            }
            if (obj is IKeyValue)
            {
                if (removePrefix)
                {
                    return false;
                }
                IKeyValue t = (IKeyValue)obj;
                return kv.Equals(t);
            }
            return false;
        }

        public override int GetHashCode()
        {
            if (hashCode == -1)
            {
                if (!removePrefix)
                {
                    hashCode = kv.GetHashCode();
                }
                else
                {
                    hashCode = (Concept + Code + removePrefix).GetHashCode();
                }
            }
            return hashCode;
        }

        public override string ToString()
        {
            return Concept + ":" + Code + ":" + removePrefix;
        }
    }

    // You may need to define the KeyValueImpl and IConstrainedKeyValue interfaces or classes accordingly.

}
