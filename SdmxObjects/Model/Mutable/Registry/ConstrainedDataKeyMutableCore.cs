// -----------------------------------------------------------------------
// <copyright file="ConstrainedDataKeyMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The constrained data key mutable core.
    /// </summary>
    [Serializable]
    public class ConstrainedDataKeyMutableCore : MutableCore, IConstrainedDataKeyMutableObject
    {       
        private IList<IConstrainedKeyValue> _keyValues;
        private IList<IConstrainedKeyValue> _componentValues;
        private string _validFrom;
        private string _validTo;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConstrainedDataKeyMutableCore" /> class.
        /// </summary>
        public ConstrainedDataKeyMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConstrainedDataKey))
        {
            _keyValues = new List<IConstrainedKeyValue>();
            _componentValues = new List<IConstrainedKeyValue>();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM IMMUTABLE OBJECT                 //////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConstrainedDataKeyMutableCore" /> class.
        /// </summary>
        /// <param name="immutable">
        ///     The immutable.
        /// </param>
        public ConstrainedDataKeyMutableCore(IConstrainedDataKey immutable)
            : base(immutable)
        {
            _keyValues = new List<IConstrainedKeyValue>();
            _keyValues.AddAll(immutable.ConstrainedKeyValues);
            

            foreach (IConstrainedKeyValue kv in immutable.ComponentValues)
            {
                _componentValues.Add(kv);
            }
        }

        /// <summary>
        ///     Gets the key values.
        /// </summary>
        public virtual IList<IKeyValue> KeyValues
        {           
            get => _keyValues.Cast<IKeyValue>().ToList();
        }
      
        public virtual IList<IConstrainedKeyValue> ComponentValues
        {
            get => _componentValues;
            set { _componentValues = value; }
        }

        public string ValidFrom
        {
            get => _validFrom;
            set { _validFrom = value; }
        }

        public string ValidTo
        {
            get => _validTo;
            set { _validTo = value; }
        }

        public List<IConstrainedKeyValue> ConstrainedKeyValues => this._keyValues.ToList();

        public void AddComponentValues(IConstrainedKeyValue componentValue)
        {
            if (componentValue != null)
            {
                _componentValues.Add(componentValue);
            }
        }

        /// <summary>
        ///     The add key value.
        /// </summary>
        /// <param name="keyvalue">
        ///     The keyvalue.
        /// </param>
        public virtual void AddKeyValue(IConstrainedKeyValue keyvalue)
        {
            if (keyvalue != null)
            {
                _keyValues.Add(keyvalue);
            }
        }
    
    }
}