// -----------------------------------------------------------------------
// <copyright file="ConstraintDataKeySetMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///     The constraint data key set mutable core.
    /// </summary>
    [Serializable]
    public class ConstraintDataKeySetMutableCore : MutableCore, IConstraintDataKeySetMutableObject
    {
        /// <summary>
        ///     The constrained keys.
        /// </summary>
        private IList<IConstrainedDataKeyMutableObject> _constrainedKeys;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConstraintDataKeySetMutableCore" /> class.
        /// </summary>
        public ConstraintDataKeySetMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConstrainedDataKeyset))
        {
            this._constrainedKeys = new List<IConstrainedDataKeyMutableObject>();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM IMMUTABLE OBJECT                 //////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConstraintDataKeySetMutableCore" /> class.
        /// </summary>
        /// <param name="immutable">
        ///     The immutable.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="immutable"/> is <see langword="null" />.</exception>
        public ConstraintDataKeySetMutableCore(IConstraintDataKeySet immutable)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConstrainedDataKeyset))
        {
            if (immutable == null)
            {
                throw new ArgumentNullException("immutable");
            }

            this._constrainedKeys = new List<IConstrainedDataKeyMutableObject>();

            foreach (IConstrainedDataKey each in immutable.ConstrainedDataKeys)
            {
                this._constrainedKeys.Add(new ConstrainedDataKeyMutableCore(each));
            }
        }

        /// <summary>
        ///     Gets the constrained data keys.
        /// </summary>
        public virtual IList<IConstrainedDataKeyMutableObject> ConstrainedDataKeys
        {
            get
            {
                return this._constrainedKeys;
            }
        }

        /// <summary>
        ///     The add constrained data key.
        /// </summary>
        /// <param name="key">
        ///     The key.
        /// </param>
        public virtual void AddConstrainedDataKey(IConstrainedDataKeyMutableObject key)
        {
            if (key == null)
            {
                this._constrainedKeys = new List<IConstrainedDataKeyMutableObject>();
            }

            this._constrainedKeys.Add(key);
        }
    }
}