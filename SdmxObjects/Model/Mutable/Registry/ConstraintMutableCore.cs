// -----------------------------------------------------------------------
// <copyright file="ConstraintMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///     The constraint mutable core.
    /// </summary>
    /// <typeparam name="T">The generic type</typeparam>
    /// <seealso cref="Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base.MaintainableMutableCore{T}" />
    /// <seealso cref="Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry.IConstraintMutableObject" />
    [Serializable]
    public abstract class ConstraintMutableCore<T> : MaintainableMutableCore<T>, IConstraintMutableObject
        where T : IConstraintObject
    {
        /// <summary>
        ///     The _content constraint attachment.
        /// </summary>
        private IConstraintAttachmentMutableObject _contentConstraintAttachment;

        /// <summary>
        ///     The _excluded metadata keys
        /// </summary>
        private IConstraintDataKeySetMutableObject _excludedMetadataKeys;

        /// <summary>
        ///     The _excluded series keys.
        /// </summary>
        private IConstraintDataKeySetMutableObject _excludedSeriesKeys;

        /// <summary>
        ///     The _included metadata keys
        /// </summary>
        private IConstraintDataKeySetMutableObject _includedMetadataKeys;

        /// <summary>
        ///     The _included series keys.
        /// </summary>
        private IConstraintDataKeySetMutableObject _includedSeriesKeys;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConstraintMutableCore{T}" /> class.
        /// </summary>
        /// <param name="objTarget">The object target.</param>
        /// <exception cref="ArgumentNullException"><paramref name="objTarget"/> is <see langword="null" />.</exception>
        protected ConstraintMutableCore(IConstraintObject objTarget)
            : base(objTarget)
        {
            if (objTarget == null)
            {
                throw new ArgumentNullException("objTarget");
            }

            this._contentConstraintAttachment = null;
            if (objTarget.ConstraintAttachment != null)
            {
                this._contentConstraintAttachment = objTarget.ConstraintAttachment.CreateMutableInstance();
            }

            if (objTarget.IncludedSeriesKeys != null)
            {
                this._includedSeriesKeys = new ConstraintDataKeySetMutableCore(objTarget.IncludedSeriesKeys);
            }

            if (objTarget.ExcludedSeriesKeys != null)
            {
                this._excludedSeriesKeys = new ConstraintDataKeySetMutableCore(objTarget.ExcludedSeriesKeys);
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConstraintMutableCore{T}" /> class.
        /// </summary>
        /// <param name="structureType">The structure type.</param>
        protected ConstraintMutableCore(SdmxStructureType structureType)
            : base(structureType)
        {
            this._contentConstraintAttachment = null;
        }

        /// <summary>
        ///     Gets or sets the constraint attachment.
        /// </summary>
        public virtual IConstraintAttachmentMutableObject ConstraintAttachment
        {
            get
            {
                return this._contentConstraintAttachment;
            }

            set
            {
                this._contentConstraintAttachment = value;
            }
        }

        /// <summary>
        ///     Gets or sets the metadata keys that this constraint defines at ones that either do not have data, or are not
        ///     allowed to have data (depending on IsDefiningActualDataPresent value)
        /// </summary>
        public IConstraintDataKeySetMutableObject ExcludedMetadataKeys
        {
            get
            {
                return this._excludedMetadataKeys;
            }

            set
            {
                this._excludedMetadataKeys = value;
            }
        }

        /// <summary>
        ///     Gets or sets the excluded series keys.
        /// </summary>
        public virtual IConstraintDataKeySetMutableObject ExcludedSeriesKeys
        {
            get
            {
                return this._excludedSeriesKeys;
            }

            set
            {
                this._excludedSeriesKeys = value;
            }
        }

        /// <summary>
        ///     Gets or sets the metadata keys that this constraint defines at ones that either have data, or are allowed to have
        ///     data (depending on IsDefiningActualDataPresent value)
        /// </summary>
        public IConstraintDataKeySetMutableObject IncludedMetadataKeys
        {
            get
            {
                return this._includedMetadataKeys;
            }

            set
            {
                this._includedMetadataKeys = value;
            }
        }

        /// <summary>
        ///     Gets or sets the included series keys.
        /// </summary>
        public virtual IConstraintDataKeySetMutableObject IncludedSeriesKeys
        {
            get
            {
                return this._includedSeriesKeys;
            }

            set
            {
                this._includedSeriesKeys = value;
            }
        }
    }
}