﻿// -----------------------------------------------------------------------
// <copyright file="MetadataTargetRegionMutableObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-03-14
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///     The MetadataTargetRegionMutableObjectCore class
    /// </summary>
    public class MetadataTargetRegionMutableObjectCore : MutableCore, IMetadataTargetRegionMutableObject
    {
        /// <summary>
        ///     The attributes
        /// </summary>
        private readonly IList<IKeyValuesMutable> _attributes = new List<IKeyValuesMutable>();

        /// <summary>
        ///     The include
        /// </summary>
        private readonly bool _include;

        /// <summary>
        ///     The key
        /// </summary>
        private readonly IList<IMetadataTargetKeyValuesMutable> _key = new List<IMetadataTargetKeyValuesMutable>();

        /// <summary>
        ///     The metadata target
        /// </summary>
        private readonly string _metadataTarget;

        /// <summary>
        ///     The report
        /// </summary>
        private readonly string _report;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataTargetRegionMutableObjectCore" /> class.
        /// </summary>
        public MetadataTargetRegionMutableObjectCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataTargetRegion))
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataTargetRegionMutableObjectCore" /> class.
        /// </summary>
        /// <param name="createdFrom">The created from.</param>
        public MetadataTargetRegionMutableObjectCore(IMetadataTargetRegion createdFrom)
            : base(createdFrom)
        {
            this._include = createdFrom.IsInclude;
            this._report = createdFrom.Report;
            this._metadataTarget = createdFrom.MetadataTarget;
            foreach (IMetadataTargetKeyValues currentKv in createdFrom.Key)
            {
                this._key.Add(new MetadataTargetKeyValuesMutableObjectCore(currentKv));
            }

            foreach (IKeyValues currentKv in createdFrom.Attributes)
            {
                this._attributes.Add(new KeyValuesMutableImpl(currentKv));
            }
        }

        /// <summary>
        ///     Gets the attributes restrictions for the metadata target region
        /// </summary>
        public IList<IKeyValuesMutable> Attributes
        {
            get
            {
                return this._attributes;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether the information reported is to be included or excluded
        /// </summary>
        public bool IsInclude
        {
            get
            {
                return this._include;
            }
        }

        /// <summary>
        ///     Gets the key values restrictions for the metadata target region
        /// </summary>
        public IList<IMetadataTargetKeyValuesMutable> Key
        {
            get
            {
                return this._key;
            }
        }

        /// <summary>
        ///     Gets the metadata target.
        /// </summary>
        /// <value>
        ///     The metadata target.
        /// </value>
        public string MetadataTarget
        {
            get
            {
                return this._metadataTarget;
            }
        }

        /// <summary>
        ///     Gets the report.
        /// </summary>
        /// <value>
        ///     The report.
        /// </value>
        public string Report
        {
            get
            {
                return this._report;
            }
        }

        /// <summary>
        ///     Adds the attribute.
        /// </summary>
        /// <param name="attribute">The attribute.</param>
        public void AddAttribute(IKeyValuesMutable attribute)
        {
            this._attributes.Add(attribute);
        }

        /// <summary>
        ///     Adds the key.
        /// </summary>
        /// <param name="key">The key.</param>
        public void AddKey(IMetadataTargetKeyValuesMutable key)
        {
            this._key.Add(key);
        }
    }
}