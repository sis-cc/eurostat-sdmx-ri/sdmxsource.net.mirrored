﻿// -----------------------------------------------------------------------
// <copyright file="DataSetReferenceMutableObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-03-14
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry
{
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    ///     The data set reference class
    /// </summary>
    public class DataSetReferenceMutableObjectCore : MutableCore, IDataSetReferenceMutableObject
    {
        /// <summary>
        ///     The data provider reference
        /// </summary>
        private IStructureReference _dataProviderRef;

        /// <summary>
        ///     The dataset identifier
        /// </summary>
        private string _datasetId;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataSetReferenceMutableObjectCore" /> class.
        /// </summary>
        public DataSetReferenceMutableObjectCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DatasetReference))
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataSetReferenceMutableObjectCore" /> class.
        /// </summary>
        /// <param name="createdFrom">The created from.</param>
        public DataSetReferenceMutableObjectCore(IDataSetReference createdFrom)
            : base(createdFrom)
        {
            if (createdFrom.DataProviderReference != null)
            {
                this._dataProviderRef = new StructureReferenceImpl(createdFrom.DataProviderReference.TargetUrn);
            }

            this._datasetId = createdFrom.DatasetId;
        }

        /// <summary>
        ///     Gets or sets the data provider reference.
        /// </summary>
        /// <value>
        ///     The data provider reference.
        /// </value>
        public IStructureReference DataProviderReference
        {
            get
            {
                return this._dataProviderRef;
            }

            set
            {
                this._dataProviderRef = value;
            }
        }

        /// <summary>
        ///     Gets or sets the dataset identifier.
        /// </summary>
        /// <value>
        ///     The dataset identifier.
        /// </value>
        public string DatasetId
        {
            get
            {
                return this._datasetId;
            }

            set
            {
                this._datasetId = value;
            }
        }
    }
}