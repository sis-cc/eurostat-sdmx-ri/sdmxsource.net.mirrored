// -----------------------------------------------------------------------
// <copyright file="SubscriptionMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry;

    /// <summary>
    ///     The subscription mutable core.
    /// </summary>
    [Serializable]
    public class SubscriptionMutableCore : MaintainableMutableCore<ISubscriptionObject>, ISubscriptionMutableObject
    {
        /// <summary>
        ///     The http post to.
        /// </summary>
        private readonly IList<string> _httpPostTo;

        /// <summary>
        ///     The mail to.
        /// </summary>
        private readonly IList<string> _mailTo;

        /// <summary>
        ///     The owner.
        /// </summary>
        private IStructureReference _owner;

        /// <summary>
        ///     The references.
        /// </summary>
        private IList<IStructureReference> _references;

        /// <summary>
        ///     The subscription type.
        /// </summary>
        private SubscriptionEnumType _subscriptionType;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SubscriptionMutableCore" /> class.
        /// </summary>
        public SubscriptionMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Subscription))
        {
            this._mailTo = new List<string>();
            this._httpPostTo = new List<string>();
            this._references = new List<IStructureReference>();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SubscriptionMutableCore" /> class.
        /// </summary>
        /// <param name="subscription">
        ///     The isubscription.
        /// </param>
        public SubscriptionMutableCore(ISubscriptionObject subscription)
            : base(subscription)
        {
            this._mailTo = new List<string>();
            this._httpPostTo = new List<string>();
            this._references = new List<IStructureReference>();
            if (subscription.HTTPPostTo != null)
            {
                this._httpPostTo = new List<string>(subscription.HTTPPostTo);
            }

            if (subscription.MailTo != null)
            {
                this._mailTo = new List<string>(subscription.MailTo);
            }

            if (subscription.References != null)
            {
                foreach (IStructureReference structureReference in subscription.References)
                {
                    this._references.Add(structureReference.CreateCopy());
                }
            }

            this.StructureType = subscription.StructureType;
            this._owner = subscription.Owner.CreateMutableInstance();
            this._subscriptionType = subscription.SubscriptionType;
        }

        /// <summary>
        ///     Gets the http post to.
        /// </summary>
        public virtual IList<string> HttpPostTo
        {
            get
            {
                return this._httpPostTo;
            }
        }

        /// <summary>
        ///     Gets the immutable instance.
        /// </summary>
        public override ISubscriptionObject ImmutableInstance
        {
            get
            {
                return new SubscriptionObjectCore(this);
            }
        }

        /// <summary>
        ///     Gets the mail to.
        /// </summary>
        public virtual IList<string> MailTo
        {
            get
            {
                return this._mailTo;
            }
        }

        /// <summary>
        ///     Gets or sets the owner.
        /// </summary>
        public virtual IStructureReference Owner
        {
            get
            {
                return this._owner;
            }

            set
            {
                this._owner = value;
            }
        }

        /// <summary>
        ///     Gets the references.
        /// </summary>
        public virtual IList<IStructureReference> References
        {
            get
            {
                return new ReadOnlyCollection<IStructureReference>(this._references);
            }
        }

        /// <summary>
        ///     Gets or sets the subscription type.
        /// </summary>
        public virtual SubscriptionEnumType SubscriptionType
        {
            get
            {
                return this._subscriptionType;
            }

            set
            {
                this._subscriptionType = value;
            }
        }

        /// <summary>
        ///     The add reference.
        /// </summary>
        /// <param name="reference">
        ///     The reference.
        /// </param>
        public virtual void AddReference(IStructureReference reference)
        {
            if (this._references == null)
            {
                this._references = new List<IStructureReference>();
            }

            this._references.Add(reference);
        }
    }
}