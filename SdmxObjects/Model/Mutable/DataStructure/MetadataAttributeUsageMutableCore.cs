// -----------------------------------------------------------------------
// <copyright file="MetadataAttributeUsageMutableCore.cs" company="EUROSTAT">
//   Date Created : 2021-10-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure
{
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    /// The base implementation of the <see cref="IMetadataAttributeUsageMutableObject"/> interface.
    /// </summary>
    public class MetadataAttributeUsageMutableCore : AnnotableMutableCore, IMetadataAttributeUsageMutableObject
    {
        private string _metadataAttributeId;
	    private AttributeAttachmentLevel _attachmentLevel;
        private string _attachmentGroup;
        private IList<string> _dimensionReference = new List<string>();

        /// <summary>
        /// Initializes a new instance of the <see cref="MetadataAttributeUsageMutableCore"/> class
        /// </summary>
        public MetadataAttributeUsageMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataAttributeUsage))
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MetadataAttributeUsageMutableCore"/> class
        /// from an immutable object.
        /// </summary>
        /// <param name="immutableObject">The immutable object to initalize the instance from.</param>
        public MetadataAttributeUsageMutableCore(IMetadataAttributeUsage immutableObject)
            : base(immutableObject)
        {
            this._attachmentLevel = immutableObject.AttachmentLevel;
            this._attachmentGroup = immutableObject.AttachmentGroup;
            this._dimensionReference = immutableObject.DimensionReferences;
            this._metadataAttributeId = immutableObject.MetadataAttributeReference;
        }

        /// <inheritdoc/>
        public string MetadataAttributeReference
        {
            get => this._metadataAttributeId;
            set => this._metadataAttributeId = value;
        }

        /// <inheritdoc/>
        public IList<string> DimensionReferences
        {
            get => this._dimensionReference;
            set => this._dimensionReference = value;
        }

        /// <inheritdoc/>
        public string AttachmentGroup
        {
            get => this._attachmentGroup;
            set => this._attachmentGroup = value;
        }

        /// <inheritdoc/>
        public string PrimaryMeasureReference
        {
            get => null;
            set => throw new SdmxSemmanticException("Primary Measure reference has been dropped in SDMX 3.0.0");
        }

        /// <inheritdoc/>
        public AttributeAttachmentLevel AttachmentLevel
        {
            get => this._attachmentLevel;
            set => this._attachmentLevel = value;
        }
    }
}
