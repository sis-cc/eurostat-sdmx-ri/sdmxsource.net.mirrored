// -----------------------------------------------------------------------
// <copyright file="MeasureListMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///     The measure list mutable core.
    /// </summary>
    [Serializable]
    public class MeasureListMutableCore : IdentifiableMutableCore, IMeasureListMutableObject
    {
        /// <summary>
        ///     The iprimary measure mutable.
        /// </summary>
        private IPrimaryMeasureMutableObject _primaryMeasureMutableObject;

        private readonly List<IMeasureMutableObject> _measureMutables = new List<IMeasureMutableObject>();

        /// <summary>
        ///     Initializes a new instance of the <see cref="MeasureListMutableCore" /> class.
        /// </summary>
        public MeasureListMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MeasureDescriptor))
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MeasureListMutableCore" /> class.
        /// </summary>
        /// <param name="objTarget">
        ///     The obj target.
        /// </param>
        public MeasureListMutableCore(IMeasureList objTarget)
            : base(objTarget)
        {
            if (objTarget.PrimaryMeasure != null)
            {
                this._primaryMeasureMutableObject = new PrimaryMeasureMutableCore(objTarget.PrimaryMeasure);
            }
            if (objTarget.Measures != null)
            {
                List<MeasureMutableCore> mutableMeasures = objTarget.Measures.Select(m => new MeasureMutableCore(m)).ToList();
                this._measureMutables.AddRange(mutableMeasures);
            }
        }

        /// <summary>
        ///     Gets or sets the primary measure.
        ///     Obsolete in SDMX v3.0.0 schema
        /// </summary>
        public virtual IPrimaryMeasureMutableObject PrimaryMeasure
        {
            get
            {
                return this._primaryMeasureMutableObject;
            }

            set
            {
                this._primaryMeasureMutableObject = value;
            }
        }

        /// <summary>
        /// Gets or sets the SDMX measures
        /// Introduced in SDMX v3.0.0 schema
        /// </summary>
        public virtual IList<IMeasureMutableObject> Measures
        {
            get { return this._measureMutables; }
            set
            {
                this._measureMutables.AddRange(value);
            }
        }

        /// <inheritdoc/>
        public void AddMeasure(IMeasureMutableObject measure)
        {
            this._measureMutables.Add(measure);
        }
    }
}