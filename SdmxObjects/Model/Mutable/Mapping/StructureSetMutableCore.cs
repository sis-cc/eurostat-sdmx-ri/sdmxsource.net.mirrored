// -----------------------------------------------------------------------
// <copyright file="StructureSetMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Mapping
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Mapping;

    /// <summary>
    ///     The structure set mutable core.
    /// </summary>
    [Serializable]
    public class StructureSetMutableCore : MaintainableMutableCore<IStructureSetObject>, IStructureSetMutableObject
    {
        /// <summary>
        ///     The category scheme map list.
        /// </summary>
        private IList<ICategorySchemeMapMutableObject> _categorySchemeMapList;

        /// <summary>
        ///     The codelist map list.
        /// </summary>
        private IList<ICodelistMapMutableObject> _codelistMapList;

        /// <summary>
        ///     The concept scheme map list.
        /// </summary>
        private IList<IConceptSchemeMapMutableObject> _conceptSchemeMapList;

        /// <summary>
        ///     The organisation scheme map list.
        /// </summary>
        private IList<IOrganisationSchemeMapMutableObject> _organisationSchemeMapList;

        /// <summary>
        ///     The related structures.
        /// </summary>
        private IRelatedStructuresMutableObject _relatedStructures;

        /// <summary>
        ///     The structure map list.
        /// </summary>
        private IList<IStructureMapMutableObject> _structureMapList;

        /// <summary>
        ///     Initializes a new instance of the <see cref="StructureSetMutableCore" /> class.
        ///     Constructor.
        /// </summary>
        public StructureSetMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.StructureSet))
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="StructureSetMutableCore" /> class.
        ///     Copy constructor
        /// </summary>
        /// <param name="objTarget">
        ///     immutable variant to copy from
        /// </param>
        public StructureSetMutableCore(IStructureSetObject objTarget)
            : base(objTarget)
        {
            if (objTarget.RelatedStructures != null)
            {
                this._relatedStructures = new RelatedStructuresMutableCore(objTarget.RelatedStructures);
            }

            foreach (IStructureMapObject each in objTarget.StructureMapList)
            {
                this.AddStructureMap(new StructureMapMutableCore(each));
            }

            foreach (ICodelistMapObject each0 in objTarget.CodelistMapList)
            {
                this.AddCodelistMap(new CodelistMapMutableCore(each0));
            }

            foreach (ICategorySchemeMapObject each1 in objTarget.CategorySchemeMapList)
            {
                this.AddCategorySchemeMap(new CategorySchemeMapMutableCore(each1));
            }

            foreach (IConceptSchemeMapObject each2 in objTarget.ConceptSchemeMapList)
            {
                this.AddConceptSchemeMap(new ConceptSchemeMapMutableCore(each2));
            }

            foreach (IOrganisationSchemeMapObject each3 in objTarget.OrganisationSchemeMapList)
            {
                this.AddOrganisationSchemeMap(new OrganisationSchemeMapMutableCore(each3));
            }
        }

        /// <summary>
        ///     Gets the category scheme map list.
        /// </summary>
        public IList<ICategorySchemeMapMutableObject> CategorySchemeMapList
        {
            get
            {
                if (this._categorySchemeMapList == null)
                {
                    this._categorySchemeMapList = new List<ICategorySchemeMapMutableObject>();
                }

                return this._categorySchemeMapList;
            }
        }

        /// <summary>
        ///     Gets the codelist map list.
        /// </summary>
        public IList<ICodelistMapMutableObject> CodelistMapList
        {
            get
            {
                return this._codelistMapList ?? (this._codelistMapList = new List<ICodelistMapMutableObject>());
            }
        }

        /// <summary>
        ///     Gets the concept scheme map list.
        /// </summary>
        public IList<IConceptSchemeMapMutableObject> ConceptSchemeMapList
        {
            get
            {
                if (this._conceptSchemeMapList == null)
                {
                    this._conceptSchemeMapList = new List<IConceptSchemeMapMutableObject>();
                }

                return this._conceptSchemeMapList;
            }
        }

        /// <summary>
        ///     Create immutable variant
        /// </summary>
        public override IStructureSetObject ImmutableInstance
        {
            get
            {
                return new StructureSetObjectCore(this);
            }
        }

        /// <summary>
        ///     Gets the organisation scheme map list.
        /// </summary>
        public IList<IOrganisationSchemeMapMutableObject> OrganisationSchemeMapList
        {
            get
            {
                return this._organisationSchemeMapList
                       ?? (this._organisationSchemeMapList = new List<IOrganisationSchemeMapMutableObject>());
            }
        }

        /// <summary>
        ///     Gets or sets the related structures.
        /// </summary>
        public IRelatedStructuresMutableObject RelatedStructures
        {
            get
            {
                return this._relatedStructures;
            }

            set
            {
                this._relatedStructures = value;
            }
        }

        /// <summary>
        ///     Gets the structure map list.
        /// </summary>
        public IList<IStructureMapMutableObject> StructureMapList
        {
            get
            {
                if (this._structureMapList == null)
                {
                    this._structureMapList = new List<IStructureMapMutableObject>();
                }

                return this._structureMapList;
            }
        }

        /// <summary>
        ///     The add category scheme map.
        /// </summary>
        /// <param name="categorySchemeMap">
        ///     The category scheme map.
        /// </param>
        public void AddCategorySchemeMap(ICategorySchemeMapMutableObject categorySchemeMap)
        {
            this.CategorySchemeMapList.Add(categorySchemeMap);
        }

        /// <summary>
        ///     The add codelist map.
        /// </summary>
        /// <param name="codelistMap">
        ///     The codelist map.
        /// </param>
        public void AddCodelistMap(ICodelistMapMutableObject codelistMap)
        {
            this.CodelistMapList.Add(codelistMap);
        }

        /// <summary>
        ///     The add concept scheme map.
        /// </summary>
        /// <param name="conceptSchemeMap">
        ///     The concept scheme map.
        /// </param>
        public void AddConceptSchemeMap(IConceptSchemeMapMutableObject conceptSchemeMap)
        {
            this.ConceptSchemeMapList.Add(conceptSchemeMap);
        }

        /// <summary>
        ///     The add organisation scheme map.
        /// </summary>
        /// <param name="organisationSchemeMap">
        ///     The organisation scheme map.
        /// </param>
        public void AddOrganisationSchemeMap(IOrganisationSchemeMapMutableObject organisationSchemeMap)
        {
            this.OrganisationSchemeMapList.Add(organisationSchemeMap);
        }

        /// <summary>
        ///     The add structure map.
        /// </summary>
        /// <param name="structureMap">
        ///     The structure map.
        /// </param>
        public void AddStructureMap(IStructureMapMutableObject structureMap)
        {
            this.StructureMapList.Add(structureMap);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////ADDERS                                  //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
    }
}