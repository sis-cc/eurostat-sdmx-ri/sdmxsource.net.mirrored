// -----------------------------------------------------------------------
// <copyright file="CodelistInheritanceRuleMutableCore.cs" company="EUROSTAT">
//   Date Created : 2021-10-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Codelist
{
    using System;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;

    /// <summary>
    /// The base implementation of the <see cref="ICodelistInheritanceRuleMutableObject"/> interface
    /// </summary>
    public class CodelistInheritanceRuleMutableCore : MutableCore, ICodelistInheritanceRuleMutableObject
    {
        private IStructureReference _codelist;
        private ICodeSelectionMutableObject _codeSelection;
        private string _prefix;

        /// <summary>
        /// Initializes a new instance of the <see cref="CodelistInheritanceRuleMutableCore"/> class,
        /// from the corresponding structure type.
        /// </summary>
        public CodelistInheritanceRuleMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodelistExtension))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CodelistInheritanceRuleMutableCore"/> class,
        /// from an immutable object.
        /// </summary>
        /// <param name="immutableObject">The immutable object to initialize from.</param>
        public CodelistInheritanceRuleMutableCore(ICodelistInheritanceRule immutableObject) 
            : base(immutableObject)
        {
            if (immutableObject == null)
            {
                throw new ArgumentNullException(nameof(immutableObject));
            }

            // following existing pattern but maybe it would be better to make
            // those checks with Objects?
            if (immutableObject.CodelistRef != null)
            {
                this._codelist = immutableObject.CodelistRef.CreateMutableInstance();
            }
            else
            {
                // TODO ExceptionCode
                throw new SdmxSemmanticException("Reference to extended codelist is missing");
            }
            if (immutableObject.CodeSelection != null)
            {
                this._codeSelection = new CodeSelectionMutableCore(immutableObject.CodeSelection);
            }
            else
            {
                this._codeSelection = null;
            }
            this._prefix = immutableObject.Prefix;
        }

        /// <inheritdoc/>
        public IStructureReference CodelistRef
        {
            get => this._codelist;
            set => this._codelist = value;
        }

        /// <inheritdoc/>
        public string Prefix
        {
            get => this._prefix;
            set => this._prefix = value;
        }

        /// <inheritdoc/>
        public ICodeSelectionMutableObject CodeSelection
        {
            get => this._codeSelection;
            set => this._codeSelection = value;
        }
    }
}
