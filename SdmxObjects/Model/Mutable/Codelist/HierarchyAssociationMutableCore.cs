// -----------------------------------------------------------------------
// <copyright file="HierarchyAssociationMutableCore.cs" company="EUROSTAT">
//   Date Created : 2024-03-05
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist
{
    [Serializable]
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public class HierarchyAssociationMutableCore : MaintainableMutableCore<IHierarchyAssociation>, IHierarchyAssociationMutableObject
    {
        private static readonly long serialVersionUID = 1L;
        private IStructureReference hierarchyRef;
        private IStructureReference maintainableRef;
        private IStructureReference contextRef;

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public HierarchyAssociationMutableCore() : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.HierarchyAssociation))
        {
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="bean"></param>
        public HierarchyAssociationMutableCore(IHierarchyAssociation bean) : base(bean)
        {
            this.hierarchyRef = new StructureReferenceImpl(bean.HierarchyRef.TargetUrn);
            this.maintainableRef = new StructureReferenceImpl(bean.LinkedStructureRef.TargetUrn);
            if (bean.ContextStructureRef != null)
            {
                this.contextRef = new StructureReferenceImpl(bean.ContextStructureRef.TargetUrn);
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public override IHierarchyAssociation ImmutableInstance
        {
            get
            {
                return new HierarchyAssociationCore(this);
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public IStructureReference HierarchyRef
        {
             get 
             { 
                return hierarchyRef; 
             }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="refBean"></param>
        /// <returns></returns>
        public IHierarchyAssociationMutableObject SetHierarchyRef(IStructureReference refBean)
        {
            this.hierarchyRef = refBean;
            return this;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public IStructureReference LinkedStructureRef
        {
            get
            {
                return maintainableRef;
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="refBean"></param>
        /// <returns></returns>
        public IHierarchyAssociationMutableObject SetLinkedStructure(IStructureReference refBean)
        {
            this.maintainableRef = refBean;
            return this;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <returns></returns>
        public IStructureReference ContextStructureRef
        {
            get
            {
                return contextRef;
            }
        }
        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="refBean"></param>
        /// <returns></returns>
        public IHierarchyAssociationMutableObject SetContextStructure(IStructureReference refBean)
        {
            this.contextRef = refBean;
            return this;
        }
    }

}
