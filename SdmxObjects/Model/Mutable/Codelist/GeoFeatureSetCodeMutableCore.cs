using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist
{
    /// <summary>
    ///    The geo code mutable core.
    /// </summary>
    public class GeoFeatureSetCodeMutableCore : ItemMutableCore, IGeoFeatureSetCodeMutableObject
    {
        private string _geoGeoFeatureSet;

        /// <summary>
        ///     The parent code.
        /// </summary>
        private string _parentCode;

        /// <summary>
        ///     The _code type.
        /// </summary>
        private static readonly SdmxStructureType _codeType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Code);

        /// <summary>
        ///     Initializes a new instance of the <see cref="GeoFeatureSetCodeMutableCore" /> class.
        /// </summary>
        public GeoFeatureSetCodeMutableCore()
            : base(_codeType)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="GeoFeatureSetCodeMutableCore" /> class.
        /// </summary>
        /// <param name="objTarget">
        ///     The obj target.
        /// </param>
        public GeoFeatureSetCodeMutableCore(IGeoFeatureSetCode objTarget)
            : base(objTarget)
        {
            _parentCode = objTarget.ParentCode;
            _geoGeoFeatureSet = objTarget.GeoFeatureSet;
        }


        /// <summary>
        ///     Initializes a new instance of the <see cref="GeoFeatureSetCodeMutableCore" /> class.
        /// </summary>
        /// <param name="reader">
        ///     The reader.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public GeoFeatureSetCodeMutableCore(ISdmxReader reader)
            : base(_codeType)
        {
            this.BuildIdentifiableAttributes(reader);

            reader.MoveNextElement();
            while (this.ProcessReader(reader))
            {
                reader.MoveNextElement();
            }
        }

        /// <summary>
        ///     Gets or sets the parent code.
        /// </summary>
        public string ParentCode
        {
            get
            {
                return this._parentCode;
            }

            set
            {
                this._parentCode = value;
            }
        }

        public string GeoFeatureSet 
        { 
            get => _geoGeoFeatureSet;
            set => _geoGeoFeatureSet = value;
        }

        /// <summary>
        ///     The process reader.
        /// </summary>
        /// <param name="reader">
        ///     The reader.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        private bool ProcessReader(ISdmxReader reader)
        {
            if (this.ProcessReaderNameable(reader))
            {
                return true;
            }

            if (reader.CurrentElement.Equals("Parent"))
            {
                reader.MoveNextElement();
                if (!reader.CurrentElement.Equals("Ref"))
                {
                    throw new SdmxSemmanticException("Expected 'Ref' as a child node of Parent");
                }

                this._parentCode = reader.GetAttributeValue("id", true);
                return true;
            }

            return false;
        }
    }
}
