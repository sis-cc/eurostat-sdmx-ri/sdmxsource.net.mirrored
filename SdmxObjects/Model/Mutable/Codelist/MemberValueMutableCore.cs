// -----------------------------------------------------------------------
// <copyright file="MemberValueMutableCore.cs" company="EUROSTAT">
//   Date Created : 2021-10-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist
{
    using System;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;

    /// <summary>
    /// The base implementation of the <see cref="IMemberValue"/> interface
    /// </summary>
    public class MemberValueMutableCore : IMemberValueMutableObject
    {
        private CascadeSelection _cascade;
        private string _value;

        /// <summary>
        /// Initializes a new instance of the <see cref="MemberValueMutableCore"/> class
        /// </summary>
        public MemberValueMutableCore()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MemberValueMutableCore"/> class,
        /// from an immutable object.
        /// </summary>
        /// <param name="immutableObj">The immutable object to initialize from.</param>
        public MemberValueMutableCore(IMemberValue immutableObj)
        {
            if (immutableObj == null)
            {
                throw new ArgumentNullException(nameof(immutableObj));
            }

            this._cascade = immutableObj.Cascade;
            this._value = immutableObj.Value;
        }

        /// <inheritdoc/>
        public CascadeSelection Cascade
        {
            get => this._cascade;
            set => this._cascade = value;
        }

        /// <inheritdoc/>
        public string Value
        {
            get => this._value;
            set => this._value = value;
        }
    }
}
