// -----------------------------------------------------------------------
// <copyright file="HierarchyMutableCoreV30.cs" company="EUROSTAT">
//   Date Created : 2024-03-05
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Reference;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Reference;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.Util.Date;
using Org.Sdmxsource.Util;

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    [Serializable]
    public class HierarchyMutableCoreV30: MaintainableMutableCore<IHierarchyV30>, IHierarchyMutableObjectV30
    {
        private List<ICodeRefMutableObject> codeRefs = new List<ICodeRefMutableObject>();
        private ILevelMutableObject level;
        private bool hasFormalLevels;

        public HierarchyMutableCoreV30() : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.HierarchyV30))
        {
        }

        public HierarchyMutableCoreV30(IHierarchyV30 bean) : base(bean)
        {
            if (bean.HierarchicalCodeObjects != null)
            {
                codeRefs = bean.HierarchicalCodeObjects.ToList()
                            .Select(currentBean => new CodeRefMutableCore(currentBean))
                            .Cast<ICodeRefMutableObject>().ToList();
            }

            if (bean.Level != null)
            {
                level = new LevelMutableCore(bean.Level);
            }

            hasFormalLevels = bean.HasFormalLevels();
        }

        public override IHierarchyV30 ImmutableInstance
        {
            get
            {
                return new HierarchyCore30(this);
            }
        }

        public bool IsFormalLevels()
        {
            return hasFormalLevels;
        }

        public void SetFormalLevels(bool value)
        {
            hasFormalLevels = value;
        }

        public ILevelMutableObject GetChildLevel()
        {
            return level;
        }

        public void SetChildLevel(ILevelMutableObject level)
        {
            this.level = level;
        }

        public void SetHierarchicalCodeBeans(List<ICodeRefMutableObject> codeRefs)
        {
            this.codeRefs = codeRefs;
        }

        public void AddHierarchicalCodeBean(ICodeRefMutableObject codeRef)
        {
            codeRefs.Add(codeRef);
        }

        public List<ICodeRefMutableObject> GetHierarchicalCodeBeans()
        {
            return codeRefs;
        }

        public IHierarchyMutableObjectV30 StripItems(DateTime aDate)
        {
            List<ICodeRefMutableObject> retVal = EvaluateCodeRefs(GetHierarchicalCodeBeans(), aDate);
            SetHierarchicalCodeBeans(retVal);
            return this;
        }

        private List<ICodeRefMutableObject> EvaluateCodeRefs(IList<ICodeRefMutableObject> hierarchicalCodeBeans, DateTime aDate)
        {
            List<ICodeRefMutableObject> retVal = new List<ICodeRefMutableObject>();
            foreach (ICodeRefMutableObject aCodeRef in hierarchicalCodeBeans)
            {
                if (aCodeRef.ValidFrom != null && aCodeRef.ValidTo != null)
                {
                    var periodCore = new SdmxPeriodCore(aCodeRef.ValidFrom.ToString(), aCodeRef.ValidTo.ToString());
                    if (!periodCore.IsInPeriod(aDate))
                    {
                        continue;
                    }
                }

                retVal.Add(aCodeRef);
                if (ObjectUtil.ValidCollection(aCodeRef.CodeRefs))
                {
                    foreach (var item in EvaluateCodeRefs(aCodeRef.CodeRefs, aDate))
                    {
                        aCodeRef.AddCodeRef(item);
                    }
                }
            }
            return retVal;
        }
    }
}
