// -----------------------------------------------------------------------
// <copyright file="GeoGridCodeMutableCore.cs" company="EUROSTAT">
//   Date Created : 2024-03-13
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist
{
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///    The geo code mutable core.
    /// </summary>
    public class GeoGridCodeMutableCore : ItemMutableCore, IGeoGridCodeMutableObject
    {      
        /// <summary>
        ///     The Geo Cell
        /// </summary>
        private string _geoCell;

        /// <summary>
        ///     The parent code.
        /// </summary>
        private string _parentCode;

        /// <summary>
        ///     The _code type.
        /// </summary>
        private static readonly SdmxStructureType _codeType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Code);

        /// <summary>
        ///   Initializes a new instance of the <see cref="GeoGridCodeMutableCore" /> class.
        /// </summary>
        public GeoGridCodeMutableCore()
        : base(_codeType)
        {
        }

        /// <summary>
        ///    Initializes a new instance of the <see cref="GeoGridCodeMutableCore" /> class.
        /// </summary>
        /// <param name="obj"></param>
        public GeoGridCodeMutableCore(IGeoGridCode obj)
        : base(obj)
        {
            _geoCell = obj.GeoCell;
            _parentCode = obj.ParentCode;
        }

        /// <summary>
        ///    Initializes a new instance of the <see cref="GeoGridCodeMutableCore" /> class.
        /// </summary>
        /// <param name="reader"></param>
        public GeoGridCodeMutableCore(ISdmxReader reader)
        : base(_codeType)
        {
            this.BuildIdentifiableAttributes(reader);

            reader.MoveNextElement();
            while (this.ProcessReader(reader))
            {
                reader.MoveNextElement();
            }
        }

        /// <summary>
        ///     Gets or sets the Geo Cell.
        /// </summary>
        public string GeoCell 
        {
            set => _geoCell = value;
            get => _geoCell;
        }

        /// <summary>
        ///     Gets or sets the parent code.
        /// </summary>
        public string ParentCode
        {
            get => _parentCode;           
            set => _parentCode = value;         
        }

        /// <summary>
        ///   The process reader.
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        /// <exception cref="SdmxSemmanticException"></exception>
        private bool ProcessReader(ISdmxReader reader)
        {
            if (this.ProcessReaderNameable(reader))
            {
                return true;
            }

            if (reader.CurrentElement.Equals("Parent"))
            {
                reader.MoveNextElement();
                if (!reader.CurrentElement.Equals("Ref"))
                {
                    throw new SdmxSemmanticException("Expected 'Ref' as a child node of Parent");
                }

                _parentCode = reader.GetAttributeValue("id", true);
                return true;
            }

            return false;
        }

    }
}
