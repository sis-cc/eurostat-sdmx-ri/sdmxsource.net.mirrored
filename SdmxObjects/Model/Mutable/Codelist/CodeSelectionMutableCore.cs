// -----------------------------------------------------------------------
// <copyright file="CodeSelectionMutableCore.cs" company="EUROSTAT">
//   Date Created : 2021-10-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;

    /// <summary>
    /// The base implementation of the <see cref="ICodeSelectionMutableObject"/> interface
    /// </summary>
    public class CodeSelectionMutableCore : ICodeSelectionMutableObject
    {
        private readonly List<IMemberValueMutableObject> _memberValues = new List<IMemberValueMutableObject>();
        private bool _isInclusive;

        /// <summary>
        /// Defualt constructor for <see cref="CodeSelectionMutableCore"/> class.
        /// </summary>
        public CodeSelectionMutableCore()
        {
        }

        /// <summary>
        /// Initalizes a new instance of the <see cref="CodeSelectionMutableCore"/> class,
        /// from an immutable object.
        /// </summary>
        /// <param name="immutableObj">The immutable object to initialize from.</param>
        public CodeSelectionMutableCore(ICodeSelection immutableObj)
        {
            if (immutableObj == null)
            {
                throw new ArgumentNullException(nameof(immutableObj));
            }

            this._memberValues.AddRange(immutableObj.MemberValues.Select(v => new MemberValueMutableCore(v)));
            this._isInclusive = immutableObj.IsInclusive;
        }

        /// <inheritdoc/>
        public bool IsInclusive
        {
            get => this._isInclusive;
            set => this._isInclusive = value;
        }

        /// <inheritdoc/>
        public IList<IMemberValueMutableObject> MemberValues => this._memberValues;
    }
}
