// -----------------------------------------------------------------------
// <copyright file="SentinelValueMutableCore.cs" company="EUROSTAT">
//   Date Created : 2021-10-19
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base
{
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    /// The base implementation of the <see cref="ISentinelValueMutableObject"/> interface
    /// </summary>
    public class SentinelValueMutableCore : MutableCore, ISentinelValueMutableObject
    {
        private IList<ITextTypeWrapperMutableObject> _names;
        private IList<ITextTypeWrapperMutableObject> _descriptions;
        private string _value;

        /// <summary>
        /// Initializes a new instance of the <see cref="SentinelValueMutableCore"/> class.
        /// </summary>
        public SentinelValueMutableCore()
            : base(Api.Constants.SdmxStructureType.GetFromEnum(Api.Constants.SdmxStructureEnumType.SentinelValue))
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SentinelValueMutableCore"/> class
        /// from a an immutable object
        /// </summary>
        /// <param name="immutable">The immutable object to intialize the instance from.</param>
        public SentinelValueMutableCore(ISentinelValue immutable)
            : base(immutable)
        {
            if (immutable.Names != null)
            {
                this._names = new List<ITextTypeWrapperMutableObject>();
                foreach (ITextTypeWrapper currentTextType in immutable.Names)
                {
                    this._names.Add(new TextTypeWrapperMutableCore(currentTextType));
                }
            }
            if (immutable.Descriptions != null)
            {
                this._descriptions = new List<ITextTypeWrapperMutableObject>();
                foreach (ITextTypeWrapper currentTextType in immutable.Descriptions)
                {
                    this._descriptions.Add(new TextTypeWrapperMutableCore(currentTextType));
                }
            }

            this._value = immutable.Value;
        }

        /// <inheritdoc/>
        public string Value
        {
            get => this._value;
            set => this._value = value;
        }

        /// <inheritdoc/>
        public IList<ITextTypeWrapperMutableObject> Names
        {
            get => this._names;
            set => this._names = value;
        }

        /// <inheritdoc/>
        public IList<ITextTypeWrapperMutableObject> Descriptions
        {
            get => this._descriptions;
            set => this._descriptions = value;
        }

        /// <summary>
        /// Add a description to the list of descriptions
        /// </summary>
        /// <param name="locale">The locale</param>
        /// <param name="name">The name</param>
        public void AddDescription(string locale, string name)
        {
            if (this._descriptions == null)
            {
                this._descriptions = new List<ITextTypeWrapperMutableObject>();
            }
            foreach (ITextTypeWrapperMutableObject currentTT in this._descriptions)
            {
                if (currentTT.Locale.Equals(locale))
                {
                    currentTT.Value = name;
                    return;
                }
            }
            ITextTypeWrapperMutableObject tt = new TextTypeWrapperMutableCore();
            tt.Locale = locale;
            tt.Value = name;
            this._descriptions.Add(tt);
        }

        /// <inheritdoc/>
        public void AddName(string locale, string name)
        {
            if (this._names == null)
            {
                this._names = new List<ITextTypeWrapperMutableObject>();
            }
            foreach (ITextTypeWrapperMutableObject currentTT in this._names)
            {
                if (currentTT.Locale.Equals(locale))
                {
                    currentTT.Value = name;
                    return;
                }
            }
            ITextTypeWrapperMutableObject tt = new TextTypeWrapperMutableCore();
            tt.Locale = locale;
            tt.Value = name;
            this._names.Add(tt);
        }
    }
}
