// -----------------------------------------------------------------------
// <copyright file="AnnotationUrlMutableCore.cs" company="EUROSTAT">
//   Date Created : 2022-03-18
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    ///     The mutable implementation of the annotation url.
    /// </summary>
    [Serializable]
    public class AnnotationUrlMutableCore : MutableCore, IAnnotationUrlMutableObject
    {
        /// <summary>
        ///     The locale.
        /// </summary>
        private string _locale;

        /// <summary>
        ///     The valueren.
        /// </summary>
        private string _uri;

        /// <summary>
        ///     Initializes a new instance of the <see cref="AnnotableMutableCore"/> class.
        /// </summary>
        public AnnotationUrlMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.AnnotationUrl))
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AnnotableMutableCore"/> class
        ///     with no localisation.
        /// </summary>
        /// <param name="uri"></param>
        public AnnotationUrlMutableCore(string uri)
            : this(null, uri)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AnnotableMutableCore"/> class.
        /// </summary>
        /// <param name="locale">The locale</param>
        /// <param name="uri">The uri</param>
        public AnnotationUrlMutableCore(string locale, string uri)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.AnnotationUrl))
        {
            this._locale = locale;
            this._uri = uri;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AnnotableMutableCore"/> class.
        /// </summary>
        /// <param name="annotationURL">The annotation url.</param>
        public AnnotationUrlMutableCore(IAnnotationURL annotationURL)
            : base(annotationURL)
        {
            this._locale = annotationURL.Locale;
            if (annotationURL.Uri != null)
            {
                this._uri = annotationURL.Uri.ToString();
            }
        }

        /// <summary>
        ///     Gets or sets the locale.
        /// </summary>
        public virtual string Locale
        {
            get
            {
                return this._locale;
            }

            set
            {
                this._locale = value;
            }
        }

        /// <summary>
        ///     Gets or sets the uri.
        /// </summary>
        public virtual string Uri
        {
            get
            {
                return this._uri;
            }

            set
            {
                this._uri = value;
            }
        }
    }
}
