// -----------------------------------------------------------------------
// <copyright file="ComponentMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base
{
    using System;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

    /// <summary>
    ///     The component mutable core.
    /// </summary>
    [Serializable]
    public abstract class ComponentMutableCore : IdentifiableMutableCore, IComponentMutableObject
    {
        /// <summary>
        ///     The concept ref.
        /// </summary>
        private IStructureReference _conceptRef;

        /// <summary>
        ///     The representation.
        /// </summary>
        private IRepresentationMutableObject _representation;

        private UsageType? _usage = UsageType.Optional;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComponentMutableCore" /> class.
        /// </summary>
        /// <param name="structureType">
        ///     The structure type.
        /// </param>
        protected ComponentMutableCore(SdmxStructureType structureType)
            : base(structureType)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComponentMutableCore" /> class.
        /// </summary>
        /// <param name="objTarget">
        ///     The obj target.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="objTarget"/> is <see langword="null" />.</exception>
        protected ComponentMutableCore(IComponent objTarget)
            : base(objTarget)
        {
            if (objTarget == null)
            {
                throw new ArgumentNullException("objTarget");
            }

            if (objTarget.Representation != null)
            {
                this._representation = new RepresentationMutableCore(objTarget.Representation);
            }

            if (objTarget.ConceptRef != null)
            {
                this._conceptRef = objTarget.ConceptRef.CreateMutableInstance();
            }

            this._usage = objTarget.Usage;
        }

        /// <summary>
        ///     Gets or sets the concept ref.
        /// </summary>
        public virtual IStructureReference ConceptRef
        {
            get
            {
                return this._conceptRef;
            }

            set
            {
                this._conceptRef = value;
            }
        }

        /// <summary>
        ///     Gets or sets the representation.
        /// </summary>
        public virtual IRepresentationMutableObject Representation
        {
            get
            {
                return this._representation;
            }

            set
            {
                this._representation = value;
            }
        }

        /// <inheritdoc/>
        public UsageType? Usage { get => _usage; set => _usage = value; }

        /// <summary>
        ///     Components using concept roles should override this method
        /// </summary>
        /// <param name="structureReference">The structure reference.</param>
        public virtual void AddConceptRole(IStructureReference structureReference) { }

        /// <inheritdoc/>
        public virtual bool Mandatory => _usage == UsageType.Mandatory;

        /// <inheritdoc/>
        public void SetEnumeration(IStructureReference representationRef)
        {
            if (this._representation is null)
            {
                this._representation = new RepresentationMutableCore();
            }

            this._representation.Representation = representationRef;
        }

        /// <inheritdoc/>
        public virtual IComponentMutableObject SetMandatory(bool assignmentStatus)
        {
            this._usage = assignmentStatus ? UsageType.Mandatory : UsageType.Optional;
            return this;
        }

        /// <inheritdoc/>
        public IComponentMutableObject SetRepresentationMaxOccurs(int? num)
        {
            if (num.HasValue)
            {
                if (this._representation is null)
                {
                    this._representation = new RepresentationMutableCore();
                }
                IOccurenceObject occurrenceObject = new FiniteOccurenceObjectCore(num.Value);
                this._representation.MaxOccurs = occurrenceObject;
            }
            else if (this._representation != null)
            {
                this._representation.MaxOccurs = null;
            }

            return this;
        }

        /// <inheritdoc/>
        public IComponentMutableObject SetRepresentationMinOccurs(int? num)
        {
            if (num.HasValue)
            {
                if (this._representation is null)
                {
                    this._representation = new RepresentationMutableCore();
                }

                this._representation.MinOccurs = num.Value;
            }
            else if (this._representation != null)
            {
                this._representation.MinOccurs = null;
            }

            return this;

        }

        /// <inheritdoc/>
        public void SetTextFormat(ITextFormatMutableObject textFormat)
        {
            if (this._representation is null)
            {
                this._representation = new RepresentationMutableCore();
            }

            this._representation.TextFormat = textFormat;
        }

        /// <inheritdoc/>
        public ITextFormatMutableObject SetTextType(TextType tt)
        {
            if (this._representation is null)
            {
                this._representation = new RepresentationMutableCore();
            }

            if (this._representation.TextFormat is null)
            {
                this._representation.TextFormat = new TextFormatMutableCore();
            }

            this._representation.TextFormat.TextType = tt;

            return this._representation.TextFormat;
        }
    }
}