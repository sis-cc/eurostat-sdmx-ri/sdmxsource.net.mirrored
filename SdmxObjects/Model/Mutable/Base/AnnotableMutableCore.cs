// -----------------------------------------------------------------------
// <copyright file="AnnotableMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Util.Attributes;

    /// <summary>
    ///     The annotable mutable core.
    /// </summary>
    [Serializable]
    public abstract class AnnotableMutableCore : MutableCore, IAnnotableMutableObject
    {
        /// <summary>
        ///     The annotations.
        /// </summary>
        private IList<IAnnotationMutableObject> _annotations;

        /// <summary>
        ///     Initializes a new instance of the <see cref="AnnotableMutableCore" /> class.
        /// </summary>
        /// <param name="structureType">
        ///     The structure type.
        /// </param>
        protected AnnotableMutableCore(SdmxStructureType structureType)
            : base(structureType)
        {
            this._annotations = new List<IAnnotationMutableObject>();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AnnotableMutableCore" /> class.
        /// </summary>
        /// <param name="objTarget">
        ///     The obj target.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="objTarget"/> is <see langword="null" />.</exception>
        protected AnnotableMutableCore([ValidatedNotNull]IAnnotableObject objTarget)
            : base(objTarget)
        {
            if (objTarget == null)
            {
                throw new ArgumentNullException("objTarget");
            }

            this._annotations = new List<IAnnotationMutableObject>();
            if (objTarget.Annotations != null)
            {
                foreach (IAnnotation annotation in objTarget.Annotations)
                {
                    this._annotations.Add(new AnnotationMutableCore(annotation));
                }
            }
        }

        /// <summary>
        ///     Gets the annotations.
        /// </summary>
        public virtual IList<IAnnotationMutableObject> Annotations
        {
            get
            {
                return this._annotations;
            }
        }

        /// <summary>
        ///     The add annotation.
        /// </summary>
        /// <param name="annotation">
        ///     The annotation.
        /// </param>
        public virtual void AddAnnotation(IAnnotationMutableObject annotation)
        {
            if (annotation == null)
            {
                this._annotations = new List<IAnnotationMutableObject>();
            }

            this._annotations.Add(annotation);
        }

        /// <summary>
        ///     Adds an annotation and returns it
        /// </summary>
        /// <param name="title">The title</param>
        /// <param name="type">The type</param>
        /// <param name="url">The URL</param>
        /// <returns>
        ///     The annotation mutable object
        /// </returns>
        public IAnnotationMutableObject AddAnnotation(string title, string type, string url)
        {
            IAnnotationMutableObject mutable = new AnnotationMutableCore();
            mutable.Title = title;
            mutable.Type = type;

            mutable.Uri = !string.IsNullOrEmpty(url) ? new Uri(url) : null;
            this.AddAnnotation(mutable);
            return mutable;
        }

        /// <summary>
        ///     The process reader.
        /// </summary>
        /// <param name="reader">
        ///     The reader.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        protected internal bool ProcessReaderAnnotable(ISdmxReader reader)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (reader.CurrentElement.Equals("Annotations"))
            {
                reader.MoveNextElement();
                while (reader.CurrentElement.Equals("Annotation"))
                {
                    this.AddAnnotation(new AnnotationMutableCore(reader));
                }
            }

            return false;
        }
    }
}