// -----------------------------------------------------------------------
// <copyright file="MetadataProviderSchemeMutableCore.cs" company="EUROSTAT">
//   Date Created : 2024-04-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
using System;
using System.Collections.Generic;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base
{
    
    /// <summary>
    /// Metadata Provider Scheme
    /// </summary>
    public class MetadataProviderSchemeMutableCore : ItemSchemeMutableCore<IMetadataProviderMutable,IMetadataProvider,IMetadataProviderScheme>, IMetadataProviderSchemeMutable
    {
        private const long serialVersionUID = 1L;
        /// <summary>
        /// Initializes a new instance of the <see cref="MetadataProviderSchemeMutableCore" /> class.
        /// </summary>
        /// <param name="bean"></param>
        public MetadataProviderSchemeMutableCore(IMetadataProviderScheme bean) : base(bean)
        {
            foreach (IMetadataProvider dataProviderBean in bean.Items)
            {
                base.AddItem(new MetadataProviderMutableCore(dataProviderBean));
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public override IMetadataProviderMutable CreateItem(string id, string name)
        {
            IMetadataProviderMutable dpBean = new MetadataProviderMutableCore();
            dpBean.Id = id;
            dpBean.AddName("en", name);
            AddItem(dpBean);
            return dpBean;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MetadataProviderSchemeMutableCore" /> class.
        /// </summary>
        public MetadataProviderSchemeMutableCore() : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataProviderScheme))
        {
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public override IMetadataProviderScheme ImmutableInstance
        {
            get
            {
                return new MetadataProviderSchemeCore(this);
            }
        }
    }
}
