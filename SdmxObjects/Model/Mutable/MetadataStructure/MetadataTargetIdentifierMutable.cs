// -----------------------------------------------------------------------
// <copyright file="MetadataTargetIdentifierMutable.cs" company="EUROSTAT">
//   Date Created : 2024-03-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Io.Sdmx.Im.Mutable.Metadatastructure
{
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    public abstract class MetadataTargetIdentifierMutable : MaintainableMutableCore<IMetadataFlow>, IMetadataTargetIdentifiableMutable
    {

        private IList<IStructureReference> _targets;

        protected MetadataTargetIdentifierMutable(SdmxStructureType st)
                : base(st)
        {
        }

        protected MetadataTargetIdentifierMutable(IMetadataTargetIdentifier sdmxObject)
            : base(sdmxObject)
        {
            foreach (IStructureReference sRef in sdmxObject.Targets)
            {
                this.AddTarget(sRef.CreateCopy());
            }
        }

        public IList<IStructureReference> Targets
        {
            set => _targets = value;
            get => _targets;
        }

        public void AddTarget(IStructureReference sRef)
        {
            if (this._targets == null)
            {
                this._targets = new List<IStructureReference>();
            }
            this._targets.Add(sRef);
        }
    }

}