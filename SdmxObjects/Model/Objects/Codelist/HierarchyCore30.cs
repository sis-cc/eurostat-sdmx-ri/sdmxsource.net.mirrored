// -----------------------------------------------------------------------
// <copyright file="HierarchyCore30.cs" company="EUROSTAT">
//   Date Created : 2024-03-05
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Policy;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Diff;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Codelist
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    [Serializable]
    
    public class HierarchyCore30 : MaintainableObjectCore<IHierarchyV30, IHierarchyMutableObjectV30>, IHierarchyV30
    {
        private const long serialVersionUID = 14L;
        private IList<IHierarchicalCode> codeRefs = new List<IHierarchicalCode>();
        private ILevelObject level;
        private bool hasFormalLevels;

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM ITSELF, CREATES STUB BEAN //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        private HierarchyCore30(IHierarchyV30 bean, Uri actualLocation, bool isServiceUrl)
            : base(bean, actualLocation, isServiceUrl)
        {
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE BEAN				 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Initializes an instance of <see cref="HierarchyCore30"/>
        /// </summary>
        /// <param name="hierarchy"></param>
        /// <exception cref="SdmxSemmanticException"></exception>
        /// <exception cref="SdmxException"></exception>
        public HierarchyCore30(IHierarchyMutableObjectV30 hierarchy)
            : base(hierarchy)
        {
            // LEVELS MUST BE SET BEFORE ANYTHING ELSE
            if (hierarchy.GetChildLevel() != null)
            {
                this.level = new LevelCore(this, hierarchy.GetChildLevel());
            }

            if (hierarchy.GetHierarchicalCodeBeans() != null)
            {
                foreach (var currentCoderef in hierarchy.GetHierarchicalCodeBeans())
                {
                    codeRefs.Add(new HierarchicalCodeCore(currentCoderef, this));
                }
            }

            this.hasFormalLevels = hierarchy.IsFormalLevels();

            try
            {
                Validate();
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
        }
        /// <summary>
        /// Deep Equals
        /// </summary>
        /// <param name="sdmxObject"></param>
        /// <param name="includeFinalProperties"></param>
        /// <param name="diffReport"></param>
        /// <returns></returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties, IDiffReport diffReport)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                IHierarchyV30 that = (IHierarchyV30)sdmxObject;
                bool returnVal = true;

                if (!base.Equivalent(codeRefs, that.HierarchicalCodeObjects, includeFinalProperties, "Code Reference", diffReport))
                {
                    returnVal = false;
                }

                if (returnVal)
                {
                    // Refs in the Hierarchy are the same but ensure the order is also the same
                    returnVal = SameOrder(codeRefs.Cast<ISdmxObject>().ToList(), that.HierarchicalCodeObjects.Cast<ISdmxObject>().ToList(), "Hierarchical Code", diffReport);
                }

                if (!base.Equivalent(level, that.Level, includeFinalProperties, diffReport))
                {
                    returnVal = false;
                }

                if (!base.Equivalent(hasFormalLevels, that.HasFormalLevels(), "Has Formal Levels", diffReport))
                {
                    returnVal = false;
                }

                return base.DeepEqualsInternal(that, includeFinalProperties, diffReport) && returnVal;
            }

            return false;
        }

        private List<IHierarchicalCode> GetAllCodeRefsFlat(IList<IHierarchicalCode> codeRefs)
        {
            List<IHierarchicalCode> returnList = new List<IHierarchicalCode>();

            foreach (IHierarchicalCode childHcode in codeRefs)
            {
                returnList.Add(childHcode);

                if (childHcode.CodeRefs != null && childHcode.CodeRefs.Any())
                {
                    returnList.AddRange(GetAllCodeRefsFlat(childHcode.CodeRefs));
                }
            }

            return returnList;
        }

        private List<IHierarchicalCode> GetValidityPeriods(IHierarchicalCode item, List<IHierarchicalCode> allItems)
        {
            List<IHierarchicalCode> returnList = new List<IHierarchicalCode>();

            // foreach (HierarchicalCodeBean childHcode in allItems)
            // {
            //     if (childHcode == item)
            //     {
            //         continue;
            //     }
            //     if (item.LevelInHierarchy != childHcode.LevelInHierarchy)
            //     {
            //         continue;
            //     }
            //     if (item.Parent != childHcode.Parent)
            //     {
            //         continue;
            //     }
            //     if (item.CodeId.Equals(childHcode.CodeId))
            //     {
            //         returnList.Add(childHcode);
            //     }
            // }

            return returnList;
        }
        private void Validate()
        {
            if (hasFormalLevels)
            {
                ValidateHasLevel(codeRefs);
            }

            List<IHierarchicalCode> allCodeRefsFlat = GetAllCodeRefsFlat(codeRefs);

            foreach (IHierarchicalCode childHcode in allCodeRefsFlat)
            {
                List<IHierarchicalCode> validityPeriodCodes = GetValidityPeriods(childHcode, allCodeRefsFlat);
                ISdmxDate thisValidFrom = childHcode.ValidFrom;
                ISdmxDate thisValidTo = childHcode.ValidTo;

                if (thisValidFrom == null && thisValidTo == null)
                {
                    continue;
                }

                ISdmxPeriod thisDatePeriod = new SdmxPeriodCore(thisValidFrom, thisValidTo);

                foreach (IHierarchicalCode hierarchicalCodeBean in validityPeriodCodes)
                {
                    ISdmxDate validFrom = hierarchicalCodeBean.ValidFrom;
                    ISdmxDate validTo = hierarchicalCodeBean.ValidTo;

                    if (validFrom == null && validTo == null)
                    {
                        continue;
                    }

                    ISdmxPeriod datePeriod = new SdmxPeriodCore(validFrom, validTo);

                    if (datePeriod.IsInPeriod(thisDatePeriod))
                    {
                        throw new SdmxSemmanticException("The dates supplied overlap with the validity period on this object");
                    }
                }
            }

            foreach (IHierarchicalCode childHcode in allCodeRefsFlat)
            {
                ISdmxStructure parent = childHcode.Parent;

                if (!(parent is IHierarchicalCode))
                {
                    continue;
                }

                do
                {
                    ISdmxDate thisValidFrom = childHcode.ValidFrom;
                    ISdmxDate thisValidTo = childHcode.ValidTo;

                    if (thisValidFrom == null && thisValidTo == null)
                    {
                        continue;
                    }

                    ISdmxPeriod thisDatePeriod = new SdmxPeriodCore(thisValidFrom, thisValidTo);
                    List<IHierarchicalCode> validityPeriods = GetValidityPeriods((IHierarchicalCode)parent, allCodeRefsFlat);
                    validityPeriods.Add((IHierarchicalCode)parent);

                    foreach (IHierarchicalCode hierarchicalCodeBean in validityPeriods)
                    {
                        ISdmxDate validFrom = hierarchicalCodeBean.ValidFrom;
                        ISdmxDate validTo = hierarchicalCodeBean.ValidTo;

                        if (validFrom == null && validTo == null)
                        {
                            continue;
                        }

                        ISdmxPeriod datePeriod = new SdmxPeriodCore(validFrom, validTo);

                        if (!datePeriod.IsFullyInPeriod(thisDatePeriod))
                        {
                            throw new SdmxSemmanticException("Validity period cannot be wider than the parent's validity period");
                        }
                    }
                } while ((parent = parent.Parent) != null && parent is IHierarchicalCode);
            }
        }


        private void ValidateHasLevel(IList<IHierarchicalCode> codeRefs)
        {
            if (codeRefs != null)
            {
                foreach (var currentHCode in codeRefs)
                {
                    ValidateHasLevel(currentHCode.CodeRefs);

                    if (currentHCode.GetLevel(true) == null)
                    {
                        throw new SdmxSemmanticException("Hierarchy indicates formal levels, but Hierarchical Code '" + currentHCode.Urn + "' is missing a level reference and there is no default level for its depth in the hierarchy");
                    }
                }
            }
        }

        /// <inheritdoc />
        public IHierarchyV30 CloneForDate(DateTime aDate)
        {
            var clone = this.MutableInstance.StripItems(aDate);
            return clone.ImmutableInstance;
        }

        /// <summary>
        /// Get Stub
        /// </summary>
        /// <param name="actualLocation"></param>
        /// <param name="isServiceUrl"></param>
        /// <returns></returns>
        public override IHierarchyV30 GetStub(Uri actualLocation, bool isServiceUrl)
        {
            return new HierarchyCore30(this, actualLocation, isServiceUrl);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public IList<IHierarchicalCode> HierarchicalCodeObjects
        {
            get
            {
                return codeRefs; 
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public ILevelObject Level
        {
            get
            {
                return level;
            }
        }
        /// <summary>
        ///     Gets the maintainable parent.
        /// </summary>
        public new IHierarchyV30 MaintainableParent
        {
            get
            {
                return (IHierarchyV30)base.MaintainableParent;
            }
        }
        
        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public override IHierarchyMutableObjectV30 MutableInstance
        {
            get
            {
                return new HierarchyMutableCoreV30(this);
            }
        }

        /// <summary>
        /// Hierarchy SDMX 3.0 doesn't have a parent
        /// </summary>
        IHierarchicalCodelistObject IHierarchy.MaintainableParent => throw new NotImplementedException();


        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="levelId"></param>
        /// <returns></returns>
        public ILevelObject GetLevelById(string levelId)
        {
            ILevelObject currentLevel = level;
            while (currentLevel != null)
            {   
                if (currentLevel.GetFullIdPath(false).Equals(levelId))
                {
                    return currentLevel;
                }
                currentLevel = currentLevel.ChildLevel;
            }
            currentLevel = level;
            while (currentLevel != null)
            {
                if (currentLevel.Id.Equals(levelId))
                {
                    return currentLevel;
                }
                currentLevel = currentLevel.ChildLevel;
            }
            return null;
        }
        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="levelPos"></param>
        /// <returns></returns>
        public ILevelObject GetLevelAtPosition(int levelPos)
        {
            return null;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <returns></returns>
        public bool HasFormalLevels()
        {
            return hasFormalLevels;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <returns></returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            AddToCompositeSet(codeRefs, composites);
            AddToCompositeSet(level, composites);
            return composites;
        }
    }
}
