// -----------------------------------------------------------------------
// <copyright file="MemberValueCore.cs" company="EUROSTAT">
//   Date Created : 2021-10-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Codelist
{
    using System;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// The base implementation of the <see cref="IMemberValue"/> interface
    /// </summary>
    public class MemberValueCore : IMemberValue
    {
        private readonly CascadeSelection _cascade;
        private readonly string _value;

        /// <summary>
        /// Initializes a new instance of the <see cref="MemberValueCore"/> class,
        /// from a mutable object.
        /// </summary>
        /// <param name="mutableObj">The mutable object to initialize from.</param>
        public MemberValueCore(IMemberValueMutableObject mutableObj)
        {
            if (mutableObj == null)
            {
                throw new ArgumentNullException(nameof(mutableObj));
            }

            this._cascade = mutableObj.Cascade;
            this._value = mutableObj.Value;

            Validate();
        }

        /// <inheritdoc/>
        public CascadeSelection Cascade => this._cascade;

        /// <inheritdoc/>
        public string Value => this._value;

        private void Validate()
        {
            if (!ObjectUtil.ValidString(this._value))
            {
                // TODO ExceptionCode
                throw new SdmxSemmanticException("Value not set");
            }
        }
    }
}
