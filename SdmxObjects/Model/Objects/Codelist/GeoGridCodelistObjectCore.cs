// -----------------------------------------------------------------------
// <copyright file="GeoGridCodelistObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-03-14
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Io.Sdmx.Im.Objects.Codelist
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Diff;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Codelist;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    public class GeoGridCodelistObjectCore : 
        ItemSchemeObjectCore<IGeoGridCode, IGeoGridCodelistObject, IGeoGridCodelistMutableObject, IGeoGridCodeMutableObject>
        ,IGeoGridCodelistObject
    {

        private string _gridDefinition;

        /// <summary>
        /// The _code set. Note this does not exist in Java but included in .NET for performance reasons.
        /// </summary>
        private IDictionary<string, IGeoGridCode> _codeSet;

        /// <summary>
        /// The codelist extensions field.
        /// Introduced in SDMX 3.0.0 schema
        /// </summary>
        private readonly List<ICodelistInheritanceRule> _codelistExtensions = new List<ICodelistInheritanceRule>();

        /// <summary>
        ///   Initializes a new instance of the <see cref="CodelistObjectCore" /> class.
        /// </summary>
        /// <param name="codelist"></param>
        /// <exception cref="SdmxSemmanticException"></exception>
        /// <exception cref="SdmxException"></exception>
        public GeoGridCodelistObjectCore(IGeoGridCodelistMutableObject codelist)
            : base(codelist)
        {
            _gridDefinition = codelist.GridDefinition;

            try
            {
                if (codelist.Items != null)
                {
                    foreach (var code in codelist.Items)
                    {
                        this.AddInternalItem(new GeoCodeCore(this, code));
                    }
                }
                if (codelist.CodelistExtensions != null)
                {
                    _codelistExtensions.AddRange(codelist.CodelistExtensions.Select(e => new CodelistInheritanceRuleCore(e, this)));
                }
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }

            try
            {
                ValidateGeoGridCl();
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref="GeoGridCodelistObjectCore" /> class.
        /// </summary>
        /// <param name="codelist"></param>
        /// <param name="actualLocation"></param>
        /// <param name="isServiceUrl"></param>
        public GeoGridCodelistObjectCore(IGeoGridCodelistObject codelist, Uri actualLocation, bool isServiceUrl)
            : base(codelist, actualLocation, isServiceUrl)
        {

        }

        /// <summary>
        ///     Gets the mutable instance.
        /// </summary>
        public override IGeoGridCodelistMutableObject MutableInstance => new GeoGridCodelistMutableCore(this);

        /// <summary>
        ///     Gets the Grid definition.
        /// </summary>
        public string GridDefinition => _gridDefinition;

        protected IGeoGridCode CreateItem(ICodeMutableObject codeMutable)
        {
            return new GeoCodeCore(this, (IGeoGridCodeMutableObject)codeMutable);
        }

        /// <summary>
        ///     Gets the Urn
        /// </summary>
        public override sealed Uri Urn
        {
            get
            {
                return base.Urn;
            }
        }

        /// <inheritdoc/>
        public IList<ICodelistInheritanceRule> CodelistExtensions => new ReadOnlyCollection<ICodelistInheritanceRule>(this._codelistExtensions);

        public IGeoGridCodelistObject RemovePrefix(string prefix)
        {
            IGeoGridCodelistMutableObject mutable = this.MutableInstance;
            foreach (ICodeMutableObject codeMutable in mutable.Items)
            {
                if (codeMutable.Id.StartsWith(prefix))
                {
                    codeMutable.Id = codeMutable.Id.Substring(prefix.Length);
                }
            }
            return mutable.ImmutableInstance;
        }


        /// <summary>
        ///    The deep equals.
        /// </summary>
        /// <param name="sdmxObject"></param>
        /// <param name="includeFinalProperties"></param>
        /// <param name="diff"></param>
        /// <returns></returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties, IDiffReport diff)
        {
            if (sdmxObject == null)
            {
                return false;
            }
            if (sdmxObject is IGeoGridCodelistObject)
            {
                IGeoGridCodelistObject that = (IGeoGridCodelistObject)sdmxObject;
                bool returnVal = true;
                if (!base.Equivalent(_gridDefinition, that.GridDefinition, "Grid Definition", diff))
                {
                    returnVal = false;
                }
                return base.DeepEquals(sdmxObject, includeFinalProperties, diff) && returnVal;
            }
            return false;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATION							 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        private void ValidateGeoGridCl()
        {
            // CHECK FOR DUPLICATION OF URN & ILLEGAL PARENTING
            var parentChildMap = new Dictionary<IGeoGridCode, HashSet<IGeoGridCode>>();

            var duplicates = this.Items.Select(code => code.Id).GetDuplicates(StringComparer.Ordinal).FirstOrDefault();
            if (duplicates != null)
            {
                throw new SdmxSemmanticException(ExceptionCode.DuplicateUrn, this.Items.First(code => duplicates.Equals(code.Id)).Urn);
            }

            // NOT in Java. Added for performance reasons in .NET do not replace blindly in the next sync
            this._codeSet = this.Items.ToDictionary(code => code.Id, StringComparer.Ordinal);

            // partial codelist might not have parent
            if (this.Partial)
            {
                return;
            }

            foreach (IGeoGridCode code in this.Items)
            {
                if (!string.IsNullOrWhiteSpace(code.ParentCode))
                {
                    IGeoGridCode parentCode = GetCode(this._codeSet, code.ParentCode);
                    HashSet<IGeoGridCode> children;

                    if (!parentChildMap.TryGetValue(parentCode, out children))
                    {
                        children = new HashSet<IGeoGridCode>();
                        parentChildMap.Add(parentCode, children);
                    }

                    children.Add(code);

                    // Check that the parent code is not directly or indirectly a child of the code it is parenting
                    HashSet<IGeoGridCode> codeChildren;
                    if (parentChildMap.TryGetValue(code, out codeChildren))
                    {
                        IterateParentMap(codeChildren, parentCode, parentChildMap);
                    }
                }
            }
        }

        /// <summary>
        ///     Iterates the map checking the children of each child, if one of the children is the parent code, then an exception
        ///     is thrown
        /// </summary>
        /// <param name="children">
        ///     The children.
        /// </param>
        /// <param name="parentCode">
        ///     The parent Code.
        /// </param>
        /// <param name="parentChildMap">
        ///     The parent Child Map.
        /// </param>
        private static void IterateParentMap(
            ISet<IGeoGridCode> children,
            IGeoGridCode parentCode,
            IDictionary<IGeoGridCode, HashSet<IGeoGridCode>> parentChildMap)
        {
            if (children == null)
            {
                return;
            }

            var stack = new Stack<ISet<IGeoGridCode>>();
            stack.Push(children);
            while (stack.Count > 0)
            {
                children = stack.Pop();

                // If the child is also a parent
                if (children.Contains(parentCode))
                {
                    throw new SdmxSemmanticException(ExceptionCode.ParentRecursiveLoop, parentCode.Id);
                }

                foreach (IGeoGridCode currentChild in children)
                {
                    HashSet<IGeoGridCode> codeChildren;
                    if (parentChildMap.TryGetValue(currentChild, out codeChildren) && codeChildren != null)
                    {
                        stack.Push(codeChildren);
                    }
                }
            }
        }

        /// <summary>
        ///     The get code.
        /// </summary>
        /// <param name="codes">
        ///     The codes.
        /// </param>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="IGeoFeIGeoGridCodeatureSetCode" /> .
        /// </returns>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        private static IGeoGridCode GetCode(IDictionary<string, IGeoGridCode> codes, string id)
        {
            // NOT in Java. Added for performance reasons in .NET do not replace blindly in the next sync
            IGeoGridCode code;
            if (codes.TryGetValue(id, out code))
            {
                return code;
            }

            throw new SdmxSemmanticException(ExceptionCode.CannotResolveParent, id);
        }

        /// <summary>
        ///     The get code by id.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="ICode" /> .
        /// </returns>
        public virtual IGeoGridCode GetCodeById(string id)
        {
            IGeoGridCode currentCode;
            if (id != null && this._codeSet.TryGetValue(id, out currentCode))
            {
                return currentCode;
            }

            return null;
        }

        public IGeoGridCodelistObject CloneForDate(DateTime date)
        {
            // return (IGeoGridCodelistObject)base.CloneForDate(date);
            throw new NotImplementedException();
        }
       
        public override IGeoGridCodelistObject GetStub(Uri actualLocation, bool isServiceUrl)
        {
            return new GeoGridCodelistObjectCore(this, actualLocation, isServiceUrl);
        }

        /// <summary>
        /// Adds the <see cref="CodelistExtensions"/> to the collection of composites.
        /// </summary>
        /// <returns>The entire set of composites.</returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            base.AddToCompositeSet(this._codelistExtensions, composites);
            return composites;
        }

        /// <summary>
        ///   Returns the item with the given id, returns null if there is no item with the id provided
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IGeoGridCode GetItemById(string id)
        {
            return base.Items.FirstOrDefault(x => x.Id == id);
        }

        /// <summary>
        ///   Returns a new item scheme based on the item filters, which are either removed or kept depending on the isKeepSet argument
        /// </summary>
        /// <param name="filterIds"></param>
        /// <param name="isKeepSet"></param>
        /// <returns></returns>
        public IGeoGridCodelistObject FilterItems(IList<string> filterIds, bool isKeepSet)
        {
            return (IGeoGridCodelistObject)base.FilterItems(filterIds, isKeepSet);
        }
                    
        //public bool IsGeoCodelist()
        //{
        //    return true;
        //}
    }

}