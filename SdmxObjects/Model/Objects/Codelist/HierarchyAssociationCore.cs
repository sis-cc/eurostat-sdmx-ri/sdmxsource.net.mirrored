// -----------------------------------------------------------------------
// <copyright file="HierarchyAssociationCore.cs" company="EUROSTAT">
//   Date Created : 2024-03-05
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Diff;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Codelist
{
    [Serializable]
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public class HierarchyAssociationCore : MaintainableObjectCore<IHierarchyAssociation,IHierarchyAssociationMutableObject>, IHierarchyAssociation
    {
        private ICrossReference hierarchyRef;
        private ICrossReference linkRef;
        private ICrossReference contextRef;

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="bean"></param>
        /// <param name="actualLocation"></param>
        /// <param name="isServiceUrl"></param>
        protected HierarchyAssociationCore(IHierarchyAssociation bean, Uri actualLocation, bool isServiceUrl)
            : base(bean, actualLocation, isServiceUrl)
        {
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="bean"></param>
        /// <exception cref="SdmxSemmanticException"></exception>
        /// <exception cref="SdmxException"></exception>
        public HierarchyAssociationCore(IHierarchyAssociationMutableObject bean)
            : base(bean)
        {
            try
            {
                if (bean.HierarchyRef != null)
                {

                    //we need to use this constructor otherwise the Urn constructor will throw
                    //an exception because it will confuse HierarchyV30 with Hierarchy
                    hierarchyRef = new CrossReferenceImpl(this, bean.HierarchyRef.AgencyId,
                        bean.HierarchyRef.MaintainableId,
                        bean.HierarchyRef.Version,
                        SdmxStructureType.GetFromEnum(SdmxStructureEnumType.HierarchyV30));
                }

                if (bean.LinkedStructureRef != null)
                {
                    linkRef = new CrossReferenceImpl(this, bean.LinkedStructureRef.TargetUrn);
                }

                if (bean.ContextStructureRef != null)
                {
                    contextRef = new CrossReferenceImpl(this,bean.ContextStructureRef.TargetUrn);
                }

                Validate();
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn.ToString());
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn.ToString());
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <exception cref="SdmxSemmanticException"></exception>
        protected void Validate()
        {
            if (hierarchyRef == null)
            {
                throw new SdmxSemmanticException("Hierarchy Association is missing reference to Hierarchy");
            }

            if (hierarchyRef.TargetReference != SdmxStructureEnumType.HierarchyV30)
            {
                throw new SdmxSemmanticException("Hierarchy Reference must reference structure type: " + SdmxStructureType.GetFromEnum(SdmxStructureEnumType.HierarchyV30).UrnPrefix);
            }

            if (linkRef == null)
            {
                throw new SdmxSemmanticException("Hierarchy Association is missing reference to Maintainable");
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="bean"></param>
        /// <param name="includeFinalProperties"></param>
        /// <param name="diff"></param>
        /// <returns></returns>
        public  override bool DeepEquals(ISdmxObject bean, bool includeFinalProperties, IDiffReport diff)
        {
            if (bean == null)
            {
                return false;
            }

            if (bean.StructureType == this.StructureType)
            {
                IHierarchyAssociation that = (IHierarchyAssociation)bean;
                bool returnVal = true;

                if (!this.Equivalent(hierarchyRef, that.HierarchyRef, "Hierarchy Reference", diff))
                {
                    returnVal = false;
                }

                if (!this.Equivalent(linkRef, that.LinkedStructureRef, "Linked Structure", diff))
                {
                    returnVal = false;
                }

                if (!this.Equivalent(contextRef, that.ContextStructureRef, "Context Structure", diff))
                {
                    returnVal = false;
                }

                return this.DeepEqualsInternal(that, includeFinalProperties, diff) && returnVal;
            }

            return false;
        }

        public override IHierarchyAssociation GetStub(Uri actualLocation, bool isServiceUrl)
        {
            return new HierarchyAssociationCore(this, actualLocation, isServiceUrl);
        }
        
        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public override ISet<ICrossReference> CrossReferences
        {
            get
            {
                ISet<ICrossReference> xsRef = base.CrossReferences;
                xsRef.Add(hierarchyRef);
                xsRef.Add(linkRef);

                if (contextRef != null)
                {
                    xsRef.Add(contextRef);
                }

                return xsRef;
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public override IHierarchyAssociationMutableObject MutableInstance
        {
            get{return new HierarchyAssociationMutableCore(this);}
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public ICrossReference HierarchyRef
        {
            get { return hierarchyRef; }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public ICrossReference LinkedStructureRef
        {
            get { return linkRef; }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public ICrossReference ContextStructureRef
        {
            get { return contextRef; }
        }
    }

}
