// -----------------------------------------------------------------------
// <copyright file="CodelistInheritanceRuleCore.cs" company="EUROSTAT">
//   Date Created : 2021-10-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Codelist
{
    using System;
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// The base implementation of the <see cref="ICodelistInheritanceRule"/> interface
    /// </summary>
    public class CodelistInheritanceRuleCore : SdmxObjectCore, ICodelistInheritanceRule
    {
        private readonly ICrossReference _codelist;
        private readonly ICodeSelection _codeSelection;
        private readonly string _prefix;

        /// <summary>
        /// Initializes a new instance of the <see cref="CodelistInheritanceRuleCore"/> class,
        /// from a mutable object.
        /// </summary>
        /// <param name="mutableObject">The mutable object to initialize from.</param>
        /// <param name="parent">The SDMX object the codelist belongs into.</param>
        public CodelistInheritanceRuleCore(ICodelistInheritanceRuleMutableObject mutableObject, ISdmxObject parent) 
            : base(mutableObject, parent)
        {
            if (mutableObject == null)
            {
                throw new ArgumentNullException(nameof(mutableObject));
            }

            // following existing pattern but maybe it would be better to make
            // those checks with Objects?
            if (mutableObject.CodelistRef != null)
            {
                this._codelist = new CrossReferenceImpl(this, mutableObject.CodelistRef);
            }
            else
            {
                // TODO ExceptionCode
                throw new SdmxSemmanticException("Reference to extended codelist is missing");
            }
            if (mutableObject.CodeSelection != null)
            {
                this._codeSelection = new CodeSelectionCore(mutableObject.CodeSelection);
            }
            else
            {
                this._codeSelection = null;
            }
            this._prefix = mutableObject.Prefix;
        }

        /// <inheritdoc/>
        public ICrossReference CodelistRef => this._codelist;

        /// <inheritdoc/>
        public string Prefix => this._prefix;

        /// <inheritdoc/>
        public ICodeSelection CodeSelection => this._codeSelection;

        /// <summary>
        /// Returns an empty set.
        /// </summary>
        /// <returns>An empty <see cref="HashSet{T}"/>.</returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            return new HashSet<ISdmxObject>();
        }
    }
}
