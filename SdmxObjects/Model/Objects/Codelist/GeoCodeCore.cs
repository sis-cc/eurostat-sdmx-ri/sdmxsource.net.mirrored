// -----------------------------------------------------------------------
// <copyright file="GeoCodeCore.cs" company="EUROSTAT">
//   Date Created : 2024-03-14
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Diff;
using Org.Sdmxsource.Util;
using Org.Sdmxsource.Util.Extensions;

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Codelist
{
    public class GeoCodeCore : ItemCore, IGeoGridCode
    {
        /// <summary>
        ///     The geo cell
        /// </summary>
        private string _geoCell;

        /// <summary>
        ///     The code type.
        /// </summary>
        private static readonly SdmxStructureType _codeType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Code);

        /// <summary>
        ///     The parent code.
        /// </summary>
        private readonly string _parentCode;

        [NonSerialized]
        private IReadOnlyList<IGeoGridCode> _childCodes;

        /// <summary>
        ///     Initializes a new instance of the <see cref="GeoCodeCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="itemMutableObject">
        ///     The sdmxObject.
        /// </param>
        public GeoCodeCore(IGeoGridCodelistObject parent, IGeoGridCodeMutableObject itemMutableObject)
            : base(itemMutableObject, parent)
        {
            this._parentCode = string.IsNullOrWhiteSpace(itemMutableObject.ParentCode)
                                 ? null
                                 : itemMutableObject.ParentCode;

            _geoCell = itemMutableObject.GeoCell;

            try
            {
                Validate();
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, Urn);
            }
            catch (Exception ex)
            {
                throw new SdmxException(ex, ExceptionCode.ObjectStructureConstructionError, Urn);
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="GeoCodeCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="code">
        ///     The sdmxObject.
        /// </param>
        public GeoCodeCore(IGeoGridCodelistObject parent, CodeType code)
            : base(code, _codeType, parent)
        {
            var parentItem = code.GetTypedParent<LocalCodeReferenceType>();
            this._parentCode = (parentItem != null) ? parentItem.GetTypedRef<LocalCodeRefType>().id : null;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="GeoCodeCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="code">
        ///     The sdmxObject.
        /// </param>
        public GeoCodeCore(ICodelistObject parent, Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.CodeType code)
            : base(code, _codeType, code.PassNoNull("GeoGridCode").value, null, code.PassNoNull("GeoGridCode").Description, null, code.PassNoNull("GeoGridCode").Annotations, parent)
        {
            // In SDMX 2.0 it is perfectly valid for the XML to state a code has a parentCode which is blank. e.g.:
            // <str:Code value="aCode" parentCode="">
            // This can cause issues when manipulating the beans, so police the input by not setting the 
            // parentCode if it is an empty string.
            this._parentCode = string.IsNullOrWhiteSpace(code.parentCode) ? null : code.parentCode;
        }

        public string GeoCell => _geoCell;
        public string ParentCode => _parentCode;

        [JsonIgnore]
        public IList<IGeoGridCode> Items
        {
            get
            {
                //TODO make this faster
                //this needs to be fixed for Hierarchical codelist
                if (_childCodes == null)
                {
                    _childCodes = (IReadOnlyList<IGeoGridCode>)((IGeoGridCodelistObject)MaintainableParent).Items.Where(c => this.Id.Equals(c.ParentCode)).ToArray();
                }

                //TODO use ReadOnly to avoid copying
                return _childCodes.ToArray();
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS							 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        public bool DeepEquals(ISdmxObject obj, bool includeFinalProperties, DiffReport diff)
        {
            if (obj == null)
            {
                return false;
            }
            if (obj is IGeoGridCode)
            {
                IGeoGridCode that = (IGeoGridCode)obj;
                bool returnVal = true;
                if (!base.Equivalent(_geoCell, that.GeoCell, "Geo Cell", diff))
                {
                    returnVal = false;
                }
                return base.DeepEquals(obj, includeFinalProperties, diff) && returnVal;
            }
            return false;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATION							 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        protected void Validate()
        {
            if (!ObjectUtil.ValidString(this._geoCell))
            {
                throw new SdmxSemmanticException("Geogrid Code is missing required Geo Cell");
            }
        }

        public bool IsHierarchyExplicit()
        {
            return false;
        }

        public bool HasChildren()
        {
            return Items.Any();
        }
    }
}
