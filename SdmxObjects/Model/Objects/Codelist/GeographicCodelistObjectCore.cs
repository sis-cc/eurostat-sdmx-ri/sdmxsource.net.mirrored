// -----------------------------------------------------------------------
// <copyright file="GeographicCodelistObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Codelist
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Diff;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The codelist object core.
    /// </summary>
    [Serializable]
    public class GeographicCodelistObjectCore :
        ItemSchemeObjectCore<IGeoFeatureSetCode, IGeographicCodelistObject, IGeographicCodelistMutableObject, IGeoFeatureSetCodeMutableObject>,
        IGeographicCodelistObject
    {

        /// <summary>
        /// The _code set. Note this does not exist in Java but included in .NET for performance reasons.
        /// </summary>
        private IDictionary<string, IGeoFeatureSetCode> _codeSet;

        /// <summary>
        /// The codelist extensions field.
        /// Introduced in SDMX 3.0.0 schema
        /// </summary>
        private readonly List<ICodelistInheritanceRule> _codelistExtensions = new List<ICodelistInheritanceRule>();

        /// <summary>
        ///     Initializes a new instance of the <see cref="CodelistObjectCore" /> class.
        /// </summary>
        /// <param name="codelist">
        ///     The codelist.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws SdmxSemmanticException.
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public GeographicCodelistObjectCore(IGeographicCodelistMutableObject codelist)
            : base(codelist)
        {
            try
            {
                if (codelist.Items != null)
                {
                    foreach (var code in codelist.Items)
                    {
                        this.AddInternalItem(new GeoFeatureSetCodeCore(this, code));
                    }
                }
                if (codelist.CodelistExtensions != null)
                {
                    _codelistExtensions.AddRange(codelist.CodelistExtensions.Select(e => new CodelistInheritanceRuleCore(e, this)));
                }
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }

            try
            {
                this.ValidateGeoCL();
            }
            catch (SdmxSemmanticException ex1)
            {
                throw new SdmxSemmanticException(ex1, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th1)
            {
                throw new SdmxException(th1, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref="CodelistObjectCore" /> class.
        /// </summary>
        /// <param name="codelist"></param>
        /// <param name="actualLocation"></param>
        /// <param name="isServiceUrl"></param>
        public GeographicCodelistObjectCore(IGeographicCodelistObject codelist, Uri actualLocation, bool isServiceUrl) 
            : base(codelist, actualLocation, isServiceUrl)
        {

        }


        /// <summary>
        ///     Gets the mutable instance.
        /// </summary>
        public override IGeographicCodelistMutableObject MutableInstance => new GeographicCodelistMutableObjectCore(this);

        /// <summary>
        ///     Gets the Urn
        /// </summary>
        public override sealed Uri Urn
        {
            get
            {
                return base.Urn;
            }
        }


        /// <summary>
        ///     The validate identifiable attributes.
        /// </summary>
        protected internal override void ValidateIdentifiableAttributes()
        {
            // Do nothing yet, not yet fully built
        }

        protected override void  ValidateNameableAttributes()
        {

        }

        public bool IsGeoCodeList => true;

        /// <inheritdoc/>
        public IList<ICodelistInheritanceRule> CodelistExtensions => new ReadOnlyCollection<ICodelistInheritanceRule>(this._codelistExtensions);

        public IGeographicCodelistObject RemovePrefix(string prefix)
        {
            IGeographicCodelistMutableObject mutableObject = MutableInstance;
            foreach (var item in mutableObject.Items)
            {
                if (item.Id.StartsWith(prefix))
                {
                    item.Id = item.Id.Substring(prefix.Length);
                }
            }

            return mutableObject.ImmutableInstance;
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The agencyScheme.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <param name="diff"></param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties, IDiffReport diff)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            bool returnVal = true;
            if (sdmxObject.StructureType == this.StructureType)
            {
                return this.DeepEqualsInternal((IGeographicCodelistObject)sdmxObject, includeFinalProperties, "Code", diff) && returnVal;
            }

            return false;
        }

        /// <summary>
        ///     The get code by id.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="ICode" /> .
        /// </returns>
        public virtual IGeoFeatureSetCode GetCodeById(string id)
        {
            IGeoFeatureSetCode currentCode;
            if (id != null && this._codeSet.TryGetValue(id, out currentCode))
            {
                return currentCode;
            }

            return null;
        }

        protected IGeoFeatureSetCode CreateItem(IGeographicCodelistMutableObject item)
        {
            return new GeoFeatureSetCodeCore(this, (IGeoFeatureSetCodeMutableObject)item);
        }

        /// <summary>
        ///     The get stub.
        /// </summary>
        /// <param name="actualLocation">
        ///     The actual location.
        /// </param>
        /// <param name="isServiceUrl">
        ///     The is service url.
        /// </param>
        /// <returns>
        ///     The <see cref="ICodelistObject" /> .
        /// </returns>
        public override IGeographicCodelistObject GetStub(Uri actualLocation, bool isServiceUrl) 
        {
            return new GeographicCodelistObjectCore(this, actualLocation, isServiceUrl);
        }

        /// <summary>
        ///    The Validate
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void ValidateGeoCL()
        {
            // CHECK FOR DUPLICATION OF URN & ILLEGAL PARENTING
            var parentChildMap = new Dictionary<IGeoFeatureSetCode, HashSet<IGeoFeatureSetCode>>();

            var duplicates = this.Items.Select(code => code.Id).GetDuplicates(StringComparer.Ordinal).FirstOrDefault();
            if (duplicates != null)
            {
                throw new SdmxSemmanticException(ExceptionCode.DuplicateUrn, this.Items.First(code => duplicates.Equals(code.Id)).Urn);
            }
         
            // NOT in Java. Added for performance reasons in .NET do not replace blindly in the next sync
            this._codeSet = this.Items.ToDictionary(code => code.Id, StringComparer.Ordinal);

            // partial codelist might not have parent
            if (this.Partial)
            {
                return;
            }

            foreach (IGeoFeatureSetCode code in this.Items)
            {
                if (!string.IsNullOrWhiteSpace(code.ParentCode))
                {
                    IGeoFeatureSetCode parentCode = GetCode(this._codeSet, code.ParentCode);
                    HashSet<IGeoFeatureSetCode> children;

                    if (!parentChildMap.TryGetValue(parentCode, out children))
                    {
                        children = new HashSet<IGeoFeatureSetCode>();
                        parentChildMap.Add(parentCode, children);
                    }

                    children.Add(code);

                    // Check that the parent code is not directly or indirectly a child of the code it is parenting
                    HashSet<IGeoFeatureSetCode> codeChildren;
                    if (parentChildMap.TryGetValue(code, out codeChildren))
                    {
                        IterateParentMap(codeChildren, parentCode, parentChildMap);
                    }
                }
            }
        }

        /// <summary>
        ///     Iterates the map checking the children of each child, if one of the children is the parent code, then an exception
        ///     is thrown
        /// </summary>
        /// <param name="children">
        ///     The children.
        /// </param>
        /// <param name="parentCode">
        ///     The parent Code.
        /// </param>
        /// <param name="parentChildMap">
        ///     The parent Child Map.
        /// </param>
        private static void IterateParentMap(
            ISet<IGeoFeatureSetCode> children,
            IGeoFeatureSetCode parentCode,
            IDictionary<IGeoFeatureSetCode, HashSet<IGeoFeatureSetCode>> parentChildMap)
        {
            if (children == null)
            {
                return;
            }

            var stack = new Stack<ISet<IGeoFeatureSetCode>>();
            stack.Push(children);
            while (stack.Count > 0)
            {
                children = stack.Pop();

                // If the child is also a parent
                if (children.Contains(parentCode))
                {
                    throw new SdmxSemmanticException(ExceptionCode.ParentRecursiveLoop, parentCode.Id);
                }

                foreach (IGeoFeatureSetCode currentChild in children)
                {
                    HashSet<IGeoFeatureSetCode> codeChildren;
                    if (parentChildMap.TryGetValue(currentChild, out codeChildren) && codeChildren != null)
                    {
                        stack.Push(codeChildren);
                    }
                }
            }
        }

        /// <summary>
        /// Adds the <see cref="CodelistExtensions"/> to the collection of composites.
        /// </summary>
        /// <returns>The entire set of composites.</returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            base.AddToCompositeSet(this._codelistExtensions, composites);
            return composites;
        }

        /// <summary>
        ///     The validate id.
        /// </summary>
        /// <param name="startWithIntAllowed">
        ///     The start with int allowed.
        /// </param>
        protected internal override void ValidateId(bool startWithIntAllowed)
        {
            // Not allowed to start with an integer
            base.ValidateId(false);
        }

        /// <summary>
        ///     The get code.
        /// </summary>
        /// <param name="codes">
        ///     The codes.
        /// </param>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="IGeoFeatureSetCode" /> .
        /// </returns>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        private static IGeoFeatureSetCode GetCode(IDictionary<string, IGeoFeatureSetCode> codes, string id)
        {
            // NOT in Java. Added for performance reasons in .NET do not replace blindly in the next sync
            IGeoFeatureSetCode code;
            if (codes.TryGetValue(id, out code))
            {
                return code;
            }

            throw new SdmxSemmanticException(ExceptionCode.CannotResolveParent, id);
        }

        public IGeographicCodelistObject CloneForDate(DateTime date)
        {
            return null;
        }
       
        public IGeographicCodelistObject FilterItems(IList<string> filterIds, bool isKeepSet)
        {
            return (IGeographicCodelistObject)base.FilterItems(filterIds, isKeepSet);
        }

        IGeographicCodelistObject IGeographicCodelistObject.GetStub(Uri actualLocation, bool isServiceUrl)
        {
            return new GeographicCodelistObjectCore(this, actualLocation, isServiceUrl);
        }
      
    }
}