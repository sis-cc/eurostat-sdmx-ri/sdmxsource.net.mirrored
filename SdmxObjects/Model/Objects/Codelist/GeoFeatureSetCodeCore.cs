using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
using Org.Sdmxsource.Util;
using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
using Org.Sdmxsource.Util.Extensions;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Diff;

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Codelist
{
    public class GeoFeatureSetCodeCore : ItemCore, IGeoFeatureSetCode
    {
        private string _geoFeatureSet;
        private string _urn;

        /// <summary>
        ///     The _code type.
        /// </summary>
        private static readonly SdmxStructureType _codeType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Code);

        /// <summary>
        ///     The parent code.
        /// </summary>
        private readonly string _parentCode;

        [NonSerialized]
        private IReadOnlyList<IGeoFeatureSetCode> _childCodes;

        /// <summary>
        ///     Initializes a new instance of the <see cref="GeoFeatureSetCodeCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="itemMutableObject">
        ///     The sdmxObject.
        /// </param>
        public GeoFeatureSetCodeCore(IGeographicCodelistObject parent, IGeoFeatureSetCodeMutableObject itemMutableObject)
            : base(itemMutableObject, parent)
        {
            this._parentCode = string.IsNullOrWhiteSpace(itemMutableObject.ParentCode)
                                 ? null
                                 : itemMutableObject.ParentCode;

            _geoFeatureSet = itemMutableObject.GeoFeatureSet;          

            try
            {
                Validate();
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, Urn);
            }
            catch (Exception ex)
            {
                throw new SdmxException(ex, ExceptionCode.ObjectStructureConstructionError, Urn);
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="GeoFeatureSetCodeCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="code">
        ///     The sdmxObject.
        /// </param>
        public GeoFeatureSetCodeCore(ICodelistObject parent, CodeType code)
            : base(code, _codeType, parent)
        {
            var parentItem = code.GetTypedParent<LocalCodeReferenceType>();
            this._parentCode = (parentItem != null) ? parentItem.GetTypedRef<LocalCodeRefType>().id : null;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="GeoFeatureSetCodeCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="code">
        ///     The sdmxObject.
        /// </param>
        public GeoFeatureSetCodeCore(ICodelistObject parent, Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.CodeType code)
            : base(code, _codeType, code.PassNoNull("GeoFeatureSetCode").value, null, code.PassNoNull("GeoFeatureSetCode").Description, null, code.PassNoNull("GeoFeatureSetCode").Annotations, parent)
        {
            // In SDMX 2.0 it is perfectly valid for the XML to state a code has a parentCode which is blank. e.g.:
            // <str:Code value="aCode" parentCode="">
            // This can cause issues when manipulating the beans, so police the input by not setting the 
            // parentCode if it is an empty string.
            this._parentCode = string.IsNullOrWhiteSpace(code.parentCode) ? null : code.parentCode;
        }

        public string GeoFeatureSet => _geoFeatureSet;

        /// <summary>
        ///     Gets the parent code.
        /// </summary>
        public virtual string ParentCode
        {
            get
            {
                return this._parentCode;
            }           
        }

        [JsonIgnore]
        public IList<IGeoFeatureSetCode> Items
        {
            get
            {
                //TODO make this faster
                //this needs to be fixed for Hierarchical codelist
                if (_childCodes == null)
                {
                    _childCodes = ((IGeographicCodelistObject)MaintainableParent).Items.Where(c => this.Id.Equals(c.ParentCode)).ToArray();
                }

                //TODO use ReadOnly to avoid copying
                return _childCodes.ToArray();
            }
        }   

        public bool DeepEquals(ISdmxObject obj, bool includeFinalProperties, DiffReport diff)
        {
            if (obj == null)
            {
                return false;
            }

            if (obj is IGeoFeatureSetCode)
            {
                IGeoFeatureSetCode that = (IGeoFeatureSetCode)obj;
                bool returnVal = true;
                if (!Equivalent(_geoFeatureSet, that.GeoFeatureSet, "Geo Feature Set", diff))
                {
                    returnVal = false;
                }

                return base.DeepEquals(obj, includeFinalProperties, diff) && returnVal;
            }

            return false;
        }

        /// <summary>
        ///  
        /// </summary>
        /// <exception cref="SdmxException"></exception>
        protected void Validate()
        {
            if (!ObjectUtil.ValidString(GeoFeatureSet))
            {
                throw new SdmxException("Geo Feature Set Code is missing required feature set");
            }
        }

        public bool IsHierarchyExplicit()
        {
            return Items.Any();
        }

        public bool HasChildren()
        {
            return false;
        }
    }
}
