// -----------------------------------------------------------------------
// <copyright file="LevelCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Codelist
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

    /// <summary>
    ///     The level core.
    /// </summary>
    [Serializable]
    public class LevelCore : NameableCore, ILevelObject
    {
        /// <summary>
        ///     The child level.
        /// </summary>
        private readonly ILevelObject _childLevel;

        /// <summary>
        ///     The itext format.
        /// </summary>
        private readonly ITextFormat _textFormat;

        /// <summary>
        ///     Initializes a new instance of the <see cref="LevelCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="levelMutable">
        ///     The level mutable.
        /// </param>
        public LevelCore(IIdentifiableObject parent, ILevelMutableObject levelMutable)
            : base(levelMutable, parent)
        {
            if (levelMutable.CodingFormat != null)
            {
                this._textFormat = new TextFormatObjectCore(levelMutable.CodingFormat, this);
            }

            if (levelMutable.ChildLevel != null)
            {
                this._childLevel = new LevelCore(this, levelMutable.ChildLevel);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="LevelCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="levels">
        ///     The levels.
        /// </param>
        /// <param name="pos">
        ///     The pos.
        /// </param>
        public LevelCore(IIdentifiableObject parent, IList<LevelType> levels, int pos)
            : base(
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Level), 
                GetLevel(levels, pos).id, 
                null, 
                GetLevel(levels, pos).Name, 
                GetLevel(levels, pos).Description, 
                GetLevel(levels, pos).Annotations, 
                parent)
        {
            LevelType level = GetLevel(levels, pos);
            if (level.CodingType != null)
            {
                this._textFormat = new TextFormatObjectCore(level.CodingType, this);
            }

            checked
            {
                if (levels.Count > pos + 1)
                {
                    this._childLevel = new LevelCore(this, levels, ++pos);
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="LevelCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="level">
        ///     The level.
        /// </param>
        public LevelCore(IIdentifiableObject parent, Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure.LevelType level)
            : base(level, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Level), parent)
        {
            if (level.CodingFormat != null)
            {
                this._textFormat = new TextFormatObjectCore(level.CodingFormat, this);
            }

            if (level.Level != null)
            {
                this._childLevel = new LevelCore(this, level.Level);
            }
        }

        /// <summary>
        ///     Gets the child level.
        /// </summary>
        public virtual ILevelObject ChildLevel
        {
            get
            {
                return this._childLevel;
            }
        }

        /// <summary>
        ///     Gets the coding format.
        /// </summary>
        public virtual ITextFormat CodingFormat
        {
            get
            {
                return this._textFormat;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (ILevelObject)sdmxObject;
                if (!this.Equivalent(this._childLevel, that.ChildLevel, includeFinalProperties))
                {
                    return false;
                }

                if (!this.Equivalent(this._textFormat, that.CodingFormat, includeFinalProperties))
                {
                    return false;
                }

                return this.DeepEqualsNameable(that, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     The has child.
        /// </summary>
        /// <returns> The <see cref="bool" /> . </returns>
        public virtual bool HasChild()
        {
            return this._childLevel != null;
        }

        /// <summary>
        ///     The get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._childLevel, composites);
            this.AddToCompositeSet(this._textFormat, composites);
            return composites;
        }

        /// <summary>
        ///     The get level.
        /// </summary>
        /// <param name="levels">
        ///     The levels.
        /// </param>
        /// <param name="pos">
        ///     The pos.
        /// </param>
        /// <returns>
        ///     The <see cref="LevelType" /> .
        /// </returns>
        private static LevelType GetLevel(IList<LevelType> levels, int pos)
        {
            return levels[pos];
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////    

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ////////////BUILD FROM MUTABLE OBJECT                 //////////////////////////////////////////////////
    }
}