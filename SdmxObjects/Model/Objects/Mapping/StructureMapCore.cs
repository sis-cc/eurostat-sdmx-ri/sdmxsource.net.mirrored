// -----------------------------------------------------------------------
// <copyright file="StructureMapCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Mapping
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    using ComponentMapType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure.ComponentMapType;
    using StructureMapType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure.StructureMapType;

    /// <summary>
    ///     The structure map core.
    /// </summary>
    [Serializable]
    public class StructureMapCore : SchemeMapCore, IStructureMapObject
    {
        /// <summary>
        ///     The components.
        /// </summary>
        private readonly IList<IComponentMapObject> _components;

        /// <summary>
        ///     The extension.
        /// </summary>
        private readonly bool _extension;

        /// <summary>
        ///     Initializes a new instance of the <see cref="StructureMapCore" /> class.
        /// </summary>
        /// <param name="structMapType">
        ///     The struct map type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public StructureMapCore(IStructureMapMutableObject structMapType, IStructureSetObject parent)
            : base(structMapType, parent)
        {
            this._components = new List<IComponentMapObject>();
            this._extension = structMapType.Extension;
            if (structMapType.Components != null)
            {
                foreach (IComponentMapMutableObject mutable in structMapType.Components)
                {
                    this._components.Add(new ComponentMapCore(mutable, this));
                }
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="StructureMapCore" /> class.
        /// </summary>
        /// <param name="structMapType">
        ///     The struct map type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public StructureMapCore(StructureMapType structMapType, IStructureSetObject parent)
            : base(structMapType, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.StructureMap), parent)
        {
            this._components = new List<IComponentMapObject>();
            this.SourceRef = RefUtil.CreateReference(this, structMapType.Source);
            this.TargetRef = RefUtil.CreateReference(this, structMapType.Target);
            IList<ComponentMapType> componentMapType = structMapType.ComponentMapTypes;
            if (ObjectUtil.ValidCollection(componentMapType))
            {
                foreach (ComponentMapType compMap in componentMapType)
                {
                    this._components.Add(new ComponentMapCore(compMap, this));
                }
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="StructureMapCore" /> class.
        /// </summary>
        /// <param name="structMapType">
        ///     The struct map type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public StructureMapCore(
            Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.StructureMapType structMapType, 
            IStructureSetObject parent)
            : base(
                structMapType, 
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.StructureMap), 
                structMapType.PassNoNull("structMapType").id, 
                null, 
                structMapType.PassNoNull("structMapType").Name, 
                structMapType.PassNoNull("structMapType").Description, 
                structMapType.PassNoNull("structMapType").Annotations, 
                parent)
        {
            this._components = new List<IComponentMapObject>();

            this._extension = structMapType.isExtension.Value;
            KeyFamilyRefType keyFamilyRef = structMapType.KeyFamilyRef;

            if (keyFamilyRef != null)
            {
                if (keyFamilyRef.URN != null)
                {
                    this.SourceRef = new CrossReferenceImpl(this, keyFamilyRef.URN);
                }
                else
                {
                    string agencyId = keyFamilyRef.KeyFamilyAgencyID;
                    if (string.IsNullOrWhiteSpace(agencyId))
                    {
                        agencyId = this.MaintainableParent.AgencyId;
                    }

                    this.SourceRef = new CrossReferenceImpl(
                        this, 
                        agencyId, 
                        keyFamilyRef.KeyFamilyID, 
                        keyFamilyRef.Version, 
                        SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd));
                }
            }
            else if (structMapType.MetadataStructureRef != null)
            {
                if (structMapType.MetadataStructureRef.URN != null)
                {
                    this.SourceRef = new CrossReferenceImpl(this, structMapType.MetadataStructureRef.URN);
                }
                else
                {
                    string agencyId0 = structMapType.KeyFamilyRef.KeyFamilyAgencyID;
                    if (string.IsNullOrWhiteSpace(agencyId0))
                    {
                        agencyId0 = this.MaintainableParent.AgencyId;
                    }

                    this.SourceRef = new CrossReferenceImpl(
                        this, 
                        agencyId0, 
                        structMapType.MetadataStructureRef.MetadataStructureID, 
                        structMapType.MetadataStructureRef.Version, 
                        SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Msd));
                }
            }

            // target ref can be one of two types so get one which isn't null
            if (structMapType.TargetKeyFamilyRef != null)
            {
                if (structMapType.TargetKeyFamilyRef.URN != null)
                {
                    this.TargetRef = new CrossReferenceImpl(this, structMapType.TargetKeyFamilyRef.URN);
                }
                else
                {
                    string agencyId1 = structMapType.TargetKeyFamilyRef.KeyFamilyAgencyID;
                    if (string.IsNullOrWhiteSpace(agencyId1))
                    {
                        agencyId1 = this.MaintainableParent.AgencyId;
                    }

                    this.TargetRef = new CrossReferenceImpl(
                        this, 
                        agencyId1, 
                        structMapType.TargetKeyFamilyRef.KeyFamilyID, 
                        structMapType.TargetKeyFamilyRef.Version, 
                        SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd));
                }
            }
            else if (structMapType.TargetMetadataStructureRef != null)
            {
                if (structMapType.TargetMetadataStructureRef.URN != null)
                {
                    this.TargetRef = new CrossReferenceImpl(this, structMapType.TargetMetadataStructureRef.URN);
                }
                else
                {
                    string agencyId2 = structMapType.TargetMetadataStructureRef.MetadataStructureAgencyID;
                    if (string.IsNullOrWhiteSpace(agencyId2))
                    {
                        agencyId2 = this.MaintainableParent.AgencyId;
                    }

                    this.TargetRef = new CrossReferenceImpl(
                        this, 
                        agencyId2, 
                        structMapType.TargetMetadataStructureRef.MetadataStructureID, 
                        structMapType.TargetMetadataStructureRef.Version, 
                        SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Msd));
                }
            }

            // get list of component maps
            if (structMapType.ComponentMap != null)
            {
                foreach (Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.ComponentMapType compMap in
                    structMapType.ComponentMap)
                {
                    var componentMapCore = new ComponentMapCore(compMap, this);
                    this._components.Add(componentMapCore);
                }
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        /// <summary>
        ///     Gets the components.
        /// </summary>
        public virtual IList<IComponentMapObject> Components
        {
            get
            {
                return new List<IComponentMapObject>(this._components);
            }
        }

        /// <summary>
        ///     Gets a value indicating whether extension.
        /// </summary>
        public virtual bool Extension
        {
            get
            {
                return this._extension;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IStructureMapObject)sdmxObject;
                if (!this.Equivalent(this._components, that.Components, includeFinalProperties))
                {
                    return false;
                }

                if (this.Extension != that.Extension)
                {
                    return false;
                }

                return this.DeepEqualsNameable(that, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     Get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._components, composites);
            return composites;
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void Validate()
        {
            if (this.SourceRef == null)
            {
                throw new SdmxSemmanticException("CategorisationStructure Map missing source component");
            }

            if (this.TargetRef == null)
            {
                throw new SdmxSemmanticException("CategorisationStructure Map missing target component");
            }

            ISet<SdmxStructureType> allowedTypes = new HashSet<SdmxStructureType>();
            allowedTypes.Add(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow));
            allowedTypes.Add(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd));
            allowedTypes.Add(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataFlow));
            allowedTypes.Add(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Msd));

            this.VerifyTypes(allowedTypes, this.SourceRef.TargetReference);
            this.VerifyTypes(allowedTypes, this.TargetRef.TargetReference);
        }

        /// <summary>
        ///     The verify types.
        /// </summary>
        /// <param name="allowedTypes">
        ///     The allowed types.
        /// </param>
        /// <param name="actualType">
        ///     The actual type.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        private void VerifyTypes(ISet<SdmxStructureType> allowedTypes, SdmxStructureType actualType)
        {
            if (!allowedTypes.Contains(actualType))
            {
                string allowedTypesStr = string.Empty;

                foreach (SdmxStructureType currentType in allowedTypes)
                {
                    allowedTypesStr += currentType + ",";
                }

                allowedTypesStr = allowedTypesStr.Substring(0, (allowedTypesStr.Length - 2) - 0);

                throw new SdmxSemmanticException(
                    "Disallowed concept map type '" + actualType + "' allowed types are '" + allowedTypesStr + "'");
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATION                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ////////////BUILD FROM MUTABLE OBJECTS             //////////////////////////////////////////////////
    }
}