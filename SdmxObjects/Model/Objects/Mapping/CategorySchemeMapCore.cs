// -----------------------------------------------------------------------
// <copyright file="CategorySchemeMapCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Mapping
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util.Extensions;

    using CategoryMapType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.CategoryMapType;

    /// <summary>
    ///     The category scheme map core.
    /// </summary>
    [Serializable]
    public class CategorySchemeMapCore : SchemeMapCore, ICategorySchemeMapObject
    {
        /// <summary>
        ///     The category maps.
        /// </summary>
        private readonly IList<ICategoryMap> _categoryMaps;

        /// <summary>
        ///     Initializes a new instance of the <see cref="CategorySchemeMapCore" /> class.
        /// </summary>
        /// <param name="itemMutableObject">
        ///     The agencyScheme.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="itemMutableObject"/> is <see langword="null" />.</exception>
        public CategorySchemeMapCore(ICategorySchemeMapMutableObject itemMutableObject, IStructureSetObject parent)
            : base(itemMutableObject, parent)
        {
            if (itemMutableObject == null)
            {
                throw new ArgumentNullException("itemMutableObject");
            }

            this._categoryMaps = new List<ICategoryMap>();
            if (itemMutableObject.CategoryMaps != null)
            {
                this._categoryMaps = new List<ICategoryMap>();

                foreach (ICategoryMapMutableObject catMap in itemMutableObject.CategoryMaps)
                {
                    ICategoryMap categoryMap = new CategoryMapCore(catMap, this);
                    this._categoryMaps.Add(categoryMap);
                }
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="CategorySchemeMapCore" /> class.
        /// </summary>
        /// <param name="catMapType">
        ///     The cat map type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="catMapType"/> is <see langword="null" />.</exception>
        public CategorySchemeMapCore(CategorySchemeMapType catMapType, IStructureSetObject parent)
            : base(catMapType, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CategorySchemeMap), parent)
        {
            if (catMapType == null)
            {
                throw new ArgumentNullException("catMapType");
            }

            this._categoryMaps = new List<ICategoryMap>();

            this.SourceRef = RefUtil.CreateReference(this, catMapType.GetTypedSource<CategorySchemeReferenceType>());
            this.TargetRef = RefUtil.CreateReference(this, catMapType.GetTypedTarget<CategorySchemeReferenceType>());

            if (catMapType.ItemAssociation != null)
            {
                foreach (CategoryMap catMap in catMapType.ItemAssociation)
                {
                    ICategoryMap categoryMap = new CategoryMapCore(catMap.Content, this);
                    this._categoryMaps.Add(categoryMap);
                }
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="CategorySchemeMapCore" /> class.
        /// </summary>
        /// <param name="catMapType">
        ///     The cat map type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public CategorySchemeMapCore(
            Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.CategorySchemeMapType catMapType, 
            IStructureSetObject parent)
            : base(
                catMapType, 
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CategorySchemeMap), 
                catMapType.PassNoNull("catMapType").id, 
                null, 
                catMapType.PassNoNull("catMapType").Name, 
                catMapType.PassNoNull("catMapType").Description, 
                catMapType.PassNoNull("catMapType").Annotations, 
                parent)
        {
            this._categoryMaps = new List<ICategoryMap>();

            if (catMapType.CategorySchemeRef != null)
            {
                if (catMapType.CategorySchemeRef.URN != null)
                {
                    this.SourceRef = new CrossReferenceImpl(this, catMapType.CategorySchemeRef.URN);
                }
                else
                {
                    this.SourceRef = new CrossReferenceImpl(
                        this, 
                        catMapType.CategorySchemeRef.AgencyID, 
                        catMapType.CategorySchemeRef.CategorySchemeID, 
                        catMapType.CategorySchemeRef.Version, 
                        SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CategoryScheme));
                }
            }

            if (catMapType.TargetCategorySchemeRef != null)
            {
                if (catMapType.TargetCategorySchemeRef.URN != null)
                {
                    this.TargetRef = new CrossReferenceImpl(this, catMapType.TargetCategorySchemeRef.URN);
                }
                else
                {
                    this.TargetRef = new CrossReferenceImpl(
                        this, 
                        catMapType.TargetCategorySchemeRef.AgencyID, 
                        catMapType.TargetCategorySchemeRef.CategorySchemeID, 
                        catMapType.TargetCategorySchemeRef.Version, 
                        SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CategoryScheme));
                }
            }

            if (catMapType.CategoryMap != null)
            {
                foreach (CategoryMapType catMap in catMapType.CategoryMap)
                {
                    ICategoryMap categoryMap = new CategoryMapCore(catMap, this);
                    this._categoryMaps.Add(categoryMap);
                }
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        /// <summary>
        ///     Gets the category maps.
        /// </summary>
        public virtual IList<ICategoryMap> CategoryMaps
        {
            get
            {
                return new List<ICategoryMap>(this._categoryMaps);
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The agencyScheme.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (ICategorySchemeMapObject)sdmxObject;
                if (!this.Equivalent(this._categoryMaps, that.CategoryMaps, includeFinalProperties))
                {
                    return false;
                }

                return this.DeepEqualsNameable(that, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        protected internal void Validate()
        {
            if (this.SourceRef == null)
            {
                throw new SdmxSemmanticException(
                    ExceptionCode.ObjectMissingRequiredElement, 
                    this.StructureType, 
                    "CategorySchemeRef");
            }

            if (this.TargetRef == null)
            {
                throw new SdmxSemmanticException(
                    ExceptionCode.ObjectMissingRequiredElement, 
                    this.StructureType, 
                    "TargetCategorySchemeRef");
            }

            if (this._categoryMaps == null)
            {
                throw new SdmxSemmanticException(
                    ExceptionCode.ObjectMissingRequiredElement, 
                    this.StructureType, 
                    "CategoryMap");
            }
        }

        /// <summary>
        ///     Get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._categoryMaps, composites);
            return composites;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECTS             //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATION                             //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
    }
}