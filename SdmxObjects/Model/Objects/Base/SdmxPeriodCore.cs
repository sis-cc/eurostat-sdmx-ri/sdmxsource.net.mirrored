namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Util.Date;
    using Org.Sdmxsource.Util;

    public class SdmxPeriodCore : ISdmxPeriod
    {
        private static readonly long serialVersionUID = 1L;
        private ISdmxDate _startPeriod;
        private ISdmxDate _endPeriod;

        private bool _isAllPeriods = false;
        public static readonly ISdmxPeriod ALL_PERIODS = new SdmxPeriodCore();



        private SdmxPeriodCore()
        {
            _isAllPeriods = true;
        }
     
        /// <summary>
        ///  Constructor taking string representations of dates, either can be null but at least one must be non-null.
        /// </summary>
        /// <param name="startPeriod"></param>
        /// <param name="endPeriod"></param>
        public SdmxPeriodCore(string startPeriod, string endPeriod)
        {
            if (ObjectUtil.ValidString(startPeriod))
            {
               _startPeriod = new SdmxDateCore(startPeriod);
            }
            if (ObjectUtil.ValidString(endPeriod))
            {
                // NOTE SDMXSOURCE-SDMXCORE CHANGE: passed startOfPeriod = false
                _endPeriod = new SdmxDateCore(endPeriod);
            }
            Validate();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="startPeriod"></param>
        /// <param name="endPeriod"></param>
        /// <param name="tf"></param>
        public SdmxPeriodCore(DateTime startPeriod, DateTime endPeriod, TimeFormat tf)
        {
            if (startPeriod != null)
            {
                // NOTE SDMXSOURCE-SDMXCORE CHANGE: passed startOfPeriod = true
                _startPeriod = new SdmxDateCore(startPeriod, tf);
            }
            if (endPeriod != null)
            {
                // NOTE SDMXSOURCE-SDMXCORE CHANGE: passed startOfPeriod = false
                _endPeriod = new SdmxDateCore(endPeriod, tf);
            }
            Validate();
        }

        public SdmxPeriodCore(ISdmxDate startPeriod, ISdmxDate endPeriod)
        {
            _startPeriod = startPeriod;
            _endPeriod = endPeriod;
            Validate();
        }

        private void Validate()
        {
            if (_startPeriod == null && _endPeriod == null)
            {
                throw new SdmxException("Can not create ISdmxPeriod - no _startPeriod or _endPeriod defined");
            }
            if (_startPeriod != null && _endPeriod != null)
            {
                if (_startPeriod.IsLater(_endPeriod))
                {
                    throw new SdmxSemmanticException(ExceptionCode.EndDateBeforeStartDate);
                }
            }
        }

        public bool IsAllPeriods()
        {
            return _isAllPeriods;
        }

        public ISdmxDate GetStartPeriod()
        {
            return _startPeriod;
        }

        public ISdmxDate GetEndPeriod()
        {
            return _endPeriod;
        }

        public PeriodOverlap GetOverlap(ISdmxPeriod that)
        {
            if (this.Equals(that))
            {
                return PeriodOverlap.ExactMatch;
            }
            if (_isAllPeriods)
            {
                return PeriodOverlap.Superset;
            }
            if (that == ALL_PERIODS)
            {
                return PeriodOverlap.Subset;
            }
            //This condition can be hit when the two periods have the same start and end but are in different frequencies
            if (this.IsStartEqual(that) && this.IsEndEqual(that))
            {
                return PeriodOverlap.ExactMatch;
            }
            if (!IsInPeriod(that))
            {
                return PeriodOverlap.NotInPeriod;
            }
            if (IsFullyInPeriod(that))
            {
                return PeriodOverlap.Superset;
            }
            if (that.IsFullyInPeriod(this))
            {
                return PeriodOverlap.Subset;
            }
            return PeriodOverlap.Overlap;
        }


        public ISet<ISdmxPeriod> Split(ISdmxPeriod that)
        {
            ISet<ISdmxPeriod> periods = new HashSet<ISdmxPeriod>();
            if (that == null)
            {
                periods.Add(this);
                return periods;
            }
            //Work out the least granular time format
            TimeFormat tf = null;
            if (this.HasStartPeriod())
            {
                tf = this.GetStartPeriod().TimeFormatOfDate;
            }
            if (this.HasEndPeriod())
            {
                TimeFormat thatTf = this.GetEndPeriod().TimeFormatOfDate;
                if (tf == null)
                {
                    tf = thatTf;
                }
                // NOTE SDMXSOURCE-SDMXCORE CHANGE: Core has periodsPerYear field in TimeFormat. Used existing functionality to get the same
                else if (DateUtil.GetNumberOfPeriods(thatTf) > DateUtil.GetNumberOfPeriods(tf))
                {
                    tf = thatTf;
                }
            }
            if (that.HasStartPeriod())
            {
                TimeFormat thatTf = that.GetStartPeriod().TimeFormatOfDate;
                if (tf == null)
                {
                    tf = thatTf;
                }
                else if (DateUtil.GetNumberOfPeriods(thatTf) > DateUtil.GetNumberOfPeriods(tf))
                {
                    tf = thatTf;
                }
            }
            if (that.HasEndPeriod())
            {
                TimeFormat thatTf = that.GetEndPeriod().TimeFormatOfDate; 
                if (tf == null)
                {
                    tf = thatTf;
                }
                else if (DateUtil.GetNumberOfPeriods(thatTf) > DateUtil.GetNumberOfPeriods(tf))
                {
                    tf = thatTf;
                }
            }
            DateTime? start1, start2, start3, end1, end2, end3;
            Nullable<DateTime> nullDate = null;
            bool thisIsSubset = false;
            switch (GetOverlap(that))
            {
                case PeriodOverlap.ExactMatch:
                    periods.Add(this);
                    break;
                case PeriodOverlap.NotInPeriod:
                    periods.Add(this);
                    periods.Add(that);
                    break;
                case PeriodOverlap.Overlap:
                    //Create three new periods; before, intesection, after
                    //Example 2010 - 2015 overlaps with 2011-2016
                    //Split 2010-2010, 2011-2015, 2016-2016
                    //Determine which period comes first and which second
                    ISdmxPeriod earlierPeriod = this.IsBeforePeriod(that) ? this : that;
                    ISdmxPeriod laterPeriod = this == earlierPeriod ? that : this;
                   
                    start1 = earlierPeriod.HasStartPeriod() ? earlierPeriod.GetStartPeriod().Date : nullDate;
                    start2 = laterPeriod.GetStartPeriod().Date;

                    end1 = start2?.AddMilliseconds(1000); // end1 = new DateTime(start2.getTime() - 1000); //minus 1 millisecond from the start of the second period
                    end2 = earlierPeriod.GetEndPeriod().Date;

                    start3 = end2?.AddMilliseconds(1000); //new DateTime(end2.TimeOfDay + 1000); //add 1 millisecond from the end of the second period
                    end3 = laterPeriod.HasEndPeriod() ? laterPeriod.GetEndPeriod().Date : nullDate;

                    periods.Add(new SdmxPeriodCore(start1.Value, end1.Value, tf));
                    periods.Add(new SdmxPeriodCore(start2.Value, end2.Value, tf));
                    periods.Add(new SdmxPeriodCore(start3.Value, end3.Value, tf));

                    break;
                case PeriodOverlap.Subset:
                    thisIsSubset = true;
                    break;
                case PeriodOverlap.Superset:
                    //This period is fully contained in that period or vica verca example
                    //This = 2010-2011, that= 2009-2012
                    //Split 2009-2009, 2010-2011, 2012-2012

                    //Note an overlap includes where start or end period match, for example
                    //This = 2010-2011, that= 2010-2012
                    //Split 2010-2011, 2012-2012
                    ISdmxPeriod outerPeriod = thisIsSubset ? that : this;
                    ISdmxPeriod innerPeriod = thisIsSubset ? this : that;
                    if (IsStartEqual(that) || IsEndEqual(that))
                    {
                        start1 = outerPeriod.HasStartPeriod() ? outerPeriod.GetStartPeriod().Date : nullDate;
                        if (this.IsStartEqual(that))
                        {
                            end1 = innerPeriod.GetEndPeriod().Date;
                        }
                        else
                        {
                            end1 = (innerPeriod.GetStartPeriod().Date.AddMilliseconds(-1000));
                        }
                        start2 = end1?.AddMilliseconds(1000); //add 1 millisecond from the end of the first period
                        end2 = outerPeriod.HasEndPeriod() ? outerPeriod.GetEndPeriod().Date : nullDate;
                        periods.Add(new SdmxPeriodCore(start1.Value, end1.Value, tf));
                        periods.Add(new SdmxPeriodCore(start2.Value, end2.Value, tf));
                    }
                    else
                    {
                        start1 = outerPeriod.HasStartPeriod() ? outerPeriod.GetStartPeriod().Date : nullDate;
                        start2 = innerPeriod.GetStartPeriod().Date;

                        //minus 1 millisecond from the start of the second period
                        end1 = start2?.AddMilliseconds(-1000);
                        end2 = innerPeriod.GetEndPeriod().Date;

                        //start3 = new Date(end2.getTime() + 1000); //add 1 millisecond from the end of the second period
                        start3 = end2?.AddMilliseconds(1000);
                        end3 = outerPeriod.HasEndPeriod() ? outerPeriod.GetEndPeriod().Date : nullDate;

                        periods.Add(new SdmxPeriodCore(start1.Value, end1.Value, tf));
                        periods.Add(new SdmxPeriodCore(start2.Value, end2.Value, tf));
                        periods.Add(new SdmxPeriodCore(start3.Value, end3.Value, tf));
                    }
                    break;
            }
            return periods;
        }

        public bool IsStartEqual(ISdmxPeriod period)
        {
            if (this.GetStartPeriod() == null && period.GetStartPeriod() == null)
            {
                return true;
            }
            if (this.GetStartPeriod() == null || period.GetStartPeriod() == null)
            {
                return false;
            }
            return this.GetStartPeriod().IsEarlier(period.GetStartPeriod());           
        }


        public  bool IsBeforePeriod(ISdmxPeriod period)
        {
            //This has no start period so it is before the other period if that does have one
            if (!this.HasStartPeriod())
            {
                return period.HasStartPeriod() == true;
            }
            //If the passed in period has no start period then it is before this one
            if (!period.HasStartPeriod())
            {
                return false;
            }
            //Both have a start period
            return this.GetStartPeriod().IsEarlier(period.GetStartPeriod());
        }


        public bool IsEndEqual(ISdmxPeriod period)
        {
            if (this.GetEndPeriod() == null && period.GetEndPeriod() == null)
            {
                return true;
            }
            if (this.GetEndPeriod() == null || period.GetEndPeriod() == null)
            {
                return false;
            }
            return this.GetEndPeriod().Date.TimeOfDay == period.GetEndPeriod().Date.TimeOfDay;
        }


        public bool IsAfterPeriod(ISdmxPeriod period)
        {
            //This has no start period so it is before the other period if that does have one
            if (!this.HasEndPeriod())
            {
                return period.HasEndPeriod() == true;
            }
            //If the passed in period has no start period then it is before this one
            if (!period.HasEndPeriod())
            {
                return false;
            }
            //Both have a start period
            return this.GetEndPeriod().IsLater(period.GetEndPeriod());
        }


        public bool IsInPeriod(ISdmxPeriod period)
        {
            Nullable<DateTime> nullDate = null;
            if (_isAllPeriods || period == ALL_PERIODS)
            {
                return true;
            }
            if (period != null)
            {
                return IsInPeriod((DateTime)(period.HasEndPeriod() ? period.GetStartPeriod().Date : nullDate),
                    (DateTime)(period.HasEndPeriod() ? period.GetEndPeriod().Date : nullDate));                                 
            }
            return false;
        }


        public bool IsFullyInPeriod(ISdmxPeriod period)
        {
            if (period == null)
            {
                return false;
            }
            bool isOutsidePeriod = period.HasStartPeriod() ? IsBeforeStartPeriod(period.GetStartPeriod().Date) : this.HasStartPeriod();
            if (isOutsidePeriod)
            {
                return false;
            }
            isOutsidePeriod = period.HasEndPeriod() ? IsAfterEndPeriod(period.GetEndPeriod().Date) : this.HasEndPeriod();
            return !isOutsidePeriod;
        }


        public bool HasStartPeriod()
        {
            return _startPeriod != null;
        }


        public bool HasEndPeriod()
        {
            return _endPeriod != null;
        }


        public bool IsInPeriod(DateTime dateFrom, DateTime dateTo)
        {
            if (IsAllPeriods())
            {
                return true;
            }
            if ((dateTo != null && IsBeforeStartPeriod(dateTo)) || (dateFrom != null && IsAfterEndPeriod(dateFrom)))
            {
                return false;
            }
            return true;
        }


        public bool IsBeforeStartPeriod(DateTime date)
        {
            if (IsAllPeriods())
            {
                return false;
            }
            if (date != null)
            {
                if (!HasStartPeriod())
                {
                    return false;
                }
                return date < GetStartPeriod().Date;
            }
            return _startPeriod != null;
        }


        public bool IsAfterEndPeriod(DateTime date)
        {
            if (IsAllPeriods())
            {
                return false;
            }
            if (date != null)
            {
                if (!HasEndPeriod())
                {
                    return false;
                }
                return date > GetEndPeriod().Date;
            }
            return false;
        }


        public bool IsInPeriod(DateTime date)
        {
            if (_isAllPeriods)
            {
                return true;
            }
            if (HasStartPeriod())
            {
                if (date < GetStartPeriod().Date)
                {
                    return false;
                }
            }
            if (HasEndPeriod())
            {
                if (date > GetEndPeriod().Date)
                {
                    return false;
                }
            }
            return true;
        }



        public int CompareTo(ISdmxPeriod o)
        {
            if (o == this)
            {
                return 0;
            }
            if (_startPeriod == null && o.GetStartPeriod() != null)
            {
                return -1;
            }
            if (_startPeriod != null && o.GetStartPeriod() == null)
            {
                return 1;
            }
            if (_startPeriod != null && o.GetStartPeriod() != null)
            {
                int result = _startPeriod.Date.CompareTo(o.GetStartPeriod().Date);
                if (result != 0)
                {
                    return result;
                }
            }
            if (_endPeriod == null && o.GetEndPeriod() != null)
            {
                return 1;
            }
            if (_endPeriod != null && o.GetEndPeriod() == null)
            {
                return -1;
            }
            if (_endPeriod != null && o.GetEndPeriod() != null)
            {
                int result = _endPeriod.Date.CompareTo(o.GetEndPeriod().Date);
                if (result != 0)
                {
                    return result;
                }
            }
            return 0;
        }


        public int HashCode()
        {
            return ToString().GetHashCode();
        }

        public bool Equals(object obj)
        {
            if (obj is ISdmxPeriod)
            {
                ISdmxPeriod that = (ISdmxPeriod)obj;
                if (!ObjectUtil.Equivalent(that.GetStartPeriod(), this.GetStartPeriod()))
                {
                    return false;
                }
                if (!ObjectUtil.Equivalent(that.GetEndPeriod(), this.GetEndPeriod()))
                {
                    return false;
                }
                return true;
            }
            return false;
        }

        /// <summary>
        ///  
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (_isAllPeriods)
            {
                return "[All Periods]";
            }
            StringBuilder sb = new StringBuilder();
            string _startPeriod = HasStartPeriod() ? GetStartPeriod().DateInSdmxFormat : "[no start]";
            string _endPeriod = HasEndPeriod() ? GetEndPeriod().DateInSdmxFormat : "[no end]";
            sb.Append(_startPeriod + " - " + _endPeriod);
            return sb.ToString();
        }

        /// <summary>
        ///  Returns a period string in the syntax &lt;start period&gt;/&lt;end period&gt;, example 2001/2009.
        /// </summary>
        /// <returns>A period string in the specified format.</returns>
        public string ToISOString()
        {
            if (IsAllPeriods())
            {
                return "AllPeriods";
            }
            if (HasStartPeriod() && HasEndPeriod())
            {
                return GetStartPeriod().DateInSdmxFormat + "/" + GetEndPeriod().DateInSdmxFormat;
            }
            if (HasStartPeriod())
            {
                return GetStartPeriod().DateInSdmxFormat + "/";
            }
            if (HasEndPeriod())
            {
                return "/" + GetEndPeriod().DateInSdmxFormat;
            }
            return "AllPeriods";
        }
    }
}
