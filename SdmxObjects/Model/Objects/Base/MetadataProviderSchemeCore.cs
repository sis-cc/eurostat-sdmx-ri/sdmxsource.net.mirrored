// -----------------------------------------------------------------------
// <copyright file="MetadataProviderSchemeCore.cs" company="EUROSTAT">
//   Date Created : 2024-04-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
using System;
using System.Collections.Generic;
using System.Text;

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using log4net.Repository.Hierarchy;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Diff;
    using System.Security.Policy;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using log4net;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Diff;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;

    /// <summary>
    /// Metadata Provider Scheme Core
    /// </summary>
    public class MetadataProviderSchemeCore : OrganisationSchemeCore<IMetadataProvider,IMetadataProviderScheme,IMetadataProviderSchemeMutable,IMetadataProviderMutable>, IMetadataProviderScheme
    {
        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(typeof(OrganisationUnitSchemeObjectCore));
        private const long serialVersionUID = 168L;

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////BUILD FROM ITSELF, CREATES STUB BEAN ////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        private MetadataProviderSchemeCore(IMetadataProviderScheme agencyScheme, Uri actualLocation, bool isServiceUrl)
            : base(agencyScheme, actualLocation, isServiceUrl)
        {
            Log.Debug("Stub IDataConsumerScheme Built");
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////BUILD FROM MUTABLE BEANS /////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        public MetadataProviderSchemeCore(IMetadataProviderSchemeMutable bean) : base(bean)
        {
            Log.Debug("Building DataProviderSchemeBean from Mutable Bean");
            if (bean.Items != null)
            {
                foreach (IMetadataProviderMutable currentBean in bean.Items)
                {
                    this.AddInternalItem(new MetadataProviderCore(currentBean, this));
                }
            }
            if (Log.IsDebugEnabled)
            {
                Log.Debug("MetadataProviderSchemeMutableBean Built " + this.Urn);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////DEEP VALIDATION //////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties, IDiffReport diff)
        {
            if (sdmxObject == null)
            {
                return false;
            }
            if (sdmxObject.StructureType == this.StructureType)
            {
                return base.DeepEqualsInternal((IMetadataProviderScheme)sdmxObject, includeFinalProperties, "Metadata Provider", diff);
            }
            return false;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="actualLocation"></param>
        /// <param name="isServiceUrl"></param>
        /// <returns></returns>
        public override IMetadataProviderScheme GetStub(Uri actualLocation, bool isServiceUrl)
        {
            return new MetadataProviderSchemeCore(this, actualLocation, isServiceUrl);
        }


        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public override IMetadataProviderSchemeMutable MutableInstance
        {
            get
            {
                return new MetadataProviderSchemeMutableCore(this);
            }
        }

    }
}
