using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base
{
    /// <summary>
    /// The default implementation of <see cref="IRepresentedObject"/>.
    /// Can be used as an encapsulated class in other classes implementing the <see cref="IRepresentedObject"/>.
    /// </summary>
    public class RepresentedObjectCore
    {
        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="representation">The representation to be used.</param>
        public RepresentedObjectCore(IRepresentation representation)
        {
            Representation = representation;
        }

        /// <summary> 
        /// Gets the minimum allowed occurrences for an attribute or measure value in a dataset 
        /// This is just a shortcut to <see cref="IRepresentation.MinOccurs"/>
        /// Introduced in SDMX 3.0.0 
        /// </summary>
        /// <returns>
        /// the minimum number of a value that must be reported; default is 1 
        /// </returns> 
        public int RepresentationMinOccurs
        {
            get
            {
                if (Representation?.MinOccurs != null)
                {
                    return Representation.MinOccurs.Value;
                }
                return 1;
            }
        }

        /// <summary> 
        /// The maxOccurs attribute indicates the maximum number of values that can be reported for the component. 
        /// This is just a shortcut to <see cref="IRepresentation.MaxOccurs"/>
        /// Introduced in SDMX 3.0.0 
        /// When it is larger than 1, then it means that the component values are to reported as an array in the dataset 
        /// </summary>
        /// <returns>
        /// the maximum number of a value that must be reported; default is 1 
        /// </returns>
        public IOccurenceObject RepresentationMaxOccurs
        {
            get
            {
                if (Representation != null && Representation.MaxOccurs != null)
                {
                    return Representation.MaxOccurs;
                }
                return new FiniteOccurenceObjectCore(1);
            }
        }

        /// <summary> 
        /// </summary>
        /// <returns>
        /// true if the text format allows for multilingual text 
        /// </returns> 
        public bool IsMultilingual
        {
            get
            {
                if (TextFormat == null)
                {
                    return false;
                }
                return TextFormat.Multilingual == TertiaryBoolEnumType.True;
            }
        }

        // public bool IsUnboundedMax() {}

        /// <summary> 
        /// Returns the text format, which is typically (but does not have to be) present for an uncoded component (one which does not reference an enumerated list). 
        /// 
        /// Returns null if there is no text format present 
        /// </summary>
        public ITextFormat TextFormat
        {
            get
            {
                if (Representation == null)
                {
                    return null;
                }
                return Representation.TextFormat;
            }
        }

        /// <summary> 
        /// Returns the text type, which is typically (but does not have to be) present for an uncoded component (one which does not reference an enumerated list). 
        /// 
        /// Returns null if there is no text format present 
        /// </summary>
        public TextType TextType
        {
            get
            {
                if (TextFormat == null)
                {
                    return null;
                }
                return TextFormat.TextType;
            }
        }

        /// <summary> 
        /// Returns the core representation for this concept. 
        /// </summary>
        /// <returns>
        /// null if no core representation is defined 
        /// </returns> 
        [Obsolete("use Representation instead")]
        public IRepresentation CoreRepresentation => Representation;


        /// <summary> 
        /// </summary>
        /// <returns>
        /// reference to an enumerated list of allowable values, or null if there is no such reference 
        /// </returns> 
        public ICrossReference EnumeratedRepresentation
        {
            get
            {
                if (Representation == null)
                {
                    return null;
                }
                return Representation.Representation;
            }
        }

        /// <summary> 
        /// </summary>
        /// <returns>
        /// the representation for this component, returns null if there is no representation 
        /// </returns> 
        public IRepresentation Representation { get; }

        /// <summary> 
        /// Return true if this identifier component has coded representation 
        /// </summary> 
        /// <returns></returns>
        public bool HasCodedRepresentation()
        {
            return EnumeratedRepresentation != null;
        }
    }
}
