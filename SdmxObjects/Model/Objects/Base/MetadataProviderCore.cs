// -----------------------------------------------------------------------
// <copyright file="MetadataProviderCore.cs" company="EUROSTAT">
//   Date Created : 2024-04-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
using System;
using System.Collections.Generic;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Model.Diff;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Diff;

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base
{
    public class MetadataProviderCore : OrganisationCore<IMetadataProvider>,IMetadataProvider 
    {
        public MetadataProviderCore(IMetadataProviderMutable bean, IMetadataProviderScheme parent) : base(bean, parent)
        {
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////DEEP VALIDATION //////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        public override bool DeepEquals(ISdmxObject bean, bool includeFinalProperties, IDiffReport diff)
        {
            if (bean == null)
            {
                return false;
            }
            if (bean.StructureType == this.StructureType)
            {
                return base.DeepEqualsInternal((IMetadataProvider)bean, includeFinalProperties, diff);
            }
            return false;
        }

        public IList<ICrossReference> CrossReferencedConstrainables
        {
            get
            {
                return new List<ICrossReference>();
            }
        }

        public IMetadataProviderScheme MaintainableParent
        {
            get
            {
                return (IMetadataProviderScheme)base.MaintainableParent;
            }
        }
    }
}
