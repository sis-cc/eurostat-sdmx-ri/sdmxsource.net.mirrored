// -----------------------------------------------------------------------
// <copyright file="UnboundedOccurenceObjectCore.cs" company="EUROSTAT">
//   Date Created : 2021-10-19
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base
{
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    /// An implementation for unbounded occurrence
    /// For bounded <see cref="FiniteOccurenceObjectCore"/>.
    /// </summary>
    public class UnboundedOccurenceObjectCore : IOccurenceObject
    {
        private static readonly IOccurenceObject _instance = new UnboundedOccurenceObjectCore();

        /// <summary>
        /// Unbounded occurrence instance
        /// </summary>
        public static IOccurenceObject Instance => _instance;

        /// <summary>
        /// Its the unbounded implementation, so this throws a <see cref="SdmxSemmanticException"/>.
        /// </summary>
        public int Occurrences => throw new SdmxSemmanticException("Unbounded occurrence, the number is not set");

        /// <summary>
        /// Gets a value indicating whether the maximum occurrence is unbounded
        /// Always true, since it is the unbounded implementation.
        /// </summary>
        public bool IsUnbounded => true;

        /// <summary>
        /// Compares two occurences for equality
        /// </summary>
        /// <param name="o">The occurence object to compare to.</param>
        /// <returns><c>true</c> if found equal, <c>false</c> otherwise.</returns>
        public override bool Equals(object o)
        {
            if (this == o)
            {
                return true;
            }

            return o != null && this.GetType() == o.GetType();
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return this.IsUnbounded.GetHashCode();
        }

        /// <summary>
        /// The string representation of the object
        /// </summary>
        /// <returns>The literal for unbounded</returns>
        public override string ToString()
        {
            return "unbounded";
        }
    }
}
