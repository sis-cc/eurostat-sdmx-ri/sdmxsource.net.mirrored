// -----------------------------------------------------------------------
// <copyright file="AnnotableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base
{
    #region Using directives

    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Util.Attributes;

    using AnnotationsType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common.AnnotationsType;

    #endregion

    /// <summary>
    ///     The annotable core.
    /// </summary>
    [Serializable]
    public abstract class AnnotableCore : SdmxStructureCore, IAnnotableObject
    {
        /// <summary>
        ///     The annotations.
        /// </summary>
        private readonly IList<IAnnotation> _annotations = new List<IAnnotation>();

        /// <summary>
        ///     Initializes a new instance of the <see cref="AnnotableCore" /> class.
        /// </summary>
        /// <param name="agencyScheme"> The agencyScheme. </param>
        protected internal AnnotableCore([ValidatedNotNull]IAnnotableObject agencyScheme)
            : base(agencyScheme)
        {
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECT                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////    

        /// <summary>
        ///     Initializes a new instance of the <see cref="AnnotableCore" /> class.
        /// </summary>
        /// <param name="mutableObject"> The mutable object. </param>
        /// <param name="parent"> The parent. </param>
        protected internal AnnotableCore(IAnnotableMutableObject mutableObject, ISdmxStructure parent)
            : base(mutableObject, parent)
        {
            if (mutableObject != null && mutableObject.Annotations != null)
            {
                foreach (IAnnotationMutableObject currentAnnotation in mutableObject.Annotations)
                {
                    this._annotations.Add(new AnnotationObjectCore(currentAnnotation, this));
                }
            }
        }
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////    

        /// <summary>
        ///     Initializes a new instance of the <see cref="AnnotableCore" /> class.
        /// </summary>
        /// <param name="structureType"> The structure type. </param>
        /// <param name="parent"> The parent. </param>
        protected internal AnnotableCore(SdmxStructureType structureType, ISdmxStructure parent)
            : base(structureType, parent)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AnnotableCore" /> class.
        /// </summary>
        /// <param name="createdFrom"> The created from. </param>
        /// <param name="structureType"> The structure type. </param>
        /// <param name="parent"> The parent. </param>
        protected internal AnnotableCore(
            AnnotableType createdFrom, 
            SdmxStructureType structureType, 
            ISdmxStructure parent)
            : base(structureType, parent)
        {
            if (createdFrom != null)
            {
                Annotations annotations1 = createdFrom.Annotations;
                if (annotations1 != null && annotations1.Annotation != null)
                {
                    IList<AnnotationType> annotationType = annotations1.Annotation;
                    if (annotationType != null)
                    {
                        foreach (AnnotationType currentAnnotation in annotationType)
                        {
                            this._annotations.Add(new AnnotationObjectCore(currentAnnotation, this));
                        }
                    }
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////    

        /// <summary>
        ///     Initializes a new instance of the <see cref="AnnotableCore" /> class.
        /// </summary>
        /// <param name="annotationType"> The annotation type. </param>
        /// <param name="structureType"> The structure type. </param>
        /// <param name="parent"> The parent. </param>
        protected internal AnnotableCore(
            AnnotationsType annotationType, 
            SdmxStructureType structureType, 
            ISdmxStructure parent)
            : base(structureType, parent)
        {
            if (annotationType != null && annotationType.Annotation != null)
            {
                foreach (Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common.AnnotationType currentAnnotation in
                    annotationType.Annotation)
                {
                    this._annotations.Add(new AnnotationObjectCore(currentAnnotation, this));
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////    

        /// <summary>
        ///     Initializes a new instance of the <see cref="AnnotableCore" /> class.
        /// </summary>
        /// <param name="annotationType"> The annotation type. </param>
        /// <param name="structureType"> The structure type. </param>
        /// <param name="parent"> The parent. </param>
        protected internal AnnotableCore(
            Org.Sdmx.Resources.SdmxMl.Schemas.V10.Common.AnnotationsType annotationType, 
            SdmxStructureType structureType, 
            ISdmxStructure parent)
            : base(structureType, parent)
        {
            if (annotationType != null && annotationType.Annotation != null)
            {
                foreach (Org.Sdmx.Resources.SdmxMl.Schemas.V10.Common.AnnotationType currentAnnotation in
                    annotationType.Annotation)
                {
                    this._annotations.Add(new AnnotationObjectCore(currentAnnotation, this));
                }
            }
        }

        /// <summary>
        ///     Gets the annotations.
        /// </summary>
        public virtual IList<IAnnotation> Annotations
        {
            get
            {
                return new List<IAnnotation>(this._annotations);
            }
        }

        /// <summary>
        ///     Returns the annotations with the given title, returns an empty Set is no annotations exist that have a type which
        ///     matches the given string
        /// </summary>
        /// <param name="title">The title.</param>
        /// <returns>
        ///     A set of annotations
        /// </returns>
        public ISet<IAnnotation> GetAnnotationsByTitle(string title)
        {
            ISet<IAnnotation> returnSet = new HashSet<IAnnotation>();
            foreach (IAnnotation currentAnnotation in this._annotations)
            {
                if (currentAnnotation.Title != null && currentAnnotation.Title.Equals(title))
                {
                    returnSet.Add(currentAnnotation);
                }
            }

            return returnSet;
        }

        /// <summary>
        ///     Returns the annotations with the given type, returns an empty Set is no annotations exist that have a type which
        ///     matches the given string
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>
        ///     A set of annotations
        /// </returns>
        public ISet<IAnnotation> GetAnnotationsByType(string type)
        {
            ISet<IAnnotation> returnSet = new HashSet<IAnnotation>();
            foreach (IAnnotation currentAnnotation in this._annotations)
            {
                if (currentAnnotation.Type != null && currentAnnotation.Type.Equals(type))
                {
                    returnSet.Add(currentAnnotation);
                }
            }

            return returnSet;
        }

        /// <summary>
        ///     The has annotation type.
        /// </summary>
        /// <param name="annoationType"> The annoation type. </param>
        /// <returns> The <see cref="bool" /> . </returns>
        public virtual bool HasAnnotationType(string annoationType)
        {
            return this.GetAnnotationsByType(annoationType).Count > 0;
        }

        /// <summary>
        ///     The deep equals internal.
        /// </summary>
        /// <param name="annotableObject">The agencyScheme.</param>
        /// <param name="includeFinalProperties">if set to <c>true</c> [include final properties].</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        protected internal bool DeepEqualsInternalAnnotable(
            IAnnotableObject annotableObject, 
            bool includeFinalProperties)
        {
            if (annotableObject == null)
            {
                return false;
            }

            if (includeFinalProperties)
            {
                IList<IAnnotation> thatAnnotations = annotableObject.Annotations;
                if (!this.Equivalent(thatAnnotations, this._annotations, includeFinalProperties))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        ///     The get composites internal.
        /// </summary>
        /// <returns>The composites</returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._annotations, composites);
            return composites;
        }
    }
}