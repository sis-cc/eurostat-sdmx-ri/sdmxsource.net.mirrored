// -----------------------------------------------------------------------
// <copyright file="DataProviderCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Util.Extensions;

    using OrganisationType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.OrganisationType;

    /// <summary>
    ///     The data provider core.
    /// </summary>
    [Serializable]
    public class DataProviderCore : OrganisationCore<IDataProvider>, IDataProvider
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="DataProviderCore" /> class.
        /// </summary>
        /// <param name="itemMutableObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public DataProviderCore(IDataProviderMutableObject itemMutableObject, IDataProviderScheme parent)
            : base(itemMutableObject, parent)
        {
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataProviderCore" /> class.
        /// </summary>
        /// <param name="type">
        ///     The type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public DataProviderCore(DataProviderType type, IDataProviderScheme parent)
            : base(type, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataProvider), parent)
        {
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataProviderCore" /> class.
        /// </summary>
        /// <param name="organisation">
        ///     The sdmxObject.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public DataProviderCore(OrganisationType organisation, IDataProviderScheme parent)
            : base(
                organisation, 
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataProvider), 
                organisation.PassNoNull("organisation").CollectorContact, 
                organisation.PassNoNull("organisation").id, 
                organisation.PassNoNull("organisation").uri, 
                organisation.PassNoNull("organisation").Name, 
                organisation.PassNoNull("organisation").Description, 
                organisation.PassNoNull("organisation").Annotations, 
                parent)
        {
        }

        /// <summary>
        ///     Gets the cross referenced constrainables.
        /// </summary>
        public virtual IList<ICrossReference> CrossReferencedConstrainables
        {
            get
            {
                return new List<ICrossReference>();
            }
        }

        /// <summary>
        ///     Gets the maintainable parent.
        /// </summary>
        public new IDataProviderScheme MaintainableParent
        {
            get
            {
                return (IDataProviderScheme)base.MaintainableParent;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">The sdmxObject.</param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                return this.DeepEqualsNameable((IDataProvider)sdmxObject, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     The get composites internal.
        /// </summary>
        /// <returns>The set of composites.</returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            return new HashSet<ISdmxObject>();
        }
    }
}