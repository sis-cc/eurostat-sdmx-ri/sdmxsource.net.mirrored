// -----------------------------------------------------------------------
// <copyright file="SentinelValueCore.cs" company="EUROSTAT">
//   Date Created : 2021-10-19
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base
{
    using System.Collections.Generic;
    using System.Linq;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Sdmx.Util.Objects;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// The base implementation of the <see cref="ISentinelValue"/> interface.
    /// </summary>
    public class SentinelValueCore : SdmxObjectCore, ISentinelValue
    {
        private readonly string _value;

        private readonly IList<ITextTypeWrapper> _name = new List<ITextTypeWrapper>();

        private readonly IList<ITextTypeWrapper> _description = new List<ITextTypeWrapper>();

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE BEAN				 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Initializes a new instance of the <see cref="SentinelValueCore"/> class.
        /// </summary>
        /// <param name="bean">The mutable object to build from</param>
        /// <param name="parent">The parent sdmx object.</param>
        public SentinelValueCore(ISentinelValueMutableObject bean, ISdmxObject parent)
            :base(bean, parent)
        {
            if (bean.Names != null)
            {
                foreach (ITextTypeWrapperMutableObject mutable in bean.Names)
                {
                    if (ObjectUtil.ValidString(mutable.Value))
                    {
                        this._name.Add(new TextTypeWrapperImpl(mutable, this));
                    }
                }
            }
            if (bean.Descriptions != null)
            {
                foreach (ITextTypeWrapperMutableObject mutable in bean.Descriptions)
                {
                    if (ObjectUtil.ValidString(mutable.Value))
                    {
                        this._description.Add(new TextTypeWrapperImpl(mutable, this));
                    }
                }
            }
            this._value = bean.Value;

            ValidateNameableAttributes();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATION							 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Validates name and description
        /// </summary>
        protected void ValidateNameableAttributes()
        {
            if (!ObjectUtil.ValidString(this._value)) {
                throw new SdmxSemmanticException("Sentinel Values requires a value");
            }
            if (this._name == null || this._name.Count == 0) {
                throw new SdmxSemmanticException(ExceptionCode.StructureIdentifiableMissingName, "Sentinel Values requires a name");
            }
            ValidationUtil.ValidateTextType(this._name, "");
            ValidationUtil.ValidateTextType(this._description, null);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS							 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Compares this object to another for equality.
        /// </summary>
        /// <param name="obj">the object to compare to.</param>
        /// <param name="includeFinalProperties">indicates whether to compare final properties.</param>
        /// <returns><c>true</c> if found equal, <c>false</c> otherwise.</returns>
        public override bool DeepEquals(ISdmxObject obj, bool includeFinalProperties)
        {
            if (obj == null)
            {
                return false;
            }
            if (obj.StructureType == this.StructureType)
            {
                ISentinelValue that = (ISentinelValue)obj;
                if (!ObjectUtil.Equivalent(_value, that.Value))
                {
                    return false;
                }

                //If the user has choosen not to include final properties, then ignore the name and description
                if (includeFinalProperties)
                {
                    List<ITextTypeWrapper> beanNameOrdered = that.Names.OrderBy(t => t.Locale).ToList();
                    List<ITextTypeWrapper> thisNameOrdered = this._name.OrderBy(t => t.Locale).ToList();
                    if (!this.Equivalent(thisNameOrdered, beanNameOrdered, includeFinalProperties))
                    {
                        return false;
                    }
                    List<ITextTypeWrapper> beanDescOrdered = that.Descriptions.OrderBy(t => t.Locale).ToList();
                    List<ITextTypeWrapper> thisDescOrdered = this._description.OrderBy(t => t.Locale).ToList();
                    if (!this.Equivalent(thisDescOrdered, beanDescOrdered, includeFinalProperties))
                    {
                        return false;
                    }
                }
                return this.DeepEqualsInternal(that);
            }

            return true;
        }


        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////COMPOSITES								 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////	
        /// <summary>
        /// Get composites
        /// </summary>
        /// <returns></returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = new HashSet<ISdmxObject>();
            this.AddToCompositeSet(this._name, composites);
            this.AddToCompositeSet(this._description, composites);
            return composites;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////GETTERS								 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////	
        
        /// <inheritdoc/>
        public string Value => this._value;

        /// <inheritdoc/>
        public IList<ITextTypeWrapper> Names => new List<ITextTypeWrapper>(this._name);

        /// <inheritdoc/>
        public IList<ITextTypeWrapper> Descriptions => new List<ITextTypeWrapper>(this._description);
    }
}
