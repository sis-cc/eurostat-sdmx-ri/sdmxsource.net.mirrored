// -----------------------------------------------------------------------
// <copyright file="SdmxObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Diff;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Attributes;
    using Org.Sdmxsource.Util.Extensions;
    using Org.Sdmxsource.Util.Reflect;

    /// <summary>
    ///     The sdmx object core.
    /// </summary>
    public abstract class SdmxObjectCore : ISdmxObject
    {
        /// <summary>
        ///     The _parent.
        /// </summary>
        private readonly ISdmxObject _parent;

        /// <summary>
        ///     The composites.
        /// </summary>
        private ISet<ISdmxObject> _composites;

        /// <summary>
        ///     The cross references.
        /// </summary>
        private ISet<ICrossReference> _crossReferences;

        /// <summary>
        ///     All the cross references
        /// </summary>
        private IList<ICrossReference> _allCrossReferences;

        /// <summary>
        ///     The structure type.
        /// </summary>
        private SdmxStructureType _structureType;
        private IReadOnlyList<string> _supportedLanguages;

        /// <summary>
        /// Needed for SupportedLanguages
        /// </summary>
        private readonly object _synchronisationLock = new object();

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxObjectCore" /> class.
        /// </summary>
        /// <param name="structureType0">
        ///     The structure type 0.
        /// </param>
        /// <param name="parent1">
        ///     The parent 1.
        /// </param>
        protected internal SdmxObjectCore(SdmxStructureType structureType0, ISdmxObject parent1)
        {
            this._structureType = structureType0;
            this._parent = parent1;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxObjectCore" /> class.
        /// </summary>
        /// <param name="agencyScheme">
        ///     The sdmxObject.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        protected internal SdmxObjectCore([ValidatedNotNull]ISdmxObject agencyScheme)
        {
            if (agencyScheme == null)
            {
                throw new SdmxSemmanticException(ExceptionCode.ObjectNull, "object in constructor");
            }

            this._structureType = agencyScheme.StructureType;
            this._parent = agencyScheme.Parent;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECT                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxObjectCore" /> class.
        /// </summary>
        /// <param name="mutableObject">
        ///     The mutable object.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        protected SdmxObjectCore(IMutableObject mutableObject, ISdmxObject parent)
        {
            if (mutableObject == null)
            {
                throw new SdmxSemmanticException(ExceptionCode.ObjectNull, "object in constructor");
            }

            this._structureType = mutableObject.StructureType;
            this._parent = parent;
        }

        /// <summary>
        ///     Gets the composites.
        /// </summary>
        public virtual ISet<ISdmxObject> Composites
        {
            get
            {
                if (this._composites == null)
                {
                    this._composites = this.GetCompositesInternal();
                }

                return new HashSet<ISdmxObject>(this._composites);
            }
        }

        /// <summary>
        ///     Gets the cross references.
        /// </summary>
        public virtual ISet<ICrossReference> CrossReferences
        {
            get
            {
                if (this._crossReferences != null)
                {
                    return new HashSet<ICrossReference>(this._crossReferences);
                }

                var reflectCrossReference = new ReflectUtil<ICrossReference>();
                ISet<ICrossReference> returnSet = reflectCrossReference.GetCompositeObjects(this,
                    this.GetType().GetProperty("CrossReferences"),
                    this.GetType().GetProperty("AllCrossReferences"));
                returnSet.Remove(null);
                if (this.StructureType.IsMaintainable)
                {
                    ISet<ISdmxObject> compositSet = this.Composites;

                    foreach (ISdmxObject currentComposite in compositSet)
                    {
                        if (!ReferenceEquals(this, currentComposite))
                        {
                            returnSet.AddAll(currentComposite.CrossReferences);
                        }

                        ////currentComposite.CrossReferences.AddAll(returnSet);
                    }
                }

                this._crossReferences = returnSet;
                return returnSet;
            }
        }

        /// <summary>
        /// Gets all the cross references.
        /// </summary>
        public virtual IList<ICrossReference> AllCrossReferences
        {
            get
            {
                if (this._allCrossReferences != null)
                {
                    return new List<ICrossReference>(this._allCrossReferences);
                }

                var reflectCrossReference = new ReflectUtil<ICrossReference>();
                IList<ICrossReference> returnList = reflectCrossReference.GetCompositeObjects(this, 
                    this.GetType().GetProperty("CrossReferences"),
                    this.GetType().GetProperty("AllCrossReferences")).ToList();
                returnList.Remove(null);
                if (this.StructureType.IsMaintainable)
                {
                    ISet<ISdmxObject> compositSet = this.Composites;

                    foreach (ISdmxObject currentComposite in compositSet)
                    {
                        foreach(var cross in currentComposite.CrossReferences)
                        {
                            if (!returnList.Contains(cross))
                            {
                                returnList.Add(cross);
                            }
                        }
                    }
                }

                this._allCrossReferences = returnList;
                return returnList;
            }
        }

        /// <summary>
        ///     Gets the parent.
        /// </summary>
        public virtual ISdmxObject Parent
        {
            get
            {
                return this._parent;
            }
        }

        /// <summary>
        ///     Gets or sets the structure type.
        /// </summary>
        public SdmxStructureType StructureType
        {
            get
            {
                return this._structureType;
            }

            set
            {
                this._structureType = value;
            }
        }

        /// <summary>
        /// Gets the bean key.
        /// </summary>
        public virtual string BeanKey => null;

        /// <summary>
        ///     The create tertiary.
        /// </summary>
        /// <param name="isSet">
        ///     The is set.
        /// </param>
        /// <param name="valueren">
        ///     The valueren.
        /// </param>
        /// <returns>
        ///     The <see cref="TertiaryBool" /> .
        /// </returns>
        public static TertiaryBool CreateTertiary(bool isSet, bool valueren)
        {
            return SdmxObjectUtil.CreateTertiary(isSet, valueren);
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        /// ///
        /// <exception cref="SdmxNotImplementedException">
        ///     Throws Unsupported Exception.
        /// </exception>
        public virtual bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (this == sdmxObject)
            {
                //Early Out
                return true;
            }
            return this.DeepEquals(sdmxObject, includeFinalProperties, null);
        }

        /// <summary>
        /// Deeps the equals.
        /// </summary>
        /// <param name="sdmxObject">The sdmx object.</param>
        /// <param name="includeFinalProperties">If true, include final properties.</param>
        /// <param name="diffReport">The diff report.</param>
        /// <returns>A bool.</returns>
        public virtual bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties, IDiffReport diffReport)
        {
            return false;
        }

        /// <summary>
        /// Equivalents the.
        /// </summary>
        /// <param name="list1">The list1.</param>
        /// <param name="list2">The list2.</param>
        /// <param name="includeFinalProperties">If true, include final properties.</param>
        /// <param name="property">The property.</param>
        /// <param name="diffReport">The diff report.</param>
        /// <returns>A bool.</returns>
        protected bool Equivalent<T>(ICollection<T> list1, ICollection<T> list2, bool includeFinalProperties, string property, IDiffReport diffReport)
            where T : ISdmxObject 
        {
            Dictionary<string, T> sourceByKey = new Dictionary<string, T>();
            List<T> keylessSource = new List<T>();

            foreach (T sourceBean in list1)
            {
                if (sourceBean.BeanKey == null)
                {
                    keylessSource.Add(sourceBean);
                }
                else
                {
                    sourceByKey.Add(sourceBean.BeanKey,sourceBean);
                }
            }

            Dictionary<string, T> targetByKey = new Dictionary<string, T>();
            List<T> keylessTarget = new List<T>();

            foreach (T targetBean in list2)
            {
                if (targetBean.BeanKey == null)
                {
                    keylessTarget.Add(targetBean);
                }
                else
                {
                    targetByKey.Add(targetBean.BeanKey,targetBean);
                }
            }

            bool returnVal = true;

            foreach (string key in sourceByKey.Keys)
            {
                T sourceBean = sourceByKey[key];
                T targetBean;

                if (targetByKey.TryGetValue(key, out targetBean))
                {
                    targetByKey.Remove(key);

                    if (!sourceBean.DeepEquals(targetBean, includeFinalProperties, diffReport))
                    {
                        if (diffReport == null)
                        {
                            return false;
                        }
                        returnVal = false;
                    }
                }
                else
                {
                    if (diffReport == null)
                    {
                        return false;
                    }
                    returnVal = false;
                    diffReport.AddDiff(sourceBean, property, sourceBean, null);
                }
            }

            // Loop left over targets, with no source
            foreach (string key in targetByKey.Keys)
            {
                T targetBean = targetByKey[key];

                if (diffReport == null)
                {
                    return false;
                }
                returnVal = false;
                diffReport.AddDiff(targetBean, property, null, targetBean);
            }

            for (int i = 0; i < keylessSource.Count; i++)
            {
                T sourceBean = keylessSource[i];
                T targetBean = i < keylessTarget.Count ? keylessTarget[i] : default(T);

                if (targetBean == null)
                {
                    if (diffReport == null)
                    {
                        return false;
                    }
                    returnVal = false;
                    diffReport.AddDiff(sourceBean, property, sourceBean, null);
                }
                else if (!sourceBean.DeepEquals(targetBean, includeFinalProperties))
                {
                    if (diffReport == null)
                    {
                        return false;
                    }
                    diffReport.AddDiff(sourceBean, property, sourceBean, null);
                    returnVal = false;
                }
            }

            for (int i = keylessSource.Count; i < keylessTarget.Count; i++)
            {
                T targetBean = keylessTarget[i];

                if (diffReport == null)
                {
                    return false;
                }
                returnVal = false;
                diffReport.AddDiff(targetBean, property, null, targetBean);
            }

            return returnVal;
        }

        protected bool Equivalent(ISdmxObject bean1, ISdmxObject bean2, bool includeFinalProperties, IDiffReport diffReport)
        {
            if (bean1 == null)
            {
                bool equivalent = bean2 == null;
                if (!equivalent && diffReport != null)
                {
                    diffReport.AddDiff(bean2, bean2.StructureType.ToString(), bean1, bean2);
                }
                return equivalent;
            }

            if (bean2 == null)
            {
                bool equivalent = bean1 == null;
                if (!equivalent && diffReport != null)
                {
                    diffReport.AddDiff(bean1, bean1.StructureType.ToString(), bean1, bean2);
                }
                return equivalent;
            }

            return bean1.DeepEquals(bean2, includeFinalProperties);
        }

        protected bool Equivalent(List<ICrossReference> sourceRef, List<ICrossReference> targetRef, IDiffReport diffReport)
        {
            Dictionary<string, ICrossReference> sourceUrnMap = ToMap(sourceRef);
            Dictionary<string, ICrossReference> targetUrnMap = ToMap(targetRef);
            HashSet<string> sourceKeys = new HashSet<string>(sourceUrnMap.Keys);

            var sourceExcept = sourceUrnMap.Keys.Except(targetUrnMap.Keys);
            var targetExcept = targetUrnMap.Keys.Except(sourceKeys);

            bool equivalent = sourceExcept.Count() == 0 && targetExcept.Count() == 0;

            if (diffReport != null)
            {
                foreach (string key in sourceExcept)
                {
                    ICrossReference refBean = sourceUrnMap[key];
                    diffReport.AddDiff(refBean.ReferencedFrom.GetParent<IIdentifiableObject>(true), refBean.TargetReference.GetType() + " Reference", FormatRef(refBean), null);
                }

                foreach (string key in targetExcept)
                {
                    ICrossReference refBean = targetUrnMap[key];
                    diffReport.AddDiff(refBean.ReferencedFrom.GetParent<IIdentifiableObject>(true), refBean.TargetReference.GetType() + " Reference", null, FormatRef(refBean));
                }
            }

            return equivalent;
        }

        /// <summary>
        /// Tos the map.
        /// </summary>
        /// <param name="refList">The ref list.</param>
        /// <returns>A Dictionary.</returns>
        private Dictionary<string, ICrossReference> ToMap(List<ICrossReference> refList)
        {
            return refList.ToDictionary(xsRef => xsRef.TargetUrn.ToString(), xsRef => xsRef);
        }

        /// <summary>
        /// Formats the ref.
        /// </summary>
        /// <param name="refBean">The ref bean.</param>
        /// <returns>A string.</returns>
        private string FormatRef(ICrossReference refBean)
        {
            if (refBean == null)
            {
                return null;
            }

            string[] parts = refBean.TargetUrn.ToString().Split('=');
            if (parts.Length > 1)
            {
                return parts[1];
            }
            else
            {
                return null; // Handle the case where split('=') does not produce at least two parts
            }
        }

        protected bool Equivalent(object o1, object o2, string property, IDiffReport diffReport)
        {
            if (!ObjectUtil.Equivalent(o1, o2))
            {
                if (diffReport != null)
                {
                    diffReport.AddDiff(this, property, o1, o2);
                }
                return false;
            }
            return true;
        }

        /// <summary>
        /// Equivalents the map.
        /// </summary>
        /// <param name="bean">The bean.</param>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        /// <param name="property">The property.</param>
        /// <param name="diffReport">The diff report.</param>
        /// <returns>A bool.</returns>
        protected bool EquivalentMap(ISdmxObject bean, IDictionary<object, object> source, IDictionary<object, object> target, string property, IDiffReport diffReport)
        {
            bool equivalentCollection = EquivalentCollection(bean, source.Keys, target.Keys, property, diffReport);
            if (!equivalentCollection && diffReport == null)
            {
                return false;
            }
            foreach (object sourceKey in source.Keys)
            {
                object targetVal;
                if (target.TryGetValue(sourceKey, out targetVal))
                {
                    object sourceVal = source[sourceKey];
                    if (!Equivalent(sourceVal, targetVal, property, diffReport))
                    {
                        equivalentCollection = false;
                    }
                }
            }
            return equivalentCollection;
        }

        protected bool Equivalent(ICrossReference sourceRef, ICrossReference targetRef, IDiffReport diffReport)
        {
            if (sourceRef == null && targetRef == null)
            {
                return true;
            }

            string structureRef = sourceRef != null ? sourceRef.TargetReference.ToString() : targetRef.TargetReference.ToString();
            structureRef += " Reference";

            if (sourceRef == null)
            {
                if (diffReport != null)
                {
                    diffReport.AddDiff(targetRef.ReferencedFrom.GetParent<IIdentifiableObject>(true), structureRef, sourceRef, FormatRef(targetRef));
                }
                return false;
            }

            if (targetRef == null)
            {
                if (diffReport != null)
                {
                    diffReport.AddDiff(sourceRef.ReferencedFrom.GetParent<IIdentifiableObject>(true), structureRef, FormatRef(sourceRef), targetRef);
                }
                return false;
            }

            if (!targetRef.TargetUrn.Equals(sourceRef.TargetUrn))
            {
                if (diffReport != null)
                {
                    diffReport.AddDiff(sourceRef.ReferencedFrom.GetParent<IIdentifiableObject>(true), structureRef, FormatRef(sourceRef), FormatRef(targetRef));
                }
                return false;
            }

            return true;
        }

        /// <summary>
        /// Sames the order.
        /// </summary>
        /// <param name="list1">The list1.</param>
        /// <param name="list2">The list2.</param>
        /// <param name="property">The property.</param>
        /// <param name="diffReport">The diff report.</param>
        /// <returns>A bool.</returns>
        protected bool SameOrder(IList<ISdmxObject> list1, IList<ISdmxObject> list2, string property, IDiffReport diffReport)
        {
            // Iterate through each element in the lists and check if they are equal
            for (int i = 0; i < list1.Count; i++)
            {
                if (!list1[i].Equals(list2[i]))
                {
                    if (diffReport == null)
                    {
                        return false;
                    }
                    else
                    {
                        // Only report a single element; otherwise, a long list may report an unmanageable number of differences
                        diffReport.AddDiff(this, property + " Order", list1[i].BeanKey, list2[i].BeanKey);
                        break;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Deeps the equals internal.
        /// </summary>
        /// <param name="bean">The bean.</param>
        /// <param name="includeFinalProperties">If true, include final properties.</param>
        /// <param name="diffReport">The diff report.</param>
        /// <returns>A bool.</returns>
        protected bool DeepEqualsInternal(ISdmxObject bean, bool includeFinalProperties, IDiffReport diffReport)
        {
            if (bean.StructureType == this.StructureType)
            {
                return true;
            }
            return false;
        }
        /// <summary>
        ///    Get the composites
        /// </summary>
        /// <typeparam name="T">
        ///     Generic type parameter.
        /// </typeparam>
        /// <returns>
        ///     The <see cref="ISet{T}" /> .
        /// </returns>
        public virtual ISet<T> GetComposites<T>()
        {
            var type = typeof(T);
            return GetComposites<T>(type);
        }

        /// <summary>
        ///     The get composites.
        /// </summary>
        /// <param name="type">
        ///     The type.
        /// </param>
        /// <typeparam name="T">
        ///     Generic type parameter.
        /// </typeparam>
        /// <returns>
        ///     The <see cref="ISet{T}" /> .
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="type"/> is <see langword="null" />.</exception>
        public virtual ISet<T> GetComposites<T>(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            ISet<T> returnSet = new HashSet<T>();
            foreach (ISdmxObject currentComposite in this.Composites)
            {
                if (type.IsInstanceOfType(currentComposite))
                {
                    returnSet.Add((T)currentComposite);
                }
            }

            return returnSet;
        }

        /// <summary>
        /// Visits all items up the parent hierarchy to return the first occurrence of parent of the given type that this
        /// SdmxObject belongs to
        /// <p />
        /// If a parent of the given type does not exist in the hierarchy, null will be returned
        /// </summary>
        /// <typeparam name="T">Generic type parameter.</typeparam>
        /// <param name="includeThisInSearch">if true then this type will be first checked to see if it is of the given type</param>
        /// <returns>The <typeparamref name="T"/>.</returns>
        public T GetParent<T>(bool includeThisInSearch) where T : class
        {
            Type type = typeof(T);
            if (this._parent != null)
            {
                if (type.IsInstanceOfType(this._parent))
                {
                    var returnObj = (T)this._parent;
                    return returnObj;
                }

                return this._parent.GetParent<T>(false);
            }

            if (type.IsInstanceOfType(this))
            {
                return this as T;
            }

            return null; // $$$default(T)/* was: null */;
        }

        /// <summary>
        ///     The create tertiary.
        /// </summary>
        /// <param name="valueren">
        ///     The valueren.
        /// </param>
        /// <returns>
        ///     The <see cref="TertiaryBool" /> .
        /// </returns>
        protected internal static TertiaryBool CreateTertiary(bool? valueren)
        {
            return SdmxObjectUtil.CreateTertiary(valueren);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATION                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Compare collections
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="bean">To be used when diff report</param>
        /// <param name="c1"></param>
        /// <param name="c2"></param>
        /// <param name="property">To be used when diff report</param>
        /// <param name="diffReport"></param>
        /// <returns></returns>
        // TODO does this need to be public ?
        public static bool EquivalentCollection<T>(ISdmxObject bean, ICollection<T> c1, ICollection<T> c2, string property, IDiffReport diffReport)
        {
            bool equivalent = true;
            if (ObjectUtil.ValidCollection(c1) && !ObjectUtil.ValidCollection(c2))
            {
                equivalent = false;
                if (diffReport != null)
                {
                    foreach (Object currentSource in c1)
                    {
                        diffReport.AddDiff(bean, property, currentSource, null);
                    }
                }
            }
            else if (!ObjectUtil.ValidCollection(c1) && ObjectUtil.ValidCollection(c2))
            {
                equivalent = false;
                if (diffReport != null)
                {
                    foreach (Object currentSource in c2)
                    {
                        diffReport.AddDiff(bean, property, currentSource, null);
                    }
                }
            }
            else
            {
                // this assumes that T impl implements IEquivelant
                var source = new HashSet<T>(c1);
                var target = new HashSet<T>(c2);
                source.ExceptWith(target);
                target.ExceptWith(c1);
                equivalent = source.Count == 0 && target.Count == 0;
                if (diffReport != null)
                {
                    foreach (Object currentSource in source)
                    {
                        diffReport.AddDiff(bean, property, currentSource, null);
                    }
                    foreach (Object currentTarget in target)
                    {
                        diffReport.AddDiff(bean, property, null, currentTarget);
                    }
                }
            }
            return equivalent;
        }
        /// <summary>
        ///     The equivalent.
        /// </summary>
        /// <typeparam name="T">Generic type param of type ISdmxObject</typeparam>
        /// <param name="list1">The list 1.</param>
        /// <param name="list2">The list 2.</param>
        /// <param name="includeFinalProperties">if set to <c>true</c> [include final properties].</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        protected internal bool Equivalent<T>(IList<T> list1, IList<T> list2, bool includeFinalProperties)
            where T : ISdmxObject
        {
            if (ReferenceEquals(list1, list2))
            {
                return true;
            }

            if (list1 == null || list2 == null)
            {
                return false;
            }

            if (list1.Count != list2.Count)
            {
                return false;
            }

            if (list1.Count == 0)
            {
                return true;
            }

            // ignore order
            var sortedList1 = list1.OrderBy(item => item.ToString()).ToList();
            var sortedList2 = list2.OrderBy(item => item.ToString()).ToList();
            for (int i = 0; i < sortedList2.Count; i++)
            {
                ISdmxObject thisCurrentObj = sortedList2[i];
                ISdmxObject thatCurrentObj = sortedList1[i];

                if (!thisCurrentObj.DeepEquals(thatCurrentObj, includeFinalProperties))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        ///     The equivalent.
        /// </summary>
        /// <param name="obj1">The obj 1.</param>
        /// <param name="obj2">The obj 2.</param>
        /// <param name="includeFinalProperties">if set to <c>true</c> [include final properties].</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        protected internal bool Equivalent(ISdmxObject obj1, ISdmxObject obj2, bool includeFinalProperties)
        {
            if (obj1 == null)
            {
                return obj2 == null;
            }

            if (obj2 == null)
            {
                return false;
            }

            return obj1.DeepEquals(obj2, includeFinalProperties);
        }

        /// <summary>
        ///     The equivalent.
        /// </summary>
        /// <param name="crossRef1">
        ///     The cross ref 1.
        /// </param>
        /// <param name="crossRef2">
        ///     The cross ref 2.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        protected internal bool Equivalent(ICrossReference crossRef1, ICrossReference crossRef2)
        {
            if (crossRef1 == null)
            {
                return crossRef2 == null;
            }

            if (crossRef2 == null)
            {
                return false;
            }

            return crossRef2.TargetUrn.Equals(crossRef1.TargetUrn);
        }

        /// <summary>
        ///     The add to composite set.
        /// </summary>
        /// <typeparam name="T">Generic type</typeparam>
        /// <param name="comp">The comp.</param>
        /// <param name="compositesSet">The composites set.</param>
        /// <exception cref="ArgumentNullException"><paramref name="comp"/> is <see langword="null" />.</exception>
        protected void AddToCompositeSet<T>(ICollection<T> comp, ISet<ISdmxObject> compositesSet) where T : ISdmxObject
        {
            if (comp == null)
            {
                throw new ArgumentNullException("comp");
            }

            foreach (T composite in comp)
            {
                this.AddToCompositeSet(composite, compositesSet);
            }
        }

        /// <summary>
        ///     The add to composite set.
        /// </summary>
        /// <param name="composite">The composite.</param>
        /// <param name="composites">The composites.</param>
        /// <exception cref="ArgumentNullException"><paramref name="composites"/> is <see langword="null" />.</exception>
        protected void AddToCompositeSet(ISdmxObject composite, ISet<ISdmxObject> composites)
        {
            if (composites == null)
            {
                throw new ArgumentNullException("composites");
            }

            if (composite != null)
            {
                composites.Add(composite);
                ISet<ISdmxObject> getComposites = composite.Composites;
                if (getComposites != null)
                {
                    composites.AddAll(getComposites);
                }
            }
        }

        /// <summary>
        ///     Deeps the equals internal.
        /// </summary>
        /// <param name="bean">The bean.</param>
        /// <returns>True if equals</returns>
        protected bool DeepEqualsInternal(ISdmxObject bean)
        {
            if (bean != null && bean.StructureType == this.StructureType)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        ///     The get composites internal.
        /// </summary>
        /// <returns>The composites</returns>
        protected abstract ISet<ISdmxObject> GetCompositesInternal();

        /// <inheritdoc/>
        public IReadOnlyList<string> GetSupportedLanguages()
        {
            if (this._supportedLanguages != null)
            {
                return _supportedLanguages;
            }
            lock(_synchronisationLock)
            {
                if (_supportedLanguages == null)
                {
                    SortedSet<string> supportedLanguages = new SortedSet<string>(StringComparer.Ordinal);
                    supportedLanguages.UnionWith(GetComposites<ITextTypeWrapper>().Select(t => t.Locale));
                    _supportedLanguages = supportedLanguages.ToArray();
                }
            }

            return _supportedLanguages;
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return this.BeanKey;
        }
    }
}