// -----------------------------------------------------------------------
// <copyright file="AnnotationUrlCore.cs" company="EUROSTAT">
//   Date Created : 2022-03-18
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// The core implementation of the <see cref="IAnnotationURL"/>.
    /// </summary>
    [Serializable]
    public class AnnotationUrlCore : SdmxObjectCore, IAnnotationURL
    {
        /// <summary>
        /// The default value for the locale
        /// </summary>
        public static string DefaultLocale = LocaleUtil.DefaultLocales.First().TwoLetterISOLanguageName;

        /// <summary>
        /// The locale.
        /// </summary>
        private string _locale;

        /// <summary>
        /// The uri.
        /// </summary>
        private Uri _uri;

        /// <summary>
        ///     Gets or sets the Uri
        /// </summary>
        public Uri Uri
        {
            get => this._uri;
            set => this._uri = value;
        }

        /// <summary>
        ///     Gets or sets the locale.
        /// </summary>
        public string Locale
        {
            get
            {
                return this._locale;
            }

            set
            {
                if (!ObjectUtil.ValidString(this._locale))
                {
                    this._locale = null;
                }
                else
                {
                    // Bug fix, in XML Locale contains a '-' to be valid, in Java '_' is used
                    this._locale = this._locale.Replace("_", "-");
                    this._locale = value;
                }
            }
        }

        /// <summary>
        ///     The get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            return new HashSet<ISdmxObject>();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        //////////// BUILD FROM VALUES   //////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>
        /// Initializes a new instance of the <see cref="AnnotationUrlCore"/> class.
        /// </summary>
        /// <param name="locale">The locale</param>
        /// <param name="uri">The uri</param>
        /// <param name="parent">The parent</param>
        public AnnotationUrlCore(string locale, Uri uri, ISdmxObject parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.AnnotationUrl), parent)
        {
            this._locale = locale;
            this._uri = uri;
            this.Validate();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AnnotationUrlCore"/> class
        /// with default locale.
        /// </summary>
        /// <param name="uri">The uri</param>
        /// <param name="parent">The parent</param>
        public AnnotationUrlCore(Uri uri, ISdmxObject parent)
            : this(null, uri, parent)
        {
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        //////////// BUILD FROM MUTABLE OBJECT   //////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Initializes a new instance of the <see cref="AnnotationUrlCore"/> class.
        /// </summary>
        /// <param name="mutable">The mutable object</param>
        /// <param name="parent">the parent</param>
        public AnnotationUrlCore(IAnnotationUrlMutableObject mutable, ISdmxObject parent)
            : base(mutable, parent)
        {
            this._locale = mutable.Locale;
            this._uri = new Uri(mutable.Uri);
            this.Validate();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        //////////// DEEP EQUALS   ////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IAnnotationURL)sdmxObject;
                if (!ObjectUtil.Equivalent(this._locale, that.Locale))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._uri, that.Uri))
                {
                    return false;
                }

                return true;
            }

            return false;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        //////////// VALIDATION   /////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        
        private void Validate()
        {
            if (!ObjectUtil.ValidString(this._locale))
            {
                // Default to no locale
                this._locale = null;
            }
            // if locale was provided, check if it is valid
            else if (!this.ParseLocale(this._locale))
            {
                throw new SdmxSemmanticException("Illegal Locale: " + this._locale);
            }

            if (this._uri == null)
            {
                throw new SdmxSemmanticException("Annotation URL can not have an empty uri value");
            }
        }

        /// <summary>
        ///     The parse locale.
        /// </summary>
        /// <param name="locale">The locale.</param>
        /// <returns>True if parse</returns>
        private bool ParseLocale(string locale)
        {
            return LocaleConstants.ValidLocaleSet.Contains(locale);
        }
    }
}
