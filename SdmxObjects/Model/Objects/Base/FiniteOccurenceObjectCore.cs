// -----------------------------------------------------------------------
// <copyright file="FiniteOccurenceObjectCore.cs" company="EUROSTAT">
//   Date Created : 2021-10-19
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base
{
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    /// An implementation for finite occurrences.
    /// For unbounded <see cref="UnboundedOccurenceObjectCore"/>.
    /// </summary>
    public class FiniteOccurenceObjectCore : IOccurenceObject
    {
        private readonly int _maxOccurs;

        private static readonly IOccurenceObject _defaultValue = new FiniteOccurenceObjectCore(1);

        /// <summary>
        /// Gets the default value; 1 occurence.
        /// </summary>
        public static IOccurenceObject DefaultValue => _defaultValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="FiniteOccurenceObjectCore"/>
        /// </summary>
        /// <param name="maxOccurs">a positive number larger than 1</param>
        /// <exception cref="SdmxSemmanticException">Occurrence must be positive number equal or larger than one.</exception>
        public FiniteOccurenceObjectCore(int maxOccurs)
        {
            if (maxOccurs <= 0)
            {
                throw new SdmxSemmanticException("Occurrence must be positive number equal or larger than zero");
            }
            this._maxOccurs = maxOccurs;
        }

        /// <summary>
        /// Gets the number that expresses the maximum occurrence of an object.
        /// </summary>
        public int Occurrences => this._maxOccurs;

        /// <summary>
        /// Gets a value indicating whether the maximum occurrence is unbounded
        /// Always false, since it is the bounded implementation.
        /// </summary>
        public bool IsUnbounded => false;

        /// <summary>
        /// The string representation of the occurence
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0:d}", this._maxOccurs);
        }

        /// <summary>
        /// Compares two occurences for equality
        /// </summary>
        /// <param name="o">The occurence object to compare to.</param>
        /// <returns><c>true</c> if found equal, <c>false</c> otherwise.</returns>
        public override bool Equals(object o)
        {
            if (this == o)
            {
                return true;
            }

            if (o == null || this.GetType() != o.GetType())
            {
                return false;
            }

            FiniteOccurenceObjectCore that = (FiniteOccurenceObjectCore)o;
            return this._maxOccurs == that._maxOccurs;
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return this._maxOccurs.GetHashCode();
        }
    }
}
