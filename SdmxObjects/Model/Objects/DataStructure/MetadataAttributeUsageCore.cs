// -----------------------------------------------------------------------
// <copyright file="MetadataAttributeUsageCore.cs" company="EUROSTAT">
//   Date Created : 2021-10-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.DataStructure
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// The base implementation of the <see cref="IMetadataAttributeUsage"/> interface.
    /// </summary>
    public class MetadataAttributeUsageCore : AnnotableCore, IMetadataAttributeUsage
    {
        private readonly string _metadataAttributeId;
	    private readonly AttributeAttachmentLevel _attachmentLevel;
        private readonly string _attachmentGroup;
        private readonly IList<string> _dimensionReference;

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE BEANS 			 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Initializes a new instance of the <see cref="MetadataAttributeUsageCore"/> class
        /// from a mutable object.
        /// </summary>
        /// <param name="bean">The mutable object to initalize the instance from.</param>
        /// <param name="parent">The attribute list it belongs into.</param>
        public MetadataAttributeUsageCore(IMetadataAttributeUsageMutableObject bean, IAttributeList parent)
            : base(bean, parent)
        {
            if (bean.MetadataAttributeReference == null)
            {
                throw new NullReferenceException(nameof(bean.MetadataAttributeReference));
            }
            this._metadataAttributeId = bean.MetadataAttributeReference;
            this._attachmentLevel = bean.AttachmentLevel;
            this._attachmentGroup = bean.AttachmentGroup;
            if (bean.DimensionReferences != null)
            {
                this._dimensionReference = new ReadOnlyCollection<string>(bean.DimensionReferences);
            }
            else
            {
                this._dimensionReference = new List<string>();
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP VALIDATION						 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Compares this to another SDMX object for equality
        /// </summary>
        /// <param name="bean">The SDMX object to compare with.</param>
        /// <param name="includeFinalProperties">Indicator to include final properties.</param>
        /// <returns><c>true</c> if found equal, <c>false</c> otherwise.</returns>
        public override bool DeepEquals(ISdmxObject bean, bool includeFinalProperties)
        {
            if (bean == null)
            {
                return false;
            }
            if (bean.StructureType != this.StructureType)
            {
                return false;
            }

            IMetadataAttributeUsage that = (IMetadataAttributeUsage)bean;

            if (!this._metadataAttributeId.Equals(that.MetadataAttributeReference))
            {
                return false;
            }

            if (!this._attachmentLevel.Equals(that.AttachmentLevel))
            {
                return false;
            }

            if (!this._attachmentGroup.Equals(that.AttachmentGroup))
            {
                return false;
            }

            if (!ObjectUtil.EquivalentCollection(this._dimensionReference, that.DimensionReferences))
            {
                return false;
            }

            return base.DeepEqualsInternal(that);
        }

        /// <inheritdoc/>
        public string MetadataAttributeReference => this._metadataAttributeId;

        /// <inheritdoc/>
        public IList<string> DimensionReferences => this._dimensionReference;

        /// <inheritdoc/>
        public string AttachmentGroup => this._attachmentGroup;

        /// <inheritdoc/>
        public string PrimaryMeasureReference => null;

        /// <inheritdoc/>
        public AttributeAttachmentLevel AttachmentLevel => this._attachmentLevel;
    }
}
