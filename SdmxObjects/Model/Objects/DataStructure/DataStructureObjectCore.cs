// -----------------------------------------------------------------------
// <copyright file="DataStructureObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.DataStructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using log4net;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Diff;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    using GroupType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.GroupType;

    // using System.ComponentModel;

    /// <summary>
    ///     The data structure object core.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "It is OK. This is a complex object and we need to be close to Java version")]
    [Serializable]
    public class DataStructureObjectCore : MaintainableObjectCore<IDataStructureObject, IDataStructureMutableObject>,
                                           IDataStructureObject
    {
        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(DataStructureObjectCore));

        /// <summary>
        ///     The _component id to component.
        /// </summary>
        private readonly Dictionary<string, IComponent> _componentIdToComponent =
            new Dictionary<string, IComponent>(StringComparer.Ordinal);

        /// <summary>
        ///     The _dimension list.
        /// </summary>
        private readonly IDimensionList _dimensionList;

        /// <summary>
        ///     Value storage for <see cref="FrequencyDimension"/>
        /// </summary>
        private IDimension _frequencyDimension;

        /// <summary>
        ///     Check flag for the <see cref="FrequencyDimension"/>, 
        ///     so that the check is not executed more than once even when there is no <see cref="_frequencyDimension"/>.
        /// </summary>
        private bool _frequencyDimensionChecked = false;

        /// <summary>
        ///     Value storage for <see cref="TimeDimension"/>
        /// </summary>
        private IDimension _timeDimension;

        /// <summary>
        ///     Check flag for the <see cref="TimeDimension"/>, 
        ///     so that the check is not executed more than once even when there is no <see cref="_timeDimension"/>.
        /// </summary>
        private bool _timeDimensionChecked = false;

        /// <summary>
        ///     The _groups.
        /// </summary>
        private readonly IList<IGroup> _groups;
        private bool _usePrimaryMeasure;

        /// <summary>
        ///     The _measure list.
        /// </summary>
        private readonly IMeasureList _measureList;

        /// <summary>
        ///     The _attribtue list.
        /// </summary>
        private readonly IAttributeList _attributeList;

        /// <summary>
        ///     The metadata structure reference
        /// </summary>
        private readonly ICrossReference _metadataStructure;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataStructureObjectCore" /> class.
        /// </summary>
        /// <param name="itemMutableObject">
        ///     The agencyScheme.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws SdmxSemmanticException.
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public DataStructureObjectCore(IDataStructureMutableObject itemMutableObject)
            : base(itemMutableObject)
        {
            this._groups = new List<IGroup>();
            _log.Debug("Building DataStructureObject from Mutable Object");
            try
            {
                if (itemMutableObject.Groups != null)
                {
                    foreach (IGroupMutableObject mutable in itemMutableObject.Groups)
                    {
                        this._groups.Add(new GroupCore(mutable, this));
                    }
                }

                if (itemMutableObject.MeasureList != null)
                {
                    this._measureList = new MeasureListCore(itemMutableObject.MeasureList, this);
                }

                if (itemMutableObject.DimensionList != null)
                {
                    this._dimensionList = new DimensionListCore(itemMutableObject.DimensionList, this);
                }

                if (itemMutableObject.AttributeList != null)
                {
                    this._attributeList = new AttributeListCore(itemMutableObject.AttributeList, this);
                }

                if (itemMutableObject.MetadataStructure != null)
                {
                    this._metadataStructure = new CrossReferenceImpl(this, itemMutableObject.MetadataStructure);
                }

                this.PopulateComponentDic();
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }

            if (_log.IsDebugEnabled)
            {
                _log.Debug("DataStructureObject Built " + this.Urn);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataStructureObjectCore" /> class.
        /// </summary>
        /// <param name="dataStructure">
        ///     The agencyScheme.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws SdmxSemmanticException.
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "It is OK. Data Structure is a complex artefact.")]
        public DataStructureObjectCore(DataStructureType dataStructure)
            : base(dataStructure, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd))
        {
            this._groups = new List<IGroup>();
            _log.Debug("Building DataStructureObject from 2.1 SDMX");
            DataStructureComponentsType components = null;

            if (dataStructure.DataStructureComponents != null)
            {
                components = dataStructure.DataStructureComponents.Content;
            }

            try
            {
                if (components != null)
                {
                    foreach (Group currentGroup in components.Group)
                    {
                        this._groups.Add(new GroupCore(currentGroup.Content, this));
                    }

                    if (components.DimensionList != null)
                    {
                        this._dimensionList = new DimensionListCore(components.DimensionList.Content, this);
                    }

                    if (components.AttributeList != null)
                    {
                        this._attributeList = new AttributeListCore(components.AttributeList.Content, this);
                    }

                    if (components.MeasureList != null)
                    {
                        this._measureList = new MeasureListCore(components.MeasureList.Content, this);
                    }

                    this.PopulateComponentDic();
                }
            }
            catch (SdmxException)
            {
                throw;
            }
            catch (Exception th)
            {
                throw new SdmxSemmanticException(th, ExceptionCode.ObjectStructureConstructionError, this);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }

            if (_log.IsDebugEnabled)
            {
                _log.Debug("DataStructureObject Built " + this.Urn);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataStructureObjectCore" /> class.
        /// </summary>
        /// <param name="keyFamilyType">
        ///     The agencyScheme.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws SdmxSemmanticException.
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="keyFamilyType"/> is <see langword="null" />.</exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "It is OK. Data Structure is a complex artefact.")]
        public DataStructureObjectCore(KeyFamilyType keyFamilyType)
            : this(keyFamilyType, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataStructureObjectCore"/> class.
        /// </summary>
        /// <param name="keyFamilyType">
        /// The agencyScheme.
        /// </param>
        /// <param name="conceptRoleBuilder">
        /// The concept Role Builder.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        /// Throws SdmxSemmanticException.
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        /// Throws Validate exception.
        /// </exception>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="keyFamilyType"/> is <see langword="null"/>.
        /// </exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "It is OK. Data Structure is a complex artefact.")]
        public DataStructureObjectCore(KeyFamilyType keyFamilyType, IBuilder<IStructureReference, ComponentRole> conceptRoleBuilder)
            : base(
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd), 
                keyFamilyType.PassNoNull("keyFamilyType").validTo, 
                keyFamilyType.PassNoNull("keyFamilyType").validFrom, 
                keyFamilyType.PassNoNull("keyFamilyType").version, 
                CreateTertiary(keyFamilyType.PassNoNull("keyFamilyType").isFinal), 
                keyFamilyType.PassNoNull("keyFamilyType").agencyID, 
                keyFamilyType.PassNoNull("keyFamilyType").id, 
                keyFamilyType.PassNoNull("keyFamilyType").uri, 
                keyFamilyType.PassNoNull("keyFamilyType").Name, 
                keyFamilyType.PassNoNull("keyFamilyType").Description, 
                CreateTertiary(keyFamilyType.PassNoNull("keyFamilyType").isExternalReference), 
                keyFamilyType.PassNoNull("keyFamilyType").Annotations)
        {
            if (keyFamilyType == null)
            {
                throw new ArgumentNullException("keyFamilyType");
            }

            this._groups = new List<IGroup>();
            _log.Debug("Building DataStructureObject from 2.0 SDMX");
            ComponentsType components = keyFamilyType.Components;
            try
            {
                if (components != null)
                {
                    foreach (GroupType currentGroup in components.Group)
                    {
                        this._groups.Add(new GroupCore(currentGroup, this));
                    }

                    if (components.Dimension != null)
                    {
                        this._dimensionList = new DimensionListCore(keyFamilyType, this, conceptRoleBuilder);
                    }

                    if (components.PrimaryMeasure != null)
                    {
                        this._measureList = new MeasureListCore(components.PrimaryMeasure, this);
                    }

                    if (ObjectUtil.ValidCollection(components.Attribute))
                    {
                        this._attributeList = new AttributeListCore(keyFamilyType, this, conceptRoleBuilder);
                    }

                    this.PopulateComponentDic();
                }
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }

            if (_log.IsDebugEnabled)
            {
                _log.Debug("DataStructureObject Built " + this.Urn);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataStructureObjectCore" /> class.
        /// </summary>
        /// <param name="keyFamilyType">
        ///     The agencyScheme.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="keyFamilyType"/> is <see langword="null" />.</exception>
        public DataStructureObjectCore(Org.Sdmx.Resources.SdmxMl.Schemas.V10.Structure.KeyFamilyType keyFamilyType)
            : base(
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd), 
                keyFamilyType.PassNoNull("keyFamilyType").version, 
                keyFamilyType.PassNoNull("keyFamilyType").agency, 
                keyFamilyType.PassNoNull("keyFamilyType").id, 
                keyFamilyType.PassNoNull("keyFamilyType").uri, 
                keyFamilyType.PassNoNull("keyFamilyType").Name, 
                null, 
                keyFamilyType.PassNoNull("keyFamilyType").Annotations)
        {
            if (keyFamilyType == null)
            {
                throw new ArgumentNullException("keyFamilyType");
            }

            this._groups = new List<IGroup>();
            _log.Debug("Building DataStructureObject from 1.0 SDMX");
            Org.Sdmx.Resources.SdmxMl.Schemas.V10.Structure.ComponentsType components = keyFamilyType.Components;
            if (components != null)
            {
                foreach (Org.Sdmx.Resources.SdmxMl.Schemas.V10.Structure.GroupType currentGroup in components.Group)
                {
                    this._groups.Add(new GroupCore(currentGroup, this));
                }

                if (components.Dimension != null)
                {
                    this._dimensionList = new DimensionListCore(keyFamilyType, this);
                }

                if (components.PrimaryMeasure != null)
                {
                    this._measureList = new MeasureListCore(components.PrimaryMeasure, this);
                }

                if (components.Attribute != null)
                {
                    this._attributeList = new AttributeListCore(keyFamilyType, this);
                }

                this.PopulateComponentDic();
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }

            if (_log.IsDebugEnabled)
            {
                _log.Debug("DataStructureObject Built " + this);
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataStructureObjectCore" /> class.
        /// </summary>
        /// <param name="agencyScheme">
        ///     The agencyScheme.
        /// </param>
        /// <param name="actualLocation">
        ///     The actual location.
        /// </param>
        /// <param name="isServiceUrl">
        ///     The is service url.
        /// </param>
        private DataStructureObjectCore(IDataStructureObject agencyScheme, Uri actualLocation, bool isServiceUrl)
            : base(agencyScheme, actualLocation, isServiceUrl)
        {
            this._groups = new List<IGroup>();
            _log.Debug("Stub DataStructureObject Built");
        }

        /// <summary>
        ///     Gets the attribtue list.
        /// </summary>
        public IAttributeList AttributeList
        {
            get
            {
                return this._attributeList;
            }
        }

        /// <summary>
        ///     Gets the attributes.
        /// </summary>
        public IList<IAttributeObject> Attributes
        {
            get
            {
                if (this._attributeList != null)
                {
                    return this._attributeList.Attributes;
                }

                return new List<IAttributeObject>();
            }
        }

        /// <summary>
        ///     Gets the components
        /// </summary>
        public virtual IList<IComponent> Components
        {
            get
            {
                IList<IComponent> returnList = new List<IComponent>();
                returnList.AddAll(this._dimensionList.Dimensions);
                returnList.AddAll(this.Attributes);
                returnList.AddAll(this.Measures); 
                return returnList;
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Gets the cross referenced constrainables.
        /// </summary>
        public IList<ICrossReference> CrossReferencedConstrainables
        {
            get
            {
                return new List<ICrossReference>();
            }
        }

        /// <summary>
        ///     Gets the dataset attributes.
        /// </summary>
        public IList<IAttributeObject> DatasetAttributes
        {
            get
            {
                return this.GetAttribute(AttributeAttachmentLevel.DataSet);
            }
        }

        /// <summary>
        ///     Gets the dimension group attributes.
        /// </summary>
        public IList<IAttributeObject> DimensionGroupAttributes
        {
            get
            {
                return this.GetAttribute(AttributeAttachmentLevel.DimensionGroup);
            }
        }

        /// <summary>
        ///     Gets the dimension list.
        /// </summary>
        public IDimensionList DimensionList
        {
            get
            {
                return this._dimensionList;
            }
        }

        /// <summary>
        ///     Gets the frequency dimension.
        /// </summary>
        public IDimension FrequencyDimension
        {
            get
            {
                if (_frequencyDimension == null && !_frequencyDimensionChecked)
                {
                    foreach (IDimension currentDimension in this.GetDimensions())
                    {
                        if (currentDimension.FrequencyDimension)
                        {
                            _frequencyDimension = currentDimension;
                            break;
                        }
                    }
                    _frequencyDimensionChecked = true;
                }
                return _frequencyDimension;
            }
        }

        /// <summary>
        ///     Gets the group attributes.
        /// </summary>
        public IList<IAttributeObject> GroupAttributes
        {
            get
            {
                return this.GetAttribute(AttributeAttachmentLevel.Group);
            }
        }

        /// <summary>
        ///     Gets the groups.
        /// </summary>
        public IList<IGroup> Groups
        {
            get
            {
                return new List<IGroup>(this._groups);
            }
        }

        /// <summary>
        ///     Gets the measure list.
        /// </summary>
        public IMeasureList MeasureList
        {
            get
            {
                return this._measureList;
            }
        }

        /// <summary>
        ///     Gets the mutable instance.
        /// </summary>
        public override IDataStructureMutableObject MutableInstance
        {
            get
            {
                return new DataStructureMutableCore(this);
            }
        }

        /// <summary>
        ///     Gets the observation attributes.
        /// </summary>
        public IList<IAttributeObject> ObservationAttributes
        {
            get
            {
                return this.GetAttribute(AttributeAttachmentLevel.Observation);
            }
        }

        /// <summary>
        ///     Gets the primary measure.
        ///     Obsolete in SDMX v3.0.0 schema
        /// </summary>
        public IMeasure PrimaryMeasure
        {
            get
            {
                if (this._measureList != null)
                {
                    return this._measureList.PrimaryMeasure;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the SDMX measures
        /// Introduced in SDMX v3.0.0 schema
        /// </summary>
        public IList<IMeasure> Measures
        {
            get
            {
                if (this._measureList == null)
                {
                    return new List<IMeasure>();
                }

                return this._measureList.Measures;
            }
        }

        /// <summary>
        ///     Gets the time dimension.
        /// </summary>
        public IDimension TimeDimension
        {
            get
            {
                if (_timeDimension == null && !_timeDimensionChecked)
                {
                    foreach (IDimension currentDimension in this.GetDimensions())
                    {
                        if (currentDimension.TimeDimension)
                        {
                            _timeDimension = currentDimension;
                            break;
                        }
                    }
                    _timeDimensionChecked = true;
                }
                return _timeDimension;
            }
        }

        /// <summary>
        ///     Gets the Urn
        /// </summary>
        public override sealed Uri Urn
        {
            get
            {
                return base.Urn;
            }
        }

        /// <summary>
        ///     Gets the mutable instance.
        /// </summary>
        IMaintainableMutableObject IMaintainableObject.MutableInstance
        {
            get
            {
                return this.MutableInstance;
            }
        }

        /// <inheritdoc/>
        public ICrossReference MetadataStructure => this._metadataStructure;

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The agencyScheme.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <param name="diffReport"></param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties,IDiffReport diffReport)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                bool returnVal = true;
                var that = (IDataStructureObject)sdmxObject;
                if (!this.Equivalent(this._groups, that.Groups, includeFinalProperties,"Group",diffReport))
                {
                    returnVal = false;
                }

                if (!this.Equivalent(this._dimensionList, that.DimensionList, includeFinalProperties, diffReport))
                {
                    returnVal = false;
                }

                if (!this.Equivalent(this._attributeList, that.AttributeList, includeFinalProperties, diffReport))
                {
                    returnVal = false;
                }

                if (!this.Equivalent(this._measureList, that.MeasureList, includeFinalProperties, diffReport))
                {
                    returnVal = false;
                }

                if (!this.Equivalent(this._metadataStructure, that.MetadataStructure, diffReport))
                {
                    returnVal = false;
                }

                return this.DeepEqualsMaintainable(that, includeFinalProperties, diffReport) && returnVal;
            }

            return false;
        }

        /// <summary>
        ///     The get attribute.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="IAttributeObject" /> .
        /// </returns>
        public IAttributeObject GetAttribute(string id)
        {
            if (!string.IsNullOrWhiteSpace(id))
            {
                foreach (IAttributeObject currentAttribtue in this.Attributes)
                {
                    if (currentAttribtue.Id.Equals(id))
                    {
                        return currentAttribtue;
                    }
                }
            }

            return null;
        }

        /// <summary>
        ///     The get component.
        /// </summary>
        /// <param name="id">
        ///     The concept id.
        /// </param>
        /// <returns>
        ///     The <see cref="IComponent" /> .
        /// </returns>
        public IComponent GetComponent(string id)
        {
            IComponent component = null;
            if (this._componentIdToComponent.TryGetValue(id, out component))
            {
                return component;
            }

            return null;
        }

        /// <summary>
        ///     The get dimension.
        /// </summary>
        /// <param name="id">
        ///     The concept id.
        /// </param>
        /// <returns>
        ///     The <see cref="IDimension" /> .
        /// </returns>
        public IDimension GetDimension(string id)
        {
            IComponent component;
            if (this._componentIdToComponent.TryGetValue(id, out component))
            {
                var dimension = component as IDimension;
                return dimension;
            }

            return null;
        }

        /// <summary>
        ///     The get dimension group attribute.
        /// </summary>
        /// <param name="id">
        ///     The concept id.
        /// </param>
        /// <returns>
        ///     The <see cref="IAttributeObject" /> .
        /// </returns>
        public IAttributeObject GetDimensionGroupAttribute(string id)
        {
            return this.GetAttribute(AttributeAttachmentLevel.DimensionGroup, id);
        }

        /// <summary>
        ///     The get dimensions.
        /// </summary>
        /// <param name="include">The include.</param>
        /// <returns>
        ///     The <see cref="IList{T}" /> .
        /// </returns>
        public IList<IDimension> GetDimensions(params SdmxStructureEnumType[] include)
        {
            var returnList = new List<IDimension>();
            if (this._dimensionList != null)
            {
                if (include == null || include.Length == 0)
                {
                    return this._dimensionList.Dimensions;
                }

                foreach (IDimension dimensionCore in this._dimensionList.Dimensions)
                {
                    foreach (SdmxStructureEnumType currentType in include)
                    {
                        if (currentType == dimensionCore.StructureType.EnumType)
                        {
                            returnList.Add(dimensionCore);
                        }
                    }
                }
            }

            return returnList;
        }

        /// <summary>
        ///     The get group.
        /// </summary>
        /// <param name="groupId">
        ///     The group id.
        /// </param>
        /// <returns>
        ///     The <see cref="IGroup" /> .
        /// </returns>
        public IGroup GetGroup(string groupId)
        {
            foreach (IGroup currentGroup in this._groups)
            {
                if (currentGroup.Id.Equals(groupId))
                {
                    return currentGroup;
                }
            }

            return null;
        }

        /// <summary>
        ///     The get group attribute.
        /// </summary>
        /// <param name="id">
        ///     The concept id.
        /// </param>
        /// <returns>
        ///     The <see cref="IAttributeObject" /> .
        /// </returns>
        public IAttributeObject GetGroupAttribute(string id)
        {
            return this.GetAttribute(AttributeAttachmentLevel.Group, id);
        }

        /// <summary>
        ///     The get group attributes.
        /// </summary>
        /// <param name="groupId">
        ///     The group id.
        /// </param>
        /// <param name="includeDimensionGroups">
        ///     The include dimension groups.
        /// </param>
        /// <returns>
        ///     The <see cref="IList{T}" /> .
        /// </returns>
        public IList<IAttributeObject> GetGroupAttributes(string groupId, bool includeDimensionGroups)
        {
            IGroup group = this.GetGroup(groupId);
            if (group == null)
            {
                throw new ArgumentNullException(
                    "Group not found on Data Structure '" + this.Urn + "' with id: " + groupId);
            }

            IList<IAttributeObject> allGroupAttributes = this.GroupAttributes;
            IList<IAttributeObject> returnList = new List<IAttributeObject>();

            foreach (IAttributeObject currentAttribtue in allGroupAttributes)
            {
                if (currentAttribtue.AttachmentGroup != null)
                {
                    if (currentAttribtue.AttachmentGroup.Equals(groupId))
                    {
                        returnList.Add(currentAttribtue);
                    }
                }
            }

            foreach (IAttributeObject attributeBean in this.DimensionGroupAttributes)
            {
                IList<string> attrDimRefs = attributeBean.DimensionReferences;
                IList<string> grpDimRefs = group.DimensionRefs;

                if (attrDimRefs.ContainsAll(grpDimRefs) && grpDimRefs.ContainsAll(attrDimRefs))
                {
                    returnList.Add(attributeBean);
                }
            }

            return returnList;
        }

        /// <summary>
        ///     The get observation attribute.
        /// </summary>
        /// <param name="id">
        ///     The concept id.
        /// </param>
        /// <returns>
        ///     The <see cref="IAttributeObject" /> .
        /// </returns>
        public IAttributeObject GetObservationAttribute(string id)
        {
            return this.GetAttribute(AttributeAttachmentLevel.Observation, id);
        }

        /// <summary>
        ///     The get observation attributes.
        /// </summary>
        /// <param name="crossSectionalConcept">
        ///     The cross sectional concept.
        /// </param>
        /// <returns>
        ///     The <see cref="IList{T}" /> .
        /// </returns>
        public IList<IAttributeObject> GetObservationAttributes(string crossSectionalConcept)
        {
            if (crossSectionalConcept == null)
            {
                return this.ObservationAttributes;
            }

            IList<IAttributeObject> returnList = new List<IAttributeObject>();

            foreach (IAttributeObject att in this.DimensionGroupAttributes)
            {
                if (att.DimensionReferences.Contains(crossSectionalConcept))
                {
                    returnList.Add(att);
                }
            }

            foreach (IAttributeObject att in this.GetAttribute(AttributeAttachmentLevel.Group))
            {
                var group = this.Groups.First(x => x.Id.Equals(att.AttachmentGroup));

                if (group.DimensionRefs.Contains(crossSectionalConcept))
                {
                    returnList.Add(att);
                }
            }

            returnList.AddAll(this.ObservationAttributes);

            return returnList;
        }

        /// <summary>
        ///     The get series attributes.
        /// </summary>
        /// <param name="crossSectionalConcept">
        ///     The cross sectional concept.
        /// </param>
        /// <returns>
        ///     The <see cref="IList{T}" /> .
        /// </returns>
        public IList<IAttributeObject> GetSeriesAttributes(string crossSectionalConcept)
        {
            if (crossSectionalConcept == null)
            {
                return this.DimensionGroupAttributes;
            }

            IList<IAttributeObject> returnList = new List<IAttributeObject>();

            foreach (IAttributeObject att in this.DimensionGroupAttributes)
            {
                if (!att.DimensionReferences.Contains(crossSectionalConcept))
                {
                    returnList.Add(att);
                }
            }

            return returnList;
        }

        /// <summary>
        ///     The get stub.
        /// </summary>
        /// <param name="actualLocation">
        ///     The actual location.
        /// </param>
        /// <param name="isServiceUrl">
        ///     The is service url.
        /// </param>
        /// <returns>
        ///     The <see cref="IDataStructureObject" /> .
        /// </returns>
        public override IDataStructureObject GetStub(Uri actualLocation, bool isServiceUrl)
        {
            return new DataStructureObjectCore(this, actualLocation, isServiceUrl);
        }

        /// <summary>
        ///     The has frequency dimension.
        /// </summary>
        /// <returns> The <see cref="bool" /> . </returns>
        public bool HasFrequencyDimension()
        {
            return FrequencyDimension != null;
        }

        /// <summary>
        ///     The is compatible.
        /// </summary>
        /// <param name="schemaVersion">The schema version</param>
        /// <returns>
        ///     The boolean
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="schemaVersion"/> is <see langword="null" />.</exception>
        public bool IsCompatible(SdmxSchema schemaVersion)
        {
            if (schemaVersion == null)
            {
                throw new ArgumentNullException("schemaVersion");
            }

            // check for 3.0.0 only features
            switch (schemaVersion.EnumType)
            {
                case SdmxSchemaEnumType.VersionTwoPointOne:
                case SdmxSchemaEnumType.VersionTwo:
                case SdmxSchemaEnumType.VersionOne:
                case SdmxSchemaEnumType.Edi:
                case SdmxSchemaEnumType.Json:
                case SdmxSchemaEnumType.Csv:
                    // NOTE there could be workarounds/conventions for all the following
                    // multiple measures
                    if (this.Measures.Count > 1)
                    {
                        return false;
                    }
                    // no measures
                    if (!this.Measures.Any() && this.PrimaryMeasure == null)
                    {
                        return false;
                    }
                    if (this.Attributes != null)
                    {
                        // array values
                        if (this.Attributes.Any(a => !a.RepresentationMaxOccurs.Equals(FiniteOccurenceObjectCore.DefaultValue)))
                        {
                            return false;
                        }
                        // multi lingual
                        if (this.Attributes
                                .Where(a => a.Representation != null && a.Representation.TextFormat != null)
                                .Any(a =>a.Representation.TextFormat.Multilingual.IsTrue))
                        {
                            return false;
                        }
                    }
                    break;
                case SdmxSchemaEnumType.CsvV20:
                case SdmxSchemaEnumType.VersionThree:
                case SdmxSchemaEnumType.JsonV20:
                    // 3.0.0 compatible formats
                    break;
            }

            switch (schemaVersion.EnumType)
            {
                case SdmxSchemaEnumType.VersionOne:
                case SdmxSchemaEnumType.VersionTwo:
                    if (schemaVersion.EnumType == SdmxSchemaEnumType.VersionOne)
                    {
                        foreach (IComponent currentComponent in
                            this.GetDimensions(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dimension).EnumType))
                        {
                            if (currentComponent.Representation == null
                                || currentComponent.Representation.Representation == null)
                            {
                                return false;
                            }
                        }
                    }

                    // Intensionally no break, as version 2.0 compatability checks also apply to version 1.0
                    ISet<string> componentIds = new HashSet<string>();
                    foreach (IComponent currentComponent in this.Components)
                    {
                        if (currentComponent.ConceptRef.TargetReference
                            == SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Concept))
                        {
                            string conceptId = currentComponent.ConceptRef.FullId;
                            if (componentIds.Contains(conceptId))
                            {
                                return false;
                            }

                            componentIds.Add(conceptId);
                        }
                    }

                    break;
                case SdmxSchemaEnumType.Edi:
                    break;
            }

            return true;
        }

        /// <summary>
        ///     Gets composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._groups, composites);
            this.AddToCompositeSet(this._dimensionList, composites);
            this.AddToCompositeSet(this._attributeList, composites);
            this.AddToCompositeSet(this._measureList, composites);
            return composites;
        }

        /// <summary>
        ///     The get attribute.
        /// </summary>
        /// <param name="type">
        ///     The type.
        /// </param>
        /// <returns>
        ///     The <see cref="IList{T}" /> .
        /// </returns>
        private IList<IAttributeObject> GetAttribute(AttributeAttachmentLevel type)
        {
            IList<IAttributeObject> returnList = new List<IAttributeObject>();

            foreach (IAttributeObject currentAttribtue in this.Attributes)
            {
                if (currentAttribtue.AttachmentLevel == type)
                {
                    returnList.Add(currentAttribtue);
                }
            }

            return returnList;
        }

        /// <summary>
        /// The get metadata attribute
        /// </summary>
        /// <param name="type">the type</param>
        /// <returns>The <see cref="IList{T}" /> .</returns>
        private List<IMetadataAttributeUsage> GetMetadataAttribute(AttributeAttachmentLevel type)
        {
            List<IMetadataAttributeUsage> returnList = new List<IMetadataAttributeUsage>();
            foreach (IMetadataAttributeUsage currentAttribute in GetMetadataAttributes())
            {
                if (currentAttribute.AttachmentLevel == type)
                {
                    returnList.Add(currentAttribute);
                }
            }
            return returnList;
        }

        /// <summary>
        ///     The get attribute.
        /// </summary>
        /// <param name="type">
        ///     The type.
        /// </param>
        /// <param name="conceptId">
        ///     The concept id.
        /// </param>
        /// <returns>
        ///     The <see cref="IAttributeObject" /> .
        /// </returns>
        private IAttributeObject GetAttribute(AttributeAttachmentLevel type, string conceptId)
        {
            foreach (IAttributeObject currentAttribtue in this.GetAttribute(type))
            {
                if (currentAttribtue.Id.Equals(conceptId))
                {
                    return currentAttribtue;
                }
            }

            return null;
        }

        /// <summary>
        ///     The i validate unique component.
        /// </summary>
        /// <param name="conceptIds">
        ///     The concept ids.
        /// </param>
        /// <param name="component">
        ///     The component.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        private void ValidateUniqueComponent(ISet<string> conceptIds, IComponent component)
        {
            string conceptId = component.Id;
            if (conceptIds.Contains(conceptId))
            {
                throw new SdmxSemmanticException("Duplicate Data CategorisationStructure Component Id : " + conceptId);
            }

            conceptIds.Add(conceptId);
        }

        /// <summary>
        ///     Populate the <see cref="_componentIdToComponent" /> from <see cref="_dimensionList" />,
        ///     <see cref="_attributeList" /> and
        ///     <see cref="_measureList" />
        /// </summary>
        private void PopulateComponentDic()
        {
            var dimensionList = this._dimensionList;
            if (dimensionList != null)
            {
                this.PopulateComponentDic(dimensionList.Dimensions);
            }

            var attribtueList = this._attributeList;
            if (attribtueList != null)
            {
                this.PopulateComponentDic(attribtueList.Attributes);
            }

            if (this._measureList != null)
            {
                this.PopulateComponentDic(this.MeasureList.Measures ?? new[] { this._measureList.PrimaryMeasure });
            }

        }

        /// <summary>
        ///     Populate <see cref="_componentIdToComponent" /> from <paramref name="components" />
        /// </summary>
        /// <param name="components">
        ///     The components.
        /// </param>
        /// <typeparam name="TComponent">
        ///     The <paramref name="components" /> type
        /// </typeparam>
        private void PopulateComponentDic<TComponent>(IEnumerable<TComponent> components) where TComponent : IComponent
        {
            foreach (TComponent component in components)
            {
                string id = component.Id;
                this._componentIdToComponent.Add(id, component);
            }
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void Validate()
        {
            if (!this.IsExternalReference.IsTrue)
            {
                IDictionary<string, IDimension> dimensionMap = new Dictionary<string, IDimension>(
                    StringComparer.Ordinal);

                ISet<string> conceptIds = new HashSet<string>(StringComparer.Ordinal);

                // VALIDATE DIMENSIONS
                this.ValidateDimensions(conceptIds, dimensionMap);

                // VALIDATE ONLY ONE FREQUENCY DIMENSION
                this.ValidateFrequencyDimension();

                // VALIDATE GROUPS
                ISet<string> groupIds = new HashSet<string>();
                this.ValidateGroups(dimensionMap, groupIds);

                // VALIDATE PRIMARY MEASURE
                this.ValidatePrimaryMeasure(conceptIds);

                // VALIDATE ATTRIBUTES
                this.ValidateAttributes(conceptIds, groupIds);
            }
        }

        /// <summary>
        /// Validates the dimensions.
        /// </summary>
        /// <param name="conceptIds">The concept ids.</param>
        /// <param name="dimensionMap">The dimension map.</param>
        /// <exception cref="SdmxSemmanticException">
        /// DSD must have at least one dimension
        /// or
        /// Two dimensions can not share the same dimension position :  + dimension.Position
        /// </exception>
        private void ValidateDimensions(ISet<string> conceptIds, IDictionary<string, IDimension> dimensionMap)
        {
            ISet<int> dimPos = new HashSet<int>();
            if (!ObjectUtil.ValidCollection(this.GetDimensions()))
            {
                throw new SdmxSemmanticException("DSD must have at least one dimension");
            }

            foreach (IDimension dimension in this.GetDimensions())
            {
                if (dimPos.Contains(dimension.Position))
                {
                    throw new SdmxSemmanticException("Two dimensions can not share the same dimension position : " + dimension.Position);
                }

                if(dimension.Representation != null && dimension.Representation.TextFormat != null)
                {
                    if (dimension.Representation.TextFormat.Multilingual == TertiaryBool.GetFromEnum(TertiaryBoolEnumType.True))
                    {
                        throw new SdmxSemmanticException("Dimensions are not allowed to have Text Format with Multilingual set to true.");
                    }
                }

                dimPos.Add(dimension.Position);
                string conceptId = dimension.Id;
                this.ValidateUniqueComponent(conceptIds, dimension);
                dimensionMap.Add(conceptId, dimension);
            }
        }

        /// <summary>
        /// Validates the frequency dimension.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">DataStructure can not have more then one frequency dimension</exception>
        private void ValidateFrequencyDimension()
        {
            bool foundFreq = false;

            foreach (IDimension dimension0 in this.GetDimensions())
            {
                if (dimension0.FrequencyDimension)
                {
                    if (foundFreq)
                    {
                        throw new SdmxSemmanticException("DataStructure can not have more then one frequency dimension");
                    }

                    foundFreq = true;
                }
            }
        }

        /// <summary>
        /// Validates the groups.
        /// </summary>
        /// <param name="dimensionMap">The dimension map.</param>
        /// <param name="groupIds">The group ids.</param>
        /// <exception cref="SdmxSemmanticException">Duplicate group found</exception>
        private void ValidateGroups(IDictionary<string, IDimension> dimensionMap, ISet<string> groupIds)
        {
            if (this._groups != null)
            {
                foreach (IGroup group in this._groups)
                {
                    foreach (string dimensionRef in @group.DimensionRefs)
                    {
                        this.ValidateGroupDimensionRef(dimensionMap, dimensionRef, @group);
                    }

                    if (groupIds.Contains(@group.Id))
                    {
                        throw new SdmxSemmanticException(ExceptionCode.KeyFamilyDuplicateGroupId, @group.Id);
                    }

                    groupIds.Add(@group.Id);
                }
            }
        }

        /// <summary>
        /// Validates the group dimension reference.
        /// </summary>
        /// <param name="dimensionMap">The dimension map.</param>
        /// <param name="dimensionRef">The dimension reference.</param>
        /// <param name="sdmxGroup">The SDMX group.</param>
        /// <exception cref="SdmxSemmanticException">
        /// Group cannot reference Time Dimension
        /// </exception>
        private void ValidateGroupDimensionRef(IDictionary<string, IDimension> dimensionMap, string dimensionRef, IGroup sdmxGroup)
        {
            if (!dimensionMap.ContainsKey(dimensionRef))
            {
                IDimension timeDimension = this.TimeDimension;
                if (timeDimension != null)
                {
                    if (timeDimension.Id.Equals(dimensionRef))
                    {
                        throw new SdmxSemmanticException(ExceptionCode.GroupCannotReferenceTimeDimension, sdmxGroup.Id);
                    }
                }

                throw new SdmxSemmanticException(
                    ExceptionCode.ReferenceError, 
                    SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dimension) + " " + dimensionRef, 
                    SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Group), 
                    sdmxGroup.ToString());
            }
        }

        /// <summary>
        /// Validates the primary measure.
        /// </summary>
        /// <param name="conceptIds">The concept ids.</param>
        /// <exception cref="SdmxSemmanticException">Missing PrimaryMeasure</exception>
        private void ValidatePrimaryMeasure(ISet<string> conceptIds)
        {
            if (this.PrimaryMeasure != null)
            {
                this.ValidateUniqueComponent(conceptIds, this.PrimaryMeasure);
            }
            // if there is primary measure
            else if (this.Measures.Any())
            {
                foreach (IMeasure currentBean in this.Measures)
                {
                    this.ValidateUniqueComponent(conceptIds, currentBean);
                }
            }
            else if (!this.IsExternalReference.IsTrue)
            {
                // SDMX 3.0.0 it is possible to have no measures
                // throw new SdmxSemmanticException(ExceptionCode.ObjectMissingRequiredElement, this.StructureType, "PrimaryMeasure");
            }
        }

        /// <summary>
        /// Validates the attributes.
        /// </summary>
        /// <param name="conceptIds">The concept ids.</param>
        /// <param name="groupIds">The group ids.</param>
        /// <exception cref="SdmxSemmanticException">Group attribute missing group id.</exception>
        private void ValidateAttributes(ISet<string> conceptIds, ISet<string> groupIds)
        {
            foreach (IAttributeObject current in this.Attributes)
            {
                this.ValidateUniqueComponent(conceptIds, current);
                if (current.AttachmentLevel == AttributeAttachmentLevel.Group)
                {
                    if (current.AttachmentGroup == null)
                    {
                        throw new SdmxSemmanticException(ExceptionCode.KeyFamilyGroupAttributeMissingGroupId, current.ToString());
                    }

                    if (!groupIds.Contains(current.AttachmentGroup))
                    {
                        throw new SdmxSemmanticException(
                            ExceptionCode.ReferenceError,
                            SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Group),
                            SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataAttribute),
                            current.ToString());
                    }
                }

                if (current.MeasureRelationships != null && current.MeasureRelationships.Any())
                {
                    /* 200 Attributes relationship to measures In addition to attaching attributes to a specific level within the dataset, their relationship to measures can also be defined
					 source: https://sdmx.org/wp-content/uploads/SDMX_3-0-0_Major_Changes_FINAL-1_0.pdf
                    if (current.AttachmentLevel != AttributeAttachmentLevel.Observation)
                    {
                        // TODO introduce ExceptionCode ???
                        throw new SdmxSemmanticException("Attribute with measure relationship should only have OBSERVATION attachment level");
                    }
                    */

                    List<string> allMeasureIds = Measures.Select(m => m.Id).ToList();
                    if (!allMeasureIds.ContainsAll(current.MeasureRelationships))
                    {
                        throw new SdmxSemmanticException(ExceptionCode.ReferenceError, SdmxStructureEnumType.Measure, SdmxStructureEnumType.DataAttribute, current.ToString());
                    }
                }
            }

            if (this._attributeList != null && this._attributeList.MetadataAttributes != null)
            {
                if (this._metadataStructure == null && this.GetMetadataAttributes().Any())
                {
                    // TODO introduce ExceptionCode
                    throw new SdmxSemmanticException("Metadata Structure not set in DSD but metadata attribute usages defined");
                }
                foreach (IMetadataAttributeUsage currentBean in this.AttributeList.MetadataAttributes)
                {
                    if (currentBean.AttachmentLevel == AttributeAttachmentLevel.Group)
                    {
                        if (currentBean.AttachmentGroup == null)
                        {
                            throw new SdmxSemmanticException(ExceptionCode.KeyFamilyGroupAttributeMissingGroupId, currentBean.ToString());
                        }
                        if (!groupIds.Contains(currentBean.AttachmentGroup))
                        {
                            throw new SdmxSemmanticException(ExceptionCode.ReferenceError, SdmxStructureEnumType.Group, SdmxStructureEnumType.DataAttribute, currentBean.ToString());
                        }
                    }
                }
            }
        }

        /// <inheritdoc/>
        public IList<IMetadataAttributeUsage> GetMetadataAttributes()
        {
            if (this._attributeList == null || this._attributeList.MetadataAttributes == null)
            {
                return new List<IMetadataAttributeUsage>();
            }
            return this._attributeList.MetadataAttributes;
        }

        /// <inheritdoc/>
        public IMetadataAttributeUsage GetMetadataAttribute(string id)
        {
            if (ObjectUtil.ValidString(id))
            {
                foreach (IMetadataAttributeUsage currentAttribute in GetMetadataAttributes())
                {
                    if (currentAttribute.MetadataAttributeReference.Equals(id))
                    {
                        return currentAttribute;
                    }
                }
            }
            return null;
        }

        /// <inheritdoc/>
        public IList<IMetadataAttributeUsage> GetDatasetMetadataAttributes()
        {
            return GetMetadataAttribute(AttributeAttachmentLevel.DataSet);
        }

        /// <inheritdoc/>
        public IList<IMetadataAttributeUsage> GetGroupMetadataAttributes()
        {
            return GetMetadataAttribute(AttributeAttachmentLevel.Group);
        }

        /// <inheritdoc/>
        public IList<IMetadataAttributeUsage> GetGroupMetadataAttributes(string groupId, bool includeDimensionGroup)
        {
            IGroup group = GetGroup(groupId);
            if (group == null)
            {
                throw new ArgumentException("Group not found on Data Structure '" + Urn + "' with id: " + groupId);
            }
            IList<IMetadataAttributeUsage> allGroupMetadataAttributes = GetGroupMetadataAttributes();
            List<IMetadataAttributeUsage> returnList = new List<IMetadataAttributeUsage>();

            foreach (IMetadataAttributeUsage currentMetadataAttribute in allGroupMetadataAttributes)
            {
                if (currentMetadataAttribute.AttachmentGroup != null)
                {
                    if (currentMetadataAttribute.AttachmentGroup.Equals(groupId))
                    {
                        returnList.Add(currentMetadataAttribute);
                    }
                }
            }

            foreach (IMetadataAttributeUsage cuMetadataAttributeBean in GetDimensionGroupMetadataAttributes())
            {
                IList<String> attrDimRefs = cuMetadataAttributeBean.DimensionReferences;
                IList<String> grpDimRefs = group.DimensionRefs;
                if (attrDimRefs.ContainsAll(grpDimRefs) && grpDimRefs.ContainsAll(attrDimRefs))
                {
                    returnList.Add(cuMetadataAttributeBean);
                }
            }
            return returnList;
        }

        /// <inheritdoc/>
        public IList<IMetadataAttributeUsage> GetDimensionGroupMetadataAttributes()
        {
            return GetMetadataAttribute(AttributeAttachmentLevel.DimensionGroup);
        }

        /// <inheritdoc/>
        public IList<IMetadataAttributeUsage> GetObservationMetadataAttributes()
        {
            return GetMetadataAttribute(AttributeAttachmentLevel.Observation);
        }

        /// <inheritdoc/>
        public IList<IMetadataAttributeUsage> GetObservationMetadataAttributes(string crossSectionalConcept)
        {
            if (crossSectionalConcept == null)
            {
                return GetMetadataAttributes();
            }
            List<IMetadataAttributeUsage> returnList = new List<IMetadataAttributeUsage>();
            foreach (IMetadataAttributeUsage att in GetDimensionGroupMetadataAttributes())
            {
                if (att.DimensionReferences.Contains(crossSectionalConcept))
                {
                    returnList.Add(att);
                }
            }
            returnList.AddRange(GetObservationMetadataAttributes());
            return returnList;
        }

        /// <inheritdoc/>
        public IList<IMetadataAttributeUsage> GetSeriesMetadataAttributes(string crossSectionalConcept)
        {
            if (crossSectionalConcept == null)
            {
                return GetDimensionGroupMetadataAttributes();
            }
            List<IMetadataAttributeUsage> returnList = new List<IMetadataAttributeUsage>();
            foreach (IMetadataAttributeUsage att in GetDimensionGroupMetadataAttributes())
            {
                if (!att.DimensionReferences.Contains(crossSectionalConcept))
                {
                    returnList.Add(att);
                }
            }
            return returnList;
        }

        /// <inheritdoc/>
        public IMeasure GetMeasure(string id)
        {
            if (ObjectUtil.ValidString(id))
            {
                var component = GetComponent(id);
                if(component == null || component.StructureType != SdmxStructureEnumType.Measure)
                {
                    return null;
                }

                return component as IMeasure;
            }

            return null;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM ITSELF, CREATES STUB OBJECT //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////    

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECTS             //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
    }
}