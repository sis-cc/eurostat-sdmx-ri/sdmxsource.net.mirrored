// -----------------------------------------------------------------------
// <copyright file="MeasureListCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.DataStructure
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

    using MeasureList = Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant.MeasureList;
    using PrimaryMeasureType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.PrimaryMeasureType;

    /// <summary>
    ///     The measure list core.
    /// </summary>
    [Serializable]
    public class MeasureListCore : IdentifiableCore, IMeasureList
    {
        private readonly IList<IMeasure> _measures = new List<IMeasure>();
        private readonly IMeasure _primaryMeasure;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MeasureListCore" /> class.
        /// </summary>
        /// <param name="measureList">
        ///     The measure list.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public MeasureListCore(IMeasureListMutableObject measureList, IMaintainableObject parent)
            : base(measureList, parent)
        {
            if (measureList.Measures != null)
            {
                foreach (IMeasureMutableObject currentMeasure in measureList.Measures)
                {
                    this._measures.Add(new MeasureCore(currentMeasure, this));
                }
            }
            if (measureList.PrimaryMeasure != null)
            {
                // if Primary measure is defined then we have 2.x DSD and it is OK
                // This constructor is *not* only for SDMX 3.0.0
                // TODO maybe throw an exception if both are set since they are mutally exclusive ?
                MeasureCore item = new MeasureCore(measureList.PrimaryMeasure, this);
                this._primaryMeasure = item;
                if (this._measures == null || !this._measures.Any())
                {
                    this._measures.Add(item);
                }
            }
            
            // we might not have a primary measure but have a 3.0.0 DSD that is compatible with 2.1
            if (_primaryMeasure == null && _measures != null && _measures.Count == 1)
            {
                _primaryMeasure = _measures.FirstOrDefault(m => string.Equals(Api.Constants.InterfaceConstant.PrimaryMeasure.FixedId, m.Id, StringComparison.Ordinal));
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="MeasureListCore" /> class.
        /// </summary>
        /// <param name="measureList">
        ///     The measure list.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public MeasureListCore(MeasureListType measureList, IMaintainableObject parent)
            : base(measureList, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MeasureDescriptor), parent)
        {
            if (measureList.Component != null)
            {
                MeasureCore item = new MeasureCore((Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure.PrimaryMeasureType)
                                        measureList.Component[0].Content, this);
                _primaryMeasure = item;
                this._measures.Add(item);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="MeasureListCore" /> class.
        /// </summary>
        /// <param name="primaryMeasure">
        ///     The primary measure.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public MeasureListCore(PrimaryMeasureType primaryMeasure, IMaintainableObject parent)
            : base(MeasureList.FixedId, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MeasureDescriptor), parent)
        {
            MeasureCore item = new MeasureCore(primaryMeasure, this);
            _primaryMeasure = item;
            this._measures.Add(item);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="MeasureListCore" /> class.
        /// </summary>
        /// <param name="primaryMeasure">
        ///     The primary measure.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public MeasureListCore(
            Org.Sdmx.Resources.SdmxMl.Schemas.V10.Structure.PrimaryMeasureType primaryMeasure,
            IMaintainableObject parent)
            : base(MeasureList.FixedId, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MeasureDescriptor), parent)
        {
            MeasureCore item = new MeasureCore(primaryMeasure, this);
            _primaryMeasure = item;
            this._measures.Add(item);
        }

        /// <summary>
        ///     Gets the id.
        /// </summary>
        public override string Id
        {
            get
            {
                return MeasureList.FixedId;
            }
        }

        /// <summary>
        ///     Gets the primary measure.
        ///     Obsolete in SDMX v3.0.0 schema
        /// </summary>
        public virtual IMeasure PrimaryMeasure
        {
            get
            {
                return _primaryMeasure;
            }
        }

        /// <summary>
        /// Gets the SDMX measures
        /// Introduced in SDMX v3.0.0 schema
        /// </summary>
        public virtual IList<IMeasure> Measures => new ReadOnlyCollection<IMeasure>(this._measures);

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IMeasureList)sdmxObject;
                if (!this.Equivalent(Measures, that.Measures, includeFinalProperties))
                {
                    return false;
                }

                return this.DeepEqualsIdentifiable(that, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     Get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._measures, composites);
            return composites;
        }
    }
}