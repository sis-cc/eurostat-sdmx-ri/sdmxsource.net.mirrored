// -----------------------------------------------------------------------
// <copyright file="MeasureCore.cs" company="EUROSTAT">
//   Date Created : 2021-10-18
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.DataStructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;
    using PrimaryMeasure = Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant.PrimaryMeasure;

    /// <summary>
    ///     The measure core.
    ///     Introduced in SDMX v3.0.0 schema
    /// </summary>
    [Serializable]
    public class MeasureCore : ComponentCore, IMeasure
    {
        private readonly IList<ICrossReference> _conceptRoles = new List<ICrossReference>();

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECT            //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>
        ///     Initializes a new instance of the <see cref="MeasureCore" /> class.
        /// </summary>
        /// <param name="itemMutableObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public MeasureCore(IMeasureMutableObject itemMutableObject, IMeasureList parent)
            : base(itemMutableObject, parent)
        {
            if (itemMutableObject.ConceptRoles != null)
            {
                foreach (var conceptRole in itemMutableObject.ConceptRoles)
                {
                    this._conceptRoles.Add(new CrossReferenceImpl(this, conceptRole));
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="PrimaryMeasureCore" /> class.
        /// </summary>
        /// <param name="primaryMeasure">
        ///     The primary measure.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public MeasureCore(PrimaryMeasureType primaryMeasure, IMeasureList parent)
            : base(primaryMeasure, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.PrimaryMeasure), parent)
        {
            this.Validate();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="PrimaryMeasureCore" /> class.
        /// </summary>
        /// <param name="primaryMeasure">
        ///     The primary measure.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public MeasureCore(
            Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.PrimaryMeasureType primaryMeasure,
            IMeasureList parent)
            : base(
                primaryMeasure,
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.PrimaryMeasure),
                primaryMeasure.PassNoNull("primaryMeasure").Annotations,
                primaryMeasure.PassNoNull("primaryMeasure").TextFormat,
                primaryMeasure.PassNoNull("primaryMeasure").codelistAgency,
                primaryMeasure.PassNoNull("primaryMeasure").codelist,
                primaryMeasure.PassNoNull("primaryMeasure").codelistVersion,
                primaryMeasure.PassNoNull("primaryMeasure").conceptSchemeAgency,
                primaryMeasure.PassNoNull("primaryMeasure").conceptSchemeRef,
                GetConceptSchemeVersion(primaryMeasure),
                primaryMeasure.PassNoNull("primaryMeasure").codelistAgency,
                primaryMeasure.PassNoNull("primaryMeasure").conceptRef,
                parent)
        {
            this.Validate();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="PrimaryMeasureCore" /> class.
        /// </summary>
        /// <param name="primaryMeasure">
        ///     The primary measure.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public MeasureCore(
            Org.Sdmx.Resources.SdmxMl.Schemas.V10.Structure.PrimaryMeasureType primaryMeasure,
            IMeasureList parent)
            : base(primaryMeasure, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.PrimaryMeasure), primaryMeasure.PassNoNull("primaryMeasure").Annotations, null, primaryMeasure.PassNoNull("primaryMeasure").concept, parent)
        {
            this.Validate();
        }

        public MeasureCore(IPrimaryMeasureMutableObject itemMutableObject, IMeasureList parent)
            : base(itemMutableObject, parent)
        {
            this.Validate();
            // For backwards compatibility
            this.StructureType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.PrimaryMeasure);
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                IMeasure that = (IMeasure)sdmxObject;
               
                if (!ObjectUtil.EquivalentCollection(_conceptRoles, that.ConceptRoles))
                {
                    return false;
                }

                return this.DeepEqualsComponent(that, includeFinalProperties);
            }

            return false;
        }

        /// <inheritdoc/>
        public IList<ICrossReference> ConceptRoles => this._conceptRoles;

        /// <summary>
        ///     The get parent ids.
        /// </summary>
        /// <param name="includeDifferentTypes">
        ///     The include different types.
        /// </param>
        /// <returns>
        ///     The <see cref="IList{T}" /> .
        /// </returns>
        protected internal override IList<string> GetParentIds(bool includeDifferentTypes)
        {
            IList<string> returnList = new List<string>();
            returnList.Add(this.Id);
            return returnList;
        }

        /// <summary>
        ///     Returns concept scheme version. It tries to detect various conventions
        /// </summary>
        /// <param name="primaryMeasure">
        ///     The primaryMeasure.
        /// </param>
        /// <returns>
        ///     The concept scheme version; otherwise null
        /// </returns>
        private static string GetConceptSchemeVersion(
            Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.PrimaryMeasureType primaryMeasure)
        {
            if (!string.IsNullOrWhiteSpace(primaryMeasure.conceptVersion))
            {
                return primaryMeasure.conceptVersion;
            }

            if (!string.IsNullOrWhiteSpace(primaryMeasure.ConceptSchemeVersionEstat))
            {
                return primaryMeasure.ConceptSchemeVersionEstat;
            }

            var extDimension =
                primaryMeasure as Org.Sdmx.Resources.SdmxMl.Schemas.V20.extension.structure.PrimaryMeasureType;
            if (extDimension != null && !string.IsNullOrWhiteSpace(extDimension.conceptSchemeVersion))
            {
                return extDimension.conceptSchemeVersion;
            }

            return null;
        }

        private void Validate()
        {
            this.OriginalId = PrimaryMeasure.FixedId;
        }

        /// <inheritdoc/>
        public IMeasure ToSDMX3Measure()
        {
            if (this.StructureType.EnumType == SdmxStructureEnumType.Measure)
            {
                return this;
            }

            MeasureMutableCore mutableObject = new MeasureMutableCore(this);
            mutableObject.StructureType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Measure);
            return new MeasureCore(mutableObject, (IMeasureList) this.Parent);
        }
    }
}