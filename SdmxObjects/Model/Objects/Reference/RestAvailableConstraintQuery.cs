// -----------------------------------------------------------------------
// <copyright file="RestAvailableConstraintQuery.cs" company="EUROSTAT">
//   Date Created : 2018-04-26
//   Copyright (c) 2018 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.Util.Objects;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// Stores a DynamicConstration aka ActualConstraint aka Cube aka AvalableData REST query
    /// </summary>
    public class RestAvailableConstraintQuery : IRestAvailableConstraintQuery
    {
        #region fields

        /// <summary>
        /// THIS WILL MOST PROBABLY CHANGE. 
        /// </summary>
        public const string ResourceName = "availableconstraint";

        /// <summary>
        /// Overridable field for the <see cref="ResourceName"/>
        /// </summary>
        protected virtual string QueryResourceName => ResourceName;

        /// <summary>
        /// The reference parameter
        /// </summary>
        protected const string ReferencesQueryParameter = "references";

        /// <summary>
        /// The mode parameter
        /// </summary>
        protected const string ModeQueryParameter = "mode";

        /// <summary>
        /// ALL wild card
        /// </summary>
        protected virtual string WildCard => "ALL";

        /// <summary>
        /// The data query
        /// </summary>
        protected readonly IRestDataQuery DataQuery;

        /// <summary>
        /// Reference value, can be null which means none, any which means all and either the URN class of <see cref="SdmxStructureEnumType.Dsd"/> <see cref="SdmxStructureEnumType.CodeList"/> <see cref="SdmxStructureEnumType.Dataflow"/> <see cref="SdmxStructureEnumType.ConceptScheme"/> 
        /// </summary>
        private readonly SdmxStructureType _specificStructure;

        /// <summary>
        /// The component ids
        /// </summary>
        private readonly List<string> _componentIds = new List<string>();

        /// <summary>
        /// Holds the query components
        /// </summary>
        protected readonly IList<string> Path;

        /// <summary>
        /// Holds the initial query parameters
        /// </summary>
        protected readonly IDictionary<string, string> QueryParameters;

        #endregion fields

        /// <inheritdoc />
        public RestAvailableConstraintQuery(string reststring, IDictionary<string, string> queryParameter)
        {
            if (reststring == null)
            {
                throw new System.ArgumentNullException("reststring");
            }

            int dataIndex = reststring.IndexOf(QueryResourceName + "/", StringComparison.Ordinal);
            if (dataIndex == -1)
            {
                throw new SdmxSyntaxException("Query not for resource :" + QueryResourceName);
            }

            if (dataIndex > 0 && reststring[dataIndex - 1] != '/')
            {
                throw new SdmxSyntaxException("Query not for resource :" + QueryResourceName);
            }

            // TODO the following depends if provider ref makes it in the standard (open question at this time)
            if (queryParameter == null)
            {
                queryParameter = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            }

            QueryParameters = new Dictionary<string, string>(queryParameter);

            Path = UrnUtil.ExtractPathAndQueryParameters(reststring.Substring(dataIndex), queryParameter);

            this._componentIds.AddRange(GetComponentIds(Path));

            var referenceValue = GetReferenceValue(queryParameter);
            // all -> Any , none-> null, others expect to be codelist, conceptscheme, datastructure, dataflow
            _specificStructure = ParseReference(referenceValue);

            Mode = GetMode(queryParameter);

            DataQuery = BuildDataQuery(Path, queryParameter);
        }


        #region properties

        /// <inheritdoc />
        public ISdmxDate EndPeriod
        {
            get
            {
                return this.DataQuery.EndPeriod;
            }
        }

        /// <inheritdoc />
        public virtual IStructureReference FlowRef
        {
            get
            {
                return this.DataQuery.FlowRef;
            }
        }

        /// <inheritdoc />
        public IStructureReference ProviderRef
        {
            get
            {
                return this.DataQuery.ProviderRef;
            }
        }

        /// <inheritdoc />
        public IList<ISet<string>> QueryList
        {
            get
            {
                return this.DataQuery.QueryList;
            }
        }

        /// <inheritdoc />
        public string RestQuery
        {
            get
            {
                return this.ToString();
            }
        }

        /// <inheritdoc />
        public ISdmxDate StartPeriod
        {
            get
            {
                return this.DataQuery.StartPeriod;
            }
        }

        /// <inheritdoc />
        public ISdmxDate UpdatedAfter
        {
            get
            {
                return this.DataQuery.UpdatedAfter;
            }
        }

        /// <inheritdoc />
        public SdmxStructureType SpecificStructureReference
        {
            get
            {
                return this._specificStructure;
            }
        }

        /// <inheritdoc />
        public IList<string> ComponentIds
        {
            get
            {
                return this._componentIds.ToArray();
            }
        }

        /// <inheritdoc />
        public AvailableConstraintQueryMode Mode { get; private set; }

        #endregion properties

        #region methods

        /// <summary>
        ///     Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        ///     A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            var path = new List<string>();

            path.Add(ResourceName);

            IMaintainableRefObject flowRefMaint = this.FlowRef.MaintainableReference;
            string flowAgency = flowRefMaint.AgencyId ?? WildCard;
            string flowId = flowRefMaint.MaintainableId ?? WildCard;
            string flowVersion = flowRefMaint.Version ?? WildCard;
            path.Add(flowAgency + "," + flowId + "," + flowVersion);

            if (!ObjectUtil.ValidCollection(this.QueryList))
            {
                path.Add(WildCard);
            }
            else
            {
                List<string> dimensionValues = new List<string>();
                foreach (ISet<string> currentQuery in this.QueryList)
                {
                    dimensionValues.Add(string.Join("+", currentQuery));

                }

                path.Add(string.Join(".", dimensionValues));
            }

            if (this.DataQuery.ProviderRef == null)
            {
                path.Add(WildCard);
            }
            else
            {
                IMaintainableRefObject provRefMaint = this.DataQuery.ProviderRef.MaintainableReference;

                string provAgency = provRefMaint.AgencyId ?? WildCard;
                string provId = (this.DataQuery.ProviderRef.ChildReference == null) ? WildCard : this.DataQuery.ProviderRef.ChildReference.Id;
                path.Add(provAgency + "," + provId);
            }

            if (!ObjectUtil.ValidCollection(this.ComponentIds))
            {
                path.Add(WildCard);
            }
            else
            {
                path.Add(string.Join("+", this.ComponentIds));
            }

            var queryParameters = new List<KeyValuePair<string, string>>();
            if (this.StartPeriod != null)
            {
                queryParameters.Add(new KeyValuePair<string, string>("startPeriod", this.StartPeriod.DateInSdmxFormat));
            }

            if (this.EndPeriod != null)
            {
                queryParameters.Add(new KeyValuePair<string, string>("endPeriod", this.EndPeriod.DateInSdmxFormat));
            }

            if (this.UpdatedAfter != null)
            {
                queryParameters.Add(new KeyValuePair<string, string>("updatedAfter", this.UpdatedAfter.DateInSdmxFormat));
            }

            if (this.SpecificStructureReference != null)
            {
                if (this.SpecificStructureReference.EnumType == SdmxStructureEnumType.Any)
                {
                    queryParameters.Add(new KeyValuePair<string, string>(ReferencesQueryParameter, WildCard));
                }
                else
                {
                    queryParameters.Add(new KeyValuePair<string, string>(ReferencesQueryParameter, this.SpecificStructureReference.UrnClass));
                }
            }
            else
            {
                queryParameters.Add(new KeyValuePair<string, string>(ReferencesQueryParameter, "none"));
            }

            queryParameters.Add(new KeyValuePair<string, string>(ModeQueryParameter, Mode.ToString("G").ToLowerInvariant()));

            var pathStr = string.Join("/", path);
            if (queryParameters.Count > 0)
            {
                var queryParameter = string.Join("&", queryParameters.Select(x => string.Format(CultureInfo.InvariantCulture, "{0}={1}", x.Key, x.Value)));
                return string.Format(CultureInfo.InvariantCulture, "{0}?{1}", pathStr, queryParameter);
            }

            return pathStr;
        }
        
        private static AvailableConstraintQueryMode GetMode(IDictionary<string, string> queryParameter)
        {
            string mode;
            if (queryParameter.TryGetValue(ModeQueryParameter, out mode))
            {
                queryParameter.Remove(ModeQueryParameter);
            }

            AvailableConstraintQueryMode modeType;
            if (mode != null && Enum.TryParse(mode, true, out modeType))
            {
               return modeType;
            }

            return AvailableConstraintQueryMode.Exact;
        }

        private static string GetReferenceValue(IDictionary<string, string> queryParameter)
        {
            string referenceValue;
            if (queryParameter.TryGetValue(ReferencesQueryParameter, out referenceValue))
            {
                queryParameter.Remove(ReferencesQueryParameter);
            }

            return referenceValue;
        }

        /// <summary>
        /// Parse the reference query parameter value.
        /// </summary>
        /// <param name="referenceValue">the value</param>
        /// <returns></returns>
        protected virtual SdmxStructureType ParseReference(string referenceValue)
        {
            if (string.IsNullOrWhiteSpace(referenceValue)  ||
                referenceValue.Equals("none", StringComparison.OrdinalIgnoreCase))
            {
                return null;
            }

            if (WildCard.Equals(referenceValue, StringComparison.OrdinalIgnoreCase))
            {
                return SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Any);
            }

            return SdmxStructureType.ParseClass(referenceValue);
        }

        /// <summary>
        /// Builds the <see cref="IRestDataQuery"/> object.
        /// </summary>
        /// <param name="queryPath">the query path.</param>
        /// <param name="queryParameter">the query parameters.</param>
        /// <returns></returns>
        protected virtual IRestDataQuery BuildDataQuery(IList<string> queryPath, IDictionary<string, string> queryParameter)
        {
            List<string> fakeDataPath = new List<string>
            {
                "data"
            };

            // for data we want data/flowref/key/providerRef but we might have <resource>/flowref/key/providerRef/componentIds
            for (int i = 1; i < Math.Min(queryPath.Count, 4); i++)
            {
                fakeDataPath.Add(queryPath[i]);
            }

            var dataQuery = new RESTDataQueryCore(fakeDataPath, queryParameter);
            if (dataQuery.FlowRef == null)
            {
                throw new SdmxSemmanticException("At least the Dataflow id must be specified");
            }

            return dataQuery;
        }

        /// <summary>
        /// Gets from the query the component ids for which the application will return the information.
        /// </summary>
        /// <param name="path">The query path</param>
        /// <returns></returns>
        protected virtual IList<string> GetComponentIds(IList<string> path)
        {
            var result = new List<String>();
            if (Path.Count > 4)
            {
                var componentPart = Path[4];
                if (!WildCard.Equals(componentPart, StringComparison.OrdinalIgnoreCase) && !string.IsNullOrWhiteSpace(componentPart))
                {
                    result.AddRange(componentPart.Split('+'));
                }
            }
            return result;
        }

        #endregion methods
    }
}