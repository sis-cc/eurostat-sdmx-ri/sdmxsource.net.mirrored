// -----------------------------------------------------------------------
// <copyright file="AvailableConstraintQuery.cs" company="EUROSTAT">
//   Date Created : 2018-04-28
//   Copyright (c) 2018 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    /// Builds the info needed to get the available data requested.
    /// </summary>
    /// <seealso cref="Org.Sdmxsource.Sdmx.Api.Model.Query.IAvailableConstraintQuery" />
    public class AvailableConstraintQuery : IAvailableConstraintQuery
    {
        /// <summary>
        /// The structure retriever
        /// </summary>
        protected readonly ISdmxObjectRetrievalManager RetrievalManger;

        /// <summary>
        /// Initializes a new instance of the <see cref="AvailableConstraintQuery" /> class.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="retrievalManger">The retrieval manger.</param>
        /// <exception cref="ArgumentNullException">query
        /// or
        /// retrievalManger</exception>
        /// <exception cref="SdmxNoResultsException">Cannot find dataflow for query " + query.FlowRef
        /// or
        /// Cannot find datasetructure for query " + Dataflow.DataStructureRef</exception>
        public AvailableConstraintQuery(IRestAvailableConstraintQuery query, ISdmxObjectRetrievalManager retrievalManger)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            if (retrievalManger == null)
            {
                throw new ArgumentNullException(nameof(retrievalManger));
            }
            this.RetrievalManger = retrievalManger;

            LastUpdatedDate = query.UpdatedAfter;
            DateFrom = query.StartPeriod;
            DateTo = query.EndPeriod;
            DataProvider = RetrieveDataProviders(query, retrievalManger);
            SpecificStructureReference = query.SpecificStructureReference;
            Mode = query.Mode;

            RetrieveArtefactInfo(query);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AvailableConstraintQuery"/> class.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <exception cref="ArgumentNullException">query</exception>
        /// <exception cref="SdmxNoResultsException">
        /// Cannot find dataflow for query " + query.Dataflow.AsReference
        /// or
        /// Cannot find datasetructure for query " + Dataflow.DataStructureRef
        /// </exception>
        public AvailableConstraintQuery(IDataQuery query)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            Dataflow = query.Dataflow;
            if (Dataflow == null)
            {
                throw new SdmxNoResultsException("Cannot find dataflow for query " + query.Dataflow.AsReference);
            }

            DataStructure = query.DataStructure;
            if (DataStructure == null)
            {
                throw new SdmxNoResultsException("Cannot find datasetructure for query " + Dataflow.DataStructureRef);
            }

            FirstNObservations = query.FirstNObservations;
            LastNObservations = query.LastNObservations;

            DimensionAtObservation = query.DimensionAtObservation;

            LastUpdatedDate = query.LastUpdatedDate;
            DataProvider = query.DataProvider;
            SetQueryFilters(query);
        }

        #region interface properties & methods

        /// <summary>
        /// Gets the Dataflow that the query is returning data for.
        /// </summary>
        /// <value>The dataflow.</value>
        public IDataflowObject Dataflow   {  get; protected set;  }

        /// <summary>
        /// Gets the data provider(s) that the query is for, an empty list represents ALL
        /// </summary>
        /// <value>The data provider.</value>
        public ISet<IDataProvider> DataProvider { get; protected set; }

        /// <summary>
        /// Gets the Key Family (Data Structure Definition) that this query is returning data for.
        /// </summary>
        /// <value>The data structure.</value>
        public IDataStructureObject DataStructure { get; protected set; }

        /// <summary>
        /// Gets last updated date is a filter on the data meaning 'only return data that was updated after this time'
        /// <p />
        /// If this attribute is used, the returned message should
        /// only include the latest version of what has changed in the database since that point in time (updates and
        /// revisions).
        /// <p />
        /// This should include:
        /// <ul><li>Observations that have been added since the last time the query was performed (INSERT)</li><li>Observations that have been revised since the last time the query was performed (UPDATE)</li><li>Observations that have been revised since the last time the query was performed (UPDATE)</li><li>Observations that have been deleted since the last time the query was performed (DELETE)</li></ul>
        /// If no offset is specified, default to local
        /// <p />
        /// Gets null if unspecified
        /// </summary>
        /// <value>The last updated date.</value>
        public ISdmxDate LastUpdatedDate { get; protected set; }

        /// <summary>
        /// Gets the date from in this selection group
        /// </summary>
        /// <value>The date from.</value>
        public ISdmxDate DateFrom { get; protected set; }

        /// <summary>
        /// Gets the date to in this selection group
        /// </summary>
        /// <value>The date to.</value>
        public ISdmxDate DateTo { get; protected set; }

        /// <summary>
        /// Gets the set of selections for this group - these are implicitly ANDED
        /// </summary>
        /// <value>The selections.</value>
        public IList<IKeyValues> Selections { get; protected set; }

        /// <summary>
        /// Gets the specific structure reference if reference is specified
        /// </summary>
        /// <value>The specific structure reference.</value>
        public SdmxStructureType SpecificStructureReference { get; protected set; }

        /// <summary>
        /// Gets the component identifiers.
        /// </summary>
        /// <value>The component identifier.</value>
        public IList<IComponent> Components { get; protected set; }

        /// <inheritdoc />
        public AvailableConstraintQueryMode Mode { get; protected set; }

        /// <summary>
        ///     Gets or sets the value of firstNObservations data query parameter.
        /// </summary>
        /// <value>
        ///     The first n observations.
        /// </value>
        public int? FirstNObservations { get; protected set; }

        /// <summary>
        ///     Gets or sets the value of lastNObservations data query parameter.
        /// </summary>
        /// <value>
        ///     The last n observations.
        /// </value>
        public int? LastNObservations { get; protected set; }

        /// <summary>
        ///     Gets or sets the value of dimensionAtObservation data query parameter.
        /// </summary>
        /// <value>
        ///     The dimension at observation.
        /// </value>
        public string DimensionAtObservation { get; protected set; }

        /// <summary>
        /// Gets the selection(s) for the given dimension id, returns null if no selection exist for the dimension id
        /// </summary>
        /// <param name="componentId">The component Id.</param>
        /// <returns>The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry.IKeyValues" /> .</returns>
        public IKeyValues GetSelectionsForConcept(string componentId)
        {
            return Selections.FirstOrDefault(x => x.Id.Equals(componentId));
        }

        /// <summary>
        /// Gets a value indicating whether the selections exist for this dimension Id
        /// </summary>
        /// <param name="dimensionId">The dimension Id.</param>
        /// <returns>The <see cref="T:System.Boolean" /> .</returns>
        /// <exception cref="NotImplementedException"></exception>
        public bool HasSelectionForConcept(string dimensionId)
        {
            return Selections.Any(x => x.Id.Equals(dimensionId));
        }

        #endregion

        #region virtual methods

        /// <summary>
        /// Retrieves the <see cref="Dataflow"/> and <see cref="DataStructure"/> for this availability request, 
        /// as well as the <see cref="Selections"/> and <see cref="Components"/>.
        /// </summary>
        /// <remarks>
        /// The structure reference in the <paramref name="query"/> is always a single dataflow.
        /// </remarks>
        /// <param name="query"></param>
        /// <exception cref="SdmxNoResultsException"></exception>
        protected virtual void RetrieveArtefactInfo(IRestAvailableConstraintQuery query)
        {
            Dataflow = RetrievalManger.GetMaintainableObject<IDataflowObject>(query.FlowRef);
            if (Dataflow == null)
            {
                throw new SdmxNoResultsException("Cannot find dataflow for query " + query.FlowRef);
            }

            DataStructure = RetrievalManger.GetMaintainableObject<IDataStructureObject>(Dataflow.DataStructureRef);
            if (DataStructure == null)
            {
                throw new SdmxNoResultsException("Cannot find datasetructure for query " + Dataflow.DataStructureRef);
            }

            Selections = ParseKeyValues(query, DataStructure);
            Components = ProcessComponents(query, DataStructure);
        }

        /// <summary>
        /// Parses the key values.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="dataStructure"></param>
        /// <returns>List&lt;IKeyValues&gt;.</returns>
        /// <exception cref="SdmxSemmanticException"></exception>
        protected virtual List<IKeyValues> ParseKeyValues(IRestAvailableConstraintQuery query, IDataStructureObject dataStructure)
        {
            var selections = new List<IKeyValues>();
            if (query.QueryList.Count > 0)
            {
                var orderedDim = dataStructure.GetDimensions(SdmxStructureEnumType.Dimension, SdmxStructureEnumType.MeasureDimension);
                if (orderedDim.Count != query.QueryList.Count)
                {
                    throw new SdmxSemmanticException(string.Format(CultureInfo.InvariantCulture,
                        "Not enough key values in query, expecting {0} got {1}", orderedDim.Count, query.QueryList.Count));
                }

                for (int i = 0; i < orderedDim.Count; i++)
                {
                    if (query.QueryList[i].Count > 0)
                    {
                        var keyValue = new KeyValuesMutableImpl
                        {
                            Id = orderedDim[i].Id
                        };
                        keyValue.KeyValues.AddAll(query.QueryList[i]);
                        selections.Add(new KeyValuesCore(keyValue, null));
                    }
                }
            }

            return selections;
        }

        #endregion

        /// <summary>
        /// Retrieves the data providers.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <returns>ISet&lt;IDataProvider&gt;.</returns>
        protected static ISet<IDataProvider> RetrieveDataProviders(IRestAvailableConstraintQuery dataQuery, ISdmxObjectRetrievalManager retrievalManager)
        {
            ISet<IDataProvider> dataProviders = new HashSet<IDataProvider>();
            if (dataQuery.ProviderRef != null)
            {
                ISet<IDataProviderScheme> dataProviderSchemes = retrievalManager.GetMaintainableObjects<IDataProviderScheme>(dataQuery.ProviderRef.MaintainableReference);
                foreach (IDataProviderScheme currentDpScheme in dataProviderSchemes)
                {
                    foreach (IDataProvider dataProvider in currentDpScheme.Items)
                    {
                        if (dataProvider.Id.Equals(dataQuery.ProviderRef.ChildReference.Id))
                        {
                            dataProviders.Add(dataProvider);
                        }
                    }
                }
            }

            return dataProviders;
        }

        /// <summary>
        /// Processes the components.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="dataStructure">The datastructure parent of the components.</param>
        /// <returns>List&lt;IComponent&gt;.</returns>
        /// <exception cref="SdmxSemmanticException"></exception>
        protected List<IComponent> ProcessComponents(IRestAvailableConstraintQuery query, IDataStructureObject dataStructure)
        {
            if (query.ComponentIds.Count == 0)
            {
                return new List<IComponent>(dataStructure.GetDimensions());
            }
            var components = new List<IComponent>();
            for (int i = 0; i < query.ComponentIds.Count; i++)
            {
                var component = dataStructure.GetComponent(query.ComponentIds[i]);
                if (component == null)
                {
                    throw new SdmxSemmanticException(string.Format(CultureInfo.InvariantCulture, 
                        "Requested component at position {0} not found. Available component id are the following [{1}]", i, 
                        string.Join(", ", dataStructure.Components.Select(x => x.Id))));
                }

                components.Add(component);
            }

            return components;
        }

        private void SetQueryFilters(IDataQuery query)
        {
            var selections = new List<IKeyValues>();
            var dataQuerySelectionGroup = query.SelectionGroups.FirstOrDefault();

            if (dataQuerySelectionGroup != null)
            {
                foreach (var selection in dataQuerySelectionGroup.Selections)
                {
                    var keyValue = new KeyValuesMutableImpl
                    {
                        Id = selection.ComponentId
                    };
                    keyValue.KeyValues.AddAll(selection.Values);
                    selections.Add(new KeyValuesCore(keyValue, null));
                }

                DateFrom = dataQuerySelectionGroup.DateFrom;
                DateTo = dataQuerySelectionGroup.DateTo;
            }

            Selections = selections;
        }
    }
}
