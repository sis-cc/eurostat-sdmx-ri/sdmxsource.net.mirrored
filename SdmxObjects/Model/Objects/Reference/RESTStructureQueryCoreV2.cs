// -----------------------------------------------------------------------
// <copyright file="RESTStructureQueryCoreV2.cs" company="EUROSTAT">
//   Date Created : 2021-07-06
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference
{
    #region Using directives

    using System;
    using System.Collections.Generic;
    using System.Text;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;

    #endregion

    /// <summary>
    ///     The rest structure query core.
    /// </summary>
    [Serializable]
    [Obsolete("use RestV2CommonStructureQueryBuilder")]
    public class RESTStructureQueryCoreV2 : RESTStructureQueryCore
    {
        private ISet<IStructureReference> _structureReferences = new HashSet<IStructureReference>();

        private readonly string _latestLiteral = ArtefactVersionQueryType.GetFromEnum(ArtefactVersionQueryEnumType.Latest).VersionType;

        private readonly string _latestStableLiteral = string.Join("+", 
            ArtefactVersionQueryType.GetFromEnum(ArtefactVersionQueryEnumType.Latest).VersionType,
            ArtefactVersionQueryType.GetFromEnum(ArtefactVersionQueryEnumType.Stable).VersionType);

        /// <summary>
        ///     Initializes a new instance of the <see cref="RESTStructureQueryCore" /> class.
        /// </summary>
        /// <param name="restString">
        ///     The rest string.
        /// </param>
        public RESTStructureQueryCoreV2(string restString)
            : this(restString, null)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RESTStructureQueryCore" /> class.
        ///     Creation of a Structure Query for structures that match the given reference
        /// </summary>
        /// <param name="structureReference">
        ///     The structure reference.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="structureReference"/> is <see langword="null" />.</exception>
        public RESTStructureQueryCoreV2(IStructureReference structureReference)
            : base(structureReference)
        {
            this.StructureReferences.Add(structureReference);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RESTStructureQueryCore" /> class.
        ///     Constructs a REST query from the rest query string, example:
        ///     /dataflow/ALL/ALL/ALL
        /// </summary>
        /// <param name="queryString">
        ///     The rest query string.
        /// </param>
        /// <param name="queryParameters">
        ///     The parameters.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="queryString"/> is <see langword="null" />.</exception>
        public RESTStructureQueryCoreV2(string queryString, IDictionary<string, string> queryParameters)
            : base(queryString, queryParameters)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RESTStructureQueryCore" /> class.
        /// </summary>
        /// <param name="queryString">
        ///     The query string.
        /// </param>
        /// <param name="queryParameters">
        ///     The query parameters.
        /// </param>
        public RESTStructureQueryCoreV2(string[] queryString, IDictionary<string, string> queryParameters)
            : base(queryString, queryParameters)
        {
        }

        /// <summary>
        ///     Gets the structure reference.
        /// </summary>
        public override IStructureReference StructureReference
        {
            get
            {
                return this._structureReferences.FirstOrDefault();
            }
        }

        /// <summary>
        ///     Gets the structure references.
        /// </summary>
        public override ISet<IStructureReference> StructureReferences
        {
            get
            {
                return this._structureReferences;
            }
        }

        /// <summary>
        ///     The to string.
        /// </summary>
        /// <returns>
        ///     A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var structureReference in this._structureReferences)
            {
                string line = "ref: " + structureReference + "-detail:" + StructureQueryMetadata.StructureQueryDetail
                   + "-references:" + structureReference + "-specific"
                   + StructureQueryMetadata.SpecificStructureReference + "latest"
                   + StructureQueryMetadata.IsReturnLatest + "stable"
                   + StructureQueryMetadata.IsReturnStable;
                sb.AppendLine(line);
            }
            return sb.ToString();
        }

        /// <summary>
        /// Format the query arguments.
        /// </summary>
        /// <param name="query">The query.</param>
        private void FormatQuery(string[] query)
        {
            for (int i = 0; i < query.Length; i++)
            {
                var param = query[i];
                if (!ObjectUtil.ValidString(param))
                {
                    query[i] = null;
                }
            }

            // for version parameter
            if (query.Length >= 4)
            {
                if (query[3].Equals("~"))
                {
                    query[3] = _latestLiteral;
                }
                else if (query[3].Equals("+"))
                {
                    query[3] = _latestStableLiteral;
                }
            }
        }

        /// <summary>
        ///     The parser query string.
        /// </summary>
        /// <param name="queryString">
        ///     The query string.
        /// </param>
        protected override void ParserQueryString(string[] queryString)
        {
            this.FormatQuery(queryString);

            if (queryString.Length < 1)
            {
                throw new SdmxSemmanticException("Structure Query Expecting at least 1 parameter (structure type)");
            }

            structureTypeHolder = GetStructureType(queryString[0]);
            string[] agencyIds = null;
            string[] ids = null;
            string[] versions = null;
            if (queryString.Length >= 2)
            {
                agencyIds = queryString[1].Split(',');
            }

            if (queryString.Length >= 3)
            {
                ids = queryString[2].Split(',');
            }

            if (queryString.Length >= 4)
            {
                versions = queryString[3].Split(',');
            }

            if (queryString.Length >= 5 && !string.IsNullOrWhiteSpace(queryString[4]))
            {
                foreach (var filterValue in queryString[4].Split(','))
                {
                    specificItemsHolder.Add(filterValue);
                }
            }

            foreach (string agencyId in agencyIds)
            {
                foreach (string id in ids)
                {
                    foreach (string version in versions)
                    {
                        string versionVal = version;
                        if (version.Equals(_latestLiteral, StringComparison.OrdinalIgnoreCase) ||
                            version.Equals(_latestStableLiteral, StringComparison.OrdinalIgnoreCase))
                        {
                            versionVal = null;
                        }
                        this._structureReferences.Add(new StructureReferenceImpl(agencyId, id, versionVal, structureTypeHolder));
                    }
                }
            }
        }
    }
}