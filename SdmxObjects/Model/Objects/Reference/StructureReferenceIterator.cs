// -----------------------------------------------------------------------
// <copyright file="StructureReferenceIterator.cs" company="EUROSTAT">
//   Date Created : 2021-08-11
//   Copyright (c) 2018 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference
{
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;

    /// <summary>
    /// An iterator over the <see cref="IStructureReference"/> of a query type.
    /// </summary>
    public class StructureReferenceIteratorCore : IStructureReferenceIterator
    {
        private int _index;
        private readonly IList<IStructureReference> _array;

        /// <summary>
        /// The current <see cref="IStructureReference"/>.
        /// </summary>
        public IStructureReference Current { get; private set; }

        /// <inheritdoc />
        public int Count => _array.Count;

        /// <summary>
        /// Initializes a new instance of the class <see cref="StructureReferenceIteratorCore"/>.
        /// </summary>
        /// <param name="structureReferences"></param>
        public StructureReferenceIteratorCore(IList<IStructureReference> structureReferences)
        {
            if (structureReferences == null || structureReferences.Count == 0)
            {
                throw new SdmxSemmanticException("At least one structure reference must be specified.");
            }
            _array = structureReferences;
            Reset();
        }

        /// <summary>
        /// Moves to the next <see cref="IStructureReference"/> in line.
        /// The first call will return the first element.
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            if (_index >= _array.Count)
            {
                return false;
            }
            else
            {
                Current = _array[_index++];
                return true;
            }
        }

        /// <summary>
        /// Resets to start from beginning
        /// </summary>
        public void Reset()
        {
            _index = 0;
            Current = _array[_index];
        }
    }
}
