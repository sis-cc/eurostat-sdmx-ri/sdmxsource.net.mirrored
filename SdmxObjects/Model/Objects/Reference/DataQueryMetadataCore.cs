// -----------------------------------------------------------------------
// <copyright file="DataQueryMetadataCore.cs" company="EUROSTAT">
//   Date Created : 2021-08-27
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference
{
    /// <summary>
    /// Holds the query metadata for the data requests.
    /// </summary>
    public class DataQueryMetadataCore : AvailabilityQueryMetadataCore
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataQueryMetadataCore"/> class, by parsing the query string
        /// </summary>
        /// <param name="queryString">The query string to parse.</param>
        public DataQueryMetadataCore(string[] queryString)
            : base(queryString)
        {
        }
    }
}
