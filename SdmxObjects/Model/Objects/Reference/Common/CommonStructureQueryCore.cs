// -----------------------------------------------------------------------
// <copyright file="CommonStructureQueryCore.cs" company="EUROSTAT">
//   Date Created : 2022-05-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Sdmx.Util.Sdmx;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// Contains all properties needed for a structure query.
    /// Use this instead of <see cref="IRestStructureQuery"/> and <see cref="IComplexStructureQuery"/> interface.
    /// </summary>
    public class CommonStructureQueryCore : ICommonStructureQuery
    {
        private readonly bool _hasSpecificMaintainableId;
        private readonly IComplexTextReference _complexAgencyId;
        private readonly IComplexTextReference _complexMaintainableId;
        private readonly ComplexStructureQueryDetail _requestedDetail;
        private readonly ComplexMaintainableQueryDetail _referencedDetail;
        private readonly IComplexAnnotationReference _annotationReference;
        private readonly IList<IComplexIdentifiableReferenceObject> _childReferences;
        private readonly IList<IVersionRequest> _versionRequests;
        private readonly IList<string> _maintainableIds;
        private readonly IList<string> _agencyIds;
        private readonly IList<SdmxStructureType> _specificReferences;
        private readonly StructureReferenceDetail _references;
        private readonly StructureOutputFormat _requestedFormat;
        private readonly CommonStructureQueryType _requestSource;
        private readonly SdmxStructureType _maintainableTarget;
        private readonly bool _hasSpecificAgencyId;
        private readonly CascadeSelection _itemCascade;

        #region Interface implementation

        /// <inheritdoc/>
        public StructureOutputFormat StructureOutputFormat => this._requestedFormat;

        /// <inheritdoc/>
        public CommonStructureQueryType QueryType => this._requestSource;

        /// <inheritdoc/>
        public StructureReferenceDetail References => this._references;

        /// <inheritdoc/>
        public IList<SdmxStructureType> SpecificReferences => this._specificReferences;

        /// <inheritdoc/>
        public bool HasSpecificAgencyId => this._hasSpecificAgencyId;

        /// <inheritdoc/>
        public bool HasSpecificMaintainableId => this._hasSpecificMaintainableId;

        /// <inheritdoc/>
        public IList<string> AgencyIds => new ReadOnlyCollection<string>(this._agencyIds);

        /// <inheritdoc/>
        public IList<string> MaintainableIds => new ReadOnlyCollection<string>(_maintainableIds);

        /// <inheritdoc/>
        public IList<IVersionRequest> VersionRequests => new ReadOnlyCollection<IVersionRequest>(this._versionRequests);

        /// <inheritdoc/>
        public ComplexStructureQueryDetail RequestedDetail => this._requestedDetail;

        /// <inheritdoc/>
        public IComplexAnnotationReference AnnotationReference => this._annotationReference;

        /// <inheritdoc/>
        public IList<IComplexIdentifiableReferenceObject> SpecificItems => this._childReferences;

        /// <inheritdoc/>
        public SdmxStructureType MaintainableTarget => this._maintainableTarget;

        /// <inheritdoc/>
        public ComplexMaintainableQueryDetail ReferencedDetail => this._referencedDetail;

        /// <inheritdoc/>
        public CascadeSelection ItemCascade => this._itemCascade;

        #endregion Interface implementation

        #region constructors

        /// <summary>
        /// Initialize a new instance from a proxy object
        /// </summary>
        /// <param name="proxy"></param>
        public CommonStructureQueryCore(ICommonStructureQuery proxy)
        {
            this._hasSpecificMaintainableId = proxy.HasSpecificMaintainableId;
            this._requestedDetail = proxy.RequestedDetail;
            this._referencedDetail = proxy.ReferencedDetail;
            this._annotationReference = proxy.AnnotationReference;
            this._childReferences = proxy.SpecificItems;
            this._versionRequests = proxy.VersionRequests;
            this._maintainableIds = proxy.MaintainableIds;
            this._agencyIds = proxy.AgencyIds;
            this._specificReferences = proxy.SpecificReferences;
            this._references = proxy.References;
            this._requestedFormat = proxy.StructureOutputFormat;
            this._requestSource = proxy.QueryType;
            this._maintainableTarget = proxy.MaintainableTarget;
            this._hasSpecificAgencyId = proxy.HasSpecificAgencyId;
            this._itemCascade = proxy.ItemCascade;
        }

        private CommonStructureQueryCore(Builder builder)
        {
            this._hasSpecificMaintainableId = builder._hasSpecificMaintainableId;
            this._complexAgencyId = builder._complexAgencyId;
            this._complexMaintainableId = builder._complexMaintainableId;
            this._requestedDetail = builder._requestedDetail;
            this._referencedDetail = builder._referencedDetail;
            this._annotationReference = builder._annotationReference;
            this._childReferences = builder._childReferences;
            this._versionRequests = builder._versionRequests;
            this._maintainableIds = builder._maintainableIds;
            this._agencyIds = builder._agencyIds;
            this._specificReferences = builder._specificReferences;
            this._references = builder._references;
            this._requestedFormat = builder._requestedFormat;
            this._requestSource = builder._requestSource;
            this._maintainableTarget = builder._maintainableTarget;
            this._hasSpecificAgencyId = builder._hasSpecificAgencyId;
            this._itemCascade = builder._itemCascade;
        }

        #endregion 

        /// <summary>
        /// A builder to help initialize a <see cref="CommonStructureQueryCore"/>.
        /// </summary>
        public class Builder
        {
            internal bool _hasSpecificMaintainableId;
            internal IComplexTextReference _complexAgencyId;
            internal IComplexTextReference _complexMaintainableId;
            internal ComplexStructureQueryDetail _requestedDetail;
            internal ComplexMaintainableQueryDetail _referencedDetail;
            internal IComplexAnnotationReference _annotationReference;
            internal IList<IComplexIdentifiableReferenceObject> _childReferences = new List<IComplexIdentifiableReferenceObject>();
            internal IList<IVersionRequest> _versionRequests = new List<IVersionRequest>();
            internal IList<string> _maintainableIds = new List<string>();
            internal IList<string> _agencyIds = new List<string>();
            internal IList<SdmxStructureType> _specificReferences = new List<SdmxStructureType>();
            internal StructureReferenceDetail _references;
            internal StructureOutputFormat _requestedFormat;
            internal CommonStructureQueryType _requestSource;
            internal SdmxStructureType _maintainableTarget;
            internal bool _hasSpecificAgencyId;
            internal CascadeSelection _itemCascade = CascadeSelection.False;

            private Builder(CommonStructureQueryType requestSource, StructureOutputFormat requestedFormat)
            {
                this._requestSource = requestSource;
                this._requestedFormat = requestedFormat;
            }

            /// <summary>
            /// Start building a new structure query.
            /// </summary>
            /// <param name="requestSource">i.e. REST or SOAP</param>
            /// <param name="requestedFormat">i.e. json or xml.</param>
            /// <returns>A new instance of the <see cref="Builder"/> class.</returns>
            public static Builder NewQuery(CommonStructureQueryType requestSource, StructureOutputFormat requestedFormat)
            {
                return new Builder(requestSource, requestedFormat);
            }

            /// <summary>
            /// Start building a new structure query.
            /// </summary>
            /// <param name="requestSource">i.e. REST or SOAP</param>
            /// <param name="requestedFormat">i.e. json or xml.</param>
            /// <returns>A new instance of the <see cref="Builder"/> class.</returns>
            public static Builder NewQuery(CommonStructureQueryType requestSource, StructureOutputFormatEnumType requestedFormat)
            {
                return new Builder(requestSource, StructureOutputFormat.GetFromEnum(requestedFormat));
            }

            /// <summary>
            /// Sets the common identification info for a structure, using a <see cref="IStructureReference"/>.
            /// i.e.: structure type, id, agency, version
            /// </summary>
            /// <param name="reference">The structure reference</param>
            /// <returns>A new instance of the <see cref="Builder"/> class.</returns>
            public Builder SetStructureIdentification(IStructureReference reference)
            {
                if (reference == null)
                {
                    throw new ArgumentNullException(nameof(reference));
                }

                this.SetMaintainableTarget(reference.MaintainableStructureEnumType.EnumType);
                if (reference.HasAgencyId())
                {
                    this.SetAgencyIds(reference.AgencyId.Split(','));
                }
                if (reference.HasMaintainableId())
                {
                    this.SetMaintainableIds(reference.MaintainableId.Split(','));
                }
                if (reference.HasVersion())
                {
                    this.SetVersionRequests(reference.Version.Split(',').Select(v => new VersionRequestCore(v)).ToArray());
                }

                return this;
            }

            /// <summary>
            /// Sets the common identification info for a structure from a <see cref="IMaintainableMutableObject"/>.
            /// i.e.: structure type, id, agency, version
            /// </summary>
            /// <param name="mutableObject">The structure mutableObject</param>
            /// <returns>A new instance of the <see cref="Builder"/> class.</returns>
            public Builder SetStructureIdentification(IMaintainableMutableObject mutableObject)
            {
                if (mutableObject == null)
                {
                    throw new ArgumentNullException(nameof(mutableObject));
                }

                this.SetMaintainableTarget(mutableObject.StructureType);
                if (ObjectUtil.ValidString(mutableObject.AgencyId))
                {
                    this.SetAgencyIds(mutableObject.AgencyId.Split(','));
                }
                if (ObjectUtil.ValidString(mutableObject.Id))
                {
                    this.SetMaintainableIds(mutableObject.Id.Split(','));
                }
                if (ObjectUtil.ValidString(mutableObject.Version))
                {
                    this.SetVersionRequests(mutableObject.Version.Split(',').Select(v => new VersionRequestCore(v)).ToArray());
                }

                return this;
            }

            /// <summary>
            /// Sets the <see cref="IComplexTextReference"/> for the agency Id
            /// </summary>
            /// <param name="complexAgencyId"></param>
            /// <returns></returns>
            public Builder SetComplexAgencyId(IComplexTextReference complexAgencyId)
            {
                this._complexAgencyId = complexAgencyId;
                return this;
            }

            /// <summary>
            /// Sets the <see cref="IComplexTextReference"/> for the maintainable Id
            /// </summary>
            /// <param name="complexMaintainableId"></param>
            /// <returns></returns>
            public Builder SetComplexMaintainableId(IComplexTextReference complexMaintainableId)
            {
                this._complexMaintainableId = complexMaintainableId;
                return this;
            }

            /// <summary>
            /// Sets the requested detaill level.
            /// </summary>
            /// <param name="requestedDetail"></param>
            /// <returns></returns>
            /// <exception cref="ArgumentNullException"></exception>
            public Builder SetRequestedDetail(ComplexStructureQueryDetail requestedDetail)
            {
                if (requestedDetail == null)
                {
                    throw new ArgumentNullException(nameof(requestedDetail));
                }
                this._requestedDetail = requestedDetail;
                return this;
            }

            /// <summary>
            /// Sets the requested detail level, using the enum type.
            /// </summary>
            /// <param name="requestedDetail"></param>
            /// <returns></returns>
            public Builder SetRequestedDetail(ComplexStructureQueryDetailEnumType requestedDetail)
            {
                this._requestedDetail = ComplexStructureQueryDetail.GetFromEnum(requestedDetail);
                return this;
            }

            /// <summary>
            /// Sets the referenced detail level.
            /// </summary>
            /// <param name="referencedDetail"></param>
            /// <returns></returns>
            /// <exception cref="ArgumentNullException"></exception>
            public Builder SetReferencedDetail(ComplexMaintainableQueryDetail referencedDetail)
            {
                if (referencedDetail == null)
                {
                    throw new ArgumentNullException(nameof(referencedDetail));
                }
                this._referencedDetail = referencedDetail;
                return this;
            }

            /// <summary>
            /// Sets the referenced detail level, using the enum type
            /// </summary>
            /// <param name="referencedDetail"></param>
            /// <returns></returns>
            public Builder SetReferencedDetail(ComplexMaintainableQueryDetailEnumType referencedDetail)
            {
                this._referencedDetail = ComplexMaintainableQueryDetail.GetFromEnum(referencedDetail);
                return this;
            }

            /// <summary>
            /// Sets the annotation reference
            /// </summary>
            /// <param name="annotationReference"></param>
            /// <returns></returns>
            public Builder SetAnnotationReference(IComplexAnnotationReference annotationReference)
            {
                this._annotationReference = annotationReference;
                return this;
            }

            /// <summary>
            /// Sets the child reference
            /// </summary>
            /// <param name="childReferences"></param>
            /// <returns></returns>
            public Builder SetChildReferences(params IComplexIdentifiableReferenceObject[] childReferences)
            {
                this._childReferences = childReferences;
                return this;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="versionRequests"></param>
            /// <returns></returns>
            /// <exception cref="ArgumentNullException"></exception>
            /// <exception cref="ArgumentException"></exception>
            public Builder SetVersionRequests(params IVersionRequest[] versionRequests)
            {
                if (versionRequests == null)
                {
                    throw new ArgumentNullException(nameof(versionRequests));
                }
                if (!versionRequests.Any())
                {
                    throw new ArgumentException("empty list not allowed");
                }
                this._versionRequests = versionRequests;
                return this;
            }

            /// <summary>
            /// Sets the maintainable ids
            /// </summary>
            /// <param name="maintainableIds"></param>
            /// <returns></returns>
            /// <exception cref="ArgumentNullException"></exception>
            /// <exception cref="ArgumentException"></exception>
            public Builder SetMaintainableIds(params string[] maintainableIds)
            {
                if (maintainableIds == null)
                {
                    throw new ArgumentNullException(nameof(maintainableIds));
                }
                if (!maintainableIds.Any())
                {
                    throw new ArgumentException("empty list not allowed");
                }
                this._maintainableIds = maintainableIds;
                return this;
            }

            /// <summary>
            /// Sets the agency Ids
            /// </summary>
            /// <param name="agencyIds"></param>
            /// <returns></returns>
            /// <exception cref="ArgumentNullException"></exception>
            /// <exception cref="ArgumentException"></exception>
            public Builder SetAgencyIds(params string[] agencyIds)
            {
                if (agencyIds == null)
                {
                    throw new ArgumentNullException(nameof(agencyIds));
                }
                if (!agencyIds.Any())
                {
                    throw new ArgumentException("empty list not allowed");
                }
                this._agencyIds = agencyIds;
                return this;
            }

            /// <summary>
            /// Sets the specific references structure types
            /// </summary>
            /// <param name="specificReferences"></param>
            /// <returns></returns>
            /// <exception cref="ArgumentNullException"></exception>
            /// <exception cref="ArgumentException"></exception>
            public Builder SetSpecificReferences(params SdmxStructureType[] specificReferences)
            {
                if (specificReferences == null)
                {
                    throw new ArgumentNullException(nameof(specificReferences));
                }
                if (!specificReferences.Any())
                {
                    throw new ArgumentException("empty list not allowed");
                }
                this._specificReferences = specificReferences;
                return this;
            }

            /// <summary>
            /// Sets the specific references structure types, using the enum type
            /// </summary>
            /// <param name="specificReferences"></param>
            /// <returns></returns>
            /// <exception cref="ArgumentNullException"></exception>
            /// <exception cref="ArgumentException"></exception>
            public Builder SetSpecificReferences(params SdmxStructureEnumType[] specificReferences)
            {
                if (specificReferences == null)
                {
                    throw new ArgumentNullException(nameof(specificReferences));
                }
                if (!specificReferences.Any())
                {
                    throw new ArgumentException("empty list not allowed");
                }
                this._specificReferences = specificReferences.Select(r => SdmxStructureType.GetFromEnum(r)).ToList();
                return this;
            }

            /// <summary>
            /// Sets the reference detail level
            /// </summary>
            /// <param name="references"></param>
            /// <returns></returns>
            /// <exception cref="ArgumentNullException"></exception>
            public Builder SetReferences(StructureReferenceDetail references)
            {
                if (references == null)
                {
                    throw new ArgumentNullException(nameof(references));
                }
                this._references = references;
                return this;
            }

            /// <summary>
            /// Sets the reference detail level, using the enum type.
            /// </summary>
            /// <param name="references"></param>
            /// <returns></returns>
            public Builder SetReferences(StructureReferenceDetailEnumType references)
            {
                this._references = StructureReferenceDetail.GetFromEnum(references);
                return this;
            }

            /// <summary>
            /// Sets the structure type of the target maintainable
            /// </summary>
            /// <param name="maintainableTarget"></param>
            /// <returns></returns>
            /// <exception cref="ArgumentNullException"></exception>
            public Builder SetMaintainableTarget(SdmxStructureType maintainableTarget)
            {
                if (maintainableTarget == null)
                {
                    throw new ArgumentNullException(nameof(maintainableTarget));
                }
                this._maintainableTarget = maintainableTarget;
                return this;
            }

            /// <summary>
            /// Sets the structure type of the target maintainable, using the enum type
            /// </summary>
            /// <param name="maintainableTarget"></param>
            /// <returns></returns>
            public Builder SetMaintainableTarget(SdmxStructureEnumType maintainableTarget)
            {
                this._maintainableTarget = SdmxStructureType.GetFromEnum(maintainableTarget);
                return this;
            }

            /// <summary>
            /// Sets the cascade child values/codes action
            /// </summary>
            /// <param name="itemCascade"></param>
            /// <returns></returns>
            public Builder SetItemCascade(CascadeSelection itemCascade)
            {
                this._itemCascade = itemCascade;
                return this;
            }

            /// <summary>
            /// Called as the last method to build the structure query object.
            /// </summary>
            /// <returns>The structure query.</returns>
            public ICommonStructureQuery Build()
            {
                // has specific must be calculated always regardless of the common structure query type
                // regardless also of whether it is built using complex or plain objects.
                this._hasSpecificAgencyId = 
                    !(!_agencyIds.Any() || _agencyIds[0].Equals("*")) ||
                    !(_complexAgencyId == null || _complexAgencyId.SearchParameter == null || _complexAgencyId.SearchParameter.Equals("*"));
                this._hasSpecificMaintainableId = 
                    !(!_maintainableIds.Any() || _maintainableIds[0].Equals("*")) ||
                    !(_complexMaintainableId == null || _complexMaintainableId.SearchParameter == null || _complexMaintainableId.SearchParameter.Equals("*"));

                // apply defaults based on the standard
                if (this._referencedDetail == null) 
                {
                    this._referencedDetail = ComplexMaintainableQueryDetail.GetFromEnum(ComplexMaintainableQueryDetailEnumType.Full);
                }

                if (this._requestedDetail == null) 
                {
                    this._requestedDetail = ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full);
                }

                if (this._references == null)
                {
                    this._references = StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.None);
                }


                ICommonStructureQuery commonStructureQuery = new CommonStructureQueryCore(this);

                return commonStructureQuery;
            }
        }
    }
}
