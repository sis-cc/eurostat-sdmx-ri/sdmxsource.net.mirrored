// -----------------------------------------------------------------------
// <copyright file="VersionRequestCore.cs" company="EUROSTAT">
//   Date Created : 2022-05-19
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common
{
    using System;
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using System.Linq;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using System.Text.RegularExpressions;

    /// <summary>
    /// The request for the version(s) of sdmx maintainable(s). 
    /// </summary>
    public class VersionRequestCore : IVersionRequest
    {
        private static readonly string _latestStable = "+";
        private static readonly string _latestUnStable = "+";
        /// <summary>
        /// No idea why this should be public.
        /// </summary>
        public static readonly IList<VersionPosition> PositionValues = Enum.GetValues(typeof(VersionPosition)).Cast<VersionPosition>().ToList().AsReadOnly();
        
        private readonly VersionQueryTypeEnum _versionQueryType;
        private readonly ITimeRange _validFrom;
        private readonly ITimeRange _validTo;
        private readonly string _extension;
        private readonly IVersionWildcard _wildcard;
        private readonly Dictionary<VersionPosition, int?> _versionParticles = new Dictionary<VersionPosition, int?>();
        private readonly string _versionString;

        private static readonly IVersionRequest _latestInstance = new VersionRequestCore("~");
        private static readonly IVersionRequest _latestStableInstance = new VersionRequestCore("+");
        private static readonly IVersionRequest _allInstance = new VersionRequestCore("*");

        #region Constructors

        /// <summary>
        /// Creates a new instance of the class, from a version literal.
        /// </summary>
        /// <param name="version">The version literal to create the instance from.</param>
        /// <exception cref="SdmxSemmanticException">Only versions with 2 or 3 parts are supported.</exception>
        public VersionRequestCore(string version)
        {
            // WARNING SOAP 2.1 requests default to 1.0
            // but REST to latest version
            // As a result the code here assumes that SOAP requests
            // without version are build with "1.0" in SdmxSource
            if (!ObjectUtil.ValidString(version) || "latest".Equals(version, StringComparison.OrdinalIgnoreCase) || "~".Equals(version))
            {
                _versionQueryType = VersionQueryTypeEnum.Latest;
            }
            else if ("*".Equals(version) || "all".Equals(version, StringComparison.OrdinalIgnoreCase))
            {
                _versionQueryType = VersionQueryTypeEnum.All;
            }
            else if ("*+".Equals(version))
            {
                _versionQueryType = VersionQueryTypeEnum.AllStable;
            }
            else if ("+".Equals(version))
            {
                _versionQueryType = VersionQueryTypeEnum.LatestStable;
            }
            else
            {
                string[] versionSplitByExt = version.Split(new char[] { '-' }, 2);
                string versionPart = versionSplitByExt[0];
                if (versionSplitByExt.Length > 1)
                {
                    _extension = versionSplitByExt[1];
                }
                string[] particles = versionPart.Split('.');
                if (particles.Length > 3)
                {
                    throw new SdmxSemmanticException("Only versions with 2 or 3 parts are supported.");
                }
                Wildcard wildCardInt = GetWildcard(particles);

                _versionQueryType = GetVersionQueryType(wildCardInt);
                if (wildCardInt.wildCardPosition > -1 && wildCardInt.wildCardPosition < PositionValues.Count)
                {
                    this._wildcard = new VersionWildcardCore(wildCardInt.wildCardType.Value, PositionValues[wildCardInt.wildCardPosition]);
                }

                PopulateVersionParticles(particles);
            }
            this._validFrom = null;
            this._validTo = null;

            this._versionString = version;
        }

        /// <summary>
        /// Creates a new instance of the class, from a complex version.
        /// </summary>
        /// <param name="versionReference">The <see cref="IComplexVersionReference"/> to create the instance from.</param>
        /// <exception cref="SdmxSemmanticException">Only versions with 2 or 3 parts are supported.</exception>
        public VersionRequestCore(IComplexVersionReference versionReference)
        {
            if ((versionReference.IsReturnLatest != null && versionReference.IsReturnLatest.IsTrue) || "*".Equals(versionReference.Version))
            {
                _versionQueryType = VersionQueryTypeEnum.Latest;
            }
            else
            {
                _versionQueryType = VersionQueryTypeEnum.All;
                if (ObjectUtil.ValidString(versionReference.Version))
                {
                    string[] particles = versionReference.Version.Split('.');
                    if (particles.Length > 3)
                    {
                        throw new SdmxSemmanticException("Only versions with 2 or 3 parts are supported.");
                    }
                    PopulateVersionParticles(particles);
                }
            }

            this._validFrom = versionReference.VersionValidFrom;
            this._validTo = versionReference.VersionValidTo;

            this._versionString = versionReference.Version;
        }

        #endregion

        public static IList<IVersionRequest> FromOldApi(string version, bool returnLatest, bool returnStable) 
        {
            if (!string.IsNullOrEmpty(version)) 
            {
                if (returnLatest) 
                {
                    var latestChar = returnStable ? _latestStable : _latestUnStable;
                    return version.Split(',').Select(v => new VersionRequestCore(v + latestChar)).ToArray();
                }
                
                return version.Split(',').Select(v => new VersionRequestCore(v)).ToArray();
            }
            else if (returnLatest) {
                if (returnStable)
                {
                    return new [] { _latestStableInstance};
                }

                return new [] { _latestInstance};
            }

            return new [] {_allInstance};
        }

        /// <summary>
        /// </summary>
        /// <param name="version"> the version string in a reference</param>
        /// <param name="isReferenceByFinal"> isReferencedByFinal weather the parent has stable version</param>
        /// <returns>equivalent REST version request</returns>
        public static IVersionRequest FromSdmxReference(string version, bool isReferenceByFinal)
        {
            return isReferenceByFinal ? 
                new VersionRequestCore(version) : 
                new VersionRequestCore(version.Replace('+', '~')); // in case of wildcard reference inside a non-final artefact, the LATEST artefact should be returned
        }

        #region private methods

        private class Wildcard
        {
            public int wildCardPosition = -1;
            public VersionQueryTypeEnum? wildCardType = null;
        }

        private Wildcard GetWildcard(string[] particles)
        {
            Wildcard wildCardInt = new Wildcard();

            // TODO: these "EndsWith" should be replaced by more strict Regex
            // to allow only valid usage for the wildcards. E.g. not +~
            for (int i = 0; i < particles.Length; i++)
            {
                string particle = particles[i];
                if (particle.EndsWith("~"))
                {
                    // latest version
                    wildCardInt.wildCardPosition = i;
                    wildCardInt.wildCardType = VersionQueryTypeEnum.Latest;
                    return wildCardInt;
                }
                if (particle.EndsWith("*+"))
                {
                    // all stable version
                    wildCardInt.wildCardPosition = i;
                    wildCardInt.wildCardType = VersionQueryTypeEnum.AllStable;
                    return wildCardInt;
                }
                if (particle.EndsWith("+"))
                {
                    // latest stable version
                    wildCardInt.wildCardPosition = i;
                    wildCardInt.wildCardType = VersionQueryTypeEnum.LatestStable;
                    return wildCardInt;

                }
                if (particle.EndsWith("*"))
                {
                    // all version
                    wildCardInt.wildCardPosition = i;
                    wildCardInt.wildCardType = VersionQueryTypeEnum.All;
                    return wildCardInt;
                }
            }
            return wildCardInt;
        }

        private VersionQueryTypeEnum GetVersionQueryType(Wildcard wildCardInt)
        {
            return wildCardInt.wildCardType ?? VersionQueryTypeEnum.All;
        }

        private void PopulateVersionParticles(string[] particles)
        {
            // TODO: _versionParticles should have all 3 keys regardless of how many particles the version request provides.
            for (int i = 0; i < particles.Length; i++)
            {
                _versionParticles.Add(PositionValues[i], VersionAsInt(particles[i]));
            }
        }

        private int? VersionAsInt(string particle)
        {
            Regex wildCardRegex = new Regex(@"[+~*]");
            if (int.TryParse(wildCardRegex.Replace(particle, ""), out int result))
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        #endregion

        #region Interface implementation

        /// <inheritdoc/>
        public bool SpecifiesVersion => Major != null;

        /// <inheritdoc/>
        public VersionQueryTypeEnum VersionQueryType => _versionQueryType;

        /// <inheritdoc/>
        public int? Major => VersionPart(VersionPosition.Major);

        /// <inheritdoc/>
        public int? Minor => VersionPart(VersionPosition.Minor);

        /// <inheritdoc/>
        public int? Patch => VersionPart(VersionPosition.Patch);

        /// <inheritdoc/>
        public string Extension => _extension;

        /// <inheritdoc/>
        public int? VersionPart(VersionPosition position)
        {
            if (_versionParticles.ContainsKey(position))
            {
                return _versionParticles[position];
            }
            return null;
        }

        /// <inheritdoc/>
        public IVersionWildcard WildCard => _wildcard;

        #endregion

        #region Other public methods & properties

        /// <summary>
        /// Get start of requested validity period; if requested else <c>null</c>
        /// </summary>
        public ITimeRange ValidFrom => _validFrom;

        /// <summary>
        /// Get end of requested validity period; if requested else <c>null</c>
        /// </summary>
        public ITimeRange ValidTo => _validTo;

        /// <summary>
        /// Get the string representation of the request.
        /// </summary>
        /// <returns>The string used in the original request.</returns>
        public override string ToString()
        {
            return _versionString;
        }
        /// <inheritdoc/>

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is VersionRequestCore))
            {
                return false;
            }

            var other = obj as VersionRequestCore;
            return Equals(this.Major, other.Major)&&
                   Equals(this.Minor, other.Minor)&&
                   Equals(this.Patch, other.Patch)&&
                   Equals(this.Extension, other.Extension)&&
                   Equals(this.WildCard, other.WildCard);
        }
        /// <inheritdoc/>

        public override int GetHashCode()
        {
            int hash = 397; // Prime number to start with

            // Combine hash codes of properties
            hash = hash * 397 + Major.GetHashCode();
            hash = hash * 397 + (Minor != null ? Minor.GetHashCode() : 0);
            hash = hash * 397 + (Patch != null ? Patch.GetHashCode() : 0);
            hash = hash * 397 + (Extension != null ? Extension.GetHashCode() : 0);
            hash = hash * 397 + (WildCard != null ? WildCard.GetHashCode() : 0);

            return hash;
        }
        #endregion
    }
}
