// -----------------------------------------------------------------------
// <copyright file="Soap20CommonStructureQueryCore.cs" company="EUROSTAT">
//   Date Created : 2022-05-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common
{
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;

    /// <summary>
    /// Contains all properties needed for a Soap 2.0 structure query.
    /// </summary>
    public class Soap20CommonStructureQueryCore : ISoap20CommonStructureQuery
    {
        private readonly IList<IStructureReference> _simpleStructureQueries;

        private readonly bool _resolveReferences;

        private readonly bool _returnStub;

        /// <summary>
        /// Create a new instance of a SOAP 2.0 structure query.
        /// </summary>
        /// <param name="simpleStructureQueries"></param>
        /// <param name="resolveReferences"></param>
        /// <param name="returnStub"></param>
        public Soap20CommonStructureQueryCore(IList<IStructureReference> simpleStructureQueries, bool resolveReferences, bool returnStub)
        {
            this._simpleStructureQueries = simpleStructureQueries;
            this._resolveReferences = resolveReferences;
            this._returnStub = returnStub;
        }

        /// <inheritdoc/>
        public IList<IStructureReference> SimpleStructureQueries => this._simpleStructureQueries;

        /// <inheritdoc/>
        public bool ResolveReferences => this._resolveReferences;

        /// <inheritdoc/>
        public bool ReturnStub => this._returnStub;
    }
}
