// -----------------------------------------------------------------------
// <copyright file="VersionWildcardCore.cs" company="EUROSTAT">
//   Date Created : 2022-05-19
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common
{
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;

    /// <summary>
    /// Implements the <see cref="IVersionWildcard"/>
    /// </summary>
    public class VersionWildcardCore : IVersionWildcard
    {
        private readonly VersionQueryTypeEnum _wildcardType;
        private readonly VersionPosition _versionPosition;

        /// <summary>
        /// Creates a new instance of the class <see cref="VersionWildcardCore"/>.
        /// </summary>
        /// <param name="wildcardType">The type (meaning) of wildcard.</param>
        /// <param name="versionPosition">The position of the wildcard in the semantic version.</param>
        public VersionWildcardCore(VersionQueryTypeEnum wildcardType, VersionPosition versionPosition)
        {
            this._wildcardType = wildcardType;
            this._versionPosition = versionPosition;
        }

        /// <inheritdoc/>
        public VersionQueryTypeEnum WildcardType => _wildcardType;

        /// <inheritdoc/>
        public VersionPosition WildcardPosition => _versionPosition;


        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is VersionWildcardCore))
            {
                return false;
            }

            var other = obj as VersionWildcardCore;
            return Equals(this.WildcardType, other.WildcardType) &&
                   Equals(this.WildcardPosition, other.WildcardPosition);
        }

        public override int GetHashCode()
        {
            int hash = 397; // Prime number to start with

            // Combine hash codes of properties
            hash = hash * 397 + WildcardType.GetHashCode();
            hash = hash * 397 + WildcardPosition.GetHashCode();

            return hash;
        }
    }
}
