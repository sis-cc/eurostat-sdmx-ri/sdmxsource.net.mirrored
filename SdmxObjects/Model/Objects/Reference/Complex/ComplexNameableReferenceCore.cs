﻿// -----------------------------------------------------------------------
// <copyright file="ComplexNameableReferenceCore.cs" company="EUROSTAT">
//   Date Created : 2013-05-31
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex
{
    #region Using directives

    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;

    #endregion

    /// <summary>
    ///     The complex nameable reference.
    /// </summary>
    public class ComplexNameableReferenceCore : IComplexNameableReference
    {
        /// ///
        /// <summary>
        ///     The annotation ref.
        /// </summary>
        private readonly IComplexAnnotationReference _annotationRef;

        /// <summary>
        ///     The description ref.
        /// </summary>
        private readonly IComplexTextReference _descriptionRef;

        /// <summary>
        ///     The name ref.
        /// </summary>
        private readonly IComplexTextReference _nameRef;

        /// <summary>
        ///     The structure type ref.
        /// </summary>
        private readonly SdmxStructureType _structureType;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComplexNameableReferenceCore" /> class.
        /// </summary>
        /// <param name="structureType">
        ///     The structure type.
        /// </param>
        /// <param name="annotationRef">
        ///     The annotation ref.
        /// </param>
        /// <param name="nameRef">
        ///     The name ref.
        /// </param>
        /// <param name="descriptionRef">
        ///     The description ref.
        /// </param>
        public ComplexNameableReferenceCore(
            SdmxStructureType structureType, 
            IComplexAnnotationReference annotationRef, 
            IComplexTextReference nameRef, 
            IComplexTextReference descriptionRef)
        {
            if (structureType == null)
            {
                throw new SdmxSemmanticException("Null structure type provided for reference in query.");
            }

            this._structureType = structureType;
            if (annotationRef != null)
            {
                this._annotationRef = annotationRef;
            }

            if (nameRef != null)
            {
                this._nameRef = nameRef;
            }

            if (descriptionRef != null)
            {
                this._descriptionRef = descriptionRef;
            }
        }

        /// <summary>
        ///     Gets the annotation reference.
        /// </summary>
        public virtual IComplexAnnotationReference AnnotationReference
        {
            get
            {
                return this._annotationRef;
            }
        }

        /// <summary>
        ///     Gets the description reference.
        /// </summary>
        public virtual IComplexTextReference DescriptionReference
        {
            get
            {
                return this._descriptionRef;
            }
        }

        /// <summary>
        ///     Gets the name reference.
        /// </summary>
        public virtual IComplexTextReference NameReference
        {
            get
            {
                return this._nameRef;
            }
        }

        /// <summary>
        ///     Gets the referenced structure type.
        /// </summary>
        public virtual SdmxStructureType ReferencedStructureType
        {
            get
            {
                return this._structureType;
            }
        }
    }
}