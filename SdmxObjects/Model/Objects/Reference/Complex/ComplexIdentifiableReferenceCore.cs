// -----------------------------------------------------------------------
// <copyright file="ComplexIdentifiableReferenceCore.cs" company="EUROSTAT">
//   Date Created : 2013-05-31
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex
{
    #region Using directives

    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;

    #endregion

    /// <summary>
    ///     The complex identifiable reference.
    /// </summary>
    [Serializable]
    public class ComplexIdentifiableReferenceCore : ComplexNameableReferenceCore, IComplexIdentifiableReferenceObject
    {
        /// <summary>
        ///     The child reference.
        /// </summary>
        private readonly IComplexIdentifiableReferenceObject _childReference;

        /// <summary>
        ///     The id.
        /// </summary>
        private readonly IComplexTextReference _id;

        /// <summary>
        ///     The cascade selection.
        /// </summary>
        private readonly CascadeSelection _cascadeSelection;

        ///<summary>
        ///     Build from a REST 1.x or 2.0.0 call
        /// </summary>
        /// <param name="itemPath">The path of an item, separated by dot</param>
        /// <param name="itemType">the type of the item e.g. code, value, category</param>
        /// <returns></returns>
        public static IComplexIdentifiableReferenceObject CreateForRest(string itemPath, SdmxStructureType itemType)
        {
            return new ComplexIdentifiableReferenceCore(itemPath, itemType);
        }

        ///<summary>
        ///     Build from Content Constraint or Codelist extension
        /// </summary>
        /// <param name="value"></param>
        /// <param name="cascadeSelection"></param>
        /// <param name="itemType"></param>
        /// <returns></returns>
        public static IComplexIdentifiableReferenceObject CreateForConstraintOrCodelistExtension(IComplexTextReference value, CascadeSelection cascadeSelection, SdmxStructureType itemType)
        {
            return new ComplexIdentifiableReferenceCore(value, cascadeSelection, itemType);
        }

        ///<summary>
        ///     Build from SOAP 2.1 Item Scheme based Structure query
        /// </summary>
        /// <param name="id">The id criteria</param>
        /// <param name="structureType">the item structure type</param>
        /// <param name="annotationRef">the annotation criteria</param>
        /// <param name="nameRef">the name criteria</param>
        /// <param name="descriptionRef">the description criteria</param>
        /// <param name="childReference">the child references</param>
        /// <returns>a <see cref="IComplexIdentifiableReferenceObject"/> using the SOAP 2.1 info.</returns>
        public static IComplexIdentifiableReferenceObject CreateForSoap21(
                IComplexTextReference id,
                SdmxStructureType structureType,
                IComplexAnnotationReference annotationRef,
                IComplexTextReference nameRef,
                IComplexTextReference descriptionRef,
                IComplexIdentifiableReferenceObject childReference)
        {
            return new ComplexIdentifiableReferenceCore(id, structureType, annotationRef, nameRef, descriptionRef, childReference);
        }

        ///<summary>
        ///     Build from a REST 1.x or 2.0.0 call
        /// </summary>
        /// <param name="itemPath">The path of an item, separated by dot</param>
        /// <param name="itemType"></param>
        private ComplexIdentifiableReferenceCore(string itemPath, SdmxStructureType itemType)
            : base(itemType, null, null, null)
        {
            if (itemPath == null)
            {
                throw new ArgumentNullException(nameof(itemPath));
            }
            string[] split = itemPath.Split(new char[] { '.' }, 2);
            _id = new ComplexTextReferenceCore(null, TextSearch.GetFromEnum(TextSearchEnumType.Equal), split[0]);
            if (split.Length > 1)
            {
                _childReference = new ComplexIdentifiableReferenceCore(split[1], itemType);
            }
            _cascadeSelection = CascadeSelection.False;
        }

        ///<summary>Build from Content Constraint or Codelist extension</summary>
        ///
        /// @param value
        /// @param cascadeSelection

        private ComplexIdentifiableReferenceCore(IComplexTextReference value, CascadeSelection cascadeSelection, SdmxStructureType itemType)
            : base(itemType, null, null, null)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }
            this._id = value;
            this._cascadeSelection = cascadeSelection;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComplexIdentifiableReferenceCore" /> class.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <param name="structureType">
        ///     The structure type.
        /// </param>
        /// <param name="annotationRef">
        ///     The annotation ref.
        /// </param>
        /// <param name="nameRef">
        ///     The name ref.
        /// </param>
        /// <param name="descriptionRef">
        ///     The description ref.
        /// </param>
        /// <param name="childReference">
        ///     The child reference.
        /// </param>
        public ComplexIdentifiableReferenceCore(
            IComplexTextReference id, 
            SdmxStructureType structureType, 
            IComplexAnnotationReference annotationRef, 
            IComplexTextReference nameRef, 
            IComplexTextReference descriptionRef, 
            IComplexIdentifiableReferenceObject childReference)
            : base(structureType, annotationRef, nameRef, descriptionRef)
        {
            this._id = id;
            this._childReference = childReference;
            // in SDMX SOAP 2.1 this is controlled at higher level
            this._cascadeSelection = CascadeSelection.False;
        }

        /// <summary>
        ///     Gets the child reference.
        /// </summary>
        public virtual IComplexIdentifiableReferenceObject ChildReference
        {
            get
            {
                return this._childReference;
            }
        }

        /// <summary>
        ///     Gets the id.
        /// </summary>
        public virtual IComplexTextReference Id
        {
            get
            {
                return this._id;
            }
        }

        /// <summary>
        ///     Gets the cascade selection.
        /// </summary>
        public virtual CascadeSelection CascadeSelection
        {
            get
            {
                return this._cascadeSelection;
            }
        }
    }
}