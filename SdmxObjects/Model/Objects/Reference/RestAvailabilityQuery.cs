// -----------------------------------------------------------------------
// <copyright file="RestAvailabilityQuery.cs" company="EUROSTAT">
//   Date Created : 2021-08-10
//   Copyright (c) 2018 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// Stores an availability rest query based on sdmx rest v2.
    /// protocol://ws-entry-point/availability/{context}/{agencyID}/{resourceID}/{version}/{key}/{componentId}?{c}amp{updatedAfter}amp{references}amp{mode}
    /// </summary>
    public class RestAvailabilityQuery : RestAvailableConstraintQuery, IRestAvailabilityQuery
    {
        #region fields

        private readonly IList<IStructureReference> _structureReferences = new List<IStructureReference>();

        private readonly string[] _versionWildcards = new string[] {"*", "~", "+" };

        /// <summary>
        /// Get the context of the query. Possible values: *, datastructure, dataflow, provisionagreement
        /// </summary>
        private SdmxStructureType _contextHolder;
        
        /// <summary>
        /// The REST resource name
        /// </summary>
        public new const string ResourceName = "availability";

        /// <inheritdoc />
        protected override string QueryResourceName => ResourceName;

        /// <inheritdoc />
        protected override string WildCard => "*";

        /// <summary>
        /// The data query
        /// </summary>
        private readonly IRestDataQueryV2 _dataQuery;

        /// <summary>
        /// The iterator to use for iterating through the <see cref="IStructureReference"/>s of the query.
        /// </summary>
        private readonly IStructureReferenceIterator _structureReferenceIterator;

        #endregion fields

        /// <summary>
        /// Generates a new instance of the class <see cref="RestAvailabilityQuery"/>.
        /// </summary>
        /// <param name="restString">the query string.</param>
        /// <param name="queryParameter">The query parameters.</param>
        public RestAvailabilityQuery(string restString, IDictionary<string, string> queryParameter)
            : base(restString, queryParameter)
        {
            this._dataQuery = DataQuery as IRestDataQueryV2;
            QueryMetadata = new AvailabilityQueryMetadataCore(Path.ToArray());
            _structureReferenceIterator = new StructureReferenceIteratorCore(_structureReferences);
        }

        #region properties

        /// <summary>
        /// Gets the dataflow reference.
        /// If the <see cref="StartFlowRefIterator"/> is run, this property will change its value in every iteration.
        /// </summary>
        public override IStructureReference FlowRef => this._structureReferenceIterator.Current;

        /// <inheritdoc />
        public IObjectQueryMetadata QueryMetadata { get; }

        /// <inheritdoc />
        public IList<ComponentFilter> ComponentFilters => this._dataQuery.ComponentFilters;

        /// <summary>
        /// Get the context of the query. 
        /// Possible values: *, datastructure, dataflow, provisionagreement
        /// </summary>
        public SdmxStructureType Context => _contextHolder ?? _structureReferences.FirstOrDefault()?.MaintainableStructureEnumType;

        #endregion properties

        #region methods

        /// <summary>
        /// Parses the query string to create the <see cref="_structureReferences"/>.
        /// </summary>
        /// <param name="queryString">The query string.</param>
        private void ParseQueryString(IList<string> queryString)
        {
            if (queryString.Count < 2)
            {
                throw new SdmxSemmanticException("Availability Query expecting at least the parameter {context}");
            }

            FormatQuery(queryString);

            _contextHolder = GetStructureType(queryString[1]);

            string[] agencyIds = {};
            string[] resourceIds = {};
            string[] versions = {};
            if (queryString.Count > 2)
            {
                agencyIds = queryString[2].Split(',');
            }

            if (queryString.Count > 3)
            {
                resourceIds = queryString[3].Split(',');
            }

            if (queryString.Count > 4)
            {
                versions = queryString[4].Split(',');
            }

            foreach (string agencyId in agencyIds)
            {
                foreach (string id in resourceIds)
                {
                    foreach (string version in versions)
                    {
                        string versionVal = version;
                        if (this._versionWildcards.Contains(version))
                        {
                            // the new wildcards will not pass the validation.
                            // the information will be passed in the Metadata property.
                            versionVal = WildCard;
                        }
                        this._structureReferences.Add(new StructureReferenceImpl(agencyId, id, versionVal, _contextHolder));
                    }
                }
            }
        }

        /// <summary>
        /// Format the query arguments.
        /// </summary>
        /// <param name="query">The query.</param>
        private static void FormatQuery(IList<string> query)
        {
            for (int i = 0; i < query.Count; i++)
            {
                var param = query[i];
                if (!ObjectUtil.ValidString(param))
                {
                    query[i] = null;
                }
            }
        }

        /// <summary>
        /// Get the context type.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>the SDMX structure type</returns>
        private static SdmxStructureType GetStructureType(string context)
        {
            if (string.IsNullOrWhiteSpace(context))
            {
                return null;
            }

            if (context.Equals("*", StringComparison.OrdinalIgnoreCase))
            {
                return SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Any);
            }

            var result = SdmxStructureType.ParseClass(context);

            switch (result.ToEnumType())
            {
                case SdmxStructureEnumType.Dsd:
                case SdmxStructureEnumType.Dataflow:
                case SdmxStructureEnumType.ProvisionAgreement:
                    return result;
                default:
                    throw new SdmxSemmanticException(
                        $"Context value '{context}' is not valid. Availability query expects context to be one of: *, datastructure, dataflow, provisionagreement");
            }
        }

        /// <inheritdoc />
        protected override SdmxStructureType ParseReference(string referenceValue)
        {
            if (string.IsNullOrWhiteSpace(referenceValue) || referenceValue.Equals("none", StringComparison.OrdinalIgnoreCase))
            {
                return null;
            }

            if ("all".Equals(referenceValue, StringComparison.OrdinalIgnoreCase))
            {
                return SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Any);
            }

            return SdmxStructureType.ParseClass(referenceValue);
        }

        /// <summary>
        /// Builds the <see cref="IRestDataQueryV2"/> object.
        /// </summary>
        /// <param name="queryPath">the query path.</param>
        /// <param name="queryParameter">the query parameters.</param>
        /// <returns></returns>
        protected override IRestDataQuery BuildDataQuery(IList<string> queryPath, IDictionary<string, string> queryParameter)
        {
            List<string> fakeDataPath = new List<string>
            {
                "data"
            };
            
            for (int i = 1; i < Math.Min(queryPath.Count, 6); i++)
            {
                fakeDataPath.Add(queryPath[i]);
            }

            var dataQuery = new RESTDataQueryCoreV2(fakeDataPath, queryParameter);
            if (dataQuery.FlowRef == null)
            {
                throw new SdmxSemmanticException("At least the Dataflow id must be specified");
            }

            ParseQueryString(queryPath);

            return dataQuery;
        }

        /// <inheritdoc />
        protected override IList<string> GetComponentIds(IList<string> queryPath)
        {
            var result = new List<string>();
            if (queryPath.Count > 6)
            {
                var componentPart = queryPath[6];
                if (!WildCard.Equals(componentPart, StringComparison.OrdinalIgnoreCase) && !string.IsNullOrWhiteSpace(componentPart))
                {
                    result.AddRange(componentPart.Split(','));
                }
            }
            return result;
        }

        /// <inheritdoc />
        public override string ToString()
        {
            var queryParameters = new Dictionary<string, string>(QueryParameters);
            // add default parameters
            if (!queryParameters.ContainsKey(ReferencesQueryParameter))
            {
                queryParameters.Add(ReferencesQueryParameter, "none");
            }
            if (!queryParameters.ContainsKey(ModeQueryParameter))
            {
                queryParameters.Add(ModeQueryParameter, "exact");
            }

            var pathStr = string.Join("/", Path);
            if (queryParameters.Count > 0)
            {
                var queryParameter = string.Join("&", queryParameters.Select(x => string.Format(CultureInfo.InvariantCulture, "{0}={1}", x.Key, x.Value)));
                return string.Format(CultureInfo.InvariantCulture, "{0}?{1}", pathStr, queryParameter);
            }

            return pathStr;
        }
        
        /// <inheritdoc />
        public virtual IStructureReferenceIterator StartFlowRefIterator()
        {
            _structureReferenceIterator.Reset();
            return _structureReferenceIterator;
        }

        #endregion methods
    }
}
