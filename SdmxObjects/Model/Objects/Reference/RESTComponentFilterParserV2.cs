// -----------------------------------------------------------------------
// <copyright file="RESTComponentFilterParserV2.cs" company="EUROSTAT">
//   Date Created : 2021-08-18
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference
{
    using System;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;

    /// <summary>
    /// Parses the value of 'c' query parameter of the SDMX REST 2.0
    /// </summary>
    public class RESTComponentFilterParserV2 : IRestComponentFilterParser
    {
        private readonly Regex _matchComparisonOperator = new Regex(@"\w{2}(?=:)");
        private readonly Regex _matchComparisonOperatorAtBeginning = new Regex(@"\A\w{2}(?=:)");
        private readonly Regex _operatorParts = new Regex(@"\w{2}:.+?(?=([,+ ]\w{2}:)|$)");
        private readonly Regex _splitByAnd = new Regex(@"\+| ");

        private readonly RESTQueryOperator _defaultQueryOperator = RESTQueryOperator.GetFromEnum(RESTQueryOperatorEnumType.Equals);

        /// <summary>
        /// Can parse a query parameter to a <see cref="ComponentFilter"/>.
        /// </summary>
        /// <param name="componentName">The name of the component.</param>
        /// <param name="queryValue">The query parameter value for the component.</param>
        /// <param name="isTimeRange">Indicates whether it is a time-range component.</param>
        /// <returns></returns>
        /// <exception cref="SdmxSemmanticException">When <paramref name="queryValue"/> not in correct format.</exception>
        public ComponentFilter Parse(string componentName, string queryValue, bool isTimeRange = false)
        {
            if (string.IsNullOrEmpty(componentName))
            {
                throw new ArgumentNullException(nameof(componentName));
            }

            if (string.IsNullOrEmpty(queryValue))
            {
                throw new ArgumentNullException(nameof(queryValue));
            }
            
            // initialize parameters
            var componentFilterBuilder = RootComponentFilter.Builder
                .NewInstance(componentName)
                .SetAsTimeRange(isTimeRange);
            int? logicalOperator = null;
            RESTQueryOperator queryOperator = _defaultQueryOperator;

            // when query operators detected
            if (_matchComparisonOperator.IsMatch(queryValue))
            {
                if (!_matchComparisonOperatorAtBeginning.IsMatch(queryValue))
                {
                    throw new SdmxSemmanticException("comparison operator must start at the beginning of the string");
                }
                foreach (Match match in _operatorParts.Matches(queryValue))
                {
                    if (match.Index > 0)
                    {
                        char logicalOperatorValue = queryValue[match.Index - 1];
                        switch (logicalOperatorValue)
                        {
                            case ',':
                                logicalOperator = 1;
                                break;
                            case '+':
                            case ' ':
                                logicalOperator = 0;
                                break;
                            default:
                                throw new SdmxSemmanticException($"Invalid logical operator '{logicalOperatorValue}'. Accepted values are ',' & '+'.");
                        }
                    }
                    string[] complexValue = match.Value.Split(':');
                    queryOperator = RESTQueryOperator.GetFromOperatorType(complexValue[0]);
                    if (queryOperator == null)
                    {
                        string allowedOperatorTypes = string.Join(", ", RESTQueryOperator.Values.Select(v => v.OperatorType));
                        throw new SdmxSemmanticException(
                            "Unknown query operator '" + complexValue[0]
                            + $"' allowed operators are [{allowedOperatorTypes}]");
                    }
                    
                    // add filter values
                    var values = complexValue[1];
                    ParseAndAddFilterValues(componentFilterBuilder, queryOperator, logicalOperator, values);
                }
            }
            // when no query operators detected (simplest case)
            else
            {
                // add filter values
                ParseAndAddFilterValues(componentFilterBuilder, queryOperator, logicalOperator, queryValue);
            }
            
            return componentFilterBuilder.Build();
        }

        private void ParseAndAddFilterValues(RootComponentFilter.Builder builder, RESTQueryOperator queryOperator, int? logicalOperator, string input)
        {
            var values = new List<string>();
            var multipleValues = new List<List<string>>();
            foreach (var value in input.Split(','))
            {
                var multipleValue = _splitByAnd.Split(value);
                if (multipleValue.Length > 1)
                {
                    multipleValues.Add(multipleValue.ToList());
                }
                else
                {
                    values.Add(multipleValue[0]);
                }
            }

            builder.AddFilterValues(queryOperator, logicalOperator, values, multipleValues);
        }
    }
}
