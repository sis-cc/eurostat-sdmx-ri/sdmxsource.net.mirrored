// -----------------------------------------------------------------------
// <copyright file="DataQueryIterator.cs" company="EUROSTAT">
//   Date Created : 2021-09-01
//   Copyright (c) 2018 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference
{
    using System;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;

    /// <summary>
    /// The basic implementation of <see cref="IDataQueryIterator"/>.
    /// </summary>
    public class DataQueryIteratorCore : IDataQueryIterator
    {
        private readonly IStructureReferenceIterator _structureReferenceIterator;

        private readonly Action _buildDataQuery;

        /// <inheritdoc />
        public int Count => _structureReferenceIterator.Count;

        /// <summary>
        /// Initializes a new instance of the class <see cref="DataQueryIteratorCore"/>
        /// </summary>
        /// <param name="buildDataQuery">The function to run in <see cref="MoveNext"/></param>
        /// <param name="restQuery">The rest query to get the info from.</param>
        public DataQueryIteratorCore(Action buildDataQuery, IRestDataQueryV2 restQuery)
        {
            this._buildDataQuery = buildDataQuery;
            _structureReferenceIterator = restQuery.StartFlowRefIterator();
        }

        /// <inheritdoc />
        public bool MoveNext()
        {
            if (_structureReferenceIterator.MoveNext())
            {
                this._buildDataQuery();
                return true;
            }
            return false;
        }

        /// <inheritdoc />
        public void Reset()
        {
            _structureReferenceIterator.Reset();
        }
    }
}
