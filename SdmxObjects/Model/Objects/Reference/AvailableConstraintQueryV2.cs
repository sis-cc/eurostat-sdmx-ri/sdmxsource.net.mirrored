// -----------------------------------------------------------------------
// <copyright file="AvailableConstraintQueryWithIterator.cs" company="EUROSTAT">
//   Date Created : 2023-04-24
//   Copyright (c) 2018 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    /// Extends the <see cref="AvailableConstraintQuery"/> to support available queries for more than one structure
    /// and component filters.
    /// </summary>
    public class AvailableConstraintQueryV2 : AvailableConstraintQuery, IAvailableConstraintQueryWithIterator, IAvailableConstraintQueryWithComponentFilters
    {
        /// <summary>
        /// The collection of all structures that are referenced by the availability query.
        /// </summary>
        private readonly IList<IStructureReference> _structureReferences = new List<IStructureReference>();

        /// <summary>
        /// The iterator to use for iterating through the <see cref="IStructureReference"/>s of the query.
        /// </summary>
        private readonly StructureReferenceIteratorWithAction _structureReferenceIterator;

        /// <inheritdoc/>
        public IList<ComponentFilter> ComponentFilters { get; private set; }

        /// <inheritdoc />
        public virtual IStructureReferenceIterator StartFlowRefIterator()
        {
            _structureReferenceIterator.Reset();
            return _structureReferenceIterator;
        }

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="query">The availability query for SDMX REST v2.0</param>
        /// <param name="retrievalManger"></param>
        public AvailableConstraintQueryV2(IRestAvailabilityQuery query, ISdmxObjectRetrievalManager retrievalManger) 
            : base(query, retrievalManger)
        {
            // _structureReferences are set in the RetrieveArtefactInfo called by base constructor
            ComponentFilters = query.ComponentFilters;
            _structureReferenceIterator = new StructureReferenceIteratorWithAction(_structureReferences, 
                (structureRef) => IteratorAction(structureRef, query));
        }

        /// <summary>
        /// Retrieves the artefacts related to this availability request.
        /// </summary>
        /// <remarks>
        /// The structure reference in the <paramref name="queryBase"/> may reference more than one artefacts
        /// and not only dataflow.
        /// </remarks>
        /// <param name="queryBase"></param>
        /// <exception cref="SdmxNoResultsException">If the structure reference in the query brings no results.</exception>
        /// <exception cref="SdmxException">If the structure reference is invalid.</exception>
        protected override void RetrieveArtefactInfo(IRestAvailableConstraintQuery queryBase)
        {
            if (!(queryBase is IRestAvailabilityQuery query))
            {
                throw new ArgumentException($"expecting query of type {nameof(IRestAvailabilityQuery)}");
            } 

            IStructureReference queryReference = query.FlowRef.ChangeStarsToNull();
            var queryMetadata = query.QueryMetadata;
            var structureType = query.Context.EnumType;

            switch (structureType)
            {
                case SdmxStructureEnumType.Dataflow:
                    AddStructureReferences<IDataflowObject>(queryReference, queryMetadata);
                    break;
                case SdmxStructureEnumType.Dsd:
                    AddStructureReferences<IDataStructureObject>(queryReference, queryMetadata);
                    break;
                case SdmxStructureEnumType.ProvisionAgreement:
                    AddStructureReferences<IProvisionAgreementObject>(queryReference, queryMetadata);
                    break;
                case SdmxStructureEnumType.Any:
                    AddStructureReferences<IDataflowObject>(queryReference, queryMetadata);
                    AddStructureReferences<IDataStructureObject>(queryReference, queryMetadata);
                    AddStructureReferences<IProvisionAgreementObject>(queryReference, queryMetadata);
                    break;
                default:
                    throw new SdmxException($"Availability requests are not available for structure type {structureType}. " +
                        $"Please use on of the following: dataflow, datastructure or provision agreement.");
            }

            if (!_structureReferences.Any())
            {
                throw new SdmxNoResultsException("Cannot find any structures for query " + query.FlowRef);
            }
        }

        /// <summary>
        /// Retrieves the artefacts that match the <paramref name="queryReference"/>,
        /// and adds it as reference in the <see cref="_structureReferences"/> to feed the iterator.
        /// </summary>
        /// <typeparam name="T">The type of <see cref="IMaintainableObject"/></typeparam>
        /// <param name="queryReference"></param>
        /// <param name="queryMetadata"></param>
        private void AddStructureReferences<T>(IStructureReference queryReference, IObjectQueryMetadata queryMetadata)
            where T : IMaintainableObject
        {
            try
            {
                var sdmxObjects = RetrievalManger.GetMaintainableObjects<T>(queryReference, queryMetadata);
                _structureReferences.AddAll(sdmxObjects.Select(d => d.AsReference));
            }
            catch (SdmxNoResultsException)
            {
                // RetrievalManger will throw SdmxNoResultsException if no objects are found for this query reference.
                // So none will be added in the _structureReferences. 
            }
        }

        /// <summary>
        /// This is the action that the <see cref="_structureReferenceIterator"/> will invoke 
        /// for each reference in <see cref="_structureReferences"/>.
        /// Retrieves the <see cref="AvailableConstraintQuery.Dataflow"/> and <see cref="AvailableConstraintQuery.DataStructure"/>, 
        /// as well as the <see cref="AvailableConstraintQuery.Selections"/> and <see cref="AvailableConstraintQuery.Components"/>.
        /// </summary>
        /// <param name="structureReference">This now references a single structure.</param>
        /// <param name="query"></param>
        /// <exception cref="SdmxNoResultsException">If the structure is not found.</exception>
        /// <exception cref="SdmxException">If the structure type is not valid.</exception>
        private void IteratorAction(IStructureReference structureReference, IRestAvailabilityQuery query)
        {
            var structureType = structureReference.TargetReference.EnumType;

            switch (structureType)
            {
                case SdmxStructureEnumType.Dataflow:
                    Dataflow = RetrievalManger.GetMaintainableObject<IDataflowObject>(structureReference);
                    if (Dataflow == null)
                    {
                        throw new SdmxNoResultsException("Cannot find dataflow for query " + structureReference);
                    }
                    DataStructure = RetrievalManger.GetMaintainableObject<IDataStructureObject>(Dataflow.DataStructureRef);
                    if (DataStructure == null)
                    {
                        throw new SdmxNoResultsException("Cannot find datasetructure for query " + Dataflow.DataStructureRef);
                    }
                    break;

                case SdmxStructureEnumType.Dsd:
                    DataStructure = RetrievalManger.GetMaintainableObject<IDataStructureObject>(structureReference);
                    if (DataStructure == null)
                    {
                        throw new SdmxNoResultsException("Cannot find datasetructure for query " + Dataflow.DataStructureRef);
                    }
                    Dataflow = null; // DSD can be used by many dataflows
                    break;

                case SdmxStructureEnumType.ProvisionAgreement:
                    var provisionAgreement = RetrievalManger.GetMaintainableObject<IProvisionAgreementObject>(structureReference);
                    if (provisionAgreement == null)
                    {
                        throw new SdmxNoResultsException("Cannot find provisionAgreement for query " + structureReference);
                    }
                    Dataflow = RetrievalManger.GetMaintainableObject<IDataflowObject>(provisionAgreement.StructureUseage);
                    if (Dataflow == null)
                    {
                        throw new SdmxNoResultsException("Cannot find dataflow for query " + structureReference);
                    }
                    DataStructure = RetrievalManger.GetMaintainableObject<IDataStructureObject>(Dataflow.DataStructureRef);
                    if (DataStructure == null)
                    {
                        throw new SdmxNoResultsException("Cannot find datasetructure for query " + Dataflow.DataStructureRef);
                    }
                    break;

                default:
                    throw new SdmxException($"Availability requests are not available for structure type {structureType}. " +
                        $"Please use on of the following: dataflow, datastructure or provision agreement.");
            }

            Selections = ParseKeyValues(query, DataStructure);
            Components = ProcessComponents(query, DataStructure);
        }

        /// <summary>
        /// Parses the key values for SDMX REST 2.0
        /// </summary>
        /// <param name="queryBase">The query.</param>
        /// <param name="dataStructure"></param>
        /// <returns>List&lt;IKeyValues&gt;.</returns>
        protected override List<IKeyValues> ParseKeyValues(IRestAvailableConstraintQuery queryBase, IDataStructureObject dataStructure)
        {
            if (!(queryBase is IRestAvailabilityQuery query))
            {
                throw new ArgumentException($"expecting query of type {nameof(IRestAvailabilityQuery)}");
            }

            var selections = new List<IKeyValues>();

            if (query.QueryList.Count <= 0)
            {
                return selections;
            }

            var orderedDim = dataStructure.GetDimensions(SdmxStructureEnumType.Dimension, SdmxStructureEnumType.MeasureDimension);
            int maxCount = Math.Min(orderedDim.Count, query.QueryList.Count);

            for (int i = 0; i < maxCount; i++)
            {
                if (query.QueryList[i].Count > 0)
                {
                    var keyValue = new KeyValuesMutableImpl
                    {
                        Id = orderedDim[i].Id
                    };
                    keyValue.KeyValues.AddAll(query.QueryList[i]);
                    selections.Add(new KeyValuesCore(keyValue, null));
                }
            }

            return selections;
        }
    }
}
