// -----------------------------------------------------------------------
// <copyright file="AvailabilityQueryMetadataCore.cs" company="EUROSTAT">
//   Date Created : 2021-08-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference
{
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;

    /// <summary>
    /// Holds the query metadata for the availability requests.
    /// </summary>
    public class AvailabilityQueryMetadataCore : IObjectQueryMetadata
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AvailabilityQueryMetadataCore"/> class, by parsing the query string
        /// </summary>
        /// <param name="queryString">The query string to parse.</param>
        public AvailabilityQueryMetadataCore(string[] queryString)
        {
            ParserQuerystring(queryString);
        }

        /// <summary>
        ///     Gets a value indicating whether this is a query for all of the latest versions of the given artefacts
        /// </summary>
        public bool IsReturnLatest { get; private set; }

        /// <summary>
        ///     Gets a value indicating whether this is a query for all of the stable versions of the given artefacts
        /// </summary>
        public bool IsReturnStable { get; private set; }

        /// <summary>
        ///     Gets a value indicating whether this is a query for all of the stable versions of the given artefacts
        /// </summary>
        public bool IsReturnStub { get; private set; }

        /// <summary>
        /// Parses a query string to get query metadata.
        /// </summary>
        /// <param name="queryString">The query string to parse.</param>
        private void ParserQuerystring(IReadOnlyList<string> queryString)
        {
            if (queryString.Count > 4)
            {
                string version = queryString[4];
                if (version.Equals("~"))
                {
                    this.IsReturnLatest = true;
                }
                else if (version.Equals("+"))
                {
                    this.IsReturnLatest = true;
                    this.IsReturnStable = true;
                }
            }
        }
    }
}
