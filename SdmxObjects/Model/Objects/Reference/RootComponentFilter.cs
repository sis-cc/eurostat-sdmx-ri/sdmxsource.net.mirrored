// -----------------------------------------------------------------------
// <copyright file="RootComponentFilter.cs" company="EUROSTAT">
//   Date Created : 2021-08-18
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;

    /// <summary>
    /// Implements/extends the <see cref="ComponentFilter"/> to represent the root class for the filters of a component.
    /// </summary>
    internal class RootComponentFilter : ComponentFilter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RootComponentFilter"/> class.
        /// </summary>
        /// <param name="componentName">The name of the component.</param>
        private RootComponentFilter(string componentName)
        {
            ComponentName = componentName;
        }

        private int _index;

        /// <inheritdoc />
        public override IComponentFilterIterator StartIterator()
        {
            _index = 0;
            return new ComponentFilterIterator(this);
        }

        /// <inheritdoc />
        public override ComponentFilter Next()
        {
            return _index < InnerFilters.Count ? InnerFilters[_index++] : null;
        }

        /// <inheritdoc />
        public override List<string> GetTimeRangeValues()
        {
            if (!IsTimeRange)
            {
                throw new InvalidOperationException(nameof(GetTimeRangeValues) + " can only be used on a time range component");
            }
            var results = new List<string>();

            try
            {
                var startPeriodFilter = InnerFilters.FirstOrDefault(f =>
                    f.OperatorType.EnumType == RESTQueryOperatorEnumType.GreaterThan ||
                    f.OperatorType.EnumType == RESTQueryOperatorEnumType.GreaterThanOrEqualTo ||
                    f.OperatorType.EnumType == RESTQueryOperatorEnumType.Equals);
                if (startPeriodFilter != null)
                {
                    string startPeriodValue = startPeriodFilter.Values.SingleOrDefault();
                    results.Add(startPeriodValue);
                }
            }
            catch (Exception)
            {
                throw new SdmxSemmanticException("Time range can only have one value for start period.");
            }

            try
            {
                var endPeriodFilter = InnerFilters.FirstOrDefault(f =>
                    f.OperatorType.EnumType == RESTQueryOperatorEnumType.LessThan ||
                    f.OperatorType.EnumType == RESTQueryOperatorEnumType.LessThanOrEqualTo);
                if (endPeriodFilter != null)
                {
                    string endPeriodValue = endPeriodFilter.Values.SingleOrDefault();
                    results.Add(endPeriodValue);
                }
            }
            catch (Exception)
            {
                throw new SdmxSemmanticException("Time range can only have one value for end period.");
            }

            return results;
        }

        private class InnerComponentFilter : ComponentFilter
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="InnerComponentFilter"/> class.
            /// </summary>
            /// <param name="componentName">The name of the component.</param>
            /// <param name="queryOperator">The comparison operator</param>
            /// <param name="logicalOperator">The logical operator</param>
            /// <param name="parent">The parent item.</param>
            /// <param name="values">The values that correspond to single value attributes</param>
            /// <param name="multipleValues">The values that correspond to multiple values attributes</param>
            public InnerComponentFilter(
                string componentName, 
                RESTQueryOperator queryOperator, 
                int? logicalOperator, 
                ComponentFilter parent,
                List<string> values,
                IEnumerable<List<string>> multipleValues)
            {
                ComponentName = componentName;
                OperatorType = queryOperator;
                LogicalOperator = logicalOperator;
                Parent = parent;
                Values.AddRange(values ?? new List<string>());
                ArrayValues.AddRange(multipleValues);
            }

            /// <inheritdoc />
            public override ComponentFilter Next()
            {
                return Parent.Next();
            }

            /// <inheritdoc />
            public override List<string> GetTimeRangeValues()
            {
                return Parent.GetTimeRangeValues();
            }

            public override IComponentFilterIterator StartIterator()
            {
                throw new NotImplementedException();
            }

            /// <inheritdoc />
            public override bool IsTimeRange => Parent.IsTimeRange;

            public void SetParent(ComponentFilter rootFilter)
            {
                Parent = rootFilter;
            }
        }

        internal class Builder
        {
            /// <summary>
            /// The <see cref="ComponentFilter.ComponentName"/>
            /// </summary>
            private readonly string _componentName;

            /// <summary>
            /// The <see cref="ComponentFilter.IsTimeRange"/>
            /// </summary>
            private bool _isTimeRange;

            /// <summary>
            /// The <see cref="ComponentFilter.InnerFilters"/>
            /// </summary>
            private readonly List<InnerComponentFilter> _innerFilters = new List<InnerComponentFilter>();

            internal static Builder NewInstance(string componentName)
            {
                return new Builder(componentName);
            }

            private Builder(string componentName)
            {
                _componentName = componentName;
            }

            internal Builder SetAsTimeRange(bool isTimeRange)
            {
                _isTimeRange = isTimeRange;
                return this;
            }

            /// <summary>
            /// Adds a filter element for the component.
            /// </summary>
            /// <param name="queryOperator">The comparison operator.</param>
            /// <param name="logicalOperator">The logical operator (null if none).</param>
            /// <param name="values">The <see cref="ComponentFilter.Values"/></param>
            /// <param name="multipleValues">The <see cref="ComponentFilter.ArrayValues"/></param>
            /// <returns></returns>
            internal Builder AddFilterValues(
                RESTQueryOperator queryOperator, int? logicalOperator,
                List<string> values, List<List<string>> multipleValues)
            {
                if (queryOperator == null || queryOperator == RESTQueryOperator.GetFromEnum(RESTQueryOperatorEnumType.Null))
                {
                    throw new ArgumentNullException(nameof(queryOperator));
                }
                if ((values == null || !values.Any()) &&
                    (multipleValues == null || !multipleValues.Any()))
                {
                    throw new InvalidOperationException("Component filter must have at least one value.");
                }

                var innerFilter = new InnerComponentFilter(_componentName, queryOperator, logicalOperator, null, values, multipleValues);
                _innerFilters.Add(innerFilter);

                return this;
            }
            
            /// <summary>
            /// Builds the data into a <see cref="RootComponentFilter"/>.
            /// </summary>
            /// <returns>A new instance of <see cref="RootComponentFilter"/>.</returns>
            /// <exception cref="ArgumentNullException">for <see cref="ComponentFilter.ComponentName"/>.</exception>
            internal RootComponentFilter Build()
            {
                if (string.IsNullOrEmpty(_componentName))
                {
                    throw new ArgumentNullException(nameof(_componentName));
                }

                if (!_innerFilters.Any())
                {
                    throw new ArgumentNullException(nameof(RootComponentFilter) + " must have at least one inner filter.");
                }
                
                var rootFilter = new RootComponentFilter(_componentName)
                {
                    IsTimeRange = _isTimeRange
                };
                
                foreach (var filter in _innerFilters)
                {
                    filter.SetParent(rootFilter);
                }
                
                rootFilter.InnerFilters.AddRange(_innerFilters);

                return rootFilter;
            }
        }
    }
}
