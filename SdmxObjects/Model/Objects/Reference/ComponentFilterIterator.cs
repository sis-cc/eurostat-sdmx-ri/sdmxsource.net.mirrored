// -----------------------------------------------------------------------
// <copyright file="ComponentFilterIterator.cs" company="EUROSTAT">
//   Date Created : 2021-08-18
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference
{
    using System;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;

    /// <summary>
    /// An iterator to run through all layers of <see cref="ComponentFilter"/>
    /// with the proper order.
    /// </summary>
    public class ComponentFilterIterator : IComponentFilterIterator
    {
        /// <summary>
        /// The current item
        /// </summary>
        public ComponentFilter Current { get; private set; }

        private readonly ComponentFilter _startItem;

        /// <summary>
        /// Initializes a new instance of the class <see cref="ComponentFilterIterator"/>
        /// </summary>
        /// <param name="startItem">The item to start the iteration from.</param>
        public ComponentFilterIterator(ComponentFilter startItem)
        {
            if (startItem == null)
            {
                throw new ArgumentNullException(nameof(startItem));
            }
            _startItem = Current = startItem;
        }

        /// <summary>
        /// Resets the iterator to start over
        /// </summary>
        public void Reset()
        {
            Current = _startItem;
        }

        /// <summary>
        /// Moves to the next item in line.
        /// </summary>
        /// <returns><c>true</c> if next item exists, <c>false</c> if is end of line.</returns>
        public bool MoveNext()
        {
            var next = Current.Next();
            if (next != null)
            {
                Current = next;
                return true;
            }
            return false;
        }
    }
}
