// -----------------------------------------------------------------------
// <copyright file="RESTDataQueryCoreV2.cs" company="EUROSTAT">
//   Date Created : 2021-08-11
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference
{
    #region Using directives

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;

    #endregion

    /// <summary>
    /// The rest data query for SDMX REST 2.0
    /// </summary>
    public class RESTDataQueryCoreV2 : RESTDataQueryCore, IRestDataQueryV2
    {
        private readonly IList<IStructureReference> _structureReferences = new List<IStructureReference>();

        private readonly string[] _versionWildcards = { "*", "~", "+" };

        private const string TIME_RANGE_ATTRIBUTE = "TIME_PERIOD";

        /// <inheritdoc />
        protected override string AllKeyword => "*";

        /// <summary>
        /// Regex to take the attribute name in a c-parameter
        /// </summary>
        private readonly Regex _cParamRegex = new Regex(@"(?<=c\[)\w+(?=\])");

        /// <summary>
        /// The pattern for key parameter values
        /// </summary>
        private readonly Regex _keyParamRegex = new Regex(@"^(\*|\S+)$");

        /// <summary>
        /// The parser to create the <see cref="_componentFilters"/> from the c-parameters
        /// </summary>
        private readonly IRestComponentFilterParser _componentFilterParser = new RESTComponentFilterParserV2();

        /// <summary>
        /// The iterator to use for iterating through the <see cref="IStructureReference"/>s of the query.
        /// </summary>
        private readonly StructureReferenceIteratorCore _structureReferenceIterator;

        /// <summary>
        /// Field for <see cref="ComponentFilter"/>s.
        /// </summary>
        private readonly List<ComponentFilter> _componentFilters = new List<ComponentFilter>();
        
        /// <summary>
        /// Get the context of the query. Possible values: *, datastructure, dataflow, provisionagreement
        /// </summary>
        private SdmxStructureType _contextHolder;

        /// <inheritdoc />
        public RESTDataQueryCoreV2(IList<string> querystring, IDictionary<string, string> queryParameters) 
            : base(querystring, queryParameters)
        {
            QueryMetadata = new DataQueryMetadataCore(querystring.ToArray());
            _structureReferenceIterator = new StructureReferenceIteratorCore(_structureReferences);
        }

        /// <summary>
        /// Gets the dataflow reference.
        /// If the <see cref="StartFlowRefIterator"/> is run, this property will change its value in every iteration.
        /// </summary>
        public override IStructureReference FlowRef => _structureReferenceIterator.Current;

        /// <inheritdoc />
        public IObjectQueryMetadata QueryMetadata { get; }

        /// <inheritdoc />
        public IList<ComponentFilter> ComponentFilters => this._componentFilters;

        /// <summary>
        /// Get the context of the query. Possible values: *, datastructure, dataflow, provisionagreement
        /// </summary>
        public SdmxStructureType Context => _contextHolder ?? _structureReferences.FirstOrDefault()?.MaintainableStructureEnumType;

        /// <summary>
        /// Gets the attributes.
        /// </summary>
        public Attributes Attributes { get; private set; } = Attributes.GetFromEnum(AttributesEnumType.Dsd);

        /// <summary>
        /// Gets the measures.
        /// </summary>
        public Measures Measures { get; private set; } = Measures.GetFromEnum(MeasuresEnumType.All);

        /// <inheritdoc />
        public virtual IStructureReferenceIterator StartFlowRefIterator()
        {
            _structureReferenceIterator.Reset();
            return _structureReferenceIterator;
        }

        /// <summary>
        /// Format the query arguments.
        /// </summary>
        /// <param name="query">The query.</param>
        private static void FormatQuery(IList<string> query)
        {
            for (int i = 0; i < query.Count; i++)
            {
                var param = query[i];
                if (!ObjectUtil.ValidString(param))
                {
                    query[i] = null;
                }
            }
        }

        /// <inheritdoc />
        protected override void ParserQuerystring(IList<string> querystring)
        {
            if (querystring.Count < 2)
            {
                throw new SdmxSemmanticException("Data query expected to contain Flow as the second argument");
            }

            FormatQuery(querystring);

            _contextHolder = GetStructureType(querystring[1]);

            string[] agencyIds = {};
            string[] resourceIds = {};
            string[] versions = {};
            if (querystring.Count > 2)
            {
                agencyIds = querystring[2].Split(',');
            }

            if (querystring.Count > 3)
            {
                resourceIds = querystring[3].Split(',');
            }

            if (querystring.Count > 4)
            {
                versions = querystring[4].Split(',');
            }

            foreach (string agencyId in agencyIds)
            {
                foreach (string id in resourceIds)
                {
                    foreach (string version in versions)
                    {
                        string versionVal = version;
                        if (this._versionWildcards.Contains(version))
                        {
                            versionVal = "*";
                        }
                        this._structureReferences.Add(new StructureReferenceImpl(agencyId, id, versionVal, _contextHolder));
                    }
                }
            }

            if (querystring.Count > 5)
            {
                this.SetKey(querystring[5]);
            }
            else
            {
                this.SetKey(AllKeyword);
            }
        }

        /// <inheritdoc />
        protected override void ParserQueryParameters(IEnumerable<KeyValuePair<string, string>> paramsDict)
        {
            if (paramsDict != null)
            {
                foreach (var keyValue in paramsDict)
                {
                    var key = keyValue.Key;
                    var value = keyValue.Value;

                    if (key.Equals("updatedAfter", StringComparison.OrdinalIgnoreCase))
                    {
                        this.SetUpdatedAfter(value);
                    }
                    else if (key.Equals("firstNObservations", StringComparison.OrdinalIgnoreCase))
                    {
                        this.SetFirstXObs(value);
                    }
                    else if (key.Equals("lastNObservations", StringComparison.OrdinalIgnoreCase))
                    {
                        this.SetLastXObs(value);
                    }
                    else if (key.Equals("dimensionAtObservation", StringComparison.OrdinalIgnoreCase))
                    {
                        this.SetDimensionAtObservation(value);
                    }
                    else if (key.Equals("detail", StringComparison.OrdinalIgnoreCase))
                    {
                        this.SetDetail(value);
                    }
                    else if (key.Equals("includeHistory", StringComparison.OrdinalIgnoreCase))
                    {
                        this.SetIncludeHistory(value);
                    }
                    else if (key.Equals("attributes", StringComparison.OrdinalIgnoreCase))
                    {
                        this.SetAttributes(value);
                    }
                    else if (key.Equals("measures", StringComparison.OrdinalIgnoreCase))
                    {
                        this.SetMeasures(value);
                    }
                    else if (key.Equals("asOf", StringComparison.OrdinalIgnoreCase))
                    {
                        this.SetAsOfDate(value);
                    }
                    else if (_cParamRegex.IsMatch(key))
                    {
                        string component = _cParamRegex.Match(key).Value;
                        this.SetComponentValue(component, value);
                    }

                    else
                    {
                        throw new SdmxSemmanticException(
                            "Unknown query parameter  '" + key
                            + "' allowed parameters [updatedAfter, firstNObservations, lastNObservations, dimensionAtObservation, detail, attributes, measures, includeHistory, asOf]");
                    }
                }
            }
        }

        /// <inheritdoc />
        protected override void SetKey(string value)
        {
            if (!value.Equals(AllKeyword, StringComparison.OrdinalIgnoreCase))
            {
                // NOTE in .NET Split does not accept regex like in Java.
                string[] split = value.Split('.');

                foreach (string currentKey in split)
                {
                    ISet<string> selectionsList = new HashSet<string>();
                    this._queryList.Add(selectionsList);
                    
                    // treat 'all' keyword as no filter 
                    if (currentKey.Equals(AllKeyword, StringComparison.OrdinalIgnoreCase))
                    {
                        continue;
                    }

                    // the docs don't refer to any other operator like '+'...
                    if (_keyParamRegex.IsMatch(currentKey))
                    {
                        selectionsList.Add(currentKey);
                    }
                    else
                    {
                        throw new SdmxSemmanticException($"key parameter value '{currentKey}' doesn't comply with the value pattern '{_keyParamRegex}'");
                    } 
                }
            }
        }

        private void SetComponentValue(string component, string value)
        {
            if (_componentFilters.Any(f => f.ComponentName.Equals(component)))
            {
                throw new SdmxSemmanticException(
                    $"Duplicate component found: '{component}'. Parameter 'c' can only be used once per component.");
            }

            bool isTimeRange = component.Equals(TIME_RANGE_ATTRIBUTE, StringComparison.OrdinalIgnoreCase);

            ComponentFilter filter = _componentFilterParser.Parse(component, value, isTimeRange);
            _componentFilters.Add(filter);

            // do special cases
            if (isTimeRange)
            {
                var values = filter.GetTimeRangeValues();
                this.SetStartPeriod(values[0]);
                if (values.Count > 1)
                {
                    this.SetEndPeriod(values[1]);
                }
            }
        }

        /// <summary>
        /// Get the context type.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>the SDMX structure type</returns>
        private SdmxStructureType GetStructureType(string context)
        {
            if (string.IsNullOrWhiteSpace(context))
            {
                return null;
            }

            if (context.Equals(AllKeyword, StringComparison.OrdinalIgnoreCase))
            {
                return SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Any);
            }

            var result = SdmxStructureType.ParseClass(context);

            switch (result.ToEnumType())
            {
                case SdmxStructureEnumType.Dsd:
                case SdmxStructureEnumType.Dataflow:
                case SdmxStructureEnumType.ProvisionAgreement:
                    return result;
                default:
                    throw new SdmxSemmanticException(
                        $"Context value '{context}' is not valid. Availability query expects context to be one of: *, datastructure, dataflow, provisionagreement");
            }
        }

        /// <summary>
        /// Sets the attributes.
        /// </summary>
        /// <param name="value">The value.</param>
        private void SetAttributes(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                Attributes = Attributes.ParseString(value);
            }
        }

        /// <summary>
        /// Sets the measures.
        /// </summary>
        /// <param name="value">The value.</param>
        private void SetMeasures(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                Measures = Measures.ParseString(value);
            }
        }
    }
}