using System;
using Org.Sdmxsource.Sdmx.Api.Model.Diff;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Util;

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Diff
{
    public class Diff : IComparable<Diff>, IDiff
    {
        public IStructureReference StructureReference { get; private set; }
        public string Property { get; private set; }
        public string SourceValue { get; private set; }
        public string TargetValue { get; private set; }
        public ModificationAction Modification { get; private set; }

        public Diff(IStructureReference structureReference, bool isSourceIdentifiable, string property, string sourceValue, string targetValue)
        {
            StructureReference = structureReference;
            Property = property;
            SourceValue = sourceValue;
            TargetValue = targetValue;

            if (!isSourceIdentifiable || ObjectUtil.ValidObject(sourceValue, targetValue))
            {
                Modification = ModificationAction.Edit;
            }
            else if (ObjectUtil.ValidObject(sourceValue))
            {
                Modification = ModificationAction.Removal;
            }
            else
            {
                Modification = ModificationAction.Addition;
            }
        }

        private int CompareStrings(string str1, string str2)
        {
            if (str1 == null)
            {
                return str2 != null ? 1 : 0;
            }
            else if (str2 == null)
            {
                return 0;
            }
            return str1.CompareTo(str2);
        }

        public int CompareTo(Diff that)
        {
            int returnVal = Modification.CompareTo(that.Modification);
            if (returnVal == 0)
            {
                returnVal = string.Compare(StructureReference.TargetUrn.ToString(), that.StructureReference.TargetUrn.ToString());
            }
            if (returnVal == 0)
            {
                returnVal = CompareStrings(Property, that.Property);
            }
            if (returnVal == 0)
            {
                returnVal = CompareStrings(SourceValue, that.SourceValue);
            }
            if (returnVal == 0)
            {
                returnVal = CompareStrings(TargetValue, that.TargetValue);
            }
            return returnVal;
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is Diff)
            {
                return ((Diff)obj).ToString() == ToString();
            }
            return false;
        }

        public override string ToString()
        {
            return Modification + "," + StructureReference.TargetUrn + "," + Property + "," + SourceValue + "," + TargetValue;
        }
    }
}
