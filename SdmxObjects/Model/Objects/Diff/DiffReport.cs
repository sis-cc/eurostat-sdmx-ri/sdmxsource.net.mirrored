using System;
using System.Collections.Generic;
using System.Linq;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Diff;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Diff
{
    public class DiffReport : IDiffReport
    {
        private readonly Dictionary<ModificationAction, SortedSet<Diff>> diffMap;
        private readonly IMaintainableObject source;
        private readonly IMaintainableObject target;
        private readonly IMaintainableObject originalTarget;
        private readonly HashSet<string> propsToIgnore;

        public DiffReport(IMaintainableObject source, IMaintainableObject target)
        {
            if (source == null)
            {
                throw new SdmxException("Diff Report requires a Source Maintainable");
            }
            if (target == null)
            {
                throw new SdmxException("Diff Report requires a Target Maintainable");
            }

            this.source = source;
            this.target = target;
            diffMap = new Dictionary<ModificationAction, SortedSet<Diff>>();
            propsToIgnore = new HashSet<string>();

            foreach (ModificationAction mod in Enum.GetValues(typeof(ModificationAction)))
            {
                diffMap[mod] = new SortedSet<Diff>();
            }

            if (!target.AgencyId.Equals(source.AgencyId) || !target.Id.Equals(source.Id))
            {
                originalTarget = target;
                var mutated = target.MutableInstance;
                mutated.AgencyId = source.AgencyId;
                mutated.Id = source.Id;
                this.target = mutated.ImmutableInstance;
            }
        }

        public DiffReport(IMaintainableObject source, IMaintainableObject target, HashSet<string> ignoreProps)
        {
            this.source = source;
            this.target = target;
            diffMap = new Dictionary<ModificationAction, SortedSet<Diff>>();
            propsToIgnore = ignoreProps;

            foreach (ModificationAction mod in Enum.GetValues(typeof(ModificationAction)))
            {
                diffMap[mod] = new SortedSet<Diff>();
            }
        }

        public void AddDiff(ISdmxObject source, string property, object sourceVal, object targetVal)
        {
            if (propsToIgnore.Contains(property))
            {
                return;
            }

            string sourceStr = ToString(sourceVal);
            string targetStr = ToString(targetVal);

            IIdentifiableObject identifiable = source.GetParent<IIdentifiableObject>(true);
            IStructureReference sRef;

            if (originalTarget != null && identifiable.MaintainableParent == target)
            {
                var newRef = new StructureReferenceImpl(identifiable.Urn);
                newRef.AgencyId = originalTarget.AgencyId;
                newRef.MaintainableId = originalTarget.Id;
                sRef = newRef;
            }
            else
            {
                sRef = identifiable.AsReference;
            }

            bool isSourceIdentifiable = identifiable == source;
            Diff diff = new Diff(sRef, isSourceIdentifiable, property, sourceStr, targetStr);
            if (diffMap.ContainsKey(diff.Modification))
            {
                diffMap[diff.Modification].Add(diff);
            }
            else
            {
                diffMap.Add(diff.Modification, new SortedSet<Diff> { diff });
            }

        }

        private string ToString(object val)
        {
            if (val == null)
            {
                return null;
            }

            if (val is INameableObject)
            {
                var nameable = (INameableObject)val;
                string description = nameable.Description != null ? $"<br/><strong>Description</strong>: {nameable.Description}" : "";
                return $"<strong>Id</strong>: {nameable.Id}<br/><strong>Name</strong>: {nameable.Name}{description}";
            }
            else
            {
                return val.ToString();
            }
        }

        public ISet<IDiff> GetDifferences(ModificationAction modificationType)
        {
            return new SortedSet<IDiff>(diffMap[modificationType]);
        }

        public ISet<IDiff> GetDifferences()
        {
            HashSet<IDiff> returnSet = new HashSet<IDiff>();

            foreach (ModificationAction action in diffMap.Keys)
            {
                returnSet.UnionWith(diffMap[action]);
            }

            return returnSet;
        }

        public IMaintainableObject Source => source;

        public IMaintainableObject Target => target;

    }
}
