// -----------------------------------------------------------------------
// <copyright file="CubeRegionCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The cube region core.
    /// </summary>
    [Serializable]
    public class CubeRegionCore : SdmxStructureCore, ICubeRegion
    {
        /// <summary>
        ///     The attribute values.
        /// </summary>
        private readonly IList<IKeyValues> _attributeValues;

        private readonly IList<string> _warnings = new List<string>();

        /// <summary>
        ///     The key values.
        /// </summary>
        private readonly IList<IKeyValues> _keyValues;

        /// <summary>
        ///     Initializes a new instance of the <see cref="CubeRegionCore" /> class.
        /// </summary>
        /// <param name="mutable">
        ///     The mutable.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="mutable"/> is <see langword="null" />.</exception>
        public CubeRegionCore(ICubeRegionMutableObject mutable, IContentConstraintObject parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CubeRegion), parent)
        {
            if (mutable == null)
            {
                throw new ArgumentNullException("mutable");
            }

            this._keyValues = new List<IKeyValues>();
            this._attributeValues = new List<IKeyValues>();
            this._keyValues = new List<IKeyValues>();
            if (ObjectUtil.ValidCollection(mutable.KeyValues))
            {
                foreach (IKeyValuesMutable keyValuesMutable in mutable.KeyValues)
                {
                    if (ObjectUtil.ValidCollection(keyValuesMutable.KeyValues) || keyValuesMutable.TimeRange !=null)
                    {
                        this._keyValues.Add(new KeyValuesCore(keyValuesMutable, this));
                    }
                }
            }

            this._attributeValues = new List<IKeyValues>();
            if (ObjectUtil.ValidCollection(mutable.AttributeValues))
            {
                foreach (IKeyValuesMutable keyValuesMutable in mutable.AttributeValues)
                {
                    if (ObjectUtil.ValidCollection(keyValuesMutable.KeyValues))
                    {
                        this._attributeValues.Add(new KeyValuesCore(keyValuesMutable, this));
                    }
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="CubeRegionCore" /> class.
        /// </summary>
        /// <param name="cubeRegionType">
        ///     The cube region type.
        /// </param>
        /// <param name="negate">
        ///     The negate.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="cubeRegionType"/> is <see langword="null" />.</exception>
        public CubeRegionCore(CubeRegionType cubeRegionType, bool negate, IContentConstraintObject parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CubeRegion), parent)
        {
            if (cubeRegionType == null)
            {
                throw new ArgumentNullException("cubeRegionType");
            }
            
            this._keyValues = new List<IKeyValues>();
            this._attributeValues = new List<IKeyValues>();
            var attributeValueSetTypes = cubeRegionType.GetTypedAttribute<AttributeValueSetType>();

            ISet<string> ensureNoDuplicate = new HashSet<string>(StringComparer.Ordinal);
            if (attributeValueSetTypes != null)
            {
                // we ignore any duplicate key/attribute value for the same component as it is not clear yet how to handle it
                // if a specific case is found that we need to workaround we can see then
                // Note component IDs are unique so the same set is used for both attributes and dimensions
                // Note ISet<>.Add returns true if the item doesn't exist in the ISet<>
                foreach (AttributeValueSetType valueSetType in attributeValueSetTypes)
                {
                    var keyValues = new KeyValuesCore(valueSetType, this);
                    if (keyValues.Warnings.Any())
                    {
                        foreach (var warning in keyValues.Warnings)
                        {
                            this._warnings.Add(warning + $" for component id {valueSetType.id}");
                        }

                    }
                    if (!valueSetType.include)
                    {
                        if (negate)
                        {
                            if (!CheckDuplicates(this._attributeValues, valueSetType))
                            {
                                this._attributeValues.Add(keyValues);
                            }
                        }
                    }
                    else if (!negate)
                    {
                        if (!CheckDuplicates(this._attributeValues, valueSetType))
                        {
                            this._attributeValues.Add(keyValues);
                        }
                    }
                }
            }

            var cubeRegionKeyTypes = cubeRegionType.GetTypedKeyValue<CubeRegionKeyType>();
            if (cubeRegionKeyTypes != null)
            {
                // we ignore any duplicate key/attribute value for the same component as it is not clear yet how to handle it
                // if a specific case is found that we need to workaround we can see then
                // Note component IDs are unique so the same set is used for both attributes and dimensions
                // Note ISet<>.Add returns true if the item doesn't exist in the ISet<>
                foreach (var valueSetType0 in cubeRegionKeyTypes)
                {
                    var keyValues = new KeyValuesCore(valueSetType0, this);
                    if(keyValues.Warnings.Any())
                    {
                        foreach(var warning in keyValues.Warnings)
                        {
                            this._warnings.Add(warning + $" for component id {valueSetType0.id}");
                        }
                        
                    }
                    if (!valueSetType0.include)
                    {
                        if (negate)
                        {
                            if (!CheckDuplicates(this._keyValues, valueSetType0))
                            {
                                this._keyValues.Add(keyValues);
                            }
                        }
                    }
                    else if (!negate)
                    {
                        if (!CheckDuplicates(this._keyValues, valueSetType0))
                        {
                            this._keyValues.Add(keyValues);
                        }
                    }
                }
            }
        }

        private bool CheckDuplicates(IList<IKeyValues> keyValues, ComponentValueSetType valueSetType)
        {
            if (keyValues.FirstOrDefault(x => x.Id == valueSetType.id) != null)
            {
                this._warnings.Add($"Duplicate entry for component {valueSetType.id}");
                return true;
            }
            return false;
        }

        /// <summary>
        ///     Gets the attribute values.
        /// </summary>
        public virtual IList<IKeyValues> AttributeValues
        {
            get
            {
                return new List<IKeyValues>(this._attributeValues);
            }
        }

        /// <summary>
        ///     Gets the key values.
        /// </summary>
        public virtual IList<IKeyValues> KeyValues
        {
            get
            {
                return new List<IKeyValues>(this._keyValues);
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (ICubeRegion)sdmxObject;
                if (!this.Equivalent(this._keyValues, that.KeyValues, includeFinalProperties))
                {
                    return false;
                }

                if (!this.Equivalent(this._attributeValues, that.AttributeValues, includeFinalProperties))
                {
                    return false;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        ///     Gets a set of values for the given component id.  Returns an empty set if there are no values specified for the
        ///     given component
        /// </summary>
        /// <param name="componentId">The component Id.</param>
        /// <returns>
        ///     The set of values
        /// </returns>
        public ISet<string> GetValues(string componentId)
        {
            foreach (IKeyValues kvs in this._keyValues)
            {
                if (kvs.Id.Equals(componentId))
                {
                    return new HashSet<string>(kvs.Values);
                }
            }

            foreach (IKeyValues kvs in this._attributeValues)
            {
                if (kvs.Id.Equals(componentId))
                {
                    return new HashSet<string>(kvs.Values);
                }
            }

            return new HashSet<string>();
        }

        /// <summary>
        ///     The get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._keyValues, composites);
            this.AddToCompositeSet(this._attributeValues, composites);
            return composites;
        }

        /// <summary>
        /// Returns the warnings
        /// </summary>
        public IList<string> Warnings
        {
            get { return this._warnings; }
        }
    }
}