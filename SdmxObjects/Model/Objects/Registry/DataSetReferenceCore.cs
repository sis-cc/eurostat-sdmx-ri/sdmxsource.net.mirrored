﻿// -----------------------------------------------------------------------
// <copyright file="DataSetReferenceCore.cs" company="EUROSTAT">
//   Date Created : 2013-03-14
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry
{
    #region Using directives

    using System;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;

    #endregion

    /// <summary>
    ///     DataSetReferenceCore class
    /// </summary>
    public class DataSetReferenceCore : SdmxStructureCore, IDataSetReference
    {
        /// <summary>
        ///     The _data provider reference
        /// </summary>
        private readonly ICrossReference _dataProviderReference;

        /// <summary>
        ///     The _dataset identifier
        /// </summary>
        private readonly string _datasetId;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataSetReferenceCore" /> class.
        /// </summary>
        /// <param name="mutableObject">The mutable object.</param>
        /// <param name="parent">The parent.</param>
        /// <exception cref="SdmxSemmanticException">Semantic exception</exception>
        public DataSetReferenceCore(IDataSetReferenceMutableObject mutableObject, IMetadataTargetKeyValues parent)
            : base(mutableObject, parent)
        {
            this._datasetId = mutableObject.DatasetId;
            if (mutableObject.DataProviderReference != null)
            {
                this._dataProviderReference = new CrossReferenceImpl(this, mutableObject.DataProviderReference);
                try
                {
                    this.Validate();
                }
                catch (SdmxSemmanticException e)
                {
                    throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
                }
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataSetReferenceCore" /> class.
        /// </summary>
        /// <param name="refType">Type of the reference.</param>
        /// <param name="parent">The parent.</param>
        /// <exception cref="SdmxSemmanticException">Semantic exception</exception>
        /// <exception cref="ArgumentNullException"><paramref name="refType"/> is <see langword="null" />.</exception>
        public DataSetReferenceCore(SetReferenceType refType, IMetadataTargetKeyValues parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DatasetReference), parent)
        {
            if (refType == null)
            {
                throw new ArgumentNullException("refType");
            }

            this._datasetId = refType.ID;
            this._dataProviderReference = RefUtil.CreateReference(this, refType.DataProvider);
            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        /// <summary>
        ///     Gets the Data Provider Reference
        /// </summary>
        public ICrossReference DataProviderReference
        {
            get
            {
                return this._dataProviderReference;
            }
        }

        /// <summary>
        ///     Gets the dataset id
        /// </summary>
        public string DatasetId
        {
            get
            {
                return this._datasetId;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">The sdmxObject.</param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            var that = sdmxObject as IDataSetReference;
            if (that != null)
            {
                if (!ObjectUtil.Equivalent(this.DataProviderReference, that.DataProviderReference))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this.DatasetId, that.DatasetId))
                {
                    return false;
                }
            }

            return false;
        }

        /// <summary>
        ///     Validates this instance.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">
        ///     Dataset Reference missing mandatory 'id' identifier
        ///     or
        ///     Dataset Reference missing mandatory 'data provider reference'
        /// </exception>
        private void Validate()
        {
            if (!ObjectUtil.ValidString(this._datasetId))
            {
                throw new SdmxSemmanticException("Dataset Reference missing mandatory 'id' identifier");
            }

            if (this._dataProviderReference == null)
            {
                throw new SdmxSemmanticException("Dataset Reference missing mandatory 'data provider reference'");
            }
        }
    }
}