// -----------------------------------------------------------------------
// <copyright file="InputOutputCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Process
{
    using System;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The input output core.
    /// </summary>
    [Serializable]
    public class InputOutputCore : AnnotableCore, IInputOutputObject
    {
        /// <summary>
        ///     The local id.
        /// </summary>
        private readonly string _localId;

        /// <summary>
        ///     The structure reference.
        /// </summary>
        private readonly ICrossReference _structureReference;

        /// <summary>
        ///     Initializes a new instance of the <see cref="InputOutputCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="mutableObject">
        ///     The mutable object.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="mutableObject"/> is <see langword="null" />.</exception>
        public InputOutputCore(IIdentifiableObject parent, IInputOutputMutableObject mutableObject)
            : base(mutableObject, parent)
        {
            if (mutableObject == null)
            {
                throw new ArgumentNullException("mutableObject");
            }

            this._localId = mutableObject.LocalId;
            if (mutableObject.StructureReference != null)
            {
                this._structureReference = new CrossReferenceImpl(this, mutableObject.StructureReference);
            }

            this.Validate();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="InputOutputCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="xmlType">
        ///     The xml type.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="xmlType"/> is <see langword="null" />.</exception>
        public InputOutputCore(IIdentifiableObject parent, InputOutputType xmlType)
            : base(xmlType, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.InputOutput), parent)
        {
            if (xmlType == null)
            {
                throw new ArgumentNullException("xmlType");
            }

            this._localId = xmlType.localID;
            if (xmlType.ObjectReference != null)
            {
                this._structureReference = RefUtil.CreateReference(this, xmlType.ObjectReference);
            }

            this.Validate();
        }

        /// <summary>
        ///     Gets the local id.
        /// </summary>
        public virtual string LocalId
        {
            get
            {
                return this._localId;
            }
        }

        /// <summary>
        ///     Gets the structure reference.
        /// </summary>
        public virtual ICrossReference StructureReference
        {
            get
            {
                return this._structureReference;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IInputOutputObject)sdmxObject;
                if (!this.Equivalent(this._structureReference, that.StructureReference))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._localId, that.LocalId))
                {
                    return false;
                }

                return this.DeepEqualsInternalAnnotable(that, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void Validate()
        {
            if (this._structureReference == null)
            {
                throw new SdmxSemmanticException("Input / Output sdmxObject, requires an Object Reference");
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALiDATION                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS                             //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ////////////BUILD FROM MUTABLE OBJECTS             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
    }
}