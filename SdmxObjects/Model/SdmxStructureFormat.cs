﻿// -----------------------------------------------------------------------
// <copyright file="SdmxStructureFormat.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model
{
    using System;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;

    /// <summary>
    ///     SdmxStructureFormat class
    /// </summary>
    public class SdmxStructureFormat : AbstractSdmxStructureFormat, IStructureFormat
    {
        /// <summary>
        /// The output Encoding
        /// </summary>
        private readonly Encoding _outputEncoding;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxStructureFormat" /> class.
        /// </summary>
        /// <param name="structureFormat">The structure format.</param>
        /// <exception cref="ArgumentException">STRUCTURE_OUTPUT_FORMAT can not be null</exception>
        public SdmxStructureFormat(StructureOutputFormat structureFormat)
            : this(structureFormat, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SdmxStructureFormat" /> class.
        /// </summary>
        /// <param name="structureFormat">The structure format.</param>
        /// <param name="encoding">The encoding.</param>
        /// <exception cref="System.ArgumentException">STRUCTURE_OUTPUT_FORMAT can not be null</exception>
        /// <exception cref="ArgumentException">STRUCTURE_OUTPUT_FORMAT can not be null</exception>
        public SdmxStructureFormat(StructureOutputFormat structureFormat, Encoding encoding)
            : base(structureFormat)
        {
            this._outputEncoding = encoding ?? new UTF8Encoding(false);
        }

        /// <summary>
        /// Gets the output encoding.
        /// </summary>
        /// <value>
        /// The output encoding.
        /// </value>
        public Encoding OutputEncoding
        {
            get
            {
                return this._outputEncoding;
            }
        }
    }
}