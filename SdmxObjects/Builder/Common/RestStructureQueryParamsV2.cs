// -----------------------------------------------------------------------
// <copyright file="CommonStructureQueryCore.cs" company="EUROSTAT">
//   Date Created : 2022-05-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Builder.Common
{
    using System;
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Builder.Common;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Util;
    /// <summary>
    /// The query parameters for the REST structure requests v2.
    /// </summary>
    public class RestStructureQueryParamsV2 : RestStructureQueryParams
    {


        /// <summary>
        /// Builds a RestStructureQueryParamsV2
        /// </summary>
        /// <param name="structureOutputFormat"></param>
        /// <param name="queryString"></param>
        /// <exception cref="SdmxSemmanticException"></exception>
        public static RestStructureQueryParamsV2 Build(StructureOutputFormat structureOutputFormat, string queryString)
        {
           
            if (queryString.StartsWith("/"))
            {
                queryString = queryString.Substring(1);
            }

            Dictionary<string, string> queryParameters = new Dictionary<string, string>();

            //Parse any additional parameters
            if (queryString.IndexOf("?") > 1)
            {
                string parameters = queryString.Substring(queryString.IndexOf("?") + 1);
                queryString = queryString.Substring(0, queryString.IndexOf("?"));

                foreach (string currentParam in parameters.Split('&'))
                {
                    string[] param = currentParam.Split('=');
                    queryParameters.Add(param[0], param[1]);
                }
            }

            string[] args = queryString.Split('/');
            if (args.Length < 1)
            {
                throw new SdmxSemmanticException("Structure Query Expecting at least 1 parameter (structure type)");
            }

            var structure = args[0];
            string agencyID;
            if (args.Length >= 2)
            {
                agencyID = args[1];
            }
            else
            {
                agencyID = "*";
            }
            string resourceID;
            if (args.Length >= 3)
            {
                resourceID = args[2];
            }
            else
            {
                resourceID = "*";
            }

            string version;
            if (args.Length >= 4)
            {
                version = args[3];
            }
            else
            {
                version = "+";
            }
            string childRefs;
            if (args.Length >= 5 && (ObjectUtil.ValidString(args[4])))
            {
                childRefs = args[4];
            }
            else
            {
                childRefs = null;
            }

            return new RestStructureQueryParamsV2(structureOutputFormat, structure, agencyID, resourceID, version,childRefs, queryParameters);
        }
        /// <summary>
        /// Initializes a new <see cref="RestStructureQueryParamsV2"/>.
        /// </summary>
        /// <param name="structureOutputFormat"></param>
        /// <param name="structure"></param>
        /// <param name="agencyID"></param>
        /// <param name="resourceID"></param>
        /// <param name="version"></param>
        /// <param name="childRefs"></param>
        /// <param name="queryParameters"></param>
        public RestStructureQueryParamsV2(StructureOutputFormat structureOutputFormat, string structure, string agencyID, string resourceID, string version, string childRefs, Dictionary<string,string> queryParameters)
        :base(structureOutputFormat,structure,agencyID,resourceID,version, childRefs, queryParameters)
        {
            
        }

    }
}
