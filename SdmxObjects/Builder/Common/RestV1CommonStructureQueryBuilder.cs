// -----------------------------------------------------------------------
// <copyright file="RestV1CommonStructureQueryBuilder.cs" company="EUROSTAT">
//   Date Created : 2022-05-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Builder.Common
{
    using System;
    using System.Linq;
    using Org.Sdmxsource.Sdmx.Api.Builder.Common;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Util.Sdmx;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// Builds a <see cref="ICommonStructureQuery"/> from REST v1 request.
    /// </summary>
    public class RestV1CommonStructureQueryBuilder : RestAbstractCommonStructureQueryBuilder, ICommonStructureQueryBuilder<RestStructureQueryParams>
    {
        /// <inheritdoc/>
        public ICommonStructureQuery BuildCommonStructureQuery(RestStructureQueryParams queryParams)
        {
            var builder = CommonStructureQueryCore.Builder.NewQuery(CommonStructureQueryType.REST, queryParams.StructureOutputFormat);

            FillFromRestStructureQuery(queryParams, builder);

            return builder.Build();
        }

        private void FillFromRestStructureQuery(RestStructureQueryParams queryParams, CommonStructureQueryCore.Builder builder)
        {
            var structureType = GetStructureType(queryParams.StructureType);
            builder.SetMaintainableTarget(structureType);

            // CommonStructureQueryCore validates wildcards with the REST v2 api
            // they must be converted here
            // TODO - common structure query - better build a pattern for wildcard rules
            if (ObjectUtil.ValidString(queryParams.AgencyId))
            {
                builder.SetAgencyIds(ParseString(queryParams.AgencyId).Split('+'));
            }
            if (ObjectUtil.ValidString(queryParams.ResourceId))
            {
                builder.SetMaintainableIds(ParseString(queryParams.ResourceId).Split('+'));
            }
            if (ObjectUtil.ValidString(queryParams.Version))
            {
                builder.SetVersionRequests(ParseString(queryParams.Version).Split('+').Select(s => new VersionRequestCore(s)).ToArray<IVersionRequest>());
            }
            else
            {
                builder.SetVersionRequests(new VersionRequestCore(ParseString(queryParams.Version)));
            }

            if (ObjectUtil.ValidString(queryParams.SpecificItemIds))
            {
                var itemType = SdmxStructureTypeUtils.GetItemStructureType(structureType);
                var childReferences = queryParams.SpecificItemIds.Split('+')
                    .Select(id => ComplexIdentifiableReferenceCore.CreateForRest(id, itemType));
                builder.SetChildReferences(childReferences.ToArray());
            }

            ParseRESTQueryParameters(queryParams.QueryParameters, builder);
        }

        /// <summary>
        /// converts REST v1 wildcards to REST v2
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private string ParseString(string value)
        {
            if (value.Equals("all", StringComparison.OrdinalIgnoreCase))
            {
                return "*";
            }
            return value;
        }

        /// <summary>
        ///     The get structure type.
        /// </summary>
        /// <param name="str">The structure.</param>
        /// <returns>the SDMX structure type</returns>
        private static SdmxStructureType GetStructureType(string str)
        {
            if (str.Equals("structure", StringComparison.OrdinalIgnoreCase))
            {
                return SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Any);
            }

            if (str.Equals("organisationscheme", StringComparison.OrdinalIgnoreCase))
            {
                return SdmxStructureType.GetFromEnum(SdmxStructureEnumType.OrganisationScheme);
            }

            return SdmxStructureType.ParseClass(str);
        }
    }
}
