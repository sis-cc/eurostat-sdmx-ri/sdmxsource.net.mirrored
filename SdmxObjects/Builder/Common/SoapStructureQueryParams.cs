// -----------------------------------------------------------------------
// <copyright file="SoapStructureQueryParams.cs" company="EUROSTAT">
//   Date Created : 2022-05-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Builder.Common
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using Org.Sdmxsource.Sdmx.Api.Builder.Common;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;

    public class SoapStructureQueryParams : ICommonStructureQueryParams
    {
        private readonly StructureOutputFormat _structureOutputFormat;
        private readonly Stream _request;

        public SoapStructureQueryParams(Stream request, StructureOutputFormat structureOutputFormat)
        {
            if(request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            this._request = request;
            this._structureOutputFormat = structureOutputFormat;
        }

        public Stream Request()
        {
            return _request;
        }

        public StructureOutputFormat StructureOutputFormat()
        {
            return _structureOutputFormat;
        }
    }
}
