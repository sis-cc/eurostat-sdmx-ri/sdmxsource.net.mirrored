// -----------------------------------------------------------------------
// <copyright file="CommonStructureQueryCore.cs" company="EUROSTAT">
//   Date Created : 2022-05-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Builder.Common
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Builder.Common;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// The query parameters for the REST structure requests.
    /// These are the same for v1 and v2, because the request parts are the same. 
    /// The two versions differ in the builders, meaning the reading of the parameters.
    /// </summary>
    public class RestStructureQueryParams : ICommonStructureQueryParams
    {
        /// <summary>
        /// The requested format for the results
        /// </summary>
        // TODO - common structure query - this can be deleted; see ICommonStructureQuery
        public StructureOutputFormat StructureOutputFormat { get; private set; }

        /// <summary>
        /// The type of requested structures, e.g. Codelist
        /// </summary>
        public string StructureType { get; private set; }

        /// <summary>
        /// Filters by the agency id(s) owning the artefacts.
        /// </summary>
        public string AgencyId { get; private set; }

        /// <summary>
        /// Searches for specific structure(s) with this id.
        /// </summary>
        public string ResourceId { get; private set; }

        /// <summary>
        /// Filters by artefact version.
        /// </summary>
        public string Version { get; private set; }

        /// <summary>
        /// The specific child items (if applied) to be returned.
        /// </summary>
        public string SpecificItemIds { get; private set; }

        /// <summary>
        /// The structure query parameters like detail, references etc
        /// </summary>
        public IDictionary<string, string> QueryParameters { get; private set; }

        /// <summary>
        /// Initializes a new <see cref="RestStructureQueryParams"/>.
        /// </summary>
        /// <param name="structureOutputFormat"></param>
        /// <param name="structureType"></param>
        /// <param name="agencyId"></param>
        /// <param name="resourceId"></param>
        /// <param name="version"></param>
        /// <param name="specificItemIds"></param>
        /// <param name="queryParameters"></param>
        public RestStructureQueryParams(
            StructureOutputFormat structureOutputFormat,
            string structureType, string agencyId, string resourceId, string version, string specificItemIds,
            IDictionary<string, string> queryParameters)
        {
            if (structureType == null)
            {
                throw new ArgumentNullException(nameof(structureType));
            }

            StructureOutputFormat = structureOutputFormat;
            StructureType = structureType;
            AgencyId = agencyId;
            ResourceId = resourceId;
            Version = version;
            SpecificItemIds = specificItemIds;
            QueryParameters = queryParameters;
        }
    }
}
