// -----------------------------------------------------------------------
// <copyright file="RestAbstractCommonStructureQueryBuilder.cs" company="EUROSTAT">
//   Date Created : 2022-09-13
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Builder.Common
{
    using System;
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;

    /// <summary>
    /// Implements part of the common methods for REST v1, v2 CommonStructureQuery builders.
    /// </summary>
    public abstract class RestAbstractCommonStructureQueryBuilder
    {
        /// <summary>
        /// Parses the rest parameters like detail and references.
        /// </summary>
        /// <param name="restQueryParameters"></param>
        /// <param name="builder"></param>
        /// <exception cref="SdmxSemmanticException"></exception>
        protected void ParseRESTQueryParameters(IDictionary<string, string> restQueryParameters, CommonStructureQueryCore.Builder builder)
        {
            // set default values
            var queryDetail = StructureQueryDetail.GetFromEnum(StructureQueryDetailEnumType.Full);
            var referenceDetail = StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.None);

            foreach (var keyPair in restQueryParameters)
            {
                string key = keyPair.Key;
                string value = keyPair.Value;
                if (key.Equals("detail", StringComparison.OrdinalIgnoreCase))
                {
                    queryDetail = StructureQueryDetail.ParseString(value);

                    if (queryDetail == null)
                    {
                        throw new SdmxSemmanticException("unable to parse value for key " + key);
                    }
                }
                else if (key.Equals("references", StringComparison.OrdinalIgnoreCase))
                {
                    try
                    {
                        referenceDetail = StructureReferenceDetail.ParseString(value);

                        if (referenceDetail.EnumType == StructureReferenceDetailEnumType.Specific)
                        {
                            //TODO could the value contain multiple comma separated structures?
                            //i.e. codelist, dataflow?
                            builder.SetSpecificReferences(SdmxStructureType.ParseClass(value));
                        }
                    }
                    catch (SdmxSemmanticException e)
                    {
                        throw new SdmxSemmanticException("unable to parse value for key " + key, e);
                    }
                }
                else
                {
                    throw new SdmxSemmanticException("Unknown query parameter : " + key);
                }
            }

            builder.SetRequestedDetail(GetDetailForRequestedArtefacts(queryDetail));
            builder.SetReferencedDetail(GetDetailForReferencedArtefacts(queryDetail));
            builder.SetReferences(referenceDetail);
        }

        /// <summary>
        /// Convertion for the requested detail
        /// </summary>
        /// <param name="detail"></param>
        /// <returns></returns>
        protected ComplexStructureQueryDetail GetDetailForRequestedArtefacts(StructureQueryDetail detail)
        {
            switch (detail.EnumType)
            {
                case StructureQueryDetailEnumType.AllCompleteStubs:
                    return ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.CompleteStub);
                case StructureQueryDetailEnumType.AllStubs:
                    return ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Stub);
                default:
                    return ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full);
            }
        }

        /// <summary>
        /// Conversion for the reference detail
        /// </summary>
        /// <param name="detail"></param>
        /// <returns></returns>
        protected ComplexMaintainableQueryDetail GetDetailForReferencedArtefacts(StructureQueryDetail detail)
        {
            switch (detail.EnumType)
            {
                case StructureQueryDetailEnumType.AllCompleteStubs:
                    return ComplexMaintainableQueryDetail.GetFromEnum(ComplexMaintainableQueryDetailEnumType.CompleteStub);
                case StructureQueryDetailEnumType.AllStubs:
                case StructureQueryDetailEnumType.ReferencedStubs:
                    return ComplexMaintainableQueryDetail.GetFromEnum(ComplexMaintainableQueryDetailEnumType.Stub);
                case StructureQueryDetailEnumType.ReferencePartial:
                    return ComplexMaintainableQueryDetail.GetFromEnum(ComplexMaintainableQueryDetailEnumType.ReferencePartial);
                default:
                    return ComplexMaintainableQueryDetail.GetFromEnum(ComplexMaintainableQueryDetailEnumType.Full);
            }
        }
    }
}
