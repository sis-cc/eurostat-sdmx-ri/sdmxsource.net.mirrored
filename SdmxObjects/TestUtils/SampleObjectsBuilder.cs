// -----------------------------------------------------------------------
// <copyright file="SampleObjectsBuilder.cs" company="EUROSTAT">
//   Date Created : 2022-02-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjectsTests.
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.TestUtils
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// Creates sample ojects used by many tests
    /// </summary>
    public class SampleObjectsBuilder
    {
        public static readonly string TEST_AGENCY_ID = "EXAMPLE";
        public static readonly string TEST_CONCEPT_SCHEME_ID = "TEST_CONCEPT_SCHEME";
        public static readonly string TEST_CONCEPT_ID = "TEST_CONCEPT";
        public static readonly string TEST_CODELIST_ID = "CL_TEST";
        public static readonly string TEST_VERSION = "1.0.0";

        public static IDataStructureMutableObject BuildDataStructureMutableObject()
        {
            IDataStructureMutableObject dsd = new DataStructureMutableCore();
            // Mandatory information
            // agency
            dsd.AgencyId = TEST_AGENCY_ID;
            // id
            dsd.Id = "TEST_ID";
            // a name
            dsd.AddName("en", "Mandatory name");
            // a dimension
            dsd.AddDimension(
                new StructureReferenceImpl(TEST_AGENCY_ID, TEST_CONCEPT_SCHEME_ID, TEST_VERSION, SdmxStructureEnumType.Concept, TEST_CONCEPT_ID),
                new StructureReferenceImpl(TEST_AGENCY_ID, TEST_CODELIST_ID, TEST_VERSION, SdmxStructureEnumType.CodeList)
            );
            return dsd;
        }

        public static IAttributeMutableObject BuildAttributeMutableObject()
        {
            IAttributeMutableObject unit = new AttributeMutableCore();
            unit.Id = "UNIT";
            unit.AttachmentLevel = AttributeAttachmentLevel.DataSet;
            unit.ConceptRef = new StructureReferenceImpl(TEST_AGENCY_ID, TEST_CONCEPT_SCHEME_ID, TEST_VERSION, SdmxStructureEnumType.Concept, "UNIT");
            return unit;
        }

        public static IRepresentationMutableObject BuildMutableRepresentationObjectWithTextFormat()
        {
            IRepresentationMutableObject representationMutableObject = new RepresentationMutableCore();
            ITextFormatMutableObject textFormat = new TextFormatMutableCore();
            textFormat.TextType = TextType.GetFromEnum(TextEnumType.String);
            representationMutableObject.TextFormat = textFormat;

            return representationMutableObject;
        }

        public static IRepresentationMutableObject BuildMutableRepresentationObjectWithCodelist()
        {
            IRepresentationMutableObject representationMutableObject = new RepresentationMutableCore();
            IStructureReference codelistReference = new StructureReferenceImpl(TEST_AGENCY_ID, TEST_CODELIST_ID, TEST_VERSION, SdmxStructureEnumType.CodeList);
            representationMutableObject.Representation = codelistReference;

            return representationMutableObject;
        }

        public static ICodelistMutableObject BuildCodelistMutableObject()
        {
            return BuildCodelistMutableObject(TEST_CODELIST_ID);
        }

        private static ICodelistMutableObject BuildCodelistMutableObject(string id)
        {
            ICodelistMutableObject codelist = new CodelistMutableCore();
            codelist.Id = id;
            codelist.AgencyId = TEST_AGENCY_ID;
            codelist.Version = TEST_VERSION;
            codelist.AddName("en", "example " + id);

            AddCodeToCodelist(codelist, "A");
            AddCodeToCodelist(codelist, "B");

            return codelist;
        }

        public static void AddCodeToCodelist(ICodelistMutableObject codelist, string codeId)
        {
            ICodeMutableObject code = new CodeMutableCore();
            code.Id = codeId;
            code.AddName("en", string.Format("code %1$s from %2$s", codeId, codelist.Id));
            codelist.AddItem(code);
        }

        public static IConceptSchemeMutableObject BuildConceptSchemeMutableObject()
        {
            IConceptSchemeMutableObject conceptSchemeMutableObject = new ConceptSchemeMutableCore();

            conceptSchemeMutableObject.AgencyId = TEST_AGENCY_ID;
            conceptSchemeMutableObject.Id = TEST_CONCEPT_SCHEME_ID;
            conceptSchemeMutableObject.Version = TEST_VERSION;
            conceptSchemeMutableObject.AddName("en", "General Statistics");

            IConceptMutableObject concept = conceptSchemeMutableObject.CreateItem(TEST_CONCEPT_ID, "Con name");

            return conceptSchemeMutableObject;
        }
    }
}
