// -----------------------------------------------------------------------
// <copyright file="CodeListUtil.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
using System;
using Org.Sdmxsource.Sdmx.Api.Constants;

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Util
{
    public static class QueryUtil 
    {
        public static ComplexStructureQueryDetail GetDetailForRequestedArtefacts(this StructureQueryDetail detail)
        {
            if (detail is null) 
            {
                return null;
            }

            switch(detail.EnumType) 
            {
                case StructureQueryDetailEnumType.AllCompleteStubs:
                    return ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.CompleteStub);
                case StructureQueryDetailEnumType.AllStubs:
                    return ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Stub);
                default:
                    return ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full);
            }
        }
        public static ComplexMaintainableQueryDetail GetDetailForReferencedArtefacts(this StructureQueryDetail detail)
        {
            if (detail is null) 
            {
                return null;
            }

            switch(detail.EnumType) 
            {
                case StructureQueryDetailEnumType.AllCompleteStubs:
                    return ComplexMaintainableQueryDetail.GetFromEnum(ComplexMaintainableQueryDetailEnumType.CompleteStub);
                case StructureQueryDetailEnumType.AllStubs:
                case StructureQueryDetailEnumType.ReferencedStubs:
                    return ComplexMaintainableQueryDetail.GetFromEnum(ComplexMaintainableQueryDetailEnumType.Stub);
                case StructureQueryDetailEnumType.ReferencePartial:
                    return ComplexMaintainableQueryDetail.GetFromEnum(ComplexMaintainableQueryDetailEnumType.ReferencePartial);
                default:
                    return ComplexMaintainableQueryDetail.GetFromEnum(ComplexMaintainableQueryDetailEnumType.Full);
            }
        }

        internal static SdmxStructureType GetStructure(string structureType)
        {
            SdmxStructureEnumType targetStructure;

            if (structureType.Equals("structure"))
            {
                targetStructure = SdmxStructureEnumType.Any;
            }
            else if (structureType.Equals("organisationscheme"))
            {
                targetStructure = SdmxStructureEnumType.OrganisationScheme;
            }
            else 
            { 
                targetStructure = SdmxStructureType.ParseClass(structureType); 
            }

            return SdmxStructureType.GetFromEnum(targetStructure);
        }
    }
}