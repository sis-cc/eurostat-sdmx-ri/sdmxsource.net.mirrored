#!/bin/bash

set -e
set -u
work="${1:-workdir}"
package="${2:-workdir/package}"
scratch="$work/scratch"
mkdir "$work"
git archive -o "$work/src.tar.gz" HEAD
if [[ -f .gitmodules ]]; then
    git submodule foreach --quiet 'git archive --prefix=\$path/ --format=tar.gz -o \$toplevel/src.\$name.tar.gz HEAD'
    if compgen -G 'src.*.tar.gz' > /dev/null; then 
        cat src.*.tar.gz >> "$work/src.tar.gz"
        rm src.*.tar.gz 
    fi
fi

# Usually we package only maws nsiws and sdmxsource.net
#if [[ ! -f CHANGELOG.md ]]; then
#   echo "No $PWD/CHANGELOG.md found, Exiting"
#    exit
#fi

mkdir -p "$package"/src

if [[ -d Tool ]]; then
    mv Tool "$package/"
fi

if [[ -d lib ]]; then
    mv lib "$package/"
fi

if [[ -d app ]]; then
    mv app "$package"/
fi
url=$(git config --get remote.origin.url)
if [[ -z "$url" ]]; then
    echo "Cannot determine the git url" >&2
    exit -1
fi

name=${url##*/}
if [[ -z "$name" ]]; then
    echo "Cannot determine the name" >&2
    exit -2
fi
mkdir -p "$package"/src/$name
# The i is important
tar -xivf "$work/src.tar.gz" -C "$package"/src/$name/

# requires npm
mkdir -p $scratch/{input,output}
cp -l *.md $scratch/input
if [[ -d doc ]]; then
    cp -R doc $scratch/input
fi 
cd $scratch
generate-md --layout mixu-bootstrap-2col
cd -
mv $scratch/output/* "$package"/

cp licence* "$package"/
