#!/bin/bash

set -e
set -u

if [[ $# != 3 ]]; then
    echo "In case old and new commit do not match tag and commit"
    echo "USAGE: $0 <old commit hash> <new commit hash> <tag>"
    exit -1
fi

OLD_COMMIT=$1
NEW_COMMIT=$2
NEW_VERSION=$3

if [[ "x$OLD_COMMIT" != "x$NEW_COMMIT" ]]; then
    git tag -a "rel-$NEW_VERSION" -m"Jenkins automated build"
    git push --follow-tags
    if [[ $NEW_VERSION != *-* ]]; then
      git checkout -b "$NEW_VERSION"
      git push -u origin "$NEW_VERSION"
      if [[ $NEW_VERSION == *.*.0 ]]; then
        git checkout master
        git pull
        git merge "$NEW_VERSION"
        git push
      fi
    fi
fi
