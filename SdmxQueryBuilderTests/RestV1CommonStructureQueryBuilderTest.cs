// -----------------------------------------------------------------------
// <copyright file="RestV1CommonStructureQueryBuilderTest.cs" company="EUROSTAT">
//   Date Created : 2022-05-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxQueryBuilderTests
{
    using System.Collections.Generic;
    using NUnit.Framework;
    using Org.Sdmxsource.Util.Extensions;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Builder.Common;
    using Org.Sdmxsource.Sdmx.Api.Builder.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;

    [TestFixture]
    public class RestV1CommonStructureQueryBuilderTest
    {
        [Test]
        public void TestStructureWithExplicitDefaults()
        {
            // Given
            string structure = "categorisation";
            string agencyID = "all";
            string resourceID = "all";
            string version = "latest";
            string detail = "full";
            string references = "none";
            StructureOutputFormat structureOutputFormat = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV3StructureDocument);

            // When
            var queryParams = CreateQueryParameters(detail, references);

            var commonStructureQueryBuilder = new RestV1CommonStructureQueryBuilder();
            RestStructureQueryParams restStructureQueryParams = new RestStructureQueryParams(structureOutputFormat,structure, agencyID, resourceID, version, null, queryParams);

            ICommonStructureQuery commonStructureQuery = commonStructureQueryBuilder.BuildCommonStructureQuery(restStructureQueryParams);

            // Then
            Assert.AreEqual(CommonStructureQueryType.REST, commonStructureQuery.QueryType);
            Assert.AreEqual(StructureOutputFormatEnumType.SdmxV3StructureDocument, commonStructureQuery.StructureOutputFormat.EnumType);
            Assert.IsFalse(commonStructureQuery.HasSpecificAgencyId);
            Assert.AreEqual("*", commonStructureQuery.AgencyIds[0]);
            Assert.IsFalse(commonStructureQuery.HasSpecificMaintainableId);
            Assert.AreEqual("*", commonStructureQuery.MaintainableIds[0]);
            Assert.AreEqual(ComplexStructureQueryDetailEnumType.Full, commonStructureQuery.RequestedDetail.EnumType);
            Assert.AreEqual(ComplexMaintainableQueryDetailEnumType.Full, commonStructureQuery.ReferencedDetail.EnumType);
            Assert.AreEqual(StructureReferenceDetailEnumType.None, commonStructureQuery.References.EnumType);
        }

        [Test]
        public void TestSpecificStructure()
        {
            // Given
            string structure = "datastructure";
            string agencyID = "IT1";
            string resourceID = "DDB";
            string version = "1.0";
            string childRefs = "TEST";
            string detail = null;
            string references = null;
            StructureOutputFormat structureOutputFormat = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument);

            // When
            var queryParams = CreateQueryParameters(detail, references);

            var commonStructureQueryBuilder = new RestV1CommonStructureQueryBuilder();
            RestStructureQueryParams restStructureQueryParams = new RestStructureQueryParams(structureOutputFormat, structure, agencyID, resourceID, version, childRefs, queryParams);

            ICommonStructureQuery commonStructureQuery = commonStructureQueryBuilder.BuildCommonStructureQuery(restStructureQueryParams);

            // Then
            Assert.AreEqual(SdmxStructureEnumType.Dsd, commonStructureQuery.MaintainableTarget.EnumType);
            Assert.IsTrue(commonStructureQuery.HasSpecificAgencyId);
            Assert.AreEqual("IT1", commonStructureQuery.AgencyIds[0]);
            Assert.IsTrue(commonStructureQuery.HasSpecificMaintainableId);
            Assert.AreEqual("DDB", commonStructureQuery.MaintainableIds[0]);
            Assert.AreEqual("TEST", commonStructureQuery.SpecificItems[0].Id.SearchParameter);
            Assert.AreEqual("1.0", commonStructureQuery.VersionRequests[0].ToString());
            Assert.AreEqual(CascadeSelection.False, commonStructureQuery.ItemCascade);
        }

        // Testing all possible detail values
        // (full, allstubs, referencestubs, referencepartial, allcompletestubs)
        [Test]
        public void TestDetailAllStubs()
        {
            // Given
            string structure = "datastructure";
            string agencyID = "all";
            string resourceID = "all";
            string version = "latest";
            string childRefs = null;
            string detail = "allstubs";
            string references = null;
            StructureOutputFormat structureOutputFormat = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.Json);

            // When
            var queryParams = CreateQueryParameters(detail, references);

            var commonStructureQueryBuilder = new RestV1CommonStructureQueryBuilder();
            RestStructureQueryParams restStructureQueryParams = new RestStructureQueryParams(structureOutputFormat, structure, agencyID, resourceID, version, childRefs, queryParams);

            ICommonStructureQuery commonStructureQuery = commonStructureQueryBuilder.BuildCommonStructureQuery(restStructureQueryParams);

            // Then
            Assert.AreEqual(ComplexStructureQueryDetailEnumType.Stub, commonStructureQuery.RequestedDetail.EnumType);
            Assert.AreEqual(ComplexMaintainableQueryDetailEnumType.Stub, commonStructureQuery.ReferencedDetail.EnumType);
        }

        [Test]
        public void TestDetailReferenceStubs()
        {
            // Given
            string structure = "datastructure";
            string agencyID = "all";
            string resourceID = "all";
            string version = "latest";
            string childRefs = null;
            string detail = "referencestubs";
            string references = null;
            StructureOutputFormat structureOutputFormat = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.Json);

            // When
            var queryParams = CreateQueryParameters(detail, references);

            var commonStructureQueryBuilder = new RestV1CommonStructureQueryBuilder();
            RestStructureQueryParams restStructureQueryParams = new RestStructureQueryParams(structureOutputFormat, structure, agencyID, resourceID, version, childRefs, queryParams);

            ICommonStructureQuery commonStructureQuery = commonStructureQueryBuilder.BuildCommonStructureQuery(restStructureQueryParams);

            // Then
            Assert.AreEqual(ComplexStructureQueryDetailEnumType.Full, commonStructureQuery.RequestedDetail.EnumType);
            Assert.AreEqual(ComplexMaintainableQueryDetailEnumType.Stub, commonStructureQuery.ReferencedDetail.EnumType);
        }

        [Test]
        public void TestDetailReferencePartial()
        {
            // Given
            string structure = "datastructure";
            string agencyID = "all";
            string resourceID = "all";
            string version = "latest";
            string childRefs = null;
            string detail = "referencepartial";
            string references = null;
            StructureOutputFormat structureOutputFormat = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.Json);

            // When
            var queryParams = CreateQueryParameters(detail, references);

            var commonStructureQueryBuilder = new RestV1CommonStructureQueryBuilder();
            RestStructureQueryParams restStructureQueryParams = new RestStructureQueryParams(structureOutputFormat, structure, agencyID, resourceID, version, childRefs, queryParams);

            ICommonStructureQuery commonStructureQuery = commonStructureQueryBuilder.BuildCommonStructureQuery(restStructureQueryParams);

            // Then
            Assert.AreEqual(ComplexStructureQueryDetailEnumType.Full, commonStructureQuery.RequestedDetail.EnumType);
            Assert.AreEqual(ComplexMaintainableQueryDetailEnumType.ReferencePartial, commonStructureQuery.ReferencedDetail.EnumType);
        }

        [Test]
        public void TestDetailAllCompleteStubs()
        {
            // Given
            string structure = "datastructure";
            string agencyID = "all";
            string resourceID = "all";
            string version = "latest";
            string childRefs = null;
            string detail = "allcompletestubs";
            string references = null;
            StructureOutputFormat structureOutputFormat = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.Json);

            // When
            var queryParams = CreateQueryParameters(detail, references);

            var commonStructureQueryBuilder = new RestV1CommonStructureQueryBuilder();
            RestStructureQueryParams restStructureQueryParams = new RestStructureQueryParams(structureOutputFormat, structure, agencyID, resourceID, version, childRefs, queryParams);

            ICommonStructureQuery commonStructureQuery = commonStructureQueryBuilder.BuildCommonStructureQuery(restStructureQueryParams);

            // Then
            Assert.AreEqual(ComplexStructureQueryDetailEnumType.CompleteStub, commonStructureQuery.RequestedDetail.EnumType);
            Assert.AreEqual(ComplexMaintainableQueryDetailEnumType.CompleteStub, commonStructureQuery.ReferencedDetail.EnumType);
        }

        [Test]
        public void TestReferencesParents()
        {
            // Given
            string structure = "datastructure";
            string agencyID = "all";
            string resourceID = "all";
            string version = "latest";
            string childRefs = null;
            string detail = null;
            string references = "parents";
            StructureOutputFormat structureOutputFormat = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument);

            // When
            var queryParams = CreateQueryParameters(detail, references);

            var commonStructureQueryBuilder = new RestV1CommonStructureQueryBuilder();
            RestStructureQueryParams restStructureQueryParams = new RestStructureQueryParams(structureOutputFormat, structure, agencyID, resourceID, version, childRefs, queryParams);

            ICommonStructureQuery commonStructureQuery = commonStructureQueryBuilder.BuildCommonStructureQuery(restStructureQueryParams);

            // Then
            Assert.AreEqual(0, commonStructureQuery.SpecificReferences.Count);
            Assert.AreEqual(StructureReferenceDetailEnumType.Parents, commonStructureQuery.References.EnumType);
        }

        [Test]
        public void TestReferencesParentsAndSiblings()
        {
            // Given
            string structure = "datastructure";
            string agencyID = "all";
            string resourceID = "all";
            string version = "latest";
            string childRefs = null;
            string detail = null;
            string references = "parentsandsiblings";
            StructureOutputFormat structureOutputFormat = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument);

            // When
            var queryParams = CreateQueryParameters(detail, references);

            var commonStructureQueryBuilder = new RestV1CommonStructureQueryBuilder();
            RestStructureQueryParams restStructureQueryParams = new RestStructureQueryParams(structureOutputFormat, structure, agencyID, resourceID, version, childRefs, queryParams);

            ICommonStructureQuery commonStructureQuery = commonStructureQueryBuilder.BuildCommonStructureQuery(restStructureQueryParams);

            // Then
            Assert.AreEqual(0, commonStructureQuery.SpecificReferences.Count);
            Assert.AreEqual(StructureReferenceDetailEnumType.ParentsSiblings, commonStructureQuery.References.EnumType);
        }

        [Test]
        public void TestReferencesChildren()
        {
            // Given
            string structure = "datastructure";
            string agencyID = "all";
            string resourceID = "all";
            string version = "latest";
            string childRefs = null;
            string detail = null;
            string references = "children";
            StructureOutputFormat structureOutputFormat = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument);

            // When
            var queryParams = CreateQueryParameters(detail, references);

            var commonStructureQueryBuilder = new RestV1CommonStructureQueryBuilder();
            RestStructureQueryParams restStructureQueryParams = new RestStructureQueryParams(structureOutputFormat, structure, agencyID, resourceID, version, childRefs, queryParams);

            ICommonStructureQuery commonStructureQuery = commonStructureQueryBuilder.BuildCommonStructureQuery(restStructureQueryParams);

            // Then
            Assert.AreEqual(0, commonStructureQuery.SpecificReferences.Count);
            Assert.AreEqual(StructureReferenceDetailEnumType.Children, commonStructureQuery.References.EnumType);
        }

        [Test]
        public void TestReferencesDescendants()
        {
            // Given
            string structure = "datastructure";
            string agencyID = "all";
            string resourceID = "all";
            string version = "latest";
            string childRefs = null;
            string detail = null;
            string references = "descendants";
            StructureOutputFormat structureOutputFormat = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument);

            // When
            var queryParams = CreateQueryParameters(detail, references);

            var commonStructureQueryBuilder = new RestV1CommonStructureQueryBuilder();
            RestStructureQueryParams restStructureQueryParams = new RestStructureQueryParams(structureOutputFormat, structure, agencyID, resourceID, version, childRefs, queryParams);

            ICommonStructureQuery commonStructureQuery = commonStructureQueryBuilder.BuildCommonStructureQuery(restStructureQueryParams);

            // Then
            Assert.AreEqual(0, commonStructureQuery.SpecificReferences.Count);
            Assert.AreEqual(StructureReferenceDetailEnumType.Descendants, commonStructureQuery.References.EnumType);
        }

        [Test]
        public void TestReferencesAll()
        {
            // Given
            string structure = "datastructure";
            string agencyID = "all";
            string resourceID = "all";
            string version = "latest";
            string childRefs = null;
            string detail = null;
            string references = "all";
            StructureOutputFormat structureOutputFormat = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument);

            // When
            var queryParams = CreateQueryParameters(detail, references);

            var commonStructureQueryBuilder = new RestV1CommonStructureQueryBuilder();
            RestStructureQueryParams restStructureQueryParams = new RestStructureQueryParams(structureOutputFormat, structure, agencyID, resourceID, version, childRefs, queryParams);

            ICommonStructureQuery commonStructureQuery = commonStructureQueryBuilder.BuildCommonStructureQuery(restStructureQueryParams);

            // Then
            Assert.AreEqual(0, commonStructureQuery.SpecificReferences.Count);
            Assert.AreEqual(StructureReferenceDetailEnumType.All, commonStructureQuery.References.EnumType);
        }

        [Test]
        public void TestReferencesSpecificResource()
        {
            // Given
            string structure = "datastructure";
            string agencyID = "all";
            string resourceID = "all";
            string version = "latest";
            string childRefs = null;
            string detail = null;
            string references = "codelist";
            StructureOutputFormat structureOutputFormat = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument);

            // When
            var queryParams = CreateQueryParameters(detail, references);

            var commonStructureQueryBuilder = new RestV1CommonStructureQueryBuilder();
            RestStructureQueryParams restStructureQueryParams = new RestStructureQueryParams(structureOutputFormat, structure, agencyID, resourceID, version, childRefs, queryParams);
            ICommonStructureQuery commonStructureQuery = commonStructureQueryBuilder.BuildCommonStructureQuery(restStructureQueryParams);

            // Then
            Assert.AreEqual(SdmxStructureEnumType.CodeList, commonStructureQuery.SpecificReferences[0].EnumType);
            Assert.AreEqual(StructureReferenceDetailEnumType.Specific, commonStructureQuery.References.EnumType);
        }

        [Test]
        public void TestMultiples()
        {
            // Given
            string structure = "categoryscheme";
            string agencyID = "AID1+AID2";
            string resourceID = "RID1+RID2";
            string version = "1.0.0+1.1.0";
            string childRefs = "CHR1+CHR2";
            string detail = null;
            string references = "none";
            StructureOutputFormat structureOutputFormat = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.Json);

            // When
            var queryParams = CreateQueryParameters(detail, references);

            ICommonStructureQueryBuilder<RestStructureQueryParams> commonStructureQueryBuilder = new RestV1CommonStructureQueryBuilder();
            RestStructureQueryParams restStructureQueryParams = new RestStructureQueryParams(structureOutputFormat, structure, agencyID, resourceID, version, childRefs, queryParams);

            ICommonStructureQuery commonStructureQuery = commonStructureQueryBuilder.BuildCommonStructureQuery(restStructureQueryParams);

            IList<string> agencyIds = new List<string>() { "AID1", "AID2" };
            IList<string> maintainableIds = new List<string>() { "RID1", "RID2" };
            //List<ComplexIdentifiableReferenceBean> childReferences = new ArrayList<>();
            //childReferences.Add(ComplexIdentifiableReferenceBeanImpl.CreateForRest("CHR1", SdmxStructureType.Category));
            //childReferences.Add(ComplexIdentifiableReferenceBeanImpl.CreateForRest("CHR2", SdmxStructureType.Category));

            // Then
            Assert.IsTrue(commonStructureQuery.HasSpecificAgencyId);
            Assert.AreEqual(2, commonStructureQuery.AgencyIds.Count);
            Assert.IsTrue(commonStructureQuery.AgencyIds.ContainsAll(agencyIds));
            Assert.IsTrue(commonStructureQuery.HasSpecificMaintainableId);
            Assert.AreEqual(2, commonStructureQuery.MaintainableIds.Count);
            Assert.IsTrue(commonStructureQuery.MaintainableIds.ContainsAll(maintainableIds));
            Assert.AreEqual(2, commonStructureQuery.SpecificItems.Count);
            //Assert.IsTrue(commonStructureQuery.SpecificItems.ContainsAll(childReferences));
            Assert.AreEqual(2, commonStructureQuery.VersionRequests.Count);
            Assert.IsTrue(commonStructureQuery.VersionRequests[0].ToString().Equals("1.0.0"));
            Assert.IsTrue(commonStructureQuery.VersionRequests[1].ToString().Equals("1.1.0"));
            Assert.AreEqual("CHR1", commonStructureQuery.SpecificItems[0].Id.SearchParameter);
            Assert.AreEqual("CHR2", commonStructureQuery.SpecificItems[1].Id.SearchParameter);               
        }

        private Dictionary<string, string> CreateQueryParameters(string detail, string references)
        {
            Dictionary<string, string> queryParameters = new Dictionary<string, string>();

            // detail and references are sent only from GET methods. PUT and DELETE methods do not send them
            if (detail != null)
            {
                queryParameters.Put("detail", detail);
            }
            if (references != null)
            {
                queryParameters.Put("references", references);
            }

            return queryParameters;
        }
    }
}
