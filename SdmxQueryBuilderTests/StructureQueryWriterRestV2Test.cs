using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.SdmxObjects.Builder.Common;
using Org.Sdmxsource.Sdmx.SdmxQueryBuilder.Builder;

namespace SdmxQueryBuilderTests
{
    [TestFixture]
    public class StructureQueryWriterRestV2Test
    {
       [TestCase("structure", "structure/*/*/*/+/?references=none&detail=full")]
       [TestCase("structure/*", "structure/*/*/*/+/?references=none&detail=full")]
       [TestCase("structure/*/*", "structure/*/*/*/+/?references=none&detail=full")]
       [TestCase("structure/*/*/*", "structure/*/*/*/*/?references=none&detail=full")]
       [TestCase("codelist/BIS/CL_FREQ/1.0/A", "structure/codelist/BIS/CL_FREQ/1.0/A/?references=none&detail=full")]
       [TestCase("codelist/BIS/CL_FREQ/1.0/A?references=descendants", "structure/codelist/BIS/CL_FREQ/1.0/A/?references=descendants&detail=full")]
       [TestCase("codelist/BIS/CL_FREQ/1.0/A?references=descendants&detail=allStubs", "structure/codelist/BIS/CL_FREQ/1.0/A/?references=descendants&detail=allstubs")]
       public void TestStructureQueryWriter(string queryString, string expected)
       {
           RestStructureQueryParamsV2 queryParamsV2 = RestStructureQueryParamsV2.Build(StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV3StructureDocument), queryString);
           ICommonStructureQuery restQuery = new RestV2CommonStructureQueryBuilder().BuildCommonStructureQuery(queryParamsV2);
           StringBuilder builder = new StringBuilder();
           RestStructureQueryWriterV2 queryWriterRestV2 = new RestStructureQueryWriterV2(builder);
           queryWriterRestV2.WriteStructureQuery(restQuery);
           Assert.AreEqual(expected, builder.ToString());
       }
    }
}
