// -----------------------------------------------------------------------
// <copyright file="RestV2CommonStructureQueryBuilderTest.cs" company="EUROSTAT">
//   Date Created : 2022-09-14
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxQueryBuilderTests
{
    using System.Collections.Generic;
    using NUnit.Framework;
    using Org.Sdmxsource.Util.Extensions;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Builder.Common;
    using Org.Sdmxsource.Sdmx.Api.Builder.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using System.Linq;

    [TestFixture]
    public class RestV2CommonStructureQueryBuilderTest
    {
        [Test]
        public void TestStructureWithExplicitDefaults()
        {
            // Given
            string structure = "categorisation";
            string agencyID = "*";
            string resourceID = "*";
            string version = "~";
            string detail = "full";
            string references = "none";
            StructureOutputFormat structureOutputFormat = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV3StructureDocument);

            // When
            ICommonStructureQuery commonStructureQuery = CreateCommonStructureQuery(
                structureOutputFormat,
                structure, agencyID, resourceID, version, null,
                detail, references);

            // Then
            Assert.AreEqual(CommonStructureQueryType.REST, commonStructureQuery.QueryType);
            Assert.AreEqual(StructureOutputFormatEnumType.SdmxV3StructureDocument, commonStructureQuery.StructureOutputFormat.EnumType);
            Assert.IsFalse(commonStructureQuery.HasSpecificAgencyId);
            Assert.AreEqual("*", commonStructureQuery.AgencyIds[0]);
            Assert.IsFalse(commonStructureQuery.HasSpecificMaintainableId);
            Assert.AreEqual("*", commonStructureQuery.MaintainableIds[0]);
            Assert.AreEqual(ComplexStructureQueryDetailEnumType.Full, commonStructureQuery.RequestedDetail.EnumType);
            Assert.AreEqual(ComplexMaintainableQueryDetailEnumType.Full, commonStructureQuery.ReferencedDetail.EnumType);
            Assert.AreEqual(StructureReferenceDetailEnumType.None, commonStructureQuery.References.EnumType);
        }

        [Test]
        public void TestSpecificStructure()
        {
            // Given
            string structure = "datastructure";
            string agencyID = "IT1";
            string resourceID = "DDB";
            string version = "1.0";
            string childRefs = "TEST";
            string detail = null;
            string references = null;
            StructureOutputFormat structureOutputFormat = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument);

            // When
            ICommonStructureQuery commonStructureQuery = CreateCommonStructureQuery(
                structureOutputFormat,
                structure, agencyID, resourceID, version, childRefs,
                detail, references);

            // Then
            Assert.AreEqual(SdmxStructureEnumType.Dsd, commonStructureQuery.MaintainableTarget.EnumType);
            Assert.IsTrue(commonStructureQuery.HasSpecificAgencyId);
            Assert.AreEqual("IT1", commonStructureQuery.AgencyIds[0]);
            Assert.IsTrue(commonStructureQuery.HasSpecificMaintainableId);
            Assert.AreEqual("DDB", commonStructureQuery.MaintainableIds[0]);
            Assert.AreEqual("TEST", commonStructureQuery.SpecificItems[0].Id.SearchParameter);
            Assert.AreEqual("1.0", commonStructureQuery.VersionRequests[0].ToString());
            Assert.AreEqual(CascadeSelection.False, commonStructureQuery.ItemCascade);
        }

        // semantic versioning is tested in VersionTests.cs
        // here the wildcard for REST v2 is tested
        [TestCase("*", VersionQueryTypeEnum.All)]
        [TestCase("~", VersionQueryTypeEnum.Latest)]
        [TestCase("+", VersionQueryTypeEnum.LatestStable)]
        [TestCase(null, VersionQueryTypeEnum.Latest)]
        public void TestVersionWildcards(string wildcard, VersionQueryTypeEnum expectedQueryType)
        {
            // Given
            string structure = "datastructure";
            string agencyID = "*";
            string resourceID = "*";
            string childRefs = null;
            string detail = null;
            string references = null;
            StructureOutputFormat structureOutputFormat = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.Json);

            // When
            ICommonStructureQuery commonStructureQuery = CreateCommonStructureQuery(
                structureOutputFormat,
                structure, agencyID, resourceID, wildcard, childRefs,
                detail, references);

            // Then
            Assert.AreEqual(1, commonStructureQuery.VersionRequests.Count);
            Assert.IsFalse(commonStructureQuery.VersionRequests.First().SpecifiesVersion);
            Assert.AreEqual(expectedQueryType, commonStructureQuery.VersionRequests.First().VersionQueryType);
        }

        [Test]
        public void TestMultipleVersions()
        {
            // Given
            string structure = "datastructure";
            string agencyID = "*";
            string resourceID = "*";
            string version = "1.0.0-ext,2.1+.7)";
            string childRefs = null;
            string detail = null;
            string references = null;
            StructureOutputFormat structureOutputFormat = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.Json);

            // When
            ICommonStructureQuery commonStructureQuery = CreateCommonStructureQuery(
                structureOutputFormat,
                structure, agencyID, resourceID, version, childRefs,
                detail, references);

            // Then
            Assert.AreEqual(2, commonStructureQuery.VersionRequests.Count);

            var version1 = commonStructureQuery.VersionRequests[0];
            Assert.IsTrue(version1.SpecifiesVersion);
            Assert.AreEqual(1, version1.Major);
            Assert.AreEqual(0, version1.Minor);
            Assert.AreEqual(0, version1.Patch);
            Assert.AreEqual("ext", version1.Extension);

            var version2 = commonStructureQuery.VersionRequests[1];
            Assert.IsTrue(version2.SpecifiesVersion);
            Assert.AreEqual(2, version2.Major);
            Assert.AreEqual(1, version2.Minor);
            Assert.IsNull(version2.Patch);
            Assert.IsNull(version2.Extension);
            Assert.AreEqual(VersionPosition.Minor, version2.WildCard.WildcardPosition);
            Assert.AreEqual(VersionQueryTypeEnum.LatestStable, version2.WildCard.WildcardType);
        }

        // Testing all possible detail values
        [TestCase("full", ComplexStructureQueryDetailEnumType.Full, ComplexMaintainableQueryDetailEnumType.Full)]
        [TestCase("allstubs", ComplexStructureQueryDetailEnumType.Stub, ComplexMaintainableQueryDetailEnumType.Stub)]
        [TestCase("referencestubs", ComplexStructureQueryDetailEnumType.Full, ComplexMaintainableQueryDetailEnumType.Stub)]
        [TestCase("referencepartial", ComplexStructureQueryDetailEnumType.Full, ComplexMaintainableQueryDetailEnumType.ReferencePartial)]
        [TestCase("allcompletestubs", ComplexStructureQueryDetailEnumType.CompleteStub, ComplexMaintainableQueryDetailEnumType.CompleteStub)]
        //[TestCase("referencecompletestubs", ComplexStructureQueryDetailEnumType.Full, ComplexMaintainableQueryDetailEnumType.CompleteStub)] // this is not supported yet
        //[TestCase("raw", ComplexStructureQueryDetailEnumType.Full)] // don't know how to test that
        [TestCase(null, ComplexStructureQueryDetailEnumType.Full, ComplexMaintainableQueryDetailEnumType.Full)]
        public void TestDetail(string detail, ComplexStructureQueryDetailEnumType expectedDetail, ComplexMaintainableQueryDetailEnumType expectedReferenceDetail)
        {
            // Given
            string structure = "datastructure";
            string agencyID = "*";
            string resourceID = "*";
            string version = "~";
            string childRefs = null;
            string references = null;
            StructureOutputFormat structureOutputFormat = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.Json);

            // When
            ICommonStructureQuery commonStructureQuery = CreateCommonStructureQuery(
                structureOutputFormat,
                structure, agencyID, resourceID, version, childRefs,
                detail, references);

            // Then
            Assert.AreEqual(expectedDetail, commonStructureQuery.RequestedDetail.EnumType);
            Assert.AreEqual(expectedReferenceDetail, commonStructureQuery.ReferencedDetail.EnumType);
        }

        // Testing all possible reference values - documentation allows multple values but don't know how (if) this works
        [TestCase("none", StructureReferenceDetailEnumType.None)]
        [TestCase("parents", StructureReferenceDetailEnumType.Parents)]
        [TestCase("parentsandsiblings", StructureReferenceDetailEnumType.ParentsSiblings)]
        [TestCase("ancestors", StructureReferenceDetailEnumType.Ancestors)]
        [TestCase("children", StructureReferenceDetailEnumType.Children)]
        [TestCase("descendants", StructureReferenceDetailEnumType.Descendants)]
        [TestCase("all", StructureReferenceDetailEnumType.All)]
        [TestCase(null, StructureReferenceDetailEnumType.None)]
        public void TestReferences(string references, StructureReferenceDetailEnumType expectedReferences)
        {
            // Given
            string structure = "datastructure";
            string agencyID = "*";
            string resourceID = "*";
            string version = "~";
            string childRefs = null;
            string detail = null;
            StructureOutputFormat structureOutputFormat = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.Json);

            // When
            ICommonStructureQuery commonStructureQuery = CreateCommonStructureQuery(
                structureOutputFormat,
                structure, agencyID, resourceID, version, childRefs,
                detail, references);

            // Then
            Assert.AreEqual(0, commonStructureQuery.SpecificReferences.Count);
            Assert.AreEqual(expectedReferences, commonStructureQuery.References.EnumType);
        }

        [Test]
        public void TestReferencesSpecificResource()
        {
            // Given
            string structure = "datastructure";
            string agencyID = "*";
            string resourceID = "*";
            string version = "~";
            string childRefs = null;
            string detail = null;
            string references = "codelist";
            StructureOutputFormat structureOutputFormat = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument);

            // When
            ICommonStructureQuery commonStructureQuery = CreateCommonStructureQuery(
                structureOutputFormat,
                structure, agencyID, resourceID, version, childRefs,
                detail, references);

            // Then
            Assert.AreEqual(SdmxStructureEnumType.CodeList, commonStructureQuery.SpecificReferences[0].EnumType);
            Assert.AreEqual(StructureReferenceDetailEnumType.Specific, commonStructureQuery.References.EnumType);
        }

        [Test]
        public void TestMultiples()
        {
            // Given
            string structure = "categoryscheme";
            string agencyID = "AID1,AID2";
            string resourceID = "RID1,RID2";
            string version = "1.0.0,2.1.0";
            string childRefs = "CHR1,CHR2";
            string detail = null;
            string references = "none";
            StructureOutputFormat structureOutputFormat = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.Json);

            // When
            ICommonStructureQuery commonStructureQuery = CreateCommonStructureQuery(
                structureOutputFormat,
                structure, agencyID, resourceID, version, childRefs,
                detail, references);

            IList<string> agencyIds = new List<string>() { "AID1", "AID2" };
            IList<string> maintainableIds = new List<string>() { "RID1", "RID2" };
            //List<ComplexIdentifiableReferenceBean> childReferences = new ArrayList<>();
            //childReferences.Add(ComplexIdentifiableReferenceBeanImpl.CreateForRest("CHR1", SdmxStructureType.Category));
            //childReferences.Add(ComplexIdentifiableReferenceBeanImpl.CreateForRest("CHR2", SdmxStructureType.Category));

            // Then
            Assert.IsTrue(commonStructureQuery.HasSpecificAgencyId);
            Assert.AreEqual(2, commonStructureQuery.AgencyIds.Count);
            Assert.IsTrue(commonStructureQuery.AgencyIds.ContainsAll(agencyIds));
            Assert.IsTrue(commonStructureQuery.HasSpecificMaintainableId);
            Assert.AreEqual(2, commonStructureQuery.MaintainableIds.Count);
            Assert.IsTrue(commonStructureQuery.MaintainableIds.ContainsAll(maintainableIds));
            Assert.AreEqual(2, commonStructureQuery.SpecificItems.Count);
            //Assert.IsTrue(commonStructureQuery.SpecificItems.ContainsAll(childReferences));
            Assert.AreEqual("CHR1", commonStructureQuery.SpecificItems[0].Id.SearchParameter);
            Assert.AreEqual("CHR2", commonStructureQuery.SpecificItems[1].Id.SearchParameter);
        }

        private static ICommonStructureQuery CreateCommonStructureQuery(
            StructureOutputFormat structureOutputFormat,
            string structure, string agencyID, string resourceID, string version, string childRefs,
            string detail, string references)
        {
            Dictionary<string, string> queryParameters = new Dictionary<string, string>();
            if (detail != null)
            {
                queryParameters.Put("detail", detail);
            }
            if (references != null)
            {
                queryParameters.Put("references", references);
            }

            ICommonStructureQueryBuilder<RestStructureQueryParams> commonStructureQueryBuilder = new RestV2CommonStructureQueryBuilder();
            RestStructureQueryParams restStructureQueryParams = new RestStructureQueryParams(structureOutputFormat, structure, agencyID, resourceID, version, childRefs, queryParameters);
            ICommonStructureQuery commonStructureQuery = commonStructureQueryBuilder.BuildCommonStructureQuery(restStructureQueryParams);

            return commonStructureQuery;
        }
    }
}
