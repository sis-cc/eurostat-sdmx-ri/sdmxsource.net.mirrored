#!/bin/bash

set -e
set -u
snaps="$HOME/nuget-snaps"
[ ! -d "${snaps}" ] && mkdir "${snaps}"
source='My Snapshots'

if [ -d build ]; then
  rm build/*.nupkg || true
fi
shopt -s globstar
for i in *.sln; do
  dotnet restore -f $i
  dotnet clean $i
  dotnet restore -f $i
  dotnet build -p:IncludeSymbols=true -p:IncludeSource=true $i 
done
echo "build done";

if [ ! -d build ]; then
  exit
else
	echo "packing";
	mv -v **/Debug/*.nupkg ./build/ || dotnet pack -c Debug -o build --include-symbols --include-source
fi

echo "moving symbols";
cd build
for i in *.symbols.nupkg; do 
	echo i;
    b=`basename $i .symbols.nupkg`
    t="$b.nupkg"
    mv $i $t
done

echo "pushing nuget";
for t in *.nupkg; do
	echo "$t";
    x=$(echo $t | perl -pe 's/^(.*?)\.((?:\.?[0-9]+){3,}(?:[-a-zA-Z0-9].+)?)\.nupkg$/$1 $2/')
    dotnet nuget delete --non-interactive -s "$source" $x || true
    dotnet nuget push -s "$source" "$t"
    package="${x%% *}"
    if [ -n "${package}" ]; then
      rm -v -rf ~/.nuget/packages/"${package}"
    fi
done

cd -

