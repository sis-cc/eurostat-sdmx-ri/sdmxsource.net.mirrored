using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;
using System.Xml.Schema;
using Xml.Schema.Linq;

namespace Org.Sdmx.Resources.SdmxMl.Schemas.V21.MetaData.Generic
{
    public partial class MetadataSetType : global::Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common.AnnotableType, IXMetaData
    {
        /// <summary>
        /// <para>
        /// The publicationYear holds the ISO 8601 four-digit year.
        /// </para>
        /// <para>
        /// Occurrence: optional
        /// </para>
        /// </summary>
        public string PublicationYearString
        {
            get
            {
                XAttribute x = this.Attribute(XName.Get("publicationYear", ""));
                if ((x == null))
                {
                    return null;
                }
                return XTypedServices.ParseValue<string>(x, XmlSchemaType.GetBuiltInSimpleType(XmlTypeCode.GYear).Datatype);
            }
            set
            {
                this.SetAttribute(XName.Get("publicationYear", ""), value, XmlSchemaType.GetBuiltInSimpleType(XmlTypeCode.GYear).Datatype);
            }
        }
    }
}
