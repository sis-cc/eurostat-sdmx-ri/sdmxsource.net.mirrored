﻿// -----------------------------------------------------------------------
// <copyright file="KeySetType.cs" company="EUROSTAT">
//   Date Created : 2016-08-08
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of Org.Sdmx.Resources.SdmxMl.Schemas.V21.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with Org.Sdmx.Resources.SdmxMl.Schemas.V21.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure
{
    using System.Collections.Generic;
    using System.Xml.Linq;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;

    using Xml.Schema.Linq;

    /// <summary>
    /// <para>
    /// KeySetType is an abstract base type for defining a collection of keys.
    /// </para>
    /// <para>
    /// Regular expression: (Key+)
    /// </para>
    /// </summary>
    /// <seealso cref="Xml.Schema.Linq.XTypedElement" />
    /// <seealso cref="Xml.Schema.Linq.IXMetaData" />
    public partial class KeySetType
    {
        /// <summary>
        /// The key list
        /// </summary>
        private object _keyList;

        /// <summary>
        /// Gets the typed key.
        /// </summary>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <returns>The typed Key</returns>
        public IList<TKey> GetTypedKey<TKey>() where TKey : DistinctKeyType
        {
            if (this._keyList == null)
            {
               this._keyList = new XTypedList<TKey>(this, LinqToXsdTypeManager.Instance, XName.Get("Key", "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure"));
            }

            return (XTypedList<TKey>)this._keyList;
        }
    }
}