﻿// -----------------------------------------------------------------------
// <copyright file="GenericMetadataHeaderType.cs" company="EUROSTAT">
//   Date Created : 2016-03-01
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of Org.Sdmx.Resources.SdmxMl.Schemas.V21.
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with Org.Sdmx.Resources.SdmxMl.Schemas.V21.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message
{
    using System.Collections.Generic;
    using System.Xml.Linq;
    using Common;
    using Xml.Schema.Linq;

    /// <summary>
    ///     The base header type class
    /// </summary>
    /// <seealso cref="Xml.Schema.Linq.XTypedElement" />
    public partial class BaseHeaderType
    {
        /// <summary>
        ///     The reference element name
        /// </summary>
        private static readonly XName _referenceName = XName.Get("Ref", string.Empty);

        /// <summary>
        ///     <para>
        ///         Set the Reference. Ref is used to provide a complete set of reference fields. Derived reference types will
        ///         restrict the RefType so that the content of the Ref element requires exactly what is needed for a complete
        ///         reference.
        ///     </para>
        ///     <para>
        ///         Occurrence: required
        ///     </para>
        ///     <para>
        ///         Setter: Appends
        ///     </para>
        ///     <para>
        ///         Regular expression: ((@Ref, URN?)|URN)
        ///     </para>
        /// </summary>
        /// <typeparam name="T">The type of <see cref="RefBaseType" /></typeparam>
        /// <returns>
        ///     The <paramref name="value" />
        /// </returns>
        public IList<T> GetTypedRef<T>() where T : PayloadStructureType
        {
            return new XTypedList<T>(
                this, 
                LinqToXsdTypeManager.Instance, 
                XName.Get("Structure", "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message"));
        }
    }
}