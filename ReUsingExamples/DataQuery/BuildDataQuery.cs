﻿// -----------------------------------------------------------------------
// <copyright file="BuildDataQuery.cs" company="EUROSTAT">
//   Date Created : 2015-12-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of ReUsingExamples.
//     ReUsingExamples is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     ReUsingExamples is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with ReUsingExamples.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace ReUsingExamples.DataQuery
{
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

    /// <summary>
    ///     The build data query class
    /// </summary>
    public class BuildDataQuery
    {
        /// <summary>
        ///     Builds the specified DSD.
        /// </summary>
        /// <param name="dsd">The DSD.</param>
        /// <param name="dataflowObject">The dataflow object.</param>
        /// <returns>The data query</returns>
        public IDataQuery Build(IDataStructureObject dsd, IDataflowObject dataflowObject)
        {
            IDataQuerySelection dataQuerySelection = new DataQueryDimensionSelectionImpl("FREQ", "M", "A");
            IDataQuerySelectionGroup dataQuerySelectionGroup =
                new DataQuerySelectionGroupImpl(
                    new HashSet<IDataQuerySelection> { dataQuerySelection }, 
                    new SdmxDateCore("2000-01"), 
                    new SdmxDateCore("2005-03"));
            return new DataQueryImpl(
                dsd, 
                null, 
                DataQueryDetail.GetFromEnum(DataQueryDetailEnumType.Full), 
                null, 
                null, 
                null, 
                dataflowObject, 
                "TIME_PERIOD", 
                new[] { dataQuerySelectionGroup });
        }
    }
}