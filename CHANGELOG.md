# Changelog

## sdmxsource.net v9.11.0 (2024-04-17)

### Details v9.11.0

There are no changes between previous release 9.10.0 and 9.11.0. This release was only made in order to align SdmxSource version with public release 9.11.0 version.

## sdmxsource.net v9.10.0 (2024-04-01)

### Details v9.10.0

1. Support for new SDMX 3.0.0 artefacts
   1. Hierarchy
   1. HierarchyAssociation
   1. GeographicCodelist
1. Improve support for metadata attribute usage
1. Improve support SDMX-CSV 2.0.0 and multiple measures
1. Disable the MSD missing exception when querying data for a data structure that references an MSD via an annotation.
1. Remove unused methods from the Translator interface

### Tickets v9.10.0

The following new features added:

- SDMXRI-2107: SdmxSource .NET HierarchyAssociation SDMX-ML 3.0.0 writer (HierarchyAssociation,SDMX-ML-Writer,SDMX3.0)
- SDMXRI-2104: SdmxSource .NET Hierarchy SDMX-ML 3.0.0 writer (Hierarchy,SDMX-ML-Writer,SDMX3.0)
- SDMXRI-2086: SdmxSource .NET GeographicCodelist SDMX-ML 3.0.0 writer (GeographicCodelist,SDMX-ML-Writer,SDMX3.0)
- SDMXRI-2105: SdmxSource .NET Hierarchy SDMX-ML 3.0.0 reader (Hierarchy,SDMX-ML-Reader,SDMX3.0)
- SDMXRI-2375: RE: Very impacting issue in NSI_WS/MA_WS 8.15.1 and higher (ISTAT)
- SDMXRI-2379: SDMX-CSV 2.0.0 Fix support for Measures (.NET) (SDMX3.0,SDMXCSV2.0.0)
- SDMXRI-2197: SDMXRI support storing/retrieving SDMX 3.0.0 DSD Metadata attribute usage info (.net) (DataStructure,SDMX3.0)

The following bugs have been corrected:

- SDMXRI-2365: NSI_WS 8.16.0: Bug on updating the not empty text of the annotations in the dataflows

## sdmxsource.net v9.9.0 (2024-02-09)

### Details v9.9.0

1. Updated vulnerable 3rd party dependencies to their latest version
1. Support for parsing Data Consumer Schemes from SDMX 3.0.0 XML structure
1. Some performance improvements in generating hash codes in SDMX objects
1. Many bugfixes discovered while testing SDMXRI

### Tickets v9.9.0

The following new features added:

- SDMXRI-2355: Update vulnerable 3rd party dependencies
- SDMXRI-2099: SdmxSource .NET DataConsumerScheme SDMX-ML 3.0.0 reader (DataConsumerScheme,SDMX-ML-Reader,SDMX3.0)
- SDMXRI-2288: SDMXRI WS performance improvements (SDMXRI)

The following bugs have been corrected:

- SDMXRI-2319: SDMXRI minor issues found in v9.8.0-alpha (2023-12-14)
- SDMXRI-2284: MAWEB .NET- Preview Data option, does not return data for SDMX 3.0 formats
- SDMXRI-2209: MAWEB: header configuration
- SDMXRI-2321: CLONE - MAWEBApp- Test valid NsiWs endpoint, returns error message
- SDMXRI-2312: MAWEB .NET&Java- Import fails when importing content constraint file
- SDMXRI-2302: MAWEB.NET- Import files functionality ignores the sentinel values
- SDMXRI-2282: MAWEB .NET- Missing transcoding codes for components that reference codelists via wildcards

## sdmxsource.net v9.8.0 (2023-12-15)

### Details v9.8.0

- Implemented writer for SDMX-ML 3.0.0 DataConsumerScheme

### Tickets 9.8.0

1. SDMXRI-2098 SdmxSource .NET DataConsumerScheme SDMX-ML 3.0.0 writer


## sdmxsource.net v9.7.0 (2023-12-05)

### Details v9.7.0

- Implemented Readers/Writers for SDMX-ML 3.0.0 (AgencyScheme, OrganisationUnitScheme, DataConstraint)
- Fixes: ContentConstraint Writer/Reader and DataProviderScheme Reader
- Fixed two bugs for parsing xml 3.0

### Tickets 9.7.0

1. SDMXRI-2078 SdmxSource .NET AgencyScheme SDMX-ML 3.0.0 reader
2. SDMXRI-2077 SdmxSource .NET AgencyScheme SDMX-ML 3.0.0 writer
3. SDMXRI-2123 SdmxSource .NET OrganisationUnitScheme SDMX-ML 3.0.0 reader
4. SDMXRI-2122 SdmxSource .NET OrganisationUnitScheme SDMX-ML 3.0.0 writer
5. SDMXRI-2096 SdmxSource .NET DataConstraint SDMX-ML 3.0.0 reader
6. SDMXRI-2095 SdmxSource .NET DataConstraint SDMX-ML 3.0.0 writer

## sdmxsource.net v9.6.0 (2023-11-03)

### Details v9.6.0

- Implemented Readers/Writers for SDMX-ML 3.0.0 (CateggoryScheme, ProvisionAgreemnt, DataProviderScheme)
- Implemented DataProviderScheme reader for SDMX-ML 3.0.0 Structure message
- Update SDMX-CSV v2 and SDMX-JSON v2 to use 3.0.0 DataWriterEngine methods
- Implement JSON 2.0.0 parsers for Dataflow, DataStructure (DSD), Codelist, Concept Scheme, Category Scheme, Categorisation

### Tickets 9.6.0

1. SDMXRI-2102 SdmxSource .NET DataProviderScheme SDMX-ML 3.0.0 reader
2. SDMXRI-2101 SdmxSource .NET DataProviderScheme SDMX-ML 3.0.0 writer
3. SDMXRI-2084 SdmxSource .NET CategoryScheme SDMX-ML 3.0.0 reader
4. SDMXRI-2083 SdmxSource .NET CategoryScheme SDMX-ML 3.0.0 writer
5. SDMXRI-2125 SdmxSource .NET ProvisionAgreement SDMX-ML 3.0.0 writer
6. SDMXRI-2126 SdmxSource .NET ProvisionAgreement SDMX-ML 3.0.0 reader
7. SDMXRI-2206 Update SDMX-CSV v2 and SDMX-JSON v2 to use 3.0.0 DataWriterEngine methods
8. SDMXRI-1902 SdmxSource SDMX 2.0.0 JSON structure format parser (.NET)

	
## sdmxsource.net v9.5.0 (2023-09-21)

### Details v9.5.0

- Includes changes in 8.18.0 listed below

## sdmxsource.net v8.18.0 (2023-08-01)

### Details v8.18.0

1. Support for SDMX-CSV 2.0.0 `labels=name`
1. Output UTC date in some cases related to embargo date attribute
1. Fix typo in namespace for SDMX 2.1 data files
1. Use [central package management](https://learn.microsoft.com/en-us/nuget/consume-packages/central-package-management), versions of dependencies from all projects are stored in `Directory.Packages.props`.

### Tickets v8.18.0

The following new features added:

- SDMXRI-2161: Publish pre-release nuget packages to nuget.org (OECD)
- SDMXRI-2182: Implement SDMX-CSV 2.0 `labels=name` option (OECD,PULL_REQUEST)
- SDMXRI-2180: Restricted access to confidential or embargoed data (OECD,PULL_REQUEST)

The following bugs have been corrected:

- SDMXRI-2145: Malformed smdx.org URL when using structurespecificdata query param

## sdmxsource.net v8.17.0 (2023-06-07)

### Details v8.17.0

1. SDMX-JSON data writer enhancements
   1. allow use of special characters (`~`, `*`) in metadata response
   1. Changes in HCL support in data
1. SDMX-JSON Data changes in HCL support
1. Align with final standard approach agreed in SDMX-TWG.

|Format|Numeric types: float, double|Text types|
|------|----------------------------|----------|
|XML|NaN|#N/A|
|JSON|NaN|#N/A|
|CSV|NaN|#N/A|

- Change the NSIWS data-writers to return these special values accordingly.
  - For Text types there should not be any special treatement because they should be already stored and returned from the data database as {{#N/A}}
  - For float and double, make sure that {{NaN}} can be parsed accordingly to all three formats. Specially to JSON.
- Remove from NSIWS the config. setting [useIntentionallyMissingMeasures|https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/blob/develop/config/Properties.json] which is not used anymore

### Tickets v8.17.0

The following new features added:

- SDMXRI-2164: Update json 2.0 writer to use special characters (~,*) in metadata response (OECD,PULL_REQUEST)
- SDMXRI-2163: NaN managed as intentionally missing value (OECD,PULL_REQUEST)
- SDMXRI-1924: Add a new HTTP X-Level option to get the referential metadata only at the current level (2021.0333-QTM5,OECD,PULL_REQUEST)
- SDMXRI-2157: HCL parents output in SDMX-json,  use first localised HCL definition from AnnotationText and then non-localised HCL from AnnotationTitle, followup of SDMXRI-2136 (OECD,PULL_REQUEST)
- SDMXRI-2155: add parameter to add "-" and '*' as wildcard symbols to DataQueryImpl filter  (OECD,PULL_REQUEST)

## sdmxsource.net v8.16.0 (2023-05-04)

### Details v8.16.0

1. This release contains fixes and enhancements from OECD
   1. In OECD  for referential metadata the string `-` is used as an intentionally missing value for dimension (normal and time dimension) and has a different treatment to an empty string when imported. But during writing the passed string for a Time dimension was tried to be parsed as SDMX time and was throwing an exception. This change is to avoid an exception and allow the minus symbol the same as an empty string.
   1. SDMX-JSON 2.0.0 better handle datasets with delete action and metadata attribute usage, e.g. in data requests with `updatedAfter`.
   1. Support a special annotation that emulates SDMX 3.0.0 Hierarchical association that allows including hierarchical codelist in SDMX-JSON data

### Tickets v8.16.0

The following new features added:

- SDMXRI-2146: Treat "-" as an empty string when parsing datetime with DateUtil (OECD,PULL_REQUEST)
- SDMXRI-2141: Support metadata output in json/csv with updatedAfter parameter (multiple datasets) (OECD,PULL_REQUEST)
- SDMXRI-2138: write deleted observations as single datasets when updatedAfter parameter is used (OECD,PULL_REQUEST)
- SDMXRI-2136: SDMX-JSON data: Return definition of parents when child items have data, use hierachical codelist through annotation definition. Addon to SDMXRI-2037 (OECD,PULL_REQUEST)
- SDMXRI-2045: Retrieve empty textual intentionally missing measures in xml

The following bugs have been corrected:

- SDMXRI-2051: Json writer throws an error for DSD with a single non-time dimension (OECD,PULL_REQUEST)


## sdmxsource.net v9.4.0-alpha (2023-07-13)

### Details v9.4.0-alpha

1. Support for Categorisation in SDMX-ML 3.0.0 format
   1. It uses an approach ported from sdmx-core

### Tickets v9.4.0-alpha

The following new features added:

- SDMXRI-2081: SdmxSource .NET Categorisation SDMX-ML 3.0.0 reader (Categorisation,SDMX-ML-Reader,SDMX3.0)
- SDMXRI-2080: SdmxSource .NET Categorisation SDMX-ML 3.0.0 writer (Categorisation,SDMX-ML-Writer,SDMX3.0)

## sdmxsource.net v9.3.0-alpha (2023-06-16)

### Details v9.3.0-alpha

1. SDMX JSON 2.0 Structure support
1. Test Client legacy CSV support

### Tickets v9.3.0-alpha

The following new features added:

- SDMXRI-1785: SDMX REST 2.0.0 API  support SDMX 2.0.0 JSON structure format (SDMX3.0)
- SDMXRI-1901: SdmxSource support SDMX 2.0.0 JSON structure format writer (.net) (SDMX3.0)
- SDMXRI-1880: Test Client in MAWEB - Old CSV support (2022.0146–QTM3,PUBLIC_2023)

## sdmxsource.net v9.2.0-alpha (2023-05-19)

### Details v9.2.0-alpha

1. Merge specific changes from 8.16.0 
1. More work on SDMX REST 2.0.0 Availability support

### Tickets v9.2.0-alpha

The following new features added:

- SDMXRI-2148: SdmxSource cannot create stubs for Content Constrains/Provision agreement
- SDMXRI-1798: SDMX REST 2.0.0 API  availability support new query parameter "c" (SDMX3.0)
- SDMXRI-2138: write deleted observations as single datasets when updatedAfter parameter is used (OECD,PULL_REQUEST)
- SDMXRI-2136: SDMX-JSON data: Return definition of parents when child items have data, use hierachical codelist through annotation definition. Addon to SDMXRI-2037 (OECD,PULL_REQUEST)
- SDMXRI-2141: Support metadata output in json/csv with updatedAfter parameter (multiple datasets) (OECD,PULL_REQUEST)
- SDMXRI-1794: SDMX REST 2.0.0 API  support the new URL path for availability (SDMX3.0)
- SDMXRI-2044: Merge changes from 8.15.1 to 9.0.0-alpha in SDMXRI (2022.0146–QTM3)
- SDMXRI-2023: Support dimensions with NULL values in JSON writer (OECD needs to output wildcarded observations) (2022.0146–QTM3,OECD,PULL_REQUEST)

The following bugs have been corrected:

- SDMXRI-2051: Json writer throws an error for DSD with a single non-time dimension

## sdmxsource.net v9.1.0-alpha (2023-04-07)

### Details v9.1.0-alpha

1. Includes changes from 8.15.1 release
1. Increased SDMX 3.0.0 and SDMX REST 2.0.0 compliance

### Tickets v9.1.0-alpha

The following new features added:

- SDMXRI-2044: Merge changes from 8.15.1 to 9.0.0-alpha in SDMXRI (2022.0146–QTM3)
- SDMXRI-1840: SDMX 3.0.0 - MADB 7/MAWS migration Retrieve/Persist Entities (2021.0333-QTM4,Package_1_SDMX3.0,SDMX3.0)
- SDMXRI-1839: SDMX 3.0.0 - MADB 7/MAWS migration - Retrieve/Persist SDMX artefacts (2021.0333-QTM4,Package_1_SDMX3.0,SDMX3.0)
- SDMXRI-1975: Dataflow Upgrade work with wildcard/MADB7 (2022.0146–QTM3,SDMX3.0)
- SDMXRI-1908: SDMXRI SDMX 3.0 Support for DSD 3.0 Array mapping (SDMX3.0)
- SDMXRI-2049: SdmxSource: Conversion between SDMX time periods not always compliant
- SDMXRI-2045: Retrieve empty textual intentionally missing measures in xml (OECD,PULL_REQUEST)
- SDMXRI-2037: SDMX-JSON data: Return definition of parents when child items have data, use hierachical codelist through annotation definition. (2022.0146–QTM3,OECD,PULL_REQUEST)
- SDMXRI-2029: Update textType Count as numeric (2022.0146–QTM3,OECD,PULL_REQUEST)
- SDMXRI-2025: Retrieve intentionally missing values (2022.0146–QTM3,OECD,PULL_REQUEST)
- SDMXRI-1910: SDMXRI SDMX 3.0 Support for DSD 3.0 Measures (2022.0146–QTM3,SDMX3.0)
- SDMXRI-1920: SDMXRI SDMX 3.0.0  referencepartial detail in Structure requests (Package_1_SDMX3.0,SDMX3.0)
- SDMXRI-1990: Update 3rd party dependencies for the public release (.NET) (2022.0146–QTM1)
- SDMXRI-1991: XML reader yields the datasets twice when there is only one empty in the data file (OECD,PULL_REQUEST)
- SDMXRI-1982: Add alternative usage of 'X-Range' header for range requests (2022.0146–QTM1,OECD,PULL_REQUEST)
- SDMXRI-1704: SdmxSource support for SDMX v3.0 DSD, Codelist, Concept scheme (SDMX3.0)
- SDMXRI-1783: SDMX REST 2.0.0 API GET structure references=ancestors support  (SDMX3.0)
- SDMXRI-1790: SDMX REST 2.0.0 API  data support new query parameter measures (2022.0146–QTM1,SDMX3.0)
- SDMXRI-1788: SDMX REST 2.0.0 API  data support new query parameter attributes (2022.0146–QTM2,SDMX3.0)
- SDMXRI-1789: SDMX REST 2.0.0 API  data support new query parameter "c" (2021.0333-QTM5,SDMX3.0)
- SDMXRI-1784: SDMX REST 2.0.0 API  support SDMX 3.0.0 XML structure format (2021.0333-QTM5,SDMX3.0)

The following bugs have been corrected:

- SDMXRI-2030: SDMXRI minor issues found in v9.0.0-alpha.2 (2022-11-03))
- SDMXRI-2028: Expecting error when uploading content constraint with duplicated codes in a repeated concept
- SDMXRI-1803: In DSD 2.1 submissions, the dimension "position" property must be ignored

## sdmxsource.net v8.15.1 (2023-02-22)

### Details v8.15.1

1. Add a conditional parameter to JsonWriter 2.0 and NSI to include parent codes without data , that have children with data. Use parent-child relation from a hierarchical codelist, that might be attached to DSD/Dataflow through an annotation.

### Tickets v8.15.1

The following new features added:

- SDMXRI-2037: SDMX-JSON data: Return definition of parents when child items have data, use hierachical codelist through annotation definition. (OECD,PULL_REQUEST)

## sdmxsource.net v8.15.0 (2023-01-26)

### Details v8.15.0

1. Support dimensions with missing values in json format
1. Text format data type `Count` should be considered numeric data type.
1. Ignore duplicate key/attribute values entries in Cube Regions

### Tickets v8.15.0

The following new features added:

- SDMXRI-2023: Support dimensions with NULL values in JSON writer (OECD needs to output wildcarded observations) (OECD,PULL_REQUEST)
- SDMXRI-2029: Update textType Count as numeric (OECD,PULL_REQUEST)
- SDMXRI-2025: Retrieve intentionally missing values (OECD,PULL_REQUEST)

The following bugs have been corrected:

- SDMXRI-2028: Expecting error when uploading content constraint with duplicated codes in a repeated concept

## sdmxsource.net v8.13.0 (2022-11-25)

### Details v8.13.0

1. Improve complete stub support via REST
1. Support multiple multiple actions in SDMX-CSV
1. SDMX-CSV can use now culture specific decimal separator
1. Extension HTTP header `X-Range` for Range requests.
1. Updated 3rd party dependencies.

### Tickets v8.13.0

The following new features added:

- SDMXRI-2006: Suuport multiple startDatalow calls and inlude Action column in csv2.0 to support multiple actions (Append, Delete) (OECD)
- SDMXRI-1970: Culture specific column and decimal separators in sdmx csv (OECD,PULL_REQUEST)
- SDMXRI-1878: Add errors array in json v1 and v2 (OECD,PULL_REQUEST)
- SDMXRI-1991: XML reader yields the datasets twice when there is only one empty in the data file (OECD)
- SDMXRI-1990: Update 3rd party dependencies for the public release (.NET)
- SDMXRI-1982: Add alternative usage of 'X-Range' header for range requests (OECD,PULL_REQUEST)

The following bugs have been corrected:

- SDMXRI-1947: detail=allcompletestubs parameter doesn't return all related artefact's annotations


## sdmxsource.net v9.0.0-alpha.2 (2022-11-03)

### Details v9.0.0-alpha.2

1. SDMX REST v2.0.0 (SDMX 3.0.0)
   1. Support for query parameter `attributes`
   1. Support for query parameters `c`
   1. Plugin to output SDMX 3.0.0 XML in NSIWS
1. SDMX 3.0.0 various fixes improvements, programming interfaces updates
1. SDMX-CSV use culture info to determine the decimal separator
1. SDMX-JSON didn't use the correct timezone

### Tickets v9.0.0-alpha.2

The following new features added:

- SDMXRI-1788: SDMX REST 2.0.0 API  data support new query parameter attributes (SDMX3.0)
- SDMXRI-1840: SDMX 3.0.0 - MADB 7/MAWS migration Retrieve/Persist Entities (Package_1_SDMX3.0,SDMX3.0)
- SDMXRI-1839: SDMX 3.0.0 - MADB 7/MAWS migration - Retrieve/Persist SDMX artefacts (Package_1_SDMX3.0,SDMX3.0)
- SDMXRI-1789: SDMX REST 2.0.0 API  data support new query parameter "c" (SDMX3.0)
- SDMXRI-1784: SDMX REST 2.0.0 API  support SDMX 3.0.0 XML structure format (SDMX3.0)
- SDMXRI-1970: Culture specific column and decimal separators in sdmx csv (OECD,PULL_REQUEST)

The following bugs have been corrected:

- SDMXRI-1972: Incorrect timezone usage in SDMX-JSON

## sdmxsource.net v9.0.0 (2022-09-30)

### Details v9.0.0

1. Simplify structure query related interfaces:
   1. MutableSearchManagers are now marked obsolete
      1. Replaced by the new `ICommonSdmxObjectRetrievalManager` and `ICommonSdmxObjectRetrievalManagerSoap20`
   1. Old REST Structure query related classes are now marked obsolete
      1. Replaced by the new `ICommonStructureQuery` and its builder `RestV{n}CommonStructureQueryBuilder`
1. Improve deep equals

### Tickets v9.0.0

The following new features added:

- SDMXRI-1839: SDMX 3.0.0 - MADB 7/MAWS migration - Retrieve/Persist SDMX artefacts (Package_1_SDMX3.0,SDMX3.0)
- SDMXRI-1840: SDMX 3.0.0 - MADB 7/MAWS migration Retrieve/Persist Entities (Package_1_SDMX3.0,SDMX3.0)

## sdmxsource.net v8.12.1 (2022-08-15)

### Details v8.12.1

1. Improve support for `isMultiLingual` in SDMX 2.1 DSD
1. SDMX CSV v2.0.0 enhancements
   1. Read dataset attributes and actions per row
   1. Changes in DataReaderEngine to support action per observations.
1. Support for Merge Action
1. Correct time range constraint in data query

### Tickets v8.12.1

The following new features added:

- SDMXRI-1939: Extend CsvDataReaderEngineV2 to read dataset attributes on every row (OECD,PULL_REQUEST)
- SDMXRI-1934: Add new Merge Action, add CurrentAction to dataReaderEngine (OECD,PULL_REQUEST)

The following bugs have been corrected:

- SDMXRI-1938: Allowedtimecontraint timerange wrongly applied to a data query
- SDMXRI-1835: Wrong behaviour of 'isMultiLingual' property

## sdmxsource.net v8.12.0 (2022-07-08)

### Details v8.12.0

1. XSD generated classes didn't work correctly with ConceptSchemeMap
1. New custom HTTP header

### Tickets v8.12.0

The following new features added:

- SDMXRI-1495: Provide clear error message "Not implemented" when creating StructureSet with ConceptSchemeMap (OECD)
- SDMXRI-1924: Add a new HTTP X-Level option to get the referential metadata only at the current level (OECD,PULL_REQUEST)

## sdmxsource.net v8.11.1 (2022-06-16)

### Details v8.11.1

1. A way to include errors in json v1 & v2
1. Maintain UTF format in valid/from
1. More work on SDMX 3.0.0 compliance
   1. Support for writing SDMX 3.0.0 StructureSpecificData XML
   1. SDMX REST v2.0.0 API support SDMX 3.0.0 structure types
   1. Improvements for SDMX 3.0.0 Structure XML (non-final/final handling)
1. Ignore position provided in SDMX 2.1 XML files for Data Structure Dimensions 
1. Fix issue in `MaintainableUtil<T>.FilterCollectionGetLatest:`
1. Restrict the characters written to `structureID` and `structureRef` XML attribute in SDMX 2.1 datasets.
1. SDMX-JSON bugfixes

### Tickets v8.11.1

The following new features added:

- SDMXRI-1878: Add errors array in json v1 and v2 (OECD,PULL_REQUEST)
- SDMXRI-1805: SdmxSource SDMX 3.0.0 StructureSpecificData XML writer (SDMX3.0)
- SDMXRI-1781: SDMX REST 2.0.0 API structure URL path change (SDMX3.0)
- SDMXRI-1867: Actual content constraints' validFrom/validTo artefact properties to be exposed in UTC format (OECD,PULL_REQUEST)
- SDMXRI-1858: NSI should stop executing aborted/abandoned requests
- SDMXRI-1852: NSIWS plugin with default retriever
- SDMXRI-1854: [NuGet Gallery] Message for owners of the package 'Estat.SdmxSource.SdmxSourceUtil (3rd-level-support)
- SDMXRI-1801: SdmxSource SDMX 3.0.0 Structure XML parser (SDMX3.0)
- SDMXRI-1802: SdmxSource SDMX 3.0.0 Structure XML writer (SDMX3.0)

The following bugs have been corrected:

- SDMXRI-1803: In DSD 2.1 submissions, the dimension "position" property must be ignored
- SDMXRI-1875: Support of character @ and - in the structures' ID and Agency's ID
- SDMXRI-1843: "Semantic Error - 404" error log entry added when value of ObservationImpl.ObsTime is not valid
- SDMXRI-1845: Incorrect SDMX-JSON localised names when language is unmatched

## sdmxsource.net v8.0.14 (2022-06-02)

### Details v8.0.14

1. Restrict the characters written to `structureID` and `structureRef` XML attribute in SDMX 2.1 datasets.

### Tickets v8.0.14

The following bugs have been corrected:

- SDMXRI-1875: Support of character @ and - in the structures' ID and Agency's ID

## sdmxsource.net v8.11.0 (2022-04-14)

### Details v8.11.0

1. Synchronize interfaces between Java and .NET on StructureSpecificData XML parser for 3.0.0
1. first/last observations were ignored when using Range in NSIWS

### Tickets v8.11.0

The following new features added:

- SDMXRI-1804: SdmxSource SDMX 3.0.0 StructureSpecificData XML parser (SDMX3.0)

The following bugs have been corrected:

- SDMXRI-1836: Incorrect total number of observations when firstNObservations/lastNObservations parameter is provided

## sdmxsource.net v8.10.0 (2022-03-24)

### Details v8.10.0

SdmxSource support for the following schema version 3.0 features:

1. Parse and write xml files in the SDMX v3.0 schema with DSDs, Dataflows, Codelists, Concept schemes
1. Xml roundtrip unit test that compares the xml file parsed and the xml file writen
1. Parse structure specific data from xml files
1. Artefact and referenced versions support semantic versioning
1. Referenced artefacts versions support wildcards
1. Annotations with multiple localized URLs
1. Structures with multiple measures
1. Structures that uses reference metadata
1. Structured Text (XHTML) values for Data Attributes and Measures
1. Sentinel values in the representation of components
1. Multi-lingual text values for Data Attributes and Measures.
1. Array values in Data Attributes and Measures

### Tickets v8.10.0

The following new features added:

- SDMXRI-1804: SdmxSource SDMX 3.0.0 StructureSpecificData XML parser (SDMX3.0)
- SDMXRI-1719: SdmxSource SDMX 3.0 Support for Wildcards in versioning (SDMX3.0)
- SDMXRI-1777: SdmxSource support for SDMXv3.0 Annotations (SDMX3.0)
- SDMXRI-1725: SdmxSource SDMX 3.0 Support for DSD 3.0 Sentinel Values (SDMX3.0)
- SDMXRI-1728: SdmxSource SDMX 3.0 Support for DSD 3.0 Metadata attributes (SDMX3.0)
- SDMXRI-1718: SdmxSource SDMX 3.0 Support for semantic versioning (SDMX3.0)
- SDMXRI-1727: SdmxSource SDMX 3.0 Support for DSD 3.0 MSD linkage (SDMX3.0)
- SDMXRI-1801: SdmxSource SDMX 3.0.0 Structure XML parser (SDMX3.0)
- SDMXRI-1802: SdmxSource SDMX 3.0.0 Structure XML writer (SDMX3.0)
- SDMXRI-1724: SdmxSource SDMX 3.0 Support for DSD 3.0 Measures (SDMX3.0)
- SDMXRI-1776: SdmxSource SDMX 3.0 Support for DSD 3.0 XHTML (SDMX3.0)
- SDMXRI-1704: SdmxSource support for SDMX v3.0 DSD, Codelist, Concept scheme (SDMX3.0)

## sdmxsource.net v8.9.2 (2022-02-25)

### Details v8.9.2

1. SDMX-CSV v2 and SDMX-JSON 2.0.0 (SDMX 3.0.0) metadata in data fixes

### Tickets v8.9.2

The following new features added:

- SDMXRI-1819: Referential metadata fixes (OECD,PULL_REQUEST)

## sdmxsource.net v8.9.1 (2022-02-03)

### Details v8.9.1

1. Metadata support in Datasets
1. Initial SDMX-CSV v2.0.0 support for data and metadata
1. Fix issue preventing writing hierarchical codelist ID in SDMX-JSON
1. Improve Multi-dataset support in SDMX-JSON

### Tickets v8.9.1

The following new features added:

- SDMXRI-1795: Data retriever metadata support (OECD,PULL_REQUEST)
- SDMXRI-1806: SDMX-CSV 2.0.0 (meta)data writer

The following bugs have been corrected:

- SDMXRI-1816: SDMX-JSON: Hierarchy in Hierarchical Codelist (HCL) misses the ID (and links)
- SDMXRI-1812: JSON v1.0 writer improperly writes structure section of multi-dataset data messages

## sdmxsource.net v8.9.0 (2021-12-17)

### Details v8.9.0

1. Writer for SDMX-JSON Data 2.0.0 with metadata attributes

### Tickets v8.9.0

The following new features added:

- SDMXRI-1779: SDMX-JSON writer 2.0 with metadata (OECD,PULL_REQUEST)

## sdmxsource.net v8.8.0 (2021-11-05)

### Details v8.8.0

1. Test client functionality in MA WEB; return structure ref when the SDMX 3.0 constructor is used
1. SDMX-CSV 2.0 data reader attribute verification
1. SDMX 3.0 REST API, use of keyword in key parameter
1. Fix structure-specific data messages that cannot be validated with the SDMX Converter
1. Fix AfterPeriod and BeforePeriod in CCs that are wrongly converted to StartPeriod and EndPeriod

### Tickets v8.8.0

The following new features added:

- SDMXRI-1564: integrate the Test Client inside the MA WEB (future-QTM)
- SDMXRI-1745: Implement SDMX-CSV 2.0.0 data reader (OECD,PULL_REQUEST)
- SDMXRI-1565: SDMX 3.0 REST WS implementation (future-QTM)

The following bugs have been corrected:

- SDMXRI-1732: Due to an inconsistent structure ID, structure-specific data messages cannot be validated with the SDMX Converter
- SDMXRI-1687: AfterPeriod and BeforePeriod in CCs must not be converted to StartPeriod and EndPeriod

## sdmxsource.net v8.7.1 (2021-09-29)

### Details v8.7.1

1. Accept ISO 8601 Milliseconds. Note writting them is not supported.
1. SDMX CSV 2.0.0 (SDMX 3.0.0) parsing support


### Tickets v8.7.1

The following new features added:

- SDMXRI-1745: Implement SDMX-CSV 2.0.0 data reader (OECD,PULL_REQUEST)
- SDMXRI-1659: nuget.org GetTimeFormatOfDate reports invalid ISO date time (nuget)

## sdmxsource.net v8.7.0 (2021-09-23)

### Details v8.7.0

1. Introduce the changes under SDMX 3.0 REST API 

### Tickets v8.7.0

The following new features added:

- SDMXRI-1750: Allow retrieving data with constrained but non-provided optional attributes (OECD,PULL_REQUEST)
- SDMXRI-1565: SDMX 3.0 REST WS implementation (future-QTM)

## sdmxsource.net v8.6.0 (2021-09-02)

### Details v8.6.0

1. Return Sql queries generated for data

### Tickets v8.6.0

The following new features added:

- SDMXRI-1564: integrate the Test Client inside the MA WEB (future-QTM)

## sdmxsource.net v8.5.0 (2021-08-12)

### Details v8.5.0

1. Implemented structure part for SDMX 3.0 REST WS
    1. Support for multiple values with comma as separator
    2. Support for wildcards

### Tickets v8.5.0

The following new features added:

- SDMXRI-1739: Add StructureSet, MetadataStructure, Metadataflow, ProvisionAgreement support to JSON structure message (OECD,PULL_REQUEST)
- SDMXRI-1683: Allow attributes with AttributeRelationship Group which includes the time dimension (OECD,PULL_REQUEST)
- SDMXRI-1565: SDMX 3.0 REST WS implementation (future-QTM)

## sdmxsource.net v8.4.0 (2021-07-08)

### Details v8.4.0

#### Changes that affect backward compatibility (V8.4.0)

1. NSIWS uses the 422 HTTP Status code for semantic errors instead of the 403.

### Tickets v8.4.0

The following new features added:

- SDMXRI-1543: Incorrect HTTP status code returned for failing data queries (future-QTM)

## sdmxsource.net v8.2.0 (2021-05-31)

### Details v8.2.0

1. Add asynchronous variant in data retrieval interfaces
1. Improve performance by searching a Time Dimension and Frequency only once me DSD immutable object after profiling information shown by OECD
1. Fix TimeRange after/before period mapping to start/end period
1. Added validation for StartDate (i.e. valid from) of in maintainable files

### Tickets v8.2.0

The following new features added:

- SDMXRI-1534: Treatments of afterPeriod and beforePeriod are mixed up in TimeRangeCore implementation (OECD,future-QTM)
- SDMXRI-1643: Improve performance when getting the Frequency dimension from a DSD (OECD,future-QTM)
- SDMXRI-1622: Make rest data retreival asynchronous  (OECD,future-QTM)

The following bugs have been corrected:

- SDMXRI-1644: StartDate of a MaintainableObjectCore instance can be changed to invalid value

## sdmxsource.net v8.1.3 (2021-04-28)

### Details v8.1.3

1. Added Regex validation of Party/Id.
1. Set & modify the AUTHDB connection settings without having to editing any configuration file.
1. Fix a bug where values of attributes at group attachment level not written in series node in sdmx-json.
1. Fix a bug for Json writer dataflow without time dimension has a bug in a flat format.
1. Fix writing of NULLs in JSON data message when primary measure is not string-type.

### Tickets v8.1.3

The following new features added:

- SDMXRI-1582: Regex validation of Party/Id (OECD,PULL_REQUEST)
- SDMXRI-1533: Deployment of MAWS.NET: authDB connection string configuration (.net) (PUBLIC_2020,QTM10-2019.0281)

The following bugs have been corrected:

- SDMXRI-1553: Values of attributes at group attachment level not written in series node in sdmx-json
- SDMXRI-1588: Json writer dataflow without time dimension has a bug in a flat format
- SDMXRI-1566: Fix writing of NULLs in JSON data message when primary measure is not string-type

## sdmxsource.net v8.1.2 (2021-01-08)

### Details v8.1.2

1. Merge changes from v8.0.3
1. Point in Time fixes
1. Support DSD without TimeDimension in SDMX-CSV and Generic 2.1 XML formats
1. Fix getting observation count when start and end period are used
1. Support filtering data query wuth Allowed Content Constraints.

### Tickets v8.1.2

The following new features added:

- SDMXRI-1522: Allowed Constraints filter data query (OECD,PULL_REQUEST)
- SDMXRI-1494: CsvDataReaderEngine additional exception handling (OECD)

The following bugs have been corrected:

- SDMXRI-1519: SdmxSource data reader doesn't support dataflow without time dimension when format is "generic xml" or "csv"
- SDMXRI-1504: AvailableConstraintQuery to use DateFrom, DateTo
- SDMXRI-1443: Support of observation values with string data type at SDMX-JSON data writers

## sdmxsource.net v8.1.1 (2020-11-25)

### Details v8.1.1

1. HTTP header improvements for Point In Time support
1. Available Constraint query was ignoring in some cases the start/end period
1. Support for non-numeric observation values in SDMX-JSON data
1. Handle SDMX-CSV errors better

### Tickets v8.1.1

The following new features added:

- SDMXRI-1494: CsvDataReaderEngine additional exception handling (OECD)
- SDMXRI-1336: Point in Time support (OECD,PULL_REQUEST,QTM10-2019.0281)

The following bugs have been corrected:

- SDMXRI-1504: AvailableConstraintQuery to use DateFrom, DateTo
- SDMXRI-1443: Support of observation values with string data type at SDMX-JSON data writers

## sdmxsource.net v8.1.0 (2020-10-16)

### Details v8.1.0

1. SDMX-JSON support for non-numeric observation values
1. Point in Time support in utility class for HTTP Header

### Tickets v8.1.0

The following new features added:

- SDMXRI-1336: Point in Time support (OECD)

The following bugs have been corrected:

- SDMXRI-1443: Support of observation values with string data type at SDMX-JSON data writers

## sdmxsource.net v8.0.6 (2021-04-09)

No changes, version synchronized for the public release.

## sdmxsource.net v8.0.5 (2021-03-12)

### Details v8.0.5

1. Added tests for component URN while trying to reproduce an issue in Java
1. Added [Doxygen](https://www.doxygen.nl/index.html) support, see `Doxyfile`, for generating better API documentation

## sdmxsource.net v8.0.4 (2021-01-28)

### Details v8.0.4

1. Fix ServiceURL not showing in SDMX 2.0 structure messages

### Tickets v8.0.4

The following new features added:

- SDMXRI-1295: SDMX v2.0 serviceUrl vs structureUrl in sdmxsource (.net) (QTM6-2019.0281)

## sdmxsource.net v8.0.3 (2020-12-17)

### Details v8.0.3

1. Improve SDMX JSON and SDMX CSV data requests by avoiding requesting the same concept scheme for every component in a DSD.

### Tickets v8.0.3

The following new features added:

- SDMXRI-1498: Investigate performance regression between 7.10 vs 8.0 (.NET) (PUBLIC_2020,QTM10-2019.0281)

## sdmxsource.net v8.0.1 (2020-11-18)

### Details v8.0.1

1. MetaDataSet fixes
   1. Added a new property called PublicationYearString which accepts a year as a string
      1. The autogenerated publicationYear doesn't work
   1. Reporting period dates fixes
1. Counting observations in some cases would ignore startPeriod and endPeriod in available content constraint queries

### Tickets v8.0.1

The following new features added:

- SDMXRI-1497: Metadataset / SdmxSource issue (ISTAT)

The following bugs have been corrected:

- SDMXRI-1504: AvailableConstraintQuery to use DateFrom, DateTo

## sdmxsource.net v8.0.0 (2020-09-24)

### Details v8.0.0

Small bugfixes in SDMXRI

### Tickets v8.0.0

The following bugs have been corrected:

- SDMXRI-1440: MAWEB/WS issues with 2020-09-03 release

## sdmxsource.net v1.25.6 (2020-09-02)

### Details v1.25.6

1. SDMX v2.1 data message Header can reference now either a dataflow or a DSD.

### Tickets v1.25.6

The following new features added:

- SDMXRI-1414: NSIWS make StructureUsage configurable (.NET) (PUBLIC_2020,future-QTM)

## sdmxsource.net v1.25.5 (2020-07-23)

### Important changes v1.25.5

#### Non backwards compatible changes/breaking changes v1.25.5

SDMX-JSON message output has changed.

##### meta changes in all messages

`meta.content-languages` renamed to `meta.contentLanguages` in structure and data messages

##### links array changes in structure messages

- empty links array is never written
- `link.type` is never written
- `link.urn` is only written if the request had an accept header: `"urn=true"`
- `link.rel` possible types
  - `self`: only added if `"urn=true"` is defined
  - `external`: only added if `isExternalReference=true`
  - `href` is populated with structureURL
  - `urn` only added if `"urn=true"` is defined
  - `structure`: only added if `"urn=true"` is defined and the artefact has a DSD or MSD cross-reference
  - direct `"urn"` artefact property is removed as it's not part of the standard and the same information can be found now in the links array with `"self"` `link.rel` type

### Details v1.25.5

1. SDMX-CSV and SDMX-JSON improvements from OECD
1. Fix issue ignoring observation values when reading SDMX v2.1 Flat Generic Data from OECD
1. Support for Hierarchical codelists in SDMX-JSON from OECD

### Tickets v1.25.5

The following new features added:

- SDMXRI-1368: Add Hierarchical codelist support to JSON structure message  (OECD,PULL_REQUEST)

The following bugs have been corrected:

- SDMXRI-1402: CsvDataReaderEngine to handle data with ids+labels along with freetext attributes having ": " (OECD,PULL_REQUEST)
- SDMXRI-1397: Optional attributes are NOT required when importing data (not working for CSV import) (OECD,PULL_REQUEST)
- SDMXRI-1367: Fix JSON format: contentLanguages, links (OECD,PULL_REQUEST)
- SDMXRI-1384: GenericDataReaderEngine doesn't read observation value from flat data XML (OECD,PULL_REQUEST)
- SDMXRI-1400: MAWEB/WS issues with 2020-07-03 release

## sdmxsource.net v1.25.4 (2020-06-12)

### Details v1.25.4

1. SDMX CSV improvements from OECD

### Tickets v1.25.4

The following new features added:

- SDMXRI-1361: Add CSV support to SdmxDataReaderFactory (OECD,PULL_REQUEST)

## sdmxsource.net v1.25.3 (2020-05-13)

### Details v1.25.3

1. In SDMX v2.1 DSD it is now possible for an attribute to attach to the Time Dimension
1. Support for groups in SDMX-CSV
1. Support reading SDMX v2.1 data for dimensions without Time Dimension

### Tickets v1.25.3

The following new features added:

- SDMXRI-1298: NSI WS recognises attributes attached to TIME_PERIOD as observation level attributes (.NET) (OECD,QTM6-2019.0281)
- SDMXRI-1224: SDMX-CSV data writer support for groups (.NET) (OECD,QTM2-2019.0281)

## sdmxsource.net v1.25.2 (2020-04-09)

### Details v1.25.2

1. Support for de-serializing meta-datasets in JSON format. Implemented by ISTAT

### Tickets v1.25.2

The following new features added:

- SDMXRI-1293: (De)Serialization of metadatasets in JSON in SdmxSource (ISTAT)

## sdmxsource.net v1.25.1 (2020-03-19)

### Details v1.25.1

1. New language code support `Ladin Dolomitan`.

### Tickets v1.25.1

The following new features added:

- SDMXRI-1310: Support `lld` ISO-639-3 Language code in SdmxSource.NET (ISTAT)

## sdmxsource.net v1.25.0 (2020-02-27)

### Details v1.25.0

1. SDMX-JSON structure bug fix related to stub categorisation from OECD

### Tickets v1.25.0

The following bugs have been corrected:

- SDMXRI-1286: Json writer bug writing categorisation with  allstubs detail (OECD, PR)

## sdmxsource.net v1.24.9 (2020-02-07)

### Details v1.24.9

1. Merge fixes to JSON DSD parser from ISTAT

### Tickets v1.24.9

The following new features added:

- SDMXRI-1084: SDMX JSON DSD Parser (.NET) (ISTAT,QTM2-2018-SC000941)

## sdmxsource.net v1.24.8 (2019-12-20)

### Details v1.24.8

1. Remove incorrect SDMX Header ID check. It is allowed to start with a number.
1. Introduce a set of new classes to help with the reference resolution based on the SDMX REST documentation

### Tickets v1.24.8

The following new features added:

- SDMXRI-1148: SdmxSource fix Header ID validation (.NET) (QTM2-2019.0281,sync-needed)
- SDMXRI-1194: Support for references parent/children for PA, Structure Set, MSD and MDF + Constraints (.NET) (ISTAT,OECD,QTM2-2019.0281)

## sdmxsource.net v1.24.6 (2019-11-21)

### Details v1.24.6

1. SdmxSource was not parsing SDMX v2.1 SOAP requests with CodeWhere correctly.

### Tickets v1.24.6

The following bugs have been corrected:

- SDMXRI-1233: SOAP structure query doesn't return correct results on NSIWS.NET

## sdmxsource.net v1.24.5 (2019-11-15)

### Details v1.24.5

1. Fix SDMX v2.1 Content Constraint output issue, when there is a TimeRange inside a CubeRegion
1. Multidataset support in SDMX-JSON v1

### Tickets v1.24.5

The following new features added:

- SDMXRI-1213: Invalid child element 'Value' in TimeRange of Actual Content Constraint
- SDMXRI-1091: SDMX-JSON data multi-dataset support (.NET)

## sdmxsource.net v1.24.3 (2019-10-25)

### Details v1.24.3

1. Support parsing SDMX-JSON DataStructures (from ISTAT)
1. Added Generic Flat test from SDMX TWG for Generic Data Reader Engine

### Tickets v1.24.3

The following new features added:

- SDMXRI-1084: SDMX JSON DSD Parser (.NET) (ISTAT,QTM2-2018-SC000941)

The following bugs have been corrected:

- SDMXRI-1093: Support for flat SDMX v2.1 Generic Data data

## sdmxsource.net v1.24.2 (2019-10-11)

### Details v1.24.2

1. SDMX JSON fixes from OECD

### Tickets v1.24.2

The following new features added:

- SDMXRI-1145: Add external link to a json structure message when artifact has isExternalReference=true (OECD)

## sdmxsource.net v1.24.1 (2019-09-27)

### Tickets v1.24.1

The following new features added:

- SDMXRI-1053: Add support in SdmxSource for explicit measures in structure reference (.NET) (QTM2-2018-SC000941,sync-needed)
- SDMXRI-720: Do not include URN in structural metadata output (COLLAB,OECD)

The following bugs have been corrected:

- SDMXRI-1093: Support for flat SDMX v2.1 Generic Data data

## sdmxsource.net v1.24.0 (2019-09-09)

### Details v1.24.0

1. SdmxSource will no longer output URN by default in XML and JSON
1. Bugfixes by ISTAT and OECD in SDMX-JSON Content Constraint parsing

### Tickets v1.24.0

The following new features added:

- SDMXRI-720: Do not include URN in structural metadata output (COLLAB,OECD)

The following bugs have been corrected:

- SDMXRI-1114: Bug in SDMX JSON Content Contstraint parser (OECD, ISTAT)

## sdmxsource.net v1.23.0 (2019-09-02)

### Details v1.23.0

1. Fix XML output for Organisation related Artefacts when Contact was included in the artefact.

### Tickets v1.23.0

The following new features added:

- SDMXRI-1029: SdmxSource fix output for Agency with Contact (.net) (ISTAT,QTM2-2018-SC000941,future-QTM)

## sdmxsource.net v1.22.0 (2019-08-23)

### Tickets v1.22.0

The following new features added:

- SDMXRI-1013: SDMX REST support detail=allcompletestubs (.NET)
- SDMXRI-899: Support for Content Constraint Reference Period and Time Range (.NET)

## sdmxsource.net v1.21.0 (2019-07-19)

### Details v1.21.0

1. In coorporation with OECD fixes related to SDMX v2.1 flat Generic Data
1. Bug fixes related to Content Constraints and `QueryableDataSource`
   1. Fix Content Constraint output when `QueryableDataSource` was used.
   1. Fix equality tests for Content Constraints
1. Refactoring regarding `includeHistory` and available constraint
   1. `includeHistory` should not be part of the Available Constraint query model

### Tickets v1.21.0

The following new features added:

- SDMXRI-1093: Support for flat SDMX v2.1 Generic Data data (OECD)
- SDMXRI-1098: SdmxSource produces ContentConstrain in 2.1 xml in some cases (.NET) (future-QTM)
- SDMXRI-1096: SdmxSource DataSource Bean/Object bug (.NET) (future-QTM)
- SDMXRI-861: Implement ''include history'' in SDMX REST (.net) (QTM6-2018.SC000641)

## sdmxsource.net v1.20.0 (2019-06-25)

## Details v1.20.0

1. Bugfixes related to Locales and recent Windows from OECD
1. Minor API changes for Include History
1. Test files

## Tickets v1.20.0

The following new features added:

- SDMXRI-1029: SdmxSource fix output for Agency with Contact (.net) (ISTAT,QTM2-2018-SC000941,future-QTM)
- SDMXRI-861: Implement ''include history'' in SDMX REST (.net) (QTM6-2018.SC000641)

The following bugs have been corrected:

- SDMXRI-1071: Estat.Sri.Mapping.Tool.exe on w2k16 server throws an exception saying `Illegal Locale: en' when you try to init/update a mappingstore db

Tests for not yet implemented features added:

- SDMXRI-1029: SdmxSource fix output for Agency with Contact (.net) (ISTAT,QTM2-2018-SC000941,future-QTM)

## sdmxsource.net v1.19.0 (2019-05-24)

## Details v1.19.0

1. Bugfixes releated to Content Constraint support
1. Content Constraint test files added
1. Support for Actual / Allowed content constrain queries

## Tickets v1.19.0

The following new features added:

- SDMXRI-1045: SdmxSource fix support for Content Constraints DataSet or Metadataset (.net)
- SDMXRI-930: Content Constraint support attaching to Item and Data Sources (SQL) (QTM6-2018.SC000641)
- SDMXRI-711: Get feature for (Valid and/or Actual) ContentContraints (.net) (COLLAB,OECD,QTM6-2018.SC000641)

## sdmxsource.net v1.18.0 (2019-04-25)

## Details v1.18.0

1. Modify SdmxSource structure query classes to support item queries in both REST and SOAP v2.1.

## Tickets v1.18.0

The following new features added:

- SDMXRI-713: Get references for specific items (in ItemSchemes) only .net (COLLAB,OECD,QTM6-2018.SC000641)

## sdmxsource.net v1.17.0 (2019-04-03)

## Details v1.17.0

1. SDMX-JSON hardcoded to use UTF-8 without the BOM. (Changes from OECD)

## Tickets v1.17.0

The following bugs have been corrected:

- SDMXRI-983: Use hardcoded UTF8 without BOM in json writers (OECD)

## sdmxsource.net v1.16.1 (2019-03-21)

## Details v1.16.1

1. OECD SDMX Json format enhancements:
   1. Changes of data and structure message formats in JSON standard applied on JSON v1.0 components
   1. JSON data message. Set codelist code's parent ID in case of hierarchical codelist

## Tickets v1.16.1

The following new features added:

- SDMXRI-906: Change in localised text in SDMX-JSON data and structure messages

## sdmxsource.net v1.16.0 (2019-03-08)

## Details v1.16.0

1. New tests cases discovered while synchronizing date/time with Java
1. Support Structure Specific Data for SDMX v2.1 DSD without Time Dimension.

## Tickets v1.16.0

The following new features added:

- SDMXRI-883: Support reading data for v2.1 timedimension less Dsd (.NET only) (QTM6-2018.SC000641)

The following bugs have been corrected:

- SDMXRI-958: Support for leap years in SdmxSource.NET Reporting Days

## sdmxsource.net v1.15.0 (2019-02-15)

## Details v1.15.0

1. SDMX-JSON writer improvements from OECD
   1. Annotations added at all levels to json data & structure messages according to spec [https://github.com/sdmx-twg/sdmx-json/blob/master/data-message/docs/1-sdmx-json-field-guide.md](https://github.com/sdmx-twg/sdmx-json/blob/master/data-message/docs/1-sdmx-json-field-guide.md)
   1. Dataflow & DSD annotations are merged in json data message, if the annotation with the same type present then lower level takes priority.

## Tickets v1.15.0

The following new features added:

- SDMXRI-907: Add order property and annotations to component value object in json writer.

## sdmxsource.net v1.14.1 (2019-02-07)

## Details v1.14.1

*WARNING* this version targets .NET Standard 2.0

1. Include SDMX-CSV improvements from OECD
1. Change internally the behavior of data writers to avoid starting/ending XML documents when they are in SOAP messages.
   This was required by new ASP.NET Core NSIWS
1. Add support for HTTP code 406 in a new SDMX Exception. This is not mapped to SDMX error but it is part of the HTTP Content Negotiation that SDMX REST is using.

## Tickets v1.14.1

The following new features added:

- SDMXRI-839: Migrate NSI WS and MAWS to .NET Core (QTM1-2018.SC000641)
- SDMXRI-902: Apply timeFormat in csv writer to normalize date outputs (QTM6-2018.SC000641)
- SDMXRI-873: SdmxSource support HTTP 406 status code

## sdmxsource.net v1.14.0 (2019-01-15)

## Details v1.14.0

*WARNING* this version targets .NET Standard 2.0

1. Switched to package references. As a result Visual Studio versions earlier than 2017 are no longer supported.
1. This version targets .NET Standard 2.0. Applications using SdmxSource must target either
   1. .NET Standard 2.0 or later
   1. .NET Framework 4.6.2 or later
   1. .NET Core 2.1 or later
1. The tests require .NET Core 2.1 except for GuideTests which require .NET Framework 4.6.2. Tests now use NUnit 3.11
1. NuGet package split into multiple packages.
   1. Estat.SdmxSource.SdmxAPI
   1. Estat.SdmxSource.SdmxDataParser
   1. Estat.SdmxSource.SdmxEdiParser
   1. Estat.SdmxSource.SdmxMlConstants
   1. Estat.SdmxSource.SdmxObjects
   1. Estat.SdmxSource.SdmxParseBase
   1. Estat.SdmxSource.SdmxQueryBuilder
   1. Estat.SdmxSource.SdmxSourceUtil
   1. Estat.SdmxSource.SdmxStructureParser
   1. Estat.SdmxSource.SdmxStructureRetrieval
   1. Estat.SdmxSource.StructureMutableParser
   1. Org.Sdmx.Resources.SdmxMl.Schemas.V10
   1. Org.Sdmx.Resources.SdmxMl.Schemas.V20
   1. Org.Sdmx.Resources.SdmxMl.Schemas.V21
1. SDMX-JSON data and structure format fixes
1. SDMX-JSON support for content constraints

## Tickets v1.14.0

The following new features added:

- SDMXRI-868: Rename nuget packages (QTM1-2018.SC000641)
- SDMXRI-838: SDMX RI libraries to target .NET Standard 2.0 (QTM1-2018.SC000641)
- SDMXRI-837: Add support for content constraints for JSON SDMX (OECD,QTM6-2018.SC000641)

The following bugs have been corrected:

- SDMXRI-826: SDMX-JSON requesting dataflow results to NPE
- SDMXRI-850: Querying for JSON-SDMX data message fails if selected language labels are missing
- Fix NPE in empty `uri` in SDMX-JSON Structure files

## sdmxsource.net v1.13.0 (2018-10-31)

## Details v1.13.0

1. Synchronize .NET SDMX v2.0 Dataset element ouput to Java
1. Fix Frequency code for reporting period
1. Support JSON Submit Structure
1. JSON fixes by OECD:
   1. Fix for the missing series level attributes when the component id is not identical to the concept id in json data messages
   1. Parent codes no longer presented at code lists with hierarchy in structure section of `data+json;version=1.0.0`

## Tickets v1.13.0

The following new features added:

- SDMXRI-835: Include the dataflowId and the dataflowAgencyId in Compact Data Dataset
- SDMXRI-712: SDMX-JSON structure submit via REST in NSI WS (COLLAB,OECD,QTM1-2018.SC000641)

The following bugs have been corrected:

- SDMXRI-760: Attribute management fix in csv,  json, xml (abstract, generic readers) (OECD)
- SDMXRI-667: SDMX v2.1 Time Range in .NET (QTM6-2017.SC000281)

## sdmxsource.net v1.12.1 (2018-09-26)

## Details v1.12.1

1. Changes in the SDMX CSV writer error messages (OECD)
1. Align SDMX v2.1 Generic Data output to the Java version
1. Fix available constraint query handling
1. Fix text format issue in SDMX JSON Structure parser (OECD)
1. Fix time period output in SDMX-JSON data (OECD)
1. Fix SDMX-JSON DSD dimension position, it starts now from 0. (OECD)
1. Fix SDMX-JSON DSD concept role output, it now has the full URN instead of the id. (OECD)

## Tickets v1.12.1

The following new features added:

- SDMXRI-686: Generic 21 Format and ObsDimension TAG (QTM1-2018.SC000641)
- SDMXRI-642: SDMX-csv writer .NET (OECD)

The following bugs have been corrected:

- SDMXRI-781: SDMX Structure JSON parser (V10) text format issue (OECD)
- SDMXRI-789: "id" and "name" missing for the TIME_PERIOD in JSON (3rd-level-support) (OECD)
- SDMXRI-669: Support for partial/special codelists in REST in .NET (QTM6-2017.SC000281)
- SDMXRI-785: SDMX JSON v1.0 structural metadata writer possibly issues (OECD)

## sdmxsource.net v1.12.0 (2018-08-03)

## Details v1.12.0

1. Applied bug fixes from OECD
   1. Fixes for JSON writers - The JSON response was started in the constructor of the writer - This lead exception in NSI WS when no data should be presented (404 Not found) but streaming has already started.
   1. Time format related exception when reading an SDMX-ML file without observations caused by missing time dimension value.
   1. Fix duplicated codes for `data+json;version=1.0.0`
   1. SDMX-CSV Read group & dimension level attributes in Observation object
1. `SdmxSemanticException` is now associated with `HTTP` status code `403`
1. Data Registration fixes:
   1. ID was ignored
   1. In Data Source the `WADL` and `WSDL` were mixed up
1. Merge support for content constrains from OECD

## Tickets v1.12.0

The following new features added:

- SDMXRI-760: Attribute management fix in csv,  json, xml (abstract, generic readers)
- SDMXRI-673: SDMX RI  support for SDMX REST v1.2.0  (QTM6-2017.SC000281,QTM8-2017.SC000281)
- SDMXRI-675: POST & Update support in .NET (QTM6-2017.SC000281)
- SDMXRI-596: Support for applying content constrains to codelists in REST (COLLAB,DevelopedByOECD,QTM6-2017.SC000281,QTM8-2017.SC000281)

## sdmxsource.net v1.11.0 (2018-07-02)

## Details v1.11.0

1. OECD improvements on SDMX-CSV and SDMX-JSON
1. Refactor data availability to match the [latest changes](https://github.com/sdmx-twg/sdmx-rest/blob/develop/v2_1/ws/rest/docs/4_6_1_other_queries.md)
1. Bugfixes related to Data transformation, reading and writing
1. When writing SDMX v2.0 datasets with DSD specific schema, use the same namespace as in Converter and Registry.

## Tickets v1.11.0

The following new features added:

- SDMXRI-727: New SDMX-JSON data message format
- SDMXRI-672: NSI Web Client .NET to use SDMX-CSV
- SDMXRI-702: SDMX JSON Structure Reader & Writer
- SDMXRI-721: Adjust REST data availability according the SDMX TWG proposal
- SDMXRI-693: SDMX RI vs SDMX Converter SDMX v2.0 data namespaces

The following bugs have been corrected:

- SDMXRI-724: GenericDataReader for flat file format throws an error while calling MoveNextDataset method
- SDMXRI-723: SDMX-CSV writer fails when DSD has no attributes

## sdmxsource.net v1.10.0 (2018-04-30)

## Details v1.10.0

1. SDMX v2.1 Time Range support. Time zone Offset support. Improved support for Reporting Period and support for translating it to Time Range. All SDMX v2.1 time formats are recognized
1. Support for data availability via REST. New model API for retrieving dynamic constraints

## Tickets v1.10.0

The following new features added:

- SDMXRI-669: Support for partial/special codelists in REST in .NET
- SDMXRI-667: SDMX v2.1 Time Range in .NET

The following bugs have been corrected:

- SDMXRI-716: SDMX REST structure query parameter detail=referencestubs not supported

## sdmxsource.net v1.9.0 (2018-03-15)

## Details v1.9.0

1. Added more IDataWriterEngine decorators related to writing Reporting Period. Also added support for reading the SDMX v2.1 special Data Structure attribute `REPORTING_YEAR_START_DAY`

## Tickets v1.9.0

The following new features added:

- SDMXRI-537: Support transcoding for non-calendar years

The following bugs have been corrected:

- SDMXRI-692: When observation value is null, generate "null" in JSON

## sdmxsource.net v1.8.0 (2018-02-02)

## Details v1.8.0

1. Decrease size of structural metadata in SDMX-ML formats by having better namespace management.
1. Merged SDMX-CSV from OECD

## Tickets v1.8.0

The following new features added:

- SDMXRI-593: Inefficient namespace management in generated SDMX structure messages
- SDMXRI-642: SDMX-csv writer .NET

## SdmxSource.NET_v1.7.1 (2018-01-30)

## Details v1.7.1

This release includes:

1. Bug-fixes in Cross Sectional readers and writers
1. Improve handling of annotations in data readers
1. JSON Writer fixes from OECD

## Tickets v1.7.1

The following new features added:

- SDMXRI-577: SNA 1.9 Problems querying the data using Web Client
- SDMXRI-599: MA WS/API get mapped data with local codes

The following bugs have been corrected:

- SDMXRI-653: .NET SdmxSource Cross Sectional writer uses Component ID instead of concept ID
- SDMXRI-638: Json writer fixes

## SdmxSource.NET_v1.7.1 (2017-12-08)

## Details v1.7.0

This release includes:

1. A new exception and corresponding SDMX Error code. `SdmxConflictException` and `SdmxErrorEnumType.Conflict`. *Please Note* `Conflict` is not an official SDMX error code.
1. JSON data writer updates from OECD
1. Bug-fixes generated by support tickets and found while testing

## Tickets v1.7.0

The following new features added:

- SDMXRI-550: Allow updating of non-final properties of a final artefact
- SDMXRI-456: "Internal Server Error" message instead of the specific message returned previously

The following bugs have been corrected:

- SDMXRI-616: Observation key position can be wrong in `SdmxSource` `SDMX-JSON` data writer
- SDMXRI-629: SdmxSource.NET Annotation related bug
- SDMXRI-624: SdmxSource.NET URL handling related bugs

## SdmxSource.NET_v1.6.0 (2017-10-31)

## Details v1.6.0

This release includes

1. A micro-data enhancement, the SDMX writers have been modified so it doesn't write a `NaN` when no observation value is given
1. Add an `Other` data type, so new data formats writers and readers can use it
1. Added in programmers guide a new chapter explaining how to make SdmxSource output human readable error messages
1. Avoid encapsulating exceptions into other exceptions when there is no added value in some cases

## Tickets v1.6.0

The following new features added:

- SDMXRI-564: EGR Obs value NaN vs no value output
- SDMXRI-563: ISTAT RDF Writer
- SDMXRI-531: Document MessageDecoder and Error codes
- SDMXSOURCE-10: wrong exception handling when catching Throwable

## SdmxSource.NET_v1.5.0 (2017-10-06)

## Details v1.5.0

This release adds support for reporting periods such as yyyy-Mmm, yyyy-A1, yyyy-Dddd and `REPORTING_START_YEAR_DAY` attribute

Specifically:

- a class [ReportingTimePeriod](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/SdmxSourceUtil/Sdmxsource/Sdmx/Util/Date/ReportingTimePeriod.cs?at=refs%2Fheads%2F1.5.0) has been added to convert between gregorian and reporting periods.
- The data reader engines `IDataReaderEngine` for SDMX v2.1 XML formats have been modified to convert reporting periods to gregorian, taking in consideration the `REPORTING_START_YEAR_DAY` attribute if it exists. Normally tools using those readers should not be modified. If a reporting period is encountered like `2001-M01` then the reader will produce `2001-01`
- A decorator data writer engine, [ReportingPeriodDataWriterEngine](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/SdmxDataParser/Engine/ReportingPeriodDataWriterEngine.cs?at=refs%2Fheads%2F1.5.0) has been added which will convert gregorian periods to reporting periods, so the output will have values like `2001-A1` instead of `2001`. This is not automatically used, the developer must explicitly use it to decorate an existing data writer engine.
- Data Queries, both REST and SOAP 2.1, will translate requests with reporting period to gregorian.

## Tickets v1.5.0

- SDMXSOURCE-9: Support for reporting periods (yyyy-Mmm, yyyy-A1, yyyy-Dddd)

## SdmxSource.NET_v1.4.2 (2017-04-25)

The following new features added:

- SDMXRI-466: MSD/MDF persist/retrieve
- SDMXRI-489: SdxmSource mutable TextFormat Bean/Object doesn't have start/endTime
- SDMXRI-488: SdmxSource (Java,.NET, .Org) TextFormat Multilingual property not correctly handled
- SDMXRI-487: Modify ISTAT retriever code to be compatible future SDMX RI proof
- SDMXRI-427: Language 'la'
- SDMXRI-464: NsiWebService: issue on localisation codes

The following bugs have been corrected:

- SDMXRI-258: GenericData flat dataset generation issue
